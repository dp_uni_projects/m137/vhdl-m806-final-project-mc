# Multi-cycle ARM CPU implementation in VHDL
Dimitris Pagagiannis
EN1190004

## Running a custom assembly program
- Write the program and save it to a file
- Compile it with `fasmarm` (e.g. `./fasmarm test_complete.asm test_complete.bin`)
- Dump the result in hex format (e.g. `hexdump test_complete.bin -v -e '1/4 "%08x " "\n"'`)
- Replace the contents of the `ROM` constant found in `ROM_ARRAY.vhd` with the instructions that the `hexdump` produced, wrapped in `X"..."`, e.g. `e3a000ff` becomes `X"e3a000ff"`)
