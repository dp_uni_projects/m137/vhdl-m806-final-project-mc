// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Sun Oct 11 17:09:32 2020
// Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/mpliax/Documents/workspace/vhdl-m806-final-project-mc/vhdl-m806-final-project.sim/sim_1/impl/timing/xsim/ALU_TB_time_impl.v
// Design      : ALU_REGWE_32
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module ADDER_n
   (\Dout_reg[30] ,
    \Dout_reg[30]_0 ,
    \Dout_reg[1] ,
    \Dout_reg[1]_0 ,
    \Dout_reg[1]_1 ,
    \Dout_reg[1]_2 ,
    \Dout_reg[1]_3 ,
    \Dout_reg[1]_4 ,
    \Dout_reg[1]_5 ,
    \Dout_reg[1]_6 ,
    \Dout_reg[1]_7 ,
    \Dout_reg[1]_8 ,
    \Dout_reg[1]_9 ,
    \Dout_reg[1]_10 ,
    \Dout_reg[1]_11 ,
    \Dout_reg[1]_12 ,
    \Dout_reg[1]_13 ,
    \Dout_reg[1]_14 ,
    \Dout_reg[1]_15 ,
    \Dout_reg[1]_16 ,
    \Dout_reg[1]_17 ,
    \Dout_reg[1]_18 ,
    \Dout_reg[1]_19 ,
    \Dout_reg[1]_20 ,
    \Dout_reg[1]_21 ,
    \Dout_reg[1]_22 ,
    \Dout_reg[1]_23 ,
    \Dout_reg[1]_24 ,
    \Dout_reg[1]_25 ,
    Q,
    \Dout[0]_i_2__0_0 ,
    \Dout[4]_i_4 ,
    \Dout[8]_i_4_0 ,
    \Dout[12]_i_4_0 ,
    \Dout[16]_i_4_0 ,
    \Dout[20]_i_4_0 ,
    \Dout[24]_i_4_0 ,
    DI,
    \Dout_reg[3] ,
    O,
    \Dout_reg[0] ,
    \Dout_reg[0]_0 ,
    S_Sub_in,
    \Dout_reg[31]_2[25]_repN_alias );
  output [5:0]\Dout_reg[30] ;
  output [0:0]\Dout_reg[30]_0 ;
  output \Dout_reg[1] ;
  output \Dout_reg[1]_0 ;
  output \Dout_reg[1]_1 ;
  output \Dout_reg[1]_2 ;
  output \Dout_reg[1]_3 ;
  output \Dout_reg[1]_4 ;
  output \Dout_reg[1]_5 ;
  output \Dout_reg[1]_6 ;
  output \Dout_reg[1]_7 ;
  output \Dout_reg[1]_8 ;
  output \Dout_reg[1]_9 ;
  output \Dout_reg[1]_10 ;
  output \Dout_reg[1]_11 ;
  output \Dout_reg[1]_12 ;
  output \Dout_reg[1]_13 ;
  output \Dout_reg[1]_14 ;
  output \Dout_reg[1]_15 ;
  output \Dout_reg[1]_16 ;
  output \Dout_reg[1]_17 ;
  output \Dout_reg[1]_18 ;
  output \Dout_reg[1]_19 ;
  output \Dout_reg[1]_20 ;
  output \Dout_reg[1]_21 ;
  output \Dout_reg[1]_22 ;
  output \Dout_reg[1]_23 ;
  output \Dout_reg[1]_24 ;
  output \Dout_reg[1]_25 ;
  input [31:0]Q;
  input [3:0]\Dout[0]_i_2__0_0 ;
  input [3:0]\Dout[4]_i_4 ;
  input [3:0]\Dout[8]_i_4_0 ;
  input [3:0]\Dout[12]_i_4_0 ;
  input [3:0]\Dout[16]_i_4_0 ;
  input [3:0]\Dout[20]_i_4_0 ;
  input [3:0]\Dout[24]_i_4_0 ;
  input [0:0]DI;
  input [3:0]\Dout_reg[3] ;
  input [3:0]O;
  input [1:0]\Dout_reg[0] ;
  input [26:0]\Dout_reg[0]_0 ;
  input [22:0]S_Sub_in;
  input \Dout_reg[31]_2[25]_repN_alias ;

  wire [0:0]DI;
  wire [3:0]\Dout[0]_i_2__0_0 ;
  wire [3:0]\Dout[12]_i_4_0 ;
  wire [3:0]\Dout[16]_i_4_0 ;
  wire [3:0]\Dout[20]_i_4_0 ;
  wire [3:0]\Dout[24]_i_4_0 ;
  wire [3:0]\Dout[4]_i_4 ;
  wire [3:0]\Dout[8]_i_4_0 ;
  wire [1:0]\Dout_reg[0] ;
  wire [26:0]\Dout_reg[0]_0 ;
  wire \Dout_reg[1] ;
  wire \Dout_reg[1]_0 ;
  wire \Dout_reg[1]_1 ;
  wire \Dout_reg[1]_10 ;
  wire \Dout_reg[1]_11 ;
  wire \Dout_reg[1]_12 ;
  wire \Dout_reg[1]_13 ;
  wire \Dout_reg[1]_14 ;
  wire \Dout_reg[1]_15 ;
  wire \Dout_reg[1]_16 ;
  wire \Dout_reg[1]_17 ;
  wire \Dout_reg[1]_18 ;
  wire \Dout_reg[1]_19 ;
  wire \Dout_reg[1]_2 ;
  wire \Dout_reg[1]_20 ;
  wire \Dout_reg[1]_21 ;
  wire \Dout_reg[1]_22 ;
  wire \Dout_reg[1]_23 ;
  wire \Dout_reg[1]_24 ;
  wire \Dout_reg[1]_25 ;
  wire \Dout_reg[1]_3 ;
  wire \Dout_reg[1]_4 ;
  wire \Dout_reg[1]_5 ;
  wire \Dout_reg[1]_6 ;
  wire \Dout_reg[1]_7 ;
  wire \Dout_reg[1]_8 ;
  wire \Dout_reg[1]_9 ;
  wire [5:0]\Dout_reg[30] ;
  wire [0:0]\Dout_reg[30]_0 ;
  wire \Dout_reg[31]_2[25]_repN_alias ;
  wire [3:0]\Dout_reg[3] ;
  wire [3:0]O;
  wire [31:0]Q;
  wire S0_carry__0_n_0;
  wire S0_carry__1_n_0;
  wire S0_carry__2_n_0;
  wire S0_carry__3_n_0;
  wire S0_carry__4_n_0;
  wire S0_carry__5_n_0;
  wire S0_carry_n_0;
  wire [30:0]S_Add_in;
  wire [22:0]S_Sub_in;
  wire [2:0]NLW_S0_carry_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__4_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__5_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__6_CO_UNCONNECTED;

  LUT6 #(
    .INIT(64'hFCFAFC0AFC0A0C0A)) 
    \Dout[0]_i_2 
       (.I0(\Dout_reg[30] [5]),
        .I1(O[3]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0] [0]),
        .I4(\Dout_reg[0]_0 [26]),
        .I5(Q[31]),
        .O(\Dout_reg[1] ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[0]_i_2__0 
       (.I0(S_Add_in[0]),
        .I1(S_Sub_in[0]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [0]),
        .I4(Q[0]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[10]_i_4 
       (.I0(S_Add_in[10]),
        .I1(S_Sub_in[5]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [5]),
        .I4(Q[10]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_5 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[11]_i_4 
       (.I0(S_Add_in[11]),
        .I1(S_Sub_in[6]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [6]),
        .I4(Q[11]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_6 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[12]_i_4 
       (.I0(S_Add_in[12]),
        .I1(S_Sub_in[7]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [7]),
        .I4(Q[12]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_7 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[13]_i_4 
       (.I0(S_Add_in[13]),
        .I1(S_Sub_in[8]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [8]),
        .I4(Q[13]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_8 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[14]_i_4 
       (.I0(S_Add_in[14]),
        .I1(S_Sub_in[9]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [9]),
        .I4(Q[14]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_9 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[15]_i_4 
       (.I0(S_Add_in[15]),
        .I1(S_Sub_in[10]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [10]),
        .I4(Q[15]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_10 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[16]_i_4 
       (.I0(S_Add_in[16]),
        .I1(S_Sub_in[11]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [11]),
        .I4(Q[16]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_11 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[17]_i_4 
       (.I0(S_Add_in[17]),
        .I1(S_Sub_in[12]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [12]),
        .I4(Q[17]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_12 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[18]_i_4 
       (.I0(S_Add_in[18]),
        .I1(S_Sub_in[13]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [13]),
        .I4(Q[18]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_13 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[19]_i_4 
       (.I0(S_Add_in[19]),
        .I1(S_Sub_in[14]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [14]),
        .I4(Q[19]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_14 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[20]_i_4 
       (.I0(S_Add_in[20]),
        .I1(S_Sub_in[15]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [15]),
        .I4(Q[20]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_15 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[21]_i_4 
       (.I0(S_Add_in[21]),
        .I1(S_Sub_in[16]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [16]),
        .I4(Q[21]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_16 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[22]_i_4 
       (.I0(S_Add_in[22]),
        .I1(S_Sub_in[17]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [17]),
        .I4(Q[22]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_17 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[23]_i_4 
       (.I0(S_Add_in[23]),
        .I1(S_Sub_in[18]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [18]),
        .I4(Q[23]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_18 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[24]_i_4 
       (.I0(S_Add_in[24]),
        .I1(S_Sub_in[19]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [19]),
        .I4(Q[24]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_19 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[25]_i_4 
       (.I0(S_Add_in[25]),
        .I1(S_Sub_in[20]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[31]_2[25]_repN_alias ),
        .I4(Q[25]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_20 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[26]_i_4 
       (.I0(S_Add_in[26]),
        .I1(S_Sub_in[21]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [21]),
        .I4(Q[26]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_21 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[27]_i_4 
       (.I0(S_Add_in[27]),
        .I1(S_Sub_in[22]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [22]),
        .I4(Q[27]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_22 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[28]_i_4 
       (.I0(S_Add_in[28]),
        .I1(O[0]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [23]),
        .I4(Q[28]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_23 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[29]_i_4 
       (.I0(S_Add_in[29]),
        .I1(O[1]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [24]),
        .I4(Q[29]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_24 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[30]_i_4 
       (.I0(S_Add_in[30]),
        .I1(O[2]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [25]),
        .I4(Q[30]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_25 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[6]_i_4 
       (.I0(S_Add_in[6]),
        .I1(S_Sub_in[1]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [1]),
        .I4(Q[6]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_1 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[7]_i_4 
       (.I0(S_Add_in[7]),
        .I1(S_Sub_in[2]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [2]),
        .I4(Q[7]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_2 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[8]_i_4 
       (.I0(S_Add_in[8]),
        .I1(S_Sub_in[3]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [3]),
        .I4(Q[8]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_3 ));
  LUT6 #(
    .INIT(64'hFCFCFC0CFA0A0A0A)) 
    \Dout[9]_i_4 
       (.I0(S_Add_in[9]),
        .I1(S_Sub_in[4]),
        .I2(\Dout_reg[0] [1]),
        .I3(\Dout_reg[0]_0 [4]),
        .I4(Q[9]),
        .I5(\Dout_reg[0] [0]),
        .O(\Dout_reg[1]_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry
       (.CI(1'b0),
        .CO({S0_carry_n_0,NLW_S0_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[3:0]),
        .O({\Dout_reg[30] [2:0],S_Add_in[0]}),
        .S(\Dout[0]_i_2__0_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__0
       (.CI(S0_carry_n_0),
        .CO({S0_carry__0_n_0,NLW_S0_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O({S_Add_in[7:6],\Dout_reg[30] [4:3]}),
        .S(\Dout[4]_i_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__1
       (.CI(S0_carry__0_n_0),
        .CO({S0_carry__1_n_0,NLW_S0_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[11:8]),
        .O(S_Add_in[11:8]),
        .S(\Dout[8]_i_4_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__2
       (.CI(S0_carry__1_n_0),
        .CO({S0_carry__2_n_0,NLW_S0_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[15:12]),
        .O(S_Add_in[15:12]),
        .S(\Dout[12]_i_4_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__3
       (.CI(S0_carry__2_n_0),
        .CO({S0_carry__3_n_0,NLW_S0_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[19:16]),
        .O(S_Add_in[19:16]),
        .S(\Dout[16]_i_4_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__4
       (.CI(S0_carry__3_n_0),
        .CO({S0_carry__4_n_0,NLW_S0_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[23:20]),
        .O(S_Add_in[23:20]),
        .S(\Dout[20]_i_4_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__5
       (.CI(S0_carry__4_n_0),
        .CO({S0_carry__5_n_0,NLW_S0_carry__5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[27:24]),
        .O(S_Add_in[27:24]),
        .S(\Dout[24]_i_4_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__6
       (.CI(S0_carry__5_n_0),
        .CO({\Dout_reg[30]_0 ,NLW_S0_carry__6_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({DI,Q[30:28]}),
        .O({\Dout_reg[30] [5],S_Add_in[30:28]}),
        .S(\Dout_reg[3] ));
endmodule

module ADD_SUB_n
   (\Dout_reg[31] ,
    CO,
    \Dout_reg[30] ,
    \Dout_reg[30]_0 ,
    \Dout_reg[1] ,
    \Dout_reg[1]_0 ,
    \Dout_reg[1]_1 ,
    \Dout_reg[1]_2 ,
    \Dout_reg[1]_3 ,
    \Dout_reg[1]_4 ,
    \Dout_reg[1]_5 ,
    \Dout_reg[1]_6 ,
    \Dout_reg[1]_7 ,
    \Dout_reg[1]_8 ,
    \Dout_reg[1]_9 ,
    \Dout_reg[1]_10 ,
    \Dout_reg[1]_11 ,
    \Dout_reg[1]_12 ,
    \Dout_reg[1]_13 ,
    \Dout_reg[1]_14 ,
    \Dout_reg[1]_15 ,
    \Dout_reg[1]_16 ,
    \Dout_reg[1]_17 ,
    \Dout_reg[1]_18 ,
    \Dout_reg[1]_19 ,
    \Dout_reg[1]_20 ,
    \Dout_reg[1]_21 ,
    \Dout_reg[1]_22 ,
    \Dout_reg[1]_23 ,
    \Dout_reg[1]_24 ,
    \Dout_reg[1]_25 ,
    Q,
    S,
    \Dout[4]_i_4 ,
    \Dout[8]_i_4 ,
    \Dout[12]_i_4 ,
    \Dout[16]_i_4 ,
    \Dout[20]_i_4 ,
    \Dout[24]_i_4 ,
    \Dout_reg[3] ,
    \Dout_reg[3]_0 ,
    \Dout[0]_i_2__0 ,
    \Dout[4]_i_4_0 ,
    \Dout[8]_i_4_0 ,
    \Dout[12]_i_4_0 ,
    \Dout[16]_i_4_0 ,
    \Dout[20]_i_4_0 ,
    \Dout[24]_i_4_0 ,
    DI,
    \Dout_reg[3]_1 ,
    \Dout_reg[0] ,
    \Dout_reg[31]_2[25]_repN_alias );
  output [5:0]\Dout_reg[31] ;
  output [0:0]CO;
  output [5:0]\Dout_reg[30] ;
  output [0:0]\Dout_reg[30]_0 ;
  output \Dout_reg[1] ;
  output \Dout_reg[1]_0 ;
  output \Dout_reg[1]_1 ;
  output \Dout_reg[1]_2 ;
  output \Dout_reg[1]_3 ;
  output \Dout_reg[1]_4 ;
  output \Dout_reg[1]_5 ;
  output \Dout_reg[1]_6 ;
  output \Dout_reg[1]_7 ;
  output \Dout_reg[1]_8 ;
  output \Dout_reg[1]_9 ;
  output \Dout_reg[1]_10 ;
  output \Dout_reg[1]_11 ;
  output \Dout_reg[1]_12 ;
  output \Dout_reg[1]_13 ;
  output \Dout_reg[1]_14 ;
  output \Dout_reg[1]_15 ;
  output \Dout_reg[1]_16 ;
  output \Dout_reg[1]_17 ;
  output \Dout_reg[1]_18 ;
  output \Dout_reg[1]_19 ;
  output \Dout_reg[1]_20 ;
  output \Dout_reg[1]_21 ;
  output \Dout_reg[1]_22 ;
  output \Dout_reg[1]_23 ;
  output \Dout_reg[1]_24 ;
  output \Dout_reg[1]_25 ;
  input [31:0]Q;
  input [3:0]S;
  input [3:0]\Dout[4]_i_4 ;
  input [3:0]\Dout[8]_i_4 ;
  input [3:0]\Dout[12]_i_4 ;
  input [3:0]\Dout[16]_i_4 ;
  input [3:0]\Dout[20]_i_4 ;
  input [3:0]\Dout[24]_i_4 ;
  input [26:0]\Dout_reg[3] ;
  input [3:0]\Dout_reg[3]_0 ;
  input [3:0]\Dout[0]_i_2__0 ;
  input [3:0]\Dout[4]_i_4_0 ;
  input [3:0]\Dout[8]_i_4_0 ;
  input [3:0]\Dout[12]_i_4_0 ;
  input [3:0]\Dout[16]_i_4_0 ;
  input [3:0]\Dout[20]_i_4_0 ;
  input [3:0]\Dout[24]_i_4_0 ;
  input [0:0]DI;
  input [3:0]\Dout_reg[3]_1 ;
  input [1:0]\Dout_reg[0] ;
  input \Dout_reg[31]_2[25]_repN_alias ;

  wire [0:0]CO;
  wire [0:0]DI;
  wire [3:0]\Dout[0]_i_2__0 ;
  wire [3:0]\Dout[12]_i_4 ;
  wire [3:0]\Dout[12]_i_4_0 ;
  wire [3:0]\Dout[16]_i_4 ;
  wire [3:0]\Dout[16]_i_4_0 ;
  wire [3:0]\Dout[20]_i_4 ;
  wire [3:0]\Dout[20]_i_4_0 ;
  wire [3:0]\Dout[24]_i_4 ;
  wire [3:0]\Dout[24]_i_4_0 ;
  wire [3:0]\Dout[4]_i_4 ;
  wire [3:0]\Dout[4]_i_4_0 ;
  wire [3:0]\Dout[8]_i_4 ;
  wire [3:0]\Dout[8]_i_4_0 ;
  wire [1:0]\Dout_reg[0] ;
  wire \Dout_reg[1] ;
  wire \Dout_reg[1]_0 ;
  wire \Dout_reg[1]_1 ;
  wire \Dout_reg[1]_10 ;
  wire \Dout_reg[1]_11 ;
  wire \Dout_reg[1]_12 ;
  wire \Dout_reg[1]_13 ;
  wire \Dout_reg[1]_14 ;
  wire \Dout_reg[1]_15 ;
  wire \Dout_reg[1]_16 ;
  wire \Dout_reg[1]_17 ;
  wire \Dout_reg[1]_18 ;
  wire \Dout_reg[1]_19 ;
  wire \Dout_reg[1]_2 ;
  wire \Dout_reg[1]_20 ;
  wire \Dout_reg[1]_21 ;
  wire \Dout_reg[1]_22 ;
  wire \Dout_reg[1]_23 ;
  wire \Dout_reg[1]_24 ;
  wire \Dout_reg[1]_25 ;
  wire \Dout_reg[1]_3 ;
  wire \Dout_reg[1]_4 ;
  wire \Dout_reg[1]_5 ;
  wire \Dout_reg[1]_6 ;
  wire \Dout_reg[1]_7 ;
  wire \Dout_reg[1]_8 ;
  wire \Dout_reg[1]_9 ;
  wire [5:0]\Dout_reg[30] ;
  wire [0:0]\Dout_reg[30]_0 ;
  wire [5:0]\Dout_reg[31] ;
  wire \Dout_reg[31]_2[25]_repN_alias ;
  wire [26:0]\Dout_reg[3] ;
  wire [3:0]\Dout_reg[3]_0 ;
  wire [3:0]\Dout_reg[3]_1 ;
  wire [31:0]Q;
  wire [3:0]S;
  wire [30:0]S_Sub_in;

  ADDER_n ADD_UNIT
       (.DI(DI),
        .\Dout[0]_i_2__0_0 (\Dout[0]_i_2__0 ),
        .\Dout[12]_i_4_0 (\Dout[12]_i_4_0 ),
        .\Dout[16]_i_4_0 (\Dout[16]_i_4_0 ),
        .\Dout[20]_i_4_0 (\Dout[20]_i_4_0 ),
        .\Dout[24]_i_4_0 (\Dout[24]_i_4_0 ),
        .\Dout[4]_i_4 (\Dout[4]_i_4_0 ),
        .\Dout[8]_i_4_0 (\Dout[8]_i_4_0 ),
        .\Dout_reg[0] (\Dout_reg[0] ),
        .\Dout_reg[0]_0 (\Dout_reg[3] ),
        .\Dout_reg[1] (\Dout_reg[1] ),
        .\Dout_reg[1]_0 (\Dout_reg[1]_0 ),
        .\Dout_reg[1]_1 (\Dout_reg[1]_1 ),
        .\Dout_reg[1]_10 (\Dout_reg[1]_10 ),
        .\Dout_reg[1]_11 (\Dout_reg[1]_11 ),
        .\Dout_reg[1]_12 (\Dout_reg[1]_12 ),
        .\Dout_reg[1]_13 (\Dout_reg[1]_13 ),
        .\Dout_reg[1]_14 (\Dout_reg[1]_14 ),
        .\Dout_reg[1]_15 (\Dout_reg[1]_15 ),
        .\Dout_reg[1]_16 (\Dout_reg[1]_16 ),
        .\Dout_reg[1]_17 (\Dout_reg[1]_17 ),
        .\Dout_reg[1]_18 (\Dout_reg[1]_18 ),
        .\Dout_reg[1]_19 (\Dout_reg[1]_19 ),
        .\Dout_reg[1]_2 (\Dout_reg[1]_2 ),
        .\Dout_reg[1]_20 (\Dout_reg[1]_20 ),
        .\Dout_reg[1]_21 (\Dout_reg[1]_21 ),
        .\Dout_reg[1]_22 (\Dout_reg[1]_22 ),
        .\Dout_reg[1]_23 (\Dout_reg[1]_23 ),
        .\Dout_reg[1]_24 (\Dout_reg[1]_24 ),
        .\Dout_reg[1]_25 (\Dout_reg[1]_25 ),
        .\Dout_reg[1]_3 (\Dout_reg[1]_3 ),
        .\Dout_reg[1]_4 (\Dout_reg[1]_4 ),
        .\Dout_reg[1]_5 (\Dout_reg[1]_5 ),
        .\Dout_reg[1]_6 (\Dout_reg[1]_6 ),
        .\Dout_reg[1]_7 (\Dout_reg[1]_7 ),
        .\Dout_reg[1]_8 (\Dout_reg[1]_8 ),
        .\Dout_reg[1]_9 (\Dout_reg[1]_9 ),
        .\Dout_reg[30] (\Dout_reg[30] ),
        .\Dout_reg[30]_0 (\Dout_reg[30]_0 ),
        .\Dout_reg[31]_2[25]_repN_alias (\Dout_reg[31]_2[25]_repN_alias ),
        .\Dout_reg[3] (\Dout_reg[3]_1 ),
        .O({\Dout_reg[31] [5],S_Sub_in[30:28]}),
        .Q(Q),
        .S_Sub_in({S_Sub_in[27:6],S_Sub_in[0]}));
  SUBTRACTOR_n SUB_UNIT
       (.CO(CO),
        .\Dout[12]_i_4 (\Dout[12]_i_4 ),
        .\Dout[16]_i_4 (\Dout[16]_i_4 ),
        .\Dout[20]_i_4 (\Dout[20]_i_4 ),
        .\Dout[24]_i_4 (\Dout[24]_i_4 ),
        .\Dout[4]_i_4 (\Dout[4]_i_4 ),
        .\Dout[8]_i_4 (\Dout[8]_i_4 ),
        .\Dout_reg[31] (\Dout_reg[31] ),
        .\Dout_reg[3] (\Dout_reg[3] [26]),
        .\Dout_reg[3]_0 (\Dout_reg[3]_0 ),
        .Q(Q[30:0]),
        .S(S),
        .S_Sub_in({S_Sub_in[30:6],S_Sub_in[0]}));
endmodule

(* ECO_CHECKSUM = "13a9f950" *) 
(* NotValidForBitStream *)
module ALU_REGWE_32
   (SRC_A,
    SRC_B,
    ALUControl,
    SHAMT5,
    Flags,
    ALUResult,
    CLK,
    RESET,
    WE);
  input [31:0]SRC_A;
  input [31:0]SRC_B;
  input [3:0]ALUControl;
  input [4:0]SHAMT5;
  output [3:0]Flags;
  output [31:0]ALUResult;
  input CLK;
  input RESET;
  input WE;

  wire \ADD_SUB/C_Add_in ;
  wire \ADD_SUB/C_Sub_in ;
  wire [31:1]\ADD_SUB/S_Add_in ;
  wire [31:1]\ADD_SUB/S_Sub_in ;
  wire [3:0]ALUControl;
  wire [3:0]ALUControl_IBUF;
  wire [1:0]ALUControl_in;
  wire [31:0]ALUResult;
  wire [31:0]ALUResult_OBUF;
  wire [31:0]ALUResult_in;
  wire CLK;
  wire CLK_IBUF;
  wire CLK_IBUF_BUFG;
  wire \Dout[13]_i_3_n_0_alias ;
  wire \Dout[13]_i_8_n_0_alias ;
  wire \Dout[14]_i_3_n_0_alias ;
  wire \Dout[14]_i_8_n_0_alias ;
  wire \Dout[2]_i_3_n_0_alias ;
  wire \Dout[3]_i_10_n_0_alias ;
  wire \Dout[7]_i_3_n_0_alias ;
  wire \Dout_reg[0]_12_repN_alias ;
  wire \Dout_reg[0]_13_repN_alias ;
  wire \Dout_reg[0]_2_repN_1_alias ;
  wire \Dout_reg[0]_2_repN_alias ;
  wire \Dout_reg[0]_30_repN_alias ;
  wire \Dout_reg[31]_2[17]_repN_alias ;
  wire \Dout_reg[31]_2[19]_repN_1_alias ;
  wire \Dout_reg[31]_2[25]_repN_3_alias ;
  wire \Dout_reg[31]_2[25]_repN_alias ;
  wire \Dout_reg[31]_2[26]_repN_alias ;
  wire \Dout_reg[31]_2[27]_repN_1_alias ;
  wire \Dout_reg[31]_2[27]_repN_alias ;
  wire \Dout_reg[31]_2[28]_repN_3_alias ;
  wire \Dout_reg[31]_2[28]_repN_alias ;
  wire \Dout_reg[31]_2[29]_repN_1_alias ;
  wire \Dout_reg[31]_2[29]_repN_alias ;
  wire \Dout_reg[31]_2[2]_repN_alias ;
  wire \Dout_reg[31]_2[30]_repN_1_alias ;
  wire \Dout_reg[31]_2[30]_repN_2_alias ;
  wire \Dout_reg[31]_2[30]_repN_3_alias ;
  wire \Dout_reg[31]_2[30]_repN_4_alias ;
  wire \Dout_reg[31]_2[30]_repN_alias ;
  wire \Dout_reg[31]_2[31]_repN_1_alias ;
  wire \Dout_reg[31]_2[31]_repN_alias ;
  wire \Dout_reg[31]_2[4]_repN_alias ;
  wire [3:0]Flags;
  wire [3:1]Flags_OBUF;
  wire [3:2]Flags_in;
  wire \Q[1]_repN_1_alias ;
  wire \Q[1]_repN_2_alias ;
  wire \Q[1]_repN_3_alias ;
  wire \Q[1]_repN_5_alias ;
  wire \Q[1]_repN_6_alias ;
  wire \Q[1]_repN_7_alias ;
  wire \Q[1]_repN_alias ;
  wire \Q[2]_repN_1_alias ;
  wire \Q[2]_repN_2_alias ;
  wire \Q[2]_repN_3_alias ;
  wire \Q[2]_repN_4_alias ;
  wire \Q[2]_repN_5_alias ;
  wire \Q[2]_repN_6_alias ;
  wire \Q[2]_repN_7_alias ;
  wire \Q[2]_repN_alias ;
  wire \Q[3]_repN_1_alias ;
  wire \Q[3]_repN_2_alias ;
  wire \Q[3]_repN_3_alias ;
  wire \Q[3]_repN_6_alias ;
  wire \Q[3]_repN_7_alias ;
  wire \Q[3]_repN_8_alias ;
  wire \Q[3]_repN_alias ;
  wire \Q[4]_repN_1_alias ;
  wire \Q[4]_repN_2_alias ;
  wire \Q[4]_repN_3_alias ;
  wire \Q[4]_repN_5_alias ;
  wire \Q[4]_repN_alias ;
  wire RESET;
  wire RESET_IBUF;
  wire [4:1]ROTATE_RIGHT1;
  wire [4:0]SHAMT5;
  wire [4:0]SHAMT5_IBUF;
  wire [31:0]SRC_A;
  wire [31:0]SRC_A_IBUF;
  wire [31:0]SRC_A_in;
  wire [31:0]SRC_B;
  wire [31:0]SRC_B_IBUF;
  wire [31:0]SRC_B_in;
  wire WE;
  wire WE_IBUF;
  wire Z;
  wire alu_in_n_13;
  wire alu_in_n_14;
  wire alu_in_n_15;
  wire alu_in_n_16;
  wire alu_in_n_17;
  wire alu_in_n_18;
  wire alu_in_n_19;
  wire alu_in_n_20;
  wire alu_in_n_21;
  wire alu_in_n_22;
  wire alu_in_n_23;
  wire alu_in_n_24;
  wire alu_in_n_25;
  wire alu_in_n_26;
  wire alu_in_n_27;
  wire alu_in_n_28;
  wire alu_in_n_29;
  wire alu_in_n_30;
  wire alu_in_n_31;
  wire alu_in_n_32;
  wire alu_in_n_33;
  wire alu_in_n_34;
  wire alu_in_n_35;
  wire alu_in_n_36;
  wire alu_in_n_37;
  wire alu_in_n_38;
  wire alu_in_n_39;
  wire alu_in_n_40;
  wire alu_in_n_6;
  wire reg_a_n_0;
  wire reg_a_n_1;
  wire reg_a_n_34;
  wire reg_a_n_35;
  wire reg_a_n_36;
  wire reg_a_n_37;
  wire reg_a_n_38;
  wire reg_a_n_39;
  wire reg_a_n_40;
  wire reg_a_n_41;
  wire reg_a_n_42;
  wire reg_a_n_43;
  wire reg_a_n_44;
  wire reg_a_n_45;
  wire reg_a_n_46;
  wire reg_a_n_47;
  wire reg_a_n_48;
  wire reg_a_n_49;
  wire reg_a_n_50;
  wire reg_a_n_51;
  wire reg_a_n_52;
  wire reg_a_n_53;
  wire reg_a_n_54;
  wire reg_a_n_55;
  wire reg_a_n_56;
  wire reg_a_n_57;
  wire reg_a_n_58;
  wire reg_a_n_59;
  wire reg_a_n_60;
  wire reg_a_n_61;
  wire reg_a_n_62;
  wire reg_a_n_63;
  wire reg_a_n_64;
  wire reg_a_n_65;
  wire reg_alu_control_n_37;
  wire reg_alu_control_n_38;
  wire reg_alu_control_n_39;
  wire reg_alu_control_n_40;
  wire reg_alu_control_n_41;
  wire reg_b_n_0;
  wire reg_b_n_100;
  wire reg_b_n_101;
  wire reg_b_n_102;
  wire reg_b_n_103;
  wire reg_b_n_104;
  wire reg_b_n_105;
  wire reg_b_n_106;
  wire reg_b_n_107;
  wire reg_b_n_108;
  wire reg_b_n_109;
  wire reg_b_n_110;
  wire reg_b_n_111;
  wire reg_b_n_112;
  wire reg_b_n_113;
  wire reg_b_n_114;
  wire reg_b_n_115;
  wire reg_b_n_116;
  wire reg_b_n_117;
  wire reg_b_n_118;
  wire reg_b_n_119;
  wire reg_b_n_120;
  wire reg_b_n_121;
  wire reg_b_n_122;
  wire reg_b_n_123;
  wire reg_b_n_124;
  wire reg_b_n_125;
  wire reg_b_n_126;
  wire reg_b_n_127;
  wire reg_b_n_128;
  wire reg_b_n_129;
  wire reg_b_n_130;
  wire reg_b_n_131;
  wire reg_b_n_132;
  wire reg_b_n_133;
  wire reg_b_n_134;
  wire reg_b_n_135;
  wire reg_b_n_136;
  wire reg_b_n_137;
  wire reg_b_n_138;
  wire reg_b_n_139;
  wire reg_b_n_140;
  wire reg_b_n_141;
  wire reg_b_n_142;
  wire reg_b_n_143;
  wire reg_b_n_144;
  wire reg_b_n_145;
  wire reg_b_n_146;
  wire reg_b_n_147;
  wire reg_b_n_148;
  wire reg_b_n_149;
  wire reg_b_n_150;
  wire reg_b_n_151;
  wire reg_b_n_3;
  wire reg_b_n_4;
  wire reg_b_n_40;
  wire reg_b_n_41;
  wire reg_b_n_42;
  wire reg_b_n_43;
  wire reg_b_n_44;
  wire reg_b_n_45;
  wire reg_b_n_46;
  wire reg_b_n_47;
  wire reg_b_n_48;
  wire reg_b_n_49;
  wire reg_b_n_5;
  wire reg_b_n_50;
  wire reg_b_n_51;
  wire reg_b_n_52;
  wire reg_b_n_53;
  wire reg_b_n_54;
  wire reg_b_n_55;
  wire reg_b_n_56;
  wire reg_b_n_57;
  wire reg_b_n_58;
  wire reg_b_n_59;
  wire reg_b_n_6;
  wire reg_b_n_60;
  wire reg_b_n_61;
  wire reg_b_n_62;
  wire reg_b_n_63;
  wire reg_b_n_64;
  wire reg_b_n_65;
  wire reg_b_n_66;
  wire reg_b_n_67;
  wire reg_b_n_68;
  wire reg_b_n_69;
  wire reg_b_n_7;
  wire reg_b_n_70;
  wire reg_b_n_71;
  wire reg_b_n_72;
  wire reg_b_n_73;
  wire reg_b_n_74;
  wire reg_b_n_75;
  wire reg_b_n_76;
  wire reg_b_n_77;
  wire reg_b_n_78;
  wire reg_b_n_79;
  wire reg_b_n_80;
  wire reg_b_n_81;
  wire reg_b_n_82;
  wire reg_b_n_83;
  wire reg_b_n_84;
  wire reg_b_n_85;
  wire reg_b_n_86;
  wire reg_b_n_87;
  wire reg_b_n_88;
  wire reg_b_n_89;
  wire reg_b_n_90;
  wire reg_b_n_91;
  wire reg_b_n_92;
  wire reg_b_n_93;
  wire reg_b_n_94;
  wire reg_b_n_95;
  wire reg_b_n_96;
  wire reg_b_n_97;
  wire reg_b_n_98;
  wire reg_b_n_99;
  wire reg_flags_n_0;
  wire reg_shamt_n_0;
  wire reg_shamt_n_1;
  wire reg_shamt_n_10;
  wire reg_shamt_n_11;
  wire reg_shamt_n_16;
  wire reg_shamt_n_17;
  wire reg_shamt_n_18;
  wire reg_shamt_n_19;
  wire reg_shamt_n_2;
  wire reg_shamt_n_20;
  wire reg_shamt_n_21;
  wire reg_shamt_n_22;
  wire reg_shamt_n_23;
  wire reg_shamt_n_24;
  wire reg_shamt_n_25;
  wire reg_shamt_n_26;
  wire reg_shamt_n_27;
  wire reg_shamt_n_28;
  wire reg_shamt_n_29;
  wire reg_shamt_n_3;
  wire reg_shamt_n_30;
  wire reg_shamt_n_31;
  wire reg_shamt_n_32;
  wire reg_shamt_n_33;
  wire reg_shamt_n_34;
  wire reg_shamt_n_35;
  wire reg_shamt_n_36;
  wire reg_shamt_n_37;
  wire reg_shamt_n_38;
  wire reg_shamt_n_39;
  wire reg_shamt_n_4;
  wire reg_shamt_n_40;
  wire reg_shamt_n_41;
  wire reg_shamt_n_42;
  wire reg_shamt_n_43;
  wire reg_shamt_n_44;
  wire reg_shamt_n_45;
  wire reg_shamt_n_5;
  wire reg_shamt_n_6;
  wire reg_shamt_n_7;
  wire reg_shamt_n_8;
  wire reg_shamt_n_9;

initial begin
 $sdf_annotate("ALU_TB_time_impl.sdf",,,,"tool_control");
end
  IBUF \ALUControl_IBUF[0]_inst 
       (.I(ALUControl[0]),
        .O(ALUControl_IBUF[0]));
  IBUF \ALUControl_IBUF[1]_inst 
       (.I(ALUControl[1]),
        .O(ALUControl_IBUF[1]));
  IBUF \ALUControl_IBUF[2]_inst 
       (.I(ALUControl[2]),
        .O(ALUControl_IBUF[2]));
  IBUF \ALUControl_IBUF[3]_inst 
       (.I(ALUControl[3]),
        .O(ALUControl_IBUF[3]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[0]_inst 
       (.I(ALUResult_OBUF[0]),
        .O(ALUResult[0]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[10]_inst 
       (.I(ALUResult_OBUF[10]),
        .O(ALUResult[10]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[11]_inst 
       (.I(ALUResult_OBUF[11]),
        .O(ALUResult[11]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[12]_inst 
       (.I(ALUResult_OBUF[12]),
        .O(ALUResult[12]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[13]_inst 
       (.I(ALUResult_OBUF[13]),
        .O(ALUResult[13]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[14]_inst 
       (.I(ALUResult_OBUF[14]),
        .O(ALUResult[14]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[15]_inst 
       (.I(ALUResult_OBUF[15]),
        .O(ALUResult[15]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[16]_inst 
       (.I(ALUResult_OBUF[16]),
        .O(ALUResult[16]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[17]_inst 
       (.I(ALUResult_OBUF[17]),
        .O(ALUResult[17]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[18]_inst 
       (.I(ALUResult_OBUF[18]),
        .O(ALUResult[18]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[19]_inst 
       (.I(ALUResult_OBUF[19]),
        .O(ALUResult[19]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[1]_inst 
       (.I(ALUResult_OBUF[1]),
        .O(ALUResult[1]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[20]_inst 
       (.I(ALUResult_OBUF[20]),
        .O(ALUResult[20]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[21]_inst 
       (.I(ALUResult_OBUF[21]),
        .O(ALUResult[21]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[22]_inst 
       (.I(ALUResult_OBUF[22]),
        .O(ALUResult[22]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[23]_inst 
       (.I(ALUResult_OBUF[23]),
        .O(ALUResult[23]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[24]_inst 
       (.I(ALUResult_OBUF[24]),
        .O(ALUResult[24]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[25]_inst 
       (.I(ALUResult_OBUF[25]),
        .O(ALUResult[25]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[26]_inst 
       (.I(ALUResult_OBUF[26]),
        .O(ALUResult[26]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[27]_inst 
       (.I(ALUResult_OBUF[27]),
        .O(ALUResult[27]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[28]_inst 
       (.I(ALUResult_OBUF[28]),
        .O(ALUResult[28]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[29]_inst 
       (.I(ALUResult_OBUF[29]),
        .O(ALUResult[29]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[2]_inst 
       (.I(ALUResult_OBUF[2]),
        .O(ALUResult[2]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[30]_inst 
       (.I(ALUResult_OBUF[30]),
        .O(ALUResult[30]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[31]_inst 
       (.I(ALUResult_OBUF[31]),
        .O(ALUResult[31]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[3]_inst 
       (.I(ALUResult_OBUF[3]),
        .O(ALUResult[3]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[4]_inst 
       (.I(ALUResult_OBUF[4]),
        .O(ALUResult[4]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[5]_inst 
       (.I(ALUResult_OBUF[5]),
        .O(ALUResult[5]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[6]_inst 
       (.I(ALUResult_OBUF[6]),
        .O(ALUResult[6]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[7]_inst 
       (.I(ALUResult_OBUF[7]),
        .O(ALUResult[7]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[8]_inst 
       (.I(ALUResult_OBUF[8]),
        .O(ALUResult[8]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_OBUF[9]_inst 
       (.I(ALUResult_OBUF[9]),
        .O(ALUResult[9]));
  BUFG CLK_IBUF_BUFG_inst
       (.I(CLK_IBUF),
        .O(CLK_IBUF_BUFG));
  IBUF CLK_IBUF_inst
       (.I(CLK),
        .O(CLK_IBUF));
  (* IOB = "TRUE" *) 
  OBUF \Flags_OBUF[0]_inst 
       (.I(reg_flags_n_0),
        .O(Flags[0]));
  (* IOB = "TRUE" *) 
  OBUF \Flags_OBUF[1]_inst 
       (.I(Flags_OBUF[1]),
        .O(Flags[1]));
  (* IOB = "TRUE" *) 
  OBUF \Flags_OBUF[2]_inst 
       (.I(Flags_OBUF[2]),
        .O(Flags[2]));
  (* IOB = "TRUE" *) 
  OBUF \Flags_OBUF[3]_inst 
       (.I(Flags_OBUF[3]),
        .O(Flags[3]));
  IBUF RESET_IBUF_inst
       (.I(RESET),
        .O(RESET_IBUF));
  IBUF \SHAMT5_IBUF[0]_inst 
       (.I(SHAMT5[0]),
        .O(SHAMT5_IBUF[0]));
  IBUF \SHAMT5_IBUF[1]_inst 
       (.I(SHAMT5[1]),
        .O(SHAMT5_IBUF[1]));
  IBUF \SHAMT5_IBUF[2]_inst 
       (.I(SHAMT5[2]),
        .O(SHAMT5_IBUF[2]));
  IBUF \SHAMT5_IBUF[3]_inst 
       (.I(SHAMT5[3]),
        .O(SHAMT5_IBUF[3]));
  IBUF \SHAMT5_IBUF[4]_inst 
       (.I(SHAMT5[4]),
        .O(SHAMT5_IBUF[4]));
  IBUF \SRC_A_IBUF[0]_inst 
       (.I(SRC_A[0]),
        .O(SRC_A_IBUF[0]));
  IBUF \SRC_A_IBUF[10]_inst 
       (.I(SRC_A[10]),
        .O(SRC_A_IBUF[10]));
  IBUF \SRC_A_IBUF[11]_inst 
       (.I(SRC_A[11]),
        .O(SRC_A_IBUF[11]));
  IBUF \SRC_A_IBUF[12]_inst 
       (.I(SRC_A[12]),
        .O(SRC_A_IBUF[12]));
  IBUF \SRC_A_IBUF[13]_inst 
       (.I(SRC_A[13]),
        .O(SRC_A_IBUF[13]));
  IBUF \SRC_A_IBUF[14]_inst 
       (.I(SRC_A[14]),
        .O(SRC_A_IBUF[14]));
  IBUF \SRC_A_IBUF[15]_inst 
       (.I(SRC_A[15]),
        .O(SRC_A_IBUF[15]));
  IBUF \SRC_A_IBUF[16]_inst 
       (.I(SRC_A[16]),
        .O(SRC_A_IBUF[16]));
  IBUF \SRC_A_IBUF[17]_inst 
       (.I(SRC_A[17]),
        .O(SRC_A_IBUF[17]));
  IBUF \SRC_A_IBUF[18]_inst 
       (.I(SRC_A[18]),
        .O(SRC_A_IBUF[18]));
  IBUF \SRC_A_IBUF[19]_inst 
       (.I(SRC_A[19]),
        .O(SRC_A_IBUF[19]));
  IBUF \SRC_A_IBUF[1]_inst 
       (.I(SRC_A[1]),
        .O(SRC_A_IBUF[1]));
  IBUF \SRC_A_IBUF[20]_inst 
       (.I(SRC_A[20]),
        .O(SRC_A_IBUF[20]));
  IBUF \SRC_A_IBUF[21]_inst 
       (.I(SRC_A[21]),
        .O(SRC_A_IBUF[21]));
  IBUF \SRC_A_IBUF[22]_inst 
       (.I(SRC_A[22]),
        .O(SRC_A_IBUF[22]));
  IBUF \SRC_A_IBUF[23]_inst 
       (.I(SRC_A[23]),
        .O(SRC_A_IBUF[23]));
  IBUF \SRC_A_IBUF[24]_inst 
       (.I(SRC_A[24]),
        .O(SRC_A_IBUF[24]));
  IBUF \SRC_A_IBUF[25]_inst 
       (.I(SRC_A[25]),
        .O(SRC_A_IBUF[25]));
  IBUF \SRC_A_IBUF[26]_inst 
       (.I(SRC_A[26]),
        .O(SRC_A_IBUF[26]));
  IBUF \SRC_A_IBUF[27]_inst 
       (.I(SRC_A[27]),
        .O(SRC_A_IBUF[27]));
  IBUF \SRC_A_IBUF[28]_inst 
       (.I(SRC_A[28]),
        .O(SRC_A_IBUF[28]));
  IBUF \SRC_A_IBUF[29]_inst 
       (.I(SRC_A[29]),
        .O(SRC_A_IBUF[29]));
  IBUF \SRC_A_IBUF[2]_inst 
       (.I(SRC_A[2]),
        .O(SRC_A_IBUF[2]));
  IBUF \SRC_A_IBUF[30]_inst 
       (.I(SRC_A[30]),
        .O(SRC_A_IBUF[30]));
  IBUF \SRC_A_IBUF[31]_inst 
       (.I(SRC_A[31]),
        .O(SRC_A_IBUF[31]));
  IBUF \SRC_A_IBUF[3]_inst 
       (.I(SRC_A[3]),
        .O(SRC_A_IBUF[3]));
  IBUF \SRC_A_IBUF[4]_inst 
       (.I(SRC_A[4]),
        .O(SRC_A_IBUF[4]));
  IBUF \SRC_A_IBUF[5]_inst 
       (.I(SRC_A[5]),
        .O(SRC_A_IBUF[5]));
  IBUF \SRC_A_IBUF[6]_inst 
       (.I(SRC_A[6]),
        .O(SRC_A_IBUF[6]));
  IBUF \SRC_A_IBUF[7]_inst 
       (.I(SRC_A[7]),
        .O(SRC_A_IBUF[7]));
  IBUF \SRC_A_IBUF[8]_inst 
       (.I(SRC_A[8]),
        .O(SRC_A_IBUF[8]));
  IBUF \SRC_A_IBUF[9]_inst 
       (.I(SRC_A[9]),
        .O(SRC_A_IBUF[9]));
  IBUF \SRC_B_IBUF[0]_inst 
       (.I(SRC_B[0]),
        .O(SRC_B_IBUF[0]));
  IBUF \SRC_B_IBUF[10]_inst 
       (.I(SRC_B[10]),
        .O(SRC_B_IBUF[10]));
  IBUF \SRC_B_IBUF[11]_inst 
       (.I(SRC_B[11]),
        .O(SRC_B_IBUF[11]));
  IBUF \SRC_B_IBUF[12]_inst 
       (.I(SRC_B[12]),
        .O(SRC_B_IBUF[12]));
  IBUF \SRC_B_IBUF[13]_inst 
       (.I(SRC_B[13]),
        .O(SRC_B_IBUF[13]));
  IBUF \SRC_B_IBUF[14]_inst 
       (.I(SRC_B[14]),
        .O(SRC_B_IBUF[14]));
  IBUF \SRC_B_IBUF[15]_inst 
       (.I(SRC_B[15]),
        .O(SRC_B_IBUF[15]));
  IBUF \SRC_B_IBUF[16]_inst 
       (.I(SRC_B[16]),
        .O(SRC_B_IBUF[16]));
  IBUF \SRC_B_IBUF[17]_inst 
       (.I(SRC_B[17]),
        .O(SRC_B_IBUF[17]));
  IBUF \SRC_B_IBUF[18]_inst 
       (.I(SRC_B[18]),
        .O(SRC_B_IBUF[18]));
  IBUF \SRC_B_IBUF[19]_inst 
       (.I(SRC_B[19]),
        .O(SRC_B_IBUF[19]));
  IBUF \SRC_B_IBUF[1]_inst 
       (.I(SRC_B[1]),
        .O(SRC_B_IBUF[1]));
  IBUF \SRC_B_IBUF[20]_inst 
       (.I(SRC_B[20]),
        .O(SRC_B_IBUF[20]));
  IBUF \SRC_B_IBUF[21]_inst 
       (.I(SRC_B[21]),
        .O(SRC_B_IBUF[21]));
  IBUF \SRC_B_IBUF[22]_inst 
       (.I(SRC_B[22]),
        .O(SRC_B_IBUF[22]));
  IBUF \SRC_B_IBUF[23]_inst 
       (.I(SRC_B[23]),
        .O(SRC_B_IBUF[23]));
  IBUF \SRC_B_IBUF[24]_inst 
       (.I(SRC_B[24]),
        .O(SRC_B_IBUF[24]));
  IBUF \SRC_B_IBUF[25]_inst 
       (.I(SRC_B[25]),
        .O(SRC_B_IBUF[25]));
  IBUF \SRC_B_IBUF[26]_inst 
       (.I(SRC_B[26]),
        .O(SRC_B_IBUF[26]));
  IBUF \SRC_B_IBUF[27]_inst 
       (.I(SRC_B[27]),
        .O(SRC_B_IBUF[27]));
  IBUF \SRC_B_IBUF[28]_inst 
       (.I(SRC_B[28]),
        .O(SRC_B_IBUF[28]));
  IBUF \SRC_B_IBUF[29]_inst 
       (.I(SRC_B[29]),
        .O(SRC_B_IBUF[29]));
  IBUF \SRC_B_IBUF[2]_inst 
       (.I(SRC_B[2]),
        .O(SRC_B_IBUF[2]));
  IBUF \SRC_B_IBUF[30]_inst 
       (.I(SRC_B[30]),
        .O(SRC_B_IBUF[30]));
  IBUF \SRC_B_IBUF[31]_inst 
       (.I(SRC_B[31]),
        .O(SRC_B_IBUF[31]));
  IBUF \SRC_B_IBUF[3]_inst 
       (.I(SRC_B[3]),
        .O(SRC_B_IBUF[3]));
  IBUF \SRC_B_IBUF[4]_inst 
       (.I(SRC_B[4]),
        .O(SRC_B_IBUF[4]));
  IBUF \SRC_B_IBUF[5]_inst 
       (.I(SRC_B[5]),
        .O(SRC_B_IBUF[5]));
  IBUF \SRC_B_IBUF[6]_inst 
       (.I(SRC_B[6]),
        .O(SRC_B_IBUF[6]));
  IBUF \SRC_B_IBUF[7]_inst 
       (.I(SRC_B[7]),
        .O(SRC_B_IBUF[7]));
  IBUF \SRC_B_IBUF[8]_inst 
       (.I(SRC_B[8]),
        .O(SRC_B_IBUF[8]));
  IBUF \SRC_B_IBUF[9]_inst 
       (.I(SRC_B[9]),
        .O(SRC_B_IBUF[9]));
  IBUF WE_IBUF_inst
       (.I(WE),
        .O(WE_IBUF));
  ALU_n alu_in
       (.CO(alu_in_n_6),
        .DI(reg_a_n_1),
        .\Dout[0]_i_2__0 ({reg_a_n_34,reg_a_n_35,reg_a_n_36,reg_a_n_37}),
        .\Dout[12]_i_4 ({reg_b_n_129,reg_b_n_130,reg_b_n_131,reg_b_n_132}),
        .\Dout[12]_i_4_0 ({reg_a_n_46,reg_a_n_47,reg_a_n_48,reg_a_n_49}),
        .\Dout[16]_i_4 ({reg_b_n_133,reg_b_n_134,reg_b_n_135,reg_b_n_136}),
        .\Dout[16]_i_4_0 ({reg_a_n_50,reg_a_n_51,reg_a_n_52,reg_a_n_53}),
        .\Dout[20]_i_4 ({reg_b_n_137,reg_b_n_138,reg_b_n_139,reg_b_n_140}),
        .\Dout[20]_i_4_0 ({reg_a_n_54,reg_a_n_55,reg_a_n_56,reg_a_n_57}),
        .\Dout[24]_i_4 ({reg_b_n_141,reg_b_n_142,reg_b_n_143,reg_b_n_144}),
        .\Dout[24]_i_4_0 ({reg_a_n_58,reg_a_n_59,reg_a_n_60,reg_a_n_61}),
        .\Dout[4]_i_4 ({reg_b_n_121,reg_b_n_122,reg_b_n_123,reg_b_n_124}),
        .\Dout[4]_i_4_0 ({reg_a_n_38,reg_a_n_39,reg_a_n_40,reg_a_n_41}),
        .\Dout[8]_i_4 ({reg_b_n_125,reg_b_n_126,reg_b_n_127,reg_b_n_128}),
        .\Dout[8]_i_4_0 ({reg_a_n_42,reg_a_n_43,reg_a_n_44,reg_a_n_45}),
        .\Dout_reg[0] (ALUControl_in),
        .\Dout_reg[1] (alu_in_n_14),
        .\Dout_reg[1]_0 (alu_in_n_15),
        .\Dout_reg[1]_1 (alu_in_n_16),
        .\Dout_reg[1]_10 (alu_in_n_25),
        .\Dout_reg[1]_11 (alu_in_n_26),
        .\Dout_reg[1]_12 (alu_in_n_27),
        .\Dout_reg[1]_13 (alu_in_n_28),
        .\Dout_reg[1]_14 (alu_in_n_29),
        .\Dout_reg[1]_15 (alu_in_n_30),
        .\Dout_reg[1]_16 (alu_in_n_31),
        .\Dout_reg[1]_17 (alu_in_n_32),
        .\Dout_reg[1]_18 (alu_in_n_33),
        .\Dout_reg[1]_19 (alu_in_n_34),
        .\Dout_reg[1]_2 (alu_in_n_17),
        .\Dout_reg[1]_20 (alu_in_n_35),
        .\Dout_reg[1]_21 (alu_in_n_36),
        .\Dout_reg[1]_22 (alu_in_n_37),
        .\Dout_reg[1]_23 (alu_in_n_38),
        .\Dout_reg[1]_24 (alu_in_n_39),
        .\Dout_reg[1]_25 (alu_in_n_40),
        .\Dout_reg[1]_3 (alu_in_n_18),
        .\Dout_reg[1]_4 (alu_in_n_19),
        .\Dout_reg[1]_5 (alu_in_n_20),
        .\Dout_reg[1]_6 (alu_in_n_21),
        .\Dout_reg[1]_7 (alu_in_n_22),
        .\Dout_reg[1]_8 (alu_in_n_23),
        .\Dout_reg[1]_9 (alu_in_n_24),
        .\Dout_reg[30] ({\ADD_SUB/S_Add_in [31],\ADD_SUB/S_Add_in [5:1]}),
        .\Dout_reg[30]_0 (alu_in_n_13),
        .\Dout_reg[31] ({\ADD_SUB/S_Sub_in [31],\ADD_SUB/S_Sub_in [5:1]}),
        .\Dout_reg[31]_2[25]_repN_alias (\Dout_reg[31]_2[25]_repN_alias ),
        .\Dout_reg[3] ({SRC_B_in[31:6],SRC_B_in[0]}),
        .\Dout_reg[3]_0 ({reg_b_n_145,reg_b_n_146,reg_b_n_147,reg_b_n_148}),
        .\Dout_reg[3]_1 ({reg_a_n_62,reg_a_n_63,reg_a_n_64,reg_a_n_65}),
        .Q(SRC_A_in),
        .S({reg_b_n_117,reg_b_n_118,reg_b_n_119,reg_b_n_120}));
  REGrwe_n__parameterized1 reg_a
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .CO(reg_a_n_0),
        .D(SRC_A_IBUF),
        .DI(reg_a_n_1),
        .\Dout_reg[11]_0 ({reg_a_n_42,reg_a_n_43,reg_a_n_44,reg_a_n_45}),
        .\Dout_reg[15]_0 ({reg_a_n_46,reg_a_n_47,reg_a_n_48,reg_a_n_49}),
        .\Dout_reg[19]_0 ({reg_a_n_50,reg_a_n_51,reg_a_n_52,reg_a_n_53}),
        .\Dout_reg[23]_0 ({reg_a_n_54,reg_a_n_55,reg_a_n_56,reg_a_n_57}),
        .\Dout_reg[27]_0 ({reg_a_n_58,reg_a_n_59,reg_a_n_60,reg_a_n_61}),
        .\Dout_reg[31]_0 ({reg_a_n_62,reg_a_n_63,reg_a_n_64,reg_a_n_65}),
        .\Dout_reg[31]_2[17]_repN_alias (\Dout_reg[31]_2[17]_repN_alias ),
        .\Dout_reg[31]_2[19]_repN_1_alias (\Dout_reg[31]_2[19]_repN_1_alias ),
        .\Dout_reg[31]_2[25]_repN_alias (\Dout_reg[31]_2[25]_repN_alias ),
        .\Dout_reg[31]_2[27]_repN_1_alias (\Dout_reg[31]_2[27]_repN_1_alias ),
        .\Dout_reg[31]_2[28]_repN_3_alias (\Dout_reg[31]_2[28]_repN_3_alias ),
        .\Dout_reg[31]_2[29]_repN_alias (\Dout_reg[31]_2[29]_repN_alias ),
        .\Dout_reg[31]_2[2]_repN_alias (\Dout_reg[31]_2[2]_repN_alias ),
        .\Dout_reg[31]_2[30]_repN_4_alias (\Dout_reg[31]_2[30]_repN_4_alias ),
        .\Dout_reg[31]_2[30]_repN_alias (\Dout_reg[31]_2[30]_repN_alias ),
        .\Dout_reg[3]_0 ({reg_a_n_34,reg_a_n_35,reg_a_n_36,reg_a_n_37}),
        .\Dout_reg[3]_1 (alu_in_n_13),
        .\Dout_reg[7]_0 ({reg_a_n_38,reg_a_n_39,reg_a_n_40,reg_a_n_41}),
        .E(WE_IBUF),
        .Q(SRC_A_in),
        .S0_carry__6(SRC_B_in),
        .SR(RESET_IBUF));
  REGrwe_n reg_alu_control
       (.ALUResult_in(ALUResult_in),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .CO(reg_a_n_0),
        .D({Flags_in,Z}),
        .\Dout[13]_i_3_n_0_alias (\Dout[13]_i_3_n_0_alias ),
        .\Dout[13]_i_8_n_0_alias (\Dout[13]_i_8_n_0_alias ),
        .\Dout[14]_i_3_n_0_alias (\Dout[14]_i_3_n_0_alias ),
        .\Dout[14]_i_8_n_0_alias (\Dout[14]_i_8_n_0_alias ),
        .\Dout[1]_i_10_0 (reg_b_n_78),
        .\Dout[1]_i_5_0 (reg_shamt_n_36),
        .\Dout[1]_i_8 (reg_b_n_80),
        .\Dout[2]_i_3_n_0_alias (\Dout[2]_i_3_n_0_alias ),
        .\Dout[3]_i_10_n_0_alias (\Dout[3]_i_10_n_0_alias ),
        .\Dout[7]_i_3_n_0_alias (\Dout[7]_i_3_n_0_alias ),
        .\Dout_reg[0]_0 (alu_in_n_14),
        .\Dout_reg[0]_1 (reg_b_n_108),
        .\Dout_reg[0]_12_repN_alias (\Dout_reg[0]_12_repN_alias ),
        .\Dout_reg[0]_13_repN_alias (\Dout_reg[0]_13_repN_alias ),
        .\Dout_reg[0]_2 (reg_shamt_n_28),
        .\Dout_reg[0]_2_repN_1_alias (\Dout_reg[0]_2_repN_1_alias ),
        .\Dout_reg[0]_2_repN_alias (\Dout_reg[0]_2_repN_alias ),
        .\Dout_reg[0]_3 (alu_in_n_15),
        .\Dout_reg[0]_30_repN_alias (\Dout_reg[0]_30_repN_alias ),
        .\Dout_reg[0]_36_alias (reg_b_n_92),
        .\Dout_reg[0]_4 (reg_shamt_n_38),
        .\Dout_reg[0]_5 (reg_shamt_n_29),
        .\Dout_reg[0]_6 (SRC_A_in),
        .\Dout_reg[0]_7 (SRC_B_in),
        .\Dout_reg[10] (reg_b_n_75),
        .\Dout_reg[10]_0 (alu_in_n_20),
        .\Dout_reg[10]_1 (reg_b_n_47),
        .\Dout_reg[11] (reg_b_n_76),
        .\Dout_reg[11]_0 (alu_in_n_21),
        .\Dout_reg[11]_1 (reg_b_n_48),
        .\Dout_reg[12] (reg_b_n_77),
        .\Dout_reg[12]_0 (alu_in_n_22),
        .\Dout_reg[12]_1 (reg_b_n_49),
        .\Dout_reg[13] (reg_b_n_88),
        .\Dout_reg[13]_0 (alu_in_n_23),
        .\Dout_reg[13]_1 (reg_b_n_50),
        .\Dout_reg[14] (reg_b_n_91),
        .\Dout_reg[14]_0 (alu_in_n_24),
        .\Dout_reg[14]_1 (reg_b_n_51),
        .\Dout_reg[15] (reg_b_n_94),
        .\Dout_reg[15]_0 (alu_in_n_25),
        .\Dout_reg[15]_1 (reg_b_n_52),
        .\Dout_reg[16] (reg_b_n_95),
        .\Dout_reg[16]_0 (alu_in_n_26),
        .\Dout_reg[16]_1 (reg_b_n_53),
        .\Dout_reg[17] (reg_b_n_96),
        .\Dout_reg[17]_0 (alu_in_n_27),
        .\Dout_reg[17]_1 (reg_b_n_54),
        .\Dout_reg[18] (reg_b_n_55),
        .\Dout_reg[18]_0 (reg_b_n_97),
        .\Dout_reg[18]_1 (alu_in_n_28),
        .\Dout_reg[19] (reg_b_n_98),
        .\Dout_reg[19]_0 (alu_in_n_29),
        .\Dout_reg[19]_1 (reg_b_n_56),
        .\Dout_reg[1]_0 (reg_alu_control_n_41),
        .\Dout_reg[1]_1 (reg_b_n_42),
        .\Dout_reg[1]_2 (reg_b_n_81),
        .\Dout_reg[1]_3 (reg_shamt_n_30),
        .\Dout_reg[1]_4 (reg_b_n_82),
        .\Dout_reg[1]_5 (reg_b_n_90),
        .\Dout_reg[1]_6 (reg_shamt_n_39),
        .\Dout_reg[20] (reg_b_n_99),
        .\Dout_reg[20]_0 (alu_in_n_30),
        .\Dout_reg[20]_1 (reg_b_n_57),
        .\Dout_reg[21] (reg_b_n_100),
        .\Dout_reg[21]_0 (alu_in_n_31),
        .\Dout_reg[21]_1 (reg_b_n_58),
        .\Dout_reg[22] (reg_b_n_101),
        .\Dout_reg[22]_0 (alu_in_n_32),
        .\Dout_reg[22]_1 (reg_b_n_59),
        .\Dout_reg[23] (alu_in_n_33),
        .\Dout_reg[23]_0 (reg_b_n_60),
        .\Dout_reg[23]_1 (reg_shamt_n_43),
        .\Dout_reg[23]_2 (reg_b_n_102),
        .\Dout_reg[23]_3 (reg_b_n_113),
        .\Dout_reg[24] (alu_in_n_34),
        .\Dout_reg[24]_0 (reg_shamt_n_16),
        .\Dout_reg[24]_1 (reg_shamt_n_17),
        .\Dout_reg[24]_2 (reg_b_n_104),
        .\Dout_reg[25] (alu_in_n_35),
        .\Dout_reg[25]_0 (reg_shamt_n_18),
        .\Dout_reg[25]_1 (reg_shamt_n_19),
        .\Dout_reg[25]_2 (reg_b_n_106),
        .\Dout_reg[26] (reg_b_n_105),
        .\Dout_reg[26]_0 (alu_in_n_36),
        .\Dout_reg[26]_1 (reg_b_n_63),
        .\Dout_reg[27] (alu_in_n_37),
        .\Dout_reg[27]_0 (reg_shamt_n_20),
        .\Dout_reg[27]_1 (reg_shamt_n_21),
        .\Dout_reg[27]_2 (reg_b_n_107),
        .\Dout_reg[27]_3 (reg_b_n_110),
        .\Dout_reg[28] (alu_in_n_38),
        .\Dout_reg[28]_0 (reg_shamt_n_22),
        .\Dout_reg[28]_1 (reg_shamt_n_23),
        .\Dout_reg[28]_2 (reg_b_n_151),
        .\Dout_reg[29] (alu_in_n_39),
        .\Dout_reg[29]_0 (reg_shamt_n_24),
        .\Dout_reg[29]_1 (reg_shamt_n_25),
        .\Dout_reg[29]_2 (reg_b_n_150),
        .\Dout_reg[2]_0 (reg_alu_control_n_39),
        .\Dout_reg[2]_1 (reg_b_n_83),
        .\Dout_reg[2]_2 (reg_shamt_n_0),
        .\Dout_reg[2]_3 (reg_b_n_87),
        .\Dout_reg[2]_4 (reg_shamt_n_40),
        .\Dout_reg[2]_5 (\ADD_SUB/C_Add_in ),
        .\Dout_reg[30] (alu_in_n_40),
        .\Dout_reg[30]_0 (reg_shamt_n_26),
        .\Dout_reg[30]_1 (reg_shamt_n_27),
        .\Dout_reg[30]_2 (reg_b_n_109),
        .\Dout_reg[31]_2[2]_repN_alias (\Dout_reg[31]_2[2]_repN_alias ),
        .\Dout_reg[31]_2[30]_repN_3_alias (\Dout_reg[31]_2[30]_repN_3_alias ),
        .\Dout_reg[31]_2[30]_repN_alias (\Dout_reg[31]_2[30]_repN_alias ),
        .\Dout_reg[31]_2[4]_repN_alias (\Dout_reg[31]_2[4]_repN_alias ),
        .\Dout_reg[3]_0 (reg_alu_control_n_37),
        .\Dout_reg[3]_1 (reg_alu_control_n_38),
        .\Dout_reg[3]_10 (ALUControl_IBUF),
        .\Dout_reg[3]_2 (reg_alu_control_n_40),
        .\Dout_reg[3]_3 (reg_b_n_84),
        .\Dout_reg[3]_4 (reg_shamt_n_6),
        .\Dout_reg[3]_5 (reg_b_n_4),
        .\Dout_reg[3]_6 (reg_shamt_n_41),
        .\Dout_reg[3]_7 ({\ADD_SUB/S_Sub_in [31],\ADD_SUB/S_Sub_in [5:1]}),
        .\Dout_reg[3]_8 ({\ADD_SUB/S_Add_in [31],\ADD_SUB/S_Add_in [5:1]}),
        .\Dout_reg[3]_9 (reg_b_n_0),
        .\Dout_reg[4] (reg_b_n_85),
        .\Dout_reg[4]_0 (reg_b_n_3),
        .\Dout_reg[4]_1 (reg_shamt_n_42),
        .\Dout_reg[5] (reg_b_n_86),
        .\Dout_reg[5]_0 (reg_b_n_6),
        .\Dout_reg[5]_1 (reg_b_n_5),
        .\Dout_reg[5]_2 (reg_shamt_n_5),
        .\Dout_reg[5]_3 (reg_b_n_7),
        .\Dout_reg[5]_4 (reg_shamt_n_32),
        .\Dout_reg[6] (reg_b_n_40),
        .\Dout_reg[6]_0 (alu_in_n_16),
        .\Dout_reg[6]_1 (reg_b_n_41),
        .\Dout_reg[6]_2 (reg_b_n_79),
        .\Dout_reg[6]_3 (reg_shamt_n_34),
        .\Dout_reg[7] (reg_shamt_n_9),
        .\Dout_reg[7]_0 (reg_shamt_n_33),
        .\Dout_reg[7]_1 (alu_in_n_17),
        .\Dout_reg[8] (alu_in_n_18),
        .\Dout_reg[8]_0 (reg_b_n_45),
        .\Dout_reg[8]_1 (reg_b_n_46),
        .\Dout_reg[8]_2 (reg_shamt_n_35),
        .\Dout_reg[8]_3 (reg_b_n_74),
        .\Dout_reg[9] (reg_b_n_44),
        .\Dout_reg[9]_0 (alu_in_n_19),
        .\Dout_reg[9]_1 (reg_b_n_43),
        .E(WE_IBUF),
        .O(\ADD_SUB/C_Sub_in ),
        .Q(ALUControl_in),
        .SR(RESET_IBUF));
  REGrwe_n__parameterized1_0 reg_b
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .CO(alu_in_n_6),
        .D(SRC_B_IBUF),
        .\Dout[13]_i_3_n_0_alias (\Dout[13]_i_3_n_0_alias ),
        .\Dout[13]_i_8_n_0_alias (\Dout[13]_i_8_n_0_alias ),
        .\Dout[14]_i_3_n_0_alias (\Dout[14]_i_3_n_0_alias ),
        .\Dout[14]_i_8_n_0_alias (\Dout[14]_i_8_n_0_alias ),
        .\Dout[16]_i_9_0 (reg_shamt_n_8),
        .\Dout[16]_i_9_1 (reg_shamt_n_31),
        .\Dout[1]_i_14 (reg_shamt_n_35),
        .\Dout[1]_i_16 (reg_shamt_n_34),
        .\Dout[1]_i_5 (reg_alu_control_n_40),
        .\Dout[1]_i_5_0 (reg_alu_control_n_37),
        .\Dout[1]_i_5_1 (reg_alu_control_n_39),
        .\Dout[25]_i_6 (reg_shamt_n_37),
        .\Dout[2]_i_3_n_0_alias (\Dout[2]_i_3_n_0_alias ),
        .\Dout[3]_i_10_n_0_alias (\Dout[3]_i_10_n_0_alias ),
        .\Dout_reg[0]_0 (reg_b_n_3),
        .\Dout_reg[0]_1 (reg_b_n_6),
        .\Dout_reg[0]_10 (reg_b_n_48),
        .\Dout_reg[0]_11 (reg_b_n_49),
        .\Dout_reg[0]_12 (reg_b_n_50),
        .\Dout_reg[0]_12_repN_alias (\Dout_reg[0]_12_repN_alias ),
        .\Dout_reg[0]_13 (reg_b_n_51),
        .\Dout_reg[0]_13_repN_alias (\Dout_reg[0]_13_repN_alias ),
        .\Dout_reg[0]_14 (reg_b_n_52),
        .\Dout_reg[0]_15 (reg_b_n_53),
        .\Dout_reg[0]_16 (reg_b_n_54),
        .\Dout_reg[0]_17 (reg_b_n_55),
        .\Dout_reg[0]_18 (reg_b_n_56),
        .\Dout_reg[0]_19 (reg_b_n_57),
        .\Dout_reg[0]_2 (reg_b_n_40),
        .\Dout_reg[0]_20 (reg_b_n_58),
        .\Dout_reg[0]_21 (reg_b_n_59),
        .\Dout_reg[0]_22 (reg_b_n_60),
        .\Dout_reg[0]_23 (reg_b_n_63),
        .\Dout_reg[0]_24 (reg_b_n_75),
        .\Dout_reg[0]_25 (reg_b_n_76),
        .\Dout_reg[0]_26 (reg_b_n_77),
        .\Dout_reg[0]_27 (reg_b_n_78),
        .\Dout_reg[0]_28 (reg_b_n_80),
        .\Dout_reg[0]_29 (reg_b_n_81),
        .\Dout_reg[0]_3 (reg_b_n_41),
        .\Dout_reg[0]_30 (reg_b_n_83),
        .\Dout_reg[0]_30_repN_alias (\Dout_reg[0]_30_repN_alias ),
        .\Dout_reg[0]_31 (reg_b_n_84),
        .\Dout_reg[0]_32 (reg_b_n_85),
        .\Dout_reg[0]_33 (reg_b_n_86),
        .\Dout_reg[0]_34 (reg_b_n_88),
        .\Dout_reg[0]_35 (reg_b_n_91),
        .\Dout_reg[0]_36 (reg_b_n_92),
        .\Dout_reg[0]_37 (reg_b_n_93),
        .\Dout_reg[0]_38 (reg_b_n_94),
        .\Dout_reg[0]_39 (reg_b_n_95),
        .\Dout_reg[0]_4 (reg_b_n_42),
        .\Dout_reg[0]_40 (reg_b_n_96),
        .\Dout_reg[0]_41 (reg_b_n_97),
        .\Dout_reg[0]_42 (reg_b_n_98),
        .\Dout_reg[0]_43 (reg_b_n_99),
        .\Dout_reg[0]_44 (reg_b_n_100),
        .\Dout_reg[0]_45 (reg_b_n_101),
        .\Dout_reg[0]_46 (reg_b_n_105),
        .\Dout_reg[0]_47 (ALUControl_in),
        .\Dout_reg[0]_48 (reg_shamt_n_44),
        .\Dout_reg[0]_5 (reg_b_n_43),
        .\Dout_reg[0]_6 (reg_b_n_44),
        .\Dout_reg[0]_7 (reg_b_n_45),
        .\Dout_reg[0]_8 (reg_b_n_46),
        .\Dout_reg[0]_9 (reg_b_n_47),
        .\Dout_reg[10]_0 (reg_b_n_62),
        .\Dout_reg[11]_0 (reg_b_n_65),
        .\Dout_reg[11]_1 ({reg_b_n_125,reg_b_n_126,reg_b_n_127,reg_b_n_128}),
        .\Dout_reg[12]_0 (reg_b_n_64),
        .\Dout_reg[13]_0 (reg_b_n_67),
        .\Dout_reg[14]_0 (reg_b_n_69),
        .\Dout_reg[15]_0 (reg_b_n_70),
        .\Dout_reg[15]_1 ({reg_b_n_129,reg_b_n_130,reg_b_n_131,reg_b_n_132}),
        .\Dout_reg[19]_0 ({reg_b_n_133,reg_b_n_134,reg_b_n_135,reg_b_n_136}),
        .\Dout_reg[1]_0 (reg_b_n_4),
        .\Dout_reg[1]_1 (reg_b_n_5),
        .\Dout_reg[1]_10 (reg_b_n_107),
        .\Dout_reg[1]_11 (reg_b_n_110),
        .\Dout_reg[1]_12 (reg_b_n_113),
        .\Dout_reg[1]_13 (reg_b_n_114),
        .\Dout_reg[1]_2 (reg_b_n_7),
        .\Dout_reg[1]_3 (reg_b_n_82),
        .\Dout_reg[1]_4 (reg_b_n_87),
        .\Dout_reg[1]_5 (reg_b_n_89),
        .\Dout_reg[1]_6 (reg_b_n_90),
        .\Dout_reg[1]_7 (reg_b_n_102),
        .\Dout_reg[1]_8 (reg_b_n_104),
        .\Dout_reg[1]_9 (reg_b_n_106),
        .\Dout_reg[23]_0 ({reg_b_n_137,reg_b_n_138,reg_b_n_139,reg_b_n_140}),
        .\Dout_reg[23]_1 (reg_shamt_n_43),
        .\Dout_reg[25]_0 (reg_b_n_66),
        .\Dout_reg[26]_0 (reg_shamt_n_45),
        .\Dout_reg[27]_0 (reg_b_n_68),
        .\Dout_reg[27]_1 ({reg_b_n_141,reg_b_n_142,reg_b_n_143,reg_b_n_144}),
        .\Dout_reg[28]_0 (reg_b_n_112),
        .\Dout_reg[29]_0 (reg_b_n_71),
        .\Dout_reg[29]_1 (reg_b_n_103),
        .\Dout_reg[29]_2 (reg_b_n_111),
        .\Dout_reg[29]_3 (reg_b_n_116),
        .\Dout_reg[2]_0 (reg_b_n_74),
        .\Dout_reg[2]_1 (reg_b_n_109),
        .\Dout_reg[2]_2 (reg_b_n_149),
        .\Dout_reg[2]_3 (reg_b_n_150),
        .\Dout_reg[2]_4 (reg_b_n_151),
        .\Dout_reg[2]_5 (reg_a_n_0),
        .\Dout_reg[2]_i_2_0 (SRC_A_in),
        .\Dout_reg[30]_0 (reg_b_n_72),
        .\Dout_reg[30]_1 (reg_b_n_115),
        .\Dout_reg[31]_0 (reg_b_n_0),
        .\Dout_reg[31]_1 (\ADD_SUB/C_Add_in ),
        .\Dout_reg[31]_2 (SRC_B_in),
        .\Dout_reg[31]_2[17]_repN_alias (\Dout_reg[31]_2[17]_repN_alias ),
        .\Dout_reg[31]_2[19]_repN_1_alias (\Dout_reg[31]_2[19]_repN_1_alias ),
        .\Dout_reg[31]_2[25]_repN_3_alias (\Dout_reg[31]_2[25]_repN_3_alias ),
        .\Dout_reg[31]_2[25]_repN_alias (\Dout_reg[31]_2[25]_repN_alias ),
        .\Dout_reg[31]_2[26]_repN_alias (\Dout_reg[31]_2[26]_repN_alias ),
        .\Dout_reg[31]_2[27]_repN_1_alias (\Dout_reg[31]_2[27]_repN_1_alias ),
        .\Dout_reg[31]_2[27]_repN_alias (\Dout_reg[31]_2[27]_repN_alias ),
        .\Dout_reg[31]_2[28]_repN_3_alias (\Dout_reg[31]_2[28]_repN_3_alias ),
        .\Dout_reg[31]_2[28]_repN_alias (\Dout_reg[31]_2[28]_repN_alias ),
        .\Dout_reg[31]_2[29]_repN_1_alias (\Dout_reg[31]_2[29]_repN_1_alias ),
        .\Dout_reg[31]_2[29]_repN_alias (\Dout_reg[31]_2[29]_repN_alias ),
        .\Dout_reg[31]_2[2]_repN_alias (\Dout_reg[31]_2[2]_repN_alias ),
        .\Dout_reg[31]_2[30]_repN_1_alias (\Dout_reg[31]_2[30]_repN_1_alias ),
        .\Dout_reg[31]_2[30]_repN_2_alias (\Dout_reg[31]_2[30]_repN_2_alias ),
        .\Dout_reg[31]_2[30]_repN_3_alias (\Dout_reg[31]_2[30]_repN_3_alias ),
        .\Dout_reg[31]_2[30]_repN_4_alias (\Dout_reg[31]_2[30]_repN_4_alias ),
        .\Dout_reg[31]_2[30]_repN_alias (\Dout_reg[31]_2[30]_repN_alias ),
        .\Dout_reg[31]_2[31]_repN_1_alias (\Dout_reg[31]_2[31]_repN_1_alias ),
        .\Dout_reg[31]_2[31]_repN_alias (\Dout_reg[31]_2[31]_repN_alias ),
        .\Dout_reg[31]_2[4]_repN_alias (\Dout_reg[31]_2[4]_repN_alias ),
        .\Dout_reg[31]_3 (reg_b_n_73),
        .\Dout_reg[31]_4 (reg_b_n_108),
        .\Dout_reg[31]_5 ({reg_b_n_145,reg_b_n_146,reg_b_n_147,reg_b_n_148}),
        .\Dout_reg[3]_0 (reg_b_n_79),
        .\Dout_reg[4]_0 (reg_shamt_n_7),
        .\Dout_reg[6]_0 (reg_shamt_n_11),
        .\Dout_reg[7]_0 ({reg_b_n_121,reg_b_n_122,reg_b_n_123,reg_b_n_124}),
        .\Dout_reg[8]_0 (reg_shamt_n_10),
        .\Dout_reg[9]_0 (reg_b_n_61),
        .\Dout_reg[9]_1 (reg_alu_control_n_38),
        .E(WE_IBUF),
        .O(\ADD_SUB/C_Sub_in ),
        .Q({reg_shamt_n_1,reg_shamt_n_2,reg_shamt_n_3,reg_shamt_n_4,reg_shamt_n_5}),
        .\Q[1]_repN_1_alias (\Q[1]_repN_1_alias ),
        .\Q[1]_repN_2_alias (\Q[1]_repN_2_alias ),
        .\Q[1]_repN_3_alias (\Q[1]_repN_3_alias ),
        .\Q[1]_repN_5_alias (\Q[1]_repN_5_alias ),
        .\Q[1]_repN_6_alias (\Q[1]_repN_6_alias ),
        .\Q[1]_repN_7_alias (\Q[1]_repN_7_alias ),
        .\Q[1]_repN_alias (\Q[1]_repN_alias ),
        .\Q[2]_repN_1_alias (\Q[2]_repN_1_alias ),
        .\Q[2]_repN_2_alias (\Q[2]_repN_2_alias ),
        .\Q[2]_repN_3_alias (\Q[2]_repN_3_alias ),
        .\Q[2]_repN_4_alias (\Q[2]_repN_4_alias ),
        .\Q[2]_repN_5_alias (\Q[2]_repN_5_alias ),
        .\Q[2]_repN_6_alias (\Q[2]_repN_6_alias ),
        .\Q[2]_repN_7_alias (\Q[2]_repN_7_alias ),
        .\Q[2]_repN_alias (\Q[2]_repN_alias ),
        .\Q[3]_repN_1_alias (\Q[3]_repN_1_alias ),
        .\Q[3]_repN_2_alias (\Q[3]_repN_2_alias ),
        .\Q[3]_repN_3_alias (\Q[3]_repN_3_alias ),
        .\Q[3]_repN_6_alias (\Q[3]_repN_6_alias ),
        .\Q[3]_repN_7_alias (\Q[3]_repN_7_alias ),
        .\Q[3]_repN_8_alias (\Q[3]_repN_8_alias ),
        .\Q[3]_repN_alias (\Q[3]_repN_alias ),
        .\Q[4]_repN_1_alias (\Q[4]_repN_1_alias ),
        .\Q[4]_repN_2_alias (\Q[4]_repN_2_alias ),
        .\Q[4]_repN_3_alias (\Q[4]_repN_3_alias ),
        .\Q[4]_repN_5_alias (\Q[4]_repN_5_alias ),
        .\Q[4]_repN_alias (\Q[4]_repN_alias ),
        .ROTATE_RIGHT1(ROTATE_RIGHT1),
        .S({reg_b_n_117,reg_b_n_118,reg_b_n_119,reg_b_n_120}),
        .SR(RESET_IBUF));
  REGrwe_n_1 reg_flags
       (.ALUResult_OBUF(ALUResult_OBUF[31]),
        .ALUResult_in(ALUResult_in[31]),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D({Flags_in,Z}),
        .\Dout_reg[0]_0 (reg_flags_n_0),
        .Q(Flags_OBUF),
        .SR(RESET_IBUF));
  REGrwe_n__parameterized1_2 reg_result
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D(ALUResult_in[30:0]),
        .Q(ALUResult_OBUF[30:0]),
        .SR(RESET_IBUF));
  REGrwe_n__parameterized3 reg_shamt
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D(SHAMT5_IBUF),
        .\Dout[0]_i_4__0 (reg_b_n_149),
        .\Dout[0]_i_5_0 (reg_b_n_68),
        .\Dout[0]_i_5_1 (reg_b_n_73),
        .\Dout[0]_i_8_0 (reg_b_n_66),
        .\Dout[0]_i_8_1 (reg_b_n_71),
        .\Dout[23]_i_2 (reg_b_n_103),
        .\Dout[24]_i_5_0 (reg_b_n_116),
        .\Dout[24]_i_5_1 (reg_b_n_114),
        .\Dout[25]_i_2 (reg_b_n_112),
        .\Dout[25]_i_8_0 (reg_b_n_115),
        .\Dout[27]_i_2 (reg_b_n_111),
        .\Dout[28]_i_8_0 ({SRC_B_in[31:24],SRC_B_in[7:0]}),
        .\Dout[7]_i_3_n_0_alias (\Dout[7]_i_3_n_0_alias ),
        .\Dout_reg[0]_0 (reg_shamt_n_0),
        .\Dout_reg[0]_1 (reg_shamt_n_6),
        .\Dout_reg[0]_10 (reg_shamt_n_22),
        .\Dout_reg[0]_11 (reg_shamt_n_23),
        .\Dout_reg[0]_12 (reg_shamt_n_24),
        .\Dout_reg[0]_13 (reg_shamt_n_26),
        .\Dout_reg[0]_14 (reg_shamt_n_28),
        .\Dout_reg[0]_15 (reg_shamt_n_29),
        .\Dout_reg[0]_16 (reg_shamt_n_30),
        .\Dout_reg[0]_17 (reg_shamt_n_32),
        .\Dout_reg[0]_18 (reg_shamt_n_33),
        .\Dout_reg[0]_19 (reg_shamt_n_34),
        .\Dout_reg[0]_2 (reg_shamt_n_9),
        .\Dout_reg[0]_20 (reg_shamt_n_36),
        .\Dout_reg[0]_21 (reg_shamt_n_38),
        .\Dout_reg[0]_22 (reg_shamt_n_39),
        .\Dout_reg[0]_23 (reg_shamt_n_41),
        .\Dout_reg[0]_24 (reg_shamt_n_42),
        .\Dout_reg[0]_25 (reg_shamt_n_43),
        .\Dout_reg[0]_26 (ALUControl_in),
        .\Dout_reg[0]_27 (reg_b_n_89),
        .\Dout_reg[0]_28 (reg_alu_control_n_41),
        .\Dout_reg[0]_2_repN_1_alias (\Dout_reg[0]_2_repN_1_alias ),
        .\Dout_reg[0]_2_repN_alias (\Dout_reg[0]_2_repN_alias ),
        .\Dout_reg[0]_3 (reg_shamt_n_11),
        .\Dout_reg[0]_4 (reg_shamt_n_16),
        .\Dout_reg[0]_5 (reg_shamt_n_17),
        .\Dout_reg[0]_6 (reg_shamt_n_18),
        .\Dout_reg[0]_7 (reg_shamt_n_19),
        .\Dout_reg[0]_8 (reg_shamt_n_20),
        .\Dout_reg[0]_9 (reg_shamt_n_21),
        .\Dout_reg[1]_0 (reg_shamt_n_7),
        .\Dout_reg[1]_1 (reg_shamt_n_10),
        .\Dout_reg[1]_2 (reg_shamt_n_25),
        .\Dout_reg[1]_3 (reg_shamt_n_35),
        .\Dout_reg[1]_4 (reg_shamt_n_40),
        .\Dout_reg[1]_5 (reg_shamt_n_45),
        .\Dout_reg[1]_6 (reg_b_n_82),
        .\Dout_reg[24] (reg_b_n_62),
        .\Dout_reg[24]_0 (reg_b_n_61),
        .\Dout_reg[25] (reg_b_n_65),
        .\Dout_reg[27] (reg_b_n_67),
        .\Dout_reg[27]_0 (reg_b_n_64),
        .\Dout_reg[28] (reg_b_n_69),
        .\Dout_reg[29] (reg_b_n_70),
        .\Dout_reg[2]_0 (reg_b_n_90),
        .\Dout_reg[2]_1 (reg_b_n_87),
        .\Dout_reg[30] (reg_b_n_72),
        .\Dout_reg[31] (reg_shamt_n_27),
        .\Dout_reg[31]_0 (reg_shamt_n_44),
        .\Dout_reg[31]_2[25]_repN_3_alias (\Dout_reg[31]_2[25]_repN_3_alias ),
        .\Dout_reg[31]_2[26]_repN_alias (\Dout_reg[31]_2[26]_repN_alias ),
        .\Dout_reg[31]_2[27]_repN_alias (\Dout_reg[31]_2[27]_repN_alias ),
        .\Dout_reg[31]_2[28]_repN_3_alias (\Dout_reg[31]_2[28]_repN_3_alias ),
        .\Dout_reg[31]_2[28]_repN_alias (\Dout_reg[31]_2[28]_repN_alias ),
        .\Dout_reg[31]_2[29]_repN_1_alias (\Dout_reg[31]_2[29]_repN_1_alias ),
        .\Dout_reg[31]_2[2]_repN_alias (\Dout_reg[31]_2[2]_repN_alias ),
        .\Dout_reg[31]_2[30]_repN_1_alias (\Dout_reg[31]_2[30]_repN_1_alias ),
        .\Dout_reg[31]_2[30]_repN_2_alias (\Dout_reg[31]_2[30]_repN_2_alias ),
        .\Dout_reg[31]_2[30]_repN_3_alias (\Dout_reg[31]_2[30]_repN_3_alias ),
        .\Dout_reg[31]_2[30]_repN_alias (\Dout_reg[31]_2[30]_repN_alias ),
        .\Dout_reg[31]_2[31]_repN_1_alias (\Dout_reg[31]_2[31]_repN_1_alias ),
        .\Dout_reg[31]_2[31]_repN_alias (\Dout_reg[31]_2[31]_repN_alias ),
        .\Dout_reg[3]_0 (reg_b_n_4),
        .\Dout_reg[3]_0_alias (reg_alu_control_n_37),
        .\Dout_reg[4]_0 (reg_shamt_n_8),
        .\Dout_reg[4]_1 (reg_shamt_n_31),
        .\Dout_reg[4]_2 (reg_shamt_n_37),
        .\Dout_reg[7] (reg_b_n_92),
        .\Dout_reg[7]_0 (reg_b_n_93),
        .\Dout_reg[7]_1 (reg_alu_control_n_38),
        .E(WE_IBUF),
        .Q({reg_shamt_n_1,reg_shamt_n_2,reg_shamt_n_3,reg_shamt_n_4,reg_shamt_n_5}),
        .\Q[1]_repN_1_alias (\Q[1]_repN_1_alias ),
        .\Q[1]_repN_2_alias (\Q[1]_repN_2_alias ),
        .\Q[1]_repN_3_alias (\Q[1]_repN_3_alias ),
        .\Q[1]_repN_5_alias (\Q[1]_repN_5_alias ),
        .\Q[1]_repN_6_alias (\Q[1]_repN_6_alias ),
        .\Q[1]_repN_7_alias (\Q[1]_repN_7_alias ),
        .\Q[1]_repN_alias (\Q[1]_repN_alias ),
        .\Q[2]_repN_1_alias (\Q[2]_repN_1_alias ),
        .\Q[2]_repN_2_alias (\Q[2]_repN_2_alias ),
        .\Q[2]_repN_3_alias (\Q[2]_repN_3_alias ),
        .\Q[2]_repN_4_alias (\Q[2]_repN_4_alias ),
        .\Q[2]_repN_5_alias (\Q[2]_repN_5_alias ),
        .\Q[2]_repN_6_alias (\Q[2]_repN_6_alias ),
        .\Q[2]_repN_7_alias (\Q[2]_repN_7_alias ),
        .\Q[2]_repN_alias (\Q[2]_repN_alias ),
        .\Q[3]_repN_1_alias (\Q[3]_repN_1_alias ),
        .\Q[3]_repN_2_alias (\Q[3]_repN_2_alias ),
        .\Q[3]_repN_3_alias (\Q[3]_repN_3_alias ),
        .\Q[3]_repN_6_alias (\Q[3]_repN_6_alias ),
        .\Q[3]_repN_7_alias (\Q[3]_repN_7_alias ),
        .\Q[3]_repN_8_alias (\Q[3]_repN_8_alias ),
        .\Q[3]_repN_alias (\Q[3]_repN_alias ),
        .\Q[4]_repN_1_alias (\Q[4]_repN_1_alias ),
        .\Q[4]_repN_2_alias (\Q[4]_repN_2_alias ),
        .\Q[4]_repN_3_alias (\Q[4]_repN_3_alias ),
        .\Q[4]_repN_5_alias (\Q[4]_repN_5_alias ),
        .\Q[4]_repN_alias (\Q[4]_repN_alias ),
        .ROTATE_RIGHT1(ROTATE_RIGHT1),
        .SR(RESET_IBUF));
endmodule

module ALU_n
   (\Dout_reg[31] ,
    CO,
    \Dout_reg[30] ,
    \Dout_reg[30]_0 ,
    \Dout_reg[1] ,
    \Dout_reg[1]_0 ,
    \Dout_reg[1]_1 ,
    \Dout_reg[1]_2 ,
    \Dout_reg[1]_3 ,
    \Dout_reg[1]_4 ,
    \Dout_reg[1]_5 ,
    \Dout_reg[1]_6 ,
    \Dout_reg[1]_7 ,
    \Dout_reg[1]_8 ,
    \Dout_reg[1]_9 ,
    \Dout_reg[1]_10 ,
    \Dout_reg[1]_11 ,
    \Dout_reg[1]_12 ,
    \Dout_reg[1]_13 ,
    \Dout_reg[1]_14 ,
    \Dout_reg[1]_15 ,
    \Dout_reg[1]_16 ,
    \Dout_reg[1]_17 ,
    \Dout_reg[1]_18 ,
    \Dout_reg[1]_19 ,
    \Dout_reg[1]_20 ,
    \Dout_reg[1]_21 ,
    \Dout_reg[1]_22 ,
    \Dout_reg[1]_23 ,
    \Dout_reg[1]_24 ,
    \Dout_reg[1]_25 ,
    Q,
    S,
    \Dout[4]_i_4 ,
    \Dout[8]_i_4 ,
    \Dout[12]_i_4 ,
    \Dout[16]_i_4 ,
    \Dout[20]_i_4 ,
    \Dout[24]_i_4 ,
    \Dout_reg[3] ,
    \Dout_reg[3]_0 ,
    \Dout[0]_i_2__0 ,
    \Dout[4]_i_4_0 ,
    \Dout[8]_i_4_0 ,
    \Dout[12]_i_4_0 ,
    \Dout[16]_i_4_0 ,
    \Dout[20]_i_4_0 ,
    \Dout[24]_i_4_0 ,
    DI,
    \Dout_reg[3]_1 ,
    \Dout_reg[0] ,
    \Dout_reg[31]_2[25]_repN_alias );
  output [5:0]\Dout_reg[31] ;
  output [0:0]CO;
  output [5:0]\Dout_reg[30] ;
  output [0:0]\Dout_reg[30]_0 ;
  output \Dout_reg[1] ;
  output \Dout_reg[1]_0 ;
  output \Dout_reg[1]_1 ;
  output \Dout_reg[1]_2 ;
  output \Dout_reg[1]_3 ;
  output \Dout_reg[1]_4 ;
  output \Dout_reg[1]_5 ;
  output \Dout_reg[1]_6 ;
  output \Dout_reg[1]_7 ;
  output \Dout_reg[1]_8 ;
  output \Dout_reg[1]_9 ;
  output \Dout_reg[1]_10 ;
  output \Dout_reg[1]_11 ;
  output \Dout_reg[1]_12 ;
  output \Dout_reg[1]_13 ;
  output \Dout_reg[1]_14 ;
  output \Dout_reg[1]_15 ;
  output \Dout_reg[1]_16 ;
  output \Dout_reg[1]_17 ;
  output \Dout_reg[1]_18 ;
  output \Dout_reg[1]_19 ;
  output \Dout_reg[1]_20 ;
  output \Dout_reg[1]_21 ;
  output \Dout_reg[1]_22 ;
  output \Dout_reg[1]_23 ;
  output \Dout_reg[1]_24 ;
  output \Dout_reg[1]_25 ;
  input [31:0]Q;
  input [3:0]S;
  input [3:0]\Dout[4]_i_4 ;
  input [3:0]\Dout[8]_i_4 ;
  input [3:0]\Dout[12]_i_4 ;
  input [3:0]\Dout[16]_i_4 ;
  input [3:0]\Dout[20]_i_4 ;
  input [3:0]\Dout[24]_i_4 ;
  input [26:0]\Dout_reg[3] ;
  input [3:0]\Dout_reg[3]_0 ;
  input [3:0]\Dout[0]_i_2__0 ;
  input [3:0]\Dout[4]_i_4_0 ;
  input [3:0]\Dout[8]_i_4_0 ;
  input [3:0]\Dout[12]_i_4_0 ;
  input [3:0]\Dout[16]_i_4_0 ;
  input [3:0]\Dout[20]_i_4_0 ;
  input [3:0]\Dout[24]_i_4_0 ;
  input [0:0]DI;
  input [3:0]\Dout_reg[3]_1 ;
  input [1:0]\Dout_reg[0] ;
  input \Dout_reg[31]_2[25]_repN_alias ;

  wire [0:0]CO;
  wire [0:0]DI;
  wire [3:0]\Dout[0]_i_2__0 ;
  wire [3:0]\Dout[12]_i_4 ;
  wire [3:0]\Dout[12]_i_4_0 ;
  wire [3:0]\Dout[16]_i_4 ;
  wire [3:0]\Dout[16]_i_4_0 ;
  wire [3:0]\Dout[20]_i_4 ;
  wire [3:0]\Dout[20]_i_4_0 ;
  wire [3:0]\Dout[24]_i_4 ;
  wire [3:0]\Dout[24]_i_4_0 ;
  wire [3:0]\Dout[4]_i_4 ;
  wire [3:0]\Dout[4]_i_4_0 ;
  wire [3:0]\Dout[8]_i_4 ;
  wire [3:0]\Dout[8]_i_4_0 ;
  wire [1:0]\Dout_reg[0] ;
  wire \Dout_reg[1] ;
  wire \Dout_reg[1]_0 ;
  wire \Dout_reg[1]_1 ;
  wire \Dout_reg[1]_10 ;
  wire \Dout_reg[1]_11 ;
  wire \Dout_reg[1]_12 ;
  wire \Dout_reg[1]_13 ;
  wire \Dout_reg[1]_14 ;
  wire \Dout_reg[1]_15 ;
  wire \Dout_reg[1]_16 ;
  wire \Dout_reg[1]_17 ;
  wire \Dout_reg[1]_18 ;
  wire \Dout_reg[1]_19 ;
  wire \Dout_reg[1]_2 ;
  wire \Dout_reg[1]_20 ;
  wire \Dout_reg[1]_21 ;
  wire \Dout_reg[1]_22 ;
  wire \Dout_reg[1]_23 ;
  wire \Dout_reg[1]_24 ;
  wire \Dout_reg[1]_25 ;
  wire \Dout_reg[1]_3 ;
  wire \Dout_reg[1]_4 ;
  wire \Dout_reg[1]_5 ;
  wire \Dout_reg[1]_6 ;
  wire \Dout_reg[1]_7 ;
  wire \Dout_reg[1]_8 ;
  wire \Dout_reg[1]_9 ;
  wire [5:0]\Dout_reg[30] ;
  wire [0:0]\Dout_reg[30]_0 ;
  wire [5:0]\Dout_reg[31] ;
  wire \Dout_reg[31]_2[25]_repN_alias ;
  wire [26:0]\Dout_reg[3] ;
  wire [3:0]\Dout_reg[3]_0 ;
  wire [3:0]\Dout_reg[3]_1 ;
  wire [31:0]Q;
  wire [3:0]S;

  ADD_SUB_n ADD_SUB
       (.CO(CO),
        .DI(DI),
        .\Dout[0]_i_2__0 (\Dout[0]_i_2__0 ),
        .\Dout[12]_i_4 (\Dout[12]_i_4 ),
        .\Dout[12]_i_4_0 (\Dout[12]_i_4_0 ),
        .\Dout[16]_i_4 (\Dout[16]_i_4 ),
        .\Dout[16]_i_4_0 (\Dout[16]_i_4_0 ),
        .\Dout[20]_i_4 (\Dout[20]_i_4 ),
        .\Dout[20]_i_4_0 (\Dout[20]_i_4_0 ),
        .\Dout[24]_i_4 (\Dout[24]_i_4 ),
        .\Dout[24]_i_4_0 (\Dout[24]_i_4_0 ),
        .\Dout[4]_i_4 (\Dout[4]_i_4 ),
        .\Dout[4]_i_4_0 (\Dout[4]_i_4_0 ),
        .\Dout[8]_i_4 (\Dout[8]_i_4 ),
        .\Dout[8]_i_4_0 (\Dout[8]_i_4_0 ),
        .\Dout_reg[0] (\Dout_reg[0] ),
        .\Dout_reg[1] (\Dout_reg[1] ),
        .\Dout_reg[1]_0 (\Dout_reg[1]_0 ),
        .\Dout_reg[1]_1 (\Dout_reg[1]_1 ),
        .\Dout_reg[1]_10 (\Dout_reg[1]_10 ),
        .\Dout_reg[1]_11 (\Dout_reg[1]_11 ),
        .\Dout_reg[1]_12 (\Dout_reg[1]_12 ),
        .\Dout_reg[1]_13 (\Dout_reg[1]_13 ),
        .\Dout_reg[1]_14 (\Dout_reg[1]_14 ),
        .\Dout_reg[1]_15 (\Dout_reg[1]_15 ),
        .\Dout_reg[1]_16 (\Dout_reg[1]_16 ),
        .\Dout_reg[1]_17 (\Dout_reg[1]_17 ),
        .\Dout_reg[1]_18 (\Dout_reg[1]_18 ),
        .\Dout_reg[1]_19 (\Dout_reg[1]_19 ),
        .\Dout_reg[1]_2 (\Dout_reg[1]_2 ),
        .\Dout_reg[1]_20 (\Dout_reg[1]_20 ),
        .\Dout_reg[1]_21 (\Dout_reg[1]_21 ),
        .\Dout_reg[1]_22 (\Dout_reg[1]_22 ),
        .\Dout_reg[1]_23 (\Dout_reg[1]_23 ),
        .\Dout_reg[1]_24 (\Dout_reg[1]_24 ),
        .\Dout_reg[1]_25 (\Dout_reg[1]_25 ),
        .\Dout_reg[1]_3 (\Dout_reg[1]_3 ),
        .\Dout_reg[1]_4 (\Dout_reg[1]_4 ),
        .\Dout_reg[1]_5 (\Dout_reg[1]_5 ),
        .\Dout_reg[1]_6 (\Dout_reg[1]_6 ),
        .\Dout_reg[1]_7 (\Dout_reg[1]_7 ),
        .\Dout_reg[1]_8 (\Dout_reg[1]_8 ),
        .\Dout_reg[1]_9 (\Dout_reg[1]_9 ),
        .\Dout_reg[30] (\Dout_reg[30] ),
        .\Dout_reg[30]_0 (\Dout_reg[30]_0 ),
        .\Dout_reg[31] (\Dout_reg[31] ),
        .\Dout_reg[31]_2[25]_repN_alias (\Dout_reg[31]_2[25]_repN_alias ),
        .\Dout_reg[3] (\Dout_reg[3] ),
        .\Dout_reg[3]_0 (\Dout_reg[3]_0 ),
        .\Dout_reg[3]_1 (\Dout_reg[3]_1 ),
        .Q(Q),
        .S(S));
endmodule

module REGrwe_n
   (D,
    ALUResult_in,
    Q,
    \Dout_reg[3]_0 ,
    \Dout_reg[3]_1 ,
    \Dout_reg[2]_0 ,
    \Dout_reg[3]_2 ,
    \Dout_reg[1]_0 ,
    \Dout_reg[0]_0 ,
    \Dout_reg[0]_1 ,
    \Dout_reg[0]_2 ,
    \Dout_reg[6] ,
    \Dout_reg[4] ,
    \Dout_reg[4]_0 ,
    \Dout_reg[1]_1 ,
    \Dout_reg[7] ,
    \Dout[1]_i_5_0 ,
    \Dout_reg[5] ,
    \Dout_reg[5]_0 ,
    \Dout_reg[2]_1 ,
    \Dout_reg[2]_2 ,
    \Dout_reg[3]_3 ,
    \Dout_reg[3]_4 ,
    \Dout_reg[6]_0 ,
    \Dout_reg[7]_0 ,
    \Dout_reg[7]_1 ,
    \Dout_reg[8] ,
    \Dout_reg[8]_0 ,
    \Dout_reg[9] ,
    \Dout_reg[9]_0 ,
    \Dout_reg[9]_1 ,
    \Dout_reg[10] ,
    \Dout_reg[10]_0 ,
    \Dout_reg[10]_1 ,
    \Dout_reg[11] ,
    \Dout_reg[11]_0 ,
    \Dout_reg[11]_1 ,
    \Dout_reg[12] ,
    \Dout_reg[12]_0 ,
    \Dout_reg[12]_1 ,
    \Dout_reg[13] ,
    \Dout_reg[13]_0 ,
    \Dout_reg[13]_1 ,
    \Dout_reg[14] ,
    \Dout_reg[14]_0 ,
    \Dout_reg[14]_1 ,
    \Dout_reg[15] ,
    \Dout_reg[15]_0 ,
    \Dout_reg[15]_1 ,
    \Dout_reg[16] ,
    \Dout_reg[16]_0 ,
    \Dout_reg[16]_1 ,
    \Dout_reg[17] ,
    \Dout_reg[17]_0 ,
    \Dout_reg[17]_1 ,
    \Dout_reg[18] ,
    \Dout_reg[18]_0 ,
    \Dout_reg[18]_1 ,
    \Dout_reg[19] ,
    \Dout_reg[19]_0 ,
    \Dout_reg[19]_1 ,
    \Dout_reg[20] ,
    \Dout_reg[20]_0 ,
    \Dout_reg[20]_1 ,
    \Dout_reg[21] ,
    \Dout_reg[21]_0 ,
    \Dout_reg[21]_1 ,
    \Dout_reg[22] ,
    \Dout_reg[22]_0 ,
    \Dout_reg[22]_1 ,
    \Dout_reg[23] ,
    \Dout_reg[23]_0 ,
    \Dout_reg[24] ,
    \Dout_reg[24]_0 ,
    \Dout_reg[25] ,
    \Dout_reg[25]_0 ,
    \Dout_reg[26] ,
    \Dout_reg[26]_0 ,
    \Dout_reg[26]_1 ,
    \Dout_reg[27] ,
    \Dout_reg[27]_0 ,
    \Dout_reg[28] ,
    \Dout_reg[28]_0 ,
    \Dout_reg[29] ,
    \Dout_reg[29]_0 ,
    \Dout_reg[30] ,
    \Dout_reg[30]_0 ,
    \Dout_reg[0]_3 ,
    \Dout_reg[0]_4 ,
    \Dout_reg[0]_5 ,
    \Dout_reg[1]_2 ,
    \Dout_reg[1]_3 ,
    \Dout_reg[5]_1 ,
    \Dout_reg[5]_2 ,
    \Dout_reg[5]_3 ,
    \Dout_reg[5]_4 ,
    \Dout[1]_i_10_0 ,
    \Dout_reg[6]_1 ,
    \Dout_reg[6]_2 ,
    \Dout_reg[6]_3 ,
    \Dout[1]_i_8 ,
    \Dout_reg[8]_1 ,
    \Dout_reg[8]_2 ,
    \Dout_reg[8]_3 ,
    \Dout_reg[1]_4 ,
    \Dout_reg[1]_5 ,
    \Dout_reg[1]_6 ,
    \Dout_reg[2]_3 ,
    \Dout_reg[2]_4 ,
    \Dout_reg[3]_5 ,
    \Dout_reg[3]_6 ,
    \Dout_reg[4]_1 ,
    \Dout_reg[23]_1 ,
    \Dout_reg[23]_2 ,
    \Dout_reg[23]_3 ,
    \Dout_reg[24]_1 ,
    \Dout_reg[24]_2 ,
    \Dout_reg[25]_1 ,
    \Dout_reg[25]_2 ,
    \Dout_reg[27]_1 ,
    \Dout_reg[27]_2 ,
    \Dout_reg[27]_3 ,
    \Dout_reg[28]_1 ,
    \Dout_reg[28]_2 ,
    \Dout_reg[29]_1 ,
    \Dout_reg[29]_2 ,
    \Dout_reg[30]_1 ,
    \Dout_reg[30]_2 ,
    \Dout_reg[0]_6 ,
    \Dout_reg[0]_7 ,
    \Dout_reg[3]_7 ,
    \Dout_reg[3]_8 ,
    O,
    \Dout_reg[2]_5 ,
    CO,
    \Dout_reg[3]_9 ,
    SR,
    E,
    \Dout_reg[3]_10 ,
    CLK_IBUF_BUFG,
    \Dout_reg[31]_2[4]_repN_alias ,
    \Dout_reg[31]_2[30]_repN_alias ,
    \Dout[2]_i_3_n_0_alias ,
    \Dout[3]_i_10_n_0_alias ,
    \Dout_reg[0]_30_repN_alias ,
    \Dout_reg[31]_2[30]_repN_3_alias ,
    \Dout[7]_i_3_n_0_alias ,
    \Dout_reg[0]_36_alias ,
    \Dout_reg[0]_2_repN_alias ,
    \Dout_reg[0]_2_repN_1_alias ,
    \Dout[14]_i_3_n_0_alias ,
    \Dout[14]_i_8_n_0_alias ,
    \Dout_reg[0]_13_repN_alias ,
    \Dout_reg[31]_2[2]_repN_alias ,
    \Dout[13]_i_3_n_0_alias ,
    \Dout[13]_i_8_n_0_alias ,
    \Dout_reg[0]_12_repN_alias );
  output [2:0]D;
  output [31:0]ALUResult_in;
  output [1:0]Q;
  output \Dout_reg[3]_0 ;
  output \Dout_reg[3]_1 ;
  output \Dout_reg[2]_0 ;
  output \Dout_reg[3]_2 ;
  output \Dout_reg[1]_0 ;
  input \Dout_reg[0]_0 ;
  input \Dout_reg[0]_1 ;
  input \Dout_reg[0]_2 ;
  input \Dout_reg[6] ;
  input \Dout_reg[4] ;
  input \Dout_reg[4]_0 ;
  input \Dout_reg[1]_1 ;
  input \Dout_reg[7] ;
  input \Dout[1]_i_5_0 ;
  input \Dout_reg[5] ;
  input \Dout_reg[5]_0 ;
  input \Dout_reg[2]_1 ;
  input \Dout_reg[2]_2 ;
  input \Dout_reg[3]_3 ;
  input \Dout_reg[3]_4 ;
  input \Dout_reg[6]_0 ;
  input \Dout_reg[7]_0 ;
  input \Dout_reg[7]_1 ;
  input \Dout_reg[8] ;
  input \Dout_reg[8]_0 ;
  input \Dout_reg[9] ;
  input \Dout_reg[9]_0 ;
  input \Dout_reg[9]_1 ;
  input \Dout_reg[10] ;
  input \Dout_reg[10]_0 ;
  input \Dout_reg[10]_1 ;
  input \Dout_reg[11] ;
  input \Dout_reg[11]_0 ;
  input \Dout_reg[11]_1 ;
  input \Dout_reg[12] ;
  input \Dout_reg[12]_0 ;
  input \Dout_reg[12]_1 ;
  input \Dout_reg[13] ;
  input \Dout_reg[13]_0 ;
  input \Dout_reg[13]_1 ;
  input \Dout_reg[14] ;
  input \Dout_reg[14]_0 ;
  input \Dout_reg[14]_1 ;
  input \Dout_reg[15] ;
  input \Dout_reg[15]_0 ;
  input \Dout_reg[15]_1 ;
  input \Dout_reg[16] ;
  input \Dout_reg[16]_0 ;
  input \Dout_reg[16]_1 ;
  input \Dout_reg[17] ;
  input \Dout_reg[17]_0 ;
  input \Dout_reg[17]_1 ;
  input \Dout_reg[18] ;
  input \Dout_reg[18]_0 ;
  input \Dout_reg[18]_1 ;
  input \Dout_reg[19] ;
  input \Dout_reg[19]_0 ;
  input \Dout_reg[19]_1 ;
  input \Dout_reg[20] ;
  input \Dout_reg[20]_0 ;
  input \Dout_reg[20]_1 ;
  input \Dout_reg[21] ;
  input \Dout_reg[21]_0 ;
  input \Dout_reg[21]_1 ;
  input \Dout_reg[22] ;
  input \Dout_reg[22]_0 ;
  input \Dout_reg[22]_1 ;
  input \Dout_reg[23] ;
  input \Dout_reg[23]_0 ;
  input \Dout_reg[24] ;
  input \Dout_reg[24]_0 ;
  input \Dout_reg[25] ;
  input \Dout_reg[25]_0 ;
  input \Dout_reg[26] ;
  input \Dout_reg[26]_0 ;
  input \Dout_reg[26]_1 ;
  input \Dout_reg[27] ;
  input \Dout_reg[27]_0 ;
  input \Dout_reg[28] ;
  input \Dout_reg[28]_0 ;
  input \Dout_reg[29] ;
  input \Dout_reg[29]_0 ;
  input \Dout_reg[30] ;
  input \Dout_reg[30]_0 ;
  input \Dout_reg[0]_3 ;
  input \Dout_reg[0]_4 ;
  input \Dout_reg[0]_5 ;
  input \Dout_reg[1]_2 ;
  input \Dout_reg[1]_3 ;
  input \Dout_reg[5]_1 ;
  input [0:0]\Dout_reg[5]_2 ;
  input \Dout_reg[5]_3 ;
  input \Dout_reg[5]_4 ;
  input \Dout[1]_i_10_0 ;
  input \Dout_reg[6]_1 ;
  input \Dout_reg[6]_2 ;
  input \Dout_reg[6]_3 ;
  input \Dout[1]_i_8 ;
  input \Dout_reg[8]_1 ;
  input \Dout_reg[8]_2 ;
  input \Dout_reg[8]_3 ;
  input \Dout_reg[1]_4 ;
  input \Dout_reg[1]_5 ;
  input \Dout_reg[1]_6 ;
  input \Dout_reg[2]_3 ;
  input \Dout_reg[2]_4 ;
  input \Dout_reg[3]_5 ;
  input \Dout_reg[3]_6 ;
  input \Dout_reg[4]_1 ;
  input \Dout_reg[23]_1 ;
  input \Dout_reg[23]_2 ;
  input \Dout_reg[23]_3 ;
  input \Dout_reg[24]_1 ;
  input \Dout_reg[24]_2 ;
  input \Dout_reg[25]_1 ;
  input \Dout_reg[25]_2 ;
  input \Dout_reg[27]_1 ;
  input \Dout_reg[27]_2 ;
  input \Dout_reg[27]_3 ;
  input \Dout_reg[28]_1 ;
  input \Dout_reg[28]_2 ;
  input \Dout_reg[29]_1 ;
  input \Dout_reg[29]_2 ;
  input \Dout_reg[30]_1 ;
  input \Dout_reg[30]_2 ;
  input [31:0]\Dout_reg[0]_6 ;
  input [31:0]\Dout_reg[0]_7 ;
  input [5:0]\Dout_reg[3]_7 ;
  input [5:0]\Dout_reg[3]_8 ;
  input [0:0]O;
  input [0:0]\Dout_reg[2]_5 ;
  input [0:0]CO;
  input [0:0]\Dout_reg[3]_9 ;
  input [0:0]SR;
  input [0:0]E;
  input [3:0]\Dout_reg[3]_10 ;
  input CLK_IBUF_BUFG;
  input \Dout_reg[31]_2[4]_repN_alias ;
  input \Dout_reg[31]_2[30]_repN_alias ;
  output \Dout[2]_i_3_n_0_alias ;
  input \Dout[3]_i_10_n_0_alias ;
  input \Dout_reg[0]_30_repN_alias ;
  input \Dout_reg[31]_2[30]_repN_3_alias ;
  output \Dout[7]_i_3_n_0_alias ;
  input \Dout_reg[0]_36_alias ;
  input \Dout_reg[0]_2_repN_alias ;
  input \Dout_reg[0]_2_repN_1_alias ;
  output \Dout[14]_i_3_n_0_alias ;
  input \Dout[14]_i_8_n_0_alias ;
  input \Dout_reg[0]_13_repN_alias ;
  input \Dout_reg[31]_2[2]_repN_alias ;
  output \Dout[13]_i_3_n_0_alias ;
  input \Dout[13]_i_8_n_0_alias ;
  input \Dout_reg[0]_12_repN_alias ;

  wire [3:2]ALUControl_in;
  wire [31:0]ALUResult_in;
  wire CLK_IBUF_BUFG;
  wire [0:0]CO;
  wire [2:0]D;
  wire \Dout[0]_i_3__0_n_0 ;
  wire \Dout[0]_i_3_n_0 ;
  wire \Dout[10]_i_3_n_0 ;
  wire \Dout[11]_i_3_n_0 ;
  wire \Dout[12]_i_3_n_0 ;
  wire \Dout[13]_i_3_n_0 ;
  wire \Dout[13]_i_8_n_0_alias ;
  wire \Dout[14]_i_3_n_0 ;
  wire \Dout[14]_i_8_n_0_alias ;
  wire \Dout[15]_i_3_n_0 ;
  wire \Dout[16]_i_3_n_0 ;
  wire \Dout[17]_i_3_n_0 ;
  wire \Dout[18]_i_3_n_0 ;
  wire \Dout[19]_i_3_n_0 ;
  wire \Dout[1]_i_10_0 ;
  wire \Dout[1]_i_10_n_0 ;
  wire \Dout[1]_i_11__0_n_0 ;
  wire \Dout[1]_i_15_n_0 ;
  wire \Dout[1]_i_16_n_0 ;
  wire \Dout[1]_i_2__0_n_0 ;
  wire \Dout[1]_i_2_n_0 ;
  wire \Dout[1]_i_3__0_n_0 ;
  wire \Dout[1]_i_3_n_0 ;
  wire \Dout[1]_i_4__0_n_0 ;
  wire \Dout[1]_i_4_n_0 ;
  wire \Dout[1]_i_5_0 ;
  wire \Dout[1]_i_5_n_0 ;
  wire \Dout[1]_i_6_n_0 ;
  wire \Dout[1]_i_7_n_0 ;
  wire \Dout[1]_i_8 ;
  wire \Dout[1]_i_9__0_n_0 ;
  wire \Dout[1]_i_9_n_0 ;
  wire \Dout[20]_i_3_n_0 ;
  wire \Dout[21]_i_3_n_0 ;
  wire \Dout[22]_i_3_n_0 ;
  wire \Dout[23]_i_2_n_0 ;
  wire \Dout[23]_i_3_n_0 ;
  wire \Dout[24]_i_2_n_0 ;
  wire \Dout[24]_i_3_n_0 ;
  wire \Dout[25]_i_2_n_0 ;
  wire \Dout[25]_i_3_n_0 ;
  wire \Dout[26]_i_3_n_0 ;
  wire \Dout[27]_i_2_n_0 ;
  wire \Dout[27]_i_3_n_0 ;
  wire \Dout[28]_i_2_n_0 ;
  wire \Dout[28]_i_3_n_0 ;
  wire \Dout[29]_i_2_n_0 ;
  wire \Dout[29]_i_3_n_0 ;
  wire \Dout[2]_i_2_n_0 ;
  wire \Dout[2]_i_3_n_0 ;
  wire \Dout[2]_i_4_n_0 ;
  wire \Dout[2]_i_9_n_0 ;
  wire \Dout[30]_i_2_n_0 ;
  wire \Dout[30]_i_3_n_0 ;
  wire \Dout[30]_i_5_n_0 ;
  wire \Dout[3]_i_10_n_0_alias ;
  wire \Dout[3]_i_2__0_n_0 ;
  wire \Dout[3]_i_2_n_0 ;
  wire \Dout[3]_i_3_n_0 ;
  wire \Dout[3]_i_4_n_0 ;
  wire \Dout[3]_i_9_n_0 ;
  wire \Dout[4]_i_2_n_0 ;
  wire \Dout[4]_i_3_n_0 ;
  wire \Dout[4]_i_4_n_0 ;
  wire \Dout[4]_i_9_n_0 ;
  wire \Dout[5]_i_10_n_0 ;
  wire \Dout[5]_i_2_n_0 ;
  wire \Dout[5]_i_3_n_0 ;
  wire \Dout[5]_i_4_n_0 ;
  wire \Dout[6]_i_2_n_0 ;
  wire \Dout[6]_i_3_n_0 ;
  wire \Dout[7]_i_3_n_0 ;
  wire \Dout[8]_i_2_n_0 ;
  wire \Dout[8]_i_3_n_0 ;
  wire \Dout[9]_i_3_n_0 ;
  wire \Dout_reg[0]_0 ;
  wire \Dout_reg[0]_1 ;
  wire \Dout_reg[0]_12_repN_alias ;
  wire \Dout_reg[0]_13_repN_alias ;
  wire \Dout_reg[0]_2 ;
  wire \Dout_reg[0]_2_repN_1_alias ;
  wire \Dout_reg[0]_2_repN_alias ;
  wire \Dout_reg[0]_3 ;
  wire \Dout_reg[0]_30_repN_alias ;
  wire \Dout_reg[0]_36_alias ;
  wire \Dout_reg[0]_4 ;
  wire \Dout_reg[0]_5 ;
  wire [31:0]\Dout_reg[0]_6 ;
  wire [31:0]\Dout_reg[0]_7 ;
  wire \Dout_reg[10] ;
  wire \Dout_reg[10]_0 ;
  wire \Dout_reg[10]_1 ;
  wire \Dout_reg[11] ;
  wire \Dout_reg[11]_0 ;
  wire \Dout_reg[11]_1 ;
  wire \Dout_reg[12] ;
  wire \Dout_reg[12]_0 ;
  wire \Dout_reg[12]_1 ;
  wire \Dout_reg[13] ;
  wire \Dout_reg[13]_0 ;
  wire \Dout_reg[13]_1 ;
  wire \Dout_reg[14] ;
  wire \Dout_reg[14]_0 ;
  wire \Dout_reg[14]_1 ;
  wire \Dout_reg[15] ;
  wire \Dout_reg[15]_0 ;
  wire \Dout_reg[15]_1 ;
  wire \Dout_reg[16] ;
  wire \Dout_reg[16]_0 ;
  wire \Dout_reg[16]_1 ;
  wire \Dout_reg[17] ;
  wire \Dout_reg[17]_0 ;
  wire \Dout_reg[17]_1 ;
  wire \Dout_reg[18] ;
  wire \Dout_reg[18]_0 ;
  wire \Dout_reg[18]_1 ;
  wire \Dout_reg[19] ;
  wire \Dout_reg[19]_0 ;
  wire \Dout_reg[19]_1 ;
  wire \Dout_reg[1]_0 ;
  wire \Dout_reg[1]_1 ;
  wire \Dout_reg[1]_2 ;
  wire \Dout_reg[1]_3 ;
  wire \Dout_reg[1]_4 ;
  wire \Dout_reg[1]_5 ;
  wire \Dout_reg[1]_6 ;
  wire \Dout_reg[20] ;
  wire \Dout_reg[20]_0 ;
  wire \Dout_reg[20]_1 ;
  wire \Dout_reg[21] ;
  wire \Dout_reg[21]_0 ;
  wire \Dout_reg[21]_1 ;
  wire \Dout_reg[22] ;
  wire \Dout_reg[22]_0 ;
  wire \Dout_reg[22]_1 ;
  wire \Dout_reg[23] ;
  wire \Dout_reg[23]_0 ;
  wire \Dout_reg[23]_1 ;
  wire \Dout_reg[23]_2 ;
  wire \Dout_reg[23]_3 ;
  wire \Dout_reg[24] ;
  wire \Dout_reg[24]_0 ;
  wire \Dout_reg[24]_1 ;
  wire \Dout_reg[24]_2 ;
  wire \Dout_reg[25] ;
  wire \Dout_reg[25]_0 ;
  wire \Dout_reg[25]_1 ;
  wire \Dout_reg[25]_2 ;
  wire \Dout_reg[26] ;
  wire \Dout_reg[26]_0 ;
  wire \Dout_reg[26]_1 ;
  wire \Dout_reg[27] ;
  wire \Dout_reg[27]_0 ;
  wire \Dout_reg[27]_1 ;
  wire \Dout_reg[27]_2 ;
  wire \Dout_reg[27]_3 ;
  wire \Dout_reg[28] ;
  wire \Dout_reg[28]_0 ;
  wire \Dout_reg[28]_1 ;
  wire \Dout_reg[28]_2 ;
  wire \Dout_reg[29] ;
  wire \Dout_reg[29]_0 ;
  wire \Dout_reg[29]_1 ;
  wire \Dout_reg[29]_2 ;
  wire \Dout_reg[2]_0 ;
  wire \Dout_reg[2]_1 ;
  wire \Dout_reg[2]_2 ;
  wire \Dout_reg[2]_3 ;
  wire \Dout_reg[2]_4 ;
  wire [0:0]\Dout_reg[2]_5 ;
  wire \Dout_reg[30] ;
  wire \Dout_reg[30]_0 ;
  wire \Dout_reg[30]_1 ;
  wire \Dout_reg[30]_2 ;
  wire \Dout_reg[31]_2[2]_repN_alias ;
  wire \Dout_reg[31]_2[30]_repN_3_alias ;
  wire \Dout_reg[31]_2[30]_repN_alias ;
  wire \Dout_reg[31]_2[4]_repN_alias ;
  wire \Dout_reg[3]_0 ;
  wire \Dout_reg[3]_1 ;
  wire [3:0]\Dout_reg[3]_10 ;
  wire \Dout_reg[3]_2 ;
  wire \Dout_reg[3]_3 ;
  wire \Dout_reg[3]_4 ;
  wire \Dout_reg[3]_5 ;
  wire \Dout_reg[3]_6 ;
  wire [5:0]\Dout_reg[3]_7 ;
  wire [5:0]\Dout_reg[3]_8 ;
  wire [0:0]\Dout_reg[3]_9 ;
  wire \Dout_reg[4] ;
  wire \Dout_reg[4]_0 ;
  wire \Dout_reg[4]_1 ;
  wire \Dout_reg[5] ;
  wire \Dout_reg[5]_0 ;
  wire \Dout_reg[5]_1 ;
  wire [0:0]\Dout_reg[5]_2 ;
  wire \Dout_reg[5]_3 ;
  wire \Dout_reg[5]_4 ;
  wire \Dout_reg[6] ;
  wire \Dout_reg[6]_0 ;
  wire \Dout_reg[6]_1 ;
  wire \Dout_reg[6]_2 ;
  wire \Dout_reg[6]_3 ;
  wire \Dout_reg[7] ;
  wire \Dout_reg[7]_0 ;
  wire \Dout_reg[7]_1 ;
  wire \Dout_reg[8] ;
  wire \Dout_reg[8]_0 ;
  wire \Dout_reg[8]_1 ;
  wire \Dout_reg[8]_2 ;
  wire \Dout_reg[8]_3 ;
  wire \Dout_reg[9] ;
  wire \Dout_reg[9]_0 ;
  wire \Dout_reg[9]_1 ;
  wire [0:0]E;
  wire [0:0]O;
  wire [1:0]Q;
  wire [0:0]SR;

  assign \Dout[13]_i_3_n_0_alias  = \Dout[13]_i_3_n_0 ;
  assign \Dout[14]_i_3_n_0_alias  = \Dout[14]_i_3_n_0 ;
  assign \Dout[2]_i_3_n_0_alias  = \Dout[2]_i_3_n_0 ;
  assign \Dout[7]_i_3_n_0_alias  = \Dout[7]_i_3_n_0 ;
  LUT6 #(
    .INIT(64'hFFF4FFF4FFF4F0F4)) 
    \Dout[0]_i_1 
       (.I0(ALUControl_in[2]),
        .I1(\Dout_reg[0]_0 ),
        .I2(\Dout[0]_i_3__0_n_0 ),
        .I3(ALUControl_in[3]),
        .I4(\Dout_reg[0]_1 ),
        .I5(\Dout_reg[0]_2 ),
        .O(ALUResult_in[31]));
  LUT6 #(
    .INIT(64'hFFF4FFF4FFF4F0F4)) 
    \Dout[0]_i_1__0 
       (.I0(ALUControl_in[2]),
        .I1(\Dout_reg[0]_3 ),
        .I2(\Dout[0]_i_3_n_0 ),
        .I3(ALUControl_in[3]),
        .I4(\Dout_reg[0]_4 ),
        .I5(\Dout_reg[0]_5 ),
        .O(ALUResult_in[0]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[0]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [0]),
        .I3(\Dout_reg[0]_7 [0]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[0]_i_3__0 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [31]),
        .I3(\Dout_reg[0]_7 [31]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[0]_i_3__0_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \Dout[0]_i_9 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(\Dout_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[10]_i_1 
       (.I0(\Dout_reg[10] ),
        .I1(\Dout[10]_i_3_n_0 ),
        .I2(\Dout_reg[10]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[10]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[10]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[10]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [10]),
        .I3(\Dout_reg[0]_7 [10]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[10]_i_3_n_0 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[11]_i_1 
       (.I0(\Dout_reg[11] ),
        .I1(\Dout[11]_i_3_n_0 ),
        .I2(\Dout_reg[11]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[11]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[11]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[11]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [11]),
        .I3(\Dout_reg[0]_7 [11]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[12]_i_1 
       (.I0(\Dout_reg[12] ),
        .I1(\Dout[12]_i_3_n_0 ),
        .I2(\Dout_reg[12]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[12]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[12]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[12]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [12]),
        .I3(\Dout_reg[0]_7 [12]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[12]_i_3_n_0 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'hF8F8F8F8FFFFFFF8)) 
    \Dout[13]_i_1_comp 
       (.I0(\Dout[30]_i_5_n_0 ),
        .I1(\Dout_reg[13]_0 ),
        .I2(\Dout_reg[13] ),
        .I3(\Dout_reg[13]_1 ),
        .I4(\Dout[13]_i_8_n_0_alias ),
        .I5(\Dout_reg[0]_12_repN_alias ),
        .O(ALUResult_in[13]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[13]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [13]),
        .I3(\Dout_reg[0]_7 [13]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[13]_i_3_n_0 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'hF8F8F8F8FFFFFFF8)) 
    \Dout[14]_i_1_comp 
       (.I0(\Dout[30]_i_5_n_0 ),
        .I1(\Dout_reg[14]_0 ),
        .I2(\Dout_reg[14] ),
        .I3(\Dout_reg[14]_1 ),
        .I4(\Dout[14]_i_8_n_0_alias ),
        .I5(\Dout_reg[0]_13_repN_alias ),
        .O(ALUResult_in[14]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[14]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [14]),
        .I3(\Dout_reg[0]_7 [14]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[15]_i_1 
       (.I0(\Dout_reg[15] ),
        .I1(\Dout[15]_i_3_n_0 ),
        .I2(\Dout_reg[15]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[15]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[15]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[15]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [15]),
        .I3(\Dout_reg[0]_7 [15]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[16]_i_1 
       (.I0(\Dout_reg[16] ),
        .I1(\Dout[16]_i_3_n_0 ),
        .I2(\Dout_reg[16]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[16]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[16]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[16]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [16]),
        .I3(\Dout_reg[0]_7 [16]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[17]_i_1 
       (.I0(\Dout_reg[17] ),
        .I1(\Dout[17]_i_3_n_0 ),
        .I2(\Dout_reg[17]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[17]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[17]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[17]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [17]),
        .I3(\Dout_reg[0]_7 [17]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[18]_i_1 
       (.I0(\Dout_reg[18]_0 ),
        .I1(\Dout[18]_i_3_n_0 ),
        .I2(\Dout_reg[18]_1 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[18] ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[18]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[18]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [18]),
        .I3(\Dout_reg[0]_7 [18]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[19]_i_1 
       (.I0(\Dout_reg[19] ),
        .I1(\Dout[19]_i_3_n_0 ),
        .I2(\Dout_reg[19]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[19]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[19]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[19]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [19]),
        .I3(\Dout_reg[0]_7 [19]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \Dout[1]_i_10 
       (.I0(ALUResult_in[4]),
        .I1(\Dout[1]_i_16_n_0 ),
        .I2(\Dout_reg[6] ),
        .I3(\Dout_reg[3]_0 ),
        .I4(ALUResult_in[2]),
        .I5(ALUResult_in[3]),
        .O(\Dout[1]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hAAAE)) 
    \Dout[1]_i_11__0 
       (.I0(\Dout[7]_i_3_n_0 ),
        .I1(\Dout_reg[7]_1 ),
        .I2(ALUControl_in[3]),
        .I3(ALUControl_in[2]),
        .O(\Dout[1]_i_11__0_n_0 ));
  LUT4 #(
    .INIT(16'hAAAE)) 
    \Dout[1]_i_13 
       (.I0(\Dout[9]_i_3_n_0 ),
        .I1(\Dout_reg[9]_0 ),
        .I2(ALUControl_in[3]),
        .I3(ALUControl_in[2]),
        .O(\Dout_reg[3]_2 ));
  LUT6 #(
    .INIT(64'hF0F0F4F4FFF0F4F4)) 
    \Dout[1]_i_14 
       (.I0(ALUControl_in[2]),
        .I1(\Dout_reg[8] ),
        .I2(\Dout[8]_i_3_n_0 ),
        .I3(\Dout[1]_i_8 ),
        .I4(ALUControl_in[3]),
        .I5(Q[1]),
        .O(\Dout_reg[2]_0 ));
  LUT4 #(
    .INIT(16'hAAAE)) 
    \Dout[1]_i_15 
       (.I0(\Dout[18]_i_3_n_0 ),
        .I1(\Dout_reg[18]_1 ),
        .I2(ALUControl_in[3]),
        .I3(ALUControl_in[2]),
        .O(\Dout[1]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hF0F0F4F4FFF0F4F4)) 
    \Dout[1]_i_16 
       (.I0(ALUControl_in[2]),
        .I1(\Dout_reg[6]_0 ),
        .I2(\Dout[6]_i_3_n_0 ),
        .I3(\Dout[1]_i_10_0 ),
        .I4(ALUControl_in[3]),
        .I5(Q[1]),
        .O(\Dout[1]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFEFEFEFE)) 
    \Dout[1]_i_1__0 
       (.I0(\Dout[1]_i_2__0_n_0 ),
        .I1(\Dout[1]_i_3__0_n_0 ),
        .I2(\Dout[1]_i_4__0_n_0 ),
        .I3(\Dout_reg[1]_2 ),
        .I4(\Dout_reg[1]_3 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[1]));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'h0000000000800000)) 
    \Dout[1]_i_1_comp 
       (.I0(\Dout[1]_i_4_n_0 ),
        .I1(\Dout[1]_i_3_n_0 ),
        .I2(\Dout[1]_i_2_n_0 ),
        .I3(\Dout[1]_i_6_n_0 ),
        .I4(\Dout[1]_i_5_n_0 ),
        .I5(\Dout_reg[1]_1 ),
        .O(D[0]));
  LUT4 #(
    .INIT(16'h0001)) 
    \Dout[1]_i_2 
       (.I0(ALUResult_in[19]),
        .I1(ALUResult_in[20]),
        .I2(ALUResult_in[21]),
        .I3(ALUResult_in[30]),
        .O(\Dout[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAA8080000)) 
    \Dout[1]_i_2__0 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[1]_4 ),
        .I2(\Dout_reg[5]_2 ),
        .I3(\Dout_reg[1]_5 ),
        .I4(Q[0]),
        .I5(\Dout_reg[1]_6 ),
        .O(\Dout[1]_i_2__0_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \Dout[1]_i_3 
       (.I0(ALUResult_in[25]),
        .I1(ALUResult_in[24]),
        .I2(ALUResult_in[23]),
        .I3(ALUResult_in[22]),
        .O(\Dout[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[1]_i_3__0 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [1]),
        .I3(\Dout_reg[0]_7 [1]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[1]_i_3__0_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \Dout[1]_i_4 
       (.I0(ALUResult_in[29]),
        .I1(ALUResult_in[28]),
        .I2(ALUResult_in[27]),
        .I3(ALUResult_in[26]),
        .O(\Dout[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h88A888AA88A88888)) 
    \Dout[1]_i_4__0 
       (.I0(\Dout[30]_i_5_n_0 ),
        .I1(\Dout[1]_i_9__0_n_0 ),
        .I2(\Dout_reg[3]_7 [0]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(\Dout_reg[3]_8 [0]),
        .O(\Dout[1]_i_4__0_n_0 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \Dout[1]_i_5_comp_1 
       (.I0(ALUResult_in[31]),
        .I1(ALUResult_in[12]),
        .I2(ALUResult_in[11]),
        .I3(ALUResult_in[10]),
        .I4(\Dout[1]_i_7_n_0 ),
        .I5(ALUResult_in[5]),
        .O(\Dout[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \Dout[1]_i_6 
       (.I0(\Dout[1]_i_9_n_0 ),
        .I1(\Dout[1]_i_10_n_0 ),
        .I2(ALUResult_in[15]),
        .I3(ALUResult_in[14]),
        .I4(ALUResult_in[17]),
        .I5(ALUResult_in[16]),
        .O(\Dout[1]_i_6_n_0 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'hFFFCFFF8FFF4FFF0)) 
    \Dout[1]_i_7_comp 
       (.I0(Q[1]),
        .I1(ALUControl_in[3]),
        .I2(ALUResult_in[13]),
        .I3(\Dout[1]_i_11__0_n_0 ),
        .I4(\Dout[1]_i_5_0 ),
        .I5(\Dout_reg[7] ),
        .O(\Dout[1]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \Dout[1]_i_9 
       (.I0(ALUResult_in[0]),
        .I1(ALUResult_in[1]),
        .I2(\Dout_reg[3]_0 ),
        .I3(\Dout_reg[18] ),
        .I4(\Dout[1]_i_15_n_0 ),
        .I5(\Dout_reg[18]_0 ),
        .O(\Dout[1]_i_9_n_0 ));
  LUT4 #(
    .INIT(16'hE800)) 
    \Dout[1]_i_9__0 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_6 [1]),
        .I2(\Dout_reg[0]_7 [1]),
        .I3(Q[1]),
        .O(\Dout[1]_i_9__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[20]_i_1 
       (.I0(\Dout_reg[20] ),
        .I1(\Dout[20]_i_3_n_0 ),
        .I2(\Dout_reg[20]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[20]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[20]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[20]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [20]),
        .I3(\Dout_reg[0]_7 [20]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[21]_i_1 
       (.I0(\Dout_reg[21] ),
        .I1(\Dout[21]_i_3_n_0 ),
        .I2(\Dout_reg[21]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[21]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[21]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[21]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [21]),
        .I3(\Dout_reg[0]_7 [21]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[22]_i_1 
       (.I0(\Dout_reg[22] ),
        .I1(\Dout[22]_i_3_n_0 ),
        .I2(\Dout_reg[22]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[22]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[22]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[22]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [22]),
        .I3(\Dout_reg[0]_7 [22]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[23]_i_1 
       (.I0(\Dout[23]_i_2_n_0 ),
        .I1(\Dout[23]_i_3_n_0 ),
        .I2(\Dout_reg[23] ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[23]_0 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[23]));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[23]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[23]_1 ),
        .I2(Q[0]),
        .I3(\Dout_reg[23]_2 ),
        .I4(\Dout_reg[23]_3 ),
        .I5(\Dout_reg[5]_2 ),
        .O(\Dout[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[23]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [23]),
        .I3(\Dout_reg[0]_7 [23]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[24]_i_1 
       (.I0(\Dout[24]_i_2_n_0 ),
        .I1(\Dout[24]_i_3_n_0 ),
        .I2(\Dout_reg[24] ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[24]_0 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[24]));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[24]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[24]_1 ),
        .I2(Q[0]),
        .I3(\Dout_reg[23]_3 ),
        .I4(\Dout_reg[24]_2 ),
        .I5(\Dout_reg[5]_2 ),
        .O(\Dout[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[24]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [24]),
        .I3(\Dout_reg[0]_7 [24]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[25]_i_1 
       (.I0(\Dout[25]_i_2_n_0 ),
        .I1(\Dout[25]_i_3_n_0 ),
        .I2(\Dout_reg[25] ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[25]_0 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[25]));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[25]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[25]_1 ),
        .I2(Q[0]),
        .I3(\Dout_reg[24]_2 ),
        .I4(\Dout_reg[25]_2 ),
        .I5(\Dout_reg[5]_2 ),
        .O(\Dout[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[25]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [25]),
        .I3(\Dout_reg[0]_7 [25]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[26]_i_1 
       (.I0(\Dout_reg[26] ),
        .I1(\Dout[26]_i_3_n_0 ),
        .I2(\Dout_reg[26]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[26]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[26]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[26]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [26]),
        .I3(\Dout_reg[0]_7 [26]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[27]_i_1 
       (.I0(\Dout[27]_i_2_n_0 ),
        .I1(\Dout[27]_i_3_n_0 ),
        .I2(\Dout_reg[27] ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[27]_0 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[27]));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[27]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[27]_1 ),
        .I2(Q[0]),
        .I3(\Dout_reg[27]_2 ),
        .I4(\Dout_reg[27]_3 ),
        .I5(\Dout_reg[5]_2 ),
        .O(\Dout[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[27]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [27]),
        .I3(\Dout_reg[0]_7 [27]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[28]_i_1 
       (.I0(\Dout[28]_i_2_n_0 ),
        .I1(\Dout[28]_i_3_n_0 ),
        .I2(\Dout_reg[28] ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[28]_0 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[28]));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[28]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[28]_1 ),
        .I2(Q[0]),
        .I3(\Dout_reg[27]_3 ),
        .I4(\Dout_reg[28]_2 ),
        .I5(\Dout_reg[5]_2 ),
        .O(\Dout[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[28]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [28]),
        .I3(\Dout_reg[0]_7 [28]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[29]_i_1 
       (.I0(\Dout[29]_i_2_n_0 ),
        .I1(\Dout[29]_i_3_n_0 ),
        .I2(\Dout_reg[29] ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[29]_0 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[29]));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[29]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[29]_1 ),
        .I2(Q[0]),
        .I3(\Dout_reg[28]_2 ),
        .I4(\Dout_reg[29]_2 ),
        .I5(\Dout_reg[5]_2 ),
        .O(\Dout[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[29]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [29]),
        .I3(\Dout_reg[0]_7 [29]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0002000200030000)) 
    \Dout[2]_i_1__0 
       (.I0(O),
        .I1(ALUControl_in[2]),
        .I2(ALUControl_in[3]),
        .I3(Q[1]),
        .I4(\Dout_reg[2]_5 ),
        .I5(Q[0]),
        .O(D[1]));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'hFFFEFFFEFFFCFEFC)) 
    \Dout[2]_i_1_comp 
       (.I0(\Dout_reg[2]_2 ),
        .I1(\Dout[2]_i_4_n_0 ),
        .I2(\Dout[2]_i_2_n_0 ),
        .I3(\Dout_reg[2]_1 ),
        .I4(\Dout[3]_i_10_n_0_alias ),
        .I5(\Dout_reg[0]_30_repN_alias ),
        .O(ALUResult_in[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAA8080000)) 
    \Dout[2]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[1]_5 ),
        .I2(\Dout_reg[5]_2 ),
        .I3(\Dout_reg[2]_3 ),
        .I4(Q[0]),
        .I5(\Dout_reg[2]_4 ),
        .O(\Dout[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[2]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [2]),
        .I3(\Dout_reg[31]_2[2]_repN_alias ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h88A888AA88A88888)) 
    \Dout[2]_i_4 
       (.I0(\Dout[30]_i_5_n_0 ),
        .I1(\Dout[2]_i_9_n_0 ),
        .I2(\Dout_reg[3]_7 [1]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(\Dout_reg[3]_8 [1]),
        .O(\Dout[2]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hE800)) 
    \Dout[2]_i_9 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_6 [2]),
        .I2(\Dout_reg[31]_2[2]_repN_alias ),
        .I3(Q[1]),
        .O(\Dout[2]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[30]_i_1 
       (.I0(\Dout[30]_i_2_n_0 ),
        .I1(\Dout[30]_i_3_n_0 ),
        .I2(\Dout_reg[30] ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[30]_0 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[30]));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[30]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[30]_1 ),
        .I2(Q[0]),
        .I3(\Dout_reg[29]_2 ),
        .I4(\Dout_reg[30]_2 ),
        .I5(\Dout_reg[5]_2 ),
        .O(\Dout[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[30]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [30]),
        .I3(\Dout_reg[31]_2[30]_repN_3_alias ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[30]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \Dout[30]_i_5 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .O(\Dout[30]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[30]_i_7 
       (.I0(ALUControl_in[3]),
        .I1(Q[1]),
        .O(\Dout_reg[3]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Dout[30]_i_8 
       (.I0(ALUControl_in[3]),
        .I1(Q[1]),
        .O(\Dout_reg[3]_1 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFEFEFEFE)) 
    \Dout[3]_i_1 
       (.I0(\Dout[3]_i_2_n_0 ),
        .I1(\Dout[3]_i_3_n_0 ),
        .I2(\Dout[3]_i_4_n_0 ),
        .I3(\Dout_reg[3]_3 ),
        .I4(\Dout_reg[3]_4 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[3]));
  LUT6 #(
    .INIT(64'hA88A20022002A88A)) 
    \Dout[3]_i_1__0 
       (.I0(\Dout[3]_i_2__0_n_0 ),
        .I1(Q[0]),
        .I2(CO),
        .I3(\Dout_reg[3]_8 [5]),
        .I4(\Dout_reg[3]_9 ),
        .I5(\Dout_reg[3]_7 [5]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAA8080000)) 
    \Dout[3]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[2]_3 ),
        .I2(\Dout_reg[5]_2 ),
        .I3(\Dout_reg[3]_5 ),
        .I4(Q[0]),
        .I5(\Dout_reg[3]_6 ),
        .O(\Dout[3]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \Dout[3]_i_2__0 
       (.I0(ALUControl_in[2]),
        .I1(ALUControl_in[3]),
        .I2(Q[1]),
        .O(\Dout[3]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[3]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [3]),
        .I3(\Dout_reg[0]_7 [3]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h88A888AA88A88888)) 
    \Dout[3]_i_4 
       (.I0(\Dout[30]_i_5_n_0 ),
        .I1(\Dout[3]_i_9_n_0 ),
        .I2(\Dout_reg[3]_7 [2]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(\Dout_reg[3]_8 [2]),
        .O(\Dout[3]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hE800)) 
    \Dout[3]_i_9 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_6 [3]),
        .I2(\Dout_reg[0]_7 [3]),
        .I3(Q[1]),
        .O(\Dout[3]_i_9_n_0 ));
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "CRITICAL_CELL_OPT" *) 
  LUT6 #(
    .INIT(64'hFFFFFFFEFEFEFEFE)) 
    \Dout[4]_i_1 
       (.I0(\Dout[4]_i_2_n_0 ),
        .I1(\Dout[4]_i_3_n_0 ),
        .I2(\Dout[4]_i_4_n_0 ),
        .I3(\Dout_reg[4] ),
        .I4(\Dout_reg[4]_0 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[4]));
  LUT6 #(
    .INIT(64'hAAAAAAAAA8080000)) 
    \Dout[4]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[3]_5 ),
        .I2(\Dout_reg[5]_2 ),
        .I3(\Dout_reg[5]_1 ),
        .I4(Q[0]),
        .I5(\Dout_reg[4]_1 ),
        .O(\Dout[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[4]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [4]),
        .I3(\Dout_reg[0]_7 [4]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h88A888AA88A88888)) 
    \Dout[4]_i_4 
       (.I0(\Dout[30]_i_5_n_0 ),
        .I1(\Dout[4]_i_9_n_0 ),
        .I2(\Dout_reg[3]_7 [3]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(\Dout_reg[3]_8 [3]),
        .O(\Dout[4]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hE800)) 
    \Dout[4]_i_9 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_6 [4]),
        .I2(\Dout_reg[31]_2[4]_repN_alias ),
        .I3(Q[1]),
        .O(\Dout[4]_i_9_n_0 ));
  (* PHYS_OPT_MODIFIED = "PLACEMENT_OPT" *) 
  LUT6 #(
    .INIT(64'hFFFFFFFEFEFEFEFE)) 
    \Dout[5]_i_1 
       (.I0(\Dout[5]_i_2_n_0 ),
        .I1(\Dout[5]_i_3_n_0 ),
        .I2(\Dout[5]_i_4_n_0 ),
        .I3(\Dout_reg[5] ),
        .I4(\Dout_reg[5]_0 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[5]));
  LUT4 #(
    .INIT(16'hE800)) 
    \Dout[5]_i_10 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_6 [5]),
        .I2(\Dout_reg[0]_7 [5]),
        .I3(Q[1]),
        .O(\Dout[5]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAA8080000)) 
    \Dout[5]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[5]_1 ),
        .I2(\Dout_reg[5]_2 ),
        .I3(\Dout_reg[5]_3 ),
        .I4(Q[0]),
        .I5(\Dout_reg[5]_4 ),
        .O(\Dout[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[5]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [5]),
        .I3(\Dout_reg[0]_7 [5]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h88A888AA88A88888)) 
    \Dout[5]_i_4 
       (.I0(\Dout[30]_i_5_n_0 ),
        .I1(\Dout[5]_i_10_n_0 ),
        .I2(\Dout_reg[3]_7 [4]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(\Dout_reg[3]_8 [4]),
        .O(\Dout[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[6]_i_1 
       (.I0(\Dout[6]_i_2_n_0 ),
        .I1(\Dout[6]_i_3_n_0 ),
        .I2(\Dout_reg[6]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[6] ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[6]));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[6]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[6]_1 ),
        .I2(Q[0]),
        .I3(\Dout_reg[6]_2 ),
        .I4(\Dout_reg[6]_3 ),
        .I5(\Dout_reg[5]_2 ),
        .O(\Dout[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[6]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [6]),
        .I3(\Dout_reg[0]_7 [6]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[6]_i_3_n_0 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'hF8F8FFFFF8F8FFF8)) 
    \Dout[7]_i_1_comp 
       (.I0(\Dout[30]_i_5_n_0 ),
        .I1(\Dout_reg[7]_1 ),
        .I2(\Dout_reg[7]_0 ),
        .I3(\Dout_reg[0]_36_alias ),
        .I4(\Dout_reg[0]_2_repN_alias ),
        .I5(\Dout_reg[0]_2_repN_1_alias ),
        .O(ALUResult_in[7]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[7]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [7]),
        .I3(\Dout_reg[0]_7 [7]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[7]_i_3_n_0 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[8]_i_1 
       (.I0(\Dout[8]_i_2_n_0 ),
        .I1(\Dout[8]_i_3_n_0 ),
        .I2(\Dout_reg[8] ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[8]_0 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[8]));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[8]_i_2 
       (.I0(\Dout_reg[3]_1 ),
        .I1(\Dout_reg[8]_1 ),
        .I2(Q[0]),
        .I3(\Dout_reg[8]_2 ),
        .I4(\Dout_reg[8]_3 ),
        .I5(\Dout_reg[5]_2 ),
        .O(\Dout[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[8]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [8]),
        .I3(\Dout_reg[0]_7 [8]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \Dout[9]_i_1 
       (.I0(\Dout_reg[9] ),
        .I1(\Dout[9]_i_3_n_0 ),
        .I2(\Dout_reg[9]_0 ),
        .I3(\Dout[30]_i_5_n_0 ),
        .I4(\Dout_reg[9]_1 ),
        .I5(\Dout_reg[3]_0 ),
        .O(ALUResult_in[9]));
  LUT6 #(
    .INIT(64'h0440044000444400)) 
    \Dout[9]_i_3 
       (.I0(ALUControl_in[3]),
        .I1(ALUControl_in[2]),
        .I2(\Dout_reg[0]_6 [9]),
        .I3(\Dout_reg[0]_7 [9]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\Dout[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[3]_10 [0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[3]_10 [1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[3]_10 [2]),
        .Q(ALUControl_in[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[3]_10 [3]),
        .Q(ALUControl_in[3]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_1
   (\Dout_reg[0]_0 ,
    ALUResult_OBUF,
    Q,
    SR,
    ALUResult_in,
    CLK_IBUF_BUFG,
    D);
  output \Dout_reg[0]_0 ;
  output [0:0]ALUResult_OBUF;
  output [2:0]Q;
  input [0:0]SR;
  input [0:0]ALUResult_in;
  input CLK_IBUF_BUFG;
  input [2:0]D;

  wire [0:0]ALUResult_OBUF;
  wire [0:0]ALUResult_in;
  wire CLK_IBUF_BUFG;
  wire [2:0]D;
  wire \Dout_reg[0]_0 ;
  wire [2:0]Q;
  wire [0:0]SR;

  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_in),
        .Q(\Dout_reg[0]_0 ),
        .R(SR));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_in),
        .Q(ALUResult_OBUF),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n__parameterized1
   (CO,
    DI,
    Q,
    \Dout_reg[3]_0 ,
    \Dout_reg[7]_0 ,
    \Dout_reg[11]_0 ,
    \Dout_reg[15]_0 ,
    \Dout_reg[19]_0 ,
    \Dout_reg[23]_0 ,
    \Dout_reg[27]_0 ,
    \Dout_reg[31]_0 ,
    \Dout_reg[3]_1 ,
    S0_carry__6,
    SR,
    E,
    D,
    CLK_IBUF_BUFG,
    \Dout_reg[31]_2[25]_repN_alias ,
    \Dout_reg[31]_2[17]_repN_alias ,
    \Dout_reg[31]_2[19]_repN_1_alias ,
    \Dout_reg[31]_2[30]_repN_alias ,
    \Dout_reg[31]_2[28]_repN_3_alias ,
    \Dout_reg[31]_2[29]_repN_alias ,
    \Dout_reg[31]_2[30]_repN_4_alias ,
    \Dout_reg[31]_2[2]_repN_alias ,
    \Dout_reg[31]_2[27]_repN_1_alias );
  output [0:0]CO;
  output [0:0]DI;
  output [31:0]Q;
  output [3:0]\Dout_reg[3]_0 ;
  output [3:0]\Dout_reg[7]_0 ;
  output [3:0]\Dout_reg[11]_0 ;
  output [3:0]\Dout_reg[15]_0 ;
  output [3:0]\Dout_reg[19]_0 ;
  output [3:0]\Dout_reg[23]_0 ;
  output [3:0]\Dout_reg[27]_0 ;
  output [3:0]\Dout_reg[31]_0 ;
  input [0:0]\Dout_reg[3]_1 ;
  input [31:0]S0_carry__6;
  input [0:0]SR;
  input [0:0]E;
  input [31:0]D;
  input CLK_IBUF_BUFG;
  input \Dout_reg[31]_2[25]_repN_alias ;
  input \Dout_reg[31]_2[17]_repN_alias ;
  input \Dout_reg[31]_2[19]_repN_1_alias ;
  input \Dout_reg[31]_2[30]_repN_alias ;
  input \Dout_reg[31]_2[28]_repN_3_alias ;
  input \Dout_reg[31]_2[29]_repN_alias ;
  input \Dout_reg[31]_2[30]_repN_4_alias ;
  input \Dout_reg[31]_2[2]_repN_alias ;
  input \Dout_reg[31]_2[27]_repN_1_alias ;

  wire CLK_IBUF_BUFG;
  wire [0:0]CO;
  wire [31:0]D;
  wire [0:0]DI;
  wire [3:0]\Dout_reg[11]_0 ;
  wire [3:0]\Dout_reg[15]_0 ;
  wire [3:0]\Dout_reg[19]_0 ;
  wire [3:0]\Dout_reg[23]_0 ;
  wire [3:0]\Dout_reg[27]_0 ;
  wire [3:0]\Dout_reg[31]_0 ;
  wire \Dout_reg[31]_2[17]_repN_alias ;
  wire \Dout_reg[31]_2[19]_repN_1_alias ;
  wire \Dout_reg[31]_2[25]_repN_alias ;
  wire \Dout_reg[31]_2[27]_repN_1_alias ;
  wire \Dout_reg[31]_2[28]_repN_3_alias ;
  wire \Dout_reg[31]_2[29]_repN_alias ;
  wire \Dout_reg[31]_2[2]_repN_alias ;
  wire \Dout_reg[31]_2[30]_repN_4_alias ;
  wire \Dout_reg[31]_2[30]_repN_alias ;
  wire [3:0]\Dout_reg[3]_0 ;
  wire [0:0]\Dout_reg[3]_1 ;
  wire [3:0]\Dout_reg[7]_0 ;
  wire [0:0]E;
  wire [31:0]Q;
  wire [31:0]S0_carry__6;
  wire [0:0]SR;
  wire [3:1]\NLW_Dout_reg[3]_i_3_CO_UNCONNECTED ;
  wire [3:0]\NLW_Dout_reg[3]_i_3_O_UNCONNECTED ;

  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[11]),
        .Q(Q[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[15]),
        .Q(Q[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[16]),
        .Q(Q[16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[17]),
        .Q(Q[17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[18]),
        .Q(Q[18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[19]),
        .Q(Q[19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[20]),
        .Q(Q[20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[21]),
        .Q(Q[21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[22]),
        .Q(Q[22]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[23]),
        .Q(Q[23]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[24]),
        .Q(Q[24]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[25]),
        .Q(Q[25]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[26]),
        .Q(Q[26]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[27]),
        .Q(Q[27]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[28]),
        .Q(Q[28]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[29]),
        .Q(Q[29]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[30]),
        .Q(Q[30]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[31]),
        .Q(Q[31]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(Q[3]),
        .R(SR));
  CARRY4 \Dout_reg[3]_i_3 
       (.CI(\Dout_reg[3]_1 ),
        .CO({\NLW_Dout_reg[3]_i_3_CO_UNCONNECTED [3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_Dout_reg[3]_i_3_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[7]),
        .Q(Q[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[9]),
        .Q(Q[9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__0_i_1
       (.I0(Q[7]),
        .I1(S0_carry__6[7]),
        .O(\Dout_reg[7]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__0_i_2
       (.I0(Q[6]),
        .I1(S0_carry__6[6]),
        .O(\Dout_reg[7]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__0_i_3
       (.I0(Q[5]),
        .I1(S0_carry__6[5]),
        .O(\Dout_reg[7]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__0_i_4
       (.I0(Q[4]),
        .I1(S0_carry__6[4]),
        .O(\Dout_reg[7]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__1_i_1
       (.I0(Q[11]),
        .I1(S0_carry__6[11]),
        .O(\Dout_reg[11]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__1_i_2
       (.I0(Q[10]),
        .I1(S0_carry__6[10]),
        .O(\Dout_reg[11]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__1_i_3
       (.I0(Q[9]),
        .I1(S0_carry__6[9]),
        .O(\Dout_reg[11]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__1_i_4
       (.I0(Q[8]),
        .I1(S0_carry__6[8]),
        .O(\Dout_reg[11]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__2_i_1
       (.I0(Q[15]),
        .I1(S0_carry__6[15]),
        .O(\Dout_reg[15]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__2_i_2
       (.I0(Q[14]),
        .I1(S0_carry__6[14]),
        .O(\Dout_reg[15]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__2_i_3
       (.I0(Q[13]),
        .I1(S0_carry__6[13]),
        .O(\Dout_reg[15]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__2_i_4
       (.I0(Q[12]),
        .I1(S0_carry__6[12]),
        .O(\Dout_reg[15]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__3_i_1
       (.I0(Q[19]),
        .I1(\Dout_reg[31]_2[19]_repN_1_alias ),
        .O(\Dout_reg[19]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__3_i_2
       (.I0(Q[18]),
        .I1(S0_carry__6[18]),
        .O(\Dout_reg[19]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__3_i_3
       (.I0(Q[17]),
        .I1(\Dout_reg[31]_2[17]_repN_alias ),
        .O(\Dout_reg[19]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__3_i_4
       (.I0(Q[16]),
        .I1(S0_carry__6[16]),
        .O(\Dout_reg[19]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__4_i_1
       (.I0(Q[23]),
        .I1(S0_carry__6[23]),
        .O(\Dout_reg[23]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__4_i_2
       (.I0(Q[22]),
        .I1(S0_carry__6[22]),
        .O(\Dout_reg[23]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__4_i_3
       (.I0(Q[21]),
        .I1(S0_carry__6[21]),
        .O(\Dout_reg[23]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__4_i_4
       (.I0(Q[20]),
        .I1(S0_carry__6[20]),
        .O(\Dout_reg[23]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__5_i_1
       (.I0(Q[27]),
        .I1(\Dout_reg[31]_2[27]_repN_1_alias ),
        .O(\Dout_reg[27]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__5_i_2
       (.I0(Q[26]),
        .I1(S0_carry__6[26]),
        .O(\Dout_reg[27]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__5_i_3
       (.I0(Q[25]),
        .I1(\Dout_reg[31]_2[25]_repN_alias ),
        .O(\Dout_reg[27]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__5_i_4
       (.I0(Q[24]),
        .I1(S0_carry__6[24]),
        .O(\Dout_reg[27]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    S0_carry__6_i_1
       (.I0(Q[31]),
        .O(DI));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__6_i_2__0
       (.I0(Q[31]),
        .I1(S0_carry__6[31]),
        .O(\Dout_reg[31]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__6_i_3
       (.I0(Q[30]),
        .I1(\Dout_reg[31]_2[30]_repN_4_alias ),
        .O(\Dout_reg[31]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__6_i_4
       (.I0(Q[29]),
        .I1(\Dout_reg[31]_2[29]_repN_alias ),
        .O(\Dout_reg[31]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry__6_i_5
       (.I0(Q[28]),
        .I1(\Dout_reg[31]_2[28]_repN_3_alias ),
        .O(\Dout_reg[31]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry_i_1
       (.I0(Q[3]),
        .I1(S0_carry__6[3]),
        .O(\Dout_reg[3]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry_i_2
       (.I0(Q[2]),
        .I1(\Dout_reg[31]_2[2]_repN_alias ),
        .O(\Dout_reg[3]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry_i_3
       (.I0(Q[1]),
        .I1(S0_carry__6[1]),
        .O(\Dout_reg[3]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    S0_carry_i_4
       (.I0(Q[0]),
        .I1(S0_carry__6[0]),
        .O(\Dout_reg[3]_0 [0]));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n__parameterized1_0
   (\Dout_reg[31]_0 ,
    O,
    \Dout_reg[31]_1 ,
    \Dout_reg[0]_0 ,
    \Dout_reg[1]_0 ,
    \Dout_reg[1]_1 ,
    \Dout_reg[0]_1 ,
    \Dout_reg[1]_2 ,
    \Dout_reg[31]_2 ,
    \Dout_reg[0]_2 ,
    \Dout_reg[0]_3 ,
    \Dout_reg[0]_4 ,
    \Dout_reg[0]_5 ,
    \Dout_reg[0]_6 ,
    \Dout_reg[0]_7 ,
    \Dout_reg[0]_8 ,
    \Dout_reg[0]_9 ,
    \Dout_reg[0]_10 ,
    \Dout_reg[0]_11 ,
    \Dout_reg[0]_12 ,
    \Dout_reg[0]_13 ,
    \Dout_reg[0]_14 ,
    \Dout_reg[0]_15 ,
    \Dout_reg[0]_16 ,
    \Dout_reg[0]_17 ,
    \Dout_reg[0]_18 ,
    \Dout_reg[0]_19 ,
    \Dout_reg[0]_20 ,
    \Dout_reg[0]_21 ,
    \Dout_reg[0]_22 ,
    \Dout_reg[9]_0 ,
    \Dout_reg[10]_0 ,
    \Dout_reg[0]_23 ,
    \Dout_reg[12]_0 ,
    \Dout_reg[11]_0 ,
    \Dout_reg[25]_0 ,
    \Dout_reg[13]_0 ,
    \Dout_reg[27]_0 ,
    \Dout_reg[14]_0 ,
    \Dout_reg[15]_0 ,
    \Dout_reg[29]_0 ,
    \Dout_reg[30]_0 ,
    \Dout_reg[31]_3 ,
    \Dout_reg[2]_0 ,
    \Dout_reg[0]_24 ,
    \Dout_reg[0]_25 ,
    \Dout_reg[0]_26 ,
    \Dout_reg[0]_27 ,
    \Dout_reg[3]_0 ,
    \Dout_reg[0]_28 ,
    \Dout_reg[0]_29 ,
    \Dout_reg[1]_3 ,
    \Dout_reg[0]_30 ,
    \Dout_reg[0]_31 ,
    \Dout_reg[0]_32 ,
    \Dout_reg[0]_33 ,
    \Dout_reg[1]_4 ,
    \Dout_reg[0]_34 ,
    \Dout_reg[1]_5 ,
    \Dout_reg[1]_6 ,
    \Dout_reg[0]_35 ,
    \Dout_reg[0]_36 ,
    \Dout_reg[0]_37 ,
    \Dout_reg[0]_38 ,
    \Dout_reg[0]_39 ,
    \Dout_reg[0]_40 ,
    \Dout_reg[0]_41 ,
    \Dout_reg[0]_42 ,
    \Dout_reg[0]_43 ,
    \Dout_reg[0]_44 ,
    \Dout_reg[0]_45 ,
    \Dout_reg[1]_7 ,
    \Dout_reg[29]_1 ,
    \Dout_reg[1]_8 ,
    \Dout_reg[0]_46 ,
    \Dout_reg[1]_9 ,
    \Dout_reg[1]_10 ,
    \Dout_reg[31]_4 ,
    \Dout_reg[2]_1 ,
    \Dout_reg[1]_11 ,
    \Dout_reg[29]_2 ,
    \Dout_reg[28]_0 ,
    \Dout_reg[1]_12 ,
    \Dout_reg[1]_13 ,
    \Dout_reg[30]_1 ,
    \Dout_reg[29]_3 ,
    S,
    \Dout_reg[7]_0 ,
    \Dout_reg[11]_1 ,
    \Dout_reg[15]_1 ,
    \Dout_reg[19]_0 ,
    \Dout_reg[23]_0 ,
    \Dout_reg[27]_1 ,
    \Dout_reg[31]_5 ,
    \Dout_reg[2]_2 ,
    \Dout_reg[2]_3 ,
    \Dout_reg[2]_4 ,
    CO,
    \Dout_reg[2]_5 ,
    Q,
    \Dout_reg[4]_0 ,
    \Dout_reg[0]_47 ,
    ROTATE_RIGHT1,
    \Dout[16]_i_9_0 ,
    \Dout_reg[6]_0 ,
    \Dout[1]_i_5 ,
    \Dout[1]_i_5_0 ,
    \Dout[1]_i_5_1 ,
    \Dout_reg[8]_0 ,
    \Dout_reg[23]_1 ,
    \Dout_reg[26]_0 ,
    \Dout[16]_i_9_1 ,
    \Dout_reg[9]_1 ,
    \Dout[25]_i_6 ,
    \Dout[1]_i_16 ,
    \Dout[1]_i_14 ,
    \Dout_reg[0]_48 ,
    \Dout_reg[2]_i_2_0 ,
    SR,
    E,
    D,
    CLK_IBUF_BUFG,
    \Dout_reg[31]_2[31]_repN_alias ,
    \Dout_reg[31]_2[31]_repN_1_alias ,
    \Q[3]_repN_alias ,
    \Q[3]_repN_1_alias ,
    \Q[3]_repN_2_alias ,
    \Q[3]_repN_3_alias ,
    \Q[3]_repN_6_alias ,
    \Dout_reg[31]_2[25]_repN_alias ,
    \Dout_reg[31]_2[25]_repN_3_alias ,
    \Q[3]_repN_7_alias ,
    \Q[3]_repN_8_alias ,
    \Dout_reg[31]_2[17]_repN_alias ,
    \Dout_reg[31]_2[4]_repN_alias ,
    \Q[4]_repN_alias ,
    \Q[4]_repN_1_alias ,
    \Q[4]_repN_2_alias ,
    \Q[4]_repN_3_alias ,
    \Q[4]_repN_5_alias ,
    \Dout_reg[31]_2[28]_repN_alias ,
    \Dout_reg[31]_2[19]_repN_1_alias ,
    \Q[2]_repN_alias ,
    \Q[2]_repN_1_alias ,
    \Q[2]_repN_2_alias ,
    \Q[2]_repN_3_alias ,
    \Q[2]_repN_4_alias ,
    \Q[2]_repN_5_alias ,
    \Q[2]_repN_6_alias ,
    \Q[2]_repN_7_alias ,
    \Dout_reg[31]_2[30]_repN_alias ,
    \Dout_reg[31]_2[30]_repN_1_alias ,
    \Dout_reg[31]_2[30]_repN_2_alias ,
    \Q[1]_repN_alias ,
    \Q[1]_repN_1_alias ,
    \Q[1]_repN_2_alias ,
    \Q[1]_repN_3_alias ,
    \Q[1]_repN_5_alias ,
    \Q[1]_repN_6_alias ,
    \Q[1]_repN_7_alias ,
    \Dout_reg[31]_2[28]_repN_3_alias ,
    \Dout[2]_i_3_n_0_alias ,
    \Dout[3]_i_10_n_0_alias ,
    \Dout_reg[0]_30_repN_alias ,
    \Dout_reg[31]_2[29]_repN_alias ,
    \Dout_reg[31]_2[29]_repN_1_alias ,
    \Dout_reg[31]_2[30]_repN_3_alias ,
    \Dout_reg[31]_2[30]_repN_4_alias ,
    \Dout[14]_i_3_n_0_alias ,
    \Dout[14]_i_8_n_0_alias ,
    \Dout_reg[0]_13_repN_alias ,
    \Dout_reg[31]_2[2]_repN_alias ,
    \Dout_reg[31]_2[27]_repN_alias ,
    \Dout_reg[31]_2[27]_repN_1_alias ,
    \Dout[13]_i_3_n_0_alias ,
    \Dout[13]_i_8_n_0_alias ,
    \Dout_reg[0]_12_repN_alias ,
    \Dout_reg[31]_2[26]_repN_alias );
  output [0:0]\Dout_reg[31]_0 ;
  output [0:0]O;
  output [0:0]\Dout_reg[31]_1 ;
  output \Dout_reg[0]_0 ;
  output \Dout_reg[1]_0 ;
  output \Dout_reg[1]_1 ;
  output \Dout_reg[0]_1 ;
  output \Dout_reg[1]_2 ;
  output [31:0]\Dout_reg[31]_2 ;
  output \Dout_reg[0]_2 ;
  output \Dout_reg[0]_3 ;
  output \Dout_reg[0]_4 ;
  output \Dout_reg[0]_5 ;
  output \Dout_reg[0]_6 ;
  output \Dout_reg[0]_7 ;
  output \Dout_reg[0]_8 ;
  output \Dout_reg[0]_9 ;
  output \Dout_reg[0]_10 ;
  output \Dout_reg[0]_11 ;
  output \Dout_reg[0]_12 ;
  output \Dout_reg[0]_13 ;
  output \Dout_reg[0]_14 ;
  output \Dout_reg[0]_15 ;
  output \Dout_reg[0]_16 ;
  output \Dout_reg[0]_17 ;
  output \Dout_reg[0]_18 ;
  output \Dout_reg[0]_19 ;
  output \Dout_reg[0]_20 ;
  output \Dout_reg[0]_21 ;
  output \Dout_reg[0]_22 ;
  output \Dout_reg[9]_0 ;
  output \Dout_reg[10]_0 ;
  output \Dout_reg[0]_23 ;
  output \Dout_reg[12]_0 ;
  output \Dout_reg[11]_0 ;
  output \Dout_reg[25]_0 ;
  output \Dout_reg[13]_0 ;
  output \Dout_reg[27]_0 ;
  output \Dout_reg[14]_0 ;
  output \Dout_reg[15]_0 ;
  output \Dout_reg[29]_0 ;
  output \Dout_reg[30]_0 ;
  output \Dout_reg[31]_3 ;
  output \Dout_reg[2]_0 ;
  output \Dout_reg[0]_24 ;
  output \Dout_reg[0]_25 ;
  output \Dout_reg[0]_26 ;
  output \Dout_reg[0]_27 ;
  output \Dout_reg[3]_0 ;
  output \Dout_reg[0]_28 ;
  output \Dout_reg[0]_29 ;
  output \Dout_reg[1]_3 ;
  output \Dout_reg[0]_30 ;
  output \Dout_reg[0]_31 ;
  output \Dout_reg[0]_32 ;
  output \Dout_reg[0]_33 ;
  output \Dout_reg[1]_4 ;
  output \Dout_reg[0]_34 ;
  output \Dout_reg[1]_5 ;
  output \Dout_reg[1]_6 ;
  output \Dout_reg[0]_35 ;
  output \Dout_reg[0]_36 ;
  output \Dout_reg[0]_37 ;
  output \Dout_reg[0]_38 ;
  output \Dout_reg[0]_39 ;
  output \Dout_reg[0]_40 ;
  output \Dout_reg[0]_41 ;
  output \Dout_reg[0]_42 ;
  output \Dout_reg[0]_43 ;
  output \Dout_reg[0]_44 ;
  output \Dout_reg[0]_45 ;
  output \Dout_reg[1]_7 ;
  output \Dout_reg[29]_1 ;
  output \Dout_reg[1]_8 ;
  output \Dout_reg[0]_46 ;
  output \Dout_reg[1]_9 ;
  output \Dout_reg[1]_10 ;
  output \Dout_reg[31]_4 ;
  output \Dout_reg[2]_1 ;
  output \Dout_reg[1]_11 ;
  output \Dout_reg[29]_2 ;
  output \Dout_reg[28]_0 ;
  output \Dout_reg[1]_12 ;
  output \Dout_reg[1]_13 ;
  output \Dout_reg[30]_1 ;
  output \Dout_reg[29]_3 ;
  output [3:0]S;
  output [3:0]\Dout_reg[7]_0 ;
  output [3:0]\Dout_reg[11]_1 ;
  output [3:0]\Dout_reg[15]_1 ;
  output [3:0]\Dout_reg[19]_0 ;
  output [3:0]\Dout_reg[23]_0 ;
  output [3:0]\Dout_reg[27]_1 ;
  output [3:0]\Dout_reg[31]_5 ;
  output \Dout_reg[2]_2 ;
  output \Dout_reg[2]_3 ;
  output \Dout_reg[2]_4 ;
  input [0:0]CO;
  input [0:0]\Dout_reg[2]_5 ;
  input [4:0]Q;
  input \Dout_reg[4]_0 ;
  input [1:0]\Dout_reg[0]_47 ;
  input [3:0]ROTATE_RIGHT1;
  input \Dout[16]_i_9_0 ;
  input \Dout_reg[6]_0 ;
  input \Dout[1]_i_5 ;
  input \Dout[1]_i_5_0 ;
  input \Dout[1]_i_5_1 ;
  input \Dout_reg[8]_0 ;
  input \Dout_reg[23]_1 ;
  input \Dout_reg[26]_0 ;
  input \Dout[16]_i_9_1 ;
  input \Dout_reg[9]_1 ;
  input \Dout[25]_i_6 ;
  input \Dout[1]_i_16 ;
  input \Dout[1]_i_14 ;
  input \Dout_reg[0]_48 ;
  input [31:0]\Dout_reg[2]_i_2_0 ;
  input [0:0]SR;
  input [0:0]E;
  input [31:0]D;
  input CLK_IBUF_BUFG;
  output \Dout_reg[31]_2[31]_repN_alias ;
  output \Dout_reg[31]_2[31]_repN_1_alias ;
  input \Q[3]_repN_alias ;
  input \Q[3]_repN_1_alias ;
  input \Q[3]_repN_2_alias ;
  input \Q[3]_repN_3_alias ;
  input \Q[3]_repN_6_alias ;
  output \Dout_reg[31]_2[25]_repN_alias ;
  output \Dout_reg[31]_2[25]_repN_3_alias ;
  input \Q[3]_repN_7_alias ;
  input \Q[3]_repN_8_alias ;
  output \Dout_reg[31]_2[17]_repN_alias ;
  output \Dout_reg[31]_2[4]_repN_alias ;
  input \Q[4]_repN_alias ;
  input \Q[4]_repN_1_alias ;
  input \Q[4]_repN_2_alias ;
  input \Q[4]_repN_3_alias ;
  input \Q[4]_repN_5_alias ;
  output \Dout_reg[31]_2[28]_repN_alias ;
  output \Dout_reg[31]_2[19]_repN_1_alias ;
  input \Q[2]_repN_alias ;
  input \Q[2]_repN_1_alias ;
  input \Q[2]_repN_2_alias ;
  input \Q[2]_repN_3_alias ;
  input \Q[2]_repN_4_alias ;
  input \Q[2]_repN_5_alias ;
  input \Q[2]_repN_6_alias ;
  input \Q[2]_repN_7_alias ;
  output \Dout_reg[31]_2[30]_repN_alias ;
  output \Dout_reg[31]_2[30]_repN_1_alias ;
  output \Dout_reg[31]_2[30]_repN_2_alias ;
  input \Q[1]_repN_alias ;
  input \Q[1]_repN_1_alias ;
  input \Q[1]_repN_2_alias ;
  input \Q[1]_repN_3_alias ;
  input \Q[1]_repN_5_alias ;
  input \Q[1]_repN_6_alias ;
  input \Q[1]_repN_7_alias ;
  output \Dout_reg[31]_2[28]_repN_3_alias ;
  input \Dout[2]_i_3_n_0_alias ;
  output \Dout[3]_i_10_n_0_alias ;
  output \Dout_reg[0]_30_repN_alias ;
  output \Dout_reg[31]_2[29]_repN_alias ;
  output \Dout_reg[31]_2[29]_repN_1_alias ;
  output \Dout_reg[31]_2[30]_repN_3_alias ;
  output \Dout_reg[31]_2[30]_repN_4_alias ;
  input \Dout[14]_i_3_n_0_alias ;
  output \Dout[14]_i_8_n_0_alias ;
  output \Dout_reg[0]_13_repN_alias ;
  output \Dout_reg[31]_2[2]_repN_alias ;
  output \Dout_reg[31]_2[27]_repN_alias ;
  output \Dout_reg[31]_2[27]_repN_1_alias ;
  input \Dout[13]_i_3_n_0_alias ;
  output \Dout[13]_i_8_n_0_alias ;
  output \Dout_reg[0]_12_repN_alias ;
  output \Dout_reg[31]_2[26]_repN_alias ;

  wire CLK_IBUF_BUFG;
  wire [0:0]CO;
  wire [31:0]D;
  wire \Dout[0]_i_12__0_n_0 ;
  wire \Dout[0]_i_12_n_0 ;
  wire \Dout[10]_i_10_n_0 ;
  wire \Dout[10]_i_11_n_0 ;
  wire \Dout[10]_i_6_n_0 ;
  wire \Dout[10]_i_7_n_0 ;
  wire \Dout[10]_i_8_n_0 ;
  wire \Dout[10]_i_9_n_0 ;
  wire \Dout[11]_i_10_n_0 ;
  wire \Dout[11]_i_11_n_0 ;
  wire \Dout[11]_i_12_n_0 ;
  wire \Dout[11]_i_13_n_0 ;
  wire \Dout[11]_i_6_n_0 ;
  wire \Dout[11]_i_7_n_0 ;
  wire \Dout[11]_i_8_n_0 ;
  wire \Dout[11]_i_9_n_0 ;
  wire \Dout[12]_i_10_n_0 ;
  wire \Dout[12]_i_11_n_0 ;
  wire \Dout[12]_i_12_n_0 ;
  wire \Dout[12]_i_13_n_0 ;
  wire \Dout[12]_i_6_n_0 ;
  wire \Dout[12]_i_7_n_0 ;
  wire \Dout[12]_i_8_n_0 ;
  wire \Dout[12]_i_9_n_0 ;
  wire \Dout[13]_i_10_n_0 ;
  wire \Dout[13]_i_11_n_0 ;
  wire \Dout[13]_i_12_n_0 ;
  wire \Dout[13]_i_13_n_0 ;
  wire \Dout[13]_i_14_n_0 ;
  wire \Dout[13]_i_15_n_0 ;
  wire \Dout[13]_i_3_n_0_alias ;
  wire \Dout[13]_i_6_n_0 ;
  wire \Dout[13]_i_7_n_0 ;
  wire \Dout[13]_i_8_n_0 ;
  wire \Dout[13]_i_9_n_0 ;
  wire \Dout[14]_i_10_n_0 ;
  wire \Dout[14]_i_11_n_0 ;
  wire \Dout[14]_i_12_n_0 ;
  wire \Dout[14]_i_13_n_0 ;
  wire \Dout[14]_i_14_n_0 ;
  wire \Dout[14]_i_15_n_0 ;
  wire \Dout[14]_i_3_n_0_alias ;
  wire \Dout[14]_i_6_n_0 ;
  wire \Dout[14]_i_7_n_0 ;
  wire \Dout[14]_i_8_n_0 ;
  wire \Dout[14]_i_9_n_0 ;
  wire \Dout[15]_i_10_n_0 ;
  wire \Dout[15]_i_11_n_0 ;
  wire \Dout[15]_i_12_n_0 ;
  wire \Dout[15]_i_13_n_0 ;
  wire \Dout[15]_i_14_n_0 ;
  wire \Dout[15]_i_15_n_0 ;
  wire \Dout[15]_i_16_n_0 ;
  wire \Dout[15]_i_6_n_0 ;
  wire \Dout[15]_i_7_n_0 ;
  wire \Dout[15]_i_8_n_0 ;
  wire \Dout[15]_i_9_n_0 ;
  wire \Dout[16]_i_10_n_0 ;
  wire \Dout[16]_i_11_n_0 ;
  wire \Dout[16]_i_12_n_0 ;
  wire \Dout[16]_i_13_n_0 ;
  wire \Dout[16]_i_14_n_0 ;
  wire \Dout[16]_i_15_n_0 ;
  wire \Dout[16]_i_16_n_0 ;
  wire \Dout[16]_i_17_n_0 ;
  wire \Dout[16]_i_6_n_0 ;
  wire \Dout[16]_i_7_n_0 ;
  wire \Dout[16]_i_8_n_0 ;
  wire \Dout[16]_i_9_0 ;
  wire \Dout[16]_i_9_1 ;
  wire \Dout[16]_i_9_n_0 ;
  wire \Dout[17]_i_10_n_0 ;
  wire \Dout[17]_i_11_n_0 ;
  wire \Dout[17]_i_12_n_0 ;
  wire \Dout[17]_i_13_n_0 ;
  wire \Dout[17]_i_14_n_0 ;
  wire \Dout[17]_i_15_n_0 ;
  wire \Dout[17]_i_16_n_0 ;
  wire \Dout[17]_i_17_n_0 ;
  wire \Dout[17]_i_6_n_0 ;
  wire \Dout[17]_i_7_n_0 ;
  wire \Dout[17]_i_8_n_0 ;
  wire \Dout[17]_i_9_n_0 ;
  wire \Dout[18]_i_10_n_0 ;
  wire \Dout[18]_i_11_n_0 ;
  wire \Dout[18]_i_12_n_0 ;
  wire \Dout[18]_i_13_n_0 ;
  wire \Dout[18]_i_14_n_0 ;
  wire \Dout[18]_i_15_n_0 ;
  wire \Dout[18]_i_16_n_0 ;
  wire \Dout[18]_i_17_n_0 ;
  wire \Dout[18]_i_6_n_0 ;
  wire \Dout[18]_i_7_n_0 ;
  wire \Dout[18]_i_8_n_0 ;
  wire \Dout[18]_i_9_n_0 ;
  wire \Dout[19]_i_10_n_0 ;
  wire \Dout[19]_i_11_n_0 ;
  wire \Dout[19]_i_12_n_0 ;
  wire \Dout[19]_i_13_n_0 ;
  wire \Dout[19]_i_14_n_0 ;
  wire \Dout[19]_i_15_n_0 ;
  wire \Dout[19]_i_16_n_0 ;
  wire \Dout[19]_i_17_n_0 ;
  wire \Dout[19]_i_6_n_0 ;
  wire \Dout[19]_i_7_n_0 ;
  wire \Dout[19]_i_8_n_0 ;
  wire \Dout[19]_i_9_n_0 ;
  wire \Dout[1]_i_11_n_0 ;
  wire \Dout[1]_i_14 ;
  wire \Dout[1]_i_16 ;
  wire \Dout[1]_i_5 ;
  wire \Dout[1]_i_5_0 ;
  wire \Dout[1]_i_5_1 ;
  wire \Dout[20]_i_10_n_0 ;
  wire \Dout[20]_i_11_n_0 ;
  wire \Dout[20]_i_12_n_0 ;
  wire \Dout[20]_i_13_n_0 ;
  wire \Dout[20]_i_14_n_0 ;
  wire \Dout[20]_i_15_n_0 ;
  wire \Dout[20]_i_16_n_0 ;
  wire \Dout[20]_i_6_n_0 ;
  wire \Dout[20]_i_7_n_0 ;
  wire \Dout[20]_i_8_n_0 ;
  wire \Dout[20]_i_9_n_0 ;
  wire \Dout[21]_i_10_n_0 ;
  wire \Dout[21]_i_11_n_0 ;
  wire \Dout[21]_i_12_n_0 ;
  wire \Dout[21]_i_13_n_0 ;
  wire \Dout[21]_i_14_n_0 ;
  wire \Dout[21]_i_15_n_0 ;
  wire \Dout[21]_i_16_n_0 ;
  wire \Dout[21]_i_6_n_0 ;
  wire \Dout[21]_i_7_n_0 ;
  wire \Dout[21]_i_8_n_0 ;
  wire \Dout[21]_i_9_n_0 ;
  wire \Dout[22]_i_10_n_0 ;
  wire \Dout[22]_i_11_n_0 ;
  wire \Dout[22]_i_12_n_0 ;
  wire \Dout[22]_i_13_n_0 ;
  wire \Dout[22]_i_14_n_0 ;
  wire \Dout[22]_i_15_n_0 ;
  wire \Dout[22]_i_6_n_0 ;
  wire \Dout[22]_i_7_n_0 ;
  wire \Dout[22]_i_8_n_0 ;
  wire \Dout[22]_i_9_n_0 ;
  wire \Dout[23]_i_11_n_0 ;
  wire \Dout[23]_i_12_n_0 ;
  wire \Dout[23]_i_13_n_0 ;
  wire \Dout[23]_i_8_n_0 ;
  wire \Dout[23]_i_9_n_0 ;
  wire \Dout[24]_i_11_n_0 ;
  wire \Dout[24]_i_13_n_0 ;
  wire \Dout[24]_i_15_n_0 ;
  wire \Dout[25]_i_11_n_0 ;
  wire \Dout[25]_i_13_n_0 ;
  wire \Dout[25]_i_6 ;
  wire \Dout[26]_i_11_n_0 ;
  wire \Dout[26]_i_13_n_0 ;
  wire \Dout[26]_i_6_n_0 ;
  wire \Dout[27]_i_11_n_0 ;
  wire \Dout[27]_i_15_n_0 ;
  wire \Dout[28]_i_11_n_0 ;
  wire \Dout[28]_i_14_n_0 ;
  wire \Dout[29]_i_11_n_0 ;
  wire \Dout[29]_i_13_n_0 ;
  wire \Dout[2]_i_10_n_0 ;
  wire \Dout[2]_i_12_n_0 ;
  wire \Dout[2]_i_3_n_0_alias ;
  wire \Dout[2]_i_4__0_n_0 ;
  wire \Dout[2]_i_5__0_n_0 ;
  wire \Dout[30]_i_16_n_0 ;
  wire \Dout[30]_i_17_n_0 ;
  wire \Dout[30]_i_18_n_0 ;
  wire \Dout[30]_i_19_n_0 ;
  wire \Dout[30]_i_20_n_0 ;
  wire \Dout[30]_i_21_n_0 ;
  wire \Dout[30]_i_22_n_0 ;
  wire \Dout[30]_i_23_n_0 ;
  wire \Dout[30]_i_25_n_0 ;
  wire \Dout[30]_i_31_n_0 ;
  wire \Dout[30]_i_32_n_0 ;
  wire \Dout[30]_i_33_n_0 ;
  wire \Dout[30]_i_34_n_0 ;
  wire \Dout[3]_i_10_n_0 ;
  wire \Dout[3]_i_12_n_0 ;
  wire \Dout[4]_i_10_n_0 ;
  wire \Dout[4]_i_12_n_0 ;
  wire \Dout[5]_i_11_n_0 ;
  wire \Dout[5]_i_11_n_0_repN ;
  wire \Dout[5]_i_12_n_0 ;
  wire \Dout[5]_i_13_n_0 ;
  wire \Dout[5]_i_14_n_0 ;
  wire \Dout[5]_i_15_n_0 ;
  wire \Dout[5]_i_16_n_0 ;
  wire \Dout[5]_i_17_n_0 ;
  wire \Dout[5]_i_18_n_0 ;
  wire \Dout[5]_i_19_n_0 ;
  wire \Dout[5]_i_20_n_0 ;
  wire \Dout[5]_i_21_n_0 ;
  wire \Dout[5]_i_26_n_0 ;
  wire \Dout[5]_i_27_n_0 ;
  wire \Dout[5]_i_28_n_0 ;
  wire \Dout[5]_i_29_n_0 ;
  wire \Dout[5]_i_30_n_0 ;
  wire \Dout[6]_i_8_n_0 ;
  wire \Dout[6]_i_9_n_0 ;
  wire \Dout[7]_i_10_n_0 ;
  wire \Dout[7]_i_12_n_0 ;
  wire \Dout[8]_i_10_n_0 ;
  wire \Dout[8]_i_12_n_0 ;
  wire \Dout[8]_i_8_n_0 ;
  wire \Dout[9]_i_10_n_0 ;
  wire \Dout[9]_i_11_n_0 ;
  wire \Dout[9]_i_6_n_0 ;
  wire \Dout[9]_i_8_n_0 ;
  wire \Dout[9]_i_9_n_0 ;
  wire \Dout_reg[0]_0 ;
  wire \Dout_reg[0]_1 ;
  wire \Dout_reg[0]_10 ;
  wire \Dout_reg[0]_11 ;
  wire \Dout_reg[0]_12 ;
  wire \Dout_reg[0]_12_repN ;
  wire \Dout_reg[0]_13 ;
  wire \Dout_reg[0]_13_repN ;
  wire \Dout_reg[0]_14 ;
  wire \Dout_reg[0]_15 ;
  wire \Dout_reg[0]_16 ;
  wire \Dout_reg[0]_17 ;
  wire \Dout_reg[0]_18 ;
  wire \Dout_reg[0]_19 ;
  wire \Dout_reg[0]_2 ;
  wire \Dout_reg[0]_20 ;
  wire \Dout_reg[0]_21 ;
  wire \Dout_reg[0]_22 ;
  wire \Dout_reg[0]_23 ;
  wire \Dout_reg[0]_24 ;
  wire \Dout_reg[0]_25 ;
  wire \Dout_reg[0]_26 ;
  wire \Dout_reg[0]_27 ;
  wire \Dout_reg[0]_28 ;
  wire \Dout_reg[0]_29 ;
  wire \Dout_reg[0]_3 ;
  wire \Dout_reg[0]_30 ;
  wire \Dout_reg[0]_30_repN ;
  wire \Dout_reg[0]_31 ;
  wire \Dout_reg[0]_32 ;
  wire \Dout_reg[0]_33 ;
  wire \Dout_reg[0]_34 ;
  wire \Dout_reg[0]_35 ;
  wire \Dout_reg[0]_36 ;
  wire \Dout_reg[0]_37 ;
  wire \Dout_reg[0]_38 ;
  wire \Dout_reg[0]_39 ;
  wire \Dout_reg[0]_4 ;
  wire \Dout_reg[0]_40 ;
  wire \Dout_reg[0]_41 ;
  wire \Dout_reg[0]_42 ;
  wire \Dout_reg[0]_43 ;
  wire \Dout_reg[0]_44 ;
  wire \Dout_reg[0]_45 ;
  wire \Dout_reg[0]_46 ;
  wire [1:0]\Dout_reg[0]_47 ;
  wire \Dout_reg[0]_48 ;
  wire \Dout_reg[0]_5 ;
  wire \Dout_reg[0]_6 ;
  wire \Dout_reg[0]_7 ;
  wire \Dout_reg[0]_8 ;
  wire \Dout_reg[0]_9 ;
  wire \Dout_reg[10]_0 ;
  wire \Dout_reg[11]_0 ;
  wire [3:0]\Dout_reg[11]_1 ;
  wire \Dout_reg[12]_0 ;
  wire \Dout_reg[13]_0 ;
  wire \Dout_reg[14]_0 ;
  wire \Dout_reg[15]_0 ;
  wire [3:0]\Dout_reg[15]_1 ;
  wire [3:0]\Dout_reg[19]_0 ;
  wire \Dout_reg[1]_0 ;
  wire \Dout_reg[1]_1 ;
  wire \Dout_reg[1]_10 ;
  wire \Dout_reg[1]_11 ;
  wire \Dout_reg[1]_12 ;
  wire \Dout_reg[1]_13 ;
  wire \Dout_reg[1]_2 ;
  wire \Dout_reg[1]_3 ;
  wire \Dout_reg[1]_4 ;
  wire \Dout_reg[1]_5 ;
  wire \Dout_reg[1]_6 ;
  wire \Dout_reg[1]_7 ;
  wire \Dout_reg[1]_8 ;
  wire \Dout_reg[1]_9 ;
  wire [3:0]\Dout_reg[23]_0 ;
  wire \Dout_reg[23]_1 ;
  wire \Dout_reg[25]_0 ;
  wire \Dout_reg[26]_0 ;
  wire \Dout_reg[27]_0 ;
  wire [3:0]\Dout_reg[27]_1 ;
  wire \Dout_reg[28]_0 ;
  wire \Dout_reg[29]_0 ;
  wire \Dout_reg[29]_1 ;
  wire \Dout_reg[29]_2 ;
  wire \Dout_reg[29]_3 ;
  wire \Dout_reg[2]_0 ;
  wire \Dout_reg[2]_1 ;
  wire \Dout_reg[2]_2 ;
  wire \Dout_reg[2]_3 ;
  wire \Dout_reg[2]_4 ;
  wire [0:0]\Dout_reg[2]_5 ;
  wire [31:0]\Dout_reg[2]_i_2_0 ;
  wire \Dout_reg[30]_0 ;
  wire \Dout_reg[30]_1 ;
  wire [0:0]\Dout_reg[31]_0 ;
  wire [0:0]\Dout_reg[31]_1 ;
  wire [31:0]\Dout_reg[31]_2 ;
  wire \Dout_reg[31]_2[17]_repN ;
  wire \Dout_reg[31]_2[17]_repN_1 ;
  wire \Dout_reg[31]_2[17]_repN_2 ;
  wire \Dout_reg[31]_2[19]_repN ;
  wire \Dout_reg[31]_2[19]_repN_1 ;
  wire \Dout_reg[31]_2[19]_repN_2 ;
  wire \Dout_reg[31]_2[19]_repN_3 ;
  wire \Dout_reg[31]_2[22]_repN ;
  wire \Dout_reg[31]_2[25]_repN ;
  wire \Dout_reg[31]_2[25]_repN_1 ;
  wire \Dout_reg[31]_2[25]_repN_2 ;
  wire \Dout_reg[31]_2[25]_repN_3 ;
  wire \Dout_reg[31]_2[26]_repN ;
  wire \Dout_reg[31]_2[26]_repN_1 ;
  wire \Dout_reg[31]_2[27]_repN ;
  wire \Dout_reg[31]_2[27]_repN_1 ;
  wire \Dout_reg[31]_2[27]_repN_2 ;
  wire \Dout_reg[31]_2[28]_repN ;
  wire \Dout_reg[31]_2[28]_repN_1 ;
  wire \Dout_reg[31]_2[28]_repN_2 ;
  wire \Dout_reg[31]_2[28]_repN_3 ;
  wire \Dout_reg[31]_2[29]_repN ;
  wire \Dout_reg[31]_2[29]_repN_1 ;
  wire \Dout_reg[31]_2[2]_repN ;
  wire \Dout_reg[31]_2[30]_repN ;
  wire \Dout_reg[31]_2[30]_repN_1 ;
  wire \Dout_reg[31]_2[30]_repN_2 ;
  wire \Dout_reg[31]_2[30]_repN_3 ;
  wire \Dout_reg[31]_2[30]_repN_4 ;
  wire \Dout_reg[31]_2[31]_repN ;
  wire \Dout_reg[31]_2[31]_repN_1 ;
  wire \Dout_reg[31]_2[4]_repN ;
  wire \Dout_reg[31]_2[4]_repN_1 ;
  wire \Dout_reg[31]_3 ;
  wire \Dout_reg[31]_4 ;
  wire [3:0]\Dout_reg[31]_5 ;
  wire \Dout_reg[3]_0 ;
  wire \Dout_reg[4]_0 ;
  wire \Dout_reg[6]_0 ;
  wire [3:0]\Dout_reg[7]_0 ;
  wire \Dout_reg[8]_0 ;
  wire \Dout_reg[9]_0 ;
  wire \Dout_reg[9]_1 ;
  wire [0:0]E;
  wire [0:0]O;
  wire [4:0]Q;
  wire \Q[1]_repN_1_alias ;
  wire \Q[1]_repN_2_alias ;
  wire \Q[1]_repN_3_alias ;
  wire \Q[1]_repN_5_alias ;
  wire \Q[1]_repN_6_alias ;
  wire \Q[1]_repN_7_alias ;
  wire \Q[1]_repN_alias ;
  wire \Q[2]_repN_1_alias ;
  wire \Q[2]_repN_2_alias ;
  wire \Q[2]_repN_3_alias ;
  wire \Q[2]_repN_4_alias ;
  wire \Q[2]_repN_5_alias ;
  wire \Q[2]_repN_6_alias ;
  wire \Q[2]_repN_7_alias ;
  wire \Q[2]_repN_alias ;
  wire \Q[3]_repN_1_alias ;
  wire \Q[3]_repN_2_alias ;
  wire \Q[3]_repN_3_alias ;
  wire \Q[3]_repN_6_alias ;
  wire \Q[3]_repN_7_alias ;
  wire \Q[3]_repN_8_alias ;
  wire \Q[3]_repN_alias ;
  wire \Q[4]_repN_1_alias ;
  wire \Q[4]_repN_2_alias ;
  wire \Q[4]_repN_3_alias ;
  wire \Q[4]_repN_5_alias ;
  wire \Q[4]_repN_alias ;
  wire [3:0]ROTATE_RIGHT1;
  wire [3:0]S;
  wire [0:0]SR;
  wire [3:0]\NLW_Dout_reg[2]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_Dout_reg[2]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_Dout_reg[2]_i_3_CO_UNCONNECTED ;
  wire [3:0]\NLW_Dout_reg[2]_i_3_O_UNCONNECTED ;
  wire [3:1]\NLW_Dout_reg[3]_i_4_CO_UNCONNECTED ;
  wire [3:0]\NLW_Dout_reg[3]_i_4_O_UNCONNECTED ;

  assign \Dout[13]_i_8_n_0_alias  = \Dout[13]_i_8_n_0 ;
  assign \Dout[14]_i_8_n_0_alias  = \Dout[14]_i_8_n_0 ;
  assign \Dout[3]_i_10_n_0_alias  = \Dout[3]_i_10_n_0 ;
  assign \Dout_reg[0]_12_repN_alias  = \Dout_reg[0]_12_repN ;
  assign \Dout_reg[0]_13_repN_alias  = \Dout_reg[0]_13_repN ;
  assign \Dout_reg[0]_30_repN_alias  = \Dout_reg[0]_30_repN ;
  assign \Dout_reg[31]_2[17]_repN_alias  = \Dout_reg[31]_2[17]_repN ;
  assign \Dout_reg[31]_2[19]_repN_1_alias  = \Dout_reg[31]_2[19]_repN_1 ;
  assign \Dout_reg[31]_2[25]_repN_3_alias  = \Dout_reg[31]_2[25]_repN_3 ;
  assign \Dout_reg[31]_2[25]_repN_alias  = \Dout_reg[31]_2[25]_repN ;
  assign \Dout_reg[31]_2[26]_repN_alias  = \Dout_reg[31]_2[26]_repN ;
  assign \Dout_reg[31]_2[27]_repN_1_alias  = \Dout_reg[31]_2[27]_repN_1 ;
  assign \Dout_reg[31]_2[27]_repN_alias  = \Dout_reg[31]_2[27]_repN ;
  assign \Dout_reg[31]_2[28]_repN_3_alias  = \Dout_reg[31]_2[28]_repN_3 ;
  assign \Dout_reg[31]_2[28]_repN_alias  = \Dout_reg[31]_2[28]_repN ;
  assign \Dout_reg[31]_2[29]_repN_1_alias  = \Dout_reg[31]_2[29]_repN_1 ;
  assign \Dout_reg[31]_2[29]_repN_alias  = \Dout_reg[31]_2[29]_repN ;
  assign \Dout_reg[31]_2[2]_repN_alias  = \Dout_reg[31]_2[2]_repN ;
  assign \Dout_reg[31]_2[30]_repN_1_alias  = \Dout_reg[31]_2[30]_repN_1 ;
  assign \Dout_reg[31]_2[30]_repN_2_alias  = \Dout_reg[31]_2[30]_repN_2 ;
  assign \Dout_reg[31]_2[30]_repN_3_alias  = \Dout_reg[31]_2[30]_repN_3 ;
  assign \Dout_reg[31]_2[30]_repN_4_alias  = \Dout_reg[31]_2[30]_repN_4 ;
  assign \Dout_reg[31]_2[30]_repN_alias  = \Dout_reg[31]_2[30]_repN ;
  assign \Dout_reg[31]_2[31]_repN_1_alias  = \Dout_reg[31]_2[31]_repN_1 ;
  assign \Dout_reg[31]_2[31]_repN_alias  = \Dout_reg[31]_2[31]_repN ;
  assign \Dout_reg[31]_2[4]_repN_alias  = \Dout_reg[31]_2[4]_repN ;
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[0]_i_11__0 
       (.I0(\Dout_reg[31]_2[31]_repN ),
        .I1(\Dout_reg[31]_2 [15]),
        .I2(ROTATE_RIGHT1[2]),
        .I3(\Dout_reg[31]_2 [7]),
        .I4(ROTATE_RIGHT1[3]),
        .I5(\Dout_reg[31]_2 [23]),
        .O(\Dout_reg[31]_3 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[0]_i_12 
       (.I0(\Dout_reg[31]_2 [0]),
        .I1(\Dout_reg[31]_2 [16]),
        .I2(\Q[3]_repN_1_alias ),
        .I3(\Dout_reg[31]_2 [24]),
        .I4(\Q[4]_repN_3_alias ),
        .I5(\Dout_reg[31]_2 [8]),
        .O(\Dout[0]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[0]_i_12__0 
       (.I0(\Dout_reg[31]_2[31]_repN ),
        .I1(\Dout_reg[31]_2 [15]),
        .I2(\Q[3]_repN_2_alias ),
        .I3(\Dout_reg[31]_2 [7]),
        .I4(\Q[4]_repN_alias ),
        .I5(\Dout_reg[31]_2 [23]),
        .O(\Dout[0]_i_12__0_n_0 ));
  LUT6 #(
    .INIT(64'h30303030BBB8B8B8)) 
    \Dout[0]_i_4__0 
       (.I0(\Dout_reg[31]_2 [31]),
        .I1(\Dout_reg[0]_47 [1]),
        .I2(\Dout_reg[0]_48 ),
        .I3(\Dout_reg[2]_1 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_47 [0]),
        .O(\Dout_reg[31]_4 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[0]_i_6 
       (.I0(\Dout[0]_i_12_n_0 ),
        .I1(\Dout[4]_i_12_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[5]_i_21_n_0 ),
        .I4(\Q[2]_repN_4_alias ),
        .I5(\Dout[2]_i_12_n_0 ),
        .O(\Dout_reg[1]_5 ));
  LUT6 #(
    .INIT(64'hB8B8B8B8FFCC3300)) 
    \Dout[0]_i_9__0 
       (.I0(\Dout[30]_i_18_n_0 ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout[30]_i_19_n_0 ),
        .I3(\Dout[0]_i_12__0_n_0 ),
        .I4(\Dout[30]_i_17_n_0 ),
        .I5(\Q[1]_repN_2_alias ),
        .O(\Dout_reg[2]_2 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[10]_i_10 
       (.I0(\Dout[16]_i_15_n_0 ),
        .I1(\Dout[5]_i_18_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[14]_i_14_n_0 ),
        .I4(\Q[2]_repN_4_alias ),
        .I5(\Dout[5]_i_20_n_0 ),
        .O(\Dout[10]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[10]_i_11 
       (.I0(\Dout[16]_i_17_n_0 ),
        .I1(\Dout[5]_i_26_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[14]_i_15_n_0 ),
        .I4(\Q[2]_repN_4_alias ),
        .I5(\Dout[5]_i_28_n_0 ),
        .O(\Dout[10]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[10]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[10]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[10]_i_7_n_0 ),
        .I4(\Dout[11]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_24 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[10]_i_5 
       (.I0(\Dout[10]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[10]_i_6_n_0 ),
        .I3(\Dout[11]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[10]_i_9_n_0 ),
        .O(\Dout_reg[0]_9 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[10]_i_6 
       (.I0(\Dout[11]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[10]_i_10_n_0 ),
        .O(\Dout[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[10]_i_7 
       (.I0(\Dout_reg[31]_2 [3]),
        .I1(\Q[2]_repN_4_alias ),
        .I2(\Dout[25]_i_6 ),
        .I3(\Dout_reg[31]_2 [7]),
        .I4(\Q[1]_repN_3_alias ),
        .I5(\Dout[12]_i_11_n_0 ),
        .O(\Dout[10]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[10]_i_8 
       (.I0(\Dout[11]_i_12_n_0 ),
        .I1(\Dout[10]_i_11_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[10]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[10]_i_9 
       (.I0(\Dout_reg[31]_2 [3]),
        .I1(ROTATE_RIGHT1[1]),
        .I2(\Dout[16]_i_9_0 ),
        .I3(\Dout_reg[31]_2 [7]),
        .I4(ROTATE_RIGHT1[0]),
        .I5(\Dout[12]_i_13_n_0 ),
        .O(\Dout[10]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[11]_i_10 
       (.I0(\Dout[17]_i_15_n_0 ),
        .I1(\Dout[13]_i_14_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[15]_i_14_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[5]_i_14_n_0 ),
        .O(\Dout[11]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h000030300000BB88)) 
    \Dout[11]_i_11 
       (.I0(\Dout_reg[31]_2 [4]),
        .I1(\Q[2]_repN_4_alias ),
        .I2(\Dout_reg[31]_2 [0]),
        .I3(\Dout_reg[31]_2 [8]),
        .I4(\Q[4]_repN_1_alias ),
        .I5(\Q[3]_repN_1_alias ),
        .O(\Dout[11]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[11]_i_12 
       (.I0(\Dout[17]_i_17_n_0 ),
        .I1(\Dout[13]_i_15_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[15]_i_16_n_0 ),
        .I4(\Q[2]_repN_3_alias ),
        .I5(\Dout[5]_i_29_n_0 ),
        .O(\Dout[11]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBB88830003000)) 
    \Dout[11]_i_13 
       (.I0(\Dout_reg[31]_2[4]_repN ),
        .I1(ROTATE_RIGHT1[1]),
        .I2(\Dout_reg[31]_2 [0]),
        .I3(\Dout[16]_i_9_1 ),
        .I4(\Dout_reg[31]_2 [8]),
        .I5(\Dout[16]_i_9_0 ),
        .O(\Dout[11]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[11]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[11]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[11]_i_7_n_0 ),
        .I4(\Dout[12]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_25 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[11]_i_5 
       (.I0(\Dout[11]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[11]_i_6_n_0 ),
        .I3(\Dout[12]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[11]_i_9_n_0 ),
        .O(\Dout_reg[0]_10 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[11]_i_6 
       (.I0(\Dout[12]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[11]_i_10_n_0 ),
        .O(\Dout[11]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[11]_i_7 
       (.I0(\Dout[11]_i_11_n_0 ),
        .I1(\Q[1]_repN_3_alias ),
        .I2(\Dout[13]_i_11_n_0 ),
        .O(\Dout[11]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[11]_i_8 
       (.I0(\Dout[12]_i_12_n_0 ),
        .I1(\Dout[11]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[11]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hEB28)) 
    \Dout[11]_i_9 
       (.I0(\Dout[11]_i_13_n_0 ),
        .I1(\Q[1]_repN_3_alias ),
        .I2(Q[0]),
        .I3(\Dout[13]_i_13_n_0 ),
        .O(\Dout[11]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[12]_i_10 
       (.I0(\Dout[18]_i_15_n_0 ),
        .I1(\Dout[14]_i_14_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[16]_i_15_n_0 ),
        .I4(\Q[2]_repN_4_alias ),
        .I5(\Dout[5]_i_18_n_0 ),
        .O(\Dout[12]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h000030300000BB88)) 
    \Dout[12]_i_11 
       (.I0(\Dout_reg[31]_2 [5]),
        .I1(\Q[2]_repN_4_alias ),
        .I2(\Dout_reg[31]_2 [1]),
        .I3(\Dout_reg[31]_2 [9]),
        .I4(\Q[4]_repN_1_alias ),
        .I5(\Q[3]_repN_1_alias ),
        .O(\Dout[12]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[12]_i_12 
       (.I0(\Dout[18]_i_17_n_0 ),
        .I1(\Dout[14]_i_15_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[16]_i_17_n_0 ),
        .I4(\Q[2]_repN_3_alias ),
        .I5(\Dout[5]_i_26_n_0 ),
        .O(\Dout[12]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBB88830003000)) 
    \Dout[12]_i_13 
       (.I0(\Dout_reg[31]_2 [5]),
        .I1(ROTATE_RIGHT1[1]),
        .I2(\Dout_reg[31]_2 [1]),
        .I3(\Dout[16]_i_9_1 ),
        .I4(\Dout_reg[31]_2 [9]),
        .I5(\Dout[16]_i_9_0 ),
        .O(\Dout[12]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[12]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[12]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[12]_i_7_n_0 ),
        .I4(\Dout[13]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_26 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[12]_i_5 
       (.I0(\Dout[12]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[12]_i_6_n_0 ),
        .I3(\Dout[13]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[12]_i_9_n_0 ),
        .O(\Dout_reg[0]_11 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[12]_i_6 
       (.I0(\Dout[13]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[12]_i_10_n_0 ),
        .O(\Dout[12]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[12]_i_7 
       (.I0(\Dout[12]_i_11_n_0 ),
        .I1(\Q[1]_repN_3_alias ),
        .I2(\Dout[14]_i_11_n_0 ),
        .O(\Dout[12]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[12]_i_8 
       (.I0(\Dout[13]_i_12_n_0 ),
        .I1(\Dout[12]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[12]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hEB28)) 
    \Dout[12]_i_9 
       (.I0(\Dout[12]_i_13_n_0 ),
        .I1(\Q[1]_repN_3_alias ),
        .I2(Q[0]),
        .I3(\Dout[14]_i_13_n_0 ),
        .O(\Dout[12]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[13]_i_10 
       (.I0(\Dout[19]_i_15_n_0 ),
        .I1(\Dout[15]_i_14_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[17]_i_15_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[13]_i_14_n_0 ),
        .O(\Dout[13]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h000030300000BB88)) 
    \Dout[13]_i_11 
       (.I0(\Dout_reg[31]_2 [6]),
        .I1(\Q[2]_repN_3_alias ),
        .I2(\Dout_reg[31]_2[2]_repN ),
        .I3(\Dout_reg[31]_2 [10]),
        .I4(\Q[4]_repN_1_alias ),
        .I5(\Q[3]_repN_1_alias ),
        .O(\Dout[13]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[13]_i_12 
       (.I0(\Dout[19]_i_17_n_0 ),
        .I1(\Dout[15]_i_16_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[17]_i_17_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[13]_i_15_n_0 ),
        .O(\Dout[13]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBB88830003000)) 
    \Dout[13]_i_13 
       (.I0(\Dout_reg[31]_2 [6]),
        .I1(ROTATE_RIGHT1[1]),
        .I2(\Dout_reg[31]_2[2]_repN ),
        .I3(\Dout[16]_i_9_1 ),
        .I4(\Dout_reg[31]_2 [10]),
        .I5(\Dout[16]_i_9_0 ),
        .O(\Dout[13]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[13]_i_14 
       (.I0(\Dout_reg[31]_2 [21]),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2 [29]),
        .I3(Q[4]),
        .I4(\Dout_reg[31]_2 [13]),
        .O(\Dout[13]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hCFC0AFAFCFC0A0A0)) 
    \Dout[13]_i_15 
       (.I0(\Dout_reg[31]_2 [21]),
        .I1(\Dout_reg[31]_2 [31]),
        .I2(\Q[3]_repN_7_alias ),
        .I3(\Dout_reg[31]_2[29]_repN ),
        .I4(\Q[4]_repN_1_alias ),
        .I5(\Dout_reg[31]_2 [13]),
        .O(\Dout[13]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[13]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[13]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[13]_i_7_n_0 ),
        .I4(\Dout[14]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_34 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \Dout[13]_i_5_comp 
       (.I0(\Dout[1]_i_5_0 ),
        .I1(\Dout[13]_i_3_n_0_alias ),
        .O(\Dout_reg[0]_12_repN ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'hFCFCFCFCFCF4F8F0)) 
    \Dout[13]_i_5_comp_1 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[13]_i_3_n_0_alias ),
        .I3(\Dout[13]_i_9_n_0 ),
        .I4(\Dout[14]_i_9_n_0 ),
        .I5(\Dout[13]_i_6_n_0 ),
        .O(\Dout_reg[0]_12 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[13]_i_6 
       (.I0(\Dout[14]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[13]_i_10_n_0 ),
        .O(\Dout[13]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[13]_i_7 
       (.I0(\Dout[13]_i_11_n_0 ),
        .I1(\Q[1]_repN_alias ),
        .I2(\Dout[15]_i_11_n_0 ),
        .O(\Dout[13]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[13]_i_8 
       (.I0(\Dout[14]_i_12_n_0 ),
        .I1(\Dout[13]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[13]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hEBE8EB2B2B28E828)) 
    \Dout[13]_i_9 
       (.I0(\Dout[13]_i_13_n_0 ),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\Dout[15]_i_13_n_0 ),
        .I4(\Q[2]_repN_3_alias ),
        .I5(\Dout[19]_i_13_n_0 ),
        .O(\Dout[13]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[14]_i_10 
       (.I0(\Dout[16]_i_14_n_0 ),
        .I1(\Dout[16]_i_15_n_0 ),
        .I2(\Q[1]_repN_1_alias ),
        .I3(\Dout[18]_i_15_n_0 ),
        .I4(\Q[2]_repN_5_alias ),
        .I5(\Dout[14]_i_14_n_0 ),
        .O(\Dout[14]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h000030300000BB88)) 
    \Dout[14]_i_11 
       (.I0(\Dout_reg[31]_2 [7]),
        .I1(\Q[2]_repN_3_alias ),
        .I2(\Dout_reg[31]_2 [3]),
        .I3(\Dout_reg[31]_2 [11]),
        .I4(\Q[4]_repN_1_alias ),
        .I5(\Q[3]_repN_1_alias ),
        .O(\Dout[14]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[14]_i_12 
       (.I0(\Dout[20]_i_16_n_0 ),
        .I1(\Dout[16]_i_17_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[18]_i_17_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[14]_i_15_n_0 ),
        .O(\Dout[14]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBB88830003000)) 
    \Dout[14]_i_13 
       (.I0(\Dout_reg[31]_2 [7]),
        .I1(ROTATE_RIGHT1[1]),
        .I2(\Dout_reg[31]_2 [3]),
        .I3(\Dout[16]_i_9_1 ),
        .I4(\Dout_reg[31]_2 [11]),
        .I5(\Dout[16]_i_9_0 ),
        .O(\Dout[14]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[14]_i_14 
       (.I0(\Dout_reg[31]_2 [22]),
        .I1(\Q[3]_repN_1_alias ),
        .I2(\Dout_reg[31]_2[30]_repN ),
        .I3(\Q[4]_repN_1_alias ),
        .I4(\Dout_reg[31]_2 [14]),
        .O(\Dout[14]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hCFC0AFAFCFC0A0A0)) 
    \Dout[14]_i_15 
       (.I0(\Dout_reg[31]_2 [22]),
        .I1(\Dout_reg[31]_2 [31]),
        .I2(\Q[3]_repN_7_alias ),
        .I3(\Dout_reg[31]_2[30]_repN_4 ),
        .I4(\Q[4]_repN_3_alias ),
        .I5(\Dout_reg[31]_2 [14]),
        .O(\Dout[14]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[14]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[14]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[14]_i_7_n_0 ),
        .I4(\Dout[15]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_35 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \Dout[14]_i_5_comp 
       (.I0(\Dout[1]_i_5_0 ),
        .I1(\Dout[14]_i_3_n_0_alias ),
        .O(\Dout_reg[0]_13_repN ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'hFCFCFCFCFCF8F4F0)) 
    \Dout[14]_i_5_comp_1 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[14]_i_3_n_0_alias ),
        .I3(\Dout[15]_i_9_n_0 ),
        .I4(\Dout[14]_i_9_n_0 ),
        .I5(\Dout[14]_i_6_n_0 ),
        .O(\Dout_reg[0]_13 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[14]_i_6 
       (.I0(\Dout[15]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[14]_i_10_n_0 ),
        .O(\Dout[14]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[14]_i_7 
       (.I0(\Dout[14]_i_11_n_0 ),
        .I1(\Q[1]_repN_alias ),
        .I2(\Dout[16]_i_11_n_0 ),
        .O(\Dout[14]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[14]_i_8 
       (.I0(\Dout[15]_i_12_n_0 ),
        .I1(\Dout[14]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[14]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hEBE8EB2B2B28E828)) 
    \Dout[14]_i_9 
       (.I0(\Dout[14]_i_13_n_0 ),
        .I1(\Q[1]_repN_5_alias ),
        .I2(Q[0]),
        .I3(\Dout[16]_i_13_n_0 ),
        .I4(\Q[2]_repN_1_alias ),
        .I5(\Dout[20]_i_14_n_0 ),
        .O(\Dout[14]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[15]_i_10 
       (.I0(\Dout[17]_i_14_n_0 ),
        .I1(\Dout[17]_i_15_n_0 ),
        .I2(\Q[1]_repN_1_alias ),
        .I3(\Dout[19]_i_15_n_0 ),
        .I4(\Q[2]_repN_5_alias ),
        .I5(\Dout[15]_i_14_n_0 ),
        .O(\Dout[15]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0A0CFFFF0A0C0000)) 
    \Dout[15]_i_11 
       (.I0(\Dout_reg[31]_2 [0]),
        .I1(\Dout_reg[31]_2 [8]),
        .I2(\Q[4]_repN_1_alias ),
        .I3(\Q[3]_repN_1_alias ),
        .I4(\Q[2]_repN_3_alias ),
        .I5(\Dout[15]_i_15_n_0 ),
        .O(\Dout[15]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[15]_i_12 
       (.I0(\Dout[21]_i_16_n_0 ),
        .I1(\Dout[17]_i_17_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[19]_i_17_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[15]_i_16_n_0 ),
        .O(\Dout[15]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \Dout[15]_i_13 
       (.I0(\Dout_reg[31]_2 [0]),
        .I1(\Dout[16]_i_9_1 ),
        .I2(\Dout_reg[31]_2 [8]),
        .I3(\Dout[16]_i_9_0 ),
        .O(\Dout[15]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'h3300B8B8)) 
    \Dout[15]_i_14 
       (.I0(\Dout_reg[31]_2 [23]),
        .I1(Q[3]),
        .I2(\Dout_reg[31]_2 [15]),
        .I3(\Dout_reg[31]_2 [31]),
        .I4(\Q[4]_repN_1_alias ),
        .O(\Dout[15]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[15]_i_15 
       (.I0(\Dout_reg[31]_2[4]_repN ),
        .I1(\Dout_reg[31]_2 [12]),
        .I2(\Q[4]_repN_1_alias ),
        .I3(\Q[3]_repN_8_alias ),
        .O(\Dout[15]_i_15_n_0 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[15]_i_16 
       (.I0(\Dout_reg[31]_2 [23]),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2 [15]),
        .I3(\Dout_reg[31]_2 [31]),
        .I4(\Q[4]_repN_2_alias ),
        .O(\Dout[15]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[15]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[15]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[15]_i_7_n_0 ),
        .I4(\Dout[16]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_38 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[15]_i_5 
       (.I0(\Dout[15]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[15]_i_6_n_0 ),
        .I3(\Dout[16]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[15]_i_9_n_0 ),
        .O(\Dout_reg[0]_14 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[15]_i_6 
       (.I0(\Dout[16]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[15]_i_10_n_0 ),
        .O(\Dout[15]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[15]_i_7 
       (.I0(\Dout[15]_i_11_n_0 ),
        .I1(\Q[1]_repN_alias ),
        .I2(\Dout[17]_i_11_n_0 ),
        .O(\Dout[15]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[15]_i_8 
       (.I0(\Dout[16]_i_12_n_0 ),
        .I1(\Dout[15]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[15]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[15]_i_9 
       (.I0(\Dout[15]_i_13_n_0 ),
        .I1(\Dout[19]_i_13_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[17]_i_13_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[21]_i_14_n_0 ),
        .O(\Dout[15]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[16]_i_10 
       (.I0(\Dout[18]_i_14_n_0 ),
        .I1(\Dout[18]_i_15_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[16]_i_14_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[16]_i_15_n_0 ),
        .O(\Dout[16]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0A0CFFFF0A0C0000)) 
    \Dout[16]_i_11 
       (.I0(\Dout_reg[31]_2 [1]),
        .I1(\Dout_reg[31]_2 [9]),
        .I2(\Q[4]_repN_1_alias ),
        .I3(\Q[3]_repN_1_alias ),
        .I4(\Q[2]_repN_3_alias ),
        .I5(\Dout[16]_i_16_n_0 ),
        .O(\Dout[16]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[16]_i_12 
       (.I0(\Dout[22]_i_15_n_0 ),
        .I1(\Dout[18]_i_17_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[20]_i_16_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[16]_i_17_n_0 ),
        .O(\Dout[16]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \Dout[16]_i_13 
       (.I0(\Dout_reg[31]_2 [1]),
        .I1(\Dout[16]_i_9_1 ),
        .I2(\Dout_reg[31]_2 [9]),
        .I3(\Dout[16]_i_9_0 ),
        .O(\Dout[16]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[16]_i_14 
       (.I0(\Dout_reg[31]_2[28]_repN_2 ),
        .I1(\Dout_reg[31]_2 [20]),
        .I2(\Q[4]_repN_2_alias ),
        .I3(\Q[3]_repN_6_alias ),
        .O(\Dout[16]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[16]_i_15 
       (.I0(\Dout_reg[31]_2 [24]),
        .I1(\Dout_reg[31]_2 [16]),
        .I2(\Q[4]_repN_2_alias ),
        .I3(\Q[3]_repN_7_alias ),
        .O(\Dout[16]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[16]_i_16 
       (.I0(\Dout_reg[31]_2 [5]),
        .I1(\Dout_reg[31]_2 [13]),
        .I2(\Q[4]_repN_1_alias ),
        .I3(\Q[3]_repN_1_alias ),
        .O(\Dout[16]_i_16_n_0 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[16]_i_17 
       (.I0(\Dout_reg[31]_2 [24]),
        .I1(Q[3]),
        .I2(\Dout_reg[31]_2 [16]),
        .I3(\Dout_reg[31]_2 [31]),
        .I4(\Q[4]_repN_1_alias ),
        .O(\Dout[16]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[16]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[16]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[16]_i_7_n_0 ),
        .I4(\Dout[17]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_39 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[16]_i_5 
       (.I0(\Dout[16]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[16]_i_6_n_0 ),
        .I3(\Dout[17]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[16]_i_9_n_0 ),
        .O(\Dout_reg[0]_15 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[16]_i_6 
       (.I0(\Dout[17]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[16]_i_10_n_0 ),
        .O(\Dout[16]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[16]_i_7 
       (.I0(\Dout[16]_i_11_n_0 ),
        .I1(\Q[1]_repN_alias ),
        .I2(\Dout[18]_i_11_n_0 ),
        .O(\Dout[16]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[16]_i_8 
       (.I0(\Dout[17]_i_12_n_0 ),
        .I1(\Dout[16]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[16]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[16]_i_9 
       (.I0(\Dout[16]_i_13_n_0 ),
        .I1(\Dout[20]_i_14_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[18]_i_13_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[22]_i_13_n_0 ),
        .O(\Dout[16]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[17]_i_10 
       (.I0(\Dout[19]_i_14_n_0 ),
        .I1(\Dout[19]_i_15_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[17]_i_14_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[17]_i_15_n_0 ),
        .O(\Dout[17]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0A0CFFFF0A0C0000)) 
    \Dout[17]_i_11 
       (.I0(\Dout_reg[31]_2[2]_repN ),
        .I1(\Dout_reg[31]_2 [10]),
        .I2(\Q[4]_repN_1_alias ),
        .I3(\Q[3]_repN_1_alias ),
        .I4(\Q[2]_repN_3_alias ),
        .I5(\Dout[17]_i_16_n_0 ),
        .O(\Dout[17]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[17]_i_12 
       (.I0(\Dout[19]_i_16_n_0 ),
        .I1(\Dout[19]_i_17_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[21]_i_16_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[17]_i_17_n_0 ),
        .O(\Dout[17]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \Dout[17]_i_13 
       (.I0(\Dout_reg[31]_2[2]_repN ),
        .I1(\Dout[16]_i_9_1 ),
        .I2(\Dout_reg[31]_2 [10]),
        .I3(\Dout[16]_i_9_0 ),
        .O(\Dout[17]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[17]_i_14 
       (.I0(\Dout_reg[31]_2[29]_repN ),
        .I1(\Dout_reg[31]_2 [21]),
        .I2(\Q[4]_repN_2_alias ),
        .I3(\Q[3]_repN_8_alias ),
        .O(\Dout[17]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[17]_i_15 
       (.I0(\Dout_reg[31]_2[25]_repN_2 ),
        .I1(\Dout_reg[31]_2[17]_repN_1 ),
        .I2(\Q[4]_repN_2_alias ),
        .I3(\Q[3]_repN_7_alias ),
        .O(\Dout[17]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[17]_i_16 
       (.I0(\Dout_reg[31]_2 [6]),
        .I1(\Dout_reg[31]_2 [14]),
        .I2(\Q[4]_repN_2_alias ),
        .I3(\Q[3]_repN_8_alias ),
        .O(\Dout[17]_i_16_n_0 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[17]_i_17 
       (.I0(\Dout_reg[31]_2[25]_repN ),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2[17]_repN ),
        .I3(\Dout_reg[31]_2 [31]),
        .I4(Q[4]),
        .O(\Dout[17]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[17]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[17]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[17]_i_7_n_0 ),
        .I4(\Dout[18]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_40 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[17]_i_5 
       (.I0(\Dout[17]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[17]_i_6_n_0 ),
        .I3(\Dout[18]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[17]_i_9_n_0 ),
        .O(\Dout_reg[0]_16 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[17]_i_6 
       (.I0(\Dout[18]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[17]_i_10_n_0 ),
        .O(\Dout[17]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[17]_i_7 
       (.I0(\Dout[17]_i_11_n_0 ),
        .I1(\Q[1]_repN_alias ),
        .I2(\Dout[19]_i_11_n_0 ),
        .O(\Dout[17]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[17]_i_8 
       (.I0(\Dout[18]_i_12_n_0 ),
        .I1(\Dout[17]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[17]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[17]_i_9 
       (.I0(\Dout[17]_i_13_n_0 ),
        .I1(\Dout[21]_i_14_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[19]_i_13_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[23]_i_13_n_0 ),
        .O(\Dout[17]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \Dout[18]_i_10 
       (.I0(\Dout[20]_i_11_n_0 ),
        .I1(\Q[1]_repN_5_alias ),
        .I2(\Dout[18]_i_14_n_0 ),
        .I3(\Q[2]_repN_1_alias ),
        .I4(\Dout[18]_i_15_n_0 ),
        .O(\Dout[18]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0A0CFFFF0A0C0000)) 
    \Dout[18]_i_11 
       (.I0(\Dout_reg[31]_2 [3]),
        .I1(\Dout_reg[31]_2 [11]),
        .I2(\Q[4]_repN_2_alias ),
        .I3(\Q[3]_repN_8_alias ),
        .I4(\Q[2]_repN_1_alias ),
        .I5(\Dout[18]_i_16_n_0 ),
        .O(\Dout[18]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[18]_i_12 
       (.I0(\Dout[20]_i_15_n_0 ),
        .I1(\Dout[20]_i_16_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[22]_i_15_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[18]_i_17_n_0 ),
        .O(\Dout[18]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \Dout[18]_i_13 
       (.I0(\Dout_reg[31]_2 [3]),
        .I1(\Dout[16]_i_9_1 ),
        .I2(\Dout_reg[31]_2 [11]),
        .I3(\Dout[16]_i_9_0 ),
        .O(\Dout[18]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[18]_i_14 
       (.I0(\Dout_reg[31]_2[30]_repN_4 ),
        .I1(\Dout_reg[31]_2 [22]),
        .I2(\Q[4]_repN_2_alias ),
        .I3(\Q[3]_repN_7_alias ),
        .O(\Dout[18]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[18]_i_15 
       (.I0(\Dout_reg[31]_2 [26]),
        .I1(\Dout_reg[31]_2 [18]),
        .I2(\Q[4]_repN_2_alias ),
        .I3(\Q[3]_repN_7_alias ),
        .O(\Dout[18]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[18]_i_16 
       (.I0(\Dout_reg[31]_2 [7]),
        .I1(\Dout_reg[31]_2 [15]),
        .I2(\Q[4]_repN_1_alias ),
        .I3(\Q[3]_repN_1_alias ),
        .O(\Dout[18]_i_16_n_0 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[18]_i_17 
       (.I0(\Dout_reg[31]_2[26]_repN ),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2 [18]),
        .I3(\Dout_reg[31]_2 [31]),
        .I4(\Q[4]_repN_2_alias ),
        .O(\Dout[18]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[18]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[18]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[18]_i_7_n_0 ),
        .I4(\Dout[19]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_41 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[18]_i_5 
       (.I0(\Dout[18]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[18]_i_6_n_0 ),
        .I3(\Dout[19]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[18]_i_9_n_0 ),
        .O(\Dout_reg[0]_17 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[18]_i_6 
       (.I0(\Dout[19]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[18]_i_10_n_0 ),
        .O(\Dout[18]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[18]_i_7 
       (.I0(\Dout[18]_i_11_n_0 ),
        .I1(\Q[1]_repN_5_alias ),
        .I2(\Dout[20]_i_12_n_0 ),
        .O(\Dout[18]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[18]_i_8 
       (.I0(\Dout[19]_i_12_n_0 ),
        .I1(\Dout[18]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[18]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[18]_i_9 
       (.I0(\Dout[18]_i_13_n_0 ),
        .I1(\Dout[22]_i_13_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[20]_i_14_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[24]_i_13_n_0 ),
        .O(\Dout[18]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \Dout[19]_i_10 
       (.I0(\Dout[21]_i_11_n_0 ),
        .I1(\Q[1]_repN_5_alias ),
        .I2(\Dout[19]_i_14_n_0 ),
        .I3(\Q[2]_repN_1_alias ),
        .I4(\Dout[19]_i_15_n_0 ),
        .O(\Dout[19]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0A0CFFFF0A0C0000)) 
    \Dout[19]_i_11 
       (.I0(\Dout_reg[31]_2[4]_repN ),
        .I1(\Dout_reg[31]_2 [12]),
        .I2(\Q[4]_repN_1_alias ),
        .I3(\Q[3]_repN_1_alias ),
        .I4(\Q[2]_repN_3_alias ),
        .I5(\Dout[23]_i_11_n_0 ),
        .O(\Dout[19]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[19]_i_12 
       (.I0(\Dout[21]_i_15_n_0 ),
        .I1(\Dout[21]_i_16_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[19]_i_16_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[19]_i_17_n_0 ),
        .O(\Dout[19]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \Dout[19]_i_13 
       (.I0(\Dout_reg[31]_2[4]_repN ),
        .I1(\Dout[16]_i_9_1 ),
        .I2(\Dout_reg[31]_2 [12]),
        .I3(\Dout[16]_i_9_0 ),
        .O(\Dout[19]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[19]_i_14 
       (.I0(\Dout_reg[31]_2 [31]),
        .I1(\Dout_reg[31]_2 [23]),
        .I2(\Q[4]_repN_2_alias ),
        .I3(\Q[3]_repN_7_alias ),
        .O(\Dout[19]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'h0A0C)) 
    \Dout[19]_i_15 
       (.I0(\Dout_reg[31]_2[27]_repN_1 ),
        .I1(\Dout_reg[31]_2[19]_repN ),
        .I2(\Q[4]_repN_2_alias ),
        .I3(\Q[3]_repN_7_alias ),
        .O(\Dout[19]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'hF0E2)) 
    \Dout[19]_i_16 
       (.I0(\Dout_reg[31]_2 [23]),
        .I1(\Q[4]_repN_2_alias ),
        .I2(\Dout_reg[31]_2 [31]),
        .I3(\Q[3]_repN_7_alias ),
        .O(\Dout[19]_i_16_n_0 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[19]_i_17 
       (.I0(\Dout_reg[31]_2[27]_repN_1 ),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2[19]_repN ),
        .I3(\Dout_reg[31]_2 [31]),
        .I4(\Q[4]_repN_2_alias ),
        .O(\Dout[19]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[19]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[19]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[19]_i_7_n_0 ),
        .I4(\Dout[20]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_42 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[19]_i_5 
       (.I0(\Dout[19]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[19]_i_6_n_0 ),
        .I3(\Dout[20]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[19]_i_9_n_0 ),
        .O(\Dout_reg[0]_18 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \Dout[19]_i_6 
       (.I0(\Dout[20]_i_10_n_0 ),
        .I1(\Q[1]_repN_3_alias ),
        .I2(\Dout[20]_i_11_n_0 ),
        .I3(Q[0]),
        .I4(\Dout[19]_i_10_n_0 ),
        .O(\Dout[19]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[19]_i_7 
       (.I0(\Dout[19]_i_11_n_0 ),
        .I1(\Q[1]_repN_3_alias ),
        .I2(\Dout[21]_i_12_n_0 ),
        .O(\Dout[19]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[19]_i_8 
       (.I0(\Dout[20]_i_13_n_0 ),
        .I1(\Dout[19]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[19]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[19]_i_9 
       (.I0(\Dout[19]_i_13_n_0 ),
        .I1(\Dout[23]_i_13_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[21]_i_14_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[25]_i_13_n_0 ),
        .O(\Dout[19]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[1]_i_11 
       (.I0(\Dout_reg[31]_2 [1]),
        .I1(\Dout_reg[31]_2[17]_repN ),
        .I2(\Q[3]_repN_1_alias ),
        .I3(\Dout_reg[31]_2 [25]),
        .I4(\Q[4]_repN_1_alias ),
        .I5(\Dout_reg[31]_2 [9]),
        .O(\Dout[1]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[1]_i_17 
       (.I0(\Dout_reg[2]_0 ),
        .I1(\Dout[1]_i_14 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[9]_i_10_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[8]_i_10_n_0 ),
        .O(\Dout_reg[0]_28 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[1]_i_18 
       (.I0(\Dout[1]_i_16 ),
        .I1(\Dout_reg[3]_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[7]_i_10_n_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[1]_2 ),
        .O(\Dout_reg[0]_27 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[1]_i_5__0 
       (.I0(\Dout[2]_i_10_n_0 ),
        .I1(\Dout_reg[1]_3 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout_reg[0]_29 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[1]_i_7__0 
       (.I0(\Dout[1]_i_11_n_0 ),
        .I1(\Dout[5]_i_17_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[5]_i_15_n_0 ),
        .I4(\Q[2]_repN_4_alias ),
        .I5(\Dout[3]_i_12_n_0 ),
        .O(\Dout_reg[1]_3 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFCFEFC)) 
    \Dout[1]_i_8 
       (.I0(\Dout_reg[0]_5 ),
        .I1(\Dout[1]_i_5 ),
        .I2(\Dout_reg[0]_6 ),
        .I3(\Dout[1]_i_5_0 ),
        .I4(\Dout_reg[0]_7 ),
        .I5(\Dout[1]_i_5_1 ),
        .O(\Dout_reg[0]_4 ));
  LUT6 #(
    .INIT(64'h000030300000BB88)) 
    \Dout[20]_i_10 
       (.I0(\Dout_reg[31]_2[26]_repN ),
        .I1(\Q[2]_repN_alias ),
        .I2(\Dout_reg[31]_2[30]_repN_3 ),
        .I3(\Dout_reg[31]_2 [22]),
        .I4(\Q[4]_repN_2_alias ),
        .I5(\Q[3]_repN_8_alias ),
        .O(\Dout[20]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h000030300000BB88)) 
    \Dout[20]_i_11 
       (.I0(\Dout_reg[31]_2 [24]),
        .I1(\Q[2]_repN_1_alias ),
        .I2(\Dout_reg[31]_2[28]_repN_3 ),
        .I3(\Dout_reg[31]_2 [20]),
        .I4(\Q[4]_repN_2_alias ),
        .I5(\Q[3]_repN_8_alias ),
        .O(\Dout[20]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0A0CFFFF0A0C0000)) 
    \Dout[20]_i_12 
       (.I0(\Dout_reg[31]_2 [5]),
        .I1(\Dout_reg[31]_2 [13]),
        .I2(\Q[4]_repN_5_alias ),
        .I3(\Q[3]_repN_3_alias ),
        .I4(\Q[2]_repN_2_alias ),
        .I5(\Dout[24]_i_11_n_0 ),
        .O(\Dout[20]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[20]_i_13 
       (.I0(\Dout[22]_i_14_n_0 ),
        .I1(\Dout[22]_i_15_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[20]_i_15_n_0 ),
        .I4(\Q[2]_repN_alias ),
        .I5(\Dout[20]_i_16_n_0 ),
        .O(\Dout[20]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \Dout[20]_i_14 
       (.I0(\Dout_reg[31]_2 [5]),
        .I1(\Dout[16]_i_9_1 ),
        .I2(\Dout_reg[31]_2 [13]),
        .I3(\Dout[16]_i_9_0 ),
        .O(\Dout[20]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'hF0E2)) 
    \Dout[20]_i_15 
       (.I0(\Dout_reg[31]_2 [24]),
        .I1(\Q[4]_repN_2_alias ),
        .I2(\Dout_reg[31]_2 [31]),
        .I3(\Q[3]_repN_7_alias ),
        .O(\Dout[20]_i_15_n_0 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[20]_i_16 
       (.I0(\Dout_reg[31]_2[28]_repN_3 ),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2 [20]),
        .I3(\Dout_reg[31]_2 [31]),
        .I4(\Q[4]_repN_2_alias ),
        .O(\Dout[20]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[20]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[20]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[20]_i_7_n_0 ),
        .I4(\Dout[21]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_43 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[20]_i_5 
       (.I0(\Dout[20]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[20]_i_6_n_0 ),
        .I3(\Dout[21]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[20]_i_9_n_0 ),
        .O(\Dout_reg[0]_19 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[20]_i_6 
       (.I0(\Dout[21]_i_10_n_0 ),
        .I1(\Dout[21]_i_11_n_0 ),
        .I2(Q[0]),
        .I3(\Dout[20]_i_10_n_0 ),
        .I4(\Q[1]_repN_2_alias ),
        .I5(\Dout[20]_i_11_n_0 ),
        .O(\Dout[20]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[20]_i_7 
       (.I0(\Dout[20]_i_12_n_0 ),
        .I1(\Q[1]_repN_2_alias ),
        .I2(\Dout[22]_i_11_n_0 ),
        .O(\Dout[20]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[20]_i_8 
       (.I0(\Dout[21]_i_13_n_0 ),
        .I1(\Dout[20]_i_13_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[20]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[20]_i_9 
       (.I0(\Dout[20]_i_14_n_0 ),
        .I1(\Dout[24]_i_13_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[22]_i_13_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[26]_i_13_n_0 ),
        .O(\Dout[20]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h000030300000BB88)) 
    \Dout[21]_i_10 
       (.I0(\Dout_reg[31]_2[27]_repN ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout_reg[31]_2[31]_repN ),
        .I3(\Dout_reg[31]_2 [23]),
        .I4(\Q[4]_repN_alias ),
        .I5(\Q[3]_repN_2_alias ),
        .O(\Dout[21]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h000030300000BB88)) 
    \Dout[21]_i_11 
       (.I0(\Dout_reg[31]_2[25]_repN_1 ),
        .I1(\Q[2]_repN_alias ),
        .I2(\Dout_reg[31]_2[29]_repN ),
        .I3(\Dout_reg[31]_2 [21]),
        .I4(\Q[4]_repN_2_alias ),
        .I5(\Q[3]_repN_8_alias ),
        .O(\Dout[21]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0A0CFFFF0A0C0000)) 
    \Dout[21]_i_12 
       (.I0(\Dout_reg[31]_2 [6]),
        .I1(\Dout_reg[31]_2 [14]),
        .I2(\Q[4]_repN_5_alias ),
        .I3(\Q[3]_repN_3_alias ),
        .I4(\Q[2]_repN_2_alias ),
        .I5(\Dout[25]_i_11_n_0 ),
        .O(\Dout[21]_i_12_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \Dout[21]_i_13 
       (.I0(\Dout[23]_i_12_n_0 ),
        .I1(\Q[1]_repN_alias ),
        .I2(\Dout[21]_i_15_n_0 ),
        .I3(Q[2]),
        .I4(\Dout[21]_i_16_n_0 ),
        .O(\Dout[21]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \Dout[21]_i_14 
       (.I0(\Dout_reg[31]_2 [6]),
        .I1(\Dout[16]_i_9_1 ),
        .I2(\Dout_reg[31]_2 [14]),
        .I3(\Dout[16]_i_9_0 ),
        .O(\Dout[21]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'hF0E2)) 
    \Dout[21]_i_15 
       (.I0(\Dout_reg[31]_2[25]_repN ),
        .I1(\Q[4]_repN_2_alias ),
        .I2(\Dout_reg[31]_2 [31]),
        .I3(\Q[3]_repN_7_alias ),
        .O(\Dout[21]_i_15_n_0 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[21]_i_16 
       (.I0(\Dout_reg[31]_2[29]_repN ),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2 [21]),
        .I3(\Dout_reg[31]_2 [31]),
        .I4(\Q[4]_repN_2_alias ),
        .O(\Dout[21]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[21]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[21]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[21]_i_7_n_0 ),
        .I4(\Dout[22]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_44 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[21]_i_5 
       (.I0(\Dout[21]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[21]_i_6_n_0 ),
        .I3(\Dout[22]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[21]_i_9_n_0 ),
        .O(\Dout_reg[0]_20 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[21]_i_6 
       (.I0(\Dout[21]_i_10_n_0 ),
        .I1(\Q[1]_repN_7_alias ),
        .I2(\Dout[21]_i_11_n_0 ),
        .I3(\Dout[22]_i_10_n_0 ),
        .I4(Q[0]),
        .O(\Dout[21]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[21]_i_7 
       (.I0(\Dout[23]_i_11_n_0 ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout[27]_i_11_n_0 ),
        .I3(\Dout[21]_i_12_n_0 ),
        .I4(\Q[1]_repN_2_alias ),
        .O(\Dout[21]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[21]_i_8 
       (.I0(\Dout[22]_i_12_n_0 ),
        .I1(\Dout[21]_i_13_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[21]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[21]_i_9 
       (.I0(\Dout[21]_i_14_n_0 ),
        .I1(\Dout[25]_i_13_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[23]_i_13_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[27]_i_15_n_0 ),
        .O(\Dout[21]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[22]_i_10 
       (.I0(\Dout_reg[31]_2[28]_repN ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout[25]_i_6 ),
        .I3(\Dout_reg[31]_2 [24]),
        .I4(\Q[1]_repN_2_alias ),
        .I5(\Dout[20]_i_10_n_0 ),
        .O(\Dout[22]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0A0CFFFF0A0C0000)) 
    \Dout[22]_i_11 
       (.I0(\Dout_reg[31]_2 [7]),
        .I1(\Dout_reg[31]_2 [15]),
        .I2(\Q[4]_repN_5_alias ),
        .I3(\Q[3]_repN_3_alias ),
        .I4(\Q[2]_repN_2_alias ),
        .I5(\Dout[26]_i_11_n_0 ),
        .O(\Dout[22]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \Dout[22]_i_12 
       (.I0(\Dout[24]_i_15_n_0 ),
        .I1(\Q[1]_repN_alias ),
        .I2(\Dout[22]_i_14_n_0 ),
        .I3(\Q[2]_repN_alias ),
        .I4(\Dout[22]_i_15_n_0 ),
        .O(\Dout[22]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \Dout[22]_i_13 
       (.I0(\Dout_reg[31]_2 [7]),
        .I1(\Dout[16]_i_9_1 ),
        .I2(\Dout_reg[31]_2 [15]),
        .I3(\Dout[16]_i_9_0 ),
        .O(\Dout[22]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'hF0E2)) 
    \Dout[22]_i_14 
       (.I0(\Dout_reg[31]_2[26]_repN ),
        .I1(\Q[4]_repN_2_alias ),
        .I2(\Dout_reg[31]_2 [31]),
        .I3(\Q[3]_repN_8_alias ),
        .O(\Dout[22]_i_14_n_0 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[22]_i_15 
       (.I0(\Dout_reg[31]_2[30]_repN_3 ),
        .I1(\Q[3]_repN_8_alias ),
        .I2(\Dout_reg[31]_2 [22]),
        .I3(\Dout_reg[31]_2 [31]),
        .I4(\Q[4]_repN_2_alias ),
        .O(\Dout[22]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[22]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[22]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout[22]_i_7_n_0 ),
        .I4(\Dout_reg[1]_7 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_45 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[22]_i_5 
       (.I0(\Dout[22]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[22]_i_6_n_0 ),
        .I3(\Dout[23]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[22]_i_9_n_0 ),
        .O(\Dout_reg[0]_21 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[22]_i_6 
       (.I0(\Dout_reg[29]_1 ),
        .I1(Q[0]),
        .I2(\Dout[22]_i_10_n_0 ),
        .O(\Dout[22]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \Dout[22]_i_7 
       (.I0(\Dout[24]_i_11_n_0 ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout[28]_i_11_n_0 ),
        .I3(\Dout[22]_i_11_n_0 ),
        .I4(\Q[1]_repN_2_alias ),
        .O(\Dout[22]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h00000000B8B8FF00)) 
    \Dout[22]_i_8 
       (.I0(\Dout_reg[29]_3 ),
        .I1(\Q[1]_repN_alias ),
        .I2(\Dout[23]_i_12_n_0 ),
        .I3(\Dout[22]_i_12_n_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_47 [0]),
        .O(\Dout[22]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[22]_i_9 
       (.I0(\Dout[22]_i_13_n_0 ),
        .I1(\Dout[26]_i_13_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[24]_i_13_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[28]_i_14_n_0 ),
        .O(\Dout[22]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[23]_i_10 
       (.I0(\Dout_reg[31]_2[29]_repN_1 ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout[25]_i_6 ),
        .I3(\Dout_reg[31]_2[25]_repN_3 ),
        .I4(\Q[1]_repN_2_alias ),
        .I5(\Dout[21]_i_10_n_0 ),
        .O(\Dout_reg[29]_1 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[23]_i_11 
       (.I0(\Dout_reg[31]_2 [8]),
        .I1(\Q[3]_repN_1_alias ),
        .I2(\Dout_reg[31]_2 [0]),
        .I3(\Q[4]_repN_1_alias ),
        .I4(\Dout_reg[31]_2 [16]),
        .O(\Dout[23]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0000FFB800B8)) 
    \Dout[23]_i_12 
       (.I0(\Dout_reg[31]_2[27]_repN_2 ),
        .I1(\Q[2]_repN_alias ),
        .I2(\Dout_reg[31]_2 [23]),
        .I3(\Q[4]_repN_2_alias ),
        .I4(\Dout_reg[31]_2 [31]),
        .I5(\Q[3]_repN_8_alias ),
        .O(\Dout[23]_i_12_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[23]_i_13 
       (.I0(\Dout_reg[31]_2 [8]),
        .I1(ROTATE_RIGHT1[2]),
        .I2(\Dout_reg[31]_2 [0]),
        .I3(ROTATE_RIGHT1[3]),
        .I4(\Dout_reg[31]_2 [16]),
        .O(\Dout[23]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[23]_i_5 
       (.I0(\Dout[23]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout_reg[23]_1 ),
        .I3(\Dout_reg[9]_0 ),
        .I4(Q[0]),
        .I5(\Dout[23]_i_9_n_0 ),
        .O(\Dout_reg[0]_22 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[23]_i_7 
       (.I0(\Dout[23]_i_11_n_0 ),
        .I1(\Dout[27]_i_11_n_0 ),
        .I2(\Q[1]_repN_2_alias ),
        .I3(\Dout[25]_i_11_n_0 ),
        .I4(\Q[2]_repN_2_alias ),
        .I5(\Dout[29]_i_11_n_0 ),
        .O(\Dout_reg[1]_7 ));
  LUT6 #(
    .INIT(64'h00000000FF00B8B8)) 
    \Dout[23]_i_8 
       (.I0(\Dout_reg[29]_3 ),
        .I1(\Q[1]_repN_7_alias ),
        .I2(\Dout[23]_i_12_n_0 ),
        .I3(\Dout_reg[1]_13 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_47 [0]),
        .O(\Dout[23]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[23]_i_9 
       (.I0(\Dout[23]_i_13_n_0 ),
        .I1(\Dout[27]_i_15_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[25]_i_13_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[29]_i_13_n_0 ),
        .O(\Dout[23]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[24]_i_11 
       (.I0(\Dout_reg[31]_2 [9]),
        .I1(\Q[3]_repN_3_alias ),
        .I2(\Dout_reg[31]_2 [1]),
        .I3(\Q[4]_repN_5_alias ),
        .I4(\Dout_reg[31]_2[17]_repN_2 ),
        .O(\Dout[24]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[24]_i_12 
       (.I0(\Dout_reg[30]_1 ),
        .I1(\Q[1]_repN_7_alias ),
        .I2(\Dout[24]_i_15_n_0 ),
        .O(\Dout_reg[1]_13 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[24]_i_13 
       (.I0(\Dout_reg[31]_2 [9]),
        .I1(ROTATE_RIGHT1[2]),
        .I2(\Dout_reg[31]_2 [1]),
        .I3(ROTATE_RIGHT1[3]),
        .I4(\Dout_reg[31]_2[17]_repN_2 ),
        .O(\Dout[24]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0000FFB800B8)) 
    \Dout[24]_i_15 
       (.I0(\Dout_reg[31]_2[28]_repN_3 ),
        .I1(\Q[2]_repN_alias ),
        .I2(\Dout_reg[31]_2 [24]),
        .I3(\Q[4]_repN_2_alias ),
        .I4(\Dout_reg[31]_2 [31]),
        .I5(\Q[3]_repN_8_alias ),
        .O(\Dout[24]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[24]_i_7 
       (.I0(\Dout[24]_i_11_n_0 ),
        .I1(\Dout[28]_i_11_n_0 ),
        .I2(\Q[1]_repN_2_alias ),
        .I3(\Dout[26]_i_11_n_0 ),
        .I4(\Q[2]_repN_2_alias ),
        .I5(\Dout[30]_i_16_n_0 ),
        .O(\Dout_reg[1]_12 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[24]_i_9 
       (.I0(\Dout[24]_i_13_n_0 ),
        .I1(\Dout[28]_i_14_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[26]_i_13_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[30]_i_25_n_0 ),
        .O(\Dout_reg[9]_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[25]_i_11 
       (.I0(\Dout_reg[31]_2 [10]),
        .I1(\Q[3]_repN_3_alias ),
        .I2(\Dout_reg[31]_2 [2]),
        .I3(\Q[4]_repN_5_alias ),
        .I4(\Dout_reg[31]_2 [18]),
        .O(\Dout[25]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0000FFB800B8)) 
    \Dout[25]_i_12 
       (.I0(\Dout_reg[31]_2[29]_repN_1 ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout_reg[31]_2[25]_repN_3 ),
        .I3(\Q[4]_repN_alias ),
        .I4(\Dout_reg[31]_2[31]_repN ),
        .I5(\Q[3]_repN_2_alias ),
        .O(\Dout_reg[29]_3 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[25]_i_13 
       (.I0(\Dout_reg[31]_2 [10]),
        .I1(ROTATE_RIGHT1[2]),
        .I2(\Dout_reg[31]_2 [2]),
        .I3(ROTATE_RIGHT1[3]),
        .I4(\Dout_reg[31]_2 [18]),
        .O(\Dout[25]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[25]_i_7 
       (.I0(\Dout[25]_i_11_n_0 ),
        .I1(\Dout[29]_i_11_n_0 ),
        .I2(\Q[1]_repN_2_alias ),
        .I3(\Dout[27]_i_11_n_0 ),
        .I4(\Q[2]_repN_2_alias ),
        .I5(\Dout[30]_i_20_n_0 ),
        .O(\Dout_reg[1]_8 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[25]_i_9 
       (.I0(\Dout[25]_i_13_n_0 ),
        .I1(\Dout[29]_i_13_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[27]_i_15_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[30]_i_33_n_0 ),
        .O(\Dout_reg[10]_0 ));
  LUT6 #(
    .INIT(64'h30BB000030880000)) 
    \Dout[26]_i_10 
       (.I0(\Dout_reg[31]_2[28]_repN ),
        .I1(\Q[1]_repN_7_alias ),
        .I2(\Dout_reg[31]_2[30]_repN_1 ),
        .I3(\Q[2]_repN_2_alias ),
        .I4(\Dout[25]_i_6 ),
        .I5(\Dout_reg[31]_2[26]_repN_1 ),
        .O(\Dout_reg[28]_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[26]_i_11 
       (.I0(\Dout_reg[31]_2 [11]),
        .I1(\Q[3]_repN_3_alias ),
        .I2(\Dout_reg[31]_2 [3]),
        .I3(\Q[4]_repN_5_alias ),
        .I4(\Dout_reg[31]_2[19]_repN_2 ),
        .O(\Dout[26]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[26]_i_13 
       (.I0(\Dout_reg[31]_2 [11]),
        .I1(ROTATE_RIGHT1[2]),
        .I2(\Dout_reg[31]_2 [3]),
        .I3(ROTATE_RIGHT1[3]),
        .I4(\Dout_reg[31]_2[19]_repN_2 ),
        .O(\Dout[26]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0000FFB800B8)) 
    \Dout[26]_i_14 
       (.I0(\Dout_reg[31]_2[30]_repN_1 ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout_reg[31]_2[26]_repN_1 ),
        .I3(\Q[4]_repN_alias ),
        .I4(\Dout_reg[31]_2[31]_repN ),
        .I5(\Q[3]_repN_2_alias ),
        .O(\Dout_reg[30]_1 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[26]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[26]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout_reg[1]_9 ),
        .I4(\Dout_reg[1]_10 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_46 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[26]_i_5 
       (.I0(\Dout_reg[26]_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[26]_i_6_n_0 ),
        .I3(\Dout_reg[12]_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[11]_0 ),
        .O(\Dout_reg[0]_23 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[26]_i_6 
       (.I0(\Dout_reg[29]_2 ),
        .I1(Q[0]),
        .I2(\Dout_reg[28]_0 ),
        .O(\Dout[26]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[26]_i_7 
       (.I0(\Dout[26]_i_11_n_0 ),
        .I1(\Dout[30]_i_16_n_0 ),
        .I2(\Q[1]_repN_2_alias ),
        .I3(\Dout[28]_i_11_n_0 ),
        .I4(\Q[2]_repN_2_alias ),
        .I5(\Dout[30]_i_18_n_0 ),
        .O(\Dout_reg[1]_9 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[26]_i_9 
       (.I0(\Dout[26]_i_13_n_0 ),
        .I1(\Dout[30]_i_25_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[28]_i_14_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout_reg[25]_0 ),
        .O(\Dout_reg[11]_0 ));
  LUT6 #(
    .INIT(64'h30BB000030880000)) 
    \Dout[27]_i_10 
       (.I0(\Dout_reg[31]_2[29]_repN_1 ),
        .I1(\Q[1]_repN_7_alias ),
        .I2(\Dout_reg[31]_2[31]_repN ),
        .I3(\Q[2]_repN_2_alias ),
        .I4(\Dout[25]_i_6 ),
        .I5(\Dout_reg[31]_2[27]_repN ),
        .O(\Dout_reg[29]_2 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[27]_i_11 
       (.I0(\Dout_reg[31]_2 [12]),
        .I1(\Q[3]_repN_8_alias ),
        .I2(\Dout_reg[31]_2[4]_repN ),
        .I3(\Q[4]_repN_2_alias ),
        .I4(\Dout_reg[31]_2 [20]),
        .O(\Dout[27]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[27]_i_15 
       (.I0(\Dout_reg[31]_2 [12]),
        .I1(ROTATE_RIGHT1[2]),
        .I2(\Dout_reg[31]_2[4]_repN_1 ),
        .I3(ROTATE_RIGHT1[3]),
        .I4(\Dout_reg[31]_2 [20]),
        .O(\Dout[27]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[27]_i_7 
       (.I0(\Dout[27]_i_11_n_0 ),
        .I1(\Dout[30]_i_20_n_0 ),
        .I2(\Q[1]_repN_2_alias ),
        .I3(\Dout[29]_i_11_n_0 ),
        .I4(\Q[2]_repN_2_alias ),
        .I5(\Dout[30]_i_23_n_0 ),
        .O(\Dout_reg[1]_10 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[27]_i_9 
       (.I0(\Dout[27]_i_15_n_0 ),
        .I1(\Dout[30]_i_33_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[29]_i_13_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[30]_i_32_n_0 ),
        .O(\Dout_reg[12]_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[28]_i_11 
       (.I0(\Dout_reg[31]_2 [13]),
        .I1(\Q[3]_repN_3_alias ),
        .I2(\Dout_reg[31]_2 [5]),
        .I3(\Q[4]_repN_5_alias ),
        .I4(\Dout_reg[31]_2 [21]),
        .O(\Dout[28]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[28]_i_14 
       (.I0(\Dout_reg[31]_2 [13]),
        .I1(ROTATE_RIGHT1[2]),
        .I2(\Dout_reg[31]_2 [5]),
        .I3(ROTATE_RIGHT1[3]),
        .I4(\Dout_reg[31]_2 [21]),
        .O(\Dout[28]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[28]_i_7 
       (.I0(\Dout[28]_i_11_n_0 ),
        .I1(\Dout[30]_i_18_n_0 ),
        .I2(\Q[1]_repN_2_alias ),
        .I3(\Dout[30]_i_16_n_0 ),
        .I4(\Q[2]_repN_2_alias ),
        .I5(\Dout[30]_i_17_n_0 ),
        .O(\Dout_reg[1]_11 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[28]_i_9 
       (.I0(\Dout[28]_i_14_n_0 ),
        .I1(\Dout_reg[25]_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[30]_i_25_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout_reg[27]_0 ),
        .O(\Dout_reg[13]_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[29]_i_11 
       (.I0(\Dout_reg[31]_2 [14]),
        .I1(\Q[3]_repN_3_alias ),
        .I2(\Dout_reg[31]_2 [6]),
        .I3(\Q[4]_repN_5_alias ),
        .I4(\Dout_reg[31]_2[22]_repN ),
        .O(\Dout[29]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[29]_i_13 
       (.I0(\Dout_reg[31]_2 [14]),
        .I1(ROTATE_RIGHT1[2]),
        .I2(\Dout_reg[31]_2 [6]),
        .I3(ROTATE_RIGHT1[3]),
        .I4(\Dout_reg[31]_2[22]_repN ),
        .O(\Dout[29]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8B8FF33CC00)) 
    \Dout[29]_i_7 
       (.I0(\Dout[29]_i_11_n_0 ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout[30]_i_23_n_0 ),
        .I3(\Dout[30]_i_20_n_0 ),
        .I4(\Dout[30]_i_21_n_0 ),
        .I5(\Q[1]_repN_7_alias ),
        .O(\Dout_reg[2]_4 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[29]_i_9 
       (.I0(\Dout[29]_i_13_n_0 ),
        .I1(\Dout[30]_i_32_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[30]_i_33_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[30]_i_34_n_0 ),
        .O(\Dout_reg[14]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[2]_i_10 
       (.I0(\Dout[5]_i_27_n_0 ),
        .I1(\Dout[4]_i_12_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[5]_i_21_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[2]_i_12_n_0 ),
        .O(\Dout[2]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[2]_i_12 
       (.I0(\Dout_reg[31]_2[2]_repN ),
        .I1(\Dout_reg[31]_2 [18]),
        .I2(\Q[3]_repN_alias ),
        .I3(\Dout_reg[31]_2 [26]),
        .I4(\Q[4]_repN_2_alias ),
        .I5(\Dout_reg[31]_2 [10]),
        .O(\Dout[2]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Dout[2]_i_4__0 
       (.I0(\Dout_reg[31]_2 [31]),
        .I1(\Dout_reg[2]_i_2_0 [31]),
        .O(\Dout[2]_i_4__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Dout[2]_i_5__0 
       (.I0(\Dout_reg[31]_2 [31]),
        .I1(\Dout_reg[2]_i_2_0 [31]),
        .O(\Dout[2]_i_5__0_n_0 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT4 #(
    .INIT(16'hFFD0)) 
    \Dout[2]_i_5_comp 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[1]_i_5_0 ),
        .I3(\Dout[2]_i_3_n_0_alias ),
        .O(\Dout_reg[0]_30_repN ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT5 #(
    .INIT(32'hFF30FF20)) 
    \Dout[2]_i_5_comp_1 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[1]_i_5_0 ),
        .I3(\Dout[2]_i_3_n_0_alias ),
        .I4(\Dout[2]_i_10_n_0 ),
        .O(\Dout_reg[0]_30 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[2]_i_7 
       (.I0(\Dout[5]_i_19_n_0 ),
        .I1(\Dout[4]_i_12_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[5]_i_21_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[2]_i_12_n_0 ),
        .O(\Dout_reg[1]_6 ));
  LUT6 #(
    .INIT(64'hB8B8B8B8FF33CC00)) 
    \Dout[30]_i_10 
       (.I0(\Dout[30]_i_16_n_0 ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout[30]_i_17_n_0 ),
        .I3(\Dout[30]_i_18_n_0 ),
        .I4(\Dout[30]_i_19_n_0 ),
        .I5(\Q[1]_repN_2_alias ),
        .O(\Dout_reg[2]_3 ));
  LUT6 #(
    .INIT(64'hB8B8B8B8FFCC3300)) 
    \Dout[30]_i_11 
       (.I0(\Dout[30]_i_20_n_0 ),
        .I1(\Q[2]_repN_2_alias ),
        .I2(\Dout[30]_i_21_n_0 ),
        .I3(\Dout[30]_i_22_n_0 ),
        .I4(\Dout[30]_i_23_n_0 ),
        .I5(\Q[1]_repN_7_alias ),
        .O(\Dout_reg[2]_1 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[30]_i_13 
       (.I0(\Dout[30]_i_25_n_0 ),
        .I1(\Dout_reg[27]_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout_reg[25]_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout_reg[29]_0 ),
        .O(\Dout_reg[15]_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_14 
       (.I0(\Dout[30]_i_31_n_0 ),
        .I1(\Dout[30]_i_32_n_0 ),
        .I2(ROTATE_RIGHT1[0]),
        .I3(\Dout[30]_i_33_n_0 ),
        .I4(ROTATE_RIGHT1[1]),
        .I5(\Dout[30]_i_34_n_0 ),
        .O(\Dout_reg[30]_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[30]_i_16 
       (.I0(\Dout_reg[31]_2 [15]),
        .I1(\Q[3]_repN_3_alias ),
        .I2(\Dout_reg[31]_2 [7]),
        .I3(\Q[4]_repN_5_alias ),
        .I4(\Dout_reg[31]_2 [23]),
        .O(\Dout[30]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_17 
       (.I0(\Dout_reg[31]_2[27]_repN ),
        .I1(\Dout_reg[31]_2 [11]),
        .I2(\Q[3]_repN_2_alias ),
        .I3(\Dout_reg[31]_2 [3]),
        .I4(\Q[4]_repN_alias ),
        .I5(\Dout_reg[31]_2[19]_repN_2 ),
        .O(\Dout[30]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_18 
       (.I0(\Dout_reg[31]_2[25]_repN_3 ),
        .I1(\Dout_reg[31]_2 [9]),
        .I2(\Q[3]_repN_3_alias ),
        .I3(\Dout_reg[31]_2 [1]),
        .I4(\Q[4]_repN_5_alias ),
        .I5(\Dout_reg[31]_2[17]_repN_2 ),
        .O(\Dout[30]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_19 
       (.I0(\Dout_reg[31]_2[29]_repN_1 ),
        .I1(\Dout_reg[31]_2 [13]),
        .I2(\Q[3]_repN_2_alias ),
        .I3(\Dout_reg[31]_2 [5]),
        .I4(\Q[4]_repN_alias ),
        .I5(\Dout_reg[31]_2 [21]),
        .O(\Dout[30]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_20 
       (.I0(\Dout_reg[31]_2 [24]),
        .I1(\Dout_reg[31]_2 [8]),
        .I2(\Q[3]_repN_3_alias ),
        .I3(\Dout_reg[31]_2 [0]),
        .I4(\Q[4]_repN_5_alias ),
        .I5(\Dout_reg[31]_2 [16]),
        .O(\Dout[30]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_21 
       (.I0(\Dout_reg[31]_2[28]_repN ),
        .I1(\Dout_reg[31]_2 [12]),
        .I2(\Q[3]_repN_2_alias ),
        .I3(\Dout_reg[31]_2[4]_repN_1 ),
        .I4(\Q[4]_repN_alias ),
        .I5(\Dout_reg[31]_2 [20]),
        .O(\Dout[30]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_22 
       (.I0(\Dout_reg[31]_2[30]_repN_1 ),
        .I1(\Dout_reg[31]_2 [14]),
        .I2(\Q[3]_repN_3_alias ),
        .I3(\Dout_reg[31]_2 [6]),
        .I4(\Q[4]_repN_5_alias ),
        .I5(\Dout_reg[31]_2[22]_repN ),
        .O(\Dout[30]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_23 
       (.I0(\Dout_reg[31]_2[26]_repN_1 ),
        .I1(\Dout_reg[31]_2 [10]),
        .I2(\Q[3]_repN_2_alias ),
        .I3(\Dout_reg[31]_2 [2]),
        .I4(\Q[4]_repN_alias ),
        .I5(\Dout_reg[31]_2 [18]),
        .O(\Dout[30]_i_23_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[30]_i_25 
       (.I0(\Dout_reg[31]_2 [15]),
        .I1(ROTATE_RIGHT1[2]),
        .I2(\Dout_reg[31]_2 [7]),
        .I3(ROTATE_RIGHT1[3]),
        .I4(\Dout_reg[31]_2 [23]),
        .O(\Dout[30]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_26 
       (.I0(\Dout_reg[31]_2[27]_repN ),
        .I1(\Dout_reg[31]_2 [11]),
        .I2(ROTATE_RIGHT1[2]),
        .I3(\Dout_reg[31]_2 [3]),
        .I4(ROTATE_RIGHT1[3]),
        .I5(\Dout_reg[31]_2[19]_repN_3 ),
        .O(\Dout_reg[27]_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_28 
       (.I0(\Dout_reg[31]_2[25]_repN_3 ),
        .I1(\Dout_reg[31]_2 [9]),
        .I2(ROTATE_RIGHT1[2]),
        .I3(\Dout_reg[31]_2 [1]),
        .I4(ROTATE_RIGHT1[3]),
        .I5(\Dout_reg[31]_2[17]_repN_2 ),
        .O(\Dout_reg[25]_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_30 
       (.I0(\Dout_reg[31]_2[29]_repN_1 ),
        .I1(\Dout_reg[31]_2 [13]),
        .I2(ROTATE_RIGHT1[2]),
        .I3(\Dout_reg[31]_2 [5]),
        .I4(ROTATE_RIGHT1[3]),
        .I5(\Dout_reg[31]_2 [21]),
        .O(\Dout_reg[29]_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_31 
       (.I0(\Dout_reg[31]_2[30]_repN_1 ),
        .I1(\Dout_reg[31]_2 [14]),
        .I2(ROTATE_RIGHT1[2]),
        .I3(\Dout_reg[31]_2 [6]),
        .I4(ROTATE_RIGHT1[3]),
        .I5(\Dout_reg[31]_2[22]_repN ),
        .O(\Dout[30]_i_31_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_32 
       (.I0(\Dout_reg[31]_2[26]_repN_1 ),
        .I1(\Dout_reg[31]_2 [10]),
        .I2(ROTATE_RIGHT1[2]),
        .I3(\Dout_reg[31]_2 [2]),
        .I4(ROTATE_RIGHT1[3]),
        .I5(\Dout_reg[31]_2 [18]),
        .O(\Dout[30]_i_32_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_33 
       (.I0(\Dout_reg[31]_2 [24]),
        .I1(\Dout_reg[31]_2 [8]),
        .I2(ROTATE_RIGHT1[2]),
        .I3(\Dout_reg[31]_2 [0]),
        .I4(ROTATE_RIGHT1[3]),
        .I5(\Dout_reg[31]_2 [16]),
        .O(\Dout[30]_i_33_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[30]_i_34 
       (.I0(\Dout_reg[31]_2[28]_repN ),
        .I1(\Dout_reg[31]_2 [12]),
        .I2(ROTATE_RIGHT1[2]),
        .I3(\Dout_reg[31]_2[4]_repN_1 ),
        .I4(ROTATE_RIGHT1[3]),
        .I5(\Dout_reg[31]_2 [20]),
        .O(\Dout[30]_i_34_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[3]_i_10 
       (.I0(\Dout[5]_i_30_n_0 ),
        .I1(\Dout[5]_i_17_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[5]_i_15_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[3]_i_12_n_0 ),
        .O(\Dout[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[3]_i_12 
       (.I0(\Dout_reg[31]_2 [3]),
        .I1(\Dout_reg[31]_2[19]_repN_1 ),
        .I2(\Q[3]_repN_alias ),
        .I3(\Dout_reg[31]_2[27]_repN_1 ),
        .I4(\Q[4]_repN_2_alias ),
        .I5(\Dout_reg[31]_2 [11]),
        .O(\Dout[3]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[3]_i_5 
       (.I0(\Dout[4]_i_10_n_0 ),
        .I1(\Dout[3]_i_10_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout_reg[0]_31 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[3]_i_7 
       (.I0(\Dout[5]_i_16_n_0 ),
        .I1(\Dout[5]_i_17_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[5]_i_15_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[3]_i_12_n_0 ),
        .O(\Dout_reg[1]_4 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[4]_i_10 
       (.I0(\Dout[5]_i_28_n_0 ),
        .I1(\Dout[5]_i_21_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[5]_i_27_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[4]_i_12_n_0 ),
        .O(\Dout[4]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[4]_i_12 
       (.I0(\Dout_reg[31]_2 [4]),
        .I1(\Dout_reg[31]_2 [20]),
        .I2(\Q[3]_repN_7_alias ),
        .I3(\Dout_reg[31]_2[28]_repN_1 ),
        .I4(\Q[4]_repN_1_alias ),
        .I5(\Dout_reg[31]_2 [12]),
        .O(\Dout[4]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[4]_i_5 
       (.I0(\Dout[5]_i_12_n_0 ),
        .I1(\Dout[4]_i_10_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout_reg[0]_32 ));
  LUT6 #(
    .INIT(64'hFFEEF3E200000000)) 
    \Dout[4]_i_6 
       (.I0(\Dout[5]_i_13_n_0 ),
        .I1(Q[0]),
        .I2(\Dout_reg[4]_0 ),
        .I3(\Dout_reg[1]_0 ),
        .I4(\Dout_reg[1]_1 ),
        .I5(\Dout_reg[0]_47 [0]),
        .O(\Dout_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[4]_i_7 
       (.I0(\Dout[5]_i_20_n_0 ),
        .I1(\Dout[5]_i_21_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[5]_i_19_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[4]_i_12_n_0 ),
        .O(\Dout_reg[1]_0 ));
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "CRITICAL_CELL_OPT" *) 
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[5]_i_11 
       (.I0(\Dout[5]_i_26_n_0 ),
        .I1(\Dout[5]_i_27_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[5]_i_28_n_0 ),
        .I4(\Q[2]_repN_4_alias ),
        .I5(\Dout[5]_i_21_n_0 ),
        .O(\Dout[5]_i_11_n_0 ));
  (* ORIG_CELL_NAME = "Dout[5]_i_11" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[5]_i_11_replica 
       (.I0(\Dout[5]_i_26_n_0 ),
        .I1(\Dout[5]_i_27_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[5]_i_28_n_0 ),
        .I4(\Q[2]_repN_4_alias ),
        .I5(\Dout[5]_i_21_n_0 ),
        .O(\Dout[5]_i_11_n_0_repN ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[5]_i_12 
       (.I0(\Dout[5]_i_29_n_0 ),
        .I1(\Dout[5]_i_15_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[5]_i_30_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[5]_i_17_n_0 ),
        .O(\Dout[5]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h30BB000030880000)) 
    \Dout[5]_i_13 
       (.I0(\Dout_reg[31]_2[2]_repN ),
        .I1(ROTATE_RIGHT1[0]),
        .I2(\Dout_reg[31]_2 [0]),
        .I3(ROTATE_RIGHT1[1]),
        .I4(\Dout[16]_i_9_0 ),
        .I5(\Dout_reg[31]_2 [4]),
        .O(\Dout[5]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[5]_i_14 
       (.I0(\Dout_reg[31]_2 [19]),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2[27]_repN_1 ),
        .I3(\Q[4]_repN_2_alias ),
        .I4(\Dout_reg[31]_2 [11]),
        .O(\Dout[5]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hFCFC0C0CFA0AFA0A)) 
    \Dout[5]_i_15 
       (.I0(\Dout_reg[31]_2 [7]),
        .I1(\Dout_reg[31]_2 [23]),
        .I2(\Q[3]_repN_7_alias ),
        .I3(\Dout_reg[31]_2 [15]),
        .I4(\Dout_reg[31]_2 [31]),
        .I5(\Q[4]_repN_2_alias ),
        .O(\Dout[5]_i_15_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[5]_i_16 
       (.I0(\Dout_reg[31]_2[17]_repN ),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2[25]_repN ),
        .I3(Q[4]),
        .I4(\Dout_reg[31]_2 [9]),
        .O(\Dout[5]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[5]_i_17 
       (.I0(\Dout_reg[31]_2 [5]),
        .I1(\Dout_reg[31]_2 [21]),
        .I2(\Q[3]_repN_7_alias ),
        .I3(\Dout_reg[31]_2[29]_repN ),
        .I4(\Q[4]_repN_2_alias ),
        .I5(\Dout_reg[31]_2 [13]),
        .O(\Dout[5]_i_17_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[5]_i_18 
       (.I0(\Dout_reg[31]_2 [20]),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2[28]_repN_1 ),
        .I3(\Q[4]_repN_3_alias ),
        .I4(\Dout_reg[31]_2 [12]),
        .O(\Dout[5]_i_18_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[5]_i_19 
       (.I0(\Dout_reg[31]_2 [16]),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2 [24]),
        .I3(\Q[4]_repN_3_alias ),
        .I4(\Dout_reg[31]_2 [8]),
        .O(\Dout[5]_i_19_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \Dout[5]_i_20 
       (.I0(\Dout_reg[31]_2 [18]),
        .I1(\Q[3]_repN_7_alias ),
        .I2(\Dout_reg[31]_2 [26]),
        .I3(\Q[4]_repN_2_alias ),
        .I4(\Dout_reg[31]_2 [10]),
        .O(\Dout[5]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'hFC0CFAFAFC0C0A0A)) 
    \Dout[5]_i_21 
       (.I0(\Dout_reg[31]_2 [6]),
        .I1(\Dout_reg[31]_2 [22]),
        .I2(Q[3]),
        .I3(\Dout_reg[31]_2[30]_repN_4 ),
        .I4(\Q[4]_repN_1_alias ),
        .I5(\Dout_reg[31]_2 [14]),
        .O(\Dout[5]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'hCFC0AFAFCFC0A0A0)) 
    \Dout[5]_i_26 
       (.I0(\Dout_reg[31]_2 [20]),
        .I1(\Dout_reg[31]_2 [31]),
        .I2(\Q[3]_repN_7_alias ),
        .I3(\Dout_reg[31]_2[28]_repN_1 ),
        .I4(\Q[4]_repN_3_alias ),
        .I5(\Dout_reg[31]_2 [12]),
        .O(\Dout[5]_i_26_n_0 ));
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  LUT6 #(
    .INIT(64'hCFC0AFAFCFC0A0A0)) 
    \Dout[5]_i_27 
       (.I0(\Dout_reg[31]_2 [16]),
        .I1(\Dout_reg[31]_2 [31]),
        .I2(\Q[3]_repN_7_alias ),
        .I3(\Dout_reg[31]_2 [24]),
        .I4(\Q[4]_repN_1_alias ),
        .I5(\Dout_reg[31]_2 [8]),
        .O(\Dout[5]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'hCFC0AFAFCFC0A0A0)) 
    \Dout[5]_i_28 
       (.I0(\Dout_reg[31]_2 [18]),
        .I1(\Dout_reg[31]_2 [31]),
        .I2(\Q[3]_repN_7_alias ),
        .I3(\Dout_reg[31]_2 [26]),
        .I4(\Q[4]_repN_2_alias ),
        .I5(\Dout_reg[31]_2 [10]),
        .O(\Dout[5]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'hCFC0AFAFCFC0A0A0)) 
    \Dout[5]_i_29 
       (.I0(\Dout_reg[31]_2 [19]),
        .I1(\Dout_reg[31]_2 [31]),
        .I2(\Q[3]_repN_7_alias ),
        .I3(\Dout_reg[31]_2[27]_repN_1 ),
        .I4(\Q[4]_repN_2_alias ),
        .I5(\Dout_reg[31]_2 [11]),
        .O(\Dout[5]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'hCFC0AFAFCFC0A0A0)) 
    \Dout[5]_i_30 
       (.I0(\Dout_reg[31]_2[17]_repN ),
        .I1(\Dout_reg[31]_2 [31]),
        .I2(\Q[3]_repN_7_alias ),
        .I3(\Dout_reg[31]_2[25]_repN_2 ),
        .I4(\Q[4]_repN_2_alias ),
        .I5(\Dout_reg[31]_2 [9]),
        .O(\Dout[5]_i_30_n_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[5]_i_5 
       (.I0(\Dout[5]_i_11_n_0_repN ),
        .I1(\Dout[5]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout_reg[0]_33 ));
  LUT6 #(
    .INIT(64'hFFFCBBB800000000)) 
    \Dout[5]_i_6 
       (.I0(\Dout[5]_i_13_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[6]_i_9_n_0 ),
        .I3(\Dout_reg[1]_1 ),
        .I4(\Dout_reg[1]_2 ),
        .I5(\Dout_reg[0]_47 [0]),
        .O(\Dout_reg[0]_1 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[5]_i_7 
       (.I0(\Dout[5]_i_14_n_0 ),
        .I1(\Dout[5]_i_15_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[5]_i_16_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[5]_i_17_n_0 ),
        .O(\Dout_reg[1]_1 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[5]_i_8 
       (.I0(\Dout[5]_i_18_n_0 ),
        .I1(\Dout[5]_i_19_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[5]_i_20_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[5]_i_21_n_0 ),
        .O(\Dout_reg[1]_2 ));
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT RESTRUCT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "CRITICAL_CELL_OPT" *) 
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[6]_i_5 
       (.I0(\Dout[6]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout_reg[0]_3 ),
        .I3(\Dout_reg[6]_0 ),
        .I4(Q[0]),
        .I5(\Dout[6]_i_9_n_0 ),
        .O(\Dout_reg[0]_2 ));
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "CRITICAL_CELL_OPT" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[6]_i_6 
       (.I0(\Dout[7]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout_reg[1]_2 ),
        .O(\Dout_reg[0]_3 ));
  LUT6 #(
    .INIT(64'h30BB000030880000)) 
    \Dout[6]_i_7 
       (.I0(\Dout_reg[31]_2 [3]),
        .I1(\Q[1]_repN_3_alias ),
        .I2(\Dout_reg[31]_2 [1]),
        .I3(\Q[2]_repN_4_alias ),
        .I4(\Dout[25]_i_6 ),
        .I5(\Dout_reg[31]_2 [5]),
        .O(\Dout_reg[3]_0 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[6]_i_8 
       (.I0(\Dout[7]_i_12_n_0 ),
        .I1(\Dout[5]_i_11_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[6]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h30BB000030880000)) 
    \Dout[6]_i_9 
       (.I0(\Dout_reg[31]_2 [3]),
        .I1(ROTATE_RIGHT1[0]),
        .I2(\Dout_reg[31]_2 [1]),
        .I3(ROTATE_RIGHT1[1]),
        .I4(\Dout[16]_i_9_0 ),
        .I5(\Dout_reg[31]_2 [5]),
        .O(\Dout[6]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[7]_i_10 
       (.I0(\Dout[13]_i_14_n_0 ),
        .I1(\Dout[5]_i_16_n_0 ),
        .I2(\Q[1]_repN_alias ),
        .I3(\Dout[5]_i_14_n_0 ),
        .I4(\Q[2]_repN_7_alias ),
        .I5(\Dout[5]_i_15_n_0 ),
        .O(\Dout[7]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[7]_i_12 
       (.I0(\Dout[13]_i_15_n_0 ),
        .I1(\Dout[5]_i_30_n_0 ),
        .I2(\Q[1]_repN_6_alias ),
        .I3(\Dout[5]_i_29_n_0 ),
        .I4(\Q[2]_repN_6_alias ),
        .I5(\Dout[5]_i_15_n_0 ),
        .O(\Dout[7]_i_12_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[7]_i_6 
       (.I0(\Dout[8]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[7]_i_10_n_0 ),
        .O(\Dout_reg[0]_37 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[7]_i_8 
       (.I0(\Dout[8]_i_12_n_0 ),
        .I1(\Dout[7]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout_reg[0]_36 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[8]_i_10 
       (.I0(\Dout[14]_i_14_n_0 ),
        .I1(\Dout[5]_i_20_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[5]_i_18_n_0 ),
        .I4(\Q[2]_repN_4_alias ),
        .I5(\Dout[5]_i_19_n_0 ),
        .O(\Dout[8]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[8]_i_12 
       (.I0(\Dout[14]_i_15_n_0 ),
        .I1(\Dout[5]_i_28_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[5]_i_26_n_0 ),
        .I4(\Q[2]_repN_4_alias ),
        .I5(\Dout[5]_i_27_n_0 ),
        .O(\Dout[8]_i_12_n_0 ));
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "CRITICAL_CELL_OPT" *) 
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[8]_i_5 
       (.I0(\Dout[8]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout_reg[0]_8 ),
        .I3(\Dout[9]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[8]_0 ),
        .O(\Dout_reg[0]_7 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[8]_i_6 
       (.I0(\Dout[9]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[8]_i_10_n_0 ),
        .O(\Dout_reg[0]_8 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[8]_i_8 
       (.I0(\Dout[9]_i_11_n_0 ),
        .I1(\Dout[8]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[8]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[9]_i_10 
       (.I0(\Dout[15]_i_14_n_0 ),
        .I1(\Dout[5]_i_14_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[13]_i_14_n_0 ),
        .I4(\Q[2]_repN_3_alias ),
        .I5(\Dout[5]_i_16_n_0 ),
        .O(\Dout[9]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \Dout[9]_i_11 
       (.I0(\Dout[15]_i_16_n_0 ),
        .I1(\Dout[5]_i_29_n_0 ),
        .I2(\Q[1]_repN_3_alias ),
        .I3(\Dout[13]_i_15_n_0 ),
        .I4(\Q[2]_repN_4_alias ),
        .I5(\Dout[5]_i_30_n_0 ),
        .O(\Dout[9]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[9]_i_2 
       (.I0(\Dout_reg[9]_1 ),
        .I1(\Dout[9]_i_6_n_0 ),
        .I2(\Dout_reg[0]_47 [0]),
        .I3(\Dout_reg[2]_0 ),
        .I4(\Dout[10]_i_7_n_0 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_6 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[9]_i_5 
       (.I0(\Dout[9]_i_8_n_0 ),
        .I1(\Dout_reg[0]_47 [0]),
        .I2(\Dout[9]_i_6_n_0 ),
        .I3(\Dout[10]_i_9_n_0 ),
        .I4(Q[0]),
        .I5(\Dout[9]_i_9_n_0 ),
        .O(\Dout_reg[0]_5 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[9]_i_6 
       (.I0(\Dout[10]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[9]_i_10_n_0 ),
        .O(\Dout[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[9]_i_7 
       (.I0(\Dout_reg[31]_2[2]_repN ),
        .I1(\Q[2]_repN_4_alias ),
        .I2(\Dout[25]_i_6 ),
        .I3(\Dout_reg[31]_2 [6]),
        .I4(\Q[1]_repN_3_alias ),
        .I5(\Dout[11]_i_11_n_0 ),
        .O(\Dout_reg[2]_0 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[9]_i_8 
       (.I0(\Dout[10]_i_11_n_0 ),
        .I1(\Dout[9]_i_11_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_47 [0]),
        .O(\Dout[9]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[9]_i_9 
       (.I0(\Dout_reg[31]_2[2]_repN ),
        .I1(ROTATE_RIGHT1[1]),
        .I2(\Dout[16]_i_9_0 ),
        .I3(\Dout_reg[31]_2 [6]),
        .I4(ROTATE_RIGHT1[0]),
        .I5(\Dout[11]_i_13_n_0 ),
        .O(\Dout[9]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[0]),
        .Q(\Dout_reg[31]_2 [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[10]),
        .Q(\Dout_reg[31]_2 [10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[11]),
        .Q(\Dout_reg[31]_2 [11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[12]),
        .Q(\Dout_reg[31]_2 [12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[13]),
        .Q(\Dout_reg[31]_2 [13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[14]),
        .Q(\Dout_reg[31]_2 [14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[15]),
        .Q(\Dout_reg[31]_2 [15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[16]),
        .Q(\Dout_reg[31]_2 [16]),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[17]),
        .Q(\Dout_reg[31]_2 [17]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[17]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[17]),
        .Q(\Dout_reg[31]_2[17]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[17]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[17]),
        .Q(\Dout_reg[31]_2[17]_repN_1 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[17]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17]_replica_2 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[17]),
        .Q(\Dout_reg[31]_2[17]_repN_2 ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[18]),
        .Q(\Dout_reg[31]_2 [18]),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[19]),
        .Q(\Dout_reg[31]_2 [19]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[19]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[19]),
        .Q(\Dout_reg[31]_2[19]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[19]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[19]),
        .Q(\Dout_reg[31]_2[19]_repN_1 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[19]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19]_replica_2 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[19]),
        .Q(\Dout_reg[31]_2[19]_repN_2 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[19]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19]_replica_3 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[19]),
        .Q(\Dout_reg[31]_2[19]_repN_3 ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(\Dout_reg[31]_2 [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[20]),
        .Q(\Dout_reg[31]_2 [20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[21]),
        .Q(\Dout_reg[31]_2 [21]),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[22]),
        .Q(\Dout_reg[31]_2 [22]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[22]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[22]),
        .Q(\Dout_reg[31]_2[22]_repN ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[23]),
        .Q(\Dout_reg[31]_2 [23]),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[24]),
        .Q(\Dout_reg[31]_2 [24]),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[25]),
        .Q(\Dout_reg[31]_2 [25]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[25]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[25]),
        .Q(\Dout_reg[31]_2[25]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[25]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[25]),
        .Q(\Dout_reg[31]_2[25]_repN_1 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[25]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25]_replica_2 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[25]),
        .Q(\Dout_reg[31]_2[25]_repN_2 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[25]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25]_replica_3 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[25]),
        .Q(\Dout_reg[31]_2[25]_repN_3 ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "PLACEMENT_OPT FANOUT_OPT CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[26]),
        .Q(\Dout_reg[31]_2 [26]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[26]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[26]),
        .Q(\Dout_reg[31]_2[26]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[26]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[26]),
        .Q(\Dout_reg[31]_2[26]_repN_1 ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "PLACEMENT_OPT FANOUT_OPT CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[27]),
        .Q(\Dout_reg[31]_2 [27]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[27]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[27]),
        .Q(\Dout_reg[31]_2[27]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[27]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[27]),
        .Q(\Dout_reg[31]_2[27]_repN_1 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[27]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27]_replica_2 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[27]),
        .Q(\Dout_reg[31]_2[27]_repN_2 ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT PLACEMENT_OPT CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[28]),
        .Q(\Dout_reg[31]_2 [28]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[28]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[28]),
        .Q(\Dout_reg[31]_2[28]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[28]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[28]),
        .Q(\Dout_reg[31]_2[28]_repN_1 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[28]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28]_replica_2 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[28]),
        .Q(\Dout_reg[31]_2[28]_repN_2 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[28]" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28]_replica_3 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[28]),
        .Q(\Dout_reg[31]_2[28]_repN_3 ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[29]),
        .Q(\Dout_reg[31]_2 [29]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[29]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[29]),
        .Q(\Dout_reg[31]_2[29]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[29]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[29]),
        .Q(\Dout_reg[31]_2[29]_repN_1 ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Dout_reg[31]_2 [2]),
        .R(SR));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \Dout_reg[2]_i_2 
       (.CI(1'b0),
        .CO(\NLW_Dout_reg[2]_i_2_CO_UNCONNECTED [3:0]),
        .CYINIT(\Dout_reg[31]_0 ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_Dout_reg[2]_i_2_O_UNCONNECTED [3:2],O,\NLW_Dout_reg[2]_i_2_O_UNCONNECTED [0]}),
        .S({1'b0,1'b0,\Dout[2]_i_4__0_n_0 ,1'b1}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \Dout_reg[2]_i_3 
       (.CI(1'b0),
        .CO(\NLW_Dout_reg[2]_i_3_CO_UNCONNECTED [3:0]),
        .CYINIT(\Dout_reg[2]_5 ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_Dout_reg[2]_i_3_O_UNCONNECTED [3:2],\Dout_reg[31]_1 ,\NLW_Dout_reg[2]_i_3_O_UNCONNECTED [0]}),
        .S({1'b0,1'b0,\Dout[2]_i_5__0_n_0 ,1'b1}));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Dout_reg[31]_2[2]_repN ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[30]),
        .Q(\Dout_reg[31]_2 [30]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[30]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[30]),
        .Q(\Dout_reg[31]_2[30]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[30]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[30]),
        .Q(\Dout_reg[31]_2[30]_repN_1 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[30]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30]_replica_2 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[30]),
        .Q(\Dout_reg[31]_2[30]_repN_2 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[30]_replica" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30]_replica_3 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[30]),
        .Q(\Dout_reg[31]_2[30]_repN_3 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[30]_replica" *) 
  (* PHYS_OPT_MODIFIED = "CRITICAL_CELL_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30]_replica_4 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[30]),
        .Q(\Dout_reg[31]_2[30]_repN_4 ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[31]),
        .Q(\Dout_reg[31]_2 [31]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[31]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[31]),
        .Q(\Dout_reg[31]_2[31]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[31]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[31]),
        .Q(\Dout_reg[31]_2[31]_repN_1 ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(\Dout_reg[31]_2 [3]),
        .R(SR));
  CARRY4 \Dout_reg[3]_i_4 
       (.CI(CO),
        .CO({\NLW_Dout_reg[3]_i_4_CO_UNCONNECTED [3:1],\Dout_reg[31]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_Dout_reg[3]_i_4_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Dout_reg[31]_2 [4]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Dout_reg[31]_2[4]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Dout_reg[31]_2[4]_repN_1 ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "PLACEMENT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[5]),
        .Q(\Dout_reg[31]_2 [5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[6]),
        .Q(\Dout_reg[31]_2 [6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[7]),
        .Q(\Dout_reg[31]_2 [7]),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "PLACEMENT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[8]),
        .Q(\Dout_reg[31]_2 [8]),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "PLACEMENT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[9]),
        .Q(\Dout_reg[31]_2 [9]),
        .R(SR));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__0_i_1__0
       (.I0(\Dout_reg[31]_2 [7]),
        .I1(\Dout_reg[2]_i_2_0 [7]),
        .O(\Dout_reg[7]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__0_i_2__0
       (.I0(\Dout_reg[31]_2 [6]),
        .I1(\Dout_reg[2]_i_2_0 [6]),
        .O(\Dout_reg[7]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__0_i_3__0
       (.I0(\Dout_reg[31]_2 [5]),
        .I1(\Dout_reg[2]_i_2_0 [5]),
        .O(\Dout_reg[7]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__0_i_4__0
       (.I0(\Dout_reg[31]_2 [4]),
        .I1(\Dout_reg[2]_i_2_0 [4]),
        .O(\Dout_reg[7]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__1_i_1__0
       (.I0(\Dout_reg[31]_2 [11]),
        .I1(\Dout_reg[2]_i_2_0 [11]),
        .O(\Dout_reg[11]_1 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__1_i_2__0
       (.I0(\Dout_reg[31]_2 [10]),
        .I1(\Dout_reg[2]_i_2_0 [10]),
        .O(\Dout_reg[11]_1 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__1_i_3__0
       (.I0(\Dout_reg[31]_2 [9]),
        .I1(\Dout_reg[2]_i_2_0 [9]),
        .O(\Dout_reg[11]_1 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__1_i_4__0
       (.I0(\Dout_reg[31]_2 [8]),
        .I1(\Dout_reg[2]_i_2_0 [8]),
        .O(\Dout_reg[11]_1 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__2_i_1__0
       (.I0(\Dout_reg[31]_2 [15]),
        .I1(\Dout_reg[2]_i_2_0 [15]),
        .O(\Dout_reg[15]_1 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__2_i_2__0
       (.I0(\Dout_reg[31]_2 [14]),
        .I1(\Dout_reg[2]_i_2_0 [14]),
        .O(\Dout_reg[15]_1 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__2_i_3__0
       (.I0(\Dout_reg[31]_2 [13]),
        .I1(\Dout_reg[2]_i_2_0 [13]),
        .O(\Dout_reg[15]_1 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__2_i_4__0
       (.I0(\Dout_reg[31]_2 [12]),
        .I1(\Dout_reg[2]_i_2_0 [12]),
        .O(\Dout_reg[15]_1 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__3_i_1__0
       (.I0(\Dout_reg[31]_2[19]_repN_1 ),
        .I1(\Dout_reg[2]_i_2_0 [19]),
        .O(\Dout_reg[19]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__3_i_2__0
       (.I0(\Dout_reg[31]_2 [18]),
        .I1(\Dout_reg[2]_i_2_0 [18]),
        .O(\Dout_reg[19]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__3_i_3__0
       (.I0(\Dout_reg[31]_2[17]_repN ),
        .I1(\Dout_reg[2]_i_2_0 [17]),
        .O(\Dout_reg[19]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__3_i_4__0
       (.I0(\Dout_reg[31]_2 [16]),
        .I1(\Dout_reg[2]_i_2_0 [16]),
        .O(\Dout_reg[19]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__4_i_1__0
       (.I0(\Dout_reg[31]_2 [23]),
        .I1(\Dout_reg[2]_i_2_0 [23]),
        .O(\Dout_reg[23]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__4_i_2__0
       (.I0(\Dout_reg[31]_2 [22]),
        .I1(\Dout_reg[2]_i_2_0 [22]),
        .O(\Dout_reg[23]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__4_i_3__0
       (.I0(\Dout_reg[31]_2 [21]),
        .I1(\Dout_reg[2]_i_2_0 [21]),
        .O(\Dout_reg[23]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__4_i_4__0
       (.I0(\Dout_reg[31]_2 [20]),
        .I1(\Dout_reg[2]_i_2_0 [20]),
        .O(\Dout_reg[23]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__5_i_1__0
       (.I0(\Dout_reg[31]_2[27]_repN_1 ),
        .I1(\Dout_reg[2]_i_2_0 [27]),
        .O(\Dout_reg[27]_1 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__5_i_2__0
       (.I0(\Dout_reg[31]_2[26]_repN ),
        .I1(\Dout_reg[2]_i_2_0 [26]),
        .O(\Dout_reg[27]_1 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__5_i_3__0
       (.I0(\Dout_reg[31]_2[25]_repN ),
        .I1(\Dout_reg[2]_i_2_0 [25]),
        .O(\Dout_reg[27]_1 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__5_i_4__0
       (.I0(\Dout_reg[31]_2 [24]),
        .I1(\Dout_reg[2]_i_2_0 [24]),
        .O(\Dout_reg[27]_1 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__6_i_1__0
       (.I0(\Dout_reg[31]_2 [31]),
        .I1(\Dout_reg[2]_i_2_0 [31]),
        .O(\Dout_reg[31]_5 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__6_i_2
       (.I0(\Dout_reg[31]_2[30]_repN_4 ),
        .I1(\Dout_reg[2]_i_2_0 [30]),
        .O(\Dout_reg[31]_5 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__6_i_3__0
       (.I0(\Dout_reg[31]_2[29]_repN ),
        .I1(\Dout_reg[2]_i_2_0 [29]),
        .O(\Dout_reg[31]_5 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry__6_i_4__0
       (.I0(\Dout_reg[31]_2[28]_repN_3 ),
        .I1(\Dout_reg[2]_i_2_0 [28]),
        .O(\Dout_reg[31]_5 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry_i_1__0
       (.I0(\Dout_reg[31]_2 [3]),
        .I1(\Dout_reg[2]_i_2_0 [3]),
        .O(S[3]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry_i_2__0
       (.I0(\Dout_reg[31]_2[2]_repN ),
        .I1(\Dout_reg[2]_i_2_0 [2]),
        .O(S[2]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry_i_3__0
       (.I0(\Dout_reg[31]_2 [1]),
        .I1(\Dout_reg[2]_i_2_0 [1]),
        .O(S[1]));
  LUT2 #(
    .INIT(4'h9)) 
    S0_carry_i_4__0
       (.I0(\Dout_reg[31]_2 [0]),
        .I1(\Dout_reg[2]_i_2_0 [0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n__parameterized1_2
   (Q,
    SR,
    D,
    CLK_IBUF_BUFG);
  output [30:0]Q;
  input [0:0]SR;
  input [30:0]D;
  input CLK_IBUF_BUFG;

  wire CLK_IBUF_BUFG;
  wire [30:0]D;
  wire [30:0]Q;
  wire [0:0]SR;

  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[10]),
        .Q(Q[10]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[11]),
        .Q(Q[11]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[12]),
        .Q(Q[12]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[13]),
        .Q(Q[13]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[14]),
        .Q(Q[14]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[15]),
        .Q(Q[15]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[16]),
        .Q(Q[16]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[17]),
        .Q(Q[17]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[18]),
        .Q(Q[18]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[19]),
        .Q(Q[19]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[20]),
        .Q(Q[20]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[21]),
        .Q(Q[21]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[22]),
        .Q(Q[22]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[23]),
        .Q(Q[23]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[24]),
        .Q(Q[24]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[25]),
        .Q(Q[25]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[26]),
        .Q(Q[26]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[27]),
        .Q(Q[27]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[28]),
        .Q(Q[28]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[29]),
        .Q(Q[29]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[30]),
        .Q(Q[30]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[3]),
        .Q(Q[3]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[4]),
        .Q(Q[4]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[5]),
        .Q(Q[5]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[6]),
        .Q(Q[6]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[7]),
        .Q(Q[7]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[8]),
        .Q(Q[8]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[9]),
        .Q(Q[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n__parameterized3
   (\Dout_reg[0]_0 ,
    Q,
    \Dout_reg[0]_1 ,
    \Dout_reg[1]_0 ,
    \Dout_reg[4]_0 ,
    \Dout_reg[0]_2 ,
    \Dout_reg[1]_1 ,
    \Dout_reg[0]_3 ,
    ROTATE_RIGHT1,
    \Dout_reg[0]_4 ,
    \Dout_reg[0]_5 ,
    \Dout_reg[0]_6 ,
    \Dout_reg[0]_7 ,
    \Dout_reg[0]_8 ,
    \Dout_reg[0]_9 ,
    \Dout_reg[0]_10 ,
    \Dout_reg[0]_11 ,
    \Dout_reg[0]_12 ,
    \Dout_reg[1]_2 ,
    \Dout_reg[0]_13 ,
    \Dout_reg[31] ,
    \Dout_reg[0]_14 ,
    \Dout_reg[0]_15 ,
    \Dout_reg[0]_16 ,
    \Dout_reg[4]_1 ,
    \Dout_reg[0]_17 ,
    \Dout_reg[0]_18 ,
    \Dout_reg[0]_19 ,
    \Dout_reg[1]_3 ,
    \Dout_reg[0]_20 ,
    \Dout_reg[4]_2 ,
    \Dout_reg[0]_21 ,
    \Dout_reg[0]_22 ,
    \Dout_reg[1]_4 ,
    \Dout_reg[0]_23 ,
    \Dout_reg[0]_24 ,
    \Dout_reg[0]_25 ,
    \Dout_reg[31]_0 ,
    \Dout_reg[1]_5 ,
    \Dout_reg[2]_0 ,
    \Dout_reg[2]_1 ,
    \Dout_reg[0]_26 ,
    \Dout_reg[3]_0 ,
    \Dout[28]_i_8_0 ,
    \Dout_reg[7] ,
    \Dout_reg[7]_0 ,
    \Dout_reg[24] ,
    \Dout_reg[24]_0 ,
    \Dout_reg[25] ,
    \Dout_reg[27] ,
    \Dout_reg[27]_0 ,
    \Dout_reg[28] ,
    \Dout_reg[29] ,
    \Dout_reg[30] ,
    \Dout[0]_i_5_0 ,
    \Dout[0]_i_5_1 ,
    \Dout_reg[1]_6 ,
    \Dout[0]_i_8_0 ,
    \Dout[0]_i_8_1 ,
    \Dout_reg[7]_1 ,
    \Dout_reg[0]_27 ,
    \Dout_reg[0]_28 ,
    \Dout[23]_i_2 ,
    \Dout[0]_i_4__0 ,
    \Dout[25]_i_2 ,
    \Dout[27]_i_2 ,
    \Dout[25]_i_8_0 ,
    \Dout[24]_i_5_0 ,
    \Dout[24]_i_5_1 ,
    SR,
    E,
    D,
    CLK_IBUF_BUFG,
    \Dout_reg[31]_2[31]_repN_alias ,
    \Dout_reg[31]_2[31]_repN_1_alias ,
    \Q[3]_repN_alias ,
    \Q[3]_repN_1_alias ,
    \Q[3]_repN_2_alias ,
    \Q[3]_repN_3_alias ,
    \Q[3]_repN_6_alias ,
    \Dout_reg[31]_2[25]_repN_3_alias ,
    \Q[3]_repN_7_alias ,
    \Q[3]_repN_8_alias ,
    \Q[4]_repN_alias ,
    \Q[4]_repN_1_alias ,
    \Q[4]_repN_2_alias ,
    \Q[4]_repN_3_alias ,
    \Q[4]_repN_5_alias ,
    \Dout_reg[31]_2[28]_repN_alias ,
    \Q[2]_repN_alias ,
    \Q[2]_repN_1_alias ,
    \Q[2]_repN_2_alias ,
    \Q[2]_repN_3_alias ,
    \Q[2]_repN_4_alias ,
    \Q[2]_repN_5_alias ,
    \Q[2]_repN_6_alias ,
    \Q[2]_repN_7_alias ,
    \Dout_reg[31]_2[30]_repN_alias ,
    \Dout_reg[31]_2[30]_repN_1_alias ,
    \Dout_reg[31]_2[30]_repN_2_alias ,
    \Q[1]_repN_alias ,
    \Q[1]_repN_1_alias ,
    \Q[1]_repN_2_alias ,
    \Q[1]_repN_3_alias ,
    \Q[1]_repN_5_alias ,
    \Q[1]_repN_6_alias ,
    \Q[1]_repN_7_alias ,
    \Dout_reg[31]_2[28]_repN_3_alias ,
    \Dout_reg[31]_2[29]_repN_1_alias ,
    \Dout_reg[31]_2[30]_repN_3_alias ,
    \Dout_reg[3]_0_alias ,
    \Dout[7]_i_3_n_0_alias ,
    \Dout_reg[0]_2_repN_alias ,
    \Dout_reg[0]_2_repN_1_alias ,
    \Dout_reg[31]_2[2]_repN_alias ,
    \Dout_reg[31]_2[27]_repN_alias ,
    \Dout_reg[31]_2[26]_repN_alias );
  output \Dout_reg[0]_0 ;
  output [4:0]Q;
  output \Dout_reg[0]_1 ;
  output \Dout_reg[1]_0 ;
  output \Dout_reg[4]_0 ;
  output \Dout_reg[0]_2 ;
  output \Dout_reg[1]_1 ;
  output \Dout_reg[0]_3 ;
  output [3:0]ROTATE_RIGHT1;
  output \Dout_reg[0]_4 ;
  output \Dout_reg[0]_5 ;
  output \Dout_reg[0]_6 ;
  output \Dout_reg[0]_7 ;
  output \Dout_reg[0]_8 ;
  output \Dout_reg[0]_9 ;
  output \Dout_reg[0]_10 ;
  output \Dout_reg[0]_11 ;
  output \Dout_reg[0]_12 ;
  output \Dout_reg[1]_2 ;
  output \Dout_reg[0]_13 ;
  output \Dout_reg[31] ;
  output \Dout_reg[0]_14 ;
  output \Dout_reg[0]_15 ;
  output \Dout_reg[0]_16 ;
  output \Dout_reg[4]_1 ;
  output \Dout_reg[0]_17 ;
  output \Dout_reg[0]_18 ;
  output \Dout_reg[0]_19 ;
  output \Dout_reg[1]_3 ;
  output \Dout_reg[0]_20 ;
  output \Dout_reg[4]_2 ;
  output \Dout_reg[0]_21 ;
  output \Dout_reg[0]_22 ;
  output \Dout_reg[1]_4 ;
  output \Dout_reg[0]_23 ;
  output \Dout_reg[0]_24 ;
  output \Dout_reg[0]_25 ;
  output \Dout_reg[31]_0 ;
  output \Dout_reg[1]_5 ;
  input \Dout_reg[2]_0 ;
  input \Dout_reg[2]_1 ;
  input [1:0]\Dout_reg[0]_26 ;
  input \Dout_reg[3]_0 ;
  input [15:0]\Dout[28]_i_8_0 ;
  input \Dout_reg[7] ;
  input \Dout_reg[7]_0 ;
  input \Dout_reg[24] ;
  input \Dout_reg[24]_0 ;
  input \Dout_reg[25] ;
  input \Dout_reg[27] ;
  input \Dout_reg[27]_0 ;
  input \Dout_reg[28] ;
  input \Dout_reg[29] ;
  input \Dout_reg[30] ;
  input \Dout[0]_i_5_0 ;
  input \Dout[0]_i_5_1 ;
  input \Dout_reg[1]_6 ;
  input \Dout[0]_i_8_0 ;
  input \Dout[0]_i_8_1 ;
  input \Dout_reg[7]_1 ;
  input \Dout_reg[0]_27 ;
  input \Dout_reg[0]_28 ;
  input \Dout[23]_i_2 ;
  input \Dout[0]_i_4__0 ;
  input \Dout[25]_i_2 ;
  input \Dout[27]_i_2 ;
  input \Dout[25]_i_8_0 ;
  input \Dout[24]_i_5_0 ;
  input \Dout[24]_i_5_1 ;
  input [0:0]SR;
  input [0:0]E;
  input [4:0]D;
  input CLK_IBUF_BUFG;
  input \Dout_reg[31]_2[31]_repN_alias ;
  input \Dout_reg[31]_2[31]_repN_1_alias ;
  output \Q[3]_repN_alias ;
  output \Q[3]_repN_1_alias ;
  output \Q[3]_repN_2_alias ;
  output \Q[3]_repN_3_alias ;
  output \Q[3]_repN_6_alias ;
  input \Dout_reg[31]_2[25]_repN_3_alias ;
  output \Q[3]_repN_7_alias ;
  output \Q[3]_repN_8_alias ;
  output \Q[4]_repN_alias ;
  output \Q[4]_repN_1_alias ;
  output \Q[4]_repN_2_alias ;
  output \Q[4]_repN_3_alias ;
  output \Q[4]_repN_5_alias ;
  input \Dout_reg[31]_2[28]_repN_alias ;
  output \Q[2]_repN_alias ;
  output \Q[2]_repN_1_alias ;
  output \Q[2]_repN_2_alias ;
  output \Q[2]_repN_3_alias ;
  output \Q[2]_repN_4_alias ;
  output \Q[2]_repN_5_alias ;
  output \Q[2]_repN_6_alias ;
  output \Q[2]_repN_7_alias ;
  input \Dout_reg[31]_2[30]_repN_alias ;
  input \Dout_reg[31]_2[30]_repN_1_alias ;
  input \Dout_reg[31]_2[30]_repN_2_alias ;
  output \Q[1]_repN_alias ;
  output \Q[1]_repN_1_alias ;
  output \Q[1]_repN_2_alias ;
  output \Q[1]_repN_3_alias ;
  output \Q[1]_repN_5_alias ;
  output \Q[1]_repN_6_alias ;
  output \Q[1]_repN_7_alias ;
  input \Dout_reg[31]_2[28]_repN_3_alias ;
  input \Dout_reg[31]_2[29]_repN_1_alias ;
  input \Dout_reg[31]_2[30]_repN_3_alias ;
  input \Dout_reg[3]_0_alias ;
  input \Dout[7]_i_3_n_0_alias ;
  output \Dout_reg[0]_2_repN_alias ;
  output \Dout_reg[0]_2_repN_1_alias ;
  input \Dout_reg[31]_2[2]_repN_alias ;
  input \Dout_reg[31]_2[27]_repN_alias ;
  input \Dout_reg[31]_2[26]_repN_alias ;

  wire CLK_IBUF_BUFG;
  wire [4:0]D;
  wire \Dout[0]_i_10_n_0 ;
  wire \Dout[0]_i_11_n_0 ;
  wire \Dout[0]_i_4__0 ;
  wire \Dout[0]_i_5_0 ;
  wire \Dout[0]_i_5_1 ;
  wire \Dout[0]_i_7__0_n_0 ;
  wire \Dout[0]_i_7_n_0 ;
  wire \Dout[0]_i_8_0 ;
  wire \Dout[0]_i_8_1 ;
  wire \Dout[0]_i_8__0_n_0 ;
  wire \Dout[0]_i_8_n_0 ;
  wire \Dout[1]_i_10__0_n_0 ;
  wire \Dout[23]_i_2 ;
  wire \Dout[24]_i_10_n_0 ;
  wire \Dout[24]_i_14_n_0 ;
  wire \Dout[24]_i_5_0 ;
  wire \Dout[24]_i_5_1 ;
  wire \Dout[24]_i_8_n_0 ;
  wire \Dout[25]_i_10_n_0 ;
  wire \Dout[25]_i_14_n_0 ;
  wire \Dout[25]_i_2 ;
  wire \Dout[25]_i_8_0 ;
  wire \Dout[25]_i_8_n_0 ;
  wire \Dout[26]_i_12_n_0 ;
  wire \Dout[27]_i_12_n_0 ;
  wire \Dout[27]_i_13_n_0 ;
  wire \Dout[27]_i_14_n_0 ;
  wire \Dout[27]_i_2 ;
  wire \Dout[27]_i_8_n_0 ;
  wire \Dout[28]_i_10_n_0 ;
  wire \Dout[28]_i_12_n_0 ;
  wire \Dout[28]_i_13_n_0 ;
  wire [15:0]\Dout[28]_i_8_0 ;
  wire \Dout[28]_i_8_n_0 ;
  wire \Dout[29]_i_10_n_0 ;
  wire \Dout[29]_i_12_n_0 ;
  wire \Dout[29]_i_8_n_0 ;
  wire \Dout[2]_i_11_n_0 ;
  wire \Dout[30]_i_12_n_0 ;
  wire \Dout[30]_i_24_n_0 ;
  wire \Dout[3]_i_11_n_0 ;
  wire \Dout[3]_i_13_n_0 ;
  wire \Dout[4]_i_13_n_0 ;
  wire \Dout[4]_i_14_n_0 ;
  wire \Dout[4]_i_15_n_0 ;
  wire \Dout[5]_i_22_n_0 ;
  wire \Dout[5]_i_23_n_0 ;
  wire \Dout[5]_i_24_n_0 ;
  wire \Dout[5]_i_25_n_0 ;
  wire \Dout[7]_i_11_n_0 ;
  wire \Dout[7]_i_13_n_0 ;
  wire \Dout[7]_i_3_n_0_alias ;
  wire \Dout[8]_i_11_n_0 ;
  wire \Dout[8]_i_13_n_0 ;
  wire \Dout_reg[0]_0 ;
  wire \Dout_reg[0]_1 ;
  wire \Dout_reg[0]_10 ;
  wire \Dout_reg[0]_11 ;
  wire \Dout_reg[0]_12 ;
  wire \Dout_reg[0]_13 ;
  wire \Dout_reg[0]_14 ;
  wire \Dout_reg[0]_15 ;
  wire \Dout_reg[0]_16 ;
  wire \Dout_reg[0]_17 ;
  wire \Dout_reg[0]_18 ;
  wire \Dout_reg[0]_19 ;
  wire \Dout_reg[0]_2 ;
  wire \Dout_reg[0]_20 ;
  wire \Dout_reg[0]_21 ;
  wire \Dout_reg[0]_22 ;
  wire \Dout_reg[0]_23 ;
  wire \Dout_reg[0]_24 ;
  wire \Dout_reg[0]_25 ;
  wire [1:0]\Dout_reg[0]_26 ;
  wire \Dout_reg[0]_27 ;
  wire \Dout_reg[0]_28 ;
  wire \Dout_reg[0]_2_repN ;
  wire \Dout_reg[0]_2_repN_1 ;
  wire \Dout_reg[0]_3 ;
  wire \Dout_reg[0]_4 ;
  wire \Dout_reg[0]_5 ;
  wire \Dout_reg[0]_6 ;
  wire \Dout_reg[0]_7 ;
  wire \Dout_reg[0]_8 ;
  wire \Dout_reg[0]_9 ;
  wire \Dout_reg[1]_0 ;
  wire \Dout_reg[1]_1 ;
  wire \Dout_reg[1]_2 ;
  wire \Dout_reg[1]_3 ;
  wire \Dout_reg[1]_4 ;
  wire \Dout_reg[1]_5 ;
  wire \Dout_reg[1]_6 ;
  wire \Dout_reg[24] ;
  wire \Dout_reg[24]_0 ;
  wire \Dout_reg[25] ;
  wire \Dout_reg[27] ;
  wire \Dout_reg[27]_0 ;
  wire \Dout_reg[28] ;
  wire \Dout_reg[29] ;
  wire \Dout_reg[2]_0 ;
  wire \Dout_reg[2]_1 ;
  wire \Dout_reg[30] ;
  wire \Dout_reg[31] ;
  wire \Dout_reg[31]_0 ;
  wire \Dout_reg[31]_2[25]_repN_3_alias ;
  wire \Dout_reg[31]_2[26]_repN_alias ;
  wire \Dout_reg[31]_2[27]_repN_alias ;
  wire \Dout_reg[31]_2[28]_repN_3_alias ;
  wire \Dout_reg[31]_2[28]_repN_alias ;
  wire \Dout_reg[31]_2[29]_repN_1_alias ;
  wire \Dout_reg[31]_2[2]_repN_alias ;
  wire \Dout_reg[31]_2[30]_repN_1_alias ;
  wire \Dout_reg[31]_2[30]_repN_2_alias ;
  wire \Dout_reg[31]_2[30]_repN_3_alias ;
  wire \Dout_reg[31]_2[30]_repN_alias ;
  wire \Dout_reg[31]_2[31]_repN_1_alias ;
  wire \Dout_reg[31]_2[31]_repN_alias ;
  wire \Dout_reg[3]_0 ;
  wire \Dout_reg[3]_0_alias ;
  wire \Dout_reg[4]_0 ;
  wire \Dout_reg[4]_1 ;
  wire \Dout_reg[4]_2 ;
  wire \Dout_reg[7] ;
  wire \Dout_reg[7]_0 ;
  wire \Dout_reg[7]_1 ;
  wire [0:0]E;
  wire [4:0]Q;
  wire \Q[1]_repN ;
  wire \Q[1]_repN_1 ;
  wire \Q[1]_repN_2 ;
  wire \Q[1]_repN_3 ;
  wire \Q[1]_repN_4 ;
  wire \Q[1]_repN_5 ;
  wire \Q[1]_repN_6 ;
  wire \Q[1]_repN_7 ;
  wire \Q[2]_repN ;
  wire \Q[2]_repN_1 ;
  wire \Q[2]_repN_2 ;
  wire \Q[2]_repN_3 ;
  wire \Q[2]_repN_4 ;
  wire \Q[2]_repN_5 ;
  wire \Q[2]_repN_6 ;
  wire \Q[2]_repN_7 ;
  wire \Q[2]_repN_8 ;
  wire \Q[2]_repN_9 ;
  wire \Q[3]_repN ;
  wire \Q[3]_repN_1 ;
  wire \Q[3]_repN_2 ;
  wire \Q[3]_repN_3 ;
  wire \Q[3]_repN_4 ;
  wire \Q[3]_repN_5 ;
  wire \Q[3]_repN_6 ;
  wire \Q[3]_repN_7 ;
  wire \Q[3]_repN_8 ;
  wire \Q[4]_repN ;
  wire \Q[4]_repN_1 ;
  wire \Q[4]_repN_2 ;
  wire \Q[4]_repN_3 ;
  wire \Q[4]_repN_4 ;
  wire \Q[4]_repN_5 ;
  wire \Q[4]_repN_6 ;
  wire \Q[4]_repN_7 ;
  wire [3:0]ROTATE_RIGHT1;
  wire [0:0]SR;

  assign \Dout_reg[0]_2_repN_1_alias  = \Dout_reg[0]_2_repN_1 ;
  assign \Dout_reg[0]_2_repN_alias  = \Dout_reg[0]_2_repN ;
  assign \Q[1]_repN_1_alias  = \Q[1]_repN_1 ;
  assign \Q[1]_repN_2_alias  = \Q[1]_repN_2 ;
  assign \Q[1]_repN_3_alias  = \Q[1]_repN_3 ;
  assign \Q[1]_repN_5_alias  = \Q[1]_repN_5 ;
  assign \Q[1]_repN_6_alias  = \Q[1]_repN_6 ;
  assign \Q[1]_repN_7_alias  = \Q[1]_repN_7 ;
  assign \Q[1]_repN_alias  = \Q[1]_repN ;
  assign \Q[2]_repN_1_alias  = \Q[2]_repN_1 ;
  assign \Q[2]_repN_2_alias  = \Q[2]_repN_2 ;
  assign \Q[2]_repN_3_alias  = \Q[2]_repN_3 ;
  assign \Q[2]_repN_4_alias  = \Q[2]_repN_4 ;
  assign \Q[2]_repN_5_alias  = \Q[2]_repN_5 ;
  assign \Q[2]_repN_6_alias  = \Q[2]_repN_6 ;
  assign \Q[2]_repN_7_alias  = \Q[2]_repN_7 ;
  assign \Q[2]_repN_alias  = \Q[2]_repN ;
  assign \Q[3]_repN_1_alias  = \Q[3]_repN_1 ;
  assign \Q[3]_repN_2_alias  = \Q[3]_repN_2 ;
  assign \Q[3]_repN_3_alias  = \Q[3]_repN_3 ;
  assign \Q[3]_repN_6_alias  = \Q[3]_repN_6 ;
  assign \Q[3]_repN_7_alias  = \Q[3]_repN_7 ;
  assign \Q[3]_repN_8_alias  = \Q[3]_repN_8 ;
  assign \Q[3]_repN_alias  = \Q[3]_repN ;
  assign \Q[4]_repN_1_alias  = \Q[4]_repN_1 ;
  assign \Q[4]_repN_2_alias  = \Q[4]_repN_2 ;
  assign \Q[4]_repN_3_alias  = \Q[4]_repN_3 ;
  assign \Q[4]_repN_5_alias  = \Q[4]_repN_5 ;
  assign \Q[4]_repN_alias  = \Q[4]_repN ;
  LUT5 #(
    .INIT(32'hFEAB02A8)) 
    \Dout[0]_i_10 
       (.I0(\Dout[0]_i_8_0 ),
        .I1(\Q[1]_repN_7 ),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(\Dout[0]_i_8_1 ),
        .O(\Dout[0]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hAAA80001)) 
    \Dout[0]_i_10__0 
       (.I0(\Q[4]_repN_1 ),
        .I1(\Q[2]_repN_3 ),
        .I2(Q[0]),
        .I3(\Q[1]_repN_3 ),
        .I4(\Q[3]_repN_7 ),
        .O(\Dout_reg[4]_0 ));
  LUT3 #(
    .INIT(8'h81)) 
    \Dout[0]_i_11 
       (.I0(\Q[2]_repN_9 ),
        .I1(Q[0]),
        .I2(\Q[1]_repN_3 ),
        .O(\Dout[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA0000CCCCF000)) 
    \Dout[0]_i_4 
       (.I0(\Dout_reg[1]_6 ),
        .I1(\Dout_reg[0]_27 ),
        .I2(\Dout[0]_i_7__0_n_0 ),
        .I3(\Dout[0]_i_8__0_n_0 ),
        .I4(\Dout_reg[0]_28 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_21 ));
  LUT6 #(
    .INIT(64'h8888888880888000)) 
    \Dout[0]_i_5 
       (.I0(\Dout_reg[0]_26 [0]),
        .I1(\Dout_reg[0]_26 [1]),
        .I2(\Dout_reg[30] ),
        .I3(Q[0]),
        .I4(\Dout[0]_i_7_n_0 ),
        .I5(\Dout[0]_i_8_n_0 ),
        .O(\Dout_reg[0]_14 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \Dout[0]_i_5__0 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_26 [0]),
        .I2(\Dout_reg[0]_26 [1]),
        .I3(\Dout_reg[4]_0 ),
        .I4(\Dout[28]_i_8_0 [0]),
        .I5(\Dout[0]_i_11_n_0 ),
        .O(\Dout_reg[0]_15 ));
  LUT6 #(
    .INIT(64'h000080800000FF00)) 
    \Dout[0]_i_6__0 
       (.I0(\Dout[0]_i_7__0_n_0 ),
        .I1(\Dout_reg[31]_2[31]_repN_alias ),
        .I2(\Dout_reg[4]_2 ),
        .I3(\Dout[0]_i_4__0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout_reg[31]_0 ));
  LUT5 #(
    .INIT(32'h00000010)) 
    \Dout[0]_i_7 
       (.I0(\Q[1]_repN_5 ),
        .I1(\Q[2]_repN_1 ),
        .I2(\Dout[28]_i_8_0 [15]),
        .I3(Q[4]),
        .I4(Q[3]),
        .O(\Dout[0]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \Dout[0]_i_7__0 
       (.I0(\Q[2]_repN_7 ),
        .I1(\Q[1]_repN ),
        .O(\Dout[0]_i_7__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000B8B8BB88)) 
    \Dout[0]_i_8 
       (.I0(\Dout[0]_i_10_n_0 ),
        .I1(Q[1]),
        .I2(\Dout[0]_i_5_0 ),
        .I3(\Dout[0]_i_5_1 ),
        .I4(Q[2]),
        .I5(Q[0]),
        .O(\Dout[0]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'h10)) 
    \Dout[0]_i_8__0 
       (.I0(\Q[3]_repN_1 ),
        .I1(\Q[4]_repN_7 ),
        .I2(\Dout[28]_i_8_0 [0]),
        .O(\Dout[0]_i_8__0_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000002)) 
    \Dout[1]_i_10__0 
       (.I0(\Dout[28]_i_8_0 [0]),
        .I1(\Q[4]_repN_1 ),
        .I2(\Q[2]_repN_4 ),
        .I3(Q[0]),
        .I4(\Q[1]_repN_3 ),
        .I5(\Q[3]_repN_1 ),
        .O(\Dout[1]_i_10__0_n_0 ));
  LUT5 #(
    .INIT(32'hFFE400E4)) 
    \Dout[1]_i_12 
       (.I0(Q[0]),
        .I1(\Dout_reg[1]_3 ),
        .I2(\Dout_reg[0]_19 ),
        .I3(\Dout_reg[0]_26 [0]),
        .I4(\Dout_reg[7]_0 ),
        .O(\Dout_reg[0]_20 ));
  LUT6 #(
    .INIT(64'hFFFCBBB800000000)) 
    \Dout[1]_i_6__0 
       (.I0(\Dout[1]_i_10__0_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[2]_i_11_n_0 ),
        .I3(\Dout_reg[1]_6 ),
        .I4(\Dout_reg[2]_0 ),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout_reg[0]_16 ));
  LUT6 #(
    .INIT(64'h000000008800C000)) 
    \Dout[1]_i_8__0 
       (.I0(\Dout[28]_i_8_0 [0]),
        .I1(\Dout[0]_i_7__0_n_0 ),
        .I2(\Dout[28]_i_8_0 [1]),
        .I3(\Dout_reg[4]_2 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout_reg[0]_22 ));
  LUT5 #(
    .INIT(32'h0002AAA8)) 
    \Dout[22]_i_16 
       (.I0(\Q[4]_repN_1 ),
        .I1(\Q[2]_repN ),
        .I2(Q[0]),
        .I3(\Q[1]_repN ),
        .I4(\Q[3]_repN_8 ),
        .O(\Dout_reg[4]_1 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[23]_i_6 
       (.I0(\Dout[24]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[23]_i_2 ),
        .O(\Dout_reg[0]_25 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[24]_i_10 
       (.I0(\Dout_reg[31]_2[30]_repN_3_alias ),
        .I1(\Q[2]_repN ),
        .I2(\Dout_reg[4]_2 ),
        .I3(\Dout_reg[31]_2[26]_repN_alias ),
        .I4(\Q[1]_repN ),
        .I5(\Dout[24]_i_14_n_0 ),
        .O(\Dout[24]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'h000B0008)) 
    \Dout[24]_i_14 
       (.I0(\Dout_reg[31]_2[28]_repN_3_alias ),
        .I1(\Q[2]_repN ),
        .I2(\Q[3]_repN_8 ),
        .I3(\Q[4]_repN_2 ),
        .I4(\Dout[28]_i_8_0 [8]),
        .O(\Dout[24]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[24]_i_5 
       (.I0(\Dout[24]_i_8_n_0 ),
        .I1(\Dout_reg[0]_26 [0]),
        .I2(\Dout_reg[0]_5 ),
        .I3(\Dout_reg[24] ),
        .I4(Q[0]),
        .I5(\Dout_reg[24]_0 ),
        .O(\Dout_reg[0]_4 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[24]_i_6 
       (.I0(\Dout[25]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[24]_i_10_n_0 ),
        .O(\Dout_reg[0]_5 ));
  LUT6 #(
    .INIT(64'h00000000B8B8FF00)) 
    \Dout[24]_i_8 
       (.I0(\Dout[27]_i_13_n_0 ),
        .I1(\Q[1]_repN_7 ),
        .I2(\Dout[24]_i_5_0 ),
        .I3(\Dout[24]_i_5_1 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout[24]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[25]_i_10 
       (.I0(\Dout_reg[31]_2[31]_repN_alias ),
        .I1(\Q[2]_repN_2 ),
        .I2(\Dout_reg[4]_2 ),
        .I3(\Dout_reg[31]_2[27]_repN_alias ),
        .I4(\Q[1]_repN_7 ),
        .I5(\Dout[25]_i_14_n_0 ),
        .O(\Dout[25]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'h000B0008)) 
    \Dout[25]_i_14 
       (.I0(\Dout_reg[31]_2[29]_repN_1_alias ),
        .I1(\Q[2]_repN_2 ),
        .I2(\Q[3]_repN_2 ),
        .I3(\Q[4]_repN ),
        .I4(\Dout_reg[31]_2[25]_repN_3_alias ),
        .O(\Dout[25]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[25]_i_5 
       (.I0(\Dout[25]_i_8_n_0 ),
        .I1(\Dout_reg[0]_26 [0]),
        .I2(\Dout_reg[0]_7 ),
        .I3(\Dout_reg[25] ),
        .I4(Q[0]),
        .I5(\Dout_reg[24] ),
        .O(\Dout_reg[0]_6 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[25]_i_6 
       (.I0(\Dout[25]_i_2 ),
        .I1(Q[0]),
        .I2(\Dout[25]_i_10_n_0 ),
        .O(\Dout_reg[0]_7 ));
  LUT6 #(
    .INIT(64'h00000000FF00B8B8)) 
    \Dout[25]_i_8 
       (.I0(\Dout[27]_i_13_n_0 ),
        .I1(\Q[1]_repN_7 ),
        .I2(\Dout[24]_i_5_0 ),
        .I3(\Dout[26]_i_12_n_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout[25]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[26]_i_12 
       (.I0(\Dout[28]_i_13_n_0 ),
        .I1(\Q[1]_repN_7 ),
        .I2(\Dout[25]_i_8_0 ),
        .O(\Dout[26]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h00000000B8B8FF00)) 
    \Dout[26]_i_8 
       (.I0(\Dout[27]_i_12_n_0 ),
        .I1(\Q[1]_repN_4 ),
        .I2(\Dout[27]_i_13_n_0 ),
        .I3(\Dout[26]_i_12_n_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout_reg[1]_5 ));
  LUT5 #(
    .INIT(32'hFF00FE02)) 
    \Dout[27]_i_12 
       (.I0(\Dout_reg[31]_2[29]_repN_1_alias ),
        .I1(\Q[4]_repN_4 ),
        .I2(\Q[3]_repN_5 ),
        .I3(\Dout_reg[31]_2[31]_repN_1_alias ),
        .I4(\Q[2]_repN_8 ),
        .O(\Dout[27]_i_12_n_0 ));
  LUT5 #(
    .INIT(32'hFF00FE02)) 
    \Dout[27]_i_13 
       (.I0(\Dout_reg[31]_2[27]_repN_alias ),
        .I1(\Q[4]_repN ),
        .I2(\Q[3]_repN_4 ),
        .I3(\Dout_reg[31]_2[31]_repN_alias ),
        .I4(\Q[2]_repN_2 ),
        .O(\Dout[27]_i_13_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[27]_i_14 
       (.I0(\Dout[28]_i_12_n_0 ),
        .I1(\Q[1]_repN_4 ),
        .I2(\Dout[28]_i_13_n_0 ),
        .O(\Dout[27]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[27]_i_5 
       (.I0(\Dout[27]_i_8_n_0 ),
        .I1(\Dout_reg[0]_26 [0]),
        .I2(\Dout_reg[0]_9 ),
        .I3(\Dout_reg[27] ),
        .I4(Q[0]),
        .I5(\Dout_reg[27]_0 ),
        .O(\Dout_reg[0]_8 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[27]_i_6 
       (.I0(\Dout[28]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[27]_i_2 ),
        .O(\Dout_reg[0]_9 ));
  LUT6 #(
    .INIT(64'h00000000FF00B8B8)) 
    \Dout[27]_i_8 
       (.I0(\Dout[27]_i_12_n_0 ),
        .I1(\Q[1]_repN_4 ),
        .I2(\Dout[27]_i_13_n_0 ),
        .I3(\Dout[27]_i_14_n_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout[27]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000020200000300)) 
    \Dout[28]_i_10 
       (.I0(\Dout_reg[31]_2[30]_repN_1_alias ),
        .I1(\Q[3]_repN_4 ),
        .I2(\Q[4]_repN ),
        .I3(\Dout_reg[31]_2[28]_repN_alias ),
        .I4(\Q[2]_repN_2 ),
        .I5(\Q[1]_repN_7 ),
        .O(\Dout[28]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hFF00FE02)) 
    \Dout[28]_i_12 
       (.I0(\Dout_reg[31]_2[30]_repN_2_alias ),
        .I1(\Q[4]_repN_4 ),
        .I2(\Q[3]_repN_5 ),
        .I3(\Dout_reg[31]_2[31]_repN_1_alias ),
        .I4(\Q[2]_repN_8 ),
        .O(\Dout[28]_i_12_n_0 ));
  LUT5 #(
    .INIT(32'hFF00FE02)) 
    \Dout[28]_i_13 
       (.I0(\Dout_reg[31]_2[28]_repN_alias ),
        .I1(\Q[4]_repN ),
        .I2(\Q[3]_repN_2 ),
        .I3(\Dout_reg[31]_2[31]_repN_alias ),
        .I4(\Q[2]_repN_2 ),
        .O(\Dout[28]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[28]_i_5 
       (.I0(\Dout[28]_i_8_n_0 ),
        .I1(\Dout_reg[0]_26 [0]),
        .I2(\Dout_reg[0]_11 ),
        .I3(\Dout_reg[28] ),
        .I4(Q[0]),
        .I5(\Dout_reg[27] ),
        .O(\Dout_reg[0]_10 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[28]_i_6 
       (.I0(\Dout[29]_i_10_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[28]_i_10_n_0 ),
        .O(\Dout_reg[0]_11 ));
  LUT6 #(
    .INIT(64'h00000000AAAACFC0)) 
    \Dout[28]_i_8 
       (.I0(\Dout[29]_i_12_n_0 ),
        .I1(\Dout[28]_i_12_n_0 ),
        .I2(\Q[1]_repN_4 ),
        .I3(\Dout[28]_i_13_n_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout[28]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000020200000300)) 
    \Dout[29]_i_10 
       (.I0(\Dout_reg[31]_2[31]_repN_alias ),
        .I1(\Q[3]_repN_4 ),
        .I2(\Q[4]_repN ),
        .I3(\Dout_reg[31]_2[29]_repN_1_alias ),
        .I4(\Q[2]_repN_2 ),
        .I5(\Q[1]_repN_7 ),
        .O(\Dout[29]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0004)) 
    \Dout[29]_i_12 
       (.I0(\Q[1]_repN_4 ),
        .I1(\Dout_reg[31]_2[29]_repN_1_alias ),
        .I2(\Q[4]_repN_4 ),
        .I3(\Q[3]_repN_5 ),
        .I4(\Dout_reg[31]_2[31]_repN_1_alias ),
        .I5(\Q[2]_repN_8 ),
        .O(\Dout[29]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[29]_i_5 
       (.I0(\Dout[29]_i_8_n_0 ),
        .I1(\Dout_reg[0]_26 [0]),
        .I2(\Dout_reg[1]_2 ),
        .I3(\Dout_reg[29] ),
        .I4(Q[0]),
        .I5(\Dout_reg[28] ),
        .O(\Dout_reg[0]_12 ));
  LUT6 #(
    .INIT(64'h1000FFFF10000000)) 
    \Dout[29]_i_6 
       (.I0(\Q[1]_repN_7 ),
        .I1(\Q[2]_repN_2 ),
        .I2(\Dout_reg[31]_2[30]_repN_1_alias ),
        .I3(\Dout_reg[4]_2 ),
        .I4(Q[0]),
        .I5(\Dout[29]_i_10_n_0 ),
        .O(\Dout_reg[1]_2 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[29]_i_8 
       (.I0(\Dout[30]_i_24_n_0 ),
        .I1(\Dout[29]_i_12_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_26 [0]),
        .O(\Dout[29]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000002)) 
    \Dout[2]_i_11 
       (.I0(\Dout[28]_i_8_0 [1]),
        .I1(\Q[4]_repN_2 ),
        .I2(\Q[2]_repN_7 ),
        .I3(Q[0]),
        .I4(\Q[1]_repN ),
        .I5(\Q[3]_repN_7 ),
        .O(\Dout[2]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFEEF3E200000000)) 
    \Dout[2]_i_6 
       (.I0(\Dout[3]_i_11_n_0 ),
        .I1(Q[0]),
        .I2(\Dout[2]_i_11_n_0 ),
        .I3(\Dout_reg[2]_0 ),
        .I4(\Dout_reg[2]_1 ),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h000000008080FF00)) 
    \Dout[2]_i_8 
       (.I0(\Dout[0]_i_7__0_n_0 ),
        .I1(\Dout[28]_i_8_0 [1]),
        .I2(\Dout_reg[4]_2 ),
        .I3(\Dout[3]_i_13_n_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout_reg[1]_4 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[30]_i_12 
       (.I0(\Dout_reg[31]_2[31]_repN_alias ),
        .I1(\Dout[30]_i_24_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_26 [0]),
        .O(\Dout[30]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \Dout[30]_i_15 
       (.I0(\Q[4]_repN_2 ),
        .I1(\Q[3]_repN_7 ),
        .O(\Dout_reg[4]_2 ));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0004)) 
    \Dout[30]_i_24 
       (.I0(\Q[1]_repN_7 ),
        .I1(\Dout_reg[31]_2[30]_repN_1_alias ),
        .I2(\Q[4]_repN ),
        .I3(\Q[3]_repN_4 ),
        .I4(\Dout_reg[31]_2[31]_repN_alias ),
        .I5(\Q[2]_repN_2 ),
        .O(\Dout[30]_i_24_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Dout[30]_i_27 
       (.I0(\Q[1]_repN ),
        .I1(Q[0]),
        .O(ROTATE_RIGHT1[0]));
  LUT3 #(
    .INIT(8'h1E)) 
    \Dout[30]_i_29 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\Q[2]_repN_3 ),
        .O(ROTATE_RIGHT1[1]));
  LUT4 #(
    .INIT(16'h01FE)) 
    \Dout[30]_i_35 
       (.I0(\Q[2]_repN_2 ),
        .I1(Q[0]),
        .I2(\Q[1]_repN_2 ),
        .I3(\Q[3]_repN_3 ),
        .O(ROTATE_RIGHT1[2]));
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    \Dout[30]_i_36 
       (.I0(\Q[3]_repN_3 ),
        .I1(\Q[1]_repN_2 ),
        .I2(Q[0]),
        .I3(\Q[2]_repN_2 ),
        .I4(\Q[4]_repN_5 ),
        .O(ROTATE_RIGHT1[3]));
  LUT6 #(
    .INIT(64'hEEEAEEEEEEEAEAEA)) 
    \Dout[30]_i_6 
       (.I0(\Dout[30]_i_12_n_0 ),
        .I1(\Dout_reg[0]_26 [0]),
        .I2(\Dout_reg[31] ),
        .I3(\Dout_reg[29] ),
        .I4(Q[0]),
        .I5(\Dout_reg[30] ),
        .O(\Dout_reg[0]_13 ));
  LUT6 #(
    .INIT(64'h000B000800000000)) 
    \Dout[30]_i_9 
       (.I0(\Dout_reg[31]_2[31]_repN_alias ),
        .I1(Q[0]),
        .I2(\Q[1]_repN_7 ),
        .I3(\Q[2]_repN_2 ),
        .I4(\Dout_reg[31]_2[30]_repN_1_alias ),
        .I5(\Dout_reg[4]_2 ),
        .O(\Dout_reg[31] ));
  LUT6 #(
    .INIT(64'hC0008800880000C0)) 
    \Dout[3]_i_11 
       (.I0(\Dout[28]_i_8_0 [0]),
        .I1(\Dout_reg[4]_0 ),
        .I2(\Dout_reg[31]_2[2]_repN_alias ),
        .I3(\Q[2]_repN_3 ),
        .I4(Q[0]),
        .I5(\Q[1]_repN ),
        .O(\Dout[3]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0000020200000300)) 
    \Dout[3]_i_13 
       (.I0(\Dout[28]_i_8_0 [0]),
        .I1(\Q[3]_repN ),
        .I2(\Q[4]_repN_2 ),
        .I3(\Dout_reg[31]_2[2]_repN_alias ),
        .I4(\Q[2]_repN_6 ),
        .I5(\Q[1]_repN_6 ),
        .O(\Dout[3]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hFFFCBBB800000000)) 
    \Dout[3]_i_6 
       (.I0(\Dout[3]_i_11_n_0 ),
        .I1(Q[0]),
        .I2(\Dout_reg[1]_0 ),
        .I3(\Dout_reg[2]_1 ),
        .I4(\Dout_reg[3]_0 ),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout_reg[0]_1 ));
  LUT4 #(
    .INIT(16'h00AC)) 
    \Dout[3]_i_8 
       (.I0(\Dout[3]_i_13_n_0 ),
        .I1(\Dout[4]_i_13_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[0]_26 [0]),
        .O(\Dout_reg[0]_23 ));
  LUT6 #(
    .INIT(64'hC0008800880000C0)) 
    \Dout[4]_i_11 
       (.I0(\Dout[28]_i_8_0 [1]),
        .I1(\Dout_reg[4]_0 ),
        .I2(\Dout[28]_i_8_0 [3]),
        .I3(\Q[2]_repN_6 ),
        .I4(Q[0]),
        .I5(\Q[1]_repN_6 ),
        .O(\Dout_reg[1]_0 ));
  LUT6 #(
    .INIT(64'h0000020200000300)) 
    \Dout[4]_i_13 
       (.I0(\Dout[28]_i_8_0 [1]),
        .I1(\Q[3]_repN_1 ),
        .I2(\Q[4]_repN_1 ),
        .I3(\Dout[28]_i_8_0 [3]),
        .I4(\Q[2]_repN_3 ),
        .I5(\Q[1]_repN_3 ),
        .O(\Dout[4]_i_13_n_0 ));
  LUT3 #(
    .INIT(8'h10)) 
    \Dout[4]_i_14 
       (.I0(\Q[3]_repN_1 ),
        .I1(\Q[4]_repN_1 ),
        .I2(\Dout_reg[31]_2[2]_repN_alias ),
        .O(\Dout[4]_i_14_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Dout[4]_i_15 
       (.I0(\Q[1]_repN_3 ),
        .I1(\Q[2]_repN_4 ),
        .O(\Dout[4]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AAAAFFC0)) 
    \Dout[4]_i_8 
       (.I0(\Dout[4]_i_13_n_0 ),
        .I1(\Dout[4]_i_14_n_0 ),
        .I2(\Dout[4]_i_15_n_0 ),
        .I3(\Dout[5]_i_23_n_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout_reg[0]_24 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \Dout[5]_i_22 
       (.I0(\Q[2]_repN_4 ),
        .I1(\Q[1]_repN_3 ),
        .I2(\Dout_reg[31]_2[2]_repN_alias ),
        .I3(\Q[4]_repN_1 ),
        .I4(\Q[3]_repN_1 ),
        .O(\Dout[5]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'h0000000003020002)) 
    \Dout[5]_i_23 
       (.I0(\Dout[28]_i_8_0 [4]),
        .I1(\Q[4]_repN_3 ),
        .I2(\Q[3]_repN_7 ),
        .I3(\Q[2]_repN_4 ),
        .I4(\Dout[28]_i_8_0 [0]),
        .I5(\Q[1]_repN_3 ),
        .O(\Dout[5]_i_23_n_0 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \Dout[5]_i_24 
       (.I0(\Q[2]_repN_4 ),
        .I1(\Q[1]_repN_3 ),
        .I2(\Dout[28]_i_8_0 [3]),
        .I3(\Q[4]_repN_1 ),
        .I4(\Q[3]_repN_1 ),
        .O(\Dout[5]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'h0000000003020002)) 
    \Dout[5]_i_25 
       (.I0(\Dout[28]_i_8_0 [5]),
        .I1(\Q[4]_repN_1 ),
        .I2(\Q[3]_repN_1 ),
        .I3(\Q[2]_repN_4 ),
        .I4(\Dout[28]_i_8_0 [1]),
        .I5(\Q[1]_repN_3 ),
        .O(\Dout[5]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEEEFFF0)) 
    \Dout[5]_i_9 
       (.I0(\Dout[5]_i_22_n_0 ),
        .I1(\Dout[5]_i_23_n_0 ),
        .I2(\Dout[5]_i_24_n_0 ),
        .I3(\Dout[5]_i_25_n_0 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_26 [0]),
        .O(\Dout_reg[0]_17 ));
  LUT5 #(
    .INIT(32'h000B0008)) 
    \Dout[7]_i_11 
       (.I0(\Dout_reg[31]_2[2]_repN_alias ),
        .I1(\Q[2]_repN_4 ),
        .I2(\Q[3]_repN_1 ),
        .I3(\Q[4]_repN_6 ),
        .I4(\Dout[28]_i_8_0 [6]),
        .O(\Dout[7]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFEAB000002A80000)) 
    \Dout[7]_i_13 
       (.I0(\Dout_reg[31]_2[2]_repN_alias ),
        .I1(\Q[1]_repN_3 ),
        .I2(Q[0]),
        .I3(\Q[2]_repN_4 ),
        .I4(\Dout_reg[4]_0 ),
        .I5(\Dout[28]_i_8_0 [6]),
        .O(\Dout[7]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h8A808A808A8A8080)) 
    \Dout[7]_i_2 
       (.I0(\Dout_reg[7]_1 ),
        .I1(\Dout_reg[7]_0 ),
        .I2(\Dout_reg[0]_26 [0]),
        .I3(\Dout_reg[0]_19 ),
        .I4(\Dout_reg[1]_3 ),
        .I5(Q[0]),
        .O(\Dout_reg[0]_18 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAEAEAEEEA)) 
    \Dout[7]_i_5 
       (.I0(\Dout_reg[7] ),
        .I1(\Dout_reg[0]_26 [0]),
        .I2(\Dout_reg[7]_0 ),
        .I3(\Dout_reg[1]_1 ),
        .I4(Q[0]),
        .I5(\Dout_reg[0]_3 ),
        .O(\Dout_reg[0]_2 ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \Dout[7]_i_5_comp 
       (.I0(\Dout_reg[3]_0_alias ),
        .I1(\Dout[7]_i_3_n_0_alias ),
        .O(\Dout_reg[0]_2_repN ));
  (* PHYS_OPT_MODIFIED = "RESTRUCT_OPT" *) 
  LUT6 #(
    .INIT(64'hFCFCFCFCFCF4F8F0)) 
    \Dout[7]_i_5_comp_1 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_26 [0]),
        .I2(\Dout[7]_i_3_n_0_alias ),
        .I3(\Dout_reg[0]_3 ),
        .I4(\Dout_reg[1]_1 ),
        .I5(\Dout_reg[7]_0 ),
        .O(\Dout_reg[0]_2_repN_1 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[7]_i_7 
       (.I0(\Dout[28]_i_8_0 [0]),
        .I1(\Q[2]_repN_4 ),
        .I2(\Dout_reg[4]_2 ),
        .I3(\Dout[28]_i_8_0 [4]),
        .I4(\Q[1]_repN_3 ),
        .I5(\Dout[7]_i_11_n_0 ),
        .O(\Dout_reg[0]_19 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[7]_i_9 
       (.I0(\Dout[28]_i_8_0 [0]),
        .I1(ROTATE_RIGHT1[1]),
        .I2(\Dout_reg[4]_0 ),
        .I3(\Dout[28]_i_8_0 [4]),
        .I4(ROTATE_RIGHT1[0]),
        .I5(\Dout[7]_i_13_n_0 ),
        .O(\Dout_reg[0]_3 ));
  LUT5 #(
    .INIT(32'h000B0008)) 
    \Dout[8]_i_11 
       (.I0(\Dout[28]_i_8_0 [3]),
        .I1(\Q[2]_repN_4 ),
        .I2(\Q[3]_repN_1 ),
        .I3(\Q[4]_repN_6 ),
        .I4(\Dout[28]_i_8_0 [7]),
        .O(\Dout[8]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFEAB000002A80000)) 
    \Dout[8]_i_13 
       (.I0(\Dout[28]_i_8_0 [3]),
        .I1(\Q[1]_repN_3 ),
        .I2(Q[0]),
        .I3(\Q[2]_repN_4 ),
        .I4(\Dout_reg[4]_0 ),
        .I5(\Dout[28]_i_8_0 [7]),
        .O(\Dout[8]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[8]_i_7 
       (.I0(\Dout[28]_i_8_0 [1]),
        .I1(\Q[2]_repN_4 ),
        .I2(\Dout_reg[4]_2 ),
        .I3(\Dout[28]_i_8_0 [5]),
        .I4(\Q[1]_repN_3 ),
        .I5(\Dout[8]_i_11_n_0 ),
        .O(\Dout_reg[1]_3 ));
  LUT6 #(
    .INIT(64'hB080FFFFB0800000)) 
    \Dout[8]_i_9 
       (.I0(\Dout[28]_i_8_0 [1]),
        .I1(ROTATE_RIGHT1[1]),
        .I2(\Dout_reg[4]_0 ),
        .I3(\Dout[28]_i_8_0 [5]),
        .I4(ROTATE_RIGHT1[0]),
        .I5(\Dout[8]_i_13_n_0 ),
        .O(\Dout_reg[1]_1 ));
  (* PHYS_OPT_MODIFIED = "PLACEMENT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[1]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(\Q[1]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[1]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(\Q[1]_repN_1 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[1]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1]_replica_2 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(\Q[1]_repN_2 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[1]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1]_replica_3 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(\Q[1]_repN_3 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[1]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1]_replica_4 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(\Q[1]_repN_4 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[1]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1]_replica_5 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(\Q[1]_repN_5 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[1]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1]_replica_6 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(\Q[1]_repN_6 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[1]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1]_replica_7 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[1]),
        .Q(\Q[1]_repN_7 ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT PLACEMENT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Q[2]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Q[2]_repN_1 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica_2 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Q[2]_repN_2 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica_3 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Q[2]_repN_3 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica_4 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Q[2]_repN_4 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica_5 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Q[2]_repN_5 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica_6 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Q[2]_repN_6 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica_7 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Q[2]_repN_7 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica_8 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Q[2]_repN_8 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_replica_9 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[2]),
        .Q(\Q[2]_repN_9 ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT PLACEMENT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(Q[3]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[3]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(\Q[3]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[3]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(\Q[3]_repN_1 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[3]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3]_replica_2 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(\Q[3]_repN_2 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[3]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3]_replica_3 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(\Q[3]_repN_3 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[3]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3]_replica_4 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(\Q[3]_repN_4 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[3]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3]_replica_5 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(\Q[3]_repN_5 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[3]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3]_replica_6 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(\Q[3]_repN_6 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[3]_replica" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3]_replica_7 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(\Q[3]_repN_7 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[3]_replica" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3]_replica_8 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[3]),
        .Q(\Q[3]_repN_8 ),
        .R(SR));
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  (* PHYS_OPT_SKIPPED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(Q[4]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_replica 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Q[4]_repN ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_replica_1 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Q[4]_repN_1 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_replica_2 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Q[4]_repN_2 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_replica_3 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Q[4]_repN_3 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_replica_4 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Q[4]_repN_4 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_replica_5 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Q[4]_repN_5 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_replica_6 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Q[4]_repN_6 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  (* PHYS_OPT_MODIFIED = "FANOUT_OPT" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_replica_7 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(D[4]),
        .Q(\Q[4]_repN_7 ),
        .R(SR));
endmodule

module SUBTRACTOR_n
   (\Dout_reg[31] ,
    S_Sub_in,
    CO,
    Q,
    S,
    \Dout[4]_i_4 ,
    \Dout[8]_i_4 ,
    \Dout[12]_i_4 ,
    \Dout[16]_i_4 ,
    \Dout[20]_i_4 ,
    \Dout[24]_i_4 ,
    \Dout_reg[3] ,
    \Dout_reg[3]_0 );
  output [5:0]\Dout_reg[31] ;
  output [25:0]S_Sub_in;
  output [0:0]CO;
  input [30:0]Q;
  input [3:0]S;
  input [3:0]\Dout[4]_i_4 ;
  input [3:0]\Dout[8]_i_4 ;
  input [3:0]\Dout[12]_i_4 ;
  input [3:0]\Dout[16]_i_4 ;
  input [3:0]\Dout[20]_i_4 ;
  input [3:0]\Dout[24]_i_4 ;
  input [0:0]\Dout_reg[3] ;
  input [3:0]\Dout_reg[3]_0 ;

  wire [0:0]CO;
  wire [3:0]\Dout[12]_i_4 ;
  wire [3:0]\Dout[16]_i_4 ;
  wire [3:0]\Dout[20]_i_4 ;
  wire [3:0]\Dout[24]_i_4 ;
  wire [3:0]\Dout[4]_i_4 ;
  wire [3:0]\Dout[8]_i_4 ;
  wire [5:0]\Dout_reg[31] ;
  wire [0:0]\Dout_reg[3] ;
  wire [3:0]\Dout_reg[3]_0 ;
  wire [30:0]Q;
  wire [3:0]S;
  wire S0_carry__0_n_0;
  wire S0_carry__1_n_0;
  wire S0_carry__2_n_0;
  wire S0_carry__3_n_0;
  wire S0_carry__4_n_0;
  wire S0_carry__5_n_0;
  wire S0_carry_n_0;
  wire [25:0]S_Sub_in;
  wire [2:0]NLW_S0_carry_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__4_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__5_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__6_CO_UNCONNECTED;

  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry
       (.CI(1'b0),
        .CO({S0_carry_n_0,NLW_S0_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI(Q[3:0]),
        .O({\Dout_reg[31] [2:0],S_Sub_in[0]}),
        .S(S));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__0
       (.CI(S0_carry_n_0),
        .CO({S0_carry__0_n_0,NLW_S0_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O({S_Sub_in[2:1],\Dout_reg[31] [4:3]}),
        .S(\Dout[4]_i_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__1
       (.CI(S0_carry__0_n_0),
        .CO({S0_carry__1_n_0,NLW_S0_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[11:8]),
        .O(S_Sub_in[6:3]),
        .S(\Dout[8]_i_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__2
       (.CI(S0_carry__1_n_0),
        .CO({S0_carry__2_n_0,NLW_S0_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[15:12]),
        .O(S_Sub_in[10:7]),
        .S(\Dout[12]_i_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__3
       (.CI(S0_carry__2_n_0),
        .CO({S0_carry__3_n_0,NLW_S0_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[19:16]),
        .O(S_Sub_in[14:11]),
        .S(\Dout[16]_i_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__4
       (.CI(S0_carry__3_n_0),
        .CO({S0_carry__4_n_0,NLW_S0_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[23:20]),
        .O(S_Sub_in[18:15]),
        .S(\Dout[20]_i_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__5
       (.CI(S0_carry__4_n_0),
        .CO({S0_carry__5_n_0,NLW_S0_carry__5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[27:24]),
        .O(S_Sub_in[22:19]),
        .S(\Dout[24]_i_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__6
       (.CI(S0_carry__5_n_0),
        .CO({CO,NLW_S0_carry__6_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({\Dout_reg[3] ,Q[30:28]}),
        .O({\Dout_reg[31] [5],S_Sub_in[25:23]}),
        .S(\Dout_reg[3]_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
