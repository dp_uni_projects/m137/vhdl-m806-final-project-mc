// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Sun Oct 11 15:36:09 2020
// Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/mpliax/Documents/workspace/vhdl-m806-final-project-mc/vhdl-m806-final-project.sim/sim_1/impl/timing/xsim/RF_TB_time_impl.v
// Design      : RF_REGrwe
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module RAM32M_UNIQ_BASE_
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD1
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD10
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD11
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD2
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD3
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD4
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD5
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD6
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD7
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD8
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD9
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module REGISTER_FILE_n
   (DATA_OUT1__0,
    DATA_OUT2__0,
    CLK,
    REG_WRITE_in,
    Q,
    ADDR_R1_in,
    \Dout_reg[1] ,
    ADDR_R2_in);
  output [31:0]DATA_OUT1__0;
  output [31:0]DATA_OUT2__0;
  input CLK;
  input REG_WRITE_in;
  input [31:0]Q;
  input [3:0]ADDR_R1_in;
  input [3:0]\Dout_reg[1] ;
  input [3:0]ADDR_R2_in;

  wire [3:0]ADDR_R1_in;
  wire [3:0]ADDR_R2_in;
  wire CLK;
  wire [31:0]DATA_OUT1__0;
  wire [31:0]DATA_OUT2__0;
  wire [3:0]\Dout_reg[1] ;
  wire [31:0]Q;
  wire REG_WRITE_in;
  wire [1:0]NLW_RF_reg_r1_0_15_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_6_11_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_6_11_DOD_UNCONNECTED;

  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M_UNIQ_BASE_ RF_reg_r1_0_15_0_5
       (.ADDRA({1'b0,ADDR_R1_in}),
        .ADDRB({1'b0,ADDR_R1_in}),
        .ADDRC({1'b0,ADDR_R1_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[1:0]),
        .DIB(Q[3:2]),
        .DIC(Q[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1__0[1:0]),
        .DOB(DATA_OUT1__0[3:2]),
        .DOC(DATA_OUT1__0[5:4]),
        .DOD(NLW_RF_reg_r1_0_15_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "17" *) 
  RAM32M_HD1 RF_reg_r1_0_15_12_17
       (.ADDRA({1'b0,ADDR_R1_in}),
        .ADDRB({1'b0,ADDR_R1_in}),
        .ADDRC({1'b0,ADDR_R1_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[13:12]),
        .DIB(Q[15:14]),
        .DIC(Q[17:16]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1__0[13:12]),
        .DOB(DATA_OUT1__0[15:14]),
        .DOC(DATA_OUT1__0[17:16]),
        .DOD(NLW_RF_reg_r1_0_15_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAM32M_HD2 RF_reg_r1_0_15_18_23
       (.ADDRA({1'b0,ADDR_R1_in}),
        .ADDRB({1'b0,ADDR_R1_in}),
        .ADDRC({1'b0,ADDR_R1_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[19:18]),
        .DIB(Q[21:20]),
        .DIC(Q[23:22]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1__0[19:18]),
        .DOB(DATA_OUT1__0[21:20]),
        .DOC(DATA_OUT1__0[23:22]),
        .DOD(NLW_RF_reg_r1_0_15_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "29" *) 
  RAM32M_HD3 RF_reg_r1_0_15_24_29
       (.ADDRA({1'b0,ADDR_R1_in}),
        .ADDRB({1'b0,ADDR_R1_in}),
        .ADDRC({1'b0,ADDR_R1_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[25:24]),
        .DIB(Q[27:26]),
        .DIC(Q[29:28]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1__0[25:24]),
        .DOB(DATA_OUT1__0[27:26]),
        .DOC(DATA_OUT1__0[29:28]),
        .DOD(NLW_RF_reg_r1_0_15_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "31" *) 
  RAM32M_HD4 RF_reg_r1_0_15_30_31
       (.ADDRA({1'b0,ADDR_R1_in}),
        .ADDRB({1'b0,ADDR_R1_in}),
        .ADDRC({1'b0,ADDR_R1_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[31:30]),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1__0[31:30]),
        .DOB(NLW_RF_reg_r1_0_15_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_RF_reg_r1_0_15_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_RF_reg_r1_0_15_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M_HD5 RF_reg_r1_0_15_6_11
       (.ADDRA({1'b0,ADDR_R1_in}),
        .ADDRB({1'b0,ADDR_R1_in}),
        .ADDRC({1'b0,ADDR_R1_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[7:6]),
        .DIB(Q[9:8]),
        .DIC(Q[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1__0[7:6]),
        .DOB(DATA_OUT1__0[9:8]),
        .DOC(DATA_OUT1__0[11:10]),
        .DOD(NLW_RF_reg_r1_0_15_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M_HD6 RF_reg_r2_0_15_0_5
       (.ADDRA({1'b0,ADDR_R2_in}),
        .ADDRB({1'b0,ADDR_R2_in}),
        .ADDRC({1'b0,ADDR_R2_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[1:0]),
        .DIB(Q[3:2]),
        .DIC(Q[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2__0[1:0]),
        .DOB(DATA_OUT2__0[3:2]),
        .DOC(DATA_OUT2__0[5:4]),
        .DOD(NLW_RF_reg_r2_0_15_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "17" *) 
  RAM32M_HD7 RF_reg_r2_0_15_12_17
       (.ADDRA({1'b0,ADDR_R2_in}),
        .ADDRB({1'b0,ADDR_R2_in}),
        .ADDRC({1'b0,ADDR_R2_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[13:12]),
        .DIB(Q[15:14]),
        .DIC(Q[17:16]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2__0[13:12]),
        .DOB(DATA_OUT2__0[15:14]),
        .DOC(DATA_OUT2__0[17:16]),
        .DOD(NLW_RF_reg_r2_0_15_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAM32M_HD8 RF_reg_r2_0_15_18_23
       (.ADDRA({1'b0,ADDR_R2_in}),
        .ADDRB({1'b0,ADDR_R2_in}),
        .ADDRC({1'b0,ADDR_R2_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[19:18]),
        .DIB(Q[21:20]),
        .DIC(Q[23:22]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2__0[19:18]),
        .DOB(DATA_OUT2__0[21:20]),
        .DOC(DATA_OUT2__0[23:22]),
        .DOD(NLW_RF_reg_r2_0_15_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "29" *) 
  RAM32M_HD9 RF_reg_r2_0_15_24_29
       (.ADDRA({1'b0,ADDR_R2_in}),
        .ADDRB({1'b0,ADDR_R2_in}),
        .ADDRC({1'b0,ADDR_R2_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[25:24]),
        .DIB(Q[27:26]),
        .DIC(Q[29:28]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2__0[25:24]),
        .DOB(DATA_OUT2__0[27:26]),
        .DOC(DATA_OUT2__0[29:28]),
        .DOD(NLW_RF_reg_r2_0_15_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "31" *) 
  RAM32M_HD10 RF_reg_r2_0_15_30_31
       (.ADDRA({1'b0,ADDR_R2_in}),
        .ADDRB({1'b0,ADDR_R2_in}),
        .ADDRC({1'b0,ADDR_R2_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[31:30]),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2__0[31:30]),
        .DOB(NLW_RF_reg_r2_0_15_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_RF_reg_r2_0_15_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_RF_reg_r2_0_15_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "rf/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M_HD11 RF_reg_r2_0_15_6_11
       (.ADDRA({1'b0,ADDR_R2_in}),
        .ADDRB({1'b0,ADDR_R2_in}),
        .ADDRC({1'b0,ADDR_R2_in}),
        .ADDRD({1'b0,\Dout_reg[1] }),
        .DIA(Q[7:6]),
        .DIB(Q[9:8]),
        .DIC(Q[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2__0[7:6]),
        .DOB(DATA_OUT2__0[9:8]),
        .DOC(DATA_OUT2__0[11:10]),
        .DOD(NLW_RF_reg_r2_0_15_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(REG_WRITE_in));
endmodule

module REGISTER_FILE_wR15
   (DATA_OUT1__0,
    DATA_OUT2__0,
    CLK,
    REG_WRITE_in,
    Q,
    ADDR_R1_in,
    \Dout_reg[1] ,
    ADDR_R2_in);
  output [31:0]DATA_OUT1__0;
  output [31:0]DATA_OUT2__0;
  input CLK;
  input REG_WRITE_in;
  input [31:0]Q;
  input [3:0]ADDR_R1_in;
  input [3:0]\Dout_reg[1] ;
  input [3:0]ADDR_R2_in;

  wire [3:0]ADDR_R1_in;
  wire [3:0]ADDR_R2_in;
  wire CLK;
  wire [31:0]DATA_OUT1__0;
  wire [31:0]DATA_OUT2__0;
  wire [3:0]\Dout_reg[1] ;
  wire [31:0]Q;
  wire REG_WRITE_in;

  REGISTER_FILE_n REG_FILE_IN
       (.ADDR_R1_in(ADDR_R1_in),
        .ADDR_R2_in(ADDR_R2_in),
        .CLK(CLK),
        .DATA_OUT1__0(DATA_OUT1__0),
        .DATA_OUT2__0(DATA_OUT2__0),
        .\Dout_reg[1] (\Dout_reg[1] ),
        .Q(Q),
        .REG_WRITE_in(REG_WRITE_in));
endmodule

module REGrwe_n
   (ADDR_R1_in,
    D,
    SR,
    E,
    ADDR_R1_IBUF,
    CLK,
    Q,
    DATA_OUT1__0);
  output [3:0]ADDR_R1_in;
  output [31:0]D;
  input [0:0]SR;
  input [0:0]E;
  input [3:0]ADDR_R1_IBUF;
  input CLK;
  input [31:0]Q;
  input [31:0]DATA_OUT1__0;

  wire [3:0]ADDR_R1_IBUF;
  wire [3:0]ADDR_R1_in;
  wire CLK;
  wire [31:0]D;
  wire [31:0]DATA_OUT1__0;
  wire [0:0]E;
  wire [31:0]Q;
  wire [0:0]SR;

  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[0]_i_1 
       (.I0(Q[0]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[0]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[10]_i_1 
       (.I0(Q[10]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[10]),
        .O(D[10]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[11]_i_1 
       (.I0(Q[11]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[11]),
        .O(D[11]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[12]_i_1 
       (.I0(Q[12]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[12]),
        .O(D[12]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[13]_i_1 
       (.I0(Q[13]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[13]),
        .O(D[13]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[14]_i_1 
       (.I0(Q[14]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[14]),
        .O(D[14]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[15]_i_1 
       (.I0(Q[15]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[15]),
        .O(D[15]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[16]_i_1 
       (.I0(Q[16]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[16]),
        .O(D[16]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[17]_i_1 
       (.I0(Q[17]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[17]),
        .O(D[17]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[18]_i_1 
       (.I0(Q[18]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[18]),
        .O(D[18]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[19]_i_1 
       (.I0(Q[19]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[19]),
        .O(D[19]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[1]_i_1 
       (.I0(Q[1]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[1]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[20]_i_1 
       (.I0(Q[20]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[20]),
        .O(D[20]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[21]_i_1 
       (.I0(Q[21]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[21]),
        .O(D[21]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[22]_i_1 
       (.I0(Q[22]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[22]),
        .O(D[22]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[23]_i_1 
       (.I0(Q[23]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[23]),
        .O(D[23]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[24]_i_1 
       (.I0(Q[24]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[24]),
        .O(D[24]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[25]_i_1 
       (.I0(Q[25]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[25]),
        .O(D[25]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[26]_i_1 
       (.I0(Q[26]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[26]),
        .O(D[26]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[27]_i_1 
       (.I0(Q[27]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[27]),
        .O(D[27]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[28]_i_1 
       (.I0(Q[28]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[28]),
        .O(D[28]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[29]_i_1 
       (.I0(Q[29]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[29]),
        .O(D[29]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[2]_i_1 
       (.I0(Q[2]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[2]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[30]_i_1 
       (.I0(Q[30]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[30]),
        .O(D[30]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[31]_i_1 
       (.I0(Q[31]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[31]),
        .O(D[31]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[3]_i_1 
       (.I0(Q[3]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[3]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[4]_i_1 
       (.I0(Q[4]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[4]),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[5]_i_1 
       (.I0(Q[5]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[5]),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[6]_i_1 
       (.I0(Q[6]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[6]),
        .O(D[6]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[7]_i_1 
       (.I0(Q[7]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[7]),
        .O(D[7]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[8]_i_1 
       (.I0(Q[8]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[8]),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[9]_i_1 
       (.I0(Q[9]),
        .I1(ADDR_R1_in[2]),
        .I2(ADDR_R1_in[3]),
        .I3(ADDR_R1_in[0]),
        .I4(ADDR_R1_in[1]),
        .I5(DATA_OUT1__0[9]),
        .O(D[9]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(ADDR_R1_IBUF[0]),
        .Q(ADDR_R1_in[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(ADDR_R1_IBUF[1]),
        .Q(ADDR_R1_in[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(ADDR_R1_IBUF[2]),
        .Q(ADDR_R1_in[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(ADDR_R1_IBUF[3]),
        .Q(ADDR_R1_in[3]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_0
   (ADDR_R2_in,
    D,
    SR,
    E,
    ADDR_R2_IBUF,
    CLK,
    Q,
    DATA_OUT2__0);
  output [3:0]ADDR_R2_in;
  output [31:0]D;
  input [0:0]SR;
  input [0:0]E;
  input [3:0]ADDR_R2_IBUF;
  input CLK;
  input [31:0]Q;
  input [31:0]DATA_OUT2__0;

  wire [3:0]ADDR_R2_IBUF;
  wire [3:0]ADDR_R2_in;
  wire CLK;
  wire [31:0]D;
  wire [31:0]DATA_OUT2__0;
  wire [0:0]E;
  wire [31:0]Q;
  wire [0:0]SR;

  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[0]_i_1__0 
       (.I0(Q[0]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[0]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[10]_i_1__0 
       (.I0(Q[10]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[10]),
        .O(D[10]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[11]_i_1__0 
       (.I0(Q[11]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[11]),
        .O(D[11]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[12]_i_1__0 
       (.I0(Q[12]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[12]),
        .O(D[12]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[13]_i_1__0 
       (.I0(Q[13]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[13]),
        .O(D[13]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[14]_i_1__0 
       (.I0(Q[14]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[14]),
        .O(D[14]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[15]_i_1__0 
       (.I0(Q[15]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[15]),
        .O(D[15]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[16]_i_1__0 
       (.I0(Q[16]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[16]),
        .O(D[16]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[17]_i_1__0 
       (.I0(Q[17]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[17]),
        .O(D[17]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[18]_i_1__0 
       (.I0(Q[18]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[18]),
        .O(D[18]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[19]_i_1__0 
       (.I0(Q[19]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[19]),
        .O(D[19]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[1]_i_1__0 
       (.I0(Q[1]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[1]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[20]_i_1__0 
       (.I0(Q[20]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[20]),
        .O(D[20]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[21]_i_1__0 
       (.I0(Q[21]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[21]),
        .O(D[21]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[22]_i_1__0 
       (.I0(Q[22]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[22]),
        .O(D[22]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[23]_i_1__0 
       (.I0(Q[23]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[23]),
        .O(D[23]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[24]_i_1__0 
       (.I0(Q[24]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[24]),
        .O(D[24]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[25]_i_1__0 
       (.I0(Q[25]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[25]),
        .O(D[25]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[26]_i_1__0 
       (.I0(Q[26]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[26]),
        .O(D[26]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[27]_i_1__0 
       (.I0(Q[27]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[27]),
        .O(D[27]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[28]_i_1__0 
       (.I0(Q[28]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[28]),
        .O(D[28]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[29]_i_1__0 
       (.I0(Q[29]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[29]),
        .O(D[29]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[2]_i_1__0 
       (.I0(Q[2]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[2]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[30]_i_1__0 
       (.I0(Q[30]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[30]),
        .O(D[30]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[31]_i_1__0 
       (.I0(Q[31]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[31]),
        .O(D[31]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[3]_i_1__0 
       (.I0(Q[3]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[3]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[4]_i_1__0 
       (.I0(Q[4]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[4]),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[5]_i_1__0 
       (.I0(Q[5]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[5]),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[6]_i_1__0 
       (.I0(Q[6]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[6]),
        .O(D[6]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[7]_i_1__0 
       (.I0(Q[7]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[7]),
        .O(D[7]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[8]_i_1__0 
       (.I0(Q[8]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[8]),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \Dout[9]_i_1__0 
       (.I0(Q[9]),
        .I1(ADDR_R2_in[2]),
        .I2(ADDR_R2_in[3]),
        .I3(ADDR_R2_in[0]),
        .I4(ADDR_R2_in[1]),
        .I5(DATA_OUT2__0[9]),
        .O(D[9]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(ADDR_R2_IBUF[0]),
        .Q(ADDR_R2_in[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(ADDR_R2_IBUF[1]),
        .Q(ADDR_R2_in[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(ADDR_R2_IBUF[2]),
        .Q(ADDR_R2_in[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(ADDR_R2_IBUF[3]),
        .Q(ADDR_R2_in[3]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_1
   (Q,
    SR,
    E,
    D,
    CLK);
  output [3:0]Q;
  input [0:0]SR;
  input [0:0]E;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [0:0]E;
  wire [3:0]Q;
  wire [0:0]SR;

  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(D[3]),
        .Q(Q[3]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n__parameterized2
   (Q,
    SR,
    E,
    D,
    CLK);
  output [31:0]Q;
  input [0:0]SR;
  input [0:0]E;
  input [31:0]D;
  input CLK;

  wire CLK;
  wire [31:0]D;
  wire [0:0]E;
  wire [31:0]Q;
  wire [0:0]SR;

  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK),
        .CE(E),
        .D(D[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK),
        .CE(E),
        .D(D[11]),
        .Q(Q[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK),
        .CE(E),
        .D(D[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK),
        .CE(E),
        .D(D[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(E),
        .D(D[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(E),
        .D(D[15]),
        .Q(Q[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK),
        .CE(E),
        .D(D[16]),
        .Q(Q[16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK),
        .CE(E),
        .D(D[17]),
        .Q(Q[17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK),
        .CE(E),
        .D(D[18]),
        .Q(Q[18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK),
        .CE(E),
        .D(D[19]),
        .Q(Q[19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK),
        .CE(E),
        .D(D[20]),
        .Q(Q[20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK),
        .CE(E),
        .D(D[21]),
        .Q(Q[21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK),
        .CE(E),
        .D(D[22]),
        .Q(Q[22]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(E),
        .D(D[23]),
        .Q(Q[23]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK),
        .CE(E),
        .D(D[24]),
        .Q(Q[24]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK),
        .CE(E),
        .D(D[25]),
        .Q(Q[25]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK),
        .CE(E),
        .D(D[26]),
        .Q(Q[26]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK),
        .CE(E),
        .D(D[27]),
        .Q(Q[27]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK),
        .CE(E),
        .D(D[28]),
        .Q(Q[28]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK),
        .CE(E),
        .D(D[29]),
        .Q(Q[29]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK),
        .CE(E),
        .D(D[30]),
        .Q(Q[30]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(E),
        .D(D[31]),
        .Q(Q[31]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(D[3]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(D[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(D[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(D[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(D[7]),
        .Q(Q[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK),
        .CE(E),
        .D(D[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK),
        .CE(E),
        .D(D[9]),
        .Q(Q[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n__parameterized2_2
   (Q,
    SR,
    E,
    D,
    CLK);
  output [31:0]Q;
  input [0:0]SR;
  input [0:0]E;
  input [31:0]D;
  input CLK;

  wire CLK;
  wire [31:0]D;
  wire [0:0]E;
  wire [31:0]Q;
  wire [0:0]SR;

  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK),
        .CE(E),
        .D(D[10]),
        .Q(Q[10]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK),
        .CE(E),
        .D(D[11]),
        .Q(Q[11]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK),
        .CE(E),
        .D(D[12]),
        .Q(Q[12]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK),
        .CE(E),
        .D(D[13]),
        .Q(Q[13]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(E),
        .D(D[14]),
        .Q(Q[14]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(E),
        .D(D[15]),
        .Q(Q[15]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK),
        .CE(E),
        .D(D[16]),
        .Q(Q[16]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK),
        .CE(E),
        .D(D[17]),
        .Q(Q[17]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK),
        .CE(E),
        .D(D[18]),
        .Q(Q[18]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK),
        .CE(E),
        .D(D[19]),
        .Q(Q[19]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK),
        .CE(E),
        .D(D[20]),
        .Q(Q[20]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK),
        .CE(E),
        .D(D[21]),
        .Q(Q[21]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK),
        .CE(E),
        .D(D[22]),
        .Q(Q[22]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(E),
        .D(D[23]),
        .Q(Q[23]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK),
        .CE(E),
        .D(D[24]),
        .Q(Q[24]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK),
        .CE(E),
        .D(D[25]),
        .Q(Q[25]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK),
        .CE(E),
        .D(D[26]),
        .Q(Q[26]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK),
        .CE(E),
        .D(D[27]),
        .Q(Q[27]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK),
        .CE(E),
        .D(D[28]),
        .Q(Q[28]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK),
        .CE(E),
        .D(D[29]),
        .Q(Q[29]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK),
        .CE(E),
        .D(D[30]),
        .Q(Q[30]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(E),
        .D(D[31]),
        .Q(Q[31]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(D[3]),
        .Q(Q[3]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(D[4]),
        .Q(Q[4]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(D[5]),
        .Q(Q[5]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(D[6]),
        .Q(Q[6]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(D[7]),
        .Q(Q[7]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK),
        .CE(E),
        .D(D[8]),
        .Q(Q[8]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK),
        .CE(E),
        .D(D[9]),
        .Q(Q[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n__parameterized2_3
   (Q,
    SR,
    WE,
    D,
    CLK);
  output [31:0]Q;
  input [0:0]SR;
  input WE;
  input [31:0]D;
  input CLK;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire [0:0]SR;
  wire WE;

  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(WE),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK),
        .CE(WE),
        .D(D[10]),
        .Q(Q[10]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK),
        .CE(WE),
        .D(D[11]),
        .Q(Q[11]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK),
        .CE(WE),
        .D(D[12]),
        .Q(Q[12]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK),
        .CE(WE),
        .D(D[13]),
        .Q(Q[13]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(WE),
        .D(D[14]),
        .Q(Q[14]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(WE),
        .D(D[15]),
        .Q(Q[15]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK),
        .CE(WE),
        .D(D[16]),
        .Q(Q[16]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK),
        .CE(WE),
        .D(D[17]),
        .Q(Q[17]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK),
        .CE(WE),
        .D(D[18]),
        .Q(Q[18]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK),
        .CE(WE),
        .D(D[19]),
        .Q(Q[19]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(WE),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK),
        .CE(WE),
        .D(D[20]),
        .Q(Q[20]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK),
        .CE(WE),
        .D(D[21]),
        .Q(Q[21]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK),
        .CE(WE),
        .D(D[22]),
        .Q(Q[22]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(WE),
        .D(D[23]),
        .Q(Q[23]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK),
        .CE(WE),
        .D(D[24]),
        .Q(Q[24]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK),
        .CE(WE),
        .D(D[25]),
        .Q(Q[25]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK),
        .CE(WE),
        .D(D[26]),
        .Q(Q[26]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK),
        .CE(WE),
        .D(D[27]),
        .Q(Q[27]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK),
        .CE(WE),
        .D(D[28]),
        .Q(Q[28]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK),
        .CE(WE),
        .D(D[29]),
        .Q(Q[29]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(WE),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK),
        .CE(WE),
        .D(D[30]),
        .Q(Q[30]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(WE),
        .D(D[31]),
        .Q(Q[31]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(WE),
        .D(D[3]),
        .Q(Q[3]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(WE),
        .D(D[4]),
        .Q(Q[4]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK),
        .CE(WE),
        .D(D[5]),
        .Q(Q[5]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(WE),
        .D(D[6]),
        .Q(Q[6]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK),
        .CE(WE),
        .D(D[7]),
        .Q(Q[7]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK),
        .CE(WE),
        .D(D[8]),
        .Q(Q[8]),
        .R(SR));
  (* IOB = "TRUE" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK),
        .CE(WE),
        .D(D[9]),
        .Q(Q[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n__parameterized2_4
   (Q,
    RESET,
    WE,
    D,
    CLK);
  output [31:0]Q;
  input RESET;
  input WE;
  input [31:0]D;
  input CLK;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire RESET;
  wire WE;

  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(WE),
        .D(D[0]),
        .Q(Q[0]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK),
        .CE(WE),
        .D(D[10]),
        .Q(Q[10]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK),
        .CE(WE),
        .D(D[11]),
        .Q(Q[11]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK),
        .CE(WE),
        .D(D[12]),
        .Q(Q[12]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK),
        .CE(WE),
        .D(D[13]),
        .Q(Q[13]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(WE),
        .D(D[14]),
        .Q(Q[14]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(WE),
        .D(D[15]),
        .Q(Q[15]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK),
        .CE(WE),
        .D(D[16]),
        .Q(Q[16]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK),
        .CE(WE),
        .D(D[17]),
        .Q(Q[17]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK),
        .CE(WE),
        .D(D[18]),
        .Q(Q[18]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK),
        .CE(WE),
        .D(D[19]),
        .Q(Q[19]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(WE),
        .D(D[1]),
        .Q(Q[1]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK),
        .CE(WE),
        .D(D[20]),
        .Q(Q[20]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK),
        .CE(WE),
        .D(D[21]),
        .Q(Q[21]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK),
        .CE(WE),
        .D(D[22]),
        .Q(Q[22]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(WE),
        .D(D[23]),
        .Q(Q[23]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK),
        .CE(WE),
        .D(D[24]),
        .Q(Q[24]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK),
        .CE(WE),
        .D(D[25]),
        .Q(Q[25]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK),
        .CE(WE),
        .D(D[26]),
        .Q(Q[26]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK),
        .CE(WE),
        .D(D[27]),
        .Q(Q[27]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK),
        .CE(WE),
        .D(D[28]),
        .Q(Q[28]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK),
        .CE(WE),
        .D(D[29]),
        .Q(Q[29]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(WE),
        .D(D[2]),
        .Q(Q[2]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK),
        .CE(WE),
        .D(D[30]),
        .Q(Q[30]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(WE),
        .D(D[31]),
        .Q(Q[31]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(WE),
        .D(D[3]),
        .Q(Q[3]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(WE),
        .D(D[4]),
        .Q(Q[4]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK),
        .CE(WE),
        .D(D[5]),
        .Q(Q[5]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(WE),
        .D(D[6]),
        .Q(Q[6]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK),
        .CE(WE),
        .D(D[7]),
        .Q(Q[7]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK),
        .CE(WE),
        .D(D[8]),
        .Q(Q[8]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK),
        .CE(WE),
        .D(D[9]),
        .Q(Q[9]),
        .R(RESET));
endmodule

(* ECO_CHECKSUM = "10cb6e2b" *) 
(* NotValidForBitStream *)
module RF_REGrwe
   (CLK,
    WE,
    REG_WRITE,
    ADDR_W,
    ADDR_R1,
    ADDR_R2,
    DATA_IN,
    R15,
    RESET,
    DATA_OUT1,
    DATA_OUT2);
  input CLK;
  input WE;
  input REG_WRITE;
  input [3:0]ADDR_W;
  input [3:0]ADDR_R1;
  input [3:0]ADDR_R2;
  input [31:0]DATA_IN;
  input [31:0]R15;
  input RESET;
  output [31:0]DATA_OUT1;
  output [31:0]DATA_OUT2;

  wire [3:0]ADDR_R1;
  wire [3:0]ADDR_R1_IBUF;
  wire [3:0]ADDR_R1_in;
  wire [3:0]ADDR_R2;
  wire [3:0]ADDR_R2_IBUF;
  wire [3:0]ADDR_R2_in;
  wire [3:0]ADDR_W;
  wire [3:0]ADDR_W_IBUF;
  wire CLK;
  wire CLK_IBUF;
  wire CLK_IBUF_BUFG;
  wire [31:0]DATA_IN;
  wire [31:0]DATA_IN_IBUF;
  wire [31:0]DATA_OUT1;
  wire [31:0]DATA_OUT1_OBUF;
  wire [31:0]DATA_OUT1__0;
  wire [31:0]DATA_OUT2;
  wire [31:0]DATA_OUT2_OBUF;
  wire [31:0]DATA_OUT2__0;
  wire [31:0]Din;
  wire [3:0]Dout;
  wire [31:0]R15;
  wire [31:0]R15_IBUF;
  wire REG_WRITE;
  wire REG_WRITE_IBUF;
  wire REG_WRITE_in;
  wire RESET;
  wire RESET_IBUF;
  wire WE;
  wire WE_IBUF;
  wire reg_addr_r2_n_10;
  wire reg_addr_r2_n_11;
  wire reg_addr_r2_n_12;
  wire reg_addr_r2_n_13;
  wire reg_addr_r2_n_14;
  wire reg_addr_r2_n_15;
  wire reg_addr_r2_n_16;
  wire reg_addr_r2_n_17;
  wire reg_addr_r2_n_18;
  wire reg_addr_r2_n_19;
  wire reg_addr_r2_n_20;
  wire reg_addr_r2_n_21;
  wire reg_addr_r2_n_22;
  wire reg_addr_r2_n_23;
  wire reg_addr_r2_n_24;
  wire reg_addr_r2_n_25;
  wire reg_addr_r2_n_26;
  wire reg_addr_r2_n_27;
  wire reg_addr_r2_n_28;
  wire reg_addr_r2_n_29;
  wire reg_addr_r2_n_30;
  wire reg_addr_r2_n_31;
  wire reg_addr_r2_n_32;
  wire reg_addr_r2_n_33;
  wire reg_addr_r2_n_34;
  wire reg_addr_r2_n_35;
  wire reg_addr_r2_n_4;
  wire reg_addr_r2_n_5;
  wire reg_addr_r2_n_6;
  wire reg_addr_r2_n_7;
  wire reg_addr_r2_n_8;
  wire reg_addr_r2_n_9;
  wire reg_data_in_n_0;
  wire reg_data_in_n_1;
  wire reg_data_in_n_10;
  wire reg_data_in_n_11;
  wire reg_data_in_n_12;
  wire reg_data_in_n_13;
  wire reg_data_in_n_14;
  wire reg_data_in_n_15;
  wire reg_data_in_n_16;
  wire reg_data_in_n_17;
  wire reg_data_in_n_18;
  wire reg_data_in_n_19;
  wire reg_data_in_n_2;
  wire reg_data_in_n_20;
  wire reg_data_in_n_21;
  wire reg_data_in_n_22;
  wire reg_data_in_n_23;
  wire reg_data_in_n_24;
  wire reg_data_in_n_25;
  wire reg_data_in_n_26;
  wire reg_data_in_n_27;
  wire reg_data_in_n_28;
  wire reg_data_in_n_29;
  wire reg_data_in_n_3;
  wire reg_data_in_n_30;
  wire reg_data_in_n_31;
  wire reg_data_in_n_4;
  wire reg_data_in_n_5;
  wire reg_data_in_n_6;
  wire reg_data_in_n_7;
  wire reg_data_in_n_8;
  wire reg_data_in_n_9;
  wire reg_r15_n_0;
  wire reg_r15_n_1;
  wire reg_r15_n_10;
  wire reg_r15_n_11;
  wire reg_r15_n_12;
  wire reg_r15_n_13;
  wire reg_r15_n_14;
  wire reg_r15_n_15;
  wire reg_r15_n_16;
  wire reg_r15_n_17;
  wire reg_r15_n_18;
  wire reg_r15_n_19;
  wire reg_r15_n_2;
  wire reg_r15_n_20;
  wire reg_r15_n_21;
  wire reg_r15_n_22;
  wire reg_r15_n_23;
  wire reg_r15_n_24;
  wire reg_r15_n_25;
  wire reg_r15_n_26;
  wire reg_r15_n_27;
  wire reg_r15_n_28;
  wire reg_r15_n_29;
  wire reg_r15_n_3;
  wire reg_r15_n_30;
  wire reg_r15_n_31;
  wire reg_r15_n_4;
  wire reg_r15_n_5;
  wire reg_r15_n_6;
  wire reg_r15_n_7;
  wire reg_r15_n_8;
  wire reg_r15_n_9;

initial begin
 $sdf_annotate("RF_TB_time_impl.sdf",,,,"tool_control");
end
  IBUF \ADDR_R1_IBUF[0]_inst 
       (.I(ADDR_R1[0]),
        .O(ADDR_R1_IBUF[0]));
  IBUF \ADDR_R1_IBUF[1]_inst 
       (.I(ADDR_R1[1]),
        .O(ADDR_R1_IBUF[1]));
  IBUF \ADDR_R1_IBUF[2]_inst 
       (.I(ADDR_R1[2]),
        .O(ADDR_R1_IBUF[2]));
  IBUF \ADDR_R1_IBUF[3]_inst 
       (.I(ADDR_R1[3]),
        .O(ADDR_R1_IBUF[3]));
  IBUF \ADDR_R2_IBUF[0]_inst 
       (.I(ADDR_R2[0]),
        .O(ADDR_R2_IBUF[0]));
  IBUF \ADDR_R2_IBUF[1]_inst 
       (.I(ADDR_R2[1]),
        .O(ADDR_R2_IBUF[1]));
  IBUF \ADDR_R2_IBUF[2]_inst 
       (.I(ADDR_R2[2]),
        .O(ADDR_R2_IBUF[2]));
  IBUF \ADDR_R2_IBUF[3]_inst 
       (.I(ADDR_R2[3]),
        .O(ADDR_R2_IBUF[3]));
  IBUF \ADDR_W_IBUF[0]_inst 
       (.I(ADDR_W[0]),
        .O(ADDR_W_IBUF[0]));
  IBUF \ADDR_W_IBUF[1]_inst 
       (.I(ADDR_W[1]),
        .O(ADDR_W_IBUF[1]));
  IBUF \ADDR_W_IBUF[2]_inst 
       (.I(ADDR_W[2]),
        .O(ADDR_W_IBUF[2]));
  IBUF \ADDR_W_IBUF[3]_inst 
       (.I(ADDR_W[3]),
        .O(ADDR_W_IBUF[3]));
  BUFG CLK_IBUF_BUFG_inst
       (.I(CLK_IBUF),
        .O(CLK_IBUF_BUFG));
  IBUF CLK_IBUF_inst
       (.I(CLK),
        .O(CLK_IBUF));
  IBUF \DATA_IN_IBUF[0]_inst 
       (.I(DATA_IN[0]),
        .O(DATA_IN_IBUF[0]));
  IBUF \DATA_IN_IBUF[10]_inst 
       (.I(DATA_IN[10]),
        .O(DATA_IN_IBUF[10]));
  IBUF \DATA_IN_IBUF[11]_inst 
       (.I(DATA_IN[11]),
        .O(DATA_IN_IBUF[11]));
  IBUF \DATA_IN_IBUF[12]_inst 
       (.I(DATA_IN[12]),
        .O(DATA_IN_IBUF[12]));
  IBUF \DATA_IN_IBUF[13]_inst 
       (.I(DATA_IN[13]),
        .O(DATA_IN_IBUF[13]));
  IBUF \DATA_IN_IBUF[14]_inst 
       (.I(DATA_IN[14]),
        .O(DATA_IN_IBUF[14]));
  IBUF \DATA_IN_IBUF[15]_inst 
       (.I(DATA_IN[15]),
        .O(DATA_IN_IBUF[15]));
  IBUF \DATA_IN_IBUF[16]_inst 
       (.I(DATA_IN[16]),
        .O(DATA_IN_IBUF[16]));
  IBUF \DATA_IN_IBUF[17]_inst 
       (.I(DATA_IN[17]),
        .O(DATA_IN_IBUF[17]));
  IBUF \DATA_IN_IBUF[18]_inst 
       (.I(DATA_IN[18]),
        .O(DATA_IN_IBUF[18]));
  IBUF \DATA_IN_IBUF[19]_inst 
       (.I(DATA_IN[19]),
        .O(DATA_IN_IBUF[19]));
  IBUF \DATA_IN_IBUF[1]_inst 
       (.I(DATA_IN[1]),
        .O(DATA_IN_IBUF[1]));
  IBUF \DATA_IN_IBUF[20]_inst 
       (.I(DATA_IN[20]),
        .O(DATA_IN_IBUF[20]));
  IBUF \DATA_IN_IBUF[21]_inst 
       (.I(DATA_IN[21]),
        .O(DATA_IN_IBUF[21]));
  IBUF \DATA_IN_IBUF[22]_inst 
       (.I(DATA_IN[22]),
        .O(DATA_IN_IBUF[22]));
  IBUF \DATA_IN_IBUF[23]_inst 
       (.I(DATA_IN[23]),
        .O(DATA_IN_IBUF[23]));
  IBUF \DATA_IN_IBUF[24]_inst 
       (.I(DATA_IN[24]),
        .O(DATA_IN_IBUF[24]));
  IBUF \DATA_IN_IBUF[25]_inst 
       (.I(DATA_IN[25]),
        .O(DATA_IN_IBUF[25]));
  IBUF \DATA_IN_IBUF[26]_inst 
       (.I(DATA_IN[26]),
        .O(DATA_IN_IBUF[26]));
  IBUF \DATA_IN_IBUF[27]_inst 
       (.I(DATA_IN[27]),
        .O(DATA_IN_IBUF[27]));
  IBUF \DATA_IN_IBUF[28]_inst 
       (.I(DATA_IN[28]),
        .O(DATA_IN_IBUF[28]));
  IBUF \DATA_IN_IBUF[29]_inst 
       (.I(DATA_IN[29]),
        .O(DATA_IN_IBUF[29]));
  IBUF \DATA_IN_IBUF[2]_inst 
       (.I(DATA_IN[2]),
        .O(DATA_IN_IBUF[2]));
  IBUF \DATA_IN_IBUF[30]_inst 
       (.I(DATA_IN[30]),
        .O(DATA_IN_IBUF[30]));
  IBUF \DATA_IN_IBUF[31]_inst 
       (.I(DATA_IN[31]),
        .O(DATA_IN_IBUF[31]));
  IBUF \DATA_IN_IBUF[3]_inst 
       (.I(DATA_IN[3]),
        .O(DATA_IN_IBUF[3]));
  IBUF \DATA_IN_IBUF[4]_inst 
       (.I(DATA_IN[4]),
        .O(DATA_IN_IBUF[4]));
  IBUF \DATA_IN_IBUF[5]_inst 
       (.I(DATA_IN[5]),
        .O(DATA_IN_IBUF[5]));
  IBUF \DATA_IN_IBUF[6]_inst 
       (.I(DATA_IN[6]),
        .O(DATA_IN_IBUF[6]));
  IBUF \DATA_IN_IBUF[7]_inst 
       (.I(DATA_IN[7]),
        .O(DATA_IN_IBUF[7]));
  IBUF \DATA_IN_IBUF[8]_inst 
       (.I(DATA_IN[8]),
        .O(DATA_IN_IBUF[8]));
  IBUF \DATA_IN_IBUF[9]_inst 
       (.I(DATA_IN[9]),
        .O(DATA_IN_IBUF[9]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[0]_inst 
       (.I(DATA_OUT1_OBUF[0]),
        .O(DATA_OUT1[0]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[10]_inst 
       (.I(DATA_OUT1_OBUF[10]),
        .O(DATA_OUT1[10]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[11]_inst 
       (.I(DATA_OUT1_OBUF[11]),
        .O(DATA_OUT1[11]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[12]_inst 
       (.I(DATA_OUT1_OBUF[12]),
        .O(DATA_OUT1[12]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[13]_inst 
       (.I(DATA_OUT1_OBUF[13]),
        .O(DATA_OUT1[13]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[14]_inst 
       (.I(DATA_OUT1_OBUF[14]),
        .O(DATA_OUT1[14]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[15]_inst 
       (.I(DATA_OUT1_OBUF[15]),
        .O(DATA_OUT1[15]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[16]_inst 
       (.I(DATA_OUT1_OBUF[16]),
        .O(DATA_OUT1[16]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[17]_inst 
       (.I(DATA_OUT1_OBUF[17]),
        .O(DATA_OUT1[17]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[18]_inst 
       (.I(DATA_OUT1_OBUF[18]),
        .O(DATA_OUT1[18]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[19]_inst 
       (.I(DATA_OUT1_OBUF[19]),
        .O(DATA_OUT1[19]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[1]_inst 
       (.I(DATA_OUT1_OBUF[1]),
        .O(DATA_OUT1[1]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[20]_inst 
       (.I(DATA_OUT1_OBUF[20]),
        .O(DATA_OUT1[20]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[21]_inst 
       (.I(DATA_OUT1_OBUF[21]),
        .O(DATA_OUT1[21]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[22]_inst 
       (.I(DATA_OUT1_OBUF[22]),
        .O(DATA_OUT1[22]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[23]_inst 
       (.I(DATA_OUT1_OBUF[23]),
        .O(DATA_OUT1[23]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[24]_inst 
       (.I(DATA_OUT1_OBUF[24]),
        .O(DATA_OUT1[24]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[25]_inst 
       (.I(DATA_OUT1_OBUF[25]),
        .O(DATA_OUT1[25]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[26]_inst 
       (.I(DATA_OUT1_OBUF[26]),
        .O(DATA_OUT1[26]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[27]_inst 
       (.I(DATA_OUT1_OBUF[27]),
        .O(DATA_OUT1[27]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[28]_inst 
       (.I(DATA_OUT1_OBUF[28]),
        .O(DATA_OUT1[28]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[29]_inst 
       (.I(DATA_OUT1_OBUF[29]),
        .O(DATA_OUT1[29]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[2]_inst 
       (.I(DATA_OUT1_OBUF[2]),
        .O(DATA_OUT1[2]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[30]_inst 
       (.I(DATA_OUT1_OBUF[30]),
        .O(DATA_OUT1[30]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[31]_inst 
       (.I(DATA_OUT1_OBUF[31]),
        .O(DATA_OUT1[31]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[3]_inst 
       (.I(DATA_OUT1_OBUF[3]),
        .O(DATA_OUT1[3]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[4]_inst 
       (.I(DATA_OUT1_OBUF[4]),
        .O(DATA_OUT1[4]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[5]_inst 
       (.I(DATA_OUT1_OBUF[5]),
        .O(DATA_OUT1[5]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[6]_inst 
       (.I(DATA_OUT1_OBUF[6]),
        .O(DATA_OUT1[6]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[7]_inst 
       (.I(DATA_OUT1_OBUF[7]),
        .O(DATA_OUT1[7]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[8]_inst 
       (.I(DATA_OUT1_OBUF[8]),
        .O(DATA_OUT1[8]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT1_OBUF[9]_inst 
       (.I(DATA_OUT1_OBUF[9]),
        .O(DATA_OUT1[9]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[0]_inst 
       (.I(DATA_OUT2_OBUF[0]),
        .O(DATA_OUT2[0]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[10]_inst 
       (.I(DATA_OUT2_OBUF[10]),
        .O(DATA_OUT2[10]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[11]_inst 
       (.I(DATA_OUT2_OBUF[11]),
        .O(DATA_OUT2[11]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[12]_inst 
       (.I(DATA_OUT2_OBUF[12]),
        .O(DATA_OUT2[12]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[13]_inst 
       (.I(DATA_OUT2_OBUF[13]),
        .O(DATA_OUT2[13]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[14]_inst 
       (.I(DATA_OUT2_OBUF[14]),
        .O(DATA_OUT2[14]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[15]_inst 
       (.I(DATA_OUT2_OBUF[15]),
        .O(DATA_OUT2[15]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[16]_inst 
       (.I(DATA_OUT2_OBUF[16]),
        .O(DATA_OUT2[16]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[17]_inst 
       (.I(DATA_OUT2_OBUF[17]),
        .O(DATA_OUT2[17]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[18]_inst 
       (.I(DATA_OUT2_OBUF[18]),
        .O(DATA_OUT2[18]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[19]_inst 
       (.I(DATA_OUT2_OBUF[19]),
        .O(DATA_OUT2[19]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[1]_inst 
       (.I(DATA_OUT2_OBUF[1]),
        .O(DATA_OUT2[1]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[20]_inst 
       (.I(DATA_OUT2_OBUF[20]),
        .O(DATA_OUT2[20]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[21]_inst 
       (.I(DATA_OUT2_OBUF[21]),
        .O(DATA_OUT2[21]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[22]_inst 
       (.I(DATA_OUT2_OBUF[22]),
        .O(DATA_OUT2[22]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[23]_inst 
       (.I(DATA_OUT2_OBUF[23]),
        .O(DATA_OUT2[23]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[24]_inst 
       (.I(DATA_OUT2_OBUF[24]),
        .O(DATA_OUT2[24]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[25]_inst 
       (.I(DATA_OUT2_OBUF[25]),
        .O(DATA_OUT2[25]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[26]_inst 
       (.I(DATA_OUT2_OBUF[26]),
        .O(DATA_OUT2[26]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[27]_inst 
       (.I(DATA_OUT2_OBUF[27]),
        .O(DATA_OUT2[27]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[28]_inst 
       (.I(DATA_OUT2_OBUF[28]),
        .O(DATA_OUT2[28]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[29]_inst 
       (.I(DATA_OUT2_OBUF[29]),
        .O(DATA_OUT2[29]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[2]_inst 
       (.I(DATA_OUT2_OBUF[2]),
        .O(DATA_OUT2[2]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[30]_inst 
       (.I(DATA_OUT2_OBUF[30]),
        .O(DATA_OUT2[30]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[31]_inst 
       (.I(DATA_OUT2_OBUF[31]),
        .O(DATA_OUT2[31]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[3]_inst 
       (.I(DATA_OUT2_OBUF[3]),
        .O(DATA_OUT2[3]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[4]_inst 
       (.I(DATA_OUT2_OBUF[4]),
        .O(DATA_OUT2[4]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[5]_inst 
       (.I(DATA_OUT2_OBUF[5]),
        .O(DATA_OUT2[5]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[6]_inst 
       (.I(DATA_OUT2_OBUF[6]),
        .O(DATA_OUT2[6]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[7]_inst 
       (.I(DATA_OUT2_OBUF[7]),
        .O(DATA_OUT2[7]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[8]_inst 
       (.I(DATA_OUT2_OBUF[8]),
        .O(DATA_OUT2[8]));
  (* IOB = "TRUE" *) 
  OBUF \DATA_OUT2_OBUF[9]_inst 
       (.I(DATA_OUT2_OBUF[9]),
        .O(DATA_OUT2[9]));
  IBUF \R15_IBUF[0]_inst 
       (.I(R15[0]),
        .O(R15_IBUF[0]));
  IBUF \R15_IBUF[10]_inst 
       (.I(R15[10]),
        .O(R15_IBUF[10]));
  IBUF \R15_IBUF[11]_inst 
       (.I(R15[11]),
        .O(R15_IBUF[11]));
  IBUF \R15_IBUF[12]_inst 
       (.I(R15[12]),
        .O(R15_IBUF[12]));
  IBUF \R15_IBUF[13]_inst 
       (.I(R15[13]),
        .O(R15_IBUF[13]));
  IBUF \R15_IBUF[14]_inst 
       (.I(R15[14]),
        .O(R15_IBUF[14]));
  IBUF \R15_IBUF[15]_inst 
       (.I(R15[15]),
        .O(R15_IBUF[15]));
  IBUF \R15_IBUF[16]_inst 
       (.I(R15[16]),
        .O(R15_IBUF[16]));
  IBUF \R15_IBUF[17]_inst 
       (.I(R15[17]),
        .O(R15_IBUF[17]));
  IBUF \R15_IBUF[18]_inst 
       (.I(R15[18]),
        .O(R15_IBUF[18]));
  IBUF \R15_IBUF[19]_inst 
       (.I(R15[19]),
        .O(R15_IBUF[19]));
  IBUF \R15_IBUF[1]_inst 
       (.I(R15[1]),
        .O(R15_IBUF[1]));
  IBUF \R15_IBUF[20]_inst 
       (.I(R15[20]),
        .O(R15_IBUF[20]));
  IBUF \R15_IBUF[21]_inst 
       (.I(R15[21]),
        .O(R15_IBUF[21]));
  IBUF \R15_IBUF[22]_inst 
       (.I(R15[22]),
        .O(R15_IBUF[22]));
  IBUF \R15_IBUF[23]_inst 
       (.I(R15[23]),
        .O(R15_IBUF[23]));
  IBUF \R15_IBUF[24]_inst 
       (.I(R15[24]),
        .O(R15_IBUF[24]));
  IBUF \R15_IBUF[25]_inst 
       (.I(R15[25]),
        .O(R15_IBUF[25]));
  IBUF \R15_IBUF[26]_inst 
       (.I(R15[26]),
        .O(R15_IBUF[26]));
  IBUF \R15_IBUF[27]_inst 
       (.I(R15[27]),
        .O(R15_IBUF[27]));
  IBUF \R15_IBUF[28]_inst 
       (.I(R15[28]),
        .O(R15_IBUF[28]));
  IBUF \R15_IBUF[29]_inst 
       (.I(R15[29]),
        .O(R15_IBUF[29]));
  IBUF \R15_IBUF[2]_inst 
       (.I(R15[2]),
        .O(R15_IBUF[2]));
  IBUF \R15_IBUF[30]_inst 
       (.I(R15[30]),
        .O(R15_IBUF[30]));
  IBUF \R15_IBUF[31]_inst 
       (.I(R15[31]),
        .O(R15_IBUF[31]));
  IBUF \R15_IBUF[3]_inst 
       (.I(R15[3]),
        .O(R15_IBUF[3]));
  IBUF \R15_IBUF[4]_inst 
       (.I(R15[4]),
        .O(R15_IBUF[4]));
  IBUF \R15_IBUF[5]_inst 
       (.I(R15[5]),
        .O(R15_IBUF[5]));
  IBUF \R15_IBUF[6]_inst 
       (.I(R15[6]),
        .O(R15_IBUF[6]));
  IBUF \R15_IBUF[7]_inst 
       (.I(R15[7]),
        .O(R15_IBUF[7]));
  IBUF \R15_IBUF[8]_inst 
       (.I(R15[8]),
        .O(R15_IBUF[8]));
  IBUF \R15_IBUF[9]_inst 
       (.I(R15[9]),
        .O(R15_IBUF[9]));
  IBUF REG_WRITE_IBUF_inst
       (.I(REG_WRITE),
        .O(REG_WRITE_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    REG_WRITE_in_reg
       (.C(CLK_IBUF_BUFG),
        .CE(WE_IBUF),
        .D(REG_WRITE_IBUF),
        .Q(REG_WRITE_in),
        .R(RESET_IBUF));
  IBUF RESET_IBUF_inst
       (.I(RESET),
        .O(RESET_IBUF));
  IBUF WE_IBUF_inst
       (.I(WE),
        .O(WE_IBUF));
  REGrwe_n reg_addr_r1
       (.ADDR_R1_IBUF(ADDR_R1_IBUF),
        .ADDR_R1_in(ADDR_R1_in),
        .CLK(CLK_IBUF_BUFG),
        .D(Din),
        .DATA_OUT1__0(DATA_OUT1__0),
        .E(WE_IBUF),
        .Q({reg_r15_n_0,reg_r15_n_1,reg_r15_n_2,reg_r15_n_3,reg_r15_n_4,reg_r15_n_5,reg_r15_n_6,reg_r15_n_7,reg_r15_n_8,reg_r15_n_9,reg_r15_n_10,reg_r15_n_11,reg_r15_n_12,reg_r15_n_13,reg_r15_n_14,reg_r15_n_15,reg_r15_n_16,reg_r15_n_17,reg_r15_n_18,reg_r15_n_19,reg_r15_n_20,reg_r15_n_21,reg_r15_n_22,reg_r15_n_23,reg_r15_n_24,reg_r15_n_25,reg_r15_n_26,reg_r15_n_27,reg_r15_n_28,reg_r15_n_29,reg_r15_n_30,reg_r15_n_31}),
        .SR(RESET_IBUF));
  REGrwe_n_0 reg_addr_r2
       (.ADDR_R2_IBUF(ADDR_R2_IBUF),
        .ADDR_R2_in(ADDR_R2_in),
        .CLK(CLK_IBUF_BUFG),
        .D({reg_addr_r2_n_4,reg_addr_r2_n_5,reg_addr_r2_n_6,reg_addr_r2_n_7,reg_addr_r2_n_8,reg_addr_r2_n_9,reg_addr_r2_n_10,reg_addr_r2_n_11,reg_addr_r2_n_12,reg_addr_r2_n_13,reg_addr_r2_n_14,reg_addr_r2_n_15,reg_addr_r2_n_16,reg_addr_r2_n_17,reg_addr_r2_n_18,reg_addr_r2_n_19,reg_addr_r2_n_20,reg_addr_r2_n_21,reg_addr_r2_n_22,reg_addr_r2_n_23,reg_addr_r2_n_24,reg_addr_r2_n_25,reg_addr_r2_n_26,reg_addr_r2_n_27,reg_addr_r2_n_28,reg_addr_r2_n_29,reg_addr_r2_n_30,reg_addr_r2_n_31,reg_addr_r2_n_32,reg_addr_r2_n_33,reg_addr_r2_n_34,reg_addr_r2_n_35}),
        .DATA_OUT2__0(DATA_OUT2__0),
        .E(WE_IBUF),
        .Q({reg_r15_n_0,reg_r15_n_1,reg_r15_n_2,reg_r15_n_3,reg_r15_n_4,reg_r15_n_5,reg_r15_n_6,reg_r15_n_7,reg_r15_n_8,reg_r15_n_9,reg_r15_n_10,reg_r15_n_11,reg_r15_n_12,reg_r15_n_13,reg_r15_n_14,reg_r15_n_15,reg_r15_n_16,reg_r15_n_17,reg_r15_n_18,reg_r15_n_19,reg_r15_n_20,reg_r15_n_21,reg_r15_n_22,reg_r15_n_23,reg_r15_n_24,reg_r15_n_25,reg_r15_n_26,reg_r15_n_27,reg_r15_n_28,reg_r15_n_29,reg_r15_n_30,reg_r15_n_31}),
        .SR(RESET_IBUF));
  REGrwe_n_1 reg_addr_w
       (.CLK(CLK_IBUF_BUFG),
        .D(ADDR_W_IBUF),
        .E(WE_IBUF),
        .Q(Dout),
        .SR(RESET_IBUF));
  REGrwe_n__parameterized2 reg_data_in
       (.CLK(CLK_IBUF_BUFG),
        .D(DATA_IN_IBUF),
        .E(WE_IBUF),
        .Q({reg_data_in_n_0,reg_data_in_n_1,reg_data_in_n_2,reg_data_in_n_3,reg_data_in_n_4,reg_data_in_n_5,reg_data_in_n_6,reg_data_in_n_7,reg_data_in_n_8,reg_data_in_n_9,reg_data_in_n_10,reg_data_in_n_11,reg_data_in_n_12,reg_data_in_n_13,reg_data_in_n_14,reg_data_in_n_15,reg_data_in_n_16,reg_data_in_n_17,reg_data_in_n_18,reg_data_in_n_19,reg_data_in_n_20,reg_data_in_n_21,reg_data_in_n_22,reg_data_in_n_23,reg_data_in_n_24,reg_data_in_n_25,reg_data_in_n_26,reg_data_in_n_27,reg_data_in_n_28,reg_data_in_n_29,reg_data_in_n_30,reg_data_in_n_31}),
        .SR(RESET_IBUF));
  REGrwe_n__parameterized2_2 reg_data_out1
       (.CLK(CLK_IBUF_BUFG),
        .D(Din),
        .E(WE_IBUF),
        .Q(DATA_OUT1_OBUF),
        .SR(RESET_IBUF));
  REGrwe_n__parameterized2_3 reg_data_out2
       (.CLK(CLK_IBUF_BUFG),
        .D({reg_addr_r2_n_4,reg_addr_r2_n_5,reg_addr_r2_n_6,reg_addr_r2_n_7,reg_addr_r2_n_8,reg_addr_r2_n_9,reg_addr_r2_n_10,reg_addr_r2_n_11,reg_addr_r2_n_12,reg_addr_r2_n_13,reg_addr_r2_n_14,reg_addr_r2_n_15,reg_addr_r2_n_16,reg_addr_r2_n_17,reg_addr_r2_n_18,reg_addr_r2_n_19,reg_addr_r2_n_20,reg_addr_r2_n_21,reg_addr_r2_n_22,reg_addr_r2_n_23,reg_addr_r2_n_24,reg_addr_r2_n_25,reg_addr_r2_n_26,reg_addr_r2_n_27,reg_addr_r2_n_28,reg_addr_r2_n_29,reg_addr_r2_n_30,reg_addr_r2_n_31,reg_addr_r2_n_32,reg_addr_r2_n_33,reg_addr_r2_n_34,reg_addr_r2_n_35}),
        .Q(DATA_OUT2_OBUF),
        .SR(RESET_IBUF),
        .WE(WE_IBUF));
  REGrwe_n__parameterized2_4 reg_r15
       (.CLK(CLK_IBUF_BUFG),
        .D(R15_IBUF),
        .Q({reg_r15_n_0,reg_r15_n_1,reg_r15_n_2,reg_r15_n_3,reg_r15_n_4,reg_r15_n_5,reg_r15_n_6,reg_r15_n_7,reg_r15_n_8,reg_r15_n_9,reg_r15_n_10,reg_r15_n_11,reg_r15_n_12,reg_r15_n_13,reg_r15_n_14,reg_r15_n_15,reg_r15_n_16,reg_r15_n_17,reg_r15_n_18,reg_r15_n_19,reg_r15_n_20,reg_r15_n_21,reg_r15_n_22,reg_r15_n_23,reg_r15_n_24,reg_r15_n_25,reg_r15_n_26,reg_r15_n_27,reg_r15_n_28,reg_r15_n_29,reg_r15_n_30,reg_r15_n_31}),
        .RESET(RESET_IBUF),
        .WE(WE_IBUF));
  REGISTER_FILE_wR15 rf
       (.ADDR_R1_in(ADDR_R1_in),
        .ADDR_R2_in(ADDR_R2_in),
        .CLK(CLK_IBUF_BUFG),
        .DATA_OUT1__0(DATA_OUT1__0),
        .DATA_OUT2__0(DATA_OUT2__0),
        .\Dout_reg[1] (Dout),
        .Q({reg_data_in_n_0,reg_data_in_n_1,reg_data_in_n_2,reg_data_in_n_3,reg_data_in_n_4,reg_data_in_n_5,reg_data_in_n_6,reg_data_in_n_7,reg_data_in_n_8,reg_data_in_n_9,reg_data_in_n_10,reg_data_in_n_11,reg_data_in_n_12,reg_data_in_n_13,reg_data_in_n_14,reg_data_in_n_15,reg_data_in_n_16,reg_data_in_n_17,reg_data_in_n_18,reg_data_in_n_19,reg_data_in_n_20,reg_data_in_n_21,reg_data_in_n_22,reg_data_in_n_23,reg_data_in_n_24,reg_data_in_n_25,reg_data_in_n_26,reg_data_in_n_27,reg_data_in_n_28,reg_data_in_n_29,reg_data_in_n_30,reg_data_in_n_31}),
        .REG_WRITE_in(REG_WRITE_in));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
