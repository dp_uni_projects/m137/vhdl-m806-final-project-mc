// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Oct 12 18:34:36 2020
// Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/mpliax/Documents/workspace/vhdl-m806-final-project-mc/vhdl-m806-final-project.sim/sim_1/impl/timing/xsim/CONTROL_UNIT_TB_time_impl.v
// Design      : CONTROL_UNIT
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

(* ECO_CHECKSUM = "ca695505" *) 
(* NotValidForBitStream *)
module CONTROL_UNIT
   (INSTR,
    FLAGS,
    CLK,
    RESET,
    PcSrc,
    MemToReg,
    MemWrite,
    ALUControl,
    ALUSrc,
    ImmSrc,
    RegWrite,
    RegSrc,
    FlagsWrite,
    PCWrite,
    IRWrite,
    MAWrite);
  input [31:0]INSTR;
  input [3:0]FLAGS;
  input CLK;
  input RESET;
  output [1:0]PcSrc;
  output MemToReg;
  output MemWrite;
  output [3:0]ALUControl;
  output ALUSrc;
  output ImmSrc;
  output RegWrite;
  output [2:0]RegSrc;
  output FlagsWrite;
  output PCWrite;
  output IRWrite;
  output MAWrite;

  wire [3:0]ALUControl;
  wire [3:0]ALUControl_OBUF;
  wire \ALUControl_OBUF[0]_inst_i_2_n_0 ;
  wire \ALUControl_OBUF[1]_inst_i_2_n_0 ;
  wire ALUSrc;
  wire ALUSrc_OBUF;
  wire CLK;
  wire CLK_IBUF;
  wire CLK_IBUF_BUFG;
  wire [3:0]FLAGS;
  wire [3:0]FLAGS_IBUF;
  wire FSM_UNIT_n_0;
  wire FSM_UNIT_n_1;
  wire FSM_UNIT_n_2;
  wire FlagsWrite;
  wire FlagsWrite_OBUF;
  wire [31:0]INSTR;
  wire [31:5]INSTR_IBUF;
  wire IRWrite;
  wire ImmSrc;
  wire MAWrite;
  wire MemToReg;
  wire MemWrite;
  wire PCWrite;
  wire PCWrite_OBUF;
  wire [1:0]PcSrc;
  wire [1:0]PcSrc_OBUF;
  wire RESET;
  wire RESET_IBUF;
  wire [2:0]RegSrc;
  wire [1:0]RegSrc_OBUF;
  wire RegWrite;
  wire RegWrite_OBUF;

initial begin
 $sdf_annotate("CONTROL_UNIT_TB_time_impl.sdf",,,,"tool_control");
end
  (* IOB = "TRUE" *) 
  OBUF \ALUControl_OBUF[0]_inst 
       (.I(ALUControl_OBUF[0]),
        .O(ALUControl[0]));
  LUT4 #(
    .INIT(16'h0053)) 
    \ALUControl_OBUF[0]_inst_i_1 
       (.I0(INSTR_IBUF[23]),
        .I1(\ALUControl_OBUF[0]_inst_i_2_n_0 ),
        .I2(RegSrc_OBUF[1]),
        .I3(RegSrc_OBUF[0]),
        .O(ALUControl_OBUF[0]));
  LUT5 #(
    .INIT(32'h0000DF0F)) 
    \ALUControl_OBUF[0]_inst_i_2 
       (.I0(INSTR_IBUF[5]),
        .I1(INSTR_IBUF[25]),
        .I2(INSTR_IBUF[24]),
        .I3(INSTR_IBUF[21]),
        .I4(INSTR_IBUF[22]),
        .O(\ALUControl_OBUF[0]_inst_i_2_n_0 ));
  (* IOB = "TRUE" *) 
  OBUF \ALUControl_OBUF[1]_inst 
       (.I(ALUControl_OBUF[1]),
        .O(ALUControl[1]));
  LUT4 #(
    .INIT(16'h0002)) 
    \ALUControl_OBUF[1]_inst_i_1 
       (.I0(\ALUControl_OBUF[1]_inst_i_2_n_0 ),
        .I1(RegSrc_OBUF[1]),
        .I2(RegSrc_OBUF[0]),
        .I3(INSTR_IBUF[22]),
        .O(ALUControl_OBUF[1]));
  LUT5 #(
    .INIT(32'h4FF04FFF)) 
    \ALUControl_OBUF[1]_inst_i_2 
       (.I0(INSTR_IBUF[25]),
        .I1(INSTR_IBUF[6]),
        .I2(INSTR_IBUF[21]),
        .I3(INSTR_IBUF[24]),
        .I4(INSTR_IBUF[23]),
        .O(\ALUControl_OBUF[1]_inst_i_2_n_0 ));
  (* IOB = "TRUE" *) 
  OBUF \ALUControl_OBUF[2]_inst 
       (.I(ALUControl_OBUF[2]),
        .O(ALUControl[2]));
  LUT6 #(
    .INIT(64'h000000FB00000000)) 
    \ALUControl_OBUF[2]_inst_i_1 
       (.I0(INSTR_IBUF[25]),
        .I1(INSTR_IBUF[24]),
        .I2(INSTR_IBUF[22]),
        .I3(RegSrc_OBUF[1]),
        .I4(RegSrc_OBUF[0]),
        .I5(INSTR_IBUF[21]),
        .O(ALUControl_OBUF[2]));
  (* IOB = "TRUE" *) 
  OBUF \ALUControl_OBUF[3]_inst 
       (.I(ALUControl_OBUF[3]),
        .O(ALUControl[3]));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    \ALUControl_OBUF[3]_inst_i_1 
       (.I0(INSTR_IBUF[25]),
        .I1(INSTR_IBUF[24]),
        .I2(INSTR_IBUF[22]),
        .I3(RegSrc_OBUF[1]),
        .I4(RegSrc_OBUF[0]),
        .I5(INSTR_IBUF[21]),
        .O(ALUControl_OBUF[3]));
  (* IOB = "TRUE" *) 
  OBUF ALUSrc_OBUF_inst
       (.I(ALUSrc_OBUF),
        .O(ALUSrc));
  LUT3 #(
    .INIT(8'hFE)) 
    ALUSrc_OBUF_inst_i_1
       (.I0(INSTR_IBUF[25]),
        .I1(RegSrc_OBUF[0]),
        .I2(RegSrc_OBUF[1]),
        .O(ALUSrc_OBUF));
  BUFG CLK_IBUF_BUFG_inst
       (.I(CLK_IBUF),
        .O(CLK_IBUF_BUFG));
  IBUF CLK_IBUF_inst
       (.I(CLK),
        .O(CLK_IBUF));
  IBUF \FLAGS_IBUF[0]_inst 
       (.I(FLAGS[0]),
        .O(FLAGS_IBUF[0]));
  IBUF \FLAGS_IBUF[1]_inst 
       (.I(FLAGS[1]),
        .O(FLAGS_IBUF[1]));
  IBUF \FLAGS_IBUF[2]_inst 
       (.I(FLAGS[2]),
        .O(FLAGS_IBUF[2]));
  IBUF \FLAGS_IBUF[3]_inst 
       (.I(FLAGS[3]),
        .O(FLAGS_IBUF[3]));
  FSM FSM_UNIT
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .FLAGS_IBUF(FLAGS_IBUF),
        .FlagsWrite_OBUF(FlagsWrite_OBUF),
        .INSTR_IBUF({INSTR_IBUF[31:28],INSTR_IBUF[24:23],INSTR_IBUF[20],INSTR_IBUF[15:12]}),
        .PCWrite_OBUF(PCWrite_OBUF),
        .PcSrc_OBUF(PcSrc_OBUF),
        .Q({FSM_UNIT_n_0,FSM_UNIT_n_1,FSM_UNIT_n_2}),
        .RESET_IBUF(RESET_IBUF),
        .RegSrc_OBUF(RegSrc_OBUF),
        .RegWrite_OBUF(RegWrite_OBUF));
  (* IOB = "TRUE" *) 
  OBUF FlagsWrite_OBUF_inst
       (.I(FlagsWrite_OBUF),
        .O(FlagsWrite));
  IBUF \INSTR_IBUF[12]_inst 
       (.I(INSTR[12]),
        .O(INSTR_IBUF[12]));
  IBUF \INSTR_IBUF[13]_inst 
       (.I(INSTR[13]),
        .O(INSTR_IBUF[13]));
  IBUF \INSTR_IBUF[14]_inst 
       (.I(INSTR[14]),
        .O(INSTR_IBUF[14]));
  IBUF \INSTR_IBUF[15]_inst 
       (.I(INSTR[15]),
        .O(INSTR_IBUF[15]));
  IBUF \INSTR_IBUF[20]_inst 
       (.I(INSTR[20]),
        .O(INSTR_IBUF[20]));
  IBUF \INSTR_IBUF[21]_inst 
       (.I(INSTR[21]),
        .O(INSTR_IBUF[21]));
  IBUF \INSTR_IBUF[22]_inst 
       (.I(INSTR[22]),
        .O(INSTR_IBUF[22]));
  IBUF \INSTR_IBUF[23]_inst 
       (.I(INSTR[23]),
        .O(INSTR_IBUF[23]));
  IBUF \INSTR_IBUF[24]_inst 
       (.I(INSTR[24]),
        .O(INSTR_IBUF[24]));
  IBUF \INSTR_IBUF[25]_inst 
       (.I(INSTR[25]),
        .O(INSTR_IBUF[25]));
  IBUF \INSTR_IBUF[26]_inst 
       (.I(INSTR[26]),
        .O(RegSrc_OBUF[1]));
  IBUF \INSTR_IBUF[27]_inst 
       (.I(INSTR[27]),
        .O(RegSrc_OBUF[0]));
  IBUF \INSTR_IBUF[28]_inst 
       (.I(INSTR[28]),
        .O(INSTR_IBUF[28]));
  IBUF \INSTR_IBUF[29]_inst 
       (.I(INSTR[29]),
        .O(INSTR_IBUF[29]));
  IBUF \INSTR_IBUF[30]_inst 
       (.I(INSTR[30]),
        .O(INSTR_IBUF[30]));
  IBUF \INSTR_IBUF[31]_inst 
       (.I(INSTR[31]),
        .O(INSTR_IBUF[31]));
  IBUF \INSTR_IBUF[5]_inst 
       (.I(INSTR[5]),
        .O(INSTR_IBUF[5]));
  IBUF \INSTR_IBUF[6]_inst 
       (.I(INSTR[6]),
        .O(INSTR_IBUF[6]));
  (* IOB = "TRUE" *) 
  OBUF IRWrite_OBUF_inst
       (.I(FSM_UNIT_n_2),
        .O(IRWrite));
  (* IOB = "TRUE" *) 
  OBUF ImmSrc_OBUF_inst
       (.I(RegSrc_OBUF[0]),
        .O(ImmSrc));
  (* IOB = "TRUE" *) 
  OBUF MAWrite_OBUF_inst
       (.I(FSM_UNIT_n_1),
        .O(MAWrite));
  (* IOB = "TRUE" *) 
  OBUF MemToReg_OBUF_inst
       (.I(RegSrc_OBUF[1]),
        .O(MemToReg));
  (* IOB = "TRUE" *) 
  OBUF MemWrite_OBUF_inst
       (.I(FSM_UNIT_n_0),
        .O(MemWrite));
  (* IOB = "TRUE" *) 
  OBUF PCWrite_OBUF_inst
       (.I(PCWrite_OBUF),
        .O(PCWrite));
  (* IOB = "TRUE" *) 
  OBUF \PcSrc_OBUF[0]_inst 
       (.I(PcSrc_OBUF[0]),
        .O(PcSrc[0]));
  (* IOB = "TRUE" *) 
  OBUF \PcSrc_OBUF[1]_inst 
       (.I(PcSrc_OBUF[1]),
        .O(PcSrc[1]));
  IBUF RESET_IBUF_inst
       (.I(RESET),
        .O(RESET_IBUF));
  (* IOB = "TRUE" *) 
  OBUF \RegSrc_OBUF[0]_inst 
       (.I(RegSrc_OBUF[0]),
        .O(RegSrc[0]));
  (* IOB = "TRUE" *) 
  OBUF \RegSrc_OBUF[1]_inst 
       (.I(RegSrc_OBUF[1]),
        .O(RegSrc[1]));
  (* IOB = "TRUE" *) 
  OBUF \RegSrc_OBUF[2]_inst 
       (.I(RegSrc_OBUF[0]),
        .O(RegSrc[2]));
  (* IOB = "TRUE" *) 
  OBUF RegWrite_OBUF_inst
       (.I(RegWrite_OBUF),
        .O(RegWrite));
endmodule

module FSM
   (Q,
    PcSrc_OBUF,
    FlagsWrite_OBUF,
    RegWrite_OBUF,
    PCWrite_OBUF,
    RegSrc_OBUF,
    INSTR_IBUF,
    FLAGS_IBUF,
    RESET_IBUF,
    CLK_IBUF_BUFG);
  output [2:0]Q;
  output [1:0]PcSrc_OBUF;
  output FlagsWrite_OBUF;
  output RegWrite_OBUF;
  output PCWrite_OBUF;
  input [1:0]RegSrc_OBUF;
  input [10:0]INSTR_IBUF;
  input [3:0]FLAGS_IBUF;
  input RESET_IBUF;
  input CLK_IBUF_BUFG;

  wire CLK_IBUF_BUFG;
  wire [3:0]FLAGS_IBUF;
  wire \FSM_onehot_current_state[0]_i_1_n_0 ;
  wire \FSM_onehot_current_state[0]_i_2_n_0 ;
  wire \FSM_onehot_current_state[10]_i_1_n_0 ;
  wire \FSM_onehot_current_state[10]_i_2_n_0 ;
  wire \FSM_onehot_current_state[11]_i_1_n_0 ;
  wire \FSM_onehot_current_state[11]_i_2_n_0 ;
  wire \FSM_onehot_current_state[12]_i_1_n_0 ;
  wire \FSM_onehot_current_state[13]_i_1_n_0 ;
  wire \FSM_onehot_current_state[13]_i_5_n_0 ;
  wire \FSM_onehot_current_state[13]_i_6_n_0 ;
  wire \FSM_onehot_current_state[13]_i_7_n_0 ;
  wire \FSM_onehot_current_state[13]_i_8_n_0 ;
  wire \FSM_onehot_current_state[2]_i_1_n_0 ;
  wire \FSM_onehot_current_state[3]_i_1_n_0 ;
  wire \FSM_onehot_current_state[4]_i_1_n_0 ;
  wire \FSM_onehot_current_state[5]_i_1_n_0 ;
  wire \FSM_onehot_current_state[6]_i_1_n_0 ;
  wire \FSM_onehot_current_state[7]_i_1_n_0 ;
  wire \FSM_onehot_current_state[8]_i_1_n_0 ;
  wire \FSM_onehot_current_state[8]_i_2_n_0 ;
  wire \FSM_onehot_current_state[9]_i_1_n_0 ;
  wire \FSM_onehot_current_state_reg[13]_i_2_n_0 ;
  wire \FSM_onehot_current_state_reg[13]_i_3_n_0 ;
  wire \FSM_onehot_current_state_reg[13]_i_4_n_0 ;
  wire \FSM_onehot_current_state_reg_n_0_[10] ;
  wire \FSM_onehot_current_state_reg_n_0_[11] ;
  wire \FSM_onehot_current_state_reg_n_0_[12] ;
  wire \FSM_onehot_current_state_reg_n_0_[13] ;
  wire \FSM_onehot_current_state_reg_n_0_[1] ;
  wire \FSM_onehot_current_state_reg_n_0_[2] ;
  wire \FSM_onehot_current_state_reg_n_0_[4] ;
  wire \FSM_onehot_current_state_reg_n_0_[6] ;
  wire \FSM_onehot_current_state_reg_n_0_[7] ;
  wire \FSM_onehot_current_state_reg_n_0_[8] ;
  wire \FSM_onehot_current_state_reg_n_0_[9] ;
  wire FlagsWrite_OBUF;
  wire [10:0]INSTR_IBUF;
  wire IRWrite_OBUF;
  wire MAWrite_OBUF;
  wire MemWrite_OBUF;
  wire PCWrite_OBUF;
  wire PCWrite_OBUF_inst_i_2_n_0;
  wire [1:0]PcSrc_OBUF;
  wire [2:0]Q;
  wire RESET_IBUF;
  wire [1:0]RegSrc_OBUF;
  wire RegWrite_OBUF;

  LUT6 #(
    .INIT(64'h00000000C000AAAA)) 
    \FSM_onehot_current_state[0]_i_1 
       (.I0(\FSM_onehot_current_state[0]_i_2_n_0 ),
        .I1(\FSM_onehot_current_state_reg[13]_i_2_n_0 ),
        .I2(RegSrc_OBUF[0]),
        .I3(RegSrc_OBUF[1]),
        .I4(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I5(IRWrite_OBUF),
        .O(\FSM_onehot_current_state[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \FSM_onehot_current_state[0]_i_2 
       (.I0(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I1(MAWrite_OBUF),
        .I2(\FSM_onehot_current_state_reg_n_0_[4] ),
        .O(\FSM_onehot_current_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \FSM_onehot_current_state[10]_i_1 
       (.I0(INSTR_IBUF[4]),
        .I1(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I2(MAWrite_OBUF),
        .I3(IRWrite_OBUF),
        .I4(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I5(\FSM_onehot_current_state[10]_i_2_n_0 ),
        .O(\FSM_onehot_current_state[10]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \FSM_onehot_current_state[10]_i_2 
       (.I0(INSTR_IBUF[3]),
        .I1(INSTR_IBUF[0]),
        .I2(INSTR_IBUF[2]),
        .I3(INSTR_IBUF[1]),
        .O(\FSM_onehot_current_state[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \FSM_onehot_current_state[11]_i_1 
       (.I0(INSTR_IBUF[5]),
        .I1(INSTR_IBUF[6]),
        .I2(\FSM_onehot_current_state[11]_i_2_n_0 ),
        .I3(\FSM_onehot_current_state_reg[13]_i_2_n_0 ),
        .I4(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I5(IRWrite_OBUF),
        .O(\FSM_onehot_current_state[11]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_onehot_current_state[11]_i_2 
       (.I0(RegSrc_OBUF[1]),
        .I1(RegSrc_OBUF[0]),
        .O(\FSM_onehot_current_state[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    \FSM_onehot_current_state[12]_i_1 
       (.I0(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I1(\FSM_onehot_current_state_reg[13]_i_2_n_0 ),
        .I2(IRWrite_OBUF),
        .I3(RegSrc_OBUF[0]),
        .I4(RegSrc_OBUF[1]),
        .I5(INSTR_IBUF[6]),
        .O(\FSM_onehot_current_state[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000800000)) 
    \FSM_onehot_current_state[13]_i_1 
       (.I0(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I1(\FSM_onehot_current_state_reg[13]_i_2_n_0 ),
        .I2(INSTR_IBUF[6]),
        .I3(IRWrite_OBUF),
        .I4(RegSrc_OBUF[0]),
        .I5(RegSrc_OBUF[1]),
        .O(\FSM_onehot_current_state[13]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h47B8)) 
    \FSM_onehot_current_state[13]_i_5 
       (.I0(FLAGS_IBUF[2]),
        .I1(INSTR_IBUF[8]),
        .I2(FLAGS_IBUF[1]),
        .I3(INSTR_IBUF[7]),
        .O(\FSM_onehot_current_state[13]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h47B8)) 
    \FSM_onehot_current_state[13]_i_6 
       (.I0(FLAGS_IBUF[3]),
        .I1(INSTR_IBUF[8]),
        .I2(FLAGS_IBUF[0]),
        .I3(INSTR_IBUF[7]),
        .O(\FSM_onehot_current_state[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h55A6AAA6AAA655A6)) 
    \FSM_onehot_current_state[13]_i_7 
       (.I0(INSTR_IBUF[7]),
        .I1(FLAGS_IBUF[2]),
        .I2(FLAGS_IBUF[1]),
        .I3(INSTR_IBUF[8]),
        .I4(FLAGS_IBUF[3]),
        .I5(FLAGS_IBUF[0]),
        .O(\FSM_onehot_current_state[13]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hEFFEBAAB)) 
    \FSM_onehot_current_state[13]_i_8 
       (.I0(INSTR_IBUF[8]),
        .I1(FLAGS_IBUF[1]),
        .I2(FLAGS_IBUF[3]),
        .I3(FLAGS_IBUF[0]),
        .I4(INSTR_IBUF[7]),
        .O(\FSM_onehot_current_state[13]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \FSM_onehot_current_state[2]_i_1 
       (.I0(IRWrite_OBUF),
        .I1(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I2(\FSM_onehot_current_state_reg[13]_i_2_n_0 ),
        .O(\FSM_onehot_current_state[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00004000)) 
    \FSM_onehot_current_state[3]_i_1 
       (.I0(RegSrc_OBUF[0]),
        .I1(RegSrc_OBUF[1]),
        .I2(\FSM_onehot_current_state_reg[13]_i_2_n_0 ),
        .I3(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I4(IRWrite_OBUF),
        .O(\FSM_onehot_current_state[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \FSM_onehot_current_state[4]_i_1 
       (.I0(INSTR_IBUF[4]),
        .I1(MAWrite_OBUF),
        .I2(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I3(IRWrite_OBUF),
        .O(\FSM_onehot_current_state[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \FSM_onehot_current_state[5]_i_1 
       (.I0(INSTR_IBUF[4]),
        .I1(MAWrite_OBUF),
        .I2(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I3(IRWrite_OBUF),
        .O(\FSM_onehot_current_state[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000000B0000000)) 
    \FSM_onehot_current_state[6]_i_1 
       (.I0(INSTR_IBUF[5]),
        .I1(INSTR_IBUF[6]),
        .I2(\FSM_onehot_current_state[11]_i_2_n_0 ),
        .I3(\FSM_onehot_current_state_reg[13]_i_2_n_0 ),
        .I4(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I5(IRWrite_OBUF),
        .O(\FSM_onehot_current_state[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00000100)) 
    \FSM_onehot_current_state[7]_i_1 
       (.I0(MAWrite_OBUF),
        .I1(IRWrite_OBUF),
        .I2(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I3(\FSM_onehot_current_state[10]_i_2_n_0 ),
        .I4(\FSM_onehot_current_state[8]_i_2_n_0 ),
        .O(\FSM_onehot_current_state[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \FSM_onehot_current_state[8]_i_1 
       (.I0(MAWrite_OBUF),
        .I1(IRWrite_OBUF),
        .I2(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I3(\FSM_onehot_current_state[10]_i_2_n_0 ),
        .I4(\FSM_onehot_current_state[8]_i_2_n_0 ),
        .O(\FSM_onehot_current_state[8]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hA3)) 
    \FSM_onehot_current_state[8]_i_2 
       (.I0(INSTR_IBUF[4]),
        .I1(\FSM_onehot_current_state_reg_n_0_[4] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[6] ),
        .O(\FSM_onehot_current_state[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \FSM_onehot_current_state[9]_i_1 
       (.I0(INSTR_IBUF[4]),
        .I1(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I2(MAWrite_OBUF),
        .I3(IRWrite_OBUF),
        .I4(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I5(\FSM_onehot_current_state[10]_i_2_n_0 ),
        .O(\FSM_onehot_current_state[9]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "FSM_onehot_current_state_reg[0]" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_current_state_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[0]_i_1_n_0 ),
        .Q(Q[0]),
        .S(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  (* ORIG_CELL_NAME = "FSM_onehot_current_state_reg[0]" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_current_state_reg[0]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[0]_i_1_n_0 ),
        .Q(IRWrite_OBUF),
        .S(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[10]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[10] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[11]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[11] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[12]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[12] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[13]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[13] ),
        .R(RESET_IBUF));
  MUXF8 \FSM_onehot_current_state_reg[13]_i_2 
       (.I0(\FSM_onehot_current_state_reg[13]_i_3_n_0 ),
        .I1(\FSM_onehot_current_state_reg[13]_i_4_n_0 ),
        .O(\FSM_onehot_current_state_reg[13]_i_2_n_0 ),
        .S(INSTR_IBUF[10]));
  MUXF7 \FSM_onehot_current_state_reg[13]_i_3 
       (.I0(\FSM_onehot_current_state[13]_i_5_n_0 ),
        .I1(\FSM_onehot_current_state[13]_i_6_n_0 ),
        .O(\FSM_onehot_current_state_reg[13]_i_3_n_0 ),
        .S(INSTR_IBUF[9]));
  MUXF7 \FSM_onehot_current_state_reg[13]_i_4 
       (.I0(\FSM_onehot_current_state[13]_i_7_n_0 ),
        .I1(\FSM_onehot_current_state[13]_i_8_n_0 ),
        .O(\FSM_onehot_current_state_reg[13]_i_4_n_0 ),
        .S(INSTR_IBUF[9]));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(IRWrite_OBUF),
        .Q(\FSM_onehot_current_state_reg_n_0_[1] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[2]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[2] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "FSM_onehot_current_state_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[3]_i_1_n_0 ),
        .Q(Q[1]),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  (* ORIG_CELL_NAME = "FSM_onehot_current_state_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[3]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[3]_i_1_n_0 ),
        .Q(MAWrite_OBUF),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[4]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[4] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "FSM_onehot_current_state_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[5]_i_1_n_0 ),
        .Q(Q[2]),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  (* ORIG_CELL_NAME = "FSM_onehot_current_state_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[5]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[5]_i_1_n_0 ),
        .Q(MemWrite_OBUF),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[6]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[6] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[7]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[7] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[8]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[8] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[9]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[9] ),
        .R(RESET_IBUF));
  LUT3 #(
    .INIT(8'hFE)) 
    FlagsWrite_OBUF_inst_i_1
       (.I0(\FSM_onehot_current_state_reg_n_0_[10] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[9] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[11] ),
        .O(FlagsWrite_OBUF));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    PCWrite_OBUF_inst_i_1
       (.I0(\FSM_onehot_current_state_reg_n_0_[12] ),
        .I1(PCWrite_OBUF_inst_i_2_n_0),
        .I2(\FSM_onehot_current_state_reg_n_0_[11] ),
        .I3(\FSM_onehot_current_state_reg_n_0_[13] ),
        .I4(\FSM_onehot_current_state_reg_n_0_[9] ),
        .I5(\FSM_onehot_current_state_reg_n_0_[10] ),
        .O(PCWrite_OBUF));
  LUT4 #(
    .INIT(16'hFFFE)) 
    PCWrite_OBUF_inst_i_2
       (.I0(\FSM_onehot_current_state_reg_n_0_[7] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[8] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[2] ),
        .I3(MemWrite_OBUF),
        .O(PCWrite_OBUF_inst_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    \PcSrc_OBUF[0]_inst_i_1 
       (.I0(\FSM_onehot_current_state_reg_n_0_[12] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[13] ),
        .O(PcSrc_OBUF[0]));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \PcSrc_OBUF[1]_inst_i_1 
       (.I0(\FSM_onehot_current_state_reg_n_0_[13] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[12] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[7] ),
        .I3(\FSM_onehot_current_state_reg_n_0_[9] ),
        .O(PcSrc_OBUF[1]));
  LUT3 #(
    .INIT(8'hFE)) 
    RegWrite_OBUF_inst_i_1
       (.I0(\FSM_onehot_current_state_reg_n_0_[10] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[8] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[13] ),
        .O(RegWrite_OBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
