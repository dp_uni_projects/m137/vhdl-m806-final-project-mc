// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Oct 12 22:55:10 2020
// Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/mpliax/Documents/workspace/vhdl-m806-final-project-mc/vhdl-m806-final-project.sim/sim_1/impl/timing/xsim/PROCESSOR_TB_time_impl.v
// Design      : ARM_PROCESSOR_n
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module RAM32M_UNIQ_BASE_
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD1
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD10
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD11
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD2
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD3
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD4
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD5
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD6
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD7
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD8
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD9
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module ADDER_n
   (PCPlus8,
    \Dout_reg[31] ,
    Q,
    \Dout_reg[3] ,
    DATA_OUT2);
  output [28:0]PCPlus8;
  output [28:0]\Dout_reg[31] ;
  input [29:0]Q;
  input \Dout_reg[3] ;
  input [28:0]DATA_OUT2;

  wire [28:0]DATA_OUT2;
  wire [28:0]\Dout_reg[31] ;
  wire \Dout_reg[3] ;
  wire [28:0]PCPlus8;
  wire [29:0]Q;
  wire S0_carry__0_n_0;
  wire S0_carry__1_n_0;
  wire S0_carry__2_n_0;
  wire S0_carry__3_n_0;
  wire S0_carry__4_n_0;
  wire S0_carry__5_n_0;
  wire S0_carry_n_0;
  wire [2:0]NLW_S0_carry_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__4_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__5_CO_UNCONNECTED;
  wire [3:0]NLW_S0_carry__6_CO_UNCONNECTED;
  wire [3:1]NLW_S0_carry__6_O_UNCONNECTED;

  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry
       (.CI(1'b0),
        .CO({S0_carry_n_0,NLW_S0_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(Q[0]),
        .DI(Q[4:1]),
        .O(PCPlus8[3:0]),
        .S(Q[4:1]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__0
       (.CI(S0_carry_n_0),
        .CO({S0_carry__0_n_0,NLW_S0_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[8:5]),
        .O(PCPlus8[7:4]),
        .S(Q[8:5]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__1
       (.CI(S0_carry__0_n_0),
        .CO({S0_carry__1_n_0,NLW_S0_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[12:9]),
        .O(PCPlus8[11:8]),
        .S(Q[12:9]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__2
       (.CI(S0_carry__1_n_0),
        .CO({S0_carry__2_n_0,NLW_S0_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[16:13]),
        .O(PCPlus8[15:12]),
        .S(Q[16:13]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__3
       (.CI(S0_carry__2_n_0),
        .CO({S0_carry__3_n_0,NLW_S0_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[20:17]),
        .O(PCPlus8[19:16]),
        .S(Q[20:17]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__4
       (.CI(S0_carry__3_n_0),
        .CO({S0_carry__4_n_0,NLW_S0_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[24:21]),
        .O(PCPlus8[23:20]),
        .S(Q[24:21]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__5
       (.CI(S0_carry__4_n_0),
        .CO({S0_carry__5_n_0,NLW_S0_carry__5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[28:25]),
        .O(PCPlus8[27:24]),
        .S(Q[28:25]));
  CARRY4 S0_carry__6
       (.CI(S0_carry__5_n_0),
        .CO(NLW_S0_carry__6_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_S0_carry__6_O_UNCONNECTED[3:1],PCPlus8[28]}),
        .S({1'b0,1'b0,1'b0,Q[29]}));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[10]_inst_i_1 
       (.I0(PCPlus8[7]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[7]),
        .O(\Dout_reg[31] [7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[11]_inst_i_1 
       (.I0(PCPlus8[8]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[8]),
        .O(\Dout_reg[31] [8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[12]_inst_i_1 
       (.I0(PCPlus8[9]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[9]),
        .O(\Dout_reg[31] [9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[13]_inst_i_1 
       (.I0(PCPlus8[10]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[10]),
        .O(\Dout_reg[31] [10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[14]_inst_i_1 
       (.I0(PCPlus8[11]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[11]),
        .O(\Dout_reg[31] [11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[15]_inst_i_1 
       (.I0(PCPlus8[12]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[12]),
        .O(\Dout_reg[31] [12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[16]_inst_i_1 
       (.I0(PCPlus8[13]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[13]),
        .O(\Dout_reg[31] [13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[17]_inst_i_1 
       (.I0(PCPlus8[14]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[14]),
        .O(\Dout_reg[31] [14]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[18]_inst_i_1 
       (.I0(PCPlus8[15]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[15]),
        .O(\Dout_reg[31] [15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[19]_inst_i_1 
       (.I0(PCPlus8[16]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[16]),
        .O(\Dout_reg[31] [16]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[20]_inst_i_1 
       (.I0(PCPlus8[17]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[17]),
        .O(\Dout_reg[31] [17]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[21]_inst_i_1 
       (.I0(PCPlus8[18]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[18]),
        .O(\Dout_reg[31] [18]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[22]_inst_i_1 
       (.I0(PCPlus8[19]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[19]),
        .O(\Dout_reg[31] [19]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[23]_inst_i_1 
       (.I0(PCPlus8[20]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[20]),
        .O(\Dout_reg[31] [20]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[24]_inst_i_1 
       (.I0(PCPlus8[21]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[21]),
        .O(\Dout_reg[31] [21]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[25]_inst_i_1 
       (.I0(PCPlus8[22]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[22]),
        .O(\Dout_reg[31] [22]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[26]_inst_i_1 
       (.I0(PCPlus8[23]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[23]),
        .O(\Dout_reg[31] [23]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[27]_inst_i_1 
       (.I0(PCPlus8[24]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[24]),
        .O(\Dout_reg[31] [24]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[28]_inst_i_1 
       (.I0(PCPlus8[25]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[25]),
        .O(\Dout_reg[31] [25]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[29]_inst_i_1 
       (.I0(PCPlus8[26]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[26]),
        .O(\Dout_reg[31] [26]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[30]_inst_i_1 
       (.I0(PCPlus8[27]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[27]),
        .O(\Dout_reg[31] [27]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[31]_inst_i_1 
       (.I0(PCPlus8[28]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[28]),
        .O(\Dout_reg[31] [28]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[3]_inst_i_1 
       (.I0(PCPlus8[0]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[0]),
        .O(\Dout_reg[31] [0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[4]_inst_i_1 
       (.I0(PCPlus8[1]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[1]),
        .O(\Dout_reg[31] [1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[5]_inst_i_1 
       (.I0(PCPlus8[2]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[2]),
        .O(\Dout_reg[31] [2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[6]_inst_i_1 
       (.I0(PCPlus8[3]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[3]),
        .O(\Dout_reg[31] [3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[7]_inst_i_1 
       (.I0(PCPlus8[4]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[4]),
        .O(\Dout_reg[31] [4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[8]_inst_i_1 
       (.I0(PCPlus8[5]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[5]),
        .O(\Dout_reg[31] [5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[9]_inst_i_1 
       (.I0(PCPlus8[6]),
        .I1(\Dout_reg[3] ),
        .I2(DATA_OUT2[6]),
        .O(\Dout_reg[31] [6]));
endmodule

(* ORIG_REF_NAME = "ADDER_n" *) 
module ADDER_n_8
   (\Dout_reg[30] ,
    S_Add_in,
    \Dout_reg[30]_0 ,
    \Dout_reg[30]_1 ,
    Q,
    \ALUResult_out_OBUF[0]_inst_i_6 ,
    \ALUResult_out_OBUF[4]_inst_i_4 ,
    \ALUResult_out_OBUF[8]_inst_i_5 ,
    \ALUResult_out_OBUF[12]_inst_i_5 ,
    \ALUResult_out_OBUF[16]_inst_i_5 ,
    \ALUResult_out_OBUF[20]_inst_i_4 ,
    \ALUResult_out_OBUF[24]_inst_i_3 ,
    DI,
    \ALUResult_out_OBUF[28]_inst_i_4 ,
    \ALUResult_out_OBUF[31]_inst_i_3 ,
    O);
  output [17:0]\Dout_reg[30] ;
  output [13:0]S_Add_in;
  output [0:0]\Dout_reg[30]_0 ;
  output \Dout_reg[30]_1 ;
  input [30:0]Q;
  input [3:0]\ALUResult_out_OBUF[0]_inst_i_6 ;
  input [3:0]\ALUResult_out_OBUF[4]_inst_i_4 ;
  input [3:0]\ALUResult_out_OBUF[8]_inst_i_5 ;
  input [3:0]\ALUResult_out_OBUF[12]_inst_i_5 ;
  input [3:0]\ALUResult_out_OBUF[16]_inst_i_5 ;
  input [3:0]\ALUResult_out_OBUF[20]_inst_i_4 ;
  input [3:0]\ALUResult_out_OBUF[24]_inst_i_3 ;
  input [0:0]DI;
  input [3:0]\ALUResult_out_OBUF[28]_inst_i_4 ;
  input \ALUResult_out_OBUF[31]_inst_i_3 ;
  input [0:0]O;

  wire [3:0]\ALUResult_out_OBUF[0]_inst_i_6 ;
  wire [3:0]\ALUResult_out_OBUF[12]_inst_i_5 ;
  wire [3:0]\ALUResult_out_OBUF[16]_inst_i_5 ;
  wire [3:0]\ALUResult_out_OBUF[20]_inst_i_4 ;
  wire [3:0]\ALUResult_out_OBUF[24]_inst_i_3 ;
  wire [3:0]\ALUResult_out_OBUF[28]_inst_i_4 ;
  wire \ALUResult_out_OBUF[31]_inst_i_3 ;
  wire [3:0]\ALUResult_out_OBUF[4]_inst_i_4 ;
  wire [3:0]\ALUResult_out_OBUF[8]_inst_i_5 ;
  wire [0:0]DI;
  wire [17:0]\Dout_reg[30] ;
  wire [0:0]\Dout_reg[30]_0 ;
  wire \Dout_reg[30]_1 ;
  wire [0:0]O;
  wire [30:0]Q;
  wire S0_carry__0_n_0;
  wire S0_carry__1_n_0;
  wire S0_carry__2_n_0;
  wire S0_carry__3_n_0;
  wire S0_carry__4_n_0;
  wire S0_carry__5_n_0;
  wire S0_carry_n_0;
  wire [13:0]S_Add_in;
  wire [2:0]NLW_S0_carry_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__4_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__5_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__6_CO_UNCONNECTED;

  LUT3 #(
    .INIT(8'h1D)) 
    \ALUResult_out_OBUF[31]_inst_i_5 
       (.I0(S_Add_in[13]),
        .I1(\ALUResult_out_OBUF[31]_inst_i_3 ),
        .I2(O),
        .O(\Dout_reg[30]_1 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry
       (.CI(1'b0),
        .CO({S0_carry_n_0,NLW_S0_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[3:0]),
        .O({\Dout_reg[30] [2],S_Add_in[0],\Dout_reg[30] [1:0]}),
        .S(\ALUResult_out_OBUF[0]_inst_i_6 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__0
       (.CI(S0_carry_n_0),
        .CO({S0_carry__0_n_0,NLW_S0_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O({S_Add_in[2:1],\Dout_reg[30] [4:3]}),
        .S(\ALUResult_out_OBUF[4]_inst_i_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__1
       (.CI(S0_carry__0_n_0),
        .CO({S0_carry__1_n_0,NLW_S0_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[11:8]),
        .O({\Dout_reg[30] [5],S_Add_in[5:3]}),
        .S(\ALUResult_out_OBUF[8]_inst_i_5 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__2
       (.CI(S0_carry__1_n_0),
        .CO({S0_carry__2_n_0,NLW_S0_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[15:12]),
        .O({S_Add_in[8:7],\Dout_reg[30] [6],S_Add_in[6]}),
        .S(\ALUResult_out_OBUF[12]_inst_i_5 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__3
       (.CI(S0_carry__2_n_0),
        .CO({S0_carry__3_n_0,NLW_S0_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[19:16]),
        .O({\Dout_reg[30] [9:7],S_Add_in[9]}),
        .S(\ALUResult_out_OBUF[16]_inst_i_5 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__4
       (.CI(S0_carry__3_n_0),
        .CO({S0_carry__4_n_0,NLW_S0_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[23:20]),
        .O({S_Add_in[12],\Dout_reg[30] [10],S_Add_in[11:10]}),
        .S(\ALUResult_out_OBUF[20]_inst_i_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__5
       (.CI(S0_carry__4_n_0),
        .CO({S0_carry__5_n_0,NLW_S0_carry__5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[27:24]),
        .O(\Dout_reg[30] [14:11]),
        .S(\ALUResult_out_OBUF[24]_inst_i_3 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__6
       (.CI(S0_carry__5_n_0),
        .CO({\Dout_reg[30]_0 ,NLW_S0_carry__6_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({DI,Q[30:28]}),
        .O({S_Add_in[13],\Dout_reg[30] [17:15]}),
        .S(\ALUResult_out_OBUF[28]_inst_i_4 ));
endmodule

module ADD_SUB_n
   (\Dout_reg[30] ,
    CO,
    \Dout_reg[30]_0 ,
    \Dout_reg[30]_1 ,
    \Dout_reg[30]_2 ,
    \Dout_reg[3] ,
    \Dout_reg[7] ,
    \Dout_reg[7]_0 ,
    \Dout_reg[11] ,
    \Dout_reg[11]_0 ,
    \Dout_reg[11]_1 ,
    \Dout_reg[15] ,
    \Dout_reg[15]_0 ,
    \Dout_reg[15]_1 ,
    \Dout_reg[19] ,
    \Dout_reg[23] ,
    \Dout_reg[23]_0 ,
    \Dout_reg[23]_1 ,
    \Dout_reg[30]_3 ,
    Q,
    S,
    \ALUResult_out_OBUF[4]_inst_i_4 ,
    \ALUResult_out_OBUF[8]_inst_i_5 ,
    \ALUResult_out_OBUF[12]_inst_i_5 ,
    \ALUResult_out_OBUF[16]_inst_i_5 ,
    \ALUResult_out_OBUF[20]_inst_i_4 ,
    \ALUResult_out_OBUF[24]_inst_i_3 ,
    DI,
    \ALUResult_out_OBUF[28]_inst_i_4 ,
    \ALUResult_out_OBUF[0]_inst_i_6 ,
    \ALUResult_out_OBUF[4]_inst_i_4_0 ,
    \ALUResult_out_OBUF[8]_inst_i_5_0 ,
    \ALUResult_out_OBUF[12]_inst_i_5_0 ,
    \ALUResult_out_OBUF[16]_inst_i_5_0 ,
    \ALUResult_out_OBUF[20]_inst_i_4_0 ,
    \ALUResult_out_OBUF[24]_inst_i_3_0 ,
    \ALUResult_out_OBUF[28]_inst_i_4_0 ,
    \Dout_reg[3]_0 ,
    \Dout_reg[3]_1 ,
    \Dout_reg[3]_2 );
  output [17:0]\Dout_reg[30] ;
  output [0:0]CO;
  output [17:0]\Dout_reg[30]_0 ;
  output [0:0]\Dout_reg[30]_1 ;
  output \Dout_reg[30]_2 ;
  output \Dout_reg[3] ;
  output \Dout_reg[7] ;
  output \Dout_reg[7]_0 ;
  output \Dout_reg[11] ;
  output \Dout_reg[11]_0 ;
  output \Dout_reg[11]_1 ;
  output \Dout_reg[15] ;
  output \Dout_reg[15]_0 ;
  output \Dout_reg[15]_1 ;
  output \Dout_reg[19] ;
  output \Dout_reg[23] ;
  output \Dout_reg[23]_0 ;
  output \Dout_reg[23]_1 ;
  output \Dout_reg[30]_3 ;
  input [30:0]Q;
  input [3:0]S;
  input [3:0]\ALUResult_out_OBUF[4]_inst_i_4 ;
  input [3:0]\ALUResult_out_OBUF[8]_inst_i_5 ;
  input [3:0]\ALUResult_out_OBUF[12]_inst_i_5 ;
  input [3:0]\ALUResult_out_OBUF[16]_inst_i_5 ;
  input [3:0]\ALUResult_out_OBUF[20]_inst_i_4 ;
  input [3:0]\ALUResult_out_OBUF[24]_inst_i_3 ;
  input [0:0]DI;
  input [3:0]\ALUResult_out_OBUF[28]_inst_i_4 ;
  input [3:0]\ALUResult_out_OBUF[0]_inst_i_6 ;
  input [3:0]\ALUResult_out_OBUF[4]_inst_i_4_0 ;
  input [3:0]\ALUResult_out_OBUF[8]_inst_i_5_0 ;
  input [3:0]\ALUResult_out_OBUF[12]_inst_i_5_0 ;
  input [3:0]\ALUResult_out_OBUF[16]_inst_i_5_0 ;
  input [3:0]\ALUResult_out_OBUF[20]_inst_i_4_0 ;
  input [3:0]\ALUResult_out_OBUF[24]_inst_i_3_0 ;
  input [3:0]\ALUResult_out_OBUF[28]_inst_i_4_0 ;
  input [0:0]\Dout_reg[3]_0 ;
  input \Dout_reg[3]_1 ;
  input [0:0]\Dout_reg[3]_2 ;

  wire [3:0]\ALUResult_out_OBUF[0]_inst_i_6 ;
  wire [3:0]\ALUResult_out_OBUF[12]_inst_i_5 ;
  wire [3:0]\ALUResult_out_OBUF[12]_inst_i_5_0 ;
  wire [3:0]\ALUResult_out_OBUF[16]_inst_i_5 ;
  wire [3:0]\ALUResult_out_OBUF[16]_inst_i_5_0 ;
  wire [3:0]\ALUResult_out_OBUF[20]_inst_i_4 ;
  wire [3:0]\ALUResult_out_OBUF[20]_inst_i_4_0 ;
  wire [3:0]\ALUResult_out_OBUF[24]_inst_i_3 ;
  wire [3:0]\ALUResult_out_OBUF[24]_inst_i_3_0 ;
  wire [3:0]\ALUResult_out_OBUF[28]_inst_i_4 ;
  wire [3:0]\ALUResult_out_OBUF[28]_inst_i_4_0 ;
  wire [3:0]\ALUResult_out_OBUF[4]_inst_i_4 ;
  wire [3:0]\ALUResult_out_OBUF[4]_inst_i_4_0 ;
  wire [3:0]\ALUResult_out_OBUF[8]_inst_i_5 ;
  wire [3:0]\ALUResult_out_OBUF[8]_inst_i_5_0 ;
  wire [0:0]CO;
  wire [0:0]DI;
  wire \Dout_reg[11] ;
  wire \Dout_reg[11]_0 ;
  wire \Dout_reg[11]_1 ;
  wire \Dout_reg[15] ;
  wire \Dout_reg[15]_0 ;
  wire \Dout_reg[15]_1 ;
  wire \Dout_reg[19] ;
  wire \Dout_reg[23] ;
  wire \Dout_reg[23]_0 ;
  wire \Dout_reg[23]_1 ;
  wire [17:0]\Dout_reg[30] ;
  wire [17:0]\Dout_reg[30]_0 ;
  wire [0:0]\Dout_reg[30]_1 ;
  wire \Dout_reg[30]_2 ;
  wire \Dout_reg[30]_3 ;
  wire \Dout_reg[3] ;
  wire [0:0]\Dout_reg[3]_0 ;
  wire \Dout_reg[3]_1 ;
  wire [0:0]\Dout_reg[3]_2 ;
  wire \Dout_reg[7] ;
  wire \Dout_reg[7]_0 ;
  wire [30:0]Q;
  wire [3:0]S;
  wire [31:2]S_Add_in;
  wire [31:31]S_Sub_in;

  ADDER_n_8 ADD_UNIT
       (.\ALUResult_out_OBUF[0]_inst_i_6 (\ALUResult_out_OBUF[0]_inst_i_6 ),
        .\ALUResult_out_OBUF[12]_inst_i_5 (\ALUResult_out_OBUF[12]_inst_i_5_0 ),
        .\ALUResult_out_OBUF[16]_inst_i_5 (\ALUResult_out_OBUF[16]_inst_i_5_0 ),
        .\ALUResult_out_OBUF[20]_inst_i_4 (\ALUResult_out_OBUF[20]_inst_i_4_0 ),
        .\ALUResult_out_OBUF[24]_inst_i_3 (\ALUResult_out_OBUF[24]_inst_i_3_0 ),
        .\ALUResult_out_OBUF[28]_inst_i_4 (\ALUResult_out_OBUF[28]_inst_i_4_0 ),
        .\ALUResult_out_OBUF[31]_inst_i_3 (\Dout_reg[3]_1 ),
        .\ALUResult_out_OBUF[4]_inst_i_4 (\ALUResult_out_OBUF[4]_inst_i_4_0 ),
        .\ALUResult_out_OBUF[8]_inst_i_5 (\ALUResult_out_OBUF[8]_inst_i_5_0 ),
        .DI(DI),
        .\Dout_reg[30] (\Dout_reg[30]_0 ),
        .\Dout_reg[30]_0 (\Dout_reg[30]_1 ),
        .\Dout_reg[30]_1 (\Dout_reg[30]_3 ),
        .O(S_Sub_in),
        .Q(Q),
        .S_Add_in({S_Add_in[31],S_Add_in[23],S_Add_in[21:20],S_Add_in[16:14],S_Add_in[12],S_Add_in[10:6],S_Add_in[2]}));
  SUBTRACTOR_n SUB_UNIT
       (.\ALUResult_out_OBUF[12]_inst_i_5_0 (\ALUResult_out_OBUF[12]_inst_i_5 ),
        .\ALUResult_out_OBUF[16]_inst_i_5_0 (\ALUResult_out_OBUF[16]_inst_i_5 ),
        .\ALUResult_out_OBUF[20]_inst_i_4_0 (\ALUResult_out_OBUF[20]_inst_i_4 ),
        .\ALUResult_out_OBUF[24]_inst_i_3 (\ALUResult_out_OBUF[24]_inst_i_3 ),
        .\ALUResult_out_OBUF[28]_inst_i_4 (\ALUResult_out_OBUF[28]_inst_i_4 ),
        .\ALUResult_out_OBUF[4]_inst_i_4 (\ALUResult_out_OBUF[4]_inst_i_4 ),
        .\ALUResult_out_OBUF[8]_inst_i_5_0 (\ALUResult_out_OBUF[8]_inst_i_5 ),
        .CO(CO),
        .DI(DI),
        .\Dout_reg[11] (\Dout_reg[11] ),
        .\Dout_reg[11]_0 (\Dout_reg[11]_0 ),
        .\Dout_reg[11]_1 (\Dout_reg[11]_1 ),
        .\Dout_reg[15] (\Dout_reg[15] ),
        .\Dout_reg[15]_0 (\Dout_reg[15]_0 ),
        .\Dout_reg[15]_1 (\Dout_reg[15]_1 ),
        .\Dout_reg[19] (\Dout_reg[19] ),
        .\Dout_reg[23] (\Dout_reg[23] ),
        .\Dout_reg[23]_0 (\Dout_reg[23]_0 ),
        .\Dout_reg[23]_1 (\Dout_reg[23]_1 ),
        .\Dout_reg[30] (\Dout_reg[30] ),
        .\Dout_reg[30]_0 (\Dout_reg[30]_2 ),
        .\Dout_reg[3] (\Dout_reg[3] ),
        .\Dout_reg[3]_0 (\Dout_reg[3]_0 ),
        .\Dout_reg[3]_1 (\Dout_reg[3]_1 ),
        .\Dout_reg[3]_2 (\Dout_reg[3]_2 ),
        .\Dout_reg[7] (\Dout_reg[7] ),
        .\Dout_reg[7]_0 (\Dout_reg[7]_0 ),
        .O(S_Sub_in),
        .Q(Q),
        .S(S),
        .S_Add_in({S_Add_in[31],S_Add_in[23],S_Add_in[21:20],S_Add_in[16:14],S_Add_in[12],S_Add_in[10:6],S_Add_in[2]}));
endmodule

module ALU_n
   (\Dout_reg[30] ,
    CO,
    \Dout_reg[30]_0 ,
    \Dout_reg[30]_1 ,
    \Dout_reg[30]_2 ,
    \Dout_reg[3] ,
    \Dout_reg[7] ,
    \Dout_reg[7]_0 ,
    \Dout_reg[11] ,
    \Dout_reg[11]_0 ,
    \Dout_reg[11]_1 ,
    \Dout_reg[15] ,
    \Dout_reg[15]_0 ,
    \Dout_reg[15]_1 ,
    \Dout_reg[19] ,
    \Dout_reg[23] ,
    \Dout_reg[23]_0 ,
    \Dout_reg[23]_1 ,
    \Dout_reg[30]_3 ,
    Q,
    S,
    \ALUResult_out_OBUF[4]_inst_i_4 ,
    \ALUResult_out_OBUF[8]_inst_i_5 ,
    \ALUResult_out_OBUF[12]_inst_i_5 ,
    \ALUResult_out_OBUF[16]_inst_i_5 ,
    \ALUResult_out_OBUF[20]_inst_i_4 ,
    \ALUResult_out_OBUF[24]_inst_i_3 ,
    DI,
    \ALUResult_out_OBUF[28]_inst_i_4 ,
    \ALUResult_out_OBUF[0]_inst_i_6 ,
    \ALUResult_out_OBUF[4]_inst_i_4_0 ,
    \ALUResult_out_OBUF[8]_inst_i_5_0 ,
    \ALUResult_out_OBUF[12]_inst_i_5_0 ,
    \ALUResult_out_OBUF[16]_inst_i_5_0 ,
    \ALUResult_out_OBUF[20]_inst_i_4_0 ,
    \ALUResult_out_OBUF[24]_inst_i_3_0 ,
    \ALUResult_out_OBUF[28]_inst_i_4_0 ,
    \Dout_reg[3]_0 ,
    \Dout_reg[3]_1 ,
    \Dout_reg[3]_2 );
  output [17:0]\Dout_reg[30] ;
  output [0:0]CO;
  output [17:0]\Dout_reg[30]_0 ;
  output [0:0]\Dout_reg[30]_1 ;
  output \Dout_reg[30]_2 ;
  output \Dout_reg[3] ;
  output \Dout_reg[7] ;
  output \Dout_reg[7]_0 ;
  output \Dout_reg[11] ;
  output \Dout_reg[11]_0 ;
  output \Dout_reg[11]_1 ;
  output \Dout_reg[15] ;
  output \Dout_reg[15]_0 ;
  output \Dout_reg[15]_1 ;
  output \Dout_reg[19] ;
  output \Dout_reg[23] ;
  output \Dout_reg[23]_0 ;
  output \Dout_reg[23]_1 ;
  output \Dout_reg[30]_3 ;
  input [30:0]Q;
  input [3:0]S;
  input [3:0]\ALUResult_out_OBUF[4]_inst_i_4 ;
  input [3:0]\ALUResult_out_OBUF[8]_inst_i_5 ;
  input [3:0]\ALUResult_out_OBUF[12]_inst_i_5 ;
  input [3:0]\ALUResult_out_OBUF[16]_inst_i_5 ;
  input [3:0]\ALUResult_out_OBUF[20]_inst_i_4 ;
  input [3:0]\ALUResult_out_OBUF[24]_inst_i_3 ;
  input [0:0]DI;
  input [3:0]\ALUResult_out_OBUF[28]_inst_i_4 ;
  input [3:0]\ALUResult_out_OBUF[0]_inst_i_6 ;
  input [3:0]\ALUResult_out_OBUF[4]_inst_i_4_0 ;
  input [3:0]\ALUResult_out_OBUF[8]_inst_i_5_0 ;
  input [3:0]\ALUResult_out_OBUF[12]_inst_i_5_0 ;
  input [3:0]\ALUResult_out_OBUF[16]_inst_i_5_0 ;
  input [3:0]\ALUResult_out_OBUF[20]_inst_i_4_0 ;
  input [3:0]\ALUResult_out_OBUF[24]_inst_i_3_0 ;
  input [3:0]\ALUResult_out_OBUF[28]_inst_i_4_0 ;
  input [0:0]\Dout_reg[3]_0 ;
  input \Dout_reg[3]_1 ;
  input [0:0]\Dout_reg[3]_2 ;

  wire [3:0]\ALUResult_out_OBUF[0]_inst_i_6 ;
  wire [3:0]\ALUResult_out_OBUF[12]_inst_i_5 ;
  wire [3:0]\ALUResult_out_OBUF[12]_inst_i_5_0 ;
  wire [3:0]\ALUResult_out_OBUF[16]_inst_i_5 ;
  wire [3:0]\ALUResult_out_OBUF[16]_inst_i_5_0 ;
  wire [3:0]\ALUResult_out_OBUF[20]_inst_i_4 ;
  wire [3:0]\ALUResult_out_OBUF[20]_inst_i_4_0 ;
  wire [3:0]\ALUResult_out_OBUF[24]_inst_i_3 ;
  wire [3:0]\ALUResult_out_OBUF[24]_inst_i_3_0 ;
  wire [3:0]\ALUResult_out_OBUF[28]_inst_i_4 ;
  wire [3:0]\ALUResult_out_OBUF[28]_inst_i_4_0 ;
  wire [3:0]\ALUResult_out_OBUF[4]_inst_i_4 ;
  wire [3:0]\ALUResult_out_OBUF[4]_inst_i_4_0 ;
  wire [3:0]\ALUResult_out_OBUF[8]_inst_i_5 ;
  wire [3:0]\ALUResult_out_OBUF[8]_inst_i_5_0 ;
  wire [0:0]CO;
  wire [0:0]DI;
  wire \Dout_reg[11] ;
  wire \Dout_reg[11]_0 ;
  wire \Dout_reg[11]_1 ;
  wire \Dout_reg[15] ;
  wire \Dout_reg[15]_0 ;
  wire \Dout_reg[15]_1 ;
  wire \Dout_reg[19] ;
  wire \Dout_reg[23] ;
  wire \Dout_reg[23]_0 ;
  wire \Dout_reg[23]_1 ;
  wire [17:0]\Dout_reg[30] ;
  wire [17:0]\Dout_reg[30]_0 ;
  wire [0:0]\Dout_reg[30]_1 ;
  wire \Dout_reg[30]_2 ;
  wire \Dout_reg[30]_3 ;
  wire \Dout_reg[3] ;
  wire [0:0]\Dout_reg[3]_0 ;
  wire \Dout_reg[3]_1 ;
  wire [0:0]\Dout_reg[3]_2 ;
  wire \Dout_reg[7] ;
  wire \Dout_reg[7]_0 ;
  wire [30:0]Q;
  wire [3:0]S;

  ADD_SUB_n ADD_SUB
       (.\ALUResult_out_OBUF[0]_inst_i_6 (\ALUResult_out_OBUF[0]_inst_i_6 ),
        .\ALUResult_out_OBUF[12]_inst_i_5 (\ALUResult_out_OBUF[12]_inst_i_5 ),
        .\ALUResult_out_OBUF[12]_inst_i_5_0 (\ALUResult_out_OBUF[12]_inst_i_5_0 ),
        .\ALUResult_out_OBUF[16]_inst_i_5 (\ALUResult_out_OBUF[16]_inst_i_5 ),
        .\ALUResult_out_OBUF[16]_inst_i_5_0 (\ALUResult_out_OBUF[16]_inst_i_5_0 ),
        .\ALUResult_out_OBUF[20]_inst_i_4 (\ALUResult_out_OBUF[20]_inst_i_4 ),
        .\ALUResult_out_OBUF[20]_inst_i_4_0 (\ALUResult_out_OBUF[20]_inst_i_4_0 ),
        .\ALUResult_out_OBUF[24]_inst_i_3 (\ALUResult_out_OBUF[24]_inst_i_3 ),
        .\ALUResult_out_OBUF[24]_inst_i_3_0 (\ALUResult_out_OBUF[24]_inst_i_3_0 ),
        .\ALUResult_out_OBUF[28]_inst_i_4 (\ALUResult_out_OBUF[28]_inst_i_4 ),
        .\ALUResult_out_OBUF[28]_inst_i_4_0 (\ALUResult_out_OBUF[28]_inst_i_4_0 ),
        .\ALUResult_out_OBUF[4]_inst_i_4 (\ALUResult_out_OBUF[4]_inst_i_4 ),
        .\ALUResult_out_OBUF[4]_inst_i_4_0 (\ALUResult_out_OBUF[4]_inst_i_4_0 ),
        .\ALUResult_out_OBUF[8]_inst_i_5 (\ALUResult_out_OBUF[8]_inst_i_5 ),
        .\ALUResult_out_OBUF[8]_inst_i_5_0 (\ALUResult_out_OBUF[8]_inst_i_5_0 ),
        .CO(CO),
        .DI(DI),
        .\Dout_reg[11] (\Dout_reg[11] ),
        .\Dout_reg[11]_0 (\Dout_reg[11]_0 ),
        .\Dout_reg[11]_1 (\Dout_reg[11]_1 ),
        .\Dout_reg[15] (\Dout_reg[15] ),
        .\Dout_reg[15]_0 (\Dout_reg[15]_0 ),
        .\Dout_reg[15]_1 (\Dout_reg[15]_1 ),
        .\Dout_reg[19] (\Dout_reg[19] ),
        .\Dout_reg[23] (\Dout_reg[23] ),
        .\Dout_reg[23]_0 (\Dout_reg[23]_0 ),
        .\Dout_reg[23]_1 (\Dout_reg[23]_1 ),
        .\Dout_reg[30] (\Dout_reg[30] ),
        .\Dout_reg[30]_0 (\Dout_reg[30]_0 ),
        .\Dout_reg[30]_1 (\Dout_reg[30]_1 ),
        .\Dout_reg[30]_2 (\Dout_reg[30]_2 ),
        .\Dout_reg[30]_3 (\Dout_reg[30]_3 ),
        .\Dout_reg[3] (\Dout_reg[3] ),
        .\Dout_reg[3]_0 (\Dout_reg[3]_0 ),
        .\Dout_reg[3]_1 (\Dout_reg[3]_1 ),
        .\Dout_reg[3]_2 (\Dout_reg[3]_2 ),
        .\Dout_reg[7] (\Dout_reg[7] ),
        .\Dout_reg[7]_0 (\Dout_reg[7]_0 ),
        .Q(Q),
        .S(S));
endmodule

(* ECO_CHECKSUM = "92c7a243" *) (* POWER_OPT_BRAM_CDC = "0" *) (* POWER_OPT_BRAM_SR_ADDR = "0" *) 
(* POWER_OPT_LOOPED_NET_PERCENTAGE = "0" *) 
(* NotValidForBitStream *)
module ARM_PROCESSOR_n
   (CLK,
    RESET,
    ALUControl_out,
    Instr_out,
    ALUResult_out,
    Result_out,
    WriteData_out);
  input CLK;
  input RESET;
  output [3:0]ALUControl_out;
  output [31:0]Instr_out;
  output [31:0]ALUResult_out;
  output [31:0]Result_out;
  output [31:0]WriteData_out;

  wire [3:0]ALUControl_out;
  wire [3:0]ALUControl_out_OBUF;
  wire [31:0]ALUResult_out;
  wire [31:0]ALUResult_out_OBUF;
  wire CLK;
  wire CLK_IBUF;
  wire CLK_IBUF_BUFG;
  wire CONTROL_n_0;
  wire CONTROL_n_1;
  wire CONTROL_n_10;
  wire CONTROL_n_2;
  wire CONTROL_n_7;
  wire DATAPATH_n_109;
  wire DATAPATH_n_111;
  wire DATAPATH_n_112;
  wire DATAPATH_n_113;
  wire DATAPATH_n_114;
  wire DATAPATH_n_115;
  wire DATAPATH_n_116;
  wire DATAPATH_n_117;
  wire DATAPATH_n_118;
  wire DATAPATH_n_119;
  wire DATAPATH_n_120;
  wire DATAPATH_n_121;
  wire DATAPATH_n_122;
  wire DATAPATH_n_123;
  wire DATAPATH_n_124;
  wire DATAPATH_n_125;
  wire DATAPATH_n_126;
  wire DATAPATH_n_127;
  wire DATAPATH_n_128;
  wire DATAPATH_n_129;
  wire DATAPATH_n_130;
  wire DATAPATH_n_131;
  wire DATAPATH_n_132;
  wire DATAPATH_n_133;
  wire DATAPATH_n_134;
  wire DATAPATH_n_135;
  wire DATAPATH_n_136;
  wire DATAPATH_n_137;
  wire DATAPATH_n_138;
  wire DATAPATH_n_139;
  wire DATAPATH_n_140;
  wire DATAPATH_n_71;
  wire DATAPATH_n_72;
  wire DATAPATH_n_73;
  wire DATAPATH_n_74;
  wire DATAPATH_n_75;
  wire DATAPATH_n_76;
  wire FlagsWrite_in;
  wire [0:0]Flags_in;
  wire IRWrite_in;
  wire [31:0]Instr_out;
  wire [27:20]Instr_out_OBUF;
  wire MAWrite_in;
  wire MemWrite_in;
  wire PCWrite_in;
  wire RESET;
  wire RESET_IBUF;
  wire RegWrite_in;
  wire [31:0]Result_out;
  wire [31:0]Result_out_OBUF;
  wire [31:0]WriteData_out;
  wire [31:0]WriteData_out_OBUF;

initial begin
 $sdf_annotate("PROCESSOR_TB_time_impl.sdf",,,,"tool_control");
end
  (* IOB = "TRUE" *) 
  OBUF \ALUControl_out_OBUF[0]_inst 
       (.I(ALUControl_out_OBUF[0]),
        .O(ALUControl_out[0]));
  (* IOB = "TRUE" *) 
  OBUF \ALUControl_out_OBUF[1]_inst 
       (.I(ALUControl_out_OBUF[1]),
        .O(ALUControl_out[1]));
  (* IOB = "TRUE" *) 
  OBUF \ALUControl_out_OBUF[2]_inst 
       (.I(ALUControl_out_OBUF[2]),
        .O(ALUControl_out[2]));
  (* IOB = "TRUE" *) 
  OBUF \ALUControl_out_OBUF[3]_inst 
       (.I(ALUControl_out_OBUF[3]),
        .O(ALUControl_out[3]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[0]_inst 
       (.I(ALUResult_out_OBUF[0]),
        .O(ALUResult_out[0]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[10]_inst 
       (.I(ALUResult_out_OBUF[10]),
        .O(ALUResult_out[10]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[11]_inst 
       (.I(ALUResult_out_OBUF[11]),
        .O(ALUResult_out[11]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[12]_inst 
       (.I(ALUResult_out_OBUF[12]),
        .O(ALUResult_out[12]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[13]_inst 
       (.I(ALUResult_out_OBUF[13]),
        .O(ALUResult_out[13]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[14]_inst 
       (.I(ALUResult_out_OBUF[14]),
        .O(ALUResult_out[14]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[15]_inst 
       (.I(ALUResult_out_OBUF[15]),
        .O(ALUResult_out[15]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[16]_inst 
       (.I(ALUResult_out_OBUF[16]),
        .O(ALUResult_out[16]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[17]_inst 
       (.I(ALUResult_out_OBUF[17]),
        .O(ALUResult_out[17]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[18]_inst 
       (.I(ALUResult_out_OBUF[18]),
        .O(ALUResult_out[18]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[19]_inst 
       (.I(ALUResult_out_OBUF[19]),
        .O(ALUResult_out[19]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[1]_inst 
       (.I(ALUResult_out_OBUF[1]),
        .O(ALUResult_out[1]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[20]_inst 
       (.I(ALUResult_out_OBUF[20]),
        .O(ALUResult_out[20]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[21]_inst 
       (.I(ALUResult_out_OBUF[21]),
        .O(ALUResult_out[21]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[22]_inst 
       (.I(ALUResult_out_OBUF[22]),
        .O(ALUResult_out[22]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[23]_inst 
       (.I(ALUResult_out_OBUF[23]),
        .O(ALUResult_out[23]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[24]_inst 
       (.I(ALUResult_out_OBUF[24]),
        .O(ALUResult_out[24]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[25]_inst 
       (.I(ALUResult_out_OBUF[25]),
        .O(ALUResult_out[25]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[26]_inst 
       (.I(ALUResult_out_OBUF[26]),
        .O(ALUResult_out[26]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[27]_inst 
       (.I(ALUResult_out_OBUF[27]),
        .O(ALUResult_out[27]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[28]_inst 
       (.I(ALUResult_out_OBUF[28]),
        .O(ALUResult_out[28]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[29]_inst 
       (.I(ALUResult_out_OBUF[29]),
        .O(ALUResult_out[29]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[2]_inst 
       (.I(ALUResult_out_OBUF[2]),
        .O(ALUResult_out[2]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[30]_inst 
       (.I(ALUResult_out_OBUF[30]),
        .O(ALUResult_out[30]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[31]_inst 
       (.I(ALUResult_out_OBUF[31]),
        .O(ALUResult_out[31]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[3]_inst 
       (.I(ALUResult_out_OBUF[3]),
        .O(ALUResult_out[3]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[4]_inst 
       (.I(ALUResult_out_OBUF[4]),
        .O(ALUResult_out[4]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[5]_inst 
       (.I(ALUResult_out_OBUF[5]),
        .O(ALUResult_out[5]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[6]_inst 
       (.I(ALUResult_out_OBUF[6]),
        .O(ALUResult_out[6]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[7]_inst 
       (.I(ALUResult_out_OBUF[7]),
        .O(ALUResult_out[7]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[8]_inst 
       (.I(ALUResult_out_OBUF[8]),
        .O(ALUResult_out[8]));
  (* IOB = "TRUE" *) 
  OBUF \ALUResult_out_OBUF[9]_inst 
       (.I(ALUResult_out_OBUF[9]),
        .O(ALUResult_out[9]));
  BUFG CLK_IBUF_BUFG_inst
       (.I(CLK_IBUF),
        .O(CLK_IBUF_BUFG));
  IBUF CLK_IBUF_inst
       (.I(CLK),
        .O(CLK_IBUF));
  CONTROL_UNIT CONTROL
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D({DATAPATH_n_72,DATAPATH_n_73,DATAPATH_n_74,DATAPATH_n_75,DATAPATH_n_76}),
        .\Dout_reg[0] (DATAPATH_n_71),
        .E(PCWrite_in),
        .\FSM_onehot_current_state_reg[10] (DATAPATH_n_111),
        .\FSM_onehot_current_state_reg[11] (CONTROL_n_10),
        .\FSM_onehot_current_state_reg[13] (CONTROL_n_7),
        .\FSM_onehot_current_state_reg[1] (CONTROL_n_0),
        .\FSM_onehot_current_state_reg[2] (DATAPATH_n_109),
        .FlagsWrite_in(FlagsWrite_in),
        .Flags_in(Flags_in),
        .Instr_out_OBUF({Instr_out_OBUF[27:26],Instr_out_OBUF[20]}),
        .Q({CONTROL_n_1,CONTROL_n_2,MemWrite_in,MAWrite_in,IRWrite_in}),
        .RESET_IBUF(RESET_IBUF),
        .RegWrite_in(RegWrite_in));
  DATAPATH_MC_n DATAPATH
       (.ALUControl_out_OBUF(ALUControl_out_OBUF),
        .ALUResult_out_OBUF(ALUResult_out_OBUF),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D({DATAPATH_n_72,DATAPATH_n_73,DATAPATH_n_74,DATAPATH_n_75,DATAPATH_n_76}),
        .\Dout_reg[0] (Flags_in),
        .\Dout_reg[0]_0 (CONTROL_n_10),
        .\Dout_reg[15]_rep (DATAPATH_n_111),
        .\Dout_reg[19]_rep (DATAPATH_n_140),
        .\Dout_reg[25]_rep (DATAPATH_n_71),
        .\Dout_reg[27]_rep__0 ({Instr_out_OBUF[27:26],Instr_out_OBUF[20]}),
        .\Dout_reg[28]_rep (DATAPATH_n_109),
        .\Dout_reg[30] (CONTROL_n_7),
        .\Dout_reg[31] (WriteData_out_OBUF),
        .\Dout_reg[31]_0 ({DATAPATH_n_112,DATAPATH_n_113,DATAPATH_n_114,DATAPATH_n_115,DATAPATH_n_116,DATAPATH_n_117,DATAPATH_n_118,DATAPATH_n_119,DATAPATH_n_120,DATAPATH_n_121,DATAPATH_n_122,DATAPATH_n_123,DATAPATH_n_124,DATAPATH_n_125,DATAPATH_n_126,DATAPATH_n_127,DATAPATH_n_128,DATAPATH_n_129,DATAPATH_n_130,DATAPATH_n_131,DATAPATH_n_132,DATAPATH_n_133,DATAPATH_n_134,DATAPATH_n_135,DATAPATH_n_136,DATAPATH_n_137,DATAPATH_n_138}),
        .\Dout_reg[31]_rep (DATAPATH_n_139),
        .E(PCWrite_in),
        .\FSM_onehot_current_state_reg[13] (CONTROL_n_0),
        .FlagsWrite_in(FlagsWrite_in),
        .Q({CONTROL_n_1,CONTROL_n_2,MemWrite_in,MAWrite_in,IRWrite_in}),
        .RESET_IBUF(RESET_IBUF),
        .RegWrite_in(RegWrite_in),
        .Result_out_OBUF(Result_out_OBUF));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[0]_inst 
       (.I(DATAPATH_n_138),
        .O(Instr_out[0]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[10]_inst 
       (.I(1'b0),
        .O(Instr_out[10]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[11]_inst 
       (.I(1'b0),
        .O(Instr_out[11]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[12]_inst 
       (.I(DATAPATH_n_129),
        .O(Instr_out[12]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[13]_inst 
       (.I(DATAPATH_n_128),
        .O(Instr_out[13]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[14]_inst 
       (.I(DATAPATH_n_127),
        .O(Instr_out[14]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[15]_inst 
       (.I(DATAPATH_n_126),
        .O(Instr_out[15]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[16]_inst 
       (.I(DATAPATH_n_125),
        .O(Instr_out[16]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[17]_inst 
       (.I(DATAPATH_n_140),
        .O(Instr_out[17]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[18]_inst 
       (.I(DATAPATH_n_124),
        .O(Instr_out[18]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[19]_inst 
       (.I(DATAPATH_n_123),
        .O(Instr_out[19]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[1]_inst 
       (.I(DATAPATH_n_137),
        .O(Instr_out[1]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[20]_inst 
       (.I(DATAPATH_n_122),
        .O(Instr_out[20]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[21]_inst 
       (.I(DATAPATH_n_121),
        .O(Instr_out[21]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[22]_inst 
       (.I(DATAPATH_n_120),
        .O(Instr_out[22]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[23]_inst 
       (.I(DATAPATH_n_119),
        .O(Instr_out[23]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[24]_inst 
       (.I(DATAPATH_n_118),
        .O(Instr_out[24]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[25]_inst 
       (.I(DATAPATH_n_117),
        .O(Instr_out[25]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[26]_inst 
       (.I(DATAPATH_n_116),
        .O(Instr_out[26]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[27]_inst 
       (.I(DATAPATH_n_115),
        .O(Instr_out[27]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[28]_inst 
       (.I(DATAPATH_n_114),
        .O(Instr_out[28]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[29]_inst 
       (.I(DATAPATH_n_139),
        .O(Instr_out[29]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[2]_inst 
       (.I(DATAPATH_n_136),
        .O(Instr_out[2]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[30]_inst 
       (.I(DATAPATH_n_113),
        .O(Instr_out[30]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[31]_inst 
       (.I(DATAPATH_n_112),
        .O(Instr_out[31]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[3]_inst 
       (.I(DATAPATH_n_135),
        .O(Instr_out[3]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[4]_inst 
       (.I(DATAPATH_n_134),
        .O(Instr_out[4]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[5]_inst 
       (.I(DATAPATH_n_133),
        .O(Instr_out[5]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[6]_inst 
       (.I(DATAPATH_n_132),
        .O(Instr_out[6]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[7]_inst 
       (.I(DATAPATH_n_131),
        .O(Instr_out[7]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[8]_inst 
       (.I(DATAPATH_n_130),
        .O(Instr_out[8]));
  (* IOB = "TRUE" *) 
  OBUF \Instr_out_OBUF[9]_inst 
       (.I(1'b0),
        .O(Instr_out[9]));
  IBUF RESET_IBUF_inst
       (.I(RESET),
        .O(RESET_IBUF));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[0]_inst 
       (.I(Result_out_OBUF[0]),
        .O(Result_out[0]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[10]_inst 
       (.I(Result_out_OBUF[10]),
        .O(Result_out[10]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[11]_inst 
       (.I(Result_out_OBUF[11]),
        .O(Result_out[11]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[12]_inst 
       (.I(Result_out_OBUF[12]),
        .O(Result_out[12]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[13]_inst 
       (.I(Result_out_OBUF[13]),
        .O(Result_out[13]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[14]_inst 
       (.I(Result_out_OBUF[14]),
        .O(Result_out[14]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[15]_inst 
       (.I(Result_out_OBUF[15]),
        .O(Result_out[15]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[16]_inst 
       (.I(Result_out_OBUF[16]),
        .O(Result_out[16]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[17]_inst 
       (.I(Result_out_OBUF[17]),
        .O(Result_out[17]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[18]_inst 
       (.I(Result_out_OBUF[18]),
        .O(Result_out[18]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[19]_inst 
       (.I(Result_out_OBUF[19]),
        .O(Result_out[19]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[1]_inst 
       (.I(Result_out_OBUF[1]),
        .O(Result_out[1]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[20]_inst 
       (.I(Result_out_OBUF[20]),
        .O(Result_out[20]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[21]_inst 
       (.I(Result_out_OBUF[21]),
        .O(Result_out[21]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[22]_inst 
       (.I(Result_out_OBUF[22]),
        .O(Result_out[22]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[23]_inst 
       (.I(Result_out_OBUF[23]),
        .O(Result_out[23]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[24]_inst 
       (.I(Result_out_OBUF[24]),
        .O(Result_out[24]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[25]_inst 
       (.I(Result_out_OBUF[25]),
        .O(Result_out[25]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[26]_inst 
       (.I(Result_out_OBUF[26]),
        .O(Result_out[26]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[27]_inst 
       (.I(Result_out_OBUF[27]),
        .O(Result_out[27]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[28]_inst 
       (.I(Result_out_OBUF[28]),
        .O(Result_out[28]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[29]_inst 
       (.I(Result_out_OBUF[29]),
        .O(Result_out[29]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[2]_inst 
       (.I(Result_out_OBUF[2]),
        .O(Result_out[2]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[30]_inst 
       (.I(Result_out_OBUF[30]),
        .O(Result_out[30]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[31]_inst 
       (.I(Result_out_OBUF[31]),
        .O(Result_out[31]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[3]_inst 
       (.I(Result_out_OBUF[3]),
        .O(Result_out[3]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[4]_inst 
       (.I(Result_out_OBUF[4]),
        .O(Result_out[4]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[5]_inst 
       (.I(Result_out_OBUF[5]),
        .O(Result_out[5]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[6]_inst 
       (.I(Result_out_OBUF[6]),
        .O(Result_out[6]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[7]_inst 
       (.I(Result_out_OBUF[7]),
        .O(Result_out[7]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[8]_inst 
       (.I(Result_out_OBUF[8]),
        .O(Result_out[8]));
  (* IOB = "TRUE" *) 
  OBUF \Result_out_OBUF[9]_inst 
       (.I(Result_out_OBUF[9]),
        .O(Result_out[9]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[0]_inst 
       (.I(WriteData_out_OBUF[0]),
        .O(WriteData_out[0]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[10]_inst 
       (.I(WriteData_out_OBUF[10]),
        .O(WriteData_out[10]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[11]_inst 
       (.I(WriteData_out_OBUF[11]),
        .O(WriteData_out[11]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[12]_inst 
       (.I(WriteData_out_OBUF[12]),
        .O(WriteData_out[12]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[13]_inst 
       (.I(WriteData_out_OBUF[13]),
        .O(WriteData_out[13]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[14]_inst 
       (.I(WriteData_out_OBUF[14]),
        .O(WriteData_out[14]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[15]_inst 
       (.I(WriteData_out_OBUF[15]),
        .O(WriteData_out[15]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[16]_inst 
       (.I(WriteData_out_OBUF[16]),
        .O(WriteData_out[16]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[17]_inst 
       (.I(WriteData_out_OBUF[17]),
        .O(WriteData_out[17]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[18]_inst 
       (.I(WriteData_out_OBUF[18]),
        .O(WriteData_out[18]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[19]_inst 
       (.I(WriteData_out_OBUF[19]),
        .O(WriteData_out[19]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[1]_inst 
       (.I(WriteData_out_OBUF[1]),
        .O(WriteData_out[1]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[20]_inst 
       (.I(WriteData_out_OBUF[20]),
        .O(WriteData_out[20]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[21]_inst 
       (.I(WriteData_out_OBUF[21]),
        .O(WriteData_out[21]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[22]_inst 
       (.I(WriteData_out_OBUF[22]),
        .O(WriteData_out[22]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[23]_inst 
       (.I(WriteData_out_OBUF[23]),
        .O(WriteData_out[23]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[24]_inst 
       (.I(WriteData_out_OBUF[24]),
        .O(WriteData_out[24]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[25]_inst 
       (.I(WriteData_out_OBUF[25]),
        .O(WriteData_out[25]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[26]_inst 
       (.I(WriteData_out_OBUF[26]),
        .O(WriteData_out[26]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[27]_inst 
       (.I(WriteData_out_OBUF[27]),
        .O(WriteData_out[27]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[28]_inst 
       (.I(WriteData_out_OBUF[28]),
        .O(WriteData_out[28]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[29]_inst 
       (.I(WriteData_out_OBUF[29]),
        .O(WriteData_out[29]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[2]_inst 
       (.I(WriteData_out_OBUF[2]),
        .O(WriteData_out[2]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[30]_inst 
       (.I(WriteData_out_OBUF[30]),
        .O(WriteData_out[30]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[31]_inst 
       (.I(WriteData_out_OBUF[31]),
        .O(WriteData_out[31]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[3]_inst 
       (.I(WriteData_out_OBUF[3]),
        .O(WriteData_out[3]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[4]_inst 
       (.I(WriteData_out_OBUF[4]),
        .O(WriteData_out[4]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[5]_inst 
       (.I(WriteData_out_OBUF[5]),
        .O(WriteData_out[5]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[6]_inst 
       (.I(WriteData_out_OBUF[6]),
        .O(WriteData_out[6]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[7]_inst 
       (.I(WriteData_out_OBUF[7]),
        .O(WriteData_out[7]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[8]_inst 
       (.I(WriteData_out_OBUF[8]),
        .O(WriteData_out[8]));
  (* IOB = "TRUE" *) 
  OBUF \WriteData_out_OBUF[9]_inst 
       (.I(WriteData_out_OBUF[9]),
        .O(WriteData_out[9]));
endmodule

module CONTROL_UNIT
   (\FSM_onehot_current_state_reg[1] ,
    Q,
    E,
    \FSM_onehot_current_state_reg[13] ,
    FlagsWrite_in,
    RegWrite_in,
    \FSM_onehot_current_state_reg[11] ,
    Instr_out_OBUF,
    \FSM_onehot_current_state_reg[2] ,
    \FSM_onehot_current_state_reg[10] ,
    \Dout_reg[0] ,
    Flags_in,
    RESET_IBUF,
    D,
    CLK_IBUF_BUFG);
  output \FSM_onehot_current_state_reg[1] ;
  output [4:0]Q;
  output [0:0]E;
  output \FSM_onehot_current_state_reg[13] ;
  output FlagsWrite_in;
  output RegWrite_in;
  output \FSM_onehot_current_state_reg[11] ;
  input [2:0]Instr_out_OBUF;
  input \FSM_onehot_current_state_reg[2] ;
  input \FSM_onehot_current_state_reg[10] ;
  input \Dout_reg[0] ;
  input [0:0]Flags_in;
  input RESET_IBUF;
  input [4:0]D;
  input CLK_IBUF_BUFG;

  wire CLK_IBUF_BUFG;
  wire [4:0]D;
  wire \Dout_reg[0] ;
  wire [0:0]E;
  wire \FSM_onehot_current_state_reg[10] ;
  wire \FSM_onehot_current_state_reg[11] ;
  wire \FSM_onehot_current_state_reg[13] ;
  wire \FSM_onehot_current_state_reg[1] ;
  wire \FSM_onehot_current_state_reg[2] ;
  wire FlagsWrite_in;
  wire [0:0]Flags_in;
  wire [2:0]Instr_out_OBUF;
  wire [4:0]Q;
  wire RESET_IBUF;
  wire RegWrite_in;

  FSM FSM_UNIT
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D(D),
        .\Dout_reg[0] (\Dout_reg[0] ),
        .E(E),
        .\FSM_onehot_current_state_reg[10]_0 (\FSM_onehot_current_state_reg[10] ),
        .\FSM_onehot_current_state_reg[11]_0 (\FSM_onehot_current_state_reg[11] ),
        .\FSM_onehot_current_state_reg[13]_0 (\FSM_onehot_current_state_reg[13] ),
        .\FSM_onehot_current_state_reg[1]_0 (\FSM_onehot_current_state_reg[1] ),
        .\FSM_onehot_current_state_reg[2]_0 (\FSM_onehot_current_state_reg[2] ),
        .FlagsWrite_in(FlagsWrite_in),
        .Flags_in(Flags_in),
        .Instr_out_OBUF(Instr_out_OBUF),
        .Q(Q),
        .RESET_IBUF(RESET_IBUF),
        .RegWrite_in(RegWrite_in));
endmodule

module DATAPATH_MC_n
   (ALUControl_out_OBUF,
    Result_out_OBUF,
    ALUResult_out_OBUF,
    \Dout_reg[27]_rep__0 ,
    \Dout_reg[25]_rep ,
    D,
    \Dout_reg[31] ,
    \Dout_reg[28]_rep ,
    \Dout_reg[0] ,
    \Dout_reg[15]_rep ,
    \Dout_reg[31]_0 ,
    \Dout_reg[31]_rep ,
    \Dout_reg[19]_rep ,
    Q,
    \Dout_reg[30] ,
    \FSM_onehot_current_state_reg[13] ,
    CLK_IBUF_BUFG,
    RegWrite_in,
    RESET_IBUF,
    FlagsWrite_in,
    \Dout_reg[0]_0 ,
    E);
  output [3:0]ALUControl_out_OBUF;
  output [31:0]Result_out_OBUF;
  output [31:0]ALUResult_out_OBUF;
  output [2:0]\Dout_reg[27]_rep__0 ;
  output \Dout_reg[25]_rep ;
  output [4:0]D;
  output [31:0]\Dout_reg[31] ;
  output \Dout_reg[28]_rep ;
  output [0:0]\Dout_reg[0] ;
  output \Dout_reg[15]_rep ;
  output [26:0]\Dout_reg[31]_0 ;
  output \Dout_reg[31]_rep ;
  output \Dout_reg[19]_rep ;
  input [4:0]Q;
  input \Dout_reg[30] ;
  input \FSM_onehot_current_state_reg[13] ;
  input CLK_IBUF_BUFG;
  input RegWrite_in;
  input RESET_IBUF;
  input FlagsWrite_in;
  input \Dout_reg[0]_0 ;
  input [0:0]E;

  wire [4:0]ADDR;
  wire [30:0]\ADD_SUB/S_Add_in ;
  wire [30:0]\ADD_SUB/S_Sub_in ;
  wire [3:0]ALUControl_out_OBUF;
  wire [31:0]ALUResult_in_regS;
  wire [31:0]ALUResult_out_OBUF;
  wire ALU_n_18;
  wire ALU_n_37;
  wire ALU_n_38;
  wire ALU_n_39;
  wire ALU_n_40;
  wire ALU_n_41;
  wire ALU_n_42;
  wire ALU_n_43;
  wire ALU_n_44;
  wire ALU_n_45;
  wire ALU_n_46;
  wire ALU_n_47;
  wire ALU_n_48;
  wire ALU_n_49;
  wire ALU_n_50;
  wire ALU_n_51;
  wire ALU_n_52;
  wire CLK_IBUF_BUFG;
  wire [4:0]D;
  wire [0:0]\Dout_reg[0] ;
  wire \Dout_reg[0]_0 ;
  wire \Dout_reg[15]_rep ;
  wire \Dout_reg[19]_rep ;
  wire \Dout_reg[25]_rep ;
  wire [2:0]\Dout_reg[27]_rep__0 ;
  wire \Dout_reg[28]_rep ;
  wire \Dout_reg[30] ;
  wire [31:0]\Dout_reg[31] ;
  wire [26:0]\Dout_reg[31]_0 ;
  wire \Dout_reg[31]_rep ;
  wire [0:0]E;
  wire [31:0]ExtImm;
  wire [31:0]ExtImm_regI;
  wire \FSM_onehot_current_state_reg[13] ;
  wire FlagsWrite_in;
  wire [3:1]Flags_in;
  wire [31:1]Instr_in;
  wire [21:7]Instr_out_OBUF;
  wire [31:2]PCPlus4;
  wire [31:0]PCPlus4_regPCP4;
  wire [31:3]PCPlus8;
  wire PC_REG_n_0;
  wire PC_REG_n_1;
  wire PC_REG_n_10;
  wire PC_REG_n_11;
  wire PC_REG_n_12;
  wire PC_REG_n_13;
  wire PC_REG_n_14;
  wire PC_REG_n_15;
  wire PC_REG_n_16;
  wire PC_REG_n_17;
  wire PC_REG_n_18;
  wire PC_REG_n_19;
  wire PC_REG_n_2;
  wire PC_REG_n_20;
  wire PC_REG_n_21;
  wire PC_REG_n_22;
  wire PC_REG_n_23;
  wire PC_REG_n_3;
  wire PC_REG_n_4;
  wire PC_REG_n_5;
  wire PC_REG_n_6;
  wire PC_REG_n_7;
  wire PC_REG_n_8;
  wire PC_REG_n_9;
  wire [31:0]PC_next;
  wire [4:0]Q;
  wire [2:0]RA1_in;
  wire [3:0]RA2_in;
  wire [3:0]RA3_in;
  wire REG_A_n_0;
  wire REG_A_n_1;
  wire REG_A_n_2;
  wire REG_B_n_0;
  wire REG_B_n_1;
  wire REG_B_n_10;
  wire REG_B_n_11;
  wire REG_B_n_12;
  wire REG_B_n_13;
  wire REG_B_n_14;
  wire REG_B_n_15;
  wire REG_B_n_16;
  wire REG_B_n_17;
  wire REG_B_n_18;
  wire REG_B_n_19;
  wire REG_B_n_2;
  wire REG_B_n_20;
  wire REG_B_n_21;
  wire REG_B_n_22;
  wire REG_B_n_23;
  wire REG_B_n_24;
  wire REG_B_n_3;
  wire REG_B_n_4;
  wire REG_B_n_5;
  wire REG_B_n_57;
  wire REG_B_n_58;
  wire REG_B_n_59;
  wire REG_B_n_6;
  wire REG_B_n_60;
  wire REG_B_n_61;
  wire REG_B_n_62;
  wire REG_B_n_63;
  wire REG_B_n_64;
  wire REG_B_n_65;
  wire REG_B_n_66;
  wire REG_B_n_67;
  wire REG_B_n_68;
  wire REG_B_n_7;
  wire REG_B_n_8;
  wire REG_B_n_9;
  wire REG_FILE_n_0;
  wire REG_FILE_n_1;
  wire REG_FILE_n_10;
  wire REG_FILE_n_11;
  wire REG_FILE_n_12;
  wire REG_FILE_n_13;
  wire REG_FILE_n_14;
  wire REG_FILE_n_15;
  wire REG_FILE_n_16;
  wire REG_FILE_n_17;
  wire REG_FILE_n_18;
  wire REG_FILE_n_19;
  wire REG_FILE_n_2;
  wire REG_FILE_n_20;
  wire REG_FILE_n_21;
  wire REG_FILE_n_22;
  wire REG_FILE_n_23;
  wire REG_FILE_n_24;
  wire REG_FILE_n_25;
  wire REG_FILE_n_26;
  wire REG_FILE_n_27;
  wire REG_FILE_n_28;
  wire REG_FILE_n_29;
  wire REG_FILE_n_3;
  wire REG_FILE_n_30;
  wire REG_FILE_n_31;
  wire REG_FILE_n_32;
  wire REG_FILE_n_33;
  wire REG_FILE_n_34;
  wire REG_FILE_n_35;
  wire REG_FILE_n_36;
  wire REG_FILE_n_37;
  wire REG_FILE_n_38;
  wire REG_FILE_n_39;
  wire REG_FILE_n_4;
  wire REG_FILE_n_40;
  wire REG_FILE_n_41;
  wire REG_FILE_n_42;
  wire REG_FILE_n_43;
  wire REG_FILE_n_44;
  wire REG_FILE_n_45;
  wire REG_FILE_n_46;
  wire REG_FILE_n_47;
  wire REG_FILE_n_48;
  wire REG_FILE_n_49;
  wire REG_FILE_n_5;
  wire REG_FILE_n_50;
  wire REG_FILE_n_51;
  wire REG_FILE_n_52;
  wire REG_FILE_n_53;
  wire REG_FILE_n_54;
  wire REG_FILE_n_55;
  wire REG_FILE_n_56;
  wire REG_FILE_n_57;
  wire REG_FILE_n_58;
  wire REG_FILE_n_59;
  wire REG_FILE_n_6;
  wire REG_FILE_n_60;
  wire REG_FILE_n_61;
  wire REG_FILE_n_62;
  wire REG_FILE_n_63;
  wire REG_FILE_n_7;
  wire REG_FILE_n_8;
  wire REG_FILE_n_9;
  wire REG_IR_n_0;
  wire REG_IR_n_145;
  wire REG_IR_n_156;
  wire REG_IR_n_157;
  wire REG_IR_n_158;
  wire REG_IR_n_159;
  wire REG_IR_n_160;
  wire REG_IR_n_161;
  wire REG_IR_n_162;
  wire REG_IR_n_163;
  wire REG_IR_n_164;
  wire REG_IR_n_165;
  wire REG_IR_n_166;
  wire REG_IR_n_167;
  wire REG_IR_n_168;
  wire REG_IR_n_169;
  wire REG_IR_n_170;
  wire REG_IR_n_171;
  wire REG_IR_n_172;
  wire REG_IR_n_173;
  wire REG_IR_n_174;
  wire REG_IR_n_175;
  wire REG_IR_n_176;
  wire REG_IR_n_177;
  wire REG_IR_n_178;
  wire REG_IR_n_179;
  wire REG_IR_n_180;
  wire REG_IR_n_181;
  wire REG_IR_n_182;
  wire REG_IR_n_183;
  wire REG_IR_n_184;
  wire REG_IR_n_185;
  wire REG_IR_n_186;
  wire REG_IR_n_187;
  wire REG_IR_n_188;
  wire REG_IR_n_189;
  wire REG_IR_n_190;
  wire REG_IR_n_191;
  wire REG_IR_n_192;
  wire REG_IR_n_193;
  wire REG_IR_n_194;
  wire REG_IR_n_195;
  wire REG_IR_n_196;
  wire REG_IR_n_197;
  wire REG_IR_n_198;
  wire REG_IR_n_199;
  wire REG_IR_n_200;
  wire REG_IR_n_201;
  wire REG_IR_n_202;
  wire REG_IR_n_203;
  wire REG_IR_n_204;
  wire REG_IR_n_205;
  wire REG_IR_n_206;
  wire REG_IR_n_207;
  wire REG_IR_n_208;
  wire REG_IR_n_209;
  wire REG_IR_n_210;
  wire REG_IR_n_211;
  wire REG_IR_n_212;
  wire REG_IR_n_213;
  wire REG_IR_n_214;
  wire REG_IR_n_215;
  wire REG_IR_n_216;
  wire REG_IR_n_217;
  wire REG_IR_n_218;
  wire REG_IR_n_219;
  wire REG_IR_n_37;
  wire REG_IR_n_41;
  wire REG_IR_n_42;
  wire REG_IR_n_43;
  wire REG_IR_n_45;
  wire REG_IR_n_46;
  wire REG_IR_n_54;
  wire REG_I_n_0;
  wire REG_I_n_23;
  wire REG_I_n_24;
  wire REG_I_n_25;
  wire REG_I_n_26;
  wire REG_I_n_27;
  wire REG_I_n_28;
  wire REG_I_n_29;
  wire REG_WD_n_0;
  wire REG_WD_n_1;
  wire REG_WD_n_10;
  wire REG_WD_n_11;
  wire REG_WD_n_12;
  wire REG_WD_n_13;
  wire REG_WD_n_14;
  wire REG_WD_n_15;
  wire REG_WD_n_16;
  wire REG_WD_n_17;
  wire REG_WD_n_18;
  wire REG_WD_n_19;
  wire REG_WD_n_2;
  wire REG_WD_n_20;
  wire REG_WD_n_21;
  wire REG_WD_n_22;
  wire REG_WD_n_23;
  wire REG_WD_n_24;
  wire REG_WD_n_25;
  wire REG_WD_n_26;
  wire REG_WD_n_27;
  wire REG_WD_n_28;
  wire REG_WD_n_29;
  wire REG_WD_n_3;
  wire REG_WD_n_30;
  wire REG_WD_n_31;
  wire REG_WD_n_4;
  wire REG_WD_n_5;
  wire REG_WD_n_6;
  wire REG_WD_n_7;
  wire REG_WD_n_8;
  wire REG_WD_n_9;
  wire RESET_IBUF;
  wire [31:0]ReadData_in_regRD;
  wire RegWrite_in;
  wire [31:0]Result_out_OBUF;
  wire [31:0]SrcA;
  wire [31:0]SrcA_regA;
  wire [31:0]WD3_in;
  wire [31:0]WriteData_regB;

  ALU_n ALU
       (.\ALUResult_out_OBUF[0]_inst_i_6 ({REG_IR_n_168,REG_IR_n_169,REG_IR_n_170,REG_IR_n_171}),
        .\ALUResult_out_OBUF[12]_inst_i_5 ({REG_IR_n_204,REG_IR_n_205,REG_IR_n_206,REG_IR_n_207}),
        .\ALUResult_out_OBUF[12]_inst_i_5_0 ({REG_IR_n_180,REG_IR_n_181,REG_IR_n_182,REG_IR_n_183}),
        .\ALUResult_out_OBUF[16]_inst_i_5 ({REG_IR_n_208,REG_IR_n_209,REG_IR_n_210,REG_IR_n_211}),
        .\ALUResult_out_OBUF[16]_inst_i_5_0 ({REG_IR_n_212,REG_IR_n_213,REG_IR_n_214,REG_IR_n_215}),
        .\ALUResult_out_OBUF[20]_inst_i_4 ({REG_IR_n_192,REG_IR_n_193,REG_IR_n_194,REG_IR_n_195}),
        .\ALUResult_out_OBUF[20]_inst_i_4_0 ({REG_IR_n_216,REG_IR_n_217,REG_IR_n_218,REG_IR_n_219}),
        .\ALUResult_out_OBUF[24]_inst_i_3 ({REG_IR_n_160,REG_IR_n_161,REG_IR_n_162,REG_IR_n_163}),
        .\ALUResult_out_OBUF[24]_inst_i_3_0 ({REG_IR_n_184,REG_IR_n_185,REG_IR_n_186,REG_IR_n_187}),
        .\ALUResult_out_OBUF[28]_inst_i_4 ({REG_IR_n_164,REG_IR_n_165,REG_IR_n_166,REG_IR_n_167}),
        .\ALUResult_out_OBUF[28]_inst_i_4_0 ({REG_IR_n_188,REG_IR_n_189,REG_IR_n_190,REG_IR_n_191}),
        .\ALUResult_out_OBUF[4]_inst_i_4 ({REG_IR_n_196,REG_IR_n_197,REG_IR_n_198,REG_IR_n_199}),
        .\ALUResult_out_OBUF[4]_inst_i_4_0 ({REG_IR_n_172,REG_IR_n_173,REG_IR_n_174,REG_IR_n_175}),
        .\ALUResult_out_OBUF[8]_inst_i_5 ({REG_IR_n_200,REG_IR_n_201,REG_IR_n_202,REG_IR_n_203}),
        .\ALUResult_out_OBUF[8]_inst_i_5_0 ({REG_IR_n_176,REG_IR_n_177,REG_IR_n_178,REG_IR_n_179}),
        .CO(ALU_n_18),
        .DI(REG_A_n_2),
        .\Dout_reg[11] (ALU_n_42),
        .\Dout_reg[11]_0 (ALU_n_43),
        .\Dout_reg[11]_1 (ALU_n_44),
        .\Dout_reg[15] (ALU_n_45),
        .\Dout_reg[15]_0 (ALU_n_46),
        .\Dout_reg[15]_1 (ALU_n_47),
        .\Dout_reg[19] (ALU_n_48),
        .\Dout_reg[23] (ALU_n_49),
        .\Dout_reg[23]_0 (ALU_n_50),
        .\Dout_reg[23]_1 (ALU_n_51),
        .\Dout_reg[30] ({\ADD_SUB/S_Sub_in [30:24],\ADD_SUB/S_Sub_in [22],\ADD_SUB/S_Sub_in [19:17],\ADD_SUB/S_Sub_in [13],\ADD_SUB/S_Sub_in [11],\ADD_SUB/S_Sub_in [5:3],\ADD_SUB/S_Sub_in [1:0]}),
        .\Dout_reg[30]_0 ({\ADD_SUB/S_Add_in [30:24],\ADD_SUB/S_Add_in [22],\ADD_SUB/S_Add_in [19:17],\ADD_SUB/S_Add_in [13],\ADD_SUB/S_Add_in [11],\ADD_SUB/S_Add_in [5:3],\ADD_SUB/S_Add_in [1:0]}),
        .\Dout_reg[30]_1 (ALU_n_37),
        .\Dout_reg[30]_2 (ALU_n_38),
        .\Dout_reg[30]_3 (ALU_n_52),
        .\Dout_reg[3] (ALU_n_39),
        .\Dout_reg[3]_0 (REG_A_n_0),
        .\Dout_reg[3]_1 (ALUControl_out_OBUF[0]),
        .\Dout_reg[3]_2 (REG_A_n_1),
        .\Dout_reg[7] (ALU_n_40),
        .\Dout_reg[7]_0 (ALU_n_41),
        .Q(SrcA_regA[30:0]),
        .S({REG_IR_n_156,REG_IR_n_157,REG_IR_n_158,REG_IR_n_159}));
  RAM_ARRAY DATA_MEMORY
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .\Dout_reg[0] (\Dout_reg[27]_rep__0 [1]),
        .\Dout_reg[31] (ALUResult_in_regS),
        .Q(ADDR),
        .RAM_reg_0({REG_WD_n_0,REG_WD_n_1,REG_WD_n_2,REG_WD_n_3,REG_WD_n_4,REG_WD_n_5,REG_WD_n_6,REG_WD_n_7,REG_WD_n_8,REG_WD_n_9,REG_WD_n_10,REG_WD_n_11,REG_WD_n_12,REG_WD_n_13,REG_WD_n_14,REG_WD_n_15,REG_WD_n_16,REG_WD_n_17,REG_WD_n_18,REG_WD_n_19,REG_WD_n_20,REG_WD_n_21,REG_WD_n_22,REG_WD_n_23,REG_WD_n_24,REG_WD_n_25,REG_WD_n_26,REG_WD_n_27,REG_WD_n_28,REG_WD_n_29,REG_WD_n_30,REG_WD_n_31}),
        .RAM_reg_1(Q[2]),
        .RESET_IBUF(RESET_IBUF),
        .ReadData_in_regRD(ReadData_in_regRD),
        .Result_out_OBUF(Result_out_OBUF));
  ADDER_n INC_4_1
       (.DATA_OUT2({REG_FILE_n_32,REG_FILE_n_33,REG_FILE_n_34,REG_FILE_n_35,REG_FILE_n_36,REG_FILE_n_37,REG_FILE_n_38,REG_FILE_n_39,REG_FILE_n_40,REG_FILE_n_41,REG_FILE_n_42,REG_FILE_n_43,REG_FILE_n_44,REG_FILE_n_45,REG_FILE_n_46,REG_FILE_n_47,REG_FILE_n_48,REG_FILE_n_49,REG_FILE_n_50,REG_FILE_n_51,REG_FILE_n_52,REG_FILE_n_53,REG_FILE_n_54,REG_FILE_n_55,REG_FILE_n_56,REG_FILE_n_57,REG_FILE_n_58,REG_FILE_n_59,REG_FILE_n_60}),
        .\Dout_reg[31] (\Dout_reg[31] [31:3]),
        .\Dout_reg[3] (REG_IR_n_145),
        .PCPlus8(PCPlus8),
        .Q(PCPlus4_regPCP4[31:2]));
  ROM_ARRAY INSTR_MEMORY
       (.D({Instr_in[31:30],Instr_in[24:23],Instr_in[21],Instr_in[15:12],Instr_in[3:1]}),
        .Q({PC_REG_n_15,PC_REG_n_16,PC_REG_n_17,PC_REG_n_18,PC_REG_n_19,PC_REG_n_20}));
  REGrwe_n PC_REG
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D({PC_REG_n_0,PC_REG_n_1,PC_REG_n_2,PC_REG_n_3,PC_REG_n_4,PC_REG_n_5,PC_REG_n_6,PC_REG_n_7,PC_REG_n_8,PC_REG_n_9,PC_REG_n_10,PC_REG_n_11,PC_REG_n_12,PC_REG_n_13,PC_REG_n_14}),
        .\Dout_reg[31]_0 (PCPlus4),
        .\Dout_reg[31]_1 (PC_next),
        .\Dout_reg[4]_0 (PC_REG_n_23),
        .E(E),
        .Q({PC_REG_n_15,PC_REG_n_16,PC_REG_n_17,PC_REG_n_18,PC_REG_n_19,PC_REG_n_20,PC_REG_n_21,PC_REG_n_22}),
        .RESET_IBUF(RESET_IBUF));
  REGrwe_n_0 REG_A
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .CO(ALU_n_18),
        .D(SrcA),
        .DI(REG_A_n_2),
        .\Dout[3]_i_3 (ALU_n_37),
        .\Dout_reg[30]_0 (REG_A_n_0),
        .\Dout_reg[30]_1 (REG_A_n_1),
        .Q(SrcA_regA),
        .RESET_IBUF(RESET_IBUF));
  REGrwe_n_1 REG_B
       (.\ALUResult_out_OBUF[28]_inst_i_2_0 ({ExtImm_regI[31],ExtImm_regI[24:20],ExtImm_regI[18:14],ExtImm_regI[10:0]}),
        .\ALUResult_out_OBUF[28]_inst_i_2_1 (REG_IR_n_46),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .\Dout_reg[0]_0 (REG_B_n_16),
        .\Dout_reg[10]_0 (REG_B_n_19),
        .\Dout_reg[12]_0 (REG_B_n_10),
        .\Dout_reg[12]_1 (REG_B_n_57),
        .\Dout_reg[13]_0 (REG_B_n_24),
        .\Dout_reg[14]_0 (REG_B_n_20),
        .\Dout_reg[14]_1 (REG_B_n_23),
        .\Dout_reg[15]_0 (REG_B_n_21),
        .\Dout_reg[15]_1 (REG_B_n_62),
        .\Dout_reg[16]_0 (REG_I_n_26),
        .\Dout_reg[16]_1 (REG_I_n_27),
        .\Dout_reg[17]_0 (REG_B_n_63),
        .\Dout_reg[18]_0 (REG_B_n_7),
        .\Dout_reg[19]_0 (REG_B_n_8),
        .\Dout_reg[1]_0 (REG_B_n_59),
        .\Dout_reg[22]_0 (REG_B_n_64),
        .\Dout_reg[23]_0 (REG_B_n_65),
        .\Dout_reg[23]_1 (REG_I_n_29),
        .\Dout_reg[24]_0 (REG_B_n_6),
        .\Dout_reg[24]_1 (REG_B_n_66),
        .\Dout_reg[25]_0 (REG_B_n_1),
        .\Dout_reg[25]_1 (REG_B_n_4),
        .\Dout_reg[26]_0 (REG_B_n_3),
        .\Dout_reg[27]_0 (REG_B_n_67),
        .\Dout_reg[28]_0 (REG_IR_n_45),
        .\Dout_reg[2]_0 (REG_B_n_58),
        .\Dout_reg[2]_1 (REG_IR_n_41),
        .\Dout_reg[30]_0 (REG_B_n_68),
        .\Dout_reg[31]_0 (\Dout_reg[31] ),
        .\Dout_reg[3]_0 (REG_B_n_14),
        .\Dout_reg[3]_1 (REG_B_n_60),
        .\Dout_reg[6]_0 (REG_I_n_23),
        .\Dout_reg[7]_0 (REG_B_n_11),
        .\Dout_reg[7]_1 (REG_I_n_0),
        .\Dout_reg[7]_rep (REG_B_n_0),
        .\Dout_reg[7]_rep_0 (REG_B_n_2),
        .\Dout_reg[7]_rep_1 (REG_B_n_5),
        .\Dout_reg[7]_rep_2 (REG_B_n_9),
        .\Dout_reg[7]_rep_3 (REG_B_n_12),
        .\Dout_reg[7]_rep_4 (REG_B_n_13),
        .\Dout_reg[7]_rep_5 (REG_B_n_15),
        .\Dout_reg[7]_rep_6 (REG_B_n_17),
        .\Dout_reg[7]_rep_7 (REG_B_n_18),
        .\Dout_reg[7]_rep_8 (REG_B_n_22),
        .\Dout_reg[8]_0 (REG_B_n_61),
        .Instr_out_OBUF(Instr_out_OBUF[8:7]),
        .Q(WriteData_regB),
        .RESET_IBUF(RESET_IBUF));
  REGISTER_FILE_wR15 REG_FILE
       (.ADDRA({RA1_in[1],RA1_in[2],RA1_in[0]}),
        .ADDRD(RA3_in),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .DATA_IN(WD3_in),
        .DATA_OUT1({REG_FILE_n_0,REG_FILE_n_1,REG_FILE_n_2,REG_FILE_n_3,REG_FILE_n_4,REG_FILE_n_5,REG_FILE_n_6,REG_FILE_n_7,REG_FILE_n_8,REG_FILE_n_9,REG_FILE_n_10,REG_FILE_n_11,REG_FILE_n_12,REG_FILE_n_13,REG_FILE_n_14,REG_FILE_n_15,REG_FILE_n_16,REG_FILE_n_17,REG_FILE_n_18,REG_FILE_n_19,REG_FILE_n_20,REG_FILE_n_21,REG_FILE_n_22,REG_FILE_n_23,REG_FILE_n_24,REG_FILE_n_25,REG_FILE_n_26,REG_FILE_n_27,REG_FILE_n_28,REG_FILE_n_29,REG_FILE_n_30,REG_FILE_n_31}),
        .DATA_OUT2({REG_FILE_n_32,REG_FILE_n_33,REG_FILE_n_34,REG_FILE_n_35,REG_FILE_n_36,REG_FILE_n_37,REG_FILE_n_38,REG_FILE_n_39,REG_FILE_n_40,REG_FILE_n_41,REG_FILE_n_42,REG_FILE_n_43,REG_FILE_n_44,REG_FILE_n_45,REG_FILE_n_46,REG_FILE_n_47,REG_FILE_n_48,REG_FILE_n_49,REG_FILE_n_50,REG_FILE_n_51,REG_FILE_n_52,REG_FILE_n_53,REG_FILE_n_54,REG_FILE_n_55,REG_FILE_n_56,REG_FILE_n_57,REG_FILE_n_58,REG_FILE_n_59,REG_FILE_n_60,REG_FILE_n_61,REG_FILE_n_62,REG_FILE_n_63}),
        .\Dout_reg[1] (RA2_in),
        .RegWrite_in(RegWrite_in));
  REGrwe_n_2 REG_I
       (.\ALUResult_out_OBUF[23]_inst_i_3 ({WriteData_regB[22],WriteData_regB[20:16],WriteData_regB[14],WriteData_regB[8:5]}),
        .\ALUResult_out_OBUF[23]_inst_i_3_0 (REG_IR_n_46),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D({ExtImm[31],ExtImm[24:20],ExtImm[18:14],ExtImm[10:0]}),
        .\Dout_reg[16]_0 (REG_I_n_24),
        .\Dout_reg[16]_1 (REG_I_n_25),
        .\Dout_reg[18]_0 (REG_I_n_26),
        .\Dout_reg[20]_0 (REG_I_n_28),
        .\Dout_reg[20]_1 (REG_I_n_29),
        .\Dout_reg[21]_0 (REG_I_n_27),
        .\Dout_reg[5]_0 (REG_I_n_0),
        .\Dout_reg[8]_0 (REG_I_n_23),
        .Instr_out_OBUF(Instr_out_OBUF[8]),
        .Q({ExtImm_regI[31],ExtImm_regI[24:20],ExtImm_regI[18:14],ExtImm_regI[10:0]}),
        .RESET_IBUF(RESET_IBUF));
  REGrwe_n_3 REG_IR
       (.ADDRA({RA1_in[1],RA1_in[2],RA1_in[0]}),
        .ADDRD(RA3_in),
        .\ALUResult_out_OBUF[29]_inst_i_3_0 (WriteData_regB),
        .\ALUResult_out_OBUF[31]_inst_i_2_0 (ALU_n_52),
        .\ALUResult_out_OBUF[31]_inst_i_4_0 (REG_B_n_68),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D(ALUResult_out_OBUF),
        .DATA_IN(WD3_in),
        .DATA_OUT1({REG_FILE_n_0,REG_FILE_n_1,REG_FILE_n_2,REG_FILE_n_3,REG_FILE_n_4,REG_FILE_n_5,REG_FILE_n_6,REG_FILE_n_7,REG_FILE_n_8,REG_FILE_n_9,REG_FILE_n_10,REG_FILE_n_11,REG_FILE_n_12,REG_FILE_n_13,REG_FILE_n_14,REG_FILE_n_15,REG_FILE_n_16,REG_FILE_n_17,REG_FILE_n_18,REG_FILE_n_19,REG_FILE_n_20,REG_FILE_n_21,REG_FILE_n_22,REG_FILE_n_23,REG_FILE_n_24,REG_FILE_n_25,REG_FILE_n_26,REG_FILE_n_27,REG_FILE_n_28,REG_FILE_n_29,REG_FILE_n_30,REG_FILE_n_31}),
        .\Dout[1]_i_6_0 (REG_IR_n_37),
        .\Dout[1]_i_7_0 (REG_IR_n_43),
        .\Dout[1]_i_8_0 (REG_IR_n_42),
        .\Dout[1]_i_9_0 (REG_IR_n_0),
        .\Dout_reg[0]_0 (REG_B_n_60),
        .\Dout_reg[10] (ALU_n_44),
        .\Dout_reg[10]_0 (REG_B_n_10),
        .\Dout_reg[10]_1 (REG_B_n_11),
        .\Dout_reg[10]_2 (REG_B_n_61),
        .\Dout_reg[11] ({REG_IR_n_176,REG_IR_n_177,REG_IR_n_178,REG_IR_n_179}),
        .\Dout_reg[11]_0 ({REG_IR_n_200,REG_IR_n_201,REG_IR_n_202,REG_IR_n_203}),
        .\Dout_reg[11]_1 (REG_B_n_20),
        .\Dout_reg[12]_0 (REG_B_n_18),
        .\Dout_reg[12]_1 (ALU_n_45),
        .\Dout_reg[13]_0 (REG_B_n_19),
        .\Dout_reg[13]_1 (REG_B_n_21),
        .\Dout_reg[13]_2 (REG_I_n_24),
        .\Dout_reg[14]_0 (ALU_n_46),
        .\Dout_reg[15]_0 ({REG_IR_n_180,REG_IR_n_181,REG_IR_n_182,REG_IR_n_183}),
        .\Dout_reg[15]_1 ({REG_IR_n_204,REG_IR_n_205,REG_IR_n_206,REG_IR_n_207}),
        .\Dout_reg[15]_2 (ALU_n_47),
        .\Dout_reg[15]_3 (REG_B_n_57),
        .\Dout_reg[15]_4 (REG_B_n_24),
        .\Dout_reg[15]_rep_0 (REG_IR_n_145),
        .\Dout_reg[15]_rep_1 (RA2_in),
        .\Dout_reg[15]_rep_2 (\Dout_reg[15]_rep ),
        .\Dout_reg[16]_0 (REG_B_n_22),
        .\Dout_reg[16]_1 (ALU_n_48),
        .\Dout_reg[17] (REG_B_n_62),
        .\Dout_reg[17]_0 (REG_B_n_23),
        .\Dout_reg[17]_1 (REG_I_n_27),
        .\Dout_reg[17]_2 (REG_I_n_28),
        .\Dout_reg[19]_0 ({REG_IR_n_212,REG_IR_n_213,REG_IR_n_214,REG_IR_n_215}),
        .\Dout_reg[19]_1 (REG_B_n_63),
        .\Dout_reg[19]_2 (REG_I_n_25),
        .\Dout_reg[19]_rep_0 (\Dout_reg[19]_rep ),
        .\Dout_reg[19]_rep_rep_0 (SrcA),
        .\Dout_reg[20]_0 (ALU_n_49),
        .\Dout_reg[20]_1 (REG_B_n_7),
        .\Dout_reg[20]_rep_0 (\Dout_reg[27]_rep__0 [0]),
        .\Dout_reg[21]_0 ({REG_IR_n_208,REG_IR_n_209,REG_IR_n_210,REG_IR_n_211}),
        .\Dout_reg[21]_1 (REG_B_n_5),
        .\Dout_reg[21]_2 (ALU_n_50),
        .\Dout_reg[21]_rep_0 ({Instr_out_OBUF[21],Instr_out_OBUF[8:7]}),
        .\Dout_reg[22]_0 (REG_B_n_6),
        .\Dout_reg[22]_1 (REG_B_n_4),
        .\Dout_reg[22]_2 (REG_I_n_29),
        .\Dout_reg[22]_3 (REG_B_n_8),
        .\Dout_reg[23]_0 ({REG_IR_n_192,REG_IR_n_193,REG_IR_n_194,REG_IR_n_195}),
        .\Dout_reg[23]_1 ({REG_IR_n_216,REG_IR_n_217,REG_IR_n_218,REG_IR_n_219}),
        .\Dout_reg[23]_2 (REG_B_n_2),
        .\Dout_reg[23]_3 (ALU_n_51),
        .\Dout_reg[23]_rep_0 (ALUControl_out_OBUF[0]),
        .\Dout_reg[23]_rep_1 (REG_IR_n_45),
        .\Dout_reg[24]_0 (REG_B_n_3),
        .\Dout_reg[24]_1 (REG_B_n_67),
        .\Dout_reg[24]_rep_0 (D),
        .\Dout_reg[25]_0 (REG_B_n_64),
        .\Dout_reg[25]_rep_0 (ALUControl_out_OBUF[3]),
        .\Dout_reg[25]_rep_1 (ALUControl_out_OBUF[2]),
        .\Dout_reg[25]_rep_2 (\Dout_reg[25]_rep ),
        .\Dout_reg[25]_rep_3 (REG_IR_n_46),
        .\Dout_reg[26]_0 (REG_B_n_65),
        .\Dout_reg[26]_1 (REG_B_n_66),
        .\Dout_reg[26]_rep_0 (ALUControl_out_OBUF[1]),
        .\Dout_reg[26]_rep_1 (\Dout_reg[27]_rep__0 [1]),
        .\Dout_reg[26]_rep_2 (REG_IR_n_54),
        .\Dout_reg[27]_0 ({REG_IR_n_160,REG_IR_n_161,REG_IR_n_162,REG_IR_n_163}),
        .\Dout_reg[27]_1 ({REG_IR_n_184,REG_IR_n_185,REG_IR_n_186,REG_IR_n_187}),
        .\Dout_reg[27]_2 (REG_B_n_1),
        .\Dout_reg[27]_rep_0 (PC_REG_n_23),
        .\Dout_reg[27]_rep__0_0 (\Dout_reg[27]_rep__0 [2]),
        .\Dout_reg[27]_rep__0_1 ({ExtImm[31],ExtImm[24:20],ExtImm[18:14],ExtImm[10:0]}),
        .\Dout_reg[28]_0 (REG_B_n_0),
        .\Dout_reg[28]_1 ({ExtImm_regI[31],ExtImm_regI[24:20],ExtImm_regI[18:14],ExtImm_regI[10:0]}),
        .\Dout_reg[28]_rep_0 (\Dout_reg[28]_rep ),
        .\Dout_reg[2]_0 (REG_B_n_15),
        .\Dout_reg[2]_1 (ALU_n_39),
        .\Dout_reg[30]_0 ({\ADD_SUB/S_Add_in [30:24],\ADD_SUB/S_Add_in [22],\ADD_SUB/S_Add_in [19:17],\ADD_SUB/S_Add_in [13],\ADD_SUB/S_Add_in [11],\ADD_SUB/S_Add_in [5:3],\ADD_SUB/S_Add_in [1:0]}),
        .\Dout_reg[30]_1 ({\ADD_SUB/S_Sub_in [30:24],\ADD_SUB/S_Sub_in [22],\ADD_SUB/S_Sub_in [19:17],\ADD_SUB/S_Sub_in [13],\ADD_SUB/S_Sub_in [11],\ADD_SUB/S_Sub_in [5:3],\ADD_SUB/S_Sub_in [1:0]}),
        .\Dout_reg[31]_0 ({REG_IR_n_164,REG_IR_n_165,REG_IR_n_166,REG_IR_n_167}),
        .\Dout_reg[31]_1 ({REG_IR_n_188,REG_IR_n_189,REG_IR_n_190,REG_IR_n_191}),
        .\Dout_reg[31]_2 (PC_next[31]),
        .\Dout_reg[31]_3 (\Dout_reg[31]_0 ),
        .\Dout_reg[31]_4 ({Q[4:3],Q[0]}),
        .\Dout_reg[31]_5 (PCPlus4_regPCP4),
        .\Dout_reg[31]_6 (\Dout_reg[30] ),
        .\Dout_reg[31]_7 ({Instr_in[31:30],PC_REG_n_0,PC_REG_n_1,PC_REG_n_2,PC_REG_n_3,Instr_in[24:23],PC_REG_n_4,Instr_in[21],PC_REG_n_5,PC_REG_n_6,PC_REG_n_7,PC_REG_n_8,Instr_in[15:12],PC_REG_n_9,PC_REG_n_10,PC_REG_n_11,PC_REG_n_12,PC_REG_n_13,Instr_in[3:1],PC_REG_n_14}),
        .\Dout_reg[31]_rep_0 (\Dout_reg[31]_rep ),
        .\Dout_reg[3]_0 ({REG_IR_n_168,REG_IR_n_169,REG_IR_n_170,REG_IR_n_171}),
        .\Dout_reg[3]_1 (REG_B_n_16),
        .\Dout_reg[3]_2 (REG_B_n_59),
        .\Dout_reg[4]_0 (REG_B_n_58),
        .\Dout_reg[5]_0 (REG_B_n_14),
        .\Dout_reg[6]_0 (REG_B_n_13),
        .\Dout_reg[6]_1 (ALU_n_40),
        .\Dout_reg[7]_0 ({REG_IR_n_172,REG_IR_n_173,REG_IR_n_174,REG_IR_n_175}),
        .\Dout_reg[7]_1 ({REG_IR_n_196,REG_IR_n_197,REG_IR_n_198,REG_IR_n_199}),
        .\Dout_reg[7]_2 (REG_B_n_12),
        .\Dout_reg[7]_3 (ALU_n_41),
        .\Dout_reg[8]_0 (REG_B_n_17),
        .\Dout_reg[8]_1 (ALU_n_42),
        .\Dout_reg[8]_rep_0 (REG_IR_n_41),
        .\Dout_reg[9] (REG_B_n_9),
        .\Dout_reg[9]_0 (ALU_n_43),
        .\FSM_onehot_current_state_reg[13] (\FSM_onehot_current_state_reg[13] ),
        .\FSM_onehot_current_state_reg[2] (\Dout_reg[0] ),
        .Flags_in({Flags_in[3],Flags_in[1]}),
        .PCPlus8(PCPlus8),
        .Q(SrcA_regA),
        .RESET_IBUF(RESET_IBUF),
        .RF_reg_r2_0_15_30_31(ALUResult_in_regS),
        .ReadData_in_regRD(ReadData_in_regRD),
        .Result_out_OBUF(Result_out_OBUF[31]),
        .S({REG_IR_n_156,REG_IR_n_157,REG_IR_n_158,REG_IR_n_159}));
  REGrwe_n_4 REG_MA
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D(ALUResult_out_OBUF[6:2]),
        .\Dout_reg[2]_0 (Q[1]),
        .Q(ADDR),
        .RESET_IBUF(RESET_IBUF));
  REGrwe_n_5 REG_PCP4
       (.ALUResult_out_OBUF(ALUResult_out_OBUF[30:0]),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D({PCPlus4,PC_REG_n_21,PC_REG_n_22}),
        .DATA_OUT2({REG_FILE_n_61,REG_FILE_n_62,REG_FILE_n_63}),
        .\Dout_reg[0]_0 (REG_IR_n_145),
        .\Dout_reg[2]_0 (\Dout_reg[31] [2:0]),
        .\Dout_reg[30]_0 (PC_next[30:0]),
        .\Dout_reg[30]_1 (Q[4:3]),
        .\Dout_reg[30]_2 (\Dout_reg[30] ),
        .Q(PCPlus4_regPCP4),
        .RESET_IBUF(RESET_IBUF),
        .Result_out_OBUF(Result_out_OBUF[30:0]));
  REGrwe_n_6 REG_S
       (.ALUResult_out_OBUF(ALUResult_out_OBUF),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .Q(ALUResult_in_regS),
        .RESET_IBUF(RESET_IBUF));
  REGrwe_n_7 REG_WD
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .D(WriteData_regB),
        .Q({REG_WD_n_0,REG_WD_n_1,REG_WD_n_2,REG_WD_n_3,REG_WD_n_4,REG_WD_n_5,REG_WD_n_6,REG_WD_n_7,REG_WD_n_8,REG_WD_n_9,REG_WD_n_10,REG_WD_n_11,REG_WD_n_12,REG_WD_n_13,REG_WD_n_14,REG_WD_n_15,REG_WD_n_16,REG_WD_n_17,REG_WD_n_18,REG_WD_n_19,REG_WD_n_20,REG_WD_n_21,REG_WD_n_22,REG_WD_n_23,REG_WD_n_24,REG_WD_n_25,REG_WD_n_26,REG_WD_n_27,REG_WD_n_28,REG_WD_n_29,REG_WD_n_30,REG_WD_n_31}),
        .RESET_IBUF(RESET_IBUF));
  REGrwe_n__parameterized1 STATUS_REG
       (.ALUControl_out_OBUF(ALUControl_out_OBUF[1]),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .\Dout_reg[0]_0 (\Dout_reg[0] ),
        .\Dout_reg[0]_1 (\Dout_reg[0]_0 ),
        .\Dout_reg[1]_0 (REG_IR_n_37),
        .\Dout_reg[1]_1 (REG_IR_n_43),
        .\Dout_reg[1]_2 (REG_IR_n_42),
        .\Dout_reg[1]_3 (REG_IR_n_0),
        .\Dout_reg[3]_0 (REG_IR_n_54),
        .\Dout_reg[3]_1 (Instr_out_OBUF[21]),
        .\Dout_reg[3]_2 (ALU_n_38),
        .FlagsWrite_in(FlagsWrite_in),
        .Flags_in({Flags_in[3],Flags_in[1]}),
        .RESET_IBUF(RESET_IBUF));
endmodule

module FSM
   (\FSM_onehot_current_state_reg[1]_0 ,
    Q,
    E,
    \FSM_onehot_current_state_reg[13]_0 ,
    FlagsWrite_in,
    RegWrite_in,
    \FSM_onehot_current_state_reg[11]_0 ,
    Instr_out_OBUF,
    \FSM_onehot_current_state_reg[2]_0 ,
    \FSM_onehot_current_state_reg[10]_0 ,
    \Dout_reg[0] ,
    Flags_in,
    RESET_IBUF,
    D,
    CLK_IBUF_BUFG);
  output \FSM_onehot_current_state_reg[1]_0 ;
  output [4:0]Q;
  output [0:0]E;
  output \FSM_onehot_current_state_reg[13]_0 ;
  output FlagsWrite_in;
  output RegWrite_in;
  output \FSM_onehot_current_state_reg[11]_0 ;
  input [2:0]Instr_out_OBUF;
  input \FSM_onehot_current_state_reg[2]_0 ;
  input \FSM_onehot_current_state_reg[10]_0 ;
  input \Dout_reg[0] ;
  input [0:0]Flags_in;
  input RESET_IBUF;
  input [4:0]D;
  input CLK_IBUF_BUFG;

  wire CLK_IBUF_BUFG;
  wire [4:0]D;
  wire \Dout_reg[0] ;
  wire [0:0]E;
  wire \FSM_onehot_current_state[0]_i_1_n_0 ;
  wire \FSM_onehot_current_state[0]_i_2_n_0 ;
  wire \FSM_onehot_current_state[10]_i_1_n_0 ;
  wire \FSM_onehot_current_state[2]_i_1_n_0 ;
  wire \FSM_onehot_current_state[4]_i_1_n_0 ;
  wire \FSM_onehot_current_state[5]_i_1_n_0 ;
  wire \FSM_onehot_current_state[7]_i_1_n_0 ;
  wire \FSM_onehot_current_state[7]_i_2_n_0 ;
  wire \FSM_onehot_current_state[8]_i_1_n_0 ;
  wire \FSM_onehot_current_state[8]_i_2_n_0 ;
  wire \FSM_onehot_current_state[9]_i_1_n_0 ;
  wire \FSM_onehot_current_state_reg[10]_0 ;
  wire \FSM_onehot_current_state_reg[11]_0 ;
  wire \FSM_onehot_current_state_reg[13]_0 ;
  wire \FSM_onehot_current_state_reg[1]_0 ;
  wire \FSM_onehot_current_state_reg[2]_0 ;
  wire \FSM_onehot_current_state_reg_n_0_[10] ;
  wire \FSM_onehot_current_state_reg_n_0_[11] ;
  wire \FSM_onehot_current_state_reg_n_0_[12] ;
  wire \FSM_onehot_current_state_reg_n_0_[13] ;
  wire \FSM_onehot_current_state_reg_n_0_[1] ;
  wire \FSM_onehot_current_state_reg_n_0_[2] ;
  wire \FSM_onehot_current_state_reg_n_0_[4] ;
  wire \FSM_onehot_current_state_reg_n_0_[6] ;
  wire \FSM_onehot_current_state_reg_n_0_[8] ;
  wire FlagsWrite_in;
  wire [0:0]Flags_in;
  wire [2:0]Instr_out_OBUF;
  wire [4:0]Q;
  wire RESET_IBUF;
  wire RegWrite_in;

  (* \PinAttr:I4:HOLD_DETOUR  = "179" *) 
  LUT5 #(
    .INIT(32'h55575554)) 
    \Dout[0]_i_1__3 
       (.I0(\Dout_reg[0] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[11] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[10] ),
        .I3(Q[4]),
        .I4(Flags_in),
        .O(\FSM_onehot_current_state_reg[11]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    \Dout[31]_i_1 
       (.I0(\FSM_onehot_current_state_reg[13]_0 ),
        .I1(Q[3]),
        .I2(\FSM_onehot_current_state_reg_n_0_[2] ),
        .I3(Q[2]),
        .I4(\FSM_onehot_current_state_reg_n_0_[8] ),
        .I5(FlagsWrite_in),
        .O(E));
  LUT2 #(
    .INIT(4'h1)) 
    \Dout[31]_i_3 
       (.I0(\FSM_onehot_current_state_reg_n_0_[13] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[12] ),
        .O(\FSM_onehot_current_state_reg[13]_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \Dout[3]_i_4 
       (.I0(\FSM_onehot_current_state_reg_n_0_[11] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[10] ),
        .I2(Q[4]),
        .O(FlagsWrite_in));
  LUT6 #(
    .INIT(64'h00000000EE0E0EAA)) 
    \FSM_onehot_current_state[0]_i_1 
       (.I0(\FSM_onehot_current_state[0]_i_2_n_0 ),
        .I1(\FSM_onehot_current_state_reg[1]_0 ),
        .I2(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I3(Instr_out_OBUF[2]),
        .I4(Instr_out_OBUF[1]),
        .I5(Q[0]),
        .O(\FSM_onehot_current_state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_onehot_current_state[0]_i_2 
       (.I0(\FSM_onehot_current_state_reg_n_0_[4] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I2(Q[1]),
        .I3(\FSM_onehot_current_state_reg_n_0_[1] ),
        .O(\FSM_onehot_current_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \FSM_onehot_current_state[10]_i_1 
       (.I0(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I1(Instr_out_OBUF[0]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I5(\FSM_onehot_current_state_reg[10]_0 ),
        .O(\FSM_onehot_current_state[10]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_onehot_current_state[13]_i_2 
       (.I0(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I1(\FSM_onehot_current_state_reg[2]_0 ),
        .O(\FSM_onehot_current_state_reg[1]_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_onehot_current_state[2]_i_1 
       (.I0(\FSM_onehot_current_state_reg[2]_0 ),
        .I1(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I2(Q[0]),
        .O(\FSM_onehot_current_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \FSM_onehot_current_state[4]_i_1 
       (.I0(Instr_out_OBUF[0]),
        .I1(Q[1]),
        .I2(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I3(Q[0]),
        .O(\FSM_onehot_current_state[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \FSM_onehot_current_state[5]_i_1 
       (.I0(Instr_out_OBUF[0]),
        .I1(Q[1]),
        .I2(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I3(Q[0]),
        .O(\FSM_onehot_current_state[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h20A8)) 
    \FSM_onehot_current_state[7]_i_1 
       (.I0(\FSM_onehot_current_state[7]_i_2_n_0 ),
        .I1(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[4] ),
        .I3(Instr_out_OBUF[0]),
        .O(\FSM_onehot_current_state[7]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0002)) 
    \FSM_onehot_current_state[7]_i_2 
       (.I0(\FSM_onehot_current_state_reg[10]_0 ),
        .I1(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\FSM_onehot_current_state[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h20A8)) 
    \FSM_onehot_current_state[8]_i_1 
       (.I0(\FSM_onehot_current_state[8]_i_2_n_0 ),
        .I1(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[4] ),
        .I3(Instr_out_OBUF[0]),
        .O(\FSM_onehot_current_state[8]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_onehot_current_state[8]_i_2 
       (.I0(\FSM_onehot_current_state_reg[10]_0 ),
        .I1(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\FSM_onehot_current_state[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \FSM_onehot_current_state[9]_i_1 
       (.I0(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I1(Instr_out_OBUF[0]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\FSM_onehot_current_state_reg_n_0_[1] ),
        .I5(\FSM_onehot_current_state_reg[10]_0 ),
        .O(\FSM_onehot_current_state[9]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_current_state_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[0]_i_1_n_0 ),
        .Q(Q[0]),
        .S(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[10]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[10] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[2]),
        .Q(\FSM_onehot_current_state_reg_n_0_[11] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[3]),
        .Q(\FSM_onehot_current_state_reg_n_0_[12] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[4]),
        .Q(\FSM_onehot_current_state_reg_n_0_[13] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(Q[0]),
        .Q(\FSM_onehot_current_state_reg_n_0_[1] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[2]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[2] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[1]),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[4]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[4] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[5]_i_1_n_0 ),
        .Q(Q[2]),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[1]),
        .Q(\FSM_onehot_current_state_reg_n_0_[6] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[7]_i_1_n_0 ),
        .Q(Q[3]),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[8]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[8] ),
        .R(RESET_IBUF));
  (* FSM_ENCODED_STATES = "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[9]_i_1_n_0 ),
        .Q(Q[4]),
        .R(RESET_IBUF));
  LUT3 #(
    .INIT(8'hFE)) 
    RF_reg_r1_0_15_0_5_i_1
       (.I0(\FSM_onehot_current_state_reg_n_0_[10] ),
        .I1(\FSM_onehot_current_state_reg_n_0_[8] ),
        .I2(\FSM_onehot_current_state_reg_n_0_[13] ),
        .O(RegWrite_in));
endmodule

module RAM_ARRAY
   (ReadData_in_regRD,
    Result_out_OBUF,
    CLK_IBUF_BUFG,
    RESET_IBUF,
    Q,
    RAM_reg_0,
    RAM_reg_1,
    \Dout_reg[0] ,
    \Dout_reg[31] );
  output [31:0]ReadData_in_regRD;
  output [31:0]Result_out_OBUF;
  input CLK_IBUF_BUFG;
  input RESET_IBUF;
  input [4:0]Q;
  input [31:0]RAM_reg_0;
  input [0:0]RAM_reg_1;
  input \Dout_reg[0] ;
  input [31:0]\Dout_reg[31] ;

  wire CLK_IBUF_BUFG;
  wire \Dout_reg[0] ;
  wire [31:0]\Dout_reg[31] ;
  wire [4:0]Q;
  wire [31:0]RAM_reg_0;
  wire [0:0]RAM_reg_1;
  wire RESET_IBUF;
  wire [31:0]ReadData_in_regRD;
  wire [31:0]Result_out_OBUF;
  wire [15:14]NLW_RAM_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_RAM_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "DATAPATH/DATA_MEMORY/RAM" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "31" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_ext_slice_begin = "18" *) 
  (* ram_ext_slice_end = "31" *) 
  (* ram_offset = "992" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    RAM_reg
       (.ADDRARDADDR({1'b0,1'b1,1'b1,1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,Q,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(CLK_IBUF_BUFG),
        .CLKBWRCLK(CLK_IBUF_BUFG),
        .DIADI(RAM_reg_0[15:0]),
        .DIBDI({1'b1,1'b1,RAM_reg_0[31:18]}),
        .DIPADIP(RAM_reg_0[17:16]),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(ReadData_in_regRD[15:0]),
        .DOBDO({NLW_RAM_reg_DOBDO_UNCONNECTED[15:14],ReadData_in_regRD[31:18]}),
        .DOPADOP(ReadData_in_regRD[17:16]),
        .DOPBDOP(NLW_RAM_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(RESET_IBUF),
        .RSTRAMB(RESET_IBUF),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({RAM_reg_1,RAM_reg_1}),
        .WEBWE({1'b0,1'b0,RAM_reg_1,RAM_reg_1}));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[0]_inst_i_1 
       (.I0(ReadData_in_regRD[0]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [0]),
        .O(Result_out_OBUF[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[10]_inst_i_1 
       (.I0(ReadData_in_regRD[10]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [10]),
        .O(Result_out_OBUF[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[11]_inst_i_1 
       (.I0(ReadData_in_regRD[11]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [11]),
        .O(Result_out_OBUF[11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[12]_inst_i_1 
       (.I0(ReadData_in_regRD[12]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [12]),
        .O(Result_out_OBUF[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[13]_inst_i_1 
       (.I0(ReadData_in_regRD[13]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [13]),
        .O(Result_out_OBUF[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[14]_inst_i_1 
       (.I0(ReadData_in_regRD[14]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [14]),
        .O(Result_out_OBUF[14]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[15]_inst_i_1 
       (.I0(ReadData_in_regRD[15]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [15]),
        .O(Result_out_OBUF[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[16]_inst_i_1 
       (.I0(ReadData_in_regRD[16]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [16]),
        .O(Result_out_OBUF[16]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[17]_inst_i_1 
       (.I0(ReadData_in_regRD[17]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [17]),
        .O(Result_out_OBUF[17]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[18]_inst_i_1 
       (.I0(ReadData_in_regRD[18]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [18]),
        .O(Result_out_OBUF[18]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[19]_inst_i_1 
       (.I0(ReadData_in_regRD[19]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [19]),
        .O(Result_out_OBUF[19]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[1]_inst_i_1 
       (.I0(ReadData_in_regRD[1]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [1]),
        .O(Result_out_OBUF[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[20]_inst_i_1 
       (.I0(ReadData_in_regRD[20]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [20]),
        .O(Result_out_OBUF[20]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[21]_inst_i_1 
       (.I0(ReadData_in_regRD[21]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [21]),
        .O(Result_out_OBUF[21]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[22]_inst_i_1 
       (.I0(ReadData_in_regRD[22]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [22]),
        .O(Result_out_OBUF[22]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[23]_inst_i_1 
       (.I0(ReadData_in_regRD[23]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [23]),
        .O(Result_out_OBUF[23]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[24]_inst_i_1 
       (.I0(ReadData_in_regRD[24]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [24]),
        .O(Result_out_OBUF[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[25]_inst_i_1 
       (.I0(ReadData_in_regRD[25]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [25]),
        .O(Result_out_OBUF[25]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[26]_inst_i_1 
       (.I0(ReadData_in_regRD[26]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [26]),
        .O(Result_out_OBUF[26]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[27]_inst_i_1 
       (.I0(ReadData_in_regRD[27]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [27]),
        .O(Result_out_OBUF[27]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[28]_inst_i_1 
       (.I0(ReadData_in_regRD[28]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [28]),
        .O(Result_out_OBUF[28]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[29]_inst_i_1 
       (.I0(ReadData_in_regRD[29]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [29]),
        .O(Result_out_OBUF[29]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[2]_inst_i_1 
       (.I0(ReadData_in_regRD[2]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [2]),
        .O(Result_out_OBUF[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[30]_inst_i_1 
       (.I0(ReadData_in_regRD[30]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [30]),
        .O(Result_out_OBUF[30]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[31]_inst_i_1 
       (.I0(ReadData_in_regRD[31]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [31]),
        .O(Result_out_OBUF[31]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[3]_inst_i_1 
       (.I0(ReadData_in_regRD[3]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [3]),
        .O(Result_out_OBUF[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[4]_inst_i_1 
       (.I0(ReadData_in_regRD[4]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [4]),
        .O(Result_out_OBUF[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[5]_inst_i_1 
       (.I0(ReadData_in_regRD[5]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [5]),
        .O(Result_out_OBUF[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[6]_inst_i_1 
       (.I0(ReadData_in_regRD[6]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [6]),
        .O(Result_out_OBUF[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[7]_inst_i_1 
       (.I0(ReadData_in_regRD[7]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [7]),
        .O(Result_out_OBUF[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[8]_inst_i_1 
       (.I0(ReadData_in_regRD[8]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [8]),
        .O(Result_out_OBUF[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Result_out_OBUF[9]_inst_i_1 
       (.I0(ReadData_in_regRD[9]),
        .I1(\Dout_reg[0] ),
        .I2(\Dout_reg[31] [9]),
        .O(Result_out_OBUF[9]));
endmodule

module REGISTER_FILE_n
   (DATA_OUT1,
    DATA_OUT2,
    CLK_IBUF_BUFG,
    RegWrite_in,
    DATA_IN,
    ADDRA,
    ADDRD,
    \Dout_reg[1] );
  output [31:0]DATA_OUT1;
  output [31:0]DATA_OUT2;
  input CLK_IBUF_BUFG;
  input RegWrite_in;
  input [31:0]DATA_IN;
  input [2:0]ADDRA;
  input [3:0]ADDRD;
  input [3:0]\Dout_reg[1] ;

  wire [2:0]ADDRA;
  wire [3:0]ADDRD;
  wire CLK_IBUF_BUFG;
  wire [31:0]DATA_IN;
  wire [31:0]DATA_OUT1;
  wire [31:0]DATA_OUT2;
  wire [3:0]\Dout_reg[1] ;
  wire RegWrite_in;
  wire [1:0]NLW_RF_reg_r1_0_15_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_6_11_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_6_11_DOD_UNCONNECTED;

  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M_UNIQ_BASE_ RF_reg_r1_0_15_0_5
       (.ADDRA({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRB({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRC({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[1:0]),
        .DIB(DATA_IN[3:2]),
        .DIC(DATA_IN[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[1:0]),
        .DOB(DATA_OUT1[3:2]),
        .DOC(DATA_OUT1[5:4]),
        .DOD(NLW_RF_reg_r1_0_15_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "17" *) 
  RAM32M_HD1 RF_reg_r1_0_15_12_17
       (.ADDRA({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRB({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRC({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[13:12]),
        .DIB(DATA_IN[15:14]),
        .DIC(DATA_IN[17:16]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[13:12]),
        .DOB(DATA_OUT1[15:14]),
        .DOC(DATA_OUT1[17:16]),
        .DOD(NLW_RF_reg_r1_0_15_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAM32M_HD2 RF_reg_r1_0_15_18_23
       (.ADDRA({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRB({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRC({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[19:18]),
        .DIB(DATA_IN[21:20]),
        .DIC(DATA_IN[23:22]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[19:18]),
        .DOB(DATA_OUT1[21:20]),
        .DOC(DATA_OUT1[23:22]),
        .DOD(NLW_RF_reg_r1_0_15_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "29" *) 
  RAM32M_HD3 RF_reg_r1_0_15_24_29
       (.ADDRA({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRB({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRC({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[25:24]),
        .DIB(DATA_IN[27:26]),
        .DIC(DATA_IN[29:28]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[25:24]),
        .DOB(DATA_OUT1[27:26]),
        .DOC(DATA_OUT1[29:28]),
        .DOD(NLW_RF_reg_r1_0_15_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "31" *) 
  RAM32M_HD4 RF_reg_r1_0_15_30_31
       (.ADDRA({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRB({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRC({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[31:30]),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[31:30]),
        .DOB(NLW_RF_reg_r1_0_15_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_RF_reg_r1_0_15_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_RF_reg_r1_0_15_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M_HD5 RF_reg_r1_0_15_6_11
       (.ADDRA({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRB({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRC({1'b0,ADDRA[2:1],ADDRA[2],ADDRA[0]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[7:6]),
        .DIB(DATA_IN[9:8]),
        .DIC(DATA_IN[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[7:6]),
        .DOB(DATA_OUT1[9:8]),
        .DOC(DATA_OUT1[11:10]),
        .DOD(NLW_RF_reg_r1_0_15_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M_HD6 RF_reg_r2_0_15_0_5
       (.ADDRA({1'b0,\Dout_reg[1] }),
        .ADDRB({1'b0,\Dout_reg[1] }),
        .ADDRC({1'b0,\Dout_reg[1] }),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[1:0]),
        .DIB(DATA_IN[3:2]),
        .DIC(DATA_IN[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[1:0]),
        .DOB(DATA_OUT2[3:2]),
        .DOC(DATA_OUT2[5:4]),
        .DOD(NLW_RF_reg_r2_0_15_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "17" *) 
  RAM32M_HD7 RF_reg_r2_0_15_12_17
       (.ADDRA({1'b0,\Dout_reg[1] }),
        .ADDRB({1'b0,\Dout_reg[1] }),
        .ADDRC({1'b0,\Dout_reg[1] }),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[13:12]),
        .DIB(DATA_IN[15:14]),
        .DIC(DATA_IN[17:16]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[13:12]),
        .DOB(DATA_OUT2[15:14]),
        .DOC(DATA_OUT2[17:16]),
        .DOD(NLW_RF_reg_r2_0_15_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAM32M_HD8 RF_reg_r2_0_15_18_23
       (.ADDRA({1'b0,\Dout_reg[1] }),
        .ADDRB({1'b0,\Dout_reg[1] }),
        .ADDRC({1'b0,\Dout_reg[1] }),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[19:18]),
        .DIB(DATA_IN[21:20]),
        .DIC(DATA_IN[23:22]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[19:18]),
        .DOB(DATA_OUT2[21:20]),
        .DOC(DATA_OUT2[23:22]),
        .DOD(NLW_RF_reg_r2_0_15_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "29" *) 
  RAM32M_HD9 RF_reg_r2_0_15_24_29
       (.ADDRA({1'b0,\Dout_reg[1] }),
        .ADDRB({1'b0,\Dout_reg[1] }),
        .ADDRC({1'b0,\Dout_reg[1] }),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[25:24]),
        .DIB(DATA_IN[27:26]),
        .DIC(DATA_IN[29:28]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[25:24]),
        .DOB(DATA_OUT2[27:26]),
        .DOC(DATA_OUT2[29:28]),
        .DOD(NLW_RF_reg_r2_0_15_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "31" *) 
  RAM32M_HD10 RF_reg_r2_0_15_30_31
       (.ADDRA({1'b0,\Dout_reg[1] }),
        .ADDRB({1'b0,\Dout_reg[1] }),
        .ADDRC({1'b0,\Dout_reg[1] }),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[31:30]),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[31:30]),
        .DOB(NLW_RF_reg_r2_0_15_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_RF_reg_r2_0_15_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_RF_reg_r2_0_15_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M_HD11 RF_reg_r2_0_15_6_11
       (.ADDRA({1'b0,\Dout_reg[1] }),
        .ADDRB({1'b0,\Dout_reg[1] }),
        .ADDRC({1'b0,\Dout_reg[1] }),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[7:6]),
        .DIB(DATA_IN[9:8]),
        .DIC(DATA_IN[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[7:6]),
        .DOB(DATA_OUT2[9:8]),
        .DOC(DATA_OUT2[11:10]),
        .DOD(NLW_RF_reg_r2_0_15_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK_IBUF_BUFG),
        .WE(RegWrite_in));
endmodule

module REGISTER_FILE_wR15
   (DATA_OUT1,
    DATA_OUT2,
    CLK_IBUF_BUFG,
    RegWrite_in,
    DATA_IN,
    ADDRA,
    ADDRD,
    \Dout_reg[1] );
  output [31:0]DATA_OUT1;
  output [31:0]DATA_OUT2;
  input CLK_IBUF_BUFG;
  input RegWrite_in;
  input [31:0]DATA_IN;
  input [2:0]ADDRA;
  input [3:0]ADDRD;
  input [3:0]\Dout_reg[1] ;

  wire [2:0]ADDRA;
  wire [3:0]ADDRD;
  wire CLK_IBUF_BUFG;
  wire [31:0]DATA_IN;
  wire [31:0]DATA_OUT1;
  wire [31:0]DATA_OUT2;
  wire [3:0]\Dout_reg[1] ;
  wire RegWrite_in;

  REGISTER_FILE_n REG_FILE_IN
       (.ADDRA(ADDRA),
        .ADDRD(ADDRD),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .DATA_IN(DATA_IN),
        .DATA_OUT1(DATA_OUT1),
        .DATA_OUT2(DATA_OUT2),
        .\Dout_reg[1] (\Dout_reg[1] ),
        .RegWrite_in(RegWrite_in));
endmodule

module REGrwe_n
   (D,
    Q,
    \Dout_reg[4]_0 ,
    \Dout_reg[31]_0 ,
    RESET_IBUF,
    E,
    \Dout_reg[31]_1 ,
    CLK_IBUF_BUFG);
  output [14:0]D;
  output [7:0]Q;
  output \Dout_reg[4]_0 ;
  output [29:0]\Dout_reg[31]_0 ;
  input RESET_IBUF;
  input [0:0]E;
  input [31:0]\Dout_reg[31]_1 ;
  input CLK_IBUF_BUFG;

  wire CLK_IBUF_BUFG;
  wire [14:0]D;
  wire \Dout_reg[10]_i_1_n_0 ;
  wire \Dout_reg[14]_i_1_n_0 ;
  wire \Dout_reg[18]_i_1_n_0 ;
  wire \Dout_reg[22]_i_1_n_0 ;
  wire \Dout_reg[26]_i_1_n_0 ;
  wire \Dout_reg[30]_i_1_n_0 ;
  wire [29:0]\Dout_reg[31]_0 ;
  wire [31:0]\Dout_reg[31]_1 ;
  wire \Dout_reg[4]_0 ;
  wire \Dout_reg[6]_i_1_n_0 ;
  wire \Dout_reg_n_0_[10] ;
  wire \Dout_reg_n_0_[11] ;
  wire \Dout_reg_n_0_[12] ;
  wire \Dout_reg_n_0_[13] ;
  wire \Dout_reg_n_0_[14] ;
  wire \Dout_reg_n_0_[15] ;
  wire \Dout_reg_n_0_[16] ;
  wire \Dout_reg_n_0_[17] ;
  wire \Dout_reg_n_0_[18] ;
  wire \Dout_reg_n_0_[19] ;
  wire \Dout_reg_n_0_[20] ;
  wire \Dout_reg_n_0_[21] ;
  wire \Dout_reg_n_0_[22] ;
  wire \Dout_reg_n_0_[23] ;
  wire \Dout_reg_n_0_[24] ;
  wire \Dout_reg_n_0_[25] ;
  wire \Dout_reg_n_0_[26] ;
  wire \Dout_reg_n_0_[27] ;
  wire \Dout_reg_n_0_[28] ;
  wire \Dout_reg_n_0_[29] ;
  wire \Dout_reg_n_0_[30] ;
  wire \Dout_reg_n_0_[31] ;
  wire \Dout_reg_n_0_[8] ;
  wire \Dout_reg_n_0_[9] ;
  wire [0:0]E;
  wire [7:0]Q;
  wire RESET_IBUF;
  wire [2:0]\NLW_Dout_reg[10]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_Dout_reg[14]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_Dout_reg[18]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_Dout_reg[22]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_Dout_reg[26]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_Dout_reg[30]_i_1_CO_UNCONNECTED ;
  wire [3:0]\NLW_Dout_reg[31]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_Dout_reg[31]_i_1_O_UNCONNECTED ;
  wire [2:0]\NLW_Dout_reg[6]_i_1_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h00000000C0290149)) 
    \Dout[0]_i_1__2 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[4]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h00000000060007B8)) 
    \Dout[16]_i_1__2 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[4]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(D[6]));
  LUT6 #(
    .INIT(64'h00000000C6A60000)) 
    \Dout[18]_i_1__2 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(D[7]));
  LUT6 #(
    .INIT(64'h0000000002200000)) 
    \Dout[19]_i_1__1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(D[8]));
  LUT6 #(
    .INIT(64'h0000000004406240)) 
    \Dout[20]_i_1__2 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[2]),
        .I3(Q[6]),
        .I4(Q[3]),
        .I5(Q[7]),
        .O(D[9]));
  LUT6 #(
    .INIT(64'h0000000006400836)) 
    \Dout[22]_i_1__2 
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(D[10]));
  LUT6 #(
    .INIT(64'h00000000001DE91D)) 
    \Dout[25]_i_1__1 
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(Q[5]),
        .I3(Q[6]),
        .I4(Q[4]),
        .I5(Q[7]),
        .O(D[11]));
  LUT6 #(
    .INIT(64'h0000000044060000)) 
    \Dout[26]_i_1__1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(D[12]));
  LUT6 #(
    .INIT(64'h0000000040000200)) 
    \Dout[27]_i_1__1 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(D[13]));
  LUT6 #(
    .INIT(64'h0000000040000200)) 
    \Dout[27]_rep_i_1 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(\Dout_reg[4]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000400)) 
    \Dout[28]_i_1__1 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[4]),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(Q[7]),
        .O(D[14]));
  LUT1 #(
    .INIT(2'h1)) 
    \Dout[2]_i_1__2 
       (.I0(Q[2]),
        .O(\Dout_reg[31]_0 [0]));
  LUT6 #(
    .INIT(64'h0000000000001005)) 
    \Dout[4]_i_1__2 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[4]),
        .I5(Q[7]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'h00000000000281D5)) 
    \Dout[5]_i_1__2 
       (.I0(Q[2]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h0000000000140141)) 
    \Dout[6]_i_1__2 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h00000000001281D5)) 
    \Dout[7]_i_1__2 
       (.I0(Q[2]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[5]),
        .I4(Q[6]),
        .I5(Q[7]),
        .O(D[4]));
  LUT6 #(
    .INIT(64'h0000000000400000)) 
    \Dout[8]_i_1__2 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[4]),
        .I5(Q[7]),
        .O(D[5]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [0]),
        .Q(Q[0]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [10]),
        .Q(\Dout_reg_n_0_[10] ),
        .R(RESET_IBUF));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \Dout_reg[10]_i_1 
       (.CI(\Dout_reg[6]_i_1_n_0 ),
        .CO({\Dout_reg[10]_i_1_n_0 ,\NLW_Dout_reg[10]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\Dout_reg_n_0_[10] ,\Dout_reg_n_0_[9] ,\Dout_reg_n_0_[8] ,Q[7]}),
        .O(\Dout_reg[31]_0 [8:5]),
        .S({\Dout_reg_n_0_[10] ,\Dout_reg_n_0_[9] ,\Dout_reg_n_0_[8] ,Q[7]}));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [11]),
        .Q(\Dout_reg_n_0_[11] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [12]),
        .Q(\Dout_reg_n_0_[12] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [13]),
        .Q(\Dout_reg_n_0_[13] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [14]),
        .Q(\Dout_reg_n_0_[14] ),
        .R(RESET_IBUF));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \Dout_reg[14]_i_1 
       (.CI(\Dout_reg[10]_i_1_n_0 ),
        .CO({\Dout_reg[14]_i_1_n_0 ,\NLW_Dout_reg[14]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\Dout_reg_n_0_[14] ,\Dout_reg_n_0_[13] ,\Dout_reg_n_0_[12] ,\Dout_reg_n_0_[11] }),
        .O(\Dout_reg[31]_0 [12:9]),
        .S({\Dout_reg_n_0_[14] ,\Dout_reg_n_0_[13] ,\Dout_reg_n_0_[12] ,\Dout_reg_n_0_[11] }));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [15]),
        .Q(\Dout_reg_n_0_[15] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [16]),
        .Q(\Dout_reg_n_0_[16] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [17]),
        .Q(\Dout_reg_n_0_[17] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [18]),
        .Q(\Dout_reg_n_0_[18] ),
        .R(RESET_IBUF));
  (* OPT_MODIFIED = "SWEEP" *) 
  (* \PinAttr:DI[0]:HOLD_DETOUR  = "154" *) 
  (* \PinAttr:S[0]:HOLD_DETOUR  = "154" *) 
  CARRY4 \Dout_reg[18]_i_1 
       (.CI(\Dout_reg[14]_i_1_n_0 ),
        .CO({\Dout_reg[18]_i_1_n_0 ,\NLW_Dout_reg[18]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\Dout_reg_n_0_[18] ,\Dout_reg_n_0_[17] ,\Dout_reg_n_0_[16] ,\Dout_reg_n_0_[15] }),
        .O(\Dout_reg[31]_0 [16:13]),
        .S({\Dout_reg_n_0_[18] ,\Dout_reg_n_0_[17] ,\Dout_reg_n_0_[16] ,\Dout_reg_n_0_[15] }));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [19]),
        .Q(\Dout_reg_n_0_[19] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [1]),
        .Q(Q[1]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [20]),
        .Q(\Dout_reg_n_0_[20] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [21]),
        .Q(\Dout_reg_n_0_[21] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [22]),
        .Q(\Dout_reg_n_0_[22] ),
        .R(RESET_IBUF));
  (* OPT_MODIFIED = "SWEEP" *) 
  (* \PinAttr:DI[1]:HOLD_DETOUR  = "157" *) 
  (* \PinAttr:S[1]:HOLD_DETOUR  = "157" *) 
  CARRY4 \Dout_reg[22]_i_1 
       (.CI(\Dout_reg[18]_i_1_n_0 ),
        .CO({\Dout_reg[22]_i_1_n_0 ,\NLW_Dout_reg[22]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\Dout_reg_n_0_[22] ,\Dout_reg_n_0_[21] ,\Dout_reg_n_0_[20] ,\Dout_reg_n_0_[19] }),
        .O(\Dout_reg[31]_0 [20:17]),
        .S({\Dout_reg_n_0_[22] ,\Dout_reg_n_0_[21] ,\Dout_reg_n_0_[20] ,\Dout_reg_n_0_[19] }));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [23]),
        .Q(\Dout_reg_n_0_[23] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [24]),
        .Q(\Dout_reg_n_0_[24] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [25]),
        .Q(\Dout_reg_n_0_[25] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [26]),
        .Q(\Dout_reg_n_0_[26] ),
        .R(RESET_IBUF));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \Dout_reg[26]_i_1 
       (.CI(\Dout_reg[22]_i_1_n_0 ),
        .CO({\Dout_reg[26]_i_1_n_0 ,\NLW_Dout_reg[26]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\Dout_reg_n_0_[26] ,\Dout_reg_n_0_[25] ,\Dout_reg_n_0_[24] ,\Dout_reg_n_0_[23] }),
        .O(\Dout_reg[31]_0 [24:21]),
        .S({\Dout_reg_n_0_[26] ,\Dout_reg_n_0_[25] ,\Dout_reg_n_0_[24] ,\Dout_reg_n_0_[23] }));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [27]),
        .Q(\Dout_reg_n_0_[27] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [28]),
        .Q(\Dout_reg_n_0_[28] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [29]),
        .Q(\Dout_reg_n_0_[29] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [2]),
        .Q(Q[2]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [30]),
        .Q(\Dout_reg_n_0_[30] ),
        .R(RESET_IBUF));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \Dout_reg[30]_i_1 
       (.CI(\Dout_reg[26]_i_1_n_0 ),
        .CO({\Dout_reg[30]_i_1_n_0 ,\NLW_Dout_reg[30]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\Dout_reg_n_0_[30] ,\Dout_reg_n_0_[29] ,\Dout_reg_n_0_[28] ,\Dout_reg_n_0_[27] }),
        .O(\Dout_reg[31]_0 [28:25]),
        .S({\Dout_reg_n_0_[30] ,\Dout_reg_n_0_[29] ,\Dout_reg_n_0_[28] ,\Dout_reg_n_0_[27] }));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [31]),
        .Q(\Dout_reg_n_0_[31] ),
        .R(RESET_IBUF));
  CARRY4 \Dout_reg[31]_i_1 
       (.CI(\Dout_reg[30]_i_1_n_0 ),
        .CO(\NLW_Dout_reg[31]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_Dout_reg[31]_i_1_O_UNCONNECTED [3:1],\Dout_reg[31]_0 [29]}),
        .S({1'b0,1'b0,1'b0,\Dout_reg_n_0_[31] }));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [3]),
        .Q(Q[3]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [4]),
        .Q(Q[4]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [5]),
        .Q(Q[5]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [6]),
        .Q(Q[6]),
        .R(RESET_IBUF));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \Dout_reg[6]_i_1 
       (.CI(1'b0),
        .CO({\Dout_reg[6]_i_1_n_0 ,\NLW_Dout_reg[6]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(Q[2]),
        .DI(Q[6:3]),
        .O(\Dout_reg[31]_0 [4:1]),
        .S(Q[6:3]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [7]),
        .Q(Q[7]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [8]),
        .Q(\Dout_reg_n_0_[8] ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(E),
        .D(\Dout_reg[31]_1 [9]),
        .Q(\Dout_reg_n_0_[9] ),
        .R(RESET_IBUF));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_0
   (\Dout_reg[30]_0 ,
    \Dout_reg[30]_1 ,
    DI,
    Q,
    CO,
    \Dout[3]_i_3 ,
    RESET_IBUF,
    D,
    CLK_IBUF_BUFG);
  output [0:0]\Dout_reg[30]_0 ;
  output [0:0]\Dout_reg[30]_1 ;
  output [0:0]DI;
  output [31:0]Q;
  input [0:0]CO;
  input [0:0]\Dout[3]_i_3 ;
  input RESET_IBUF;
  input [31:0]D;
  input CLK_IBUF_BUFG;

  wire CLK_IBUF_BUFG;
  wire [0:0]CO;
  wire [31:0]D;
  wire [0:0]DI;
  wire [0:0]\Dout[3]_i_3 ;
  wire [0:0]\Dout_reg[30]_0 ;
  wire [0:0]\Dout_reg[30]_1 ;
  wire [31:0]Q;
  wire RESET_IBUF;
  wire [3:1]\NLW_Dout_reg[3]_i_5_CO_UNCONNECTED ;
  wire [3:0]\NLW_Dout_reg[3]_i_5_O_UNCONNECTED ;
  wire [3:1]\NLW_Dout_reg[3]_i_6_CO_UNCONNECTED ;
  wire [3:0]\NLW_Dout_reg[3]_i_6_O_UNCONNECTED ;

  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[0]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[10]),
        .Q(Q[10]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[11]),
        .Q(Q[11]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[12]),
        .Q(Q[12]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[13]),
        .Q(Q[13]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[14]),
        .Q(Q[14]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[15]),
        .Q(Q[15]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[16]),
        .Q(Q[16]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[17]),
        .Q(Q[17]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[18]),
        .Q(Q[18]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[19]),
        .Q(Q[19]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[1]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[20]),
        .Q(Q[20]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[21]),
        .Q(Q[21]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[22]),
        .Q(Q[22]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[23]),
        .Q(Q[23]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[24]),
        .Q(Q[24]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[25]),
        .Q(Q[25]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[26]),
        .Q(Q[26]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[27]),
        .Q(Q[27]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[28]),
        .Q(Q[28]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[29]),
        .Q(Q[29]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[2]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[30]),
        .Q(Q[30]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[31]),
        .Q(Q[31]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[3]),
        .Q(Q[3]),
        .R(RESET_IBUF));
  CARRY4 \Dout_reg[3]_i_5 
       (.CI(CO),
        .CO({\NLW_Dout_reg[3]_i_5_CO_UNCONNECTED [3:1],\Dout_reg[30]_0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_Dout_reg[3]_i_5_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  CARRY4 \Dout_reg[3]_i_6 
       (.CI(\Dout[3]_i_3 ),
        .CO({\NLW_Dout_reg[3]_i_6_CO_UNCONNECTED [3:1],\Dout_reg[30]_1 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_Dout_reg[3]_i_6_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[4]),
        .Q(Q[4]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[5]),
        .Q(Q[5]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[6]),
        .Q(Q[6]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[7]),
        .Q(Q[7]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[8]),
        .Q(Q[8]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[9]),
        .Q(Q[9]),
        .R(RESET_IBUF));
  LUT1 #(
    .INIT(2'h1)) 
    S0_carry__6_i_1
       (.I0(Q[31]),
        .O(DI));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_1
   (\Dout_reg[7]_rep ,
    \Dout_reg[25]_0 ,
    \Dout_reg[7]_rep_0 ,
    \Dout_reg[26]_0 ,
    \Dout_reg[25]_1 ,
    \Dout_reg[7]_rep_1 ,
    \Dout_reg[24]_0 ,
    \Dout_reg[18]_0 ,
    \Dout_reg[19]_0 ,
    \Dout_reg[7]_rep_2 ,
    \Dout_reg[12]_0 ,
    \Dout_reg[7]_0 ,
    \Dout_reg[7]_rep_3 ,
    \Dout_reg[7]_rep_4 ,
    \Dout_reg[3]_0 ,
    \Dout_reg[7]_rep_5 ,
    \Dout_reg[0]_0 ,
    \Dout_reg[7]_rep_6 ,
    \Dout_reg[7]_rep_7 ,
    \Dout_reg[10]_0 ,
    \Dout_reg[14]_0 ,
    \Dout_reg[15]_0 ,
    \Dout_reg[7]_rep_8 ,
    \Dout_reg[14]_1 ,
    \Dout_reg[13]_0 ,
    Q,
    \Dout_reg[12]_1 ,
    \Dout_reg[2]_0 ,
    \Dout_reg[1]_0 ,
    \Dout_reg[3]_1 ,
    \Dout_reg[8]_0 ,
    \Dout_reg[15]_1 ,
    \Dout_reg[17]_0 ,
    \Dout_reg[22]_0 ,
    \Dout_reg[23]_0 ,
    \Dout_reg[24]_1 ,
    \Dout_reg[27]_0 ,
    \Dout_reg[30]_0 ,
    \Dout_reg[28]_0 ,
    Instr_out_OBUF,
    \Dout_reg[23]_1 ,
    \Dout_reg[7]_1 ,
    \Dout_reg[6]_0 ,
    \Dout_reg[2]_1 ,
    \Dout_reg[16]_0 ,
    \Dout_reg[16]_1 ,
    \ALUResult_out_OBUF[28]_inst_i_2_0 ,
    \ALUResult_out_OBUF[28]_inst_i_2_1 ,
    RESET_IBUF,
    \Dout_reg[31]_0 ,
    CLK_IBUF_BUFG);
  output \Dout_reg[7]_rep ;
  output \Dout_reg[25]_0 ;
  output \Dout_reg[7]_rep_0 ;
  output \Dout_reg[26]_0 ;
  output \Dout_reg[25]_1 ;
  output \Dout_reg[7]_rep_1 ;
  output \Dout_reg[24]_0 ;
  output \Dout_reg[18]_0 ;
  output \Dout_reg[19]_0 ;
  output \Dout_reg[7]_rep_2 ;
  output \Dout_reg[12]_0 ;
  output \Dout_reg[7]_0 ;
  output \Dout_reg[7]_rep_3 ;
  output \Dout_reg[7]_rep_4 ;
  output \Dout_reg[3]_0 ;
  output \Dout_reg[7]_rep_5 ;
  output \Dout_reg[0]_0 ;
  output \Dout_reg[7]_rep_6 ;
  output \Dout_reg[7]_rep_7 ;
  output \Dout_reg[10]_0 ;
  output \Dout_reg[14]_0 ;
  output \Dout_reg[15]_0 ;
  output \Dout_reg[7]_rep_8 ;
  output \Dout_reg[14]_1 ;
  output \Dout_reg[13]_0 ;
  output [31:0]Q;
  output \Dout_reg[12]_1 ;
  output \Dout_reg[2]_0 ;
  output \Dout_reg[1]_0 ;
  output \Dout_reg[3]_1 ;
  output \Dout_reg[8]_0 ;
  output \Dout_reg[15]_1 ;
  output \Dout_reg[17]_0 ;
  output \Dout_reg[22]_0 ;
  output \Dout_reg[23]_0 ;
  output \Dout_reg[24]_1 ;
  output \Dout_reg[27]_0 ;
  output \Dout_reg[30]_0 ;
  input \Dout_reg[28]_0 ;
  input [1:0]Instr_out_OBUF;
  input \Dout_reg[23]_1 ;
  input \Dout_reg[7]_1 ;
  input \Dout_reg[6]_0 ;
  input \Dout_reg[2]_1 ;
  input \Dout_reg[16]_0 ;
  input \Dout_reg[16]_1 ;
  input [21:0]\ALUResult_out_OBUF[28]_inst_i_2_0 ;
  input \ALUResult_out_OBUF[28]_inst_i_2_1 ;
  input RESET_IBUF;
  input [31:0]\Dout_reg[31]_0 ;
  input CLK_IBUF_BUFG;

  wire \ALUResult_out_OBUF[12]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[21]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[23]_inst_i_7_n_0 ;
  wire [21:0]\ALUResult_out_OBUF[28]_inst_i_2_0 ;
  wire \ALUResult_out_OBUF[28]_inst_i_2_1 ;
  wire \ALUResult_out_OBUF[28]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[28]_inst_i_8_n_0 ;
  wire \ALUResult_out_OBUF[28]_inst_i_9_n_0 ;
  wire \ALUResult_out_OBUF[2]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[2]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[7]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[7]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[8]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[9]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[9]_inst_i_7_n_0 ;
  wire CLK_IBUF_BUFG;
  wire \Dout_reg[0]_0 ;
  wire \Dout_reg[10]_0 ;
  wire \Dout_reg[12]_0 ;
  wire \Dout_reg[12]_1 ;
  wire \Dout_reg[13]_0 ;
  wire \Dout_reg[14]_0 ;
  wire \Dout_reg[14]_1 ;
  wire \Dout_reg[15]_0 ;
  wire \Dout_reg[15]_1 ;
  wire \Dout_reg[16]_0 ;
  wire \Dout_reg[16]_1 ;
  wire \Dout_reg[17]_0 ;
  wire \Dout_reg[18]_0 ;
  wire \Dout_reg[19]_0 ;
  wire \Dout_reg[1]_0 ;
  wire \Dout_reg[22]_0 ;
  wire \Dout_reg[23]_0 ;
  wire \Dout_reg[23]_1 ;
  wire \Dout_reg[24]_0 ;
  wire \Dout_reg[24]_1 ;
  wire \Dout_reg[25]_0 ;
  wire \Dout_reg[25]_1 ;
  wire \Dout_reg[26]_0 ;
  wire \Dout_reg[27]_0 ;
  wire \Dout_reg[28]_0 ;
  wire \Dout_reg[2]_0 ;
  wire \Dout_reg[2]_1 ;
  wire \Dout_reg[30]_0 ;
  wire [31:0]\Dout_reg[31]_0 ;
  wire \Dout_reg[3]_0 ;
  wire \Dout_reg[3]_1 ;
  wire \Dout_reg[6]_0 ;
  wire \Dout_reg[7]_0 ;
  wire \Dout_reg[7]_1 ;
  wire \Dout_reg[7]_rep ;
  wire \Dout_reg[7]_rep_0 ;
  wire \Dout_reg[7]_rep_1 ;
  wire \Dout_reg[7]_rep_2 ;
  wire \Dout_reg[7]_rep_3 ;
  wire \Dout_reg[7]_rep_4 ;
  wire \Dout_reg[7]_rep_5 ;
  wire \Dout_reg[7]_rep_6 ;
  wire \Dout_reg[7]_rep_7 ;
  wire \Dout_reg[7]_rep_8 ;
  wire \Dout_reg[8]_0 ;
  wire [1:0]Instr_out_OBUF;
  wire [31:0]Q;
  wire RESET_IBUF;

  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[0]_inst_i_7 
       (.I0(Q[3]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [3]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[1]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [1]),
        .O(\Dout_reg[3]_1 ));
  LUT5 #(
    .INIT(32'h44CF77CF)) 
    \ALUResult_out_OBUF[10]_inst_i_6 
       (.I0(Q[12]),
        .I1(Instr_out_OBUF[1]),
        .I2(\ALUResult_out_OBUF[28]_inst_i_2_0 [10]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(Q[10]),
        .O(\Dout_reg[12]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[10]_inst_i_7 
       (.I0(Q[7]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [7]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[9]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [9]),
        .O(\Dout_reg[7]_0 ));
  LUT6 #(
    .INIT(64'h5050303F5F5F303F)) 
    \ALUResult_out_OBUF[10]_inst_i_8 
       (.I0(Q[8]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [8]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [10]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[10]),
        .O(\Dout_reg[8]_0 ));
  LUT6 #(
    .INIT(64'h30303F3F5F505F50)) 
    \ALUResult_out_OBUF[12]_inst_i_3 
       (.I0(\Dout_reg[10]_0 ),
        .I1(\ALUResult_out_OBUF[12]_inst_i_6_n_0 ),
        .I2(\Dout_reg[28]_0 ),
        .I3(\Dout_reg[14]_0 ),
        .I4(\Dout_reg[15]_0 ),
        .I5(Instr_out_OBUF[0]),
        .O(\Dout_reg[7]_rep_7 ));
  LUT5 #(
    .INIT(32'h305F3F5F)) 
    \ALUResult_out_OBUF[12]_inst_i_6 
       (.I0(\ALUResult_out_OBUF[28]_inst_i_2_0 [9]),
        .I1(Q[9]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(Q[11]),
        .O(\ALUResult_out_OBUF[12]_inst_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hCFC0A0A0)) 
    \ALUResult_out_OBUF[12]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[28]_inst_i_2_0 [11]),
        .I1(Q[14]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[12]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .O(\Dout_reg[14]_0 ));
  LUT5 #(
    .INIT(32'h305F3F5F)) 
    \ALUResult_out_OBUF[13]_inst_i_6 
       (.I0(\ALUResult_out_OBUF[28]_inst_i_2_0 [12]),
        .I1(Q[15]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(Q[13]),
        .O(\Dout_reg[15]_0 ));
  LUT5 #(
    .INIT(32'h305F3F5F)) 
    \ALUResult_out_OBUF[13]_inst_i_8 
       (.I0(\ALUResult_out_OBUF[28]_inst_i_2_0 [10]),
        .I1(Q[10]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(Q[12]),
        .O(\Dout_reg[10]_0 ));
  LUT5 #(
    .INIT(32'hBB308830)) 
    \ALUResult_out_OBUF[15]_inst_i_7 
       (.I0(Q[12]),
        .I1(Instr_out_OBUF[1]),
        .I2(\ALUResult_out_OBUF[28]_inst_i_2_0 [11]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(Q[14]),
        .O(\Dout_reg[12]_1 ));
  LUT6 #(
    .INIT(64'hC0C0CFCF5F505F50)) 
    \ALUResult_out_OBUF[16]_inst_i_3 
       (.I0(\Dout_reg[14]_1 ),
        .I1(\Dout_reg[13]_0 ),
        .I2(\Dout_reg[28]_0 ),
        .I3(\Dout_reg[16]_0 ),
        .I4(\Dout_reg[16]_1 ),
        .I5(Instr_out_OBUF[0]),
        .O(\Dout_reg[7]_rep_8 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \ALUResult_out_OBUF[16]_inst_i_6 
       (.I0(Q[13]),
        .I1(Instr_out_OBUF[1]),
        .I2(Q[15]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_0 [12]),
        .O(\Dout_reg[13]_0 ));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    \ALUResult_out_OBUF[17]_inst_i_8 
       (.I0(Q[14]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [11]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[16]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [13]),
        .O(\Dout_reg[14]_1 ));
  LUT6 #(
    .INIT(64'hAFAFCFC0A0A0CFC0)) 
    \ALUResult_out_OBUF[18]_inst_i_6 
       (.I0(Q[15]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [12]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [14]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[17]),
        .O(\Dout_reg[15]_1 ));
  LUT6 #(
    .INIT(64'h5050303F5F5F303F)) 
    \ALUResult_out_OBUF[20]_inst_i_6 
       (.I0(Q[17]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [14]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [17]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[19]),
        .O(\Dout_reg[17]_0 ));
  LUT6 #(
    .INIT(64'h0F000FFF55335533)) 
    \ALUResult_out_OBUF[21]_inst_i_3 
       (.I0(\Dout_reg[24]_0 ),
        .I1(\ALUResult_out_OBUF[21]_inst_i_6_n_0 ),
        .I2(\Dout_reg[18]_0 ),
        .I3(Instr_out_OBUF[0]),
        .I4(\Dout_reg[19]_0 ),
        .I5(\Dout_reg[28]_0 ),
        .O(\Dout_reg[7]_rep_1 ));
  LUT6 #(
    .INIT(64'h5050303F5F5F303F)) 
    \ALUResult_out_OBUF[21]_inst_i_6 
       (.I0(Q[23]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [19]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [17]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[21]),
        .O(\ALUResult_out_OBUF[21]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    \ALUResult_out_OBUF[21]_inst_i_7 
       (.I0(Q[18]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [15]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[20]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [16]),
        .O(\Dout_reg[18]_0 ));
  LUT5 #(
    .INIT(32'h440F770F)) 
    \ALUResult_out_OBUF[22]_inst_i_6 
       (.I0(Q[19]),
        .I1(Instr_out_OBUF[1]),
        .I2(\ALUResult_out_OBUF[28]_inst_i_2_0 [17]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(Q[21]),
        .O(\Dout_reg[19]_0 ));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    \ALUResult_out_OBUF[22]_inst_i_7 
       (.I0(Q[24]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [20]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[22]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [18]),
        .O(\Dout_reg[24]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[23]_inst_i_3 
       (.I0(\Dout_reg[23]_1 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_7_n_0 ),
        .I2(\Dout_reg[28]_0 ),
        .I3(\Dout_reg[26]_0 ),
        .I4(Instr_out_OBUF[0]),
        .I5(\Dout_reg[25]_1 ),
        .O(\Dout_reg[7]_rep_0 ));
  LUT6 #(
    .INIT(64'hAFAFCFC0A0A0CFC0)) 
    \ALUResult_out_OBUF[23]_inst_i_7 
       (.I0(Q[21]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [17]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [19]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[23]),
        .O(\ALUResult_out_OBUF[23]_inst_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFAFCFC0A0A0CFC0)) 
    \ALUResult_out_OBUF[23]_inst_i_8 
       (.I0(Q[25]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [21]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [19]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[23]),
        .O(\Dout_reg[25]_1 ));
  LUT6 #(
    .INIT(64'hAFAFCFC0A0A0CFC0)) 
    \ALUResult_out_OBUF[24]_inst_i_6 
       (.I0(Q[26]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [21]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [20]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[24]),
        .O(\Dout_reg[26]_0 ));
  LUT5 #(
    .INIT(32'h440F770F)) 
    \ALUResult_out_OBUF[24]_inst_i_7 
       (.I0(Q[27]),
        .I1(Instr_out_OBUF[1]),
        .I2(\ALUResult_out_OBUF[28]_inst_i_2_0 [21]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(Q[25]),
        .O(\Dout_reg[27]_0 ));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    \ALUResult_out_OBUF[25]_inst_i_5 
       (.I0(Q[22]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [18]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[24]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [20]),
        .O(\Dout_reg[22]_0 ));
  LUT6 #(
    .INIT(64'h5050303F5F5F303F)) 
    \ALUResult_out_OBUF[26]_inst_i_6 
       (.I0(Q[23]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [19]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [21]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[25]),
        .O(\Dout_reg[23]_0 ));
  LUT6 #(
    .INIT(64'h5050303F5F5F303F)) 
    \ALUResult_out_OBUF[27]_inst_i_6 
       (.I0(Q[24]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [20]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [21]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[26]),
        .O(\Dout_reg[24]_1 ));
  LUT6 #(
    .INIT(64'hCFC05F5FCFC05050)) 
    \ALUResult_out_OBUF[28]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[28]_inst_i_5_n_0 ),
        .I1(\Dout_reg[25]_0 ),
        .I2(\Dout_reg[28]_0 ),
        .I3(\ALUResult_out_OBUF[28]_inst_i_8_n_0 ),
        .I4(Instr_out_OBUF[0]),
        .I5(\ALUResult_out_OBUF[28]_inst_i_9_n_0 ),
        .O(\Dout_reg[7]_rep ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \ALUResult_out_OBUF[28]_inst_i_5 
       (.I0(Q[26]),
        .I1(Instr_out_OBUF[1]),
        .I2(Q[28]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_0 [21]),
        .O(\ALUResult_out_OBUF[28]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h470047FF)) 
    \ALUResult_out_OBUF[28]_inst_i_6 
       (.I0(Q[25]),
        .I1(Instr_out_OBUF[1]),
        .I2(Q[27]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_0 [21]),
        .O(\Dout_reg[25]_0 ));
  LUT5 #(
    .INIT(32'h470047FF)) 
    \ALUResult_out_OBUF[28]_inst_i_8 
       (.I0(Q[31]),
        .I1(Instr_out_OBUF[1]),
        .I2(Q[29]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_0 [21]),
        .O(\ALUResult_out_OBUF[28]_inst_i_8_n_0 ));
  LUT5 #(
    .INIT(32'h470047FF)) 
    \ALUResult_out_OBUF[28]_inst_i_9 
       (.I0(Q[30]),
        .I1(Instr_out_OBUF[1]),
        .I2(Q[28]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_0 [21]),
        .O(\ALUResult_out_OBUF[28]_inst_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFAFC0AFCFA0C0A0C)) 
    \ALUResult_out_OBUF[2]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[2]_inst_i_6_n_0 ),
        .I1(\ALUResult_out_OBUF[2]_inst_i_7_n_0 ),
        .I2(\Dout_reg[28]_0 ),
        .I3(Instr_out_OBUF[0]),
        .I4(\Dout_reg[2]_1 ),
        .I5(\Dout_reg[0]_0 ),
        .O(\Dout_reg[7]_rep_5 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[2]_inst_i_6 
       (.I0(Q[5]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [5]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[3]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [3]),
        .O(\ALUResult_out_OBUF[2]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[2]_inst_i_7 
       (.I0(Q[4]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [4]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[2]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [2]),
        .O(\ALUResult_out_OBUF[2]_inst_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h303F505F)) 
    \ALUResult_out_OBUF[31]_inst_i_8 
       (.I0(Q[30]),
        .I1(Q[28]),
        .I2(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [21]),
        .I4(Instr_out_OBUF[1]),
        .O(\Dout_reg[30]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[3]_inst_i_6 
       (.I0(Q[0]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [0]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[2]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [2]),
        .O(\Dout_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[4]_inst_i_5 
       (.I0(Q[1]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [1]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[3]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [3]),
        .O(\Dout_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[5]_inst_i_6 
       (.I0(Q[2]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [2]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[4]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [4]),
        .O(\Dout_reg[2]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[6]_inst_i_3 
       (.I0(\Dout_reg[3]_0 ),
        .I1(\ALUResult_out_OBUF[7]_inst_i_7_n_0 ),
        .I2(\Dout_reg[28]_0 ),
        .I3(\ALUResult_out_OBUF[7]_inst_i_6_n_0 ),
        .I4(Instr_out_OBUF[0]),
        .I5(\Dout_reg[6]_0 ),
        .O(\Dout_reg[7]_rep_4 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[6]_inst_i_6 
       (.I0(Q[3]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [3]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[5]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [5]),
        .O(\Dout_reg[3]_0 ));
  LUT6 #(
    .INIT(64'hFA0C0A0CFAFC0AFC)) 
    \ALUResult_out_OBUF[7]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_7_n_0 ),
        .I1(\ALUResult_out_OBUF[7]_inst_i_6_n_0 ),
        .I2(\Dout_reg[28]_0 ),
        .I3(Instr_out_OBUF[0]),
        .I4(\ALUResult_out_OBUF[7]_inst_i_7_n_0 ),
        .I5(\Dout_reg[7]_1 ),
        .O(\Dout_reg[7]_rep_3 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[7]_inst_i_6 
       (.I0(Q[9]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [9]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[7]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [7]),
        .O(\ALUResult_out_OBUF[7]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFAFCFC0A0A0CFC0)) 
    \ALUResult_out_OBUF[7]_inst_i_7 
       (.I0(Q[4]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [4]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [6]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[6]),
        .O(\ALUResult_out_OBUF[7]_inst_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h30303F3F5F505F50)) 
    \ALUResult_out_OBUF[8]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[9]_inst_i_7_n_0 ),
        .I1(\Dout_reg[7]_1 ),
        .I2(\Dout_reg[28]_0 ),
        .I3(\ALUResult_out_OBUF[8]_inst_i_7_n_0 ),
        .I4(\ALUResult_out_OBUF[9]_inst_i_6_n_0 ),
        .I5(Instr_out_OBUF[0]),
        .O(\Dout_reg[7]_rep_6 ));
  LUT6 #(
    .INIT(64'hAFAFCFC0A0A0CFC0)) 
    \ALUResult_out_OBUF[8]_inst_i_7 
       (.I0(Q[10]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [10]),
        .I2(Instr_out_OBUF[1]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_0 [8]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(Q[8]),
        .O(\ALUResult_out_OBUF[8]_inst_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h05F5F3F305F50303)) 
    \ALUResult_out_OBUF[9]_inst_i_3 
       (.I0(\Dout_reg[12]_0 ),
        .I1(\ALUResult_out_OBUF[9]_inst_i_6_n_0 ),
        .I2(\Dout_reg[28]_0 ),
        .I3(\ALUResult_out_OBUF[9]_inst_i_7_n_0 ),
        .I4(Instr_out_OBUF[0]),
        .I5(\Dout_reg[7]_0 ),
        .O(\Dout_reg[7]_rep_2 ));
  LUT5 #(
    .INIT(32'h44CF77CF)) 
    \ALUResult_out_OBUF[9]_inst_i_6 
       (.I0(Q[11]),
        .I1(Instr_out_OBUF[1]),
        .I2(\ALUResult_out_OBUF[28]_inst_i_2_0 [9]),
        .I3(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I4(Q[9]),
        .O(\ALUResult_out_OBUF[9]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    \ALUResult_out_OBUF[9]_inst_i_7 
       (.I0(Q[6]),
        .I1(\ALUResult_out_OBUF[28]_inst_i_2_0 [6]),
        .I2(Instr_out_OBUF[1]),
        .I3(Q[8]),
        .I4(\ALUResult_out_OBUF[28]_inst_i_2_1 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_0 [8]),
        .O(\ALUResult_out_OBUF[9]_inst_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [0]),
        .Q(Q[0]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [10]),
        .Q(Q[10]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [11]),
        .Q(Q[11]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [12]),
        .Q(Q[12]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [13]),
        .Q(Q[13]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [14]),
        .Q(Q[14]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [15]),
        .Q(Q[15]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [16]),
        .Q(Q[16]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [17]),
        .Q(Q[17]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [18]),
        .Q(Q[18]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [19]),
        .Q(Q[19]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [1]),
        .Q(Q[1]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [20]),
        .Q(Q[20]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [21]),
        .Q(Q[21]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [22]),
        .Q(Q[22]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [23]),
        .Q(Q[23]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [24]),
        .Q(Q[24]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [25]),
        .Q(Q[25]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [26]),
        .Q(Q[26]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [27]),
        .Q(Q[27]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [28]),
        .Q(Q[28]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [29]),
        .Q(Q[29]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [2]),
        .Q(Q[2]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [30]),
        .Q(Q[30]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [31]),
        .Q(Q[31]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [3]),
        .Q(Q[3]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [4]),
        .Q(Q[4]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [5]),
        .Q(Q[5]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [6]),
        .Q(Q[6]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [7]),
        .Q(Q[7]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [8]),
        .Q(Q[8]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [9]),
        .Q(Q[9]),
        .R(RESET_IBUF));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_2
   (\Dout_reg[5]_0 ,
    Q,
    \Dout_reg[8]_0 ,
    \Dout_reg[16]_0 ,
    \Dout_reg[16]_1 ,
    \Dout_reg[18]_0 ,
    \Dout_reg[21]_0 ,
    \Dout_reg[20]_0 ,
    \Dout_reg[20]_1 ,
    \ALUResult_out_OBUF[23]_inst_i_3 ,
    Instr_out_OBUF,
    \ALUResult_out_OBUF[23]_inst_i_3_0 ,
    RESET_IBUF,
    D,
    CLK_IBUF_BUFG);
  output \Dout_reg[5]_0 ;
  output [21:0]Q;
  output \Dout_reg[8]_0 ;
  output \Dout_reg[16]_0 ;
  output \Dout_reg[16]_1 ;
  output \Dout_reg[18]_0 ;
  output \Dout_reg[21]_0 ;
  output \Dout_reg[20]_0 ;
  output \Dout_reg[20]_1 ;
  input [10:0]\ALUResult_out_OBUF[23]_inst_i_3 ;
  input [0:0]Instr_out_OBUF;
  input \ALUResult_out_OBUF[23]_inst_i_3_0 ;
  input RESET_IBUF;
  input [21:0]D;
  input CLK_IBUF_BUFG;

  wire [10:0]\ALUResult_out_OBUF[23]_inst_i_3 ;
  wire \ALUResult_out_OBUF[23]_inst_i_3_0 ;
  wire CLK_IBUF_BUFG;
  wire [21:0]D;
  wire \Dout_reg[16]_0 ;
  wire \Dout_reg[16]_1 ;
  wire \Dout_reg[18]_0 ;
  wire \Dout_reg[20]_0 ;
  wire \Dout_reg[20]_1 ;
  wire \Dout_reg[21]_0 ;
  wire \Dout_reg[5]_0 ;
  wire \Dout_reg[8]_0 ;
  wire [0:0]Instr_out_OBUF;
  wire [21:0]Q;
  wire RESET_IBUF;

  LUT6 #(
    .INIT(64'hCFCFAFA0C0C0AFA0)) 
    \ALUResult_out_OBUF[13]_inst_i_7 
       (.I0(Q[13]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_3 [5]),
        .I2(Instr_out_OBUF),
        .I3(Q[11]),
        .I4(\ALUResult_out_OBUF[23]_inst_i_3_0 ),
        .I5(\ALUResult_out_OBUF[23]_inst_i_3 [4]),
        .O(\Dout_reg[16]_0 ));
  LUT6 #(
    .INIT(64'hCFCFAFA0C0C0AFA0)) 
    \ALUResult_out_OBUF[16]_inst_i_7 
       (.I0(Q[15]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_3 [7]),
        .I2(Instr_out_OBUF),
        .I3(Q[13]),
        .I4(\ALUResult_out_OBUF[23]_inst_i_3_0 ),
        .I5(\ALUResult_out_OBUF[23]_inst_i_3 [5]),
        .O(\Dout_reg[18]_0 ));
  LUT6 #(
    .INIT(64'h303F5050303F5F5F)) 
    \ALUResult_out_OBUF[17]_inst_i_6 
       (.I0(Q[17]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_3 [8]),
        .I2(Instr_out_OBUF),
        .I3(\ALUResult_out_OBUF[23]_inst_i_3 [6]),
        .I4(\ALUResult_out_OBUF[23]_inst_i_3_0 ),
        .I5(Q[14]),
        .O(\Dout_reg[21]_0 ));
  LUT6 #(
    .INIT(64'hCFCFAFA0C0C0AFA0)) 
    \ALUResult_out_OBUF[17]_inst_i_7 
       (.I0(Q[16]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_3 [9]),
        .I2(Instr_out_OBUF),
        .I3(Q[15]),
        .I4(\ALUResult_out_OBUF[23]_inst_i_3_0 ),
        .I5(\ALUResult_out_OBUF[23]_inst_i_3 [7]),
        .O(\Dout_reg[20]_0 ));
  LUT6 #(
    .INIT(64'hCFCFAFA0C0C0AFA0)) 
    \ALUResult_out_OBUF[19]_inst_i_5 
       (.I0(Q[13]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_3 [5]),
        .I2(Instr_out_OBUF),
        .I3(Q[15]),
        .I4(\ALUResult_out_OBUF[23]_inst_i_3_0 ),
        .I5(\ALUResult_out_OBUF[23]_inst_i_3 [7]),
        .O(\Dout_reg[16]_1 ));
  LUT6 #(
    .INIT(64'hCFCFAFA0C0C0AFA0)) 
    \ALUResult_out_OBUF[23]_inst_i_6 
       (.I0(Q[16]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_3 [9]),
        .I2(Instr_out_OBUF),
        .I3(Q[18]),
        .I4(\ALUResult_out_OBUF[23]_inst_i_3_0 ),
        .I5(\ALUResult_out_OBUF[23]_inst_i_3 [10]),
        .O(\Dout_reg[20]_1 ));
  LUT6 #(
    .INIT(64'hCFCFAFA0C0C0AFA0)) 
    \ALUResult_out_OBUF[6]_inst_i_7 
       (.I0(Q[8]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_3 [3]),
        .I2(Instr_out_OBUF),
        .I3(Q[6]),
        .I4(\ALUResult_out_OBUF[23]_inst_i_3_0 ),
        .I5(\ALUResult_out_OBUF[23]_inst_i_3 [1]),
        .O(\Dout_reg[8]_0 ));
  LUT6 #(
    .INIT(64'h3030505F3F3F505F)) 
    \ALUResult_out_OBUF[8]_inst_i_6 
       (.I0(Q[5]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_3 [0]),
        .I2(Instr_out_OBUF),
        .I3(Q[7]),
        .I4(\ALUResult_out_OBUF[23]_inst_i_3_0 ),
        .I5(\ALUResult_out_OBUF[23]_inst_i_3 [2]),
        .O(\Dout_reg[5]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[0]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[10]),
        .Q(Q[10]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[11]),
        .Q(Q[11]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[12]),
        .Q(Q[12]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[13]),
        .Q(Q[13]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[14]),
        .Q(Q[14]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[15]),
        .Q(Q[15]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[1]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[16]),
        .Q(Q[16]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[17]),
        .Q(Q[17]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[18]),
        .Q(Q[18]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[19]),
        .Q(Q[19]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[20]),
        .Q(Q[20]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[2]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[21]),
        .Q(Q[21]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[3]),
        .Q(Q[3]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[4]),
        .Q(Q[4]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[5]),
        .Q(Q[5]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[6]),
        .Q(Q[6]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[7]),
        .Q(Q[7]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[8]),
        .Q(Q[8]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[9]),
        .Q(Q[9]),
        .R(RESET_IBUF));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_3
   (\Dout[1]_i_9_0 ,
    D,
    \Dout_reg[25]_rep_0 ,
    \Dout_reg[26]_rep_0 ,
    \Dout_reg[23]_rep_0 ,
    \Dout_reg[25]_rep_1 ,
    \Dout[1]_i_6_0 ,
    \Dout_reg[21]_rep_0 ,
    \Dout_reg[8]_rep_0 ,
    \Dout[1]_i_8_0 ,
    \Dout[1]_i_7_0 ,
    \Dout_reg[25]_rep_2 ,
    \Dout_reg[23]_rep_1 ,
    \Dout_reg[25]_rep_3 ,
    \Dout_reg[26]_rep_1 ,
    \Dout_reg[24]_rep_0 ,
    \Dout_reg[27]_rep__0_0 ,
    \Dout_reg[26]_rep_2 ,
    \Dout_reg[27]_rep__0_1 ,
    \Dout_reg[20]_rep_0 ,
    DATA_IN,
    \Dout_reg[19]_rep_rep_0 ,
    ADDRA,
    \Dout_reg[15]_rep_0 ,
    \Dout_reg[15]_rep_1 ,
    ADDRD,
    \Dout_reg[28]_rep_0 ,
    \Dout_reg[15]_rep_2 ,
    S,
    \Dout_reg[27]_0 ,
    \Dout_reg[31]_0 ,
    \Dout_reg[3]_0 ,
    \Dout_reg[7]_0 ,
    \Dout_reg[11] ,
    \Dout_reg[15]_0 ,
    \Dout_reg[27]_1 ,
    \Dout_reg[31]_1 ,
    \Dout_reg[23]_0 ,
    \Dout_reg[7]_1 ,
    \Dout_reg[11]_0 ,
    \Dout_reg[15]_1 ,
    \Dout_reg[21]_0 ,
    \Dout_reg[19]_0 ,
    \Dout_reg[23]_1 ,
    \Dout_reg[31]_2 ,
    \Dout_reg[31]_3 ,
    \Dout_reg[31]_rep_0 ,
    \Dout_reg[19]_rep_0 ,
    \Dout_reg[30]_0 ,
    \Dout_reg[30]_1 ,
    Q,
    \Dout_reg[2]_0 ,
    \Dout_reg[2]_1 ,
    \Dout_reg[3]_1 ,
    \Dout_reg[3]_2 ,
    \Dout_reg[6]_0 ,
    \Dout_reg[6]_1 ,
    \Dout_reg[7]_2 ,
    \Dout_reg[7]_3 ,
    \Dout_reg[8]_0 ,
    \Dout_reg[8]_1 ,
    \Dout_reg[9] ,
    \Dout_reg[9]_0 ,
    \Dout_reg[10] ,
    \Dout_reg[12]_0 ,
    \Dout_reg[12]_1 ,
    \Dout_reg[14]_0 ,
    \Dout_reg[15]_2 ,
    \Dout_reg[16]_0 ,
    \Dout_reg[16]_1 ,
    \Dout_reg[20]_0 ,
    \Dout_reg[21]_1 ,
    \Dout_reg[21]_2 ,
    \Dout_reg[23]_2 ,
    \Dout_reg[23]_3 ,
    \Dout_reg[28]_0 ,
    \Dout_reg[26]_0 ,
    \Dout_reg[26]_1 ,
    \Dout_reg[22]_0 ,
    \Dout_reg[22]_1 ,
    \Dout_reg[22]_2 ,
    \Dout_reg[22]_3 ,
    \Dout_reg[17] ,
    \Dout_reg[17]_0 ,
    \Dout_reg[17]_1 ,
    \Dout_reg[17]_2 ,
    \Dout_reg[13]_0 ,
    \Dout_reg[13]_1 ,
    \Dout_reg[13]_2 ,
    \Dout_reg[11]_1 ,
    \Dout_reg[10]_0 ,
    \Dout_reg[10]_1 ,
    \Dout_reg[10]_2 ,
    \Dout_reg[4]_0 ,
    \Dout_reg[0]_0 ,
    \Dout_reg[19]_1 ,
    \Dout_reg[19]_2 ,
    \Dout_reg[24]_0 ,
    \Dout_reg[24]_1 ,
    \ALUResult_out_OBUF[31]_inst_i_2_0 ,
    \Dout_reg[28]_1 ,
    \ALUResult_out_OBUF[29]_inst_i_3_0 ,
    \ALUResult_out_OBUF[31]_inst_i_4_0 ,
    \Dout_reg[27]_2 ,
    \Dout_reg[25]_0 ,
    \Dout_reg[20]_1 ,
    \Dout_reg[15]_3 ,
    \Dout_reg[15]_4 ,
    \Dout_reg[5]_0 ,
    \FSM_onehot_current_state_reg[13] ,
    \Dout_reg[31]_4 ,
    \Dout_reg[31]_5 ,
    ReadData_in_regRD,
    RF_reg_r2_0_15_30_31,
    DATA_OUT1,
    PCPlus8,
    \FSM_onehot_current_state_reg[2] ,
    Flags_in,
    \Dout_reg[31]_6 ,
    Result_out_OBUF,
    RESET_IBUF,
    \Dout_reg[31]_7 ,
    CLK_IBUF_BUFG,
    \Dout_reg[27]_rep_0 );
  output \Dout[1]_i_9_0 ;
  output [31:0]D;
  output \Dout_reg[25]_rep_0 ;
  output \Dout_reg[26]_rep_0 ;
  output \Dout_reg[23]_rep_0 ;
  output \Dout_reg[25]_rep_1 ;
  output \Dout[1]_i_6_0 ;
  output [2:0]\Dout_reg[21]_rep_0 ;
  output \Dout_reg[8]_rep_0 ;
  output \Dout[1]_i_8_0 ;
  output \Dout[1]_i_7_0 ;
  output \Dout_reg[25]_rep_2 ;
  output \Dout_reg[23]_rep_1 ;
  output \Dout_reg[25]_rep_3 ;
  output \Dout_reg[26]_rep_1 ;
  output [4:0]\Dout_reg[24]_rep_0 ;
  output \Dout_reg[27]_rep__0_0 ;
  output \Dout_reg[26]_rep_2 ;
  output [21:0]\Dout_reg[27]_rep__0_1 ;
  output [0:0]\Dout_reg[20]_rep_0 ;
  output [31:0]DATA_IN;
  output [31:0]\Dout_reg[19]_rep_rep_0 ;
  output [2:0]ADDRA;
  output \Dout_reg[15]_rep_0 ;
  output [3:0]\Dout_reg[15]_rep_1 ;
  output [3:0]ADDRD;
  output \Dout_reg[28]_rep_0 ;
  output \Dout_reg[15]_rep_2 ;
  output [3:0]S;
  output [3:0]\Dout_reg[27]_0 ;
  output [3:0]\Dout_reg[31]_0 ;
  output [3:0]\Dout_reg[3]_0 ;
  output [3:0]\Dout_reg[7]_0 ;
  output [3:0]\Dout_reg[11] ;
  output [3:0]\Dout_reg[15]_0 ;
  output [3:0]\Dout_reg[27]_1 ;
  output [3:0]\Dout_reg[31]_1 ;
  output [3:0]\Dout_reg[23]_0 ;
  output [3:0]\Dout_reg[7]_1 ;
  output [3:0]\Dout_reg[11]_0 ;
  output [3:0]\Dout_reg[15]_1 ;
  output [3:0]\Dout_reg[21]_0 ;
  output [3:0]\Dout_reg[19]_0 ;
  output [3:0]\Dout_reg[23]_1 ;
  output [0:0]\Dout_reg[31]_2 ;
  output [26:0]\Dout_reg[31]_3 ;
  output \Dout_reg[31]_rep_0 ;
  output \Dout_reg[19]_rep_0 ;
  input [17:0]\Dout_reg[30]_0 ;
  input [17:0]\Dout_reg[30]_1 ;
  input [31:0]Q;
  input \Dout_reg[2]_0 ;
  input \Dout_reg[2]_1 ;
  input \Dout_reg[3]_1 ;
  input \Dout_reg[3]_2 ;
  input \Dout_reg[6]_0 ;
  input \Dout_reg[6]_1 ;
  input \Dout_reg[7]_2 ;
  input \Dout_reg[7]_3 ;
  input \Dout_reg[8]_0 ;
  input \Dout_reg[8]_1 ;
  input \Dout_reg[9] ;
  input \Dout_reg[9]_0 ;
  input \Dout_reg[10] ;
  input \Dout_reg[12]_0 ;
  input \Dout_reg[12]_1 ;
  input \Dout_reg[14]_0 ;
  input \Dout_reg[15]_2 ;
  input \Dout_reg[16]_0 ;
  input \Dout_reg[16]_1 ;
  input \Dout_reg[20]_0 ;
  input \Dout_reg[21]_1 ;
  input \Dout_reg[21]_2 ;
  input \Dout_reg[23]_2 ;
  input \Dout_reg[23]_3 ;
  input \Dout_reg[28]_0 ;
  input \Dout_reg[26]_0 ;
  input \Dout_reg[26]_1 ;
  input \Dout_reg[22]_0 ;
  input \Dout_reg[22]_1 ;
  input \Dout_reg[22]_2 ;
  input \Dout_reg[22]_3 ;
  input \Dout_reg[17] ;
  input \Dout_reg[17]_0 ;
  input \Dout_reg[17]_1 ;
  input \Dout_reg[17]_2 ;
  input \Dout_reg[13]_0 ;
  input \Dout_reg[13]_1 ;
  input \Dout_reg[13]_2 ;
  input \Dout_reg[11]_1 ;
  input \Dout_reg[10]_0 ;
  input \Dout_reg[10]_1 ;
  input \Dout_reg[10]_2 ;
  input \Dout_reg[4]_0 ;
  input \Dout_reg[0]_0 ;
  input \Dout_reg[19]_1 ;
  input \Dout_reg[19]_2 ;
  input \Dout_reg[24]_0 ;
  input \Dout_reg[24]_1 ;
  input \ALUResult_out_OBUF[31]_inst_i_2_0 ;
  input [21:0]\Dout_reg[28]_1 ;
  input [31:0]\ALUResult_out_OBUF[29]_inst_i_3_0 ;
  input \ALUResult_out_OBUF[31]_inst_i_4_0 ;
  input \Dout_reg[27]_2 ;
  input \Dout_reg[25]_0 ;
  input \Dout_reg[20]_1 ;
  input \Dout_reg[15]_3 ;
  input \Dout_reg[15]_4 ;
  input \Dout_reg[5]_0 ;
  input \FSM_onehot_current_state_reg[13] ;
  input [2:0]\Dout_reg[31]_4 ;
  input [31:0]\Dout_reg[31]_5 ;
  input [31:0]ReadData_in_regRD;
  input [31:0]RF_reg_r2_0_15_30_31;
  input [31:0]DATA_OUT1;
  input [28:0]PCPlus8;
  input [0:0]\FSM_onehot_current_state_reg[2] ;
  input [1:0]Flags_in;
  input \Dout_reg[31]_6 ;
  input [0:0]Result_out_OBUF;
  input RESET_IBUF;
  input [26:0]\Dout_reg[31]_7 ;
  input CLK_IBUF_BUFG;
  input \Dout_reg[27]_rep_0 ;

  wire [2:0]ADDRA;
  wire [3:0]ADDRD;
  wire [30:1]\ALU/p_0_out ;
  wire \ALUControl_out_OBUF[0]_inst_i_2_n_0 ;
  wire \ALUControl_out_OBUF[1]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[0]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[0]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[0]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[0]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[0]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[0]_inst_i_8_n_0 ;
  wire \ALUResult_out_OBUF[10]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[10]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[11]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[11]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[11]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[11]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[11]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[11]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[12]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[12]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[13]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[13]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[13]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[13]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[13]_inst_i_9_n_0 ;
  wire \ALUResult_out_OBUF[14]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[14]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[14]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[14]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[15]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[15]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[15]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[16]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[17]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[17]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[17]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[17]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[18]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[18]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[18]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[18]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[19]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[19]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[19]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[19]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[1]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[1]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[1]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[1]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[1]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[20]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[20]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[20]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[21]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[22]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[22]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[22]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[22]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[23]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[24]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[24]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[24]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[24]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[25]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[25]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[25]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[25]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[26]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[26]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[26]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[26]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[27]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[27]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[27]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[27]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[28]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[28]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_10_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_11_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_2_n_0 ;
  wire [31:0]\ALUResult_out_OBUF[29]_inst_i_3_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_8_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_9_n_0 ;
  wire \ALUResult_out_OBUF[2]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_10_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_11_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_12_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_13_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_15_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_16_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_17_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_8_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_9_n_0 ;
  wire \ALUResult_out_OBUF[31]_inst_i_2_0 ;
  wire \ALUResult_out_OBUF[31]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[31]_inst_i_4_0 ;
  wire \ALUResult_out_OBUF[31]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[31]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[31]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[3]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[3]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[3]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[3]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[4]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[4]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[4]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[4]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[5]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[5]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[5]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[5]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[6]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[7]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[8]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[9]_inst_i_2_n_0 ;
  wire CLK_IBUF_BUFG;
  wire [31:0]D;
  wire [31:0]DATA_IN;
  wire [31:0]DATA_OUT1;
  wire \Dout[1]_i_6_0 ;
  wire \Dout[1]_i_6_n_0 ;
  wire \Dout[1]_i_7_0 ;
  wire \Dout[1]_i_7_n_0 ;
  wire \Dout[1]_i_8_0 ;
  wire \Dout[1]_i_8_n_0 ;
  wire \Dout[1]_i_9_0 ;
  wire \Dout[1]_i_9_n_0 ;
  wire \Dout_reg[0]_0 ;
  wire \Dout_reg[10] ;
  wire \Dout_reg[10]_0 ;
  wire \Dout_reg[10]_1 ;
  wire \Dout_reg[10]_2 ;
  wire [3:0]\Dout_reg[11] ;
  wire [3:0]\Dout_reg[11]_0 ;
  wire \Dout_reg[11]_1 ;
  wire \Dout_reg[12]_0 ;
  wire \Dout_reg[12]_1 ;
  wire \Dout_reg[13]_0 ;
  wire \Dout_reg[13]_1 ;
  wire \Dout_reg[13]_2 ;
  wire \Dout_reg[14]_0 ;
  wire [3:0]\Dout_reg[15]_0 ;
  wire [3:0]\Dout_reg[15]_1 ;
  wire \Dout_reg[15]_2 ;
  wire \Dout_reg[15]_3 ;
  wire \Dout_reg[15]_4 ;
  wire \Dout_reg[15]_rep_0 ;
  wire [3:0]\Dout_reg[15]_rep_1 ;
  wire \Dout_reg[15]_rep_2 ;
  wire \Dout_reg[16]_0 ;
  wire \Dout_reg[16]_1 ;
  wire \Dout_reg[17] ;
  wire \Dout_reg[17]_0 ;
  wire \Dout_reg[17]_1 ;
  wire \Dout_reg[17]_2 ;
  wire [3:0]\Dout_reg[19]_0 ;
  wire \Dout_reg[19]_1 ;
  wire \Dout_reg[19]_2 ;
  wire \Dout_reg[19]_rep_0 ;
  wire [31:0]\Dout_reg[19]_rep_rep_0 ;
  wire \Dout_reg[20]_0 ;
  wire \Dout_reg[20]_1 ;
  wire [0:0]\Dout_reg[20]_rep_0 ;
  wire [3:0]\Dout_reg[21]_0 ;
  wire \Dout_reg[21]_1 ;
  wire \Dout_reg[21]_2 ;
  wire [2:0]\Dout_reg[21]_rep_0 ;
  wire \Dout_reg[22]_0 ;
  wire \Dout_reg[22]_1 ;
  wire \Dout_reg[22]_2 ;
  wire \Dout_reg[22]_3 ;
  wire [3:0]\Dout_reg[23]_0 ;
  wire [3:0]\Dout_reg[23]_1 ;
  wire \Dout_reg[23]_2 ;
  wire \Dout_reg[23]_3 ;
  wire \Dout_reg[23]_rep_0 ;
  wire \Dout_reg[23]_rep_1 ;
  wire \Dout_reg[24]_0 ;
  wire \Dout_reg[24]_1 ;
  wire [4:0]\Dout_reg[24]_rep_0 ;
  wire \Dout_reg[25]_0 ;
  wire \Dout_reg[25]_rep_0 ;
  wire \Dout_reg[25]_rep_1 ;
  wire \Dout_reg[25]_rep_2 ;
  wire \Dout_reg[25]_rep_3 ;
  wire \Dout_reg[26]_0 ;
  wire \Dout_reg[26]_1 ;
  wire \Dout_reg[26]_rep_0 ;
  wire \Dout_reg[26]_rep_1 ;
  wire \Dout_reg[26]_rep_2 ;
  wire [3:0]\Dout_reg[27]_0 ;
  wire [3:0]\Dout_reg[27]_1 ;
  wire \Dout_reg[27]_2 ;
  wire \Dout_reg[27]_rep_0 ;
  wire \Dout_reg[27]_rep__0_0 ;
  wire [21:0]\Dout_reg[27]_rep__0_1 ;
  wire \Dout_reg[27]_rep_n_0 ;
  wire \Dout_reg[28]_0 ;
  wire [21:0]\Dout_reg[28]_1 ;
  wire \Dout_reg[28]_rep_0 ;
  wire \Dout_reg[2]_0 ;
  wire \Dout_reg[2]_1 ;
  wire [17:0]\Dout_reg[30]_0 ;
  wire [17:0]\Dout_reg[30]_1 ;
  wire [3:0]\Dout_reg[31]_0 ;
  wire [3:0]\Dout_reg[31]_1 ;
  wire [0:0]\Dout_reg[31]_2 ;
  wire [26:0]\Dout_reg[31]_3 ;
  wire [2:0]\Dout_reg[31]_4 ;
  wire [31:0]\Dout_reg[31]_5 ;
  wire \Dout_reg[31]_6 ;
  wire [26:0]\Dout_reg[31]_7 ;
  wire \Dout_reg[31]_rep_0 ;
  wire [3:0]\Dout_reg[3]_0 ;
  wire \Dout_reg[3]_1 ;
  wire \Dout_reg[3]_2 ;
  wire \Dout_reg[4]_0 ;
  wire \Dout_reg[5]_0 ;
  wire \Dout_reg[6]_0 ;
  wire \Dout_reg[6]_1 ;
  wire [3:0]\Dout_reg[7]_0 ;
  wire [3:0]\Dout_reg[7]_1 ;
  wire \Dout_reg[7]_2 ;
  wire \Dout_reg[7]_3 ;
  wire \Dout_reg[8]_0 ;
  wire \Dout_reg[8]_1 ;
  wire \Dout_reg[8]_rep_0 ;
  wire \Dout_reg[9] ;
  wire \Dout_reg[9]_0 ;
  wire \FSM_onehot_current_state_reg[13] ;
  wire [0:0]\FSM_onehot_current_state_reg[2] ;
  wire [1:0]Flags_in;
  wire [30:0]Instr_out_OBUF;
  wire [28:0]PCPlus8;
  wire [31:0]Q;
  wire RESET_IBUF;
  wire [31:0]RF_reg_r2_0_15_30_31;
  wire [31:0]ReadData_in_regRD;
  wire [0:0]Result_out_OBUF;
  wire [3:0]S;
  wire [27:0]SrcB;

  LUT4 #(
    .INIT(16'h005C)) 
    \ALUControl_out_OBUF[0]_inst_i_1 
       (.I0(Instr_out_OBUF[23]),
        .I1(\ALUControl_out_OBUF[0]_inst_i_2_n_0 ),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .O(\Dout_reg[23]_rep_0 ));
  LUT5 #(
    .INIT(32'hAFEFAAAA)) 
    \ALUControl_out_OBUF[0]_inst_i_2 
       (.I0(Instr_out_OBUF[22]),
        .I1(Instr_out_OBUF[5]),
        .I2(\Dout_reg[21]_rep_0 [2]),
        .I3(Instr_out_OBUF[25]),
        .I4(Instr_out_OBUF[24]),
        .O(\ALUControl_out_OBUF[0]_inst_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0002)) 
    \ALUControl_out_OBUF[1]_inst_i_1 
       (.I0(\ALUControl_out_OBUF[1]_inst_i_2_n_0 ),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[22]),
        .O(\Dout_reg[26]_rep_0 ));
  LUT5 #(
    .INIT(32'h4FF04FFF)) 
    \ALUControl_out_OBUF[1]_inst_i_2 
       (.I0(Instr_out_OBUF[25]),
        .I1(Instr_out_OBUF[6]),
        .I2(\Dout_reg[21]_rep_0 [2]),
        .I3(Instr_out_OBUF[24]),
        .I4(Instr_out_OBUF[23]),
        .O(\ALUControl_out_OBUF[1]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000FB00)) 
    \ALUControl_out_OBUF[2]_inst_i_1 
       (.I0(Instr_out_OBUF[25]),
        .I1(Instr_out_OBUF[24]),
        .I2(Instr_out_OBUF[22]),
        .I3(\Dout_reg[21]_rep_0 [2]),
        .I4(\Dout_reg[26]_rep_1 ),
        .I5(\Dout_reg[27]_rep_n_0 ),
        .O(\Dout_reg[25]_rep_1 ));
  LUT6 #(
    .INIT(64'h0000000000000400)) 
    \ALUControl_out_OBUF[3]_inst_i_1 
       (.I0(Instr_out_OBUF[25]),
        .I1(Instr_out_OBUF[24]),
        .I2(Instr_out_OBUF[22]),
        .I3(\Dout_reg[21]_rep_0 [2]),
        .I4(\Dout_reg[26]_rep_1 ),
        .I5(\Dout_reg[27]_rep_n_0 ),
        .O(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'hEFE0EFEFEFE0EFE0)) 
    \ALUResult_out_OBUF[0]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[0]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[0]_inst_i_3_n_0 ),
        .I2(\Dout_reg[25]_rep_0 ),
        .I3(\ALUResult_out_OBUF[0]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[0]_inst_i_5_n_0 ),
        .I5(\ALUResult_out_OBUF[0]_inst_i_6_n_0 ),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hB888B888B8880000)) 
    \ALUResult_out_OBUF[0]_inst_i_2 
       (.I0(\Dout_reg[0]_0 ),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .I2(\Dout_reg[21]_rep_0 [1]),
        .I3(SrcB[2]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[0]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h000000E2)) 
    \ALUResult_out_OBUF[0]_inst_i_3 
       (.I0(\Dout_reg[28]_1 [0]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_3_0 [0]),
        .I3(\Dout_reg[21]_rep_0 [0]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .O(\ALUResult_out_OBUF[0]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h88822282)) 
    \ALUResult_out_OBUF[0]_inst_i_4 
       (.I0(\Dout_reg[25]_rep_1 ),
        .I1(\ALUResult_out_OBUF[0]_inst_i_8_n_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(Q[0]),
        .O(\ALUResult_out_OBUF[0]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hAEAAEFAA)) 
    \ALUResult_out_OBUF[0]_inst_i_5 
       (.I0(\Dout_reg[25]_rep_1 ),
        .I1(\ALUResult_out_OBUF[0]_inst_i_8_n_0 ),
        .I2(Q[0]),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[23]_rep_0 ),
        .O(\ALUResult_out_OBUF[0]_inst_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFEAE)) 
    \ALUResult_out_OBUF[0]_inst_i_6 
       (.I0(\Dout_reg[26]_rep_0 ),
        .I1(\Dout_reg[30]_0 [0]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[30]_1 [0]),
        .O(\ALUResult_out_OBUF[0]_inst_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[0]_inst_i_8 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [0]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [0]),
        .O(\ALUResult_out_OBUF[0]_inst_i_8_n_0 ));
  MUXF7 \ALUResult_out_OBUF[10]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[10]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[10]_inst_i_3_n_0 ),
        .O(D[10]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'h3C5A3C5AE800E8FF)) 
    \ALUResult_out_OBUF[10]_inst_i_2 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(Q[10]),
        .I2(SrcB[10]),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[10] ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[10]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF5030503F5F305F3)) 
    \ALUResult_out_OBUF[10]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[11]_inst_i_6_n_0 ),
        .I1(\Dout_reg[10]_0 ),
        .I2(\Dout_reg[23]_rep_1 ),
        .I3(\Dout_reg[21]_rep_0 [0]),
        .I4(\Dout_reg[10]_1 ),
        .I5(\Dout_reg[10]_2 ),
        .O(\ALUResult_out_OBUF[10]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[10]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [10]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [10]),
        .O(SrcB[10]));
  LUT5 #(
    .INIT(32'h88BB8B8B)) 
    \ALUResult_out_OBUF[11]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[11]_inst_i_2_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[11]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[11]_inst_i_4_n_0 ),
        .I4(\Dout_reg[25]_rep_1 ),
        .O(D[11]));
  LUT6 #(
    .INIT(64'hFD010101FD01FDFD)) 
    \ALUResult_out_OBUF[11]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[11]_inst_i_5_n_0 ),
        .I1(\Dout_reg[26]_rep_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[11]_1 ),
        .I4(\Dout_reg[21]_rep_0 [0]),
        .I5(\ALUResult_out_OBUF[11]_inst_i_6_n_0 ),
        .O(\ALUResult_out_OBUF[11]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h101F7070101F7F7F)) 
    \ALUResult_out_OBUF[11]_inst_i_3 
       (.I0(Q[11]),
        .I1(\ALUResult_out_OBUF[11]_inst_i_7_n_0 ),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [5]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [5]),
        .O(\ALUResult_out_OBUF[11]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h88877787)) 
    \ALUResult_out_OBUF[11]_inst_i_4 
       (.I0(\Dout_reg[25]_rep_3 ),
        .I1(\ALUResult_out_OBUF[29]_inst_i_3_0 [11]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(Q[11]),
        .O(\ALUResult_out_OBUF[11]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hA0AF3030A0AF3F3F)) 
    \ALUResult_out_OBUF[11]_inst_i_5 
       (.I0(\ALU/p_0_out [8]),
        .I1(SrcB[10]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(SrcB[9]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(\ALUResult_out_OBUF[11]_inst_i_7_n_0 ),
        .O(\ALUResult_out_OBUF[11]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFF4FFFFFFF7)) 
    \ALUResult_out_OBUF[11]_inst_i_6 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [13]),
        .I1(\Dout_reg[21]_rep_0 [1]),
        .I2(Instr_out_OBUF[25]),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(\Dout_reg[26]_rep_1 ),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [11]),
        .O(\ALUResult_out_OBUF[11]_inst_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h0002)) 
    \ALUResult_out_OBUF[11]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [11]),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[25]),
        .O(\ALUResult_out_OBUF[11]_inst_i_7_n_0 ));
  MUXF7 \ALUResult_out_OBUF[12]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[12]_inst_i_2_n_0 ),
        .I1(\Dout_reg[12]_0 ),
        .O(D[12]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'h3C5A3C5AE800E8FF)) 
    \ALUResult_out_OBUF[12]_inst_i_2 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(Q[12]),
        .I2(\ALUResult_out_OBUF[12]_inst_i_4_n_0 ),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[12]_1 ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[12]_inst_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0002)) 
    \ALUResult_out_OBUF[12]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [12]),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[25]),
        .O(\ALUResult_out_OBUF[12]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hE0EFE0EFE0E0EFEF)) 
    \ALUResult_out_OBUF[13]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[13]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[13]_inst_i_3_n_0 ),
        .I2(\Dout_reg[25]_rep_0 ),
        .I3(\ALUResult_out_OBUF[13]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[13]_inst_i_5_n_0 ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(D[13]));
  LUT5 #(
    .INIT(32'hD1D1D100)) 
    \ALUResult_out_OBUF[13]_inst_i_2 
       (.I0(\Dout_reg[13]_1 ),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .I2(\Dout_reg[13]_2 ),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[13]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00000074)) 
    \ALUResult_out_OBUF[13]_inst_i_3 
       (.I0(\Dout_reg[13]_0 ),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .I2(\ALUResult_out_OBUF[14]_inst_i_7_n_0 ),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[13]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h88877787)) 
    \ALUResult_out_OBUF[13]_inst_i_4 
       (.I0(\Dout_reg[25]_rep_3 ),
        .I1(\ALUResult_out_OBUF[29]_inst_i_3_0 [13]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(Q[13]),
        .O(\ALUResult_out_OBUF[13]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h101F7070101F7F7F)) 
    \ALUResult_out_OBUF[13]_inst_i_5 
       (.I0(Q[13]),
        .I1(\ALUResult_out_OBUF[13]_inst_i_9_n_0 ),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [6]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [6]),
        .O(\ALUResult_out_OBUF[13]_inst_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0002)) 
    \ALUResult_out_OBUF[13]_inst_i_9 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [13]),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[25]),
        .O(\ALUResult_out_OBUF[13]_inst_i_9_n_0 ));
  MUXF7 \ALUResult_out_OBUF[14]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[14]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[14]_inst_i_3_n_0 ),
        .O(D[14]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'hFA5500FAE44EE44E)) 
    \ALUResult_out_OBUF[14]_inst_i_2 
       (.I0(\Dout_reg[25]_rep_1 ),
        .I1(\Dout_reg[14]_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\ALU/p_0_out [14]),
        .I4(Q[14]),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[14]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDCDC8C8C8)) 
    \ALUResult_out_OBUF[14]_inst_i_3 
       (.I0(\Dout_reg[26]_rep_0 ),
        .I1(\ALUResult_out_OBUF[14]_inst_i_6_n_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\ALUResult_out_OBUF[14]_inst_i_7_n_0 ),
        .I4(\Dout_reg[21]_rep_0 [0]),
        .I5(\Dout_reg[15]_3 ),
        .O(\ALUResult_out_OBUF[14]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[14]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [14]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [11]),
        .O(\ALU/p_0_out [14]));
  LUT6 #(
    .INIT(64'h505FC0C0505FCFCF)) 
    \ALUResult_out_OBUF[14]_inst_i_6 
       (.I0(\ALU/p_0_out [17]),
        .I1(SrcB[15]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\ALU/p_0_out [16]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(\ALU/p_0_out [14]),
        .O(\ALUResult_out_OBUF[14]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00000000000000B8)) 
    \ALUResult_out_OBUF[14]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [11]),
        .I1(\Dout_reg[21]_rep_0 [1]),
        .I2(\ALUResult_out_OBUF[29]_inst_i_3_0 [13]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[27]_rep_n_0 ),
        .I5(Instr_out_OBUF[25]),
        .O(\ALUResult_out_OBUF[14]_inst_i_7_n_0 ));
  MUXF7 \ALUResult_out_OBUF[15]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[15]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[15]_inst_i_3_n_0 ),
        .O(D[15]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'h55FAFA004E4EE4E4)) 
    \ALUResult_out_OBUF[15]_inst_i_2 
       (.I0(\Dout_reg[25]_rep_1 ),
        .I1(\Dout_reg[15]_2 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(Q[15]),
        .I4(SrcB[15]),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[15]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDCDC8C8C8)) 
    \ALUResult_out_OBUF[15]_inst_i_3 
       (.I0(\Dout_reg[26]_rep_0 ),
        .I1(\ALUResult_out_OBUF[15]_inst_i_6_n_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[15]_3 ),
        .I4(\Dout_reg[21]_rep_0 [0]),
        .I5(\Dout_reg[15]_4 ),
        .O(\ALUResult_out_OBUF[15]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[15]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [15]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [12]),
        .O(SrcB[15]));
  LUT6 #(
    .INIT(64'h505F3F3F505F3030)) 
    \ALUResult_out_OBUF[15]_inst_i_6 
       (.I0(\ALU/p_0_out [18]),
        .I1(\ALU/p_0_out [16]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\ALU/p_0_out [17]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(SrcB[15]),
        .O(\ALUResult_out_OBUF[15]_inst_i_6_n_0 ));
  MUXF7 \ALUResult_out_OBUF[16]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[16]_inst_i_2_n_0 ),
        .I1(\Dout_reg[16]_0 ),
        .O(D[16]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'hC399C399B200B2FF)) 
    \ALUResult_out_OBUF[16]_inst_i_2 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(\ALU/p_0_out [16]),
        .I2(Q[16]),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[16]_1 ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[16]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[16]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [16]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [13]),
        .O(\ALU/p_0_out [16]));
  LUT6 #(
    .INIT(64'hE0EFE0EFE0E0EFEF)) 
    \ALUResult_out_OBUF[17]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[17]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[17]_inst_i_3_n_0 ),
        .I2(\Dout_reg[25]_rep_0 ),
        .I3(\ALUResult_out_OBUF[17]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[17]_inst_i_5_n_0 ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(D[17]));
  LUT5 #(
    .INIT(32'hD1D1D100)) 
    \ALUResult_out_OBUF[17]_inst_i_2 
       (.I0(\Dout_reg[17]_1 ),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .I2(\Dout_reg[17]_2 ),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[17]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0000002E)) 
    \ALUResult_out_OBUF[17]_inst_i_3 
       (.I0(\Dout_reg[17] ),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .I2(\Dout_reg[17]_0 ),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[17]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hE2E2E21D1D1DE21D)) 
    \ALUResult_out_OBUF[17]_inst_i_4 
       (.I0(\Dout_reg[28]_1 [14]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_3_0 [17]),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(Q[17]),
        .O(\ALUResult_out_OBUF[17]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h202FB0B0202FBFBF)) 
    \ALUResult_out_OBUF[17]_inst_i_5 
       (.I0(\ALU/p_0_out [17]),
        .I1(Q[17]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [7]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [7]),
        .O(\ALUResult_out_OBUF[17]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[17]_inst_i_9 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [17]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [14]),
        .O(\ALU/p_0_out [17]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \ALUResult_out_OBUF[18]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[18]_inst_i_2_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[18]_inst_i_3_n_0 ),
        .I3(\Dout_reg[25]_rep_1 ),
        .I4(\ALUResult_out_OBUF[18]_inst_i_4_n_0 ),
        .O(D[18]));
  LUT6 #(
    .INIT(64'hCDC8CDCDCDC8C8C8)) 
    \ALUResult_out_OBUF[18]_inst_i_2 
       (.I0(\Dout_reg[26]_rep_0 ),
        .I1(\ALUResult_out_OBUF[18]_inst_i_5_n_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[17] ),
        .I4(\Dout_reg[21]_rep_0 [0]),
        .I5(\Dout_reg[19]_2 ),
        .O(\ALUResult_out_OBUF[18]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1D1D1DE2E2E21DE2)) 
    \ALUResult_out_OBUF[18]_inst_i_3 
       (.I0(\Dout_reg[28]_1 [15]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_3_0 [18]),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(Q[18]),
        .O(\ALUResult_out_OBUF[18]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF0FF00F0ACACACAC)) 
    \ALUResult_out_OBUF[18]_inst_i_4 
       (.I0(\Dout_reg[30]_1 [8]),
        .I1(\Dout_reg[30]_0 [8]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\ALU/p_0_out [18]),
        .I4(Q[18]),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[18]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    \ALUResult_out_OBUF[18]_inst_i_5 
       (.I0(\ALU/p_0_out [21]),
        .I1(\ALU/p_0_out [19]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\ALU/p_0_out [20]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(\ALU/p_0_out [18]),
        .O(\ALUResult_out_OBUF[18]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[18]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [18]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [15]),
        .O(\ALU/p_0_out [18]));
  LUT5 #(
    .INIT(32'h8B8B88BB)) 
    \ALUResult_out_OBUF[19]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[19]_inst_i_2_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[19]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[19]_inst_i_4_n_0 ),
        .I4(\Dout_reg[25]_rep_1 ),
        .O(D[19]));
  LUT6 #(
    .INIT(64'hFFFFFFD1000000D1)) 
    \ALUResult_out_OBUF[19]_inst_i_2 
       (.I0(\Dout_reg[19]_1 ),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .I2(\Dout_reg[19]_2 ),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(\ALUResult_out_OBUF[19]_inst_i_6_n_0 ),
        .O(\ALUResult_out_OBUF[19]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8474747B847)) 
    \ALUResult_out_OBUF[19]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [19]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\Dout_reg[28]_1 [17]),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(Q[19]),
        .O(\ALUResult_out_OBUF[19]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h202FB0B0202FBFBF)) 
    \ALUResult_out_OBUF[19]_inst_i_4 
       (.I0(\ALU/p_0_out [19]),
        .I1(Q[19]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [9]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [9]),
        .O(\ALUResult_out_OBUF[19]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0F000FFF55335533)) 
    \ALUResult_out_OBUF[19]_inst_i_6 
       (.I0(\ALU/p_0_out [21]),
        .I1(\ALU/p_0_out [19]),
        .I2(\ALU/p_0_out [22]),
        .I3(\Dout_reg[21]_rep_0 [1]),
        .I4(\ALU/p_0_out [20]),
        .I5(\Dout_reg[21]_rep_0 [0]),
        .O(\ALUResult_out_OBUF[19]_inst_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h55545557)) 
    \ALUResult_out_OBUF[19]_inst_i_7 
       (.I0(\Dout_reg[28]_1 [17]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_0 [19]),
        .O(\ALU/p_0_out [19]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \ALUResult_out_OBUF[1]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[1]_inst_i_2_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[1]_inst_i_3_n_0 ),
        .I3(\Dout_reg[25]_rep_1 ),
        .I4(\ALUResult_out_OBUF[1]_inst_i_4_n_0 ),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hCDC8CDCDCDC8C8C8)) 
    \ALUResult_out_OBUF[1]_inst_i_2 
       (.I0(\Dout_reg[26]_rep_0 ),
        .I1(\ALUResult_out_OBUF[1]_inst_i_5_n_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\ALUResult_out_OBUF[1]_inst_i_6_n_0 ),
        .I4(\Dout_reg[21]_rep_0 [0]),
        .I5(\Dout_reg[8]_rep_0 ),
        .O(\ALUResult_out_OBUF[1]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1D1D1DE2E2E21DE2)) 
    \ALUResult_out_OBUF[1]_inst_i_3 
       (.I0(\Dout_reg[28]_1 [1]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_3_0 [1]),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(Q[1]),
        .O(\ALUResult_out_OBUF[1]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hDFD04F4FDFD04040)) 
    \ALUResult_out_OBUF[1]_inst_i_4 
       (.I0(\ALU/p_0_out [1]),
        .I1(Q[1]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [1]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [1]),
        .O(\ALUResult_out_OBUF[1]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[1]_inst_i_5 
       (.I0(SrcB[4]),
        .I1(SrcB[2]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(SrcB[3]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(SrcB[1]),
        .O(\ALUResult_out_OBUF[1]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h4444444544444440)) 
    \ALUResult_out_OBUF[1]_inst_i_6 
       (.I0(\Dout_reg[21]_rep_0 [1]),
        .I1(\Dout_reg[28]_1 [0]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [0]),
        .O(\ALUResult_out_OBUF[1]_inst_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[1]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [1]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [1]),
        .O(\ALU/p_0_out [1]));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[1]_inst_i_8 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [1]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [1]),
        .O(SrcB[1]));
  MUXF7 \ALUResult_out_OBUF[20]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[20]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .O(D[20]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'hCFC0303FC505F5C5)) 
    \ALUResult_out_OBUF[20]_inst_i_2 
       (.I0(\Dout_reg[20]_0 ),
        .I1(Q[20]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\ALU/p_0_out [20]),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[20]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF1015BABF)) 
    \ALUResult_out_OBUF[20]_inst_i_3 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(\Dout_reg[19]_1 ),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\Dout_reg[20]_1 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_7_n_0 ),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[20]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [20]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [16]),
        .O(\ALU/p_0_out [20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \ALUResult_out_OBUF[20]_inst_i_7 
       (.I0(\ALU/p_0_out [23]),
        .I1(\ALU/p_0_out [21]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\ALU/p_0_out [22]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(\ALU/p_0_out [20]),
        .O(\ALUResult_out_OBUF[20]_inst_i_7_n_0 ));
  MUXF7 \ALUResult_out_OBUF[21]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[21]_inst_i_2_n_0 ),
        .I1(\Dout_reg[21]_1 ),
        .O(D[21]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'hC399C399B200B2FF)) 
    \ALUResult_out_OBUF[21]_inst_i_2 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(\ALU/p_0_out [21]),
        .I2(Q[21]),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[21]_2 ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[21]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h55545557)) 
    \ALUResult_out_OBUF[21]_inst_i_4 
       (.I0(\Dout_reg[28]_1 [17]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_0 [21]),
        .O(\ALU/p_0_out [21]));
  LUT6 #(
    .INIT(64'hE0EFE0EFE0E0EFEF)) 
    \ALUResult_out_OBUF[22]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[22]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[22]_inst_i_3_n_0 ),
        .I2(\Dout_reg[25]_rep_0 ),
        .I3(\ALUResult_out_OBUF[22]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[22]_inst_i_5_n_0 ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(D[22]));
  LUT5 #(
    .INIT(32'h0000002E)) 
    \ALUResult_out_OBUF[22]_inst_i_2 
       (.I0(\Dout_reg[22]_2 ),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .I2(\Dout_reg[22]_3 ),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[22]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hD1D1D100)) 
    \ALUResult_out_OBUF[22]_inst_i_3 
       (.I0(\Dout_reg[22]_0 ),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .I2(\Dout_reg[22]_1 ),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[22]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hE2E2E21D1D1DE21D)) 
    \ALUResult_out_OBUF[22]_inst_i_4 
       (.I0(\Dout_reg[28]_1 [18]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_3_0 [22]),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(Q[22]),
        .O(\ALUResult_out_OBUF[22]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h202FB0B0202FBFBF)) 
    \ALUResult_out_OBUF[22]_inst_i_5 
       (.I0(\ALU/p_0_out [22]),
        .I1(Q[22]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [10]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [10]),
        .O(\ALUResult_out_OBUF[22]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[22]_inst_i_8 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [22]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [18]),
        .O(\ALU/p_0_out [22]));
  MUXF7 \ALUResult_out_OBUF[23]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_2_n_0 ),
        .I1(\Dout_reg[23]_2 ),
        .O(D[23]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'hFA5500FAE44EE44E)) 
    \ALUResult_out_OBUF[23]_inst_i_2 
       (.I0(\Dout_reg[25]_rep_1 ),
        .I1(\Dout_reg[23]_3 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\ALU/p_0_out [23]),
        .I4(Q[23]),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[23]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[23]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [23]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [19]),
        .O(\ALU/p_0_out [23]));
  LUT5 #(
    .INIT(32'h88BB8B8B)) 
    \ALUResult_out_OBUF[24]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[24]_inst_i_2_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[24]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .I4(\Dout_reg[25]_rep_1 ),
        .O(D[24]));
  LUT6 #(
    .INIT(64'h0202FEFEFE02FE02)) 
    \ALUResult_out_OBUF[24]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[24]_inst_i_5_n_0 ),
        .I1(\Dout_reg[23]_rep_0 ),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[24]_0 ),
        .I4(\Dout_reg[24]_1 ),
        .I5(\Dout_reg[21]_rep_0 [0]),
        .O(\ALUResult_out_OBUF[24]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h202FB0B0202FBFBF)) 
    \ALUResult_out_OBUF[24]_inst_i_3 
       (.I0(\ALU/p_0_out [24]),
        .I1(Q[24]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [11]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [11]),
        .O(\ALUResult_out_OBUF[24]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8474747B847)) 
    \ALUResult_out_OBUF[24]_inst_i_4 
       (.I0(Q[24]),
        .I1(\Dout_reg[26]_rep_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[28]_1 [20]),
        .I4(\Dout_reg[25]_rep_3 ),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [24]),
        .O(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h05F5030305F5F3F3)) 
    \ALUResult_out_OBUF[24]_inst_i_5 
       (.I0(\ALU/p_0_out [22]),
        .I1(\ALU/p_0_out [24]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\ALU/p_0_out [21]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(\ALU/p_0_out [23]),
        .O(\ALUResult_out_OBUF[24]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[24]_inst_i_8 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [24]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [20]),
        .O(\ALU/p_0_out [24]));
  LUT5 #(
    .INIT(32'h88BB8B8B)) 
    \ALUResult_out_OBUF[25]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[25]_inst_i_2_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[25]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[25]_inst_i_4_n_0 ),
        .I4(\Dout_reg[25]_rep_1 ),
        .O(D[25]));
  LUT6 #(
    .INIT(64'h0000FFFF1015BABF)) 
    \ALUResult_out_OBUF[25]_inst_i_2 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(\Dout_reg[25]_0 ),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\Dout_reg[26]_0 ),
        .I4(\ALUResult_out_OBUF[25]_inst_i_6_n_0 ),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[25]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h101F7070101F7F7F)) 
    \ALUResult_out_OBUF[25]_inst_i_3 
       (.I0(Q[25]),
        .I1(SrcB[25]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [12]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [12]),
        .O(\ALUResult_out_OBUF[25]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8474747B847)) 
    \ALUResult_out_OBUF[25]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [25]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\Dout_reg[28]_1 [21]),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(Q[25]),
        .O(\ALUResult_out_OBUF[25]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hCFC05F5FCFC05050)) 
    \ALUResult_out_OBUF[25]_inst_i_6 
       (.I0(SrcB[26]),
        .I1(\ALU/p_0_out [28]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\ALU/p_0_out [27]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(\ALU/p_0_out [25]),
        .O(\ALUResult_out_OBUF[25]_inst_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[25]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [25]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [21]),
        .O(SrcB[25]));
  LUT5 #(
    .INIT(32'h55545557)) 
    \ALUResult_out_OBUF[25]_inst_i_8 
       (.I0(\Dout_reg[28]_1 [21]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_0 [25]),
        .O(\ALU/p_0_out [25]));
  LUT5 #(
    .INIT(32'h88BB8B8B)) 
    \ALUResult_out_OBUF[26]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[26]_inst_i_2_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[26]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[26]_inst_i_4_n_0 ),
        .I4(\Dout_reg[25]_rep_1 ),
        .O(D[26]));
  LUT6 #(
    .INIT(64'h555555555555303F)) 
    \ALUResult_out_OBUF[26]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[26]_inst_i_5_n_0 ),
        .I1(\Dout_reg[26]_0 ),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\Dout_reg[26]_1 ),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[26]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1010707F1F1F707F)) 
    \ALUResult_out_OBUF[26]_inst_i_3 
       (.I0(Q[26]),
        .I1(SrcB[26]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_0 [13]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_1 [13]),
        .O(\ALUResult_out_OBUF[26]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hB847B8B8B8474747)) 
    \ALUResult_out_OBUF[26]_inst_i_4 
       (.I0(Q[26]),
        .I1(\Dout_reg[26]_rep_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\ALUResult_out_OBUF[29]_inst_i_3_0 [26]),
        .I4(\Dout_reg[25]_rep_3 ),
        .I5(\Dout_reg[28]_1 [21]),
        .O(\ALUResult_out_OBUF[26]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFAFA0A0303F303F)) 
    \ALUResult_out_OBUF[26]_inst_i_5 
       (.I0(\ALU/p_0_out [29]),
        .I1(SrcB[27]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(SrcB[26]),
        .I4(\ALU/p_0_out [28]),
        .I5(\Dout_reg[21]_rep_0 [1]),
        .O(\ALUResult_out_OBUF[26]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[26]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [26]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [21]),
        .O(SrcB[26]));
  MUXF7 \ALUResult_out_OBUF[27]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[27]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[27]_inst_i_3_n_0 ),
        .O(D[27]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'hCCC333C355555555)) 
    \ALUResult_out_OBUF[27]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[27]_inst_i_4_n_0 ),
        .I1(\ALU/p_0_out [27]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(Q[27]),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[27]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF1015BABF)) 
    \ALUResult_out_OBUF[27]_inst_i_3 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(\Dout_reg[26]_1 ),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\Dout_reg[27]_2 ),
        .I4(\ALUResult_out_OBUF[27]_inst_i_7_n_0 ),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[27]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h101F7070101F7F7F)) 
    \ALUResult_out_OBUF[27]_inst_i_4 
       (.I0(Q[27]),
        .I1(SrcB[27]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [14]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [14]),
        .O(\ALUResult_out_OBUF[27]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h55545557)) 
    \ALUResult_out_OBUF[27]_inst_i_5 
       (.I0(\Dout_reg[28]_1 [21]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_0 [27]),
        .O(\ALU/p_0_out [27]));
  LUT6 #(
    .INIT(64'hAFA0C0C0AFA0CFCF)) 
    \ALUResult_out_OBUF[27]_inst_i_7 
       (.I0(\ALU/p_0_out [30]),
        .I1(\ALU/p_0_out [28]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\ALU/p_0_out [29]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(SrcB[27]),
        .O(\ALUResult_out_OBUF[27]_inst_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[27]_inst_i_8 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [27]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [21]),
        .O(SrcB[27]));
  LUT5 #(
    .INIT(32'h74777444)) 
    \ALUResult_out_OBUF[28]_inst_i_1 
       (.I0(\Dout_reg[28]_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[28]_inst_i_3_n_0 ),
        .I3(\Dout_reg[25]_rep_1 ),
        .I4(\ALUResult_out_OBUF[28]_inst_i_4_n_0 ),
        .O(D[28]));
  LUT3 #(
    .INIT(8'h01)) 
    \ALUResult_out_OBUF[28]_inst_i_10 
       (.I0(Instr_out_OBUF[25]),
        .I1(\Dout_reg[27]_rep_n_0 ),
        .I2(\Dout_reg[26]_rep_1 ),
        .O(\Dout_reg[25]_rep_3 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[28]_inst_i_11 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [28]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [21]),
        .O(\ALU/p_0_out [28]));
  LUT6 #(
    .INIT(64'h1D1D1DE2E2E21DE2)) 
    \ALUResult_out_OBUF[28]_inst_i_3 
       (.I0(\Dout_reg[28]_1 [21]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_3_0 [28]),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(Q[28]),
        .O(\ALUResult_out_OBUF[28]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hDFD04F4FDFD04040)) 
    \ALUResult_out_OBUF[28]_inst_i_4 
       (.I0(\ALU/p_0_out [28]),
        .I1(Q[28]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [15]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [15]),
        .O(\ALUResult_out_OBUF[28]_inst_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ALUResult_out_OBUF[28]_inst_i_7 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(\Dout_reg[26]_rep_0 ),
        .O(\Dout_reg[23]_rep_1 ));
  LUT6 #(
    .INIT(64'hE0EEE0E0EEEEEEEE)) 
    \ALUResult_out_OBUF[29]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[29]_inst_i_3_n_0 ),
        .I2(\Dout_reg[25]_rep_0 ),
        .I3(\ALUResult_out_OBUF[29]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_5_n_0 ),
        .I5(\ALUResult_out_OBUF[29]_inst_i_6_n_0 ),
        .O(D[29]));
  LUT6 #(
    .INIT(64'h020202A2A2A202A2)) 
    \ALUResult_out_OBUF[29]_inst_i_10 
       (.I0(\Dout_reg[21]_rep_0 [0]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[25]_rep_3 ),
        .I3(\ALUResult_out_OBUF[29]_inst_i_3_0 [28]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [26]),
        .O(\ALUResult_out_OBUF[29]_inst_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \ALUResult_out_OBUF[29]_inst_i_11 
       (.I0(\Dout_reg[21]_rep_0 [1]),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .O(\ALUResult_out_OBUF[29]_inst_i_11_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[29]_inst_i_12 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [29]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [21]),
        .O(\ALU/p_0_out [29]));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[29]_inst_i_13 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [0]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [0]),
        .O(SrcB[0]));
  LUT5 #(
    .INIT(32'hF7777733)) 
    \ALUResult_out_OBUF[29]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_7_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_8_n_0 ),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[23]_rep_0 ),
        .O(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0004000400045504)) 
    \ALUResult_out_OBUF[29]_inst_i_3 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(\ALUResult_out_OBUF[29]_inst_i_9_n_0 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_10_n_0 ),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_11_n_0 ),
        .I5(\ALUResult_out_OBUF[30]_inst_i_12_n_0 ),
        .O(\ALUResult_out_OBUF[29]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAEAAEFAA)) 
    \ALUResult_out_OBUF[29]_inst_i_4 
       (.I0(\Dout_reg[25]_rep_1 ),
        .I1(\ALU/p_0_out [29]),
        .I2(Q[29]),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[23]_rep_0 ),
        .O(\ALUResult_out_OBUF[29]_inst_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFEAE)) 
    \ALUResult_out_OBUF[29]_inst_i_5 
       (.I0(\Dout_reg[26]_rep_0 ),
        .I1(\Dout_reg[30]_0 [16]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[30]_1 [16]),
        .O(\ALUResult_out_OBUF[29]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h47B8FFFF)) 
    \ALUResult_out_OBUF[29]_inst_i_6 
       (.I0(Q[29]),
        .I1(\Dout_reg[26]_rep_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\ALU/p_0_out [29]),
        .I4(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[29]_inst_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \ALUResult_out_OBUF[29]_inst_i_7 
       (.I0(\ALU/p_0_out [30]),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .I2(\ALUResult_out_OBUF[30]_inst_i_12_n_0 ),
        .I3(\Dout_reg[21]_rep_0 [1]),
        .I4(\ALU/p_0_out [29]),
        .O(\ALUResult_out_OBUF[29]_inst_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFF0000B8000000B8)) 
    \ALUResult_out_OBUF[29]_inst_i_8 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [29]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\Dout_reg[28]_1 [21]),
        .I3(\Dout_reg[21]_rep_0 [1]),
        .I4(\Dout_reg[21]_rep_0 [0]),
        .I5(SrcB[0]),
        .O(\ALUResult_out_OBUF[29]_inst_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFEFEFEAEAEAEFEAE)) 
    \ALUResult_out_OBUF[29]_inst_i_9 
       (.I0(\Dout_reg[21]_rep_0 [0]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[25]_rep_3 ),
        .I3(\ALUResult_out_OBUF[29]_inst_i_3_0 [29]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [27]),
        .O(\ALUResult_out_OBUF[29]_inst_i_9_n_0 ));
  MUXF7 \ALUResult_out_OBUF[2]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[2]_inst_i_2_n_0 ),
        .I1(\Dout_reg[2]_0 ),
        .O(D[2]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'h3C5A3C5AE800E8FF)) 
    \ALUResult_out_OBUF[2]_inst_i_2 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(Q[2]),
        .I2(SrcB[2]),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[2]_1 ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[2]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[2]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [2]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [2]),
        .O(SrcB[2]));
  LUT6 #(
    .INIT(64'h4444444544444440)) 
    \ALUResult_out_OBUF[2]_inst_i_8 
       (.I0(\Dout_reg[21]_rep_0 [1]),
        .I1(\Dout_reg[28]_1 [1]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [1]),
        .O(\Dout_reg[8]_rep_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEEE0EEE0E0)) 
    \ALUResult_out_OBUF[30]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[30]_inst_i_3_n_0 ),
        .I2(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I3(\ALUResult_out_OBUF[30]_inst_i_5_n_0 ),
        .I4(\ALUResult_out_OBUF[30]_inst_i_6_n_0 ),
        .I5(\Dout_reg[25]_rep_0 ),
        .O(D[30]));
  LUT6 #(
    .INIT(64'hFFFABBFAEEFAAAFA)) 
    \ALUResult_out_OBUF[30]_inst_i_10 
       (.I0(\Dout_reg[21]_rep_0 [0]),
        .I1(\Dout_reg[21]_rep_0 [1]),
        .I2(\Dout_reg[28]_1 [21]),
        .I3(\Dout_reg[25]_rep_3 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_0 [28]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [30]),
        .O(\ALUResult_out_OBUF[30]_inst_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h020202A2A2A202A2)) 
    \ALUResult_out_OBUF[30]_inst_i_11 
       (.I0(\Dout_reg[21]_rep_0 [0]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[25]_rep_3 ),
        .I3(\ALUResult_out_OBUF[29]_inst_i_3_0 [29]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [27]),
        .O(\ALUResult_out_OBUF[30]_inst_i_11_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[30]_inst_i_12 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [31]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [21]),
        .O(\ALUResult_out_OBUF[30]_inst_i_12_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \ALUResult_out_OBUF[30]_inst_i_13 
       (.I0(\Dout_reg[21]_rep_0 [1]),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .O(\ALUResult_out_OBUF[30]_inst_i_13_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[30]_inst_i_14 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [30]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [21]),
        .O(\ALU/p_0_out [30]));
  LUT6 #(
    .INIT(64'h8888888A88888880)) 
    \ALUResult_out_OBUF[30]_inst_i_15 
       (.I0(\Dout_reg[21]_rep_0 [1]),
        .I1(\Dout_reg[28]_1 [1]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [1]),
        .O(\ALUResult_out_OBUF[30]_inst_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h4444444544444440)) 
    \ALUResult_out_OBUF[30]_inst_i_16 
       (.I0(\Dout_reg[21]_rep_0 [1]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [31]),
        .O(\ALUResult_out_OBUF[30]_inst_i_16_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \ALUResult_out_OBUF[30]_inst_i_17 
       (.I0(Instr_out_OBUF[22]),
        .I1(\Dout_reg[27]_rep_n_0 ),
        .I2(\Dout_reg[26]_rep_1 ),
        .O(\ALUResult_out_OBUF[30]_inst_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hFF4FFF4FFF0F0F0F)) 
    \ALUResult_out_OBUF[30]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_7_n_0 ),
        .I1(\ALUResult_out_OBUF[30]_inst_i_8_n_0 ),
        .I2(\Dout_reg[25]_rep_0 ),
        .I3(\ALUResult_out_OBUF[30]_inst_i_9_n_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(\Dout_reg[23]_rep_0 ),
        .O(\ALUResult_out_OBUF[30]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0004550400040004)) 
    \ALUResult_out_OBUF[30]_inst_i_3 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(\ALUResult_out_OBUF[30]_inst_i_10_n_0 ),
        .I2(\ALUResult_out_OBUF[30]_inst_i_11_n_0 ),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\ALUResult_out_OBUF[30]_inst_i_12_n_0 ),
        .I5(\ALUResult_out_OBUF[30]_inst_i_13_n_0 ),
        .O(\ALUResult_out_OBUF[30]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h88822282)) 
    \ALUResult_out_OBUF[30]_inst_i_4 
       (.I0(\Dout_reg[25]_rep_1 ),
        .I1(\ALU/p_0_out [30]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(Q[30]),
        .O(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hAEAAEFAA)) 
    \ALUResult_out_OBUF[30]_inst_i_5 
       (.I0(\Dout_reg[25]_rep_1 ),
        .I1(\ALU/p_0_out [30]),
        .I2(Q[30]),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[23]_rep_0 ),
        .O(\ALUResult_out_OBUF[30]_inst_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFEAE)) 
    \ALUResult_out_OBUF[30]_inst_i_6 
       (.I0(\Dout_reg[26]_rep_0 ),
        .I1(\Dout_reg[30]_0 [17]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[30]_1 [17]),
        .O(\ALUResult_out_OBUF[30]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FFF70000)) 
    \ALUResult_out_OBUF[30]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_15_n_0 ),
        .I1(\ALUControl_out_OBUF[1]_inst_i_2_n_0 ),
        .I2(\Dout_reg[26]_rep_2 ),
        .I3(Instr_out_OBUF[22]),
        .I4(\Dout_reg[21]_rep_0 [0]),
        .I5(\ALUResult_out_OBUF[30]_inst_i_16_n_0 ),
        .O(\ALUResult_out_OBUF[30]_inst_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAABABFAAAA)) 
    \ALUResult_out_OBUF[30]_inst_i_8 
       (.I0(\Dout_reg[21]_rep_0 [0]),
        .I1(\ALUResult_out_OBUF[0]_inst_i_8_n_0 ),
        .I2(\Dout_reg[21]_rep_0 [1]),
        .I3(\ALU/p_0_out [30]),
        .I4(\ALUControl_out_OBUF[1]_inst_i_2_n_0 ),
        .I5(\ALUResult_out_OBUF[30]_inst_i_17_n_0 ),
        .O(\ALUResult_out_OBUF[30]_inst_i_8_n_0 ));
  LUT5 #(
    .INIT(32'h000000E2)) 
    \ALUResult_out_OBUF[30]_inst_i_9 
       (.I0(\Dout_reg[28]_1 [21]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_3_0 [30]),
        .I3(\Dout_reg[21]_rep_0 [0]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .O(\ALUResult_out_OBUF[30]_inst_i_9_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ALUResult_out_OBUF[31]_inst_i_1 
       (.I0(\Dout_reg[25]_rep_2 ),
        .O(D[31]));
  MUXF7 \ALUResult_out_OBUF[31]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[31]_inst_i_3_n_0 ),
        .I1(\ALUResult_out_OBUF[31]_inst_i_4_n_0 ),
        .O(\Dout_reg[25]_rep_2 ),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'h1AF17D7D1AF12828)) 
    \ALUResult_out_OBUF[31]_inst_i_3 
       (.I0(\Dout_reg[25]_rep_1 ),
        .I1(\Dout_reg[23]_rep_0 ),
        .I2(\ALUResult_out_OBUF[30]_inst_i_12_n_0 ),
        .I3(Q[31]),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(\ALUResult_out_OBUF[31]_inst_i_2_0 ),
        .O(\ALUResult_out_OBUF[31]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h3022F0F0)) 
    \ALUResult_out_OBUF[31]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_12_n_0 ),
        .I1(\ALUResult_out_OBUF[31]_inst_i_6_n_0 ),
        .I2(\ALUResult_out_OBUF[31]_inst_i_7_n_0 ),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[31]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hC000C0F050005000)) 
    \ALUResult_out_OBUF[31]_inst_i_6 
       (.I0(\ALUResult_out_OBUF[0]_inst_i_8_n_0 ),
        .I1(SrcB[2]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[21]_rep_0 [0]),
        .I4(\ALU/p_0_out [1]),
        .I5(\Dout_reg[21]_rep_0 [1]),
        .O(\ALUResult_out_OBUF[31]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hEFEAEFEAEFEFE0E0)) 
    \ALUResult_out_OBUF[31]_inst_i_7 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(\ALUResult_out_OBUF[31]_inst_i_4_0 ),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\ALU/p_0_out [29]),
        .I4(\ALUResult_out_OBUF[30]_inst_i_12_n_0 ),
        .I5(\Dout_reg[21]_rep_0 [1]),
        .O(\ALUResult_out_OBUF[31]_inst_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \ALUResult_out_OBUF[3]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[3]_inst_i_2_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[3]_inst_i_3_n_0 ),
        .I3(\Dout_reg[25]_rep_1 ),
        .I4(\ALUResult_out_OBUF[3]_inst_i_4_n_0 ),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hCDC8CDCDCDC8C8C8)) 
    \ALUResult_out_OBUF[3]_inst_i_2 
       (.I0(\Dout_reg[26]_rep_0 ),
        .I1(\ALUResult_out_OBUF[3]_inst_i_5_n_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[3]_1 ),
        .I4(\Dout_reg[21]_rep_0 [0]),
        .I5(\Dout_reg[3]_2 ),
        .O(\ALUResult_out_OBUF[3]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1D1D1DE2E2E21DE2)) 
    \ALUResult_out_OBUF[3]_inst_i_3 
       (.I0(\Dout_reg[28]_1 [3]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_3_0 [3]),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(Q[3]),
        .O(\ALUResult_out_OBUF[3]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFF0F000ACACACAC)) 
    \ALUResult_out_OBUF[3]_inst_i_4 
       (.I0(\Dout_reg[30]_1 [2]),
        .I1(\Dout_reg[30]_0 [2]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(Q[3]),
        .I4(SrcB[3]),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[3]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h5F50CFCF5F50C0C0)) 
    \ALUResult_out_OBUF[3]_inst_i_5 
       (.I0(\ALU/p_0_out [6]),
        .I1(SrcB[4]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(SrcB[5]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(SrcB[3]),
        .O(\ALUResult_out_OBUF[3]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[3]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [3]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [3]),
        .O(SrcB[3]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \ALUResult_out_OBUF[4]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[4]_inst_i_2_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[4]_inst_i_3_n_0 ),
        .I3(\Dout_reg[25]_rep_1 ),
        .I4(\ALUResult_out_OBUF[4]_inst_i_4_n_0 ),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hFFFFFFB8000000B8)) 
    \ALUResult_out_OBUF[4]_inst_i_2 
       (.I0(\Dout_reg[3]_2 ),
        .I1(\Dout_reg[21]_rep_0 [0]),
        .I2(\Dout_reg[4]_0 ),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(\ALUResult_out_OBUF[4]_inst_i_6_n_0 ),
        .O(\ALUResult_out_OBUF[4]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1D1D1DE2E2E21DE2)) 
    \ALUResult_out_OBUF[4]_inst_i_3 
       (.I0(\Dout_reg[28]_1 [4]),
        .I1(\Dout_reg[25]_rep_3 ),
        .I2(\ALUResult_out_OBUF[29]_inst_i_3_0 [4]),
        .I3(\Dout_reg[23]_rep_0 ),
        .I4(\Dout_reg[26]_rep_0 ),
        .I5(Q[4]),
        .O(\ALUResult_out_OBUF[4]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFF0F000ACACACAC)) 
    \ALUResult_out_OBUF[4]_inst_i_4 
       (.I0(\Dout_reg[30]_1 [3]),
        .I1(\Dout_reg[30]_0 [3]),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(Q[4]),
        .I4(SrcB[4]),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[4]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hA0AFCFCFA0AFC0C0)) 
    \ALUResult_out_OBUF[4]_inst_i_6 
       (.I0(SrcB[7]),
        .I1(SrcB[5]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(\ALU/p_0_out [6]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(SrcB[4]),
        .O(\ALUResult_out_OBUF[4]_inst_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[4]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [4]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [4]),
        .O(SrcB[4]));
  LUT5 #(
    .INIT(32'h8B8B88BB)) 
    \ALUResult_out_OBUF[5]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[5]_inst_i_2_n_0 ),
        .I1(\Dout_reg[25]_rep_0 ),
        .I2(\ALUResult_out_OBUF[5]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[5]_inst_i_4_n_0 ),
        .I4(\Dout_reg[25]_rep_1 ),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hCDC8CDCDCDC8C8C8)) 
    \ALUResult_out_OBUF[5]_inst_i_2 
       (.I0(\Dout_reg[26]_rep_0 ),
        .I1(\ALUResult_out_OBUF[5]_inst_i_5_n_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[4]_0 ),
        .I4(\Dout_reg[21]_rep_0 [0]),
        .I5(\Dout_reg[5]_0 ),
        .O(\ALUResult_out_OBUF[5]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8474747B847)) 
    \ALUResult_out_OBUF[5]_inst_i_3 
       (.I0(Q[5]),
        .I1(\Dout_reg[26]_rep_0 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\Dout_reg[28]_1 [5]),
        .I4(\Dout_reg[25]_rep_3 ),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [5]),
        .O(\ALUResult_out_OBUF[5]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h101F7070101F7F7F)) 
    \ALUResult_out_OBUF[5]_inst_i_4 
       (.I0(Q[5]),
        .I1(SrcB[5]),
        .I2(\Dout_reg[26]_rep_0 ),
        .I3(\Dout_reg[30]_1 [4]),
        .I4(\Dout_reg[23]_rep_0 ),
        .I5(\Dout_reg[30]_0 [4]),
        .O(\ALUResult_out_OBUF[5]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h5F503F3F5F503030)) 
    \ALUResult_out_OBUF[5]_inst_i_5 
       (.I0(\ALU/p_0_out [8]),
        .I1(\ALU/p_0_out [6]),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .I3(SrcB[7]),
        .I4(\Dout_reg[21]_rep_0 [1]),
        .I5(SrcB[5]),
        .O(\ALUResult_out_OBUF[5]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[5]_inst_i_7 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [5]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [5]),
        .O(SrcB[5]));
  MUXF7 \ALUResult_out_OBUF[6]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[6]_inst_i_2_n_0 ),
        .I1(\Dout_reg[6]_0 ),
        .O(D[6]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'hFA5500FAE44EE44E)) 
    \ALUResult_out_OBUF[6]_inst_i_2 
       (.I0(\Dout_reg[25]_rep_1 ),
        .I1(\Dout_reg[6]_1 ),
        .I2(\Dout_reg[23]_rep_0 ),
        .I3(\ALU/p_0_out [6]),
        .I4(Q[6]),
        .I5(\Dout_reg[26]_rep_0 ),
        .O(\ALUResult_out_OBUF[6]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[6]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [6]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [6]),
        .O(\ALU/p_0_out [6]));
  MUXF7 \ALUResult_out_OBUF[7]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[7]_inst_i_2_n_0 ),
        .I1(\Dout_reg[7]_2 ),
        .O(D[7]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'h3C5A3C5AE800E8FF)) 
    \ALUResult_out_OBUF[7]_inst_i_2 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(Q[7]),
        .I2(SrcB[7]),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[7]_3 ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[7]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[7]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [7]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [7]),
        .O(SrcB[7]));
  MUXF7 \ALUResult_out_OBUF[8]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_2_n_0 ),
        .I1(\Dout_reg[8]_0 ),
        .O(D[8]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'hC399C399B200B2FF)) 
    \ALUResult_out_OBUF[8]_inst_i_2 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(\ALU/p_0_out [8]),
        .I2(Q[8]),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[8]_1 ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[8]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0001FFFD)) 
    \ALUResult_out_OBUF[8]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [8]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [8]),
        .O(\ALU/p_0_out [8]));
  MUXF7 \ALUResult_out_OBUF[9]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[9]_inst_i_2_n_0 ),
        .I1(\Dout_reg[9] ),
        .O(D[9]),
        .S(\Dout_reg[25]_rep_0 ));
  LUT6 #(
    .INIT(64'h3C5A3C5AE800E8FF)) 
    \ALUResult_out_OBUF[9]_inst_i_2 
       (.I0(\Dout_reg[23]_rep_0 ),
        .I1(Q[9]),
        .I2(SrcB[9]),
        .I3(\Dout_reg[26]_rep_0 ),
        .I4(\Dout_reg[9]_0 ),
        .I5(\Dout_reg[25]_rep_1 ),
        .O(\ALUResult_out_OBUF[9]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \ALUResult_out_OBUF[9]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [9]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [9]),
        .O(SrcB[9]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Dout[0]_i_1__0 
       (.I0(Instr_out_OBUF[0]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .O(\Dout_reg[27]_rep__0_1 [0]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[0]_i_1__1 
       (.I0(\Dout_reg[31]_5 [0]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[0]),
        .O(\Dout_reg[19]_rep_rep_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[10]_i_1__0 
       (.I0(\Dout_reg[21]_rep_0 [1]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .O(\Dout_reg[27]_rep__0_1 [10]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[10]_i_1__1 
       (.I0(PCPlus8[7]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[10]),
        .O(\Dout_reg[19]_rep_rep_0 [10]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[11]_i_1__0 
       (.I0(PCPlus8[8]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[11]),
        .O(\Dout_reg[19]_rep_rep_0 [11]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[12]_i_1__0 
       (.I0(PCPlus8[9]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[12]),
        .O(\Dout_reg[19]_rep_rep_0 [12]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[13]_i_1__0 
       (.I0(PCPlus8[10]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[13]),
        .O(\Dout_reg[19]_rep_rep_0 [13]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[14]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[12]),
        .O(\Dout_reg[27]_rep__0_1 [11]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[14]_i_1__1 
       (.I0(PCPlus8[11]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[14]),
        .O(\Dout_reg[19]_rep_rep_0 [14]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[15]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[13]),
        .O(\Dout_reg[27]_rep__0_1 [12]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[15]_i_1__1 
       (.I0(PCPlus8[12]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[15]),
        .O(\Dout_reg[19]_rep_rep_0 [15]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[16]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[14]),
        .O(\Dout_reg[27]_rep__0_1 [13]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[16]_i_1__1 
       (.I0(PCPlus8[13]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[16]),
        .O(\Dout_reg[19]_rep_rep_0 [16]));
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[17]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[15]),
        .O(\Dout_reg[27]_rep__0_1 [14]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[17]_i_1__1 
       (.I0(PCPlus8[14]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[17]),
        .O(\Dout_reg[19]_rep_rep_0 [17]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[18]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[16]),
        .O(\Dout_reg[27]_rep__0_1 [15]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[18]_i_1__1 
       (.I0(PCPlus8[15]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[18]),
        .O(\Dout_reg[19]_rep_rep_0 [18]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[19]_i_1__0 
       (.I0(PCPlus8[16]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[19]),
        .O(\Dout_reg[19]_rep_rep_0 [19]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Dout[1]_i_1__0 
       (.I0(Instr_out_OBUF[1]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .O(\Dout_reg[27]_rep__0_1 [1]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[1]_i_1__1 
       (.I0(\Dout_reg[31]_5 [1]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[1]),
        .O(\Dout_reg[19]_rep_rep_0 [1]));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \Dout[1]_i_2 
       (.I0(D[9]),
        .I1(D[7]),
        .I2(D[23]),
        .I3(D[12]),
        .I4(\Dout[1]_i_6_n_0 ),
        .O(\Dout[1]_i_6_0 ));
  LUT5 #(
    .INIT(32'h00000010)) 
    \Dout[1]_i_3 
       (.I0(D[20]),
        .I1(D[6]),
        .I2(\Dout_reg[25]_rep_2 ),
        .I3(D[14]),
        .I4(\Dout[1]_i_7_n_0 ),
        .O(\Dout[1]_i_7_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \Dout[1]_i_4 
       (.I0(D[10]),
        .I1(D[2]),
        .I2(D[21]),
        .I3(D[8]),
        .I4(\Dout[1]_i_8_n_0 ),
        .O(\Dout[1]_i_8_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \Dout[1]_i_5 
       (.I0(D[16]),
        .I1(D[15]),
        .I2(D[28]),
        .I3(D[27]),
        .I4(\Dout[1]_i_9_n_0 ),
        .O(\Dout[1]_i_9_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Dout[1]_i_6 
       (.I0(D[1]),
        .I1(D[25]),
        .I2(D[5]),
        .I3(D[11]),
        .O(\Dout[1]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Dout[1]_i_7 
       (.I0(D[4]),
        .I1(D[19]),
        .I2(D[26]),
        .I3(D[30]),
        .O(\Dout[1]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Dout[1]_i_8 
       (.I0(D[22]),
        .I1(D[24]),
        .I2(D[13]),
        .I3(D[18]),
        .O(\Dout[1]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \Dout[1]_i_9 
       (.I0(D[0]),
        .I1(D[3]),
        .I2(D[17]),
        .I3(D[29]),
        .O(\Dout[1]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[20]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[18]),
        .O(\Dout_reg[27]_rep__0_1 [16]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[20]_i_1__1 
       (.I0(PCPlus8[17]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[20]),
        .O(\Dout_reg[19]_rep_rep_0 [20]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[21]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[17]),
        .O(\Dout_reg[27]_rep__0_1 [17]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[21]_i_1__1 
       (.I0(PCPlus8[18]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[21]),
        .O(\Dout_reg[19]_rep_rep_0 [21]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[22]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(\Dout_reg[20]_rep_0 ),
        .O(\Dout_reg[27]_rep__0_1 [18]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[22]_i_1__1 
       (.I0(PCPlus8[19]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[22]),
        .O(\Dout_reg[19]_rep_rep_0 [22]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[23]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(\Dout_reg[21]_rep_0 [2]),
        .O(\Dout_reg[27]_rep__0_1 [19]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[23]_i_1__1 
       (.I0(PCPlus8[20]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[23]),
        .O(\Dout_reg[19]_rep_rep_0 [23]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[24]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[22]),
        .O(\Dout_reg[27]_rep__0_1 [20]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[24]_i_1__1 
       (.I0(PCPlus8[21]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[24]),
        .O(\Dout_reg[19]_rep_rep_0 [24]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[25]_i_1__0 
       (.I0(PCPlus8[22]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[25]),
        .O(\Dout_reg[19]_rep_rep_0 [25]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[26]_i_1__0 
       (.I0(PCPlus8[23]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[26]),
        .O(\Dout_reg[19]_rep_rep_0 [26]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[27]_i_1__0 
       (.I0(PCPlus8[24]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[27]),
        .O(\Dout_reg[19]_rep_rep_0 [27]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[28]_i_1__0 
       (.I0(PCPlus8[25]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[28]),
        .O(\Dout_reg[19]_rep_rep_0 [28]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[29]_i_1__0 
       (.I0(PCPlus8[26]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[29]),
        .O(\Dout_reg[19]_rep_rep_0 [29]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[2]_i_1__0 
       (.I0(Instr_out_OBUF[0]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(Instr_out_OBUF[2]),
        .O(\Dout_reg[27]_rep__0_1 [2]));
  LUT6 #(
    .INIT(64'h575F5F5F54505050)) 
    \Dout[2]_i_1__1 
       (.I0(\Dout_reg[31]_5 [2]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[2]),
        .O(\Dout_reg[19]_rep_rep_0 [2]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[30]_i_1__0 
       (.I0(PCPlus8[27]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[30]),
        .O(\Dout_reg[19]_rep_rep_0 [30]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[31]_i_1__0 
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[23]),
        .O(\Dout_reg[27]_rep__0_1 [21]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[31]_i_1__1 
       (.I0(PCPlus8[28]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[31]),
        .O(\Dout_reg[19]_rep_rep_0 [31]));
  LUT6 #(
    .INIT(64'hFF55FC5500550C55)) 
    \Dout[31]_i_2 
       (.I0(\Dout_reg[25]_rep_2 ),
        .I1(\Dout_reg[31]_5 [31]),
        .I2(\Dout_reg[31]_4 [2]),
        .I3(\Dout_reg[31]_6 ),
        .I4(\Dout_reg[31]_4 [1]),
        .I5(Result_out_OBUF),
        .O(\Dout_reg[31]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[3]_i_1__0 
       (.I0(Instr_out_OBUF[1]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(Instr_out_OBUF[3]),
        .O(\Dout_reg[27]_rep__0_1 [3]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[3]_i_1__1 
       (.I0(PCPlus8[0]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[3]),
        .O(\Dout_reg[19]_rep_rep_0 [3]));
  LUT2 #(
    .INIT(4'hE)) 
    \Dout[3]_i_2 
       (.I0(\Dout_reg[26]_rep_1 ),
        .I1(\Dout_reg[27]_rep_n_0 ),
        .O(\Dout_reg[26]_rep_2 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[4]_i_1__0 
       (.I0(Instr_out_OBUF[2]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(Instr_out_OBUF[4]),
        .O(\Dout_reg[27]_rep__0_1 [4]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[4]_i_1__1 
       (.I0(PCPlus8[1]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[4]),
        .O(\Dout_reg[19]_rep_rep_0 [4]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[5]_i_1__0 
       (.I0(Instr_out_OBUF[3]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(Instr_out_OBUF[5]),
        .O(\Dout_reg[27]_rep__0_1 [5]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[5]_i_1__1 
       (.I0(PCPlus8[2]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[5]),
        .O(\Dout_reg[19]_rep_rep_0 [5]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[6]_i_1__0 
       (.I0(Instr_out_OBUF[4]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(Instr_out_OBUF[6]),
        .O(\Dout_reg[27]_rep__0_1 [6]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[6]_i_1__1 
       (.I0(PCPlus8[3]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[6]),
        .O(\Dout_reg[19]_rep_rep_0 [6]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[7]_i_1__0 
       (.I0(Instr_out_OBUF[5]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(\Dout_reg[21]_rep_0 [0]),
        .O(\Dout_reg[27]_rep__0_1 [7]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[7]_i_1__1 
       (.I0(PCPlus8[4]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[7]),
        .O(\Dout_reg[19]_rep_rep_0 [7]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[8]_i_1__0 
       (.I0(Instr_out_OBUF[6]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(\Dout_reg[21]_rep_0 [1]),
        .O(\Dout_reg[27]_rep__0_1 [8]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[8]_i_1__1 
       (.I0(PCPlus8[5]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[8]),
        .O(\Dout_reg[19]_rep_rep_0 [8]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[9]_i_1__0 
       (.I0(\Dout_reg[21]_rep_0 [0]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .O(\Dout_reg[27]_rep__0_1 [9]));
  LUT6 #(
    .INIT(64'hABAFAFAFA8A0A0A0)) 
    \Dout[9]_i_1__1 
       (.I0(PCPlus8[6]),
        .I1(Instr_out_OBUF[17]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(Instr_out_OBUF[16]),
        .I4(Instr_out_OBUF[18]),
        .I5(DATA_OUT1[9]),
        .O(\Dout_reg[19]_rep_rep_0 [9]));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [0]),
        .Q(\Dout_reg[31]_3 [0]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [0]),
        .Q(Instr_out_OBUF[0]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [9]),
        .Q(\Dout_reg[31]_3 [9]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [9]),
        .Q(Instr_out_OBUF[12]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [10]),
        .Q(\Dout_reg[31]_3 [10]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [10]),
        .Q(Instr_out_OBUF[13]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [11]),
        .Q(\Dout_reg[31]_3 [11]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [11]),
        .Q(Instr_out_OBUF[14]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[15]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [12]),
        .Q(\Dout_reg[31]_3 [12]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[15]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [12]),
        .Q(Instr_out_OBUF[15]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[16]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [13]),
        .Q(\Dout_reg[31]_3 [13]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[16]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [13]),
        .Q(Instr_out_OBUF[16]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[18]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [14]),
        .Q(\Dout_reg[31]_3 [14]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[18]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [14]),
        .Q(Instr_out_OBUF[18]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[19]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [15]),
        .Q(\Dout_reg[31]_3 [15]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[19]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [15]),
        .Q(\Dout_reg[19]_rep_0 ),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[19]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19]_rep_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [15]),
        .Q(Instr_out_OBUF[17]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [1]),
        .Q(\Dout_reg[31]_3 [1]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [1]),
        .Q(Instr_out_OBUF[1]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[20]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [16]),
        .Q(\Dout_reg[31]_3 [16]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[20]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [16]),
        .Q(\Dout_reg[20]_rep_0 ),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[21]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [17]),
        .Q(\Dout_reg[31]_3 [17]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[21]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [17]),
        .Q(\Dout_reg[21]_rep_0 [2]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[22]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [18]),
        .Q(\Dout_reg[31]_3 [18]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[22]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [18]),
        .Q(Instr_out_OBUF[22]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[23]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [19]),
        .Q(\Dout_reg[31]_3 [19]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[23]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [19]),
        .Q(Instr_out_OBUF[23]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[24]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [20]),
        .Q(\Dout_reg[31]_3 [20]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[24]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [20]),
        .Q(Instr_out_OBUF[24]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[25]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [21]),
        .Q(\Dout_reg[31]_3 [21]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[25]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [21]),
        .Q(Instr_out_OBUF[25]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[26]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [22]),
        .Q(\Dout_reg[31]_3 [22]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[26]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [22]),
        .Q(\Dout_reg[26]_rep_1 ),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[27]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [23]),
        .Q(\Dout_reg[31]_3 [23]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[27]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[27]_rep_0 ),
        .Q(\Dout_reg[27]_rep_n_0 ),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[27]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27]_rep__0 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [23]),
        .Q(\Dout_reg[27]_rep__0_0 ),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[28]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [24]),
        .Q(\Dout_reg[31]_3 [24]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[28]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [24]),
        .Q(Instr_out_OBUF[28]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [2]),
        .Q(\Dout_reg[31]_3 [2]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [2]),
        .Q(Instr_out_OBUF[2]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[30]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [25]),
        .Q(\Dout_reg[31]_3 [25]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[30]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [25]),
        .Q(Instr_out_OBUF[30]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[31]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [26]),
        .Q(\Dout_reg[31]_3 [26]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[31]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [26]),
        .Q(\Dout_reg[31]_rep_0 ),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[31]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31]_rep_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [26]),
        .Q(Instr_out_OBUF[29]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [3]),
        .Q(\Dout_reg[31]_3 [3]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [3]),
        .Q(Instr_out_OBUF[3]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [4]),
        .Q(\Dout_reg[31]_3 [4]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [4]),
        .Q(Instr_out_OBUF[4]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [5]),
        .Q(\Dout_reg[31]_3 [5]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [5]),
        .Q(Instr_out_OBUF[5]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [6]),
        .Q(\Dout_reg[31]_3 [6]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [6]),
        .Q(Instr_out_OBUF[6]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [7]),
        .Q(\Dout_reg[31]_3 [7]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [7]),
        .Q(\Dout_reg[21]_rep_0 [0]),
        .R(RESET_IBUF));
  (* IOB = "TRUE" *) 
  (* ORIG_CELL_NAME = "Dout_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [8]),
        .Q(\Dout_reg[31]_3 [8]),
        .R(RESET_IBUF));
  (* ORIG_CELL_NAME = "Dout_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8]_rep 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[31]_4 [0]),
        .D(\Dout_reg[31]_7 [8]),
        .Q(\Dout_reg[21]_rep_0 [1]),
        .R(RESET_IBUF));
  LUT4 #(
    .INIT(16'h8000)) 
    \FSM_onehot_current_state[10]_i_2 
       (.I0(Instr_out_OBUF[15]),
        .I1(Instr_out_OBUF[12]),
        .I2(Instr_out_OBUF[14]),
        .I3(Instr_out_OBUF[13]),
        .O(\Dout_reg[15]_rep_2 ));
  LUT6 #(
    .INIT(64'h0000000000100000)) 
    \FSM_onehot_current_state[11]_i_1 
       (.I0(\Dout_reg[26]_rep_1 ),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(Instr_out_OBUF[24]),
        .I3(Instr_out_OBUF[23]),
        .I4(\FSM_onehot_current_state_reg[13] ),
        .I5(\Dout_reg[31]_4 [0]),
        .O(\Dout_reg[24]_rep_0 [2]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'h00000020)) 
    \FSM_onehot_current_state[12]_i_1 
       (.I0(\FSM_onehot_current_state_reg[13] ),
        .I1(\Dout_reg[31]_4 [0]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(Instr_out_OBUF[24]),
        .O(\Dout_reg[24]_rep_0 [3]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'h00000800)) 
    \FSM_onehot_current_state[13]_i_1 
       (.I0(\FSM_onehot_current_state_reg[13] ),
        .I1(Instr_out_OBUF[24]),
        .I2(\Dout_reg[31]_4 [0]),
        .I3(\Dout_reg[27]_rep__0_0 ),
        .I4(\Dout_reg[26]_rep_1 ),
        .O(\Dout_reg[24]_rep_0 [4]));
  LUT6 #(
    .INIT(64'h09069A9A09069595)) 
    \FSM_onehot_current_state[2]_i_2 
       (.I0(Instr_out_OBUF[28]),
        .I1(\FSM_onehot_current_state_reg[2] ),
        .I2(Instr_out_OBUF[30]),
        .I3(Flags_in[1]),
        .I4(Instr_out_OBUF[29]),
        .I5(Flags_in[0]),
        .O(\Dout_reg[28]_rep_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \FSM_onehot_current_state[3]_i_1 
       (.I0(\Dout_reg[26]_rep_1 ),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(\FSM_onehot_current_state_reg[13] ),
        .I3(\Dout_reg[31]_4 [0]),
        .O(\Dout_reg[24]_rep_0 [0]));
  LUT6 #(
    .INIT(64'h00000000000D0000)) 
    \FSM_onehot_current_state[6]_i_1 
       (.I0(Instr_out_OBUF[24]),
        .I1(Instr_out_OBUF[23]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep__0_0 ),
        .I4(\FSM_onehot_current_state_reg[13] ),
        .I5(\Dout_reg[31]_4 [0]),
        .O(\Dout_reg[24]_rep_0 [1]));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_10
       (.I0(Instr_out_OBUF[16]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .O(ADDRA[0]));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_11
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[15]),
        .O(ADDRD[3]));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_12
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[14]),
        .O(ADDRD[2]));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_13
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[13]),
        .O(ADDRD[1]));
  LUT2 #(
    .INIT(4'h2)) 
    RF_reg_r1_0_15_0_5_i_14
       (.I0(Instr_out_OBUF[12]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .O(ADDRD[0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_0_5_i_2
       (.I0(\Dout_reg[31]_5 [1]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[1]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[1]),
        .O(DATA_IN[1]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_0_5_i_3
       (.I0(\Dout_reg[31]_5 [0]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[0]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[0]),
        .O(DATA_IN[0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_0_5_i_4
       (.I0(\Dout_reg[31]_5 [3]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[3]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[3]),
        .O(DATA_IN[3]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_0_5_i_5
       (.I0(\Dout_reg[31]_5 [2]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[2]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[2]),
        .O(DATA_IN[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_0_5_i_6
       (.I0(\Dout_reg[31]_5 [5]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[5]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[5]),
        .O(DATA_IN[5]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_0_5_i_7
       (.I0(\Dout_reg[31]_5 [4]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[4]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[4]),
        .O(DATA_IN[4]));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_8
       (.I0(\Dout_reg[27]_rep__0_0 ),
        .I1(Instr_out_OBUF[17]),
        .O(ADDRA[2]));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_9
       (.I0(Instr_out_OBUF[18]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .O(ADDRA[1]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_12_17_i_1
       (.I0(\Dout_reg[31]_5 [13]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[13]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[13]),
        .O(DATA_IN[13]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_12_17_i_2
       (.I0(\Dout_reg[31]_5 [12]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[12]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[12]),
        .O(DATA_IN[12]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_12_17_i_3
       (.I0(\Dout_reg[31]_5 [15]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[15]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[15]),
        .O(DATA_IN[15]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_12_17_i_4
       (.I0(\Dout_reg[31]_5 [14]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[14]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[14]),
        .O(DATA_IN[14]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_12_17_i_5
       (.I0(\Dout_reg[31]_5 [17]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[17]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[17]),
        .O(DATA_IN[17]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_12_17_i_6
       (.I0(\Dout_reg[31]_5 [16]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[16]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[16]),
        .O(DATA_IN[16]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_18_23_i_1
       (.I0(\Dout_reg[31]_5 [19]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[19]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[19]),
        .O(DATA_IN[19]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_18_23_i_2
       (.I0(\Dout_reg[31]_5 [18]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[18]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[18]),
        .O(DATA_IN[18]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_18_23_i_3
       (.I0(\Dout_reg[31]_5 [21]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[21]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[21]),
        .O(DATA_IN[21]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_18_23_i_4
       (.I0(\Dout_reg[31]_5 [20]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[20]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[20]),
        .O(DATA_IN[20]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_18_23_i_5
       (.I0(\Dout_reg[31]_5 [23]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[23]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[23]),
        .O(DATA_IN[23]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_18_23_i_6
       (.I0(\Dout_reg[31]_5 [22]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[22]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[22]),
        .O(DATA_IN[22]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_24_29_i_1
       (.I0(\Dout_reg[31]_5 [25]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[25]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[25]),
        .O(DATA_IN[25]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_24_29_i_2
       (.I0(\Dout_reg[31]_5 [24]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[24]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[24]),
        .O(DATA_IN[24]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_24_29_i_3
       (.I0(\Dout_reg[31]_5 [27]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[27]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[27]),
        .O(DATA_IN[27]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_24_29_i_4
       (.I0(\Dout_reg[31]_5 [26]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[26]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[26]),
        .O(DATA_IN[26]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_24_29_i_5
       (.I0(\Dout_reg[31]_5 [29]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[29]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[29]),
        .O(DATA_IN[29]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_24_29_i_6
       (.I0(\Dout_reg[31]_5 [28]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[28]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[28]),
        .O(DATA_IN[28]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_30_31_i_1
       (.I0(\Dout_reg[31]_5 [31]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[31]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[31]),
        .O(DATA_IN[31]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_30_31_i_2
       (.I0(\Dout_reg[31]_5 [30]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[30]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[30]),
        .O(DATA_IN[30]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_6_11_i_1
       (.I0(\Dout_reg[31]_5 [7]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[7]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[7]),
        .O(DATA_IN[7]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_6_11_i_2
       (.I0(\Dout_reg[31]_5 [6]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[6]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[6]),
        .O(DATA_IN[6]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_6_11_i_3
       (.I0(\Dout_reg[31]_5 [9]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[9]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[9]),
        .O(DATA_IN[9]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_6_11_i_4
       (.I0(\Dout_reg[31]_5 [8]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[8]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[8]),
        .O(DATA_IN[8]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_6_11_i_5
       (.I0(\Dout_reg[31]_5 [11]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[11]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[11]),
        .O(DATA_IN[11]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    RF_reg_r1_0_15_6_11_i_6
       (.I0(\Dout_reg[31]_5 [10]),
        .I1(\Dout_reg[27]_rep__0_0 ),
        .I2(ReadData_in_regRD[10]),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(RF_reg_r2_0_15_30_31[10]),
        .O(DATA_IN[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r2_0_15_0_5_i_1
       (.I0(Instr_out_OBUF[15]),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(Instr_out_OBUF[3]),
        .O(\Dout_reg[15]_rep_1 [3]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r2_0_15_0_5_i_2
       (.I0(Instr_out_OBUF[14]),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(Instr_out_OBUF[2]),
        .O(\Dout_reg[15]_rep_1 [2]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r2_0_15_0_5_i_3
       (.I0(Instr_out_OBUF[13]),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(Instr_out_OBUF[1]),
        .O(\Dout_reg[15]_rep_1 [1]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r2_0_15_0_5_i_4
       (.I0(Instr_out_OBUF[12]),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(Instr_out_OBUF[0]),
        .O(\Dout_reg[15]_rep_1 [0]));
  LUT6 #(
    .INIT(64'h0001FFFDFFFE0002)) 
    S0_carry__0_i_1
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [7]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [7]),
        .I5(Q[7]),
        .O(\Dout_reg[7]_0 [3]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry__0_i_1__0
       (.I0(Q[7]),
        .I1(\Dout_reg[28]_1 [7]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep__0_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [7]),
        .O(\Dout_reg[7]_1 [3]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__0_i_2
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [6]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [6]),
        .I5(Q[6]),
        .O(\Dout_reg[7]_1 [2]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__0_i_2__0
       (.I0(Q[6]),
        .I1(\Dout_reg[28]_1 [6]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep__0_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [6]),
        .O(\Dout_reg[7]_0 [2]));
  LUT6 #(
    .INIT(64'h0001FFFDFFFE0002)) 
    S0_carry__0_i_3
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [5]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [5]),
        .I5(Q[5]),
        .O(\Dout_reg[7]_0 [1]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry__0_i_3__0
       (.I0(Q[5]),
        .I1(\Dout_reg[28]_1 [5]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep__0_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [5]),
        .O(\Dout_reg[7]_1 [1]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__0_i_4
       (.I0(Q[4]),
        .I1(\Dout_reg[28]_1 [4]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep__0_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [4]),
        .O(\Dout_reg[7]_0 [0]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__0_i_4__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [4]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [4]),
        .I5(Q[4]),
        .O(\Dout_reg[7]_1 [0]));
  LUT5 #(
    .INIT(32'hAAA9AAAA)) 
    S0_carry__1_i_1
       (.I0(Q[11]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_0 [11]),
        .O(\Dout_reg[11] [3]));
  LUT5 #(
    .INIT(32'h0002FFFD)) 
    S0_carry__1_i_1__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [11]),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[25]),
        .I4(Q[11]),
        .O(\Dout_reg[11]_0 [3]));
  LUT6 #(
    .INIT(64'h0001FFFDFFFE0002)) 
    S0_carry__1_i_2
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [10]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [10]),
        .I5(Q[10]),
        .O(\Dout_reg[11] [2]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry__1_i_2__0
       (.I0(Q[10]),
        .I1(\Dout_reg[28]_1 [10]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [10]),
        .O(\Dout_reg[11]_0 [2]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__1_i_3
       (.I0(Q[9]),
        .I1(\Dout_reg[28]_1 [9]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [9]),
        .O(\Dout_reg[11] [1]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__1_i_3__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [9]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [9]),
        .I5(Q[9]),
        .O(\Dout_reg[11]_0 [1]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__1_i_4
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [8]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [8]),
        .I5(Q[8]),
        .O(\Dout_reg[11]_0 [0]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__1_i_4__0
       (.I0(Q[8]),
        .I1(\Dout_reg[28]_1 [8]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [8]),
        .O(\Dout_reg[11] [0]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__2_i_1
       (.I0(Q[15]),
        .I1(\Dout_reg[28]_1 [12]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [15]),
        .O(\Dout_reg[15]_0 [3]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__2_i_1__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [15]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [12]),
        .I5(Q[15]),
        .O(\Dout_reg[15]_1 [3]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__2_i_2
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [14]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [11]),
        .I5(Q[14]),
        .O(\Dout_reg[15]_1 [2]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__2_i_2__0
       (.I0(Q[14]),
        .I1(\Dout_reg[28]_1 [11]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [14]),
        .O(\Dout_reg[15]_0 [2]));
  LUT5 #(
    .INIT(32'hAAA9AAAA)) 
    S0_carry__2_i_3
       (.I0(Q[13]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_0 [13]),
        .O(\Dout_reg[15]_0 [1]));
  LUT5 #(
    .INIT(32'h0002FFFD)) 
    S0_carry__2_i_3__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [13]),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[25]),
        .I4(Q[13]),
        .O(\Dout_reg[15]_1 [1]));
  LUT5 #(
    .INIT(32'hAAA9AAAA)) 
    S0_carry__2_i_4
       (.I0(Q[12]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_0 [12]),
        .O(\Dout_reg[15]_0 [0]));
  LUT5 #(
    .INIT(32'h0002FFFD)) 
    S0_carry__2_i_4__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [12]),
        .I1(\Dout_reg[26]_rep_1 ),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(Instr_out_OBUF[25]),
        .I4(Q[12]),
        .O(\Dout_reg[15]_1 [0]));
  LUT6 #(
    .INIT(64'hAAABAAA855545557)) 
    S0_carry__3_i_1
       (.I0(\Dout_reg[28]_1 [17]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_0 [19]),
        .I5(Q[19]),
        .O(\Dout_reg[21]_0 [3]));
  LUT6 #(
    .INIT(64'h55555556AAAAAAA6)) 
    S0_carry__3_i_1__0
       (.I0(Q[19]),
        .I1(\ALUResult_out_OBUF[29]_inst_i_3_0 [19]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep__0_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\Dout_reg[28]_1 [17]),
        .O(\Dout_reg[19]_0 [3]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__3_i_2
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [18]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [15]),
        .I5(Q[18]),
        .O(\Dout_reg[21]_0 [2]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__3_i_2__0
       (.I0(Q[18]),
        .I1(\Dout_reg[28]_1 [15]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep__0_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [18]),
        .O(\Dout_reg[19]_0 [2]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__3_i_3
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [17]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [14]),
        .I5(Q[17]),
        .O(\Dout_reg[21]_0 [1]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__3_i_3__0
       (.I0(Q[17]),
        .I1(\Dout_reg[28]_1 [14]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep__0_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [17]),
        .O(\Dout_reg[19]_0 [1]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__3_i_4
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [16]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep__0_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [13]),
        .I5(Q[16]),
        .O(\Dout_reg[21]_0 [0]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__3_i_4__0
       (.I0(Q[16]),
        .I1(\Dout_reg[28]_1 [13]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep__0_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [16]),
        .O(\Dout_reg[19]_0 [0]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__4_i_1
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [23]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [19]),
        .I5(Q[23]),
        .O(\Dout_reg[23]_0 [3]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__4_i_1__0
       (.I0(Q[23]),
        .I1(\Dout_reg[28]_1 [19]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [23]),
        .O(\Dout_reg[23]_1 [3]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry__4_i_2
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [22]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [18]),
        .I5(Q[22]),
        .O(\Dout_reg[23]_0 [2]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__4_i_2__0
       (.I0(Q[22]),
        .I1(\Dout_reg[28]_1 [18]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [22]),
        .O(\Dout_reg[23]_1 [2]));
  LUT6 #(
    .INIT(64'hAAABAAA855545557)) 
    S0_carry__4_i_3
       (.I0(\Dout_reg[28]_1 [17]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_0 [21]),
        .I5(Q[21]),
        .O(\Dout_reg[23]_0 [1]));
  LUT6 #(
    .INIT(64'h55555556AAAAAAA6)) 
    S0_carry__4_i_3__0
       (.I0(Q[21]),
        .I1(\ALUResult_out_OBUF[29]_inst_i_3_0 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\Dout_reg[28]_1 [17]),
        .O(\Dout_reg[23]_1 [1]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry__4_i_4
       (.I0(Q[20]),
        .I1(\Dout_reg[28]_1 [16]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [20]),
        .O(\Dout_reg[23]_0 [0]));
  LUT6 #(
    .INIT(64'h0001FFFDFFFE0002)) 
    S0_carry__4_i_4__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [20]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [16]),
        .I5(Q[20]),
        .O(\Dout_reg[23]_1 [0]));
  LUT6 #(
    .INIT(64'hAAAAAAA955555559)) 
    S0_carry__5_i_1
       (.I0(Q[27]),
        .I1(\ALUResult_out_OBUF[29]_inst_i_3_0 [27]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\Dout_reg[28]_1 [21]),
        .O(\Dout_reg[27]_0 [3]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__5_i_1__0
       (.I0(Q[27]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [27]),
        .O(\Dout_reg[27]_1 [3]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__5_i_2
       (.I0(Q[26]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [26]),
        .O(\Dout_reg[27]_1 [2]));
  LUT6 #(
    .INIT(64'hAAAAAAA955555559)) 
    S0_carry__5_i_2__0
       (.I0(Q[26]),
        .I1(\ALUResult_out_OBUF[29]_inst_i_3_0 [26]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\Dout_reg[28]_1 [21]),
        .O(\Dout_reg[27]_0 [2]));
  LUT6 #(
    .INIT(64'hAAAAAAA955555559)) 
    S0_carry__5_i_3
       (.I0(Q[25]),
        .I1(\ALUResult_out_OBUF[29]_inst_i_3_0 [25]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\Dout_reg[28]_1 [21]),
        .O(\Dout_reg[27]_0 [1]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__5_i_3__0
       (.I0(Q[25]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [25]),
        .O(\Dout_reg[27]_1 [1]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry__5_i_4
       (.I0(Q[24]),
        .I1(\Dout_reg[28]_1 [20]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [24]),
        .O(\Dout_reg[27]_0 [0]));
  LUT6 #(
    .INIT(64'h0001FFFDFFFE0002)) 
    S0_carry__5_i_4__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [24]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [20]),
        .I5(Q[24]),
        .O(\Dout_reg[27]_1 [0]));
  LUT6 #(
    .INIT(64'h0001FFFDFFFE0002)) 
    S0_carry__6_i_1__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [31]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [21]),
        .I5(Q[31]),
        .O(\Dout_reg[31]_1 [3]));
  LUT6 #(
    .INIT(64'h0001FFFDFFFE0002)) 
    S0_carry__6_i_2
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [30]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [21]),
        .I5(Q[30]),
        .O(\Dout_reg[31]_1 [2]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry__6_i_2__0
       (.I0(Q[31]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [31]),
        .O(\Dout_reg[31]_0 [3]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__6_i_3
       (.I0(Q[29]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [29]),
        .O(\Dout_reg[31]_1 [1]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry__6_i_3__0
       (.I0(Q[30]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [30]),
        .O(\Dout_reg[31]_0 [2]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry__6_i_4
       (.I0(Q[29]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [29]),
        .O(\Dout_reg[31]_0 [1]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry__6_i_4__0
       (.I0(Q[28]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [28]),
        .O(\Dout_reg[31]_1 [0]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry__6_i_5
       (.I0(Q[28]),
        .I1(\Dout_reg[28]_1 [21]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [28]),
        .O(\Dout_reg[31]_0 [0]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry_i_1
       (.I0(Q[3]),
        .I1(\Dout_reg[28]_1 [3]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [3]),
        .O(\Dout_reg[3]_0 [3]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry_i_1__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [3]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [3]),
        .I5(Q[3]),
        .O(S[3]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry_i_2
       (.I0(Q[2]),
        .I1(\Dout_reg[28]_1 [2]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [2]),
        .O(\Dout_reg[3]_0 [2]));
  LUT6 #(
    .INIT(64'hFFFE00020001FFFD)) 
    S0_carry_i_2__0
       (.I0(\ALUResult_out_OBUF[29]_inst_i_3_0 [2]),
        .I1(Instr_out_OBUF[25]),
        .I2(\Dout_reg[27]_rep_n_0 ),
        .I3(\Dout_reg[26]_rep_1 ),
        .I4(\Dout_reg[28]_1 [2]),
        .I5(Q[2]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry_i_3
       (.I0(Q[1]),
        .I1(\Dout_reg[28]_1 [1]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [1]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry_i_3__0
       (.I0(Q[1]),
        .I1(\Dout_reg[28]_1 [1]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [1]),
        .O(\Dout_reg[3]_0 [1]));
  LUT6 #(
    .INIT(64'h666666656666666A)) 
    S0_carry_i_4
       (.I0(Q[0]),
        .I1(\Dout_reg[28]_1 [0]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [0]),
        .O(\Dout_reg[3]_0 [0]));
  LUT6 #(
    .INIT(64'h9999999A99999995)) 
    S0_carry_i_4__0
       (.I0(Q[0]),
        .I1(\Dout_reg[28]_1 [0]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(\Dout_reg[27]_rep_n_0 ),
        .I4(Instr_out_OBUF[25]),
        .I5(\ALUResult_out_OBUF[29]_inst_i_3_0 [0]),
        .O(S[0]));
  LUT6 #(
    .INIT(64'h8A80000000000000)) 
    \WriteData_out_OBUF[31]_inst_i_2 
       (.I0(\Dout_reg[15]_rep_1 [0]),
        .I1(Instr_out_OBUF[15]),
        .I2(\Dout_reg[26]_rep_1 ),
        .I3(Instr_out_OBUF[3]),
        .I4(\Dout_reg[15]_rep_1 [1]),
        .I5(\Dout_reg[15]_rep_1 [2]),
        .O(\Dout_reg[15]_rep_0 ));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_4
   (Q,
    RESET_IBUF,
    \Dout_reg[2]_0 ,
    D,
    CLK_IBUF_BUFG);
  output [4:0]Q;
  input RESET_IBUF;
  input [0:0]\Dout_reg[2]_0 ;
  input [4:0]D;
  input CLK_IBUF_BUFG;

  wire CLK_IBUF_BUFG;
  wire [4:0]D;
  wire [0:0]\Dout_reg[2]_0 ;
  wire [4:0]Q;
  wire RESET_IBUF;

  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[2]_0 ),
        .D(D[0]),
        .Q(Q[0]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[2]_0 ),
        .D(D[1]),
        .Q(Q[1]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[2]_0 ),
        .D(D[2]),
        .Q(Q[2]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[2]_0 ),
        .D(D[3]),
        .Q(Q[3]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(\Dout_reg[2]_0 ),
        .D(D[4]),
        .Q(Q[4]),
        .R(RESET_IBUF));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_5
   (\Dout_reg[30]_0 ,
    Q,
    \Dout_reg[2]_0 ,
    \Dout_reg[30]_1 ,
    \Dout_reg[30]_2 ,
    Result_out_OBUF,
    ALUResult_out_OBUF,
    \Dout_reg[0]_0 ,
    DATA_OUT2,
    RESET_IBUF,
    D,
    CLK_IBUF_BUFG);
  output [30:0]\Dout_reg[30]_0 ;
  output [31:0]Q;
  output [2:0]\Dout_reg[2]_0 ;
  input [1:0]\Dout_reg[30]_1 ;
  input \Dout_reg[30]_2 ;
  input [30:0]Result_out_OBUF;
  input [30:0]ALUResult_out_OBUF;
  input \Dout_reg[0]_0 ;
  input [2:0]DATA_OUT2;
  input RESET_IBUF;
  input [31:0]D;
  input CLK_IBUF_BUFG;

  wire [30:0]ALUResult_out_OBUF;
  wire CLK_IBUF_BUFG;
  wire [31:0]D;
  wire [2:0]DATA_OUT2;
  wire \Dout_reg[0]_0 ;
  wire [2:0]\Dout_reg[2]_0 ;
  wire [30:0]\Dout_reg[30]_0 ;
  wire [1:0]\Dout_reg[30]_1 ;
  wire \Dout_reg[30]_2 ;
  wire [31:0]Q;
  wire RESET_IBUF;
  wire [30:0]Result_out_OBUF;

  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[0]_i_1 
       (.I0(Q[0]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[0]),
        .I5(ALUResult_out_OBUF[0]),
        .O(\Dout_reg[30]_0 [0]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[10]_i_1 
       (.I0(Q[10]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[10]),
        .I5(ALUResult_out_OBUF[10]),
        .O(\Dout_reg[30]_0 [10]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[11]_i_1 
       (.I0(Q[11]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[11]),
        .I5(ALUResult_out_OBUF[11]),
        .O(\Dout_reg[30]_0 [11]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[12]_i_1 
       (.I0(Q[12]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[12]),
        .I5(ALUResult_out_OBUF[12]),
        .O(\Dout_reg[30]_0 [12]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[13]_i_1 
       (.I0(Q[13]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[13]),
        .I5(ALUResult_out_OBUF[13]),
        .O(\Dout_reg[30]_0 [13]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[14]_i_1 
       (.I0(Q[14]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[14]),
        .I5(ALUResult_out_OBUF[14]),
        .O(\Dout_reg[30]_0 [14]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[15]_i_1 
       (.I0(Q[15]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[15]),
        .I5(ALUResult_out_OBUF[15]),
        .O(\Dout_reg[30]_0 [15]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[16]_i_1 
       (.I0(Q[16]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[16]),
        .I5(ALUResult_out_OBUF[16]),
        .O(\Dout_reg[30]_0 [16]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[17]_i_1 
       (.I0(Q[17]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[17]),
        .I5(ALUResult_out_OBUF[17]),
        .O(\Dout_reg[30]_0 [17]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[18]_i_1 
       (.I0(Q[18]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[18]),
        .I5(ALUResult_out_OBUF[18]),
        .O(\Dout_reg[30]_0 [18]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[19]_i_1 
       (.I0(Q[19]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[19]),
        .I5(ALUResult_out_OBUF[19]),
        .O(\Dout_reg[30]_0 [19]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[1]_i_1 
       (.I0(Q[1]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[1]),
        .I5(ALUResult_out_OBUF[1]),
        .O(\Dout_reg[30]_0 [1]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[20]_i_1 
       (.I0(Q[20]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[20]),
        .I5(ALUResult_out_OBUF[20]),
        .O(\Dout_reg[30]_0 [20]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[21]_i_1 
       (.I0(Q[21]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[21]),
        .I5(ALUResult_out_OBUF[21]),
        .O(\Dout_reg[30]_0 [21]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[22]_i_1 
       (.I0(Q[22]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[22]),
        .I5(ALUResult_out_OBUF[22]),
        .O(\Dout_reg[30]_0 [22]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[23]_i_1 
       (.I0(Q[23]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[23]),
        .I5(ALUResult_out_OBUF[23]),
        .O(\Dout_reg[30]_0 [23]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[24]_i_1 
       (.I0(Q[24]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[24]),
        .I5(ALUResult_out_OBUF[24]),
        .O(\Dout_reg[30]_0 [24]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[25]_i_1 
       (.I0(Q[25]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[25]),
        .I5(ALUResult_out_OBUF[25]),
        .O(\Dout_reg[30]_0 [25]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[26]_i_1 
       (.I0(Q[26]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[26]),
        .I5(ALUResult_out_OBUF[26]),
        .O(\Dout_reg[30]_0 [26]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[27]_i_1 
       (.I0(Q[27]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[27]),
        .I5(ALUResult_out_OBUF[27]),
        .O(\Dout_reg[30]_0 [27]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[28]_i_1 
       (.I0(Q[28]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[28]),
        .I5(ALUResult_out_OBUF[28]),
        .O(\Dout_reg[30]_0 [28]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[29]_i_1 
       (.I0(Q[29]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[29]),
        .I5(ALUResult_out_OBUF[29]),
        .O(\Dout_reg[30]_0 [29]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[2]_i_1 
       (.I0(Q[2]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[2]),
        .I5(ALUResult_out_OBUF[2]),
        .O(\Dout_reg[30]_0 [2]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[30]_i_1 
       (.I0(Q[30]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[30]),
        .I5(ALUResult_out_OBUF[30]),
        .O(\Dout_reg[30]_0 [30]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[3]_i_1 
       (.I0(Q[3]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[3]),
        .I5(ALUResult_out_OBUF[3]),
        .O(\Dout_reg[30]_0 [3]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[4]_i_1 
       (.I0(Q[4]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[4]),
        .I5(ALUResult_out_OBUF[4]),
        .O(\Dout_reg[30]_0 [4]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[5]_i_1 
       (.I0(Q[5]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[5]),
        .I5(ALUResult_out_OBUF[5]),
        .O(\Dout_reg[30]_0 [5]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[6]_i_1 
       (.I0(Q[6]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[6]),
        .I5(ALUResult_out_OBUF[6]),
        .O(\Dout_reg[30]_0 [6]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[7]_i_1 
       (.I0(Q[7]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[7]),
        .I5(ALUResult_out_OBUF[7]),
        .O(\Dout_reg[30]_0 [7]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[8]_i_1 
       (.I0(Q[8]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[8]),
        .I5(ALUResult_out_OBUF[8]),
        .O(\Dout_reg[30]_0 [8]));
  LUT6 #(
    .INIT(64'hFFEF0F2FF0E00020)) 
    \Dout[9]_i_1 
       (.I0(Q[9]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[30]_2 ),
        .I3(\Dout_reg[30]_1 [0]),
        .I4(Result_out_OBUF[9]),
        .I5(ALUResult_out_OBUF[9]),
        .O(\Dout_reg[30]_0 [9]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[0]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[10]),
        .Q(Q[10]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[11]),
        .Q(Q[11]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[12]),
        .Q(Q[12]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[13]),
        .Q(Q[13]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[14]),
        .Q(Q[14]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[15]),
        .Q(Q[15]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[16]),
        .Q(Q[16]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[17]),
        .Q(Q[17]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[18]),
        .Q(Q[18]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[19]),
        .Q(Q[19]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[1]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[20]),
        .Q(Q[20]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[21]),
        .Q(Q[21]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[22]),
        .Q(Q[22]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[23]),
        .Q(Q[23]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[24]),
        .Q(Q[24]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[25]),
        .Q(Q[25]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[26]),
        .Q(Q[26]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[27]),
        .Q(Q[27]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[28]),
        .Q(Q[28]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[29]),
        .Q(Q[29]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[2]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[30]),
        .Q(Q[30]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[31]),
        .Q(Q[31]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[3]),
        .Q(Q[3]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[4]),
        .Q(Q[4]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[5]),
        .Q(Q[5]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[6]),
        .Q(Q[6]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[7]),
        .Q(Q[7]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[8]),
        .Q(Q[8]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[9]),
        .Q(Q[9]),
        .R(RESET_IBUF));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[0]_inst_i_1 
       (.I0(Q[0]),
        .I1(\Dout_reg[0]_0 ),
        .I2(DATA_OUT2[0]),
        .O(\Dout_reg[2]_0 [0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \WriteData_out_OBUF[1]_inst_i_1 
       (.I0(Q[1]),
        .I1(\Dout_reg[0]_0 ),
        .I2(DATA_OUT2[1]),
        .O(\Dout_reg[2]_0 [1]));
  LUT3 #(
    .INIT(8'h74)) 
    \WriteData_out_OBUF[2]_inst_i_1 
       (.I0(Q[2]),
        .I1(\Dout_reg[0]_0 ),
        .I2(DATA_OUT2[2]),
        .O(\Dout_reg[2]_0 [2]));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_6
   (Q,
    RESET_IBUF,
    ALUResult_out_OBUF,
    CLK_IBUF_BUFG);
  output [31:0]Q;
  input RESET_IBUF;
  input [31:0]ALUResult_out_OBUF;
  input CLK_IBUF_BUFG;

  wire [31:0]ALUResult_out_OBUF;
  wire CLK_IBUF_BUFG;
  wire [31:0]Q;
  wire RESET_IBUF;

  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[0]),
        .Q(Q[0]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[10]),
        .Q(Q[10]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[11]),
        .Q(Q[11]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[12]),
        .Q(Q[12]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[13]),
        .Q(Q[13]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[14]),
        .Q(Q[14]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[15]),
        .Q(Q[15]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[16]),
        .Q(Q[16]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[17]),
        .Q(Q[17]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[18]),
        .Q(Q[18]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[19]),
        .Q(Q[19]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[1]),
        .Q(Q[1]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[20]),
        .Q(Q[20]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[21]),
        .Q(Q[21]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[22]),
        .Q(Q[22]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[23]),
        .Q(Q[23]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[24]),
        .Q(Q[24]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[25]),
        .Q(Q[25]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[26]),
        .Q(Q[26]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[27]),
        .Q(Q[27]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[28]),
        .Q(Q[28]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[29]),
        .Q(Q[29]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[2]),
        .Q(Q[2]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[30]),
        .Q(Q[30]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[31]),
        .Q(Q[31]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[3]),
        .Q(Q[3]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[4]),
        .Q(Q[4]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[5]),
        .Q(Q[5]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[6]),
        .Q(Q[6]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[7]),
        .Q(Q[7]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[8]),
        .Q(Q[8]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(ALUResult_out_OBUF[9]),
        .Q(Q[9]),
        .R(RESET_IBUF));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_7
   (Q,
    RESET_IBUF,
    D,
    CLK_IBUF_BUFG);
  output [31:0]Q;
  input RESET_IBUF;
  input [31:0]D;
  input CLK_IBUF_BUFG;

  wire CLK_IBUF_BUFG;
  wire [31:0]D;
  wire [31:0]Q;
  wire RESET_IBUF;

  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[0]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[10]),
        .Q(Q[10]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[11]),
        .Q(Q[11]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[12]),
        .Q(Q[12]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[13]),
        .Q(Q[13]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[14]),
        .Q(Q[14]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[15]),
        .Q(Q[15]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[16]),
        .Q(Q[16]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[17]),
        .Q(Q[17]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[18]),
        .Q(Q[18]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[19]),
        .Q(Q[19]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[1]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[20]),
        .Q(Q[20]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[21]),
        .Q(Q[21]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[22]),
        .Q(Q[22]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[23]),
        .Q(Q[23]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[24]),
        .Q(Q[24]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[25]),
        .Q(Q[25]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[26]),
        .Q(Q[26]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[27]),
        .Q(Q[27]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[28]),
        .Q(Q[28]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[29]),
        .Q(Q[29]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[2]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[30]),
        .Q(Q[30]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[31]),
        .Q(Q[31]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[3]),
        .Q(Q[3]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[4]),
        .Q(Q[4]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[5]),
        .Q(Q[5]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[6]),
        .Q(Q[6]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[7]),
        .Q(Q[7]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[8]),
        .Q(Q[8]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(D[9]),
        .Q(Q[9]),
        .R(RESET_IBUF));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n__parameterized1
   (Flags_in,
    \Dout_reg[0]_0 ,
    ALUControl_out_OBUF,
    \Dout_reg[3]_0 ,
    \Dout_reg[3]_1 ,
    \Dout_reg[3]_2 ,
    FlagsWrite_in,
    \Dout_reg[1]_0 ,
    \Dout_reg[1]_1 ,
    \Dout_reg[1]_2 ,
    \Dout_reg[1]_3 ,
    RESET_IBUF,
    CLK_IBUF_BUFG,
    \Dout_reg[0]_1 );
  output [1:0]Flags_in;
  output [0:0]\Dout_reg[0]_0 ;
  input [0:0]ALUControl_out_OBUF;
  input \Dout_reg[3]_0 ;
  input [0:0]\Dout_reg[3]_1 ;
  input \Dout_reg[3]_2 ;
  input FlagsWrite_in;
  input \Dout_reg[1]_0 ;
  input \Dout_reg[1]_1 ;
  input \Dout_reg[1]_2 ;
  input \Dout_reg[1]_3 ;
  input RESET_IBUF;
  input CLK_IBUF_BUFG;
  input \Dout_reg[0]_1 ;

  wire [0:0]ALUControl_out_OBUF;
  wire CLK_IBUF_BUFG;
  wire \Dout[1]_i_1_n_0 ;
  wire \Dout[3]_i_1_n_0 ;
  wire [0:0]\Dout_reg[0]_0 ;
  wire \Dout_reg[0]_1 ;
  wire \Dout_reg[1]_0 ;
  wire \Dout_reg[1]_1 ;
  wire \Dout_reg[1]_2 ;
  wire \Dout_reg[1]_3 ;
  wire \Dout_reg[3]_0 ;
  wire [0:0]\Dout_reg[3]_1 ;
  wire \Dout_reg[3]_2 ;
  wire FlagsWrite_in;
  wire [1:0]Flags_in;
  wire RESET_IBUF;

  LUT6 #(
    .INIT(64'h0004FFFF00040000)) 
    \Dout[1]_i_1 
       (.I0(\Dout_reg[1]_0 ),
        .I1(\Dout_reg[1]_1 ),
        .I2(\Dout_reg[1]_2 ),
        .I3(\Dout_reg[1]_3 ),
        .I4(FlagsWrite_in),
        .I5(Flags_in[0]),
        .O(\Dout[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0045FFFF00450000)) 
    \Dout[3]_i_1 
       (.I0(ALUControl_out_OBUF),
        .I1(\Dout_reg[3]_0 ),
        .I2(\Dout_reg[3]_1 ),
        .I3(\Dout_reg[3]_2 ),
        .I4(FlagsWrite_in),
        .I5(Flags_in[1]),
        .O(\Dout[3]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout_reg[0]_1 ),
        .Q(\Dout_reg[0]_0 ),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout[1]_i_1_n_0 ),
        .Q(Flags_in[0]),
        .R(RESET_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\Dout[3]_i_1_n_0 ),
        .Q(Flags_in[1]),
        .R(RESET_IBUF));
endmodule

module ROM_ARRAY
   (D,
    Q);
  output [11:0]D;
  input [5:0]Q;

  wire [11:0]D;
  wire [5:0]Q;

  LUT6 #(
    .INIT(64'h0401005304510140)) 
    \Dout[12]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h0051050200005500)) 
    \Dout[13]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[0]),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(Q[1]),
        .O(D[4]));
  LUT6 #(
    .INIT(64'h4444400004444220)) 
    \Dout[14]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(Q[2]),
        .O(D[5]));
  LUT6 #(
    .INIT(64'h0004044440000200)) 
    \Dout[15]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(D[6]));
  LUT6 #(
    .INIT(64'h4100501041111311)) 
    \Dout[1]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[0]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h1104100711074007)) 
    \Dout[21]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(D[7]));
  LUT6 #(
    .INIT(64'h5554440401145777)) 
    \Dout[23]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(Q[2]),
        .O(D[8]));
  LUT6 #(
    .INIT(64'h1404151714175447)) 
    \Dout[24]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(D[9]));
  LUT6 #(
    .INIT(64'h0000005041424213)) 
    \Dout[2]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(Q[2]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'h5555555555455777)) 
    \Dout[30]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(D[10]));
  LUT6 #(
    .INIT(64'h5554545555475757)) 
    \Dout[31]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[1]),
        .I5(Q[3]),
        .O(D[11]));
  LUT6 #(
    .INIT(64'h0000000000115319)) 
    \Dout[3]_i_1 
       (.I0(Q[5]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(Q[4]),
        .O(D[2]));
endmodule

module SUBTRACTOR_n
   (\Dout_reg[30] ,
    O,
    CO,
    \Dout_reg[30]_0 ,
    \Dout_reg[3] ,
    \Dout_reg[7] ,
    \Dout_reg[7]_0 ,
    \Dout_reg[11] ,
    \Dout_reg[11]_0 ,
    \Dout_reg[11]_1 ,
    \Dout_reg[15] ,
    \Dout_reg[15]_0 ,
    \Dout_reg[15]_1 ,
    \Dout_reg[19] ,
    \Dout_reg[23] ,
    \Dout_reg[23]_0 ,
    \Dout_reg[23]_1 ,
    Q,
    S,
    \ALUResult_out_OBUF[4]_inst_i_4 ,
    \ALUResult_out_OBUF[8]_inst_i_5_0 ,
    \ALUResult_out_OBUF[12]_inst_i_5_0 ,
    \ALUResult_out_OBUF[16]_inst_i_5_0 ,
    \ALUResult_out_OBUF[20]_inst_i_4_0 ,
    \ALUResult_out_OBUF[24]_inst_i_3 ,
    DI,
    \ALUResult_out_OBUF[28]_inst_i_4 ,
    \Dout_reg[3]_0 ,
    \Dout_reg[3]_1 ,
    S_Add_in,
    \Dout_reg[3]_2 );
  output [17:0]\Dout_reg[30] ;
  output [0:0]O;
  output [0:0]CO;
  output \Dout_reg[30]_0 ;
  output \Dout_reg[3] ;
  output \Dout_reg[7] ;
  output \Dout_reg[7]_0 ;
  output \Dout_reg[11] ;
  output \Dout_reg[11]_0 ;
  output \Dout_reg[11]_1 ;
  output \Dout_reg[15] ;
  output \Dout_reg[15]_0 ;
  output \Dout_reg[15]_1 ;
  output \Dout_reg[19] ;
  output \Dout_reg[23] ;
  output \Dout_reg[23]_0 ;
  output \Dout_reg[23]_1 ;
  input [30:0]Q;
  input [3:0]S;
  input [3:0]\ALUResult_out_OBUF[4]_inst_i_4 ;
  input [3:0]\ALUResult_out_OBUF[8]_inst_i_5_0 ;
  input [3:0]\ALUResult_out_OBUF[12]_inst_i_5_0 ;
  input [3:0]\ALUResult_out_OBUF[16]_inst_i_5_0 ;
  input [3:0]\ALUResult_out_OBUF[20]_inst_i_4_0 ;
  input [3:0]\ALUResult_out_OBUF[24]_inst_i_3 ;
  input [0:0]DI;
  input [3:0]\ALUResult_out_OBUF[28]_inst_i_4 ;
  input [0:0]\Dout_reg[3]_0 ;
  input \Dout_reg[3]_1 ;
  input [13:0]S_Add_in;
  input [0:0]\Dout_reg[3]_2 ;

  wire [3:0]\ALUResult_out_OBUF[12]_inst_i_5_0 ;
  wire [3:0]\ALUResult_out_OBUF[16]_inst_i_5_0 ;
  wire [3:0]\ALUResult_out_OBUF[20]_inst_i_4_0 ;
  wire [3:0]\ALUResult_out_OBUF[24]_inst_i_3 ;
  wire [3:0]\ALUResult_out_OBUF[28]_inst_i_4 ;
  wire [3:0]\ALUResult_out_OBUF[4]_inst_i_4 ;
  wire [3:0]\ALUResult_out_OBUF[8]_inst_i_5_0 ;
  wire [0:0]CO;
  wire [0:0]DI;
  wire \Dout_reg[11] ;
  wire \Dout_reg[11]_0 ;
  wire \Dout_reg[11]_1 ;
  wire \Dout_reg[15] ;
  wire \Dout_reg[15]_0 ;
  wire \Dout_reg[15]_1 ;
  wire \Dout_reg[19] ;
  wire \Dout_reg[23] ;
  wire \Dout_reg[23]_0 ;
  wire \Dout_reg[23]_1 ;
  wire [17:0]\Dout_reg[30] ;
  wire \Dout_reg[30]_0 ;
  wire \Dout_reg[3] ;
  wire [0:0]\Dout_reg[3]_0 ;
  wire \Dout_reg[3]_1 ;
  wire [0:0]\Dout_reg[3]_2 ;
  wire \Dout_reg[7] ;
  wire \Dout_reg[7]_0 ;
  wire [0:0]O;
  wire [30:0]Q;
  wire [3:0]S;
  wire S0_carry__0_n_0;
  wire S0_carry__1_n_0;
  wire S0_carry__2_n_0;
  wire S0_carry__3_n_0;
  wire S0_carry__4_n_0;
  wire S0_carry__5_n_0;
  wire S0_carry_n_0;
  wire [13:0]S_Add_in;
  wire [23:2]S_Sub_in;
  wire [2:0]NLW_S0_carry_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__4_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__5_CO_UNCONNECTED;
  wire [2:0]NLW_S0_carry__6_CO_UNCONNECTED;

  LUT3 #(
    .INIT(8'h47)) 
    \ALUResult_out_OBUF[10]_inst_i_5 
       (.I0(S_Sub_in[10]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[5]),
        .O(\Dout_reg[11]_1 ));
  LUT3 #(
    .INIT(8'h47)) 
    \ALUResult_out_OBUF[12]_inst_i_5 
       (.I0(S_Sub_in[12]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[6]),
        .O(\Dout_reg[15] ));
  LUT3 #(
    .INIT(8'hB8)) 
    \ALUResult_out_OBUF[14]_inst_i_4 
       (.I0(S_Sub_in[14]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[7]),
        .O(\Dout_reg[15]_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \ALUResult_out_OBUF[15]_inst_i_4 
       (.I0(S_Sub_in[15]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[8]),
        .O(\Dout_reg[15]_1 ));
  LUT3 #(
    .INIT(8'h47)) 
    \ALUResult_out_OBUF[16]_inst_i_5 
       (.I0(S_Sub_in[16]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[9]),
        .O(\Dout_reg[19] ));
  LUT3 #(
    .INIT(8'h47)) 
    \ALUResult_out_OBUF[20]_inst_i_4 
       (.I0(S_Sub_in[20]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[10]),
        .O(\Dout_reg[23] ));
  LUT3 #(
    .INIT(8'h47)) 
    \ALUResult_out_OBUF[21]_inst_i_5 
       (.I0(S_Sub_in[21]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[11]),
        .O(\Dout_reg[23]_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \ALUResult_out_OBUF[23]_inst_i_4 
       (.I0(S_Sub_in[23]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[12]),
        .O(\Dout_reg[23]_1 ));
  LUT3 #(
    .INIT(8'h47)) 
    \ALUResult_out_OBUF[2]_inst_i_5 
       (.I0(S_Sub_in[2]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[0]),
        .O(\Dout_reg[3] ));
  LUT3 #(
    .INIT(8'hB8)) 
    \ALUResult_out_OBUF[6]_inst_i_4 
       (.I0(S_Sub_in[6]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[1]),
        .O(\Dout_reg[7] ));
  LUT3 #(
    .INIT(8'h47)) 
    \ALUResult_out_OBUF[7]_inst_i_5 
       (.I0(S_Sub_in[7]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[2]),
        .O(\Dout_reg[7]_0 ));
  LUT3 #(
    .INIT(8'h47)) 
    \ALUResult_out_OBUF[8]_inst_i_5 
       (.I0(S_Sub_in[8]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[3]),
        .O(\Dout_reg[11] ));
  LUT3 #(
    .INIT(8'h47)) 
    \ALUResult_out_OBUF[9]_inst_i_5 
       (.I0(S_Sub_in[9]),
        .I1(\Dout_reg[3]_1 ),
        .I2(S_Add_in[4]),
        .O(\Dout_reg[11]_0 ));
  LUT5 #(
    .INIT(32'h606F6F60)) 
    \Dout[3]_i_3 
       (.I0(O),
        .I1(\Dout_reg[3]_0 ),
        .I2(\Dout_reg[3]_1 ),
        .I3(S_Add_in[13]),
        .I4(\Dout_reg[3]_2 ),
        .O(\Dout_reg[30]_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry
       (.CI(1'b0),
        .CO({S0_carry_n_0,NLW_S0_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI(Q[3:0]),
        .O({\Dout_reg[30] [2],S_Sub_in[2],\Dout_reg[30] [1:0]}),
        .S(S));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__0
       (.CI(S0_carry_n_0),
        .CO({S0_carry__0_n_0,NLW_S0_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O({S_Sub_in[7:6],\Dout_reg[30] [4:3]}),
        .S(\ALUResult_out_OBUF[4]_inst_i_4 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__1
       (.CI(S0_carry__0_n_0),
        .CO({S0_carry__1_n_0,NLW_S0_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[11:8]),
        .O({\Dout_reg[30] [5],S_Sub_in[10:8]}),
        .S(\ALUResult_out_OBUF[8]_inst_i_5_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__2
       (.CI(S0_carry__1_n_0),
        .CO({S0_carry__2_n_0,NLW_S0_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[15:12]),
        .O({S_Sub_in[15:14],\Dout_reg[30] [6],S_Sub_in[12]}),
        .S(\ALUResult_out_OBUF[12]_inst_i_5_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__3
       (.CI(S0_carry__2_n_0),
        .CO({S0_carry__3_n_0,NLW_S0_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[19:16]),
        .O({\Dout_reg[30] [9:7],S_Sub_in[16]}),
        .S(\ALUResult_out_OBUF[16]_inst_i_5_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__4
       (.CI(S0_carry__3_n_0),
        .CO({S0_carry__4_n_0,NLW_S0_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[23:20]),
        .O({S_Sub_in[23],\Dout_reg[30] [10],S_Sub_in[21:20]}),
        .S(\ALUResult_out_OBUF[20]_inst_i_4_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__5
       (.CI(S0_carry__4_n_0),
        .CO({S0_carry__5_n_0,NLW_S0_carry__5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[27:24]),
        .O(\Dout_reg[30] [14:11]),
        .S(\ALUResult_out_OBUF[24]_inst_i_3 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 S0_carry__6
       (.CI(S0_carry__5_n_0),
        .CO({CO,NLW_S0_carry__6_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({DI,Q[30:28]}),
        .O({O,\Dout_reg[30] [17:15]}),
        .S(\ALUResult_out_OBUF[28]_inst_i_4 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
