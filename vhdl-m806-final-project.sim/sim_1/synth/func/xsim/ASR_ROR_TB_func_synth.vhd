-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Thu Sep 24 21:34:36 2020
-- Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/mpliax/Documents/workspace/vhdl-m806-final-project-sc/vhdl-m806-final-project.sim/sim_1/synth/func/xsim/ASR_ROR_TB_func_synth.vhd
-- Design      : ASR_ROR_n
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ASR_ROR_n is
  port (
    A : in STD_LOGIC_VECTOR ( 31 downto 0 );
    B : in STD_LOGIC_VECTOR ( 4 downto 0 );
    RorA : in STD_LOGIC;
    R : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of ASR_ROR_n : entity is true;
end ASR_ROR_n;

architecture STRUCTURE of ASR_ROR_n is
  signal A_IBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal B_IBUF : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal R_OBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \R_OBUF[10]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[11]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[12]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[13]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[14]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[15]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[16]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[17]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[18]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[19]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[1]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[20]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[21]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[22]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[23]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[24]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[25]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[26]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[26]_inst_i_3_n_0\ : STD_LOGIC;
  signal \R_OBUF[27]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[27]_inst_i_3_n_0\ : STD_LOGIC;
  signal \R_OBUF[28]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[28]_inst_i_3_n_0\ : STD_LOGIC;
  signal \R_OBUF[29]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[29]_inst_i_3_n_0\ : STD_LOGIC;
  signal \R_OBUF[2]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[30]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[30]_inst_i_3_n_0\ : STD_LOGIC;
  signal \R_OBUF[31]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[31]_inst_i_3_n_0\ : STD_LOGIC;
  signal \R_OBUF[3]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[4]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[5]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[6]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[7]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[8]_inst_i_2_n_0\ : STD_LOGIC;
  signal \R_OBUF[9]_inst_i_2_n_0\ : STD_LOGIC;
  signal RorA_IBUF : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \R_OBUF[0]_inst_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \R_OBUF[10]_inst_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \R_OBUF[11]_inst_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \R_OBUF[12]_inst_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \R_OBUF[13]_inst_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \R_OBUF[14]_inst_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \R_OBUF[15]_inst_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \R_OBUF[16]_inst_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \R_OBUF[17]_inst_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \R_OBUF[18]_inst_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \R_OBUF[19]_inst_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \R_OBUF[1]_inst_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \R_OBUF[20]_inst_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \R_OBUF[21]_inst_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \R_OBUF[22]_inst_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \R_OBUF[23]_inst_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \R_OBUF[24]_inst_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \R_OBUF[27]_inst_i_3\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \R_OBUF[28]_inst_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \R_OBUF[29]_inst_i_3\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \R_OBUF[2]_inst_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \R_OBUF[30]_inst_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \R_OBUF[4]_inst_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \R_OBUF[5]_inst_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \R_OBUF[6]_inst_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \R_OBUF[7]_inst_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \R_OBUF[8]_inst_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \R_OBUF[9]_inst_i_1\ : label is "soft_lutpair9";
begin
\A_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(0),
      O => A_IBUF(0)
    );
\A_IBUF[10]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(10),
      O => A_IBUF(10)
    );
\A_IBUF[11]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(11),
      O => A_IBUF(11)
    );
\A_IBUF[12]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(12),
      O => A_IBUF(12)
    );
\A_IBUF[13]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(13),
      O => A_IBUF(13)
    );
\A_IBUF[14]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(14),
      O => A_IBUF(14)
    );
\A_IBUF[15]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(15),
      O => A_IBUF(15)
    );
\A_IBUF[16]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(16),
      O => A_IBUF(16)
    );
\A_IBUF[17]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(17),
      O => A_IBUF(17)
    );
\A_IBUF[18]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(18),
      O => A_IBUF(18)
    );
\A_IBUF[19]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(19),
      O => A_IBUF(19)
    );
\A_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(1),
      O => A_IBUF(1)
    );
\A_IBUF[20]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(20),
      O => A_IBUF(20)
    );
\A_IBUF[21]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(21),
      O => A_IBUF(21)
    );
\A_IBUF[22]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(22),
      O => A_IBUF(22)
    );
\A_IBUF[23]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(23),
      O => A_IBUF(23)
    );
\A_IBUF[24]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(24),
      O => A_IBUF(24)
    );
\A_IBUF[25]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(25),
      O => A_IBUF(25)
    );
\A_IBUF[26]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(26),
      O => A_IBUF(26)
    );
\A_IBUF[27]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(27),
      O => A_IBUF(27)
    );
\A_IBUF[28]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(28),
      O => A_IBUF(28)
    );
\A_IBUF[29]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(29),
      O => A_IBUF(29)
    );
\A_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(2),
      O => A_IBUF(2)
    );
\A_IBUF[30]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(30),
      O => A_IBUF(30)
    );
\A_IBUF[31]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(31),
      O => A_IBUF(31)
    );
\A_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(3),
      O => A_IBUF(3)
    );
\A_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(4),
      O => A_IBUF(4)
    );
\A_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(5),
      O => A_IBUF(5)
    );
\A_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(6),
      O => A_IBUF(6)
    );
\A_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(7),
      O => A_IBUF(7)
    );
\A_IBUF[8]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(8),
      O => A_IBUF(8)
    );
\A_IBUF[9]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(9),
      O => A_IBUF(9)
    );
\B_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(0),
      O => B_IBUF(0)
    );
\B_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(1),
      O => B_IBUF(1)
    );
\B_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(2),
      O => B_IBUF(2)
    );
\R_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(0),
      O => R(0)
    );
\R_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[1]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[31]_inst_i_2_n_0\,
      O => R_OBUF(0)
    );
\R_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(10),
      O => R(10)
    );
\R_OBUF[10]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[11]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[10]_inst_i_2_n_0\,
      O => R_OBUF(10)
    );
\R_OBUF[10]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(16),
      I1 => A_IBUF(12),
      I2 => B_IBUF(1),
      I3 => A_IBUF(14),
      I4 => B_IBUF(2),
      I5 => A_IBUF(10),
      O => \R_OBUF[10]_inst_i_2_n_0\
    );
\R_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(11),
      O => R(11)
    );
\R_OBUF[11]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[12]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[11]_inst_i_2_n_0\,
      O => R_OBUF(11)
    );
\R_OBUF[11]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(17),
      I1 => A_IBUF(13),
      I2 => B_IBUF(1),
      I3 => A_IBUF(15),
      I4 => B_IBUF(2),
      I5 => A_IBUF(11),
      O => \R_OBUF[11]_inst_i_2_n_0\
    );
\R_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(12),
      O => R(12)
    );
\R_OBUF[12]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[13]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[12]_inst_i_2_n_0\,
      O => R_OBUF(12)
    );
\R_OBUF[12]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(18),
      I1 => A_IBUF(14),
      I2 => B_IBUF(1),
      I3 => A_IBUF(16),
      I4 => B_IBUF(2),
      I5 => A_IBUF(12),
      O => \R_OBUF[12]_inst_i_2_n_0\
    );
\R_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(13),
      O => R(13)
    );
\R_OBUF[13]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[14]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[13]_inst_i_2_n_0\,
      O => R_OBUF(13)
    );
\R_OBUF[13]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(19),
      I1 => A_IBUF(15),
      I2 => B_IBUF(1),
      I3 => A_IBUF(17),
      I4 => B_IBUF(2),
      I5 => A_IBUF(13),
      O => \R_OBUF[13]_inst_i_2_n_0\
    );
\R_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(14),
      O => R(14)
    );
\R_OBUF[14]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[15]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[14]_inst_i_2_n_0\,
      O => R_OBUF(14)
    );
\R_OBUF[14]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(20),
      I1 => A_IBUF(16),
      I2 => B_IBUF(1),
      I3 => A_IBUF(18),
      I4 => B_IBUF(2),
      I5 => A_IBUF(14),
      O => \R_OBUF[14]_inst_i_2_n_0\
    );
\R_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(15),
      O => R(15)
    );
\R_OBUF[15]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[16]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[15]_inst_i_2_n_0\,
      O => R_OBUF(15)
    );
\R_OBUF[15]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(21),
      I1 => A_IBUF(17),
      I2 => B_IBUF(1),
      I3 => A_IBUF(19),
      I4 => B_IBUF(2),
      I5 => A_IBUF(15),
      O => \R_OBUF[15]_inst_i_2_n_0\
    );
\R_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(16),
      O => R(16)
    );
\R_OBUF[16]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[17]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[16]_inst_i_2_n_0\,
      O => R_OBUF(16)
    );
\R_OBUF[16]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(22),
      I1 => A_IBUF(18),
      I2 => B_IBUF(1),
      I3 => A_IBUF(20),
      I4 => B_IBUF(2),
      I5 => A_IBUF(16),
      O => \R_OBUF[16]_inst_i_2_n_0\
    );
\R_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(17),
      O => R(17)
    );
\R_OBUF[17]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[18]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[17]_inst_i_2_n_0\,
      O => R_OBUF(17)
    );
\R_OBUF[17]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(23),
      I1 => A_IBUF(19),
      I2 => B_IBUF(1),
      I3 => A_IBUF(21),
      I4 => B_IBUF(2),
      I5 => A_IBUF(17),
      O => \R_OBUF[17]_inst_i_2_n_0\
    );
\R_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(18),
      O => R(18)
    );
\R_OBUF[18]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[19]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[18]_inst_i_2_n_0\,
      O => R_OBUF(18)
    );
\R_OBUF[18]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(24),
      I1 => A_IBUF(20),
      I2 => B_IBUF(1),
      I3 => A_IBUF(22),
      I4 => B_IBUF(2),
      I5 => A_IBUF(18),
      O => \R_OBUF[18]_inst_i_2_n_0\
    );
\R_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(19),
      O => R(19)
    );
\R_OBUF[19]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[20]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[19]_inst_i_2_n_0\,
      O => R_OBUF(19)
    );
\R_OBUF[19]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(25),
      I1 => A_IBUF(21),
      I2 => B_IBUF(1),
      I3 => A_IBUF(23),
      I4 => B_IBUF(2),
      I5 => A_IBUF(19),
      O => \R_OBUF[19]_inst_i_2_n_0\
    );
\R_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(1),
      O => R(1)
    );
\R_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[2]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[1]_inst_i_2_n_0\,
      O => R_OBUF(1)
    );
\R_OBUF[1]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(7),
      I1 => A_IBUF(3),
      I2 => B_IBUF(1),
      I3 => A_IBUF(5),
      I4 => B_IBUF(2),
      I5 => A_IBUF(1),
      O => \R_OBUF[1]_inst_i_2_n_0\
    );
\R_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(20),
      O => R(20)
    );
\R_OBUF[20]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[21]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[20]_inst_i_2_n_0\,
      O => R_OBUF(20)
    );
\R_OBUF[20]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(26),
      I1 => A_IBUF(22),
      I2 => B_IBUF(1),
      I3 => A_IBUF(24),
      I4 => B_IBUF(2),
      I5 => A_IBUF(20),
      O => \R_OBUF[20]_inst_i_2_n_0\
    );
\R_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(21),
      O => R(21)
    );
\R_OBUF[21]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[22]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[21]_inst_i_2_n_0\,
      O => R_OBUF(21)
    );
\R_OBUF[21]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(27),
      I1 => A_IBUF(23),
      I2 => B_IBUF(1),
      I3 => A_IBUF(25),
      I4 => B_IBUF(2),
      I5 => A_IBUF(21),
      O => \R_OBUF[21]_inst_i_2_n_0\
    );
\R_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(22),
      O => R(22)
    );
\R_OBUF[22]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[23]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[22]_inst_i_2_n_0\,
      O => R_OBUF(22)
    );
\R_OBUF[22]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(28),
      I1 => A_IBUF(24),
      I2 => B_IBUF(1),
      I3 => A_IBUF(26),
      I4 => B_IBUF(2),
      I5 => A_IBUF(22),
      O => \R_OBUF[22]_inst_i_2_n_0\
    );
\R_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(23),
      O => R(23)
    );
\R_OBUF[23]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[24]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[23]_inst_i_2_n_0\,
      O => R_OBUF(23)
    );
\R_OBUF[23]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(29),
      I1 => A_IBUF(25),
      I2 => B_IBUF(1),
      I3 => A_IBUF(27),
      I4 => B_IBUF(2),
      I5 => A_IBUF(23),
      O => \R_OBUF[23]_inst_i_2_n_0\
    );
\R_OBUF[24]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(24),
      O => R(24)
    );
\R_OBUF[24]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[25]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[24]_inst_i_2_n_0\,
      O => R_OBUF(24)
    );
\R_OBUF[24]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(30),
      I1 => A_IBUF(26),
      I2 => B_IBUF(1),
      I3 => A_IBUF(28),
      I4 => B_IBUF(2),
      I5 => A_IBUF(24),
      O => \R_OBUF[24]_inst_i_2_n_0\
    );
\R_OBUF[25]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(25),
      O => R(25)
    );
\R_OBUF[25]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \R_OBUF[26]_inst_i_2_n_0\,
      I1 => RorA_IBUF,
      I2 => \R_OBUF[26]_inst_i_3_n_0\,
      I3 => B_IBUF(0),
      I4 => \R_OBUF[25]_inst_i_2_n_0\,
      O => R_OBUF(25)
    );
\R_OBUF[25]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(31),
      I1 => A_IBUF(27),
      I2 => B_IBUF(1),
      I3 => A_IBUF(29),
      I4 => B_IBUF(2),
      I5 => A_IBUF(25),
      O => \R_OBUF[25]_inst_i_2_n_0\
    );
\R_OBUF[26]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(26),
      O => R(26)
    );
\R_OBUF[26]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \R_OBUF[27]_inst_i_2_n_0\,
      I1 => \R_OBUF[27]_inst_i_3_n_0\,
      I2 => B_IBUF(0),
      I3 => \R_OBUF[26]_inst_i_2_n_0\,
      I4 => RorA_IBUF,
      I5 => \R_OBUF[26]_inst_i_3_n_0\,
      O => R_OBUF(26)
    );
\R_OBUF[26]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(0),
      I1 => A_IBUF(28),
      I2 => B_IBUF(1),
      I3 => A_IBUF(30),
      I4 => B_IBUF(2),
      I5 => A_IBUF(26),
      O => \R_OBUF[26]_inst_i_2_n_0\
    );
\R_OBUF[26]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(31),
      I1 => A_IBUF(28),
      I2 => B_IBUF(1),
      I3 => A_IBUF(30),
      I4 => B_IBUF(2),
      I5 => A_IBUF(26),
      O => \R_OBUF[26]_inst_i_3_n_0\
    );
\R_OBUF[27]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(27),
      O => R(27)
    );
\R_OBUF[27]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \R_OBUF[28]_inst_i_2_n_0\,
      I1 => \R_OBUF[28]_inst_i_3_n_0\,
      I2 => B_IBUF(0),
      I3 => \R_OBUF[27]_inst_i_2_n_0\,
      I4 => RorA_IBUF,
      I5 => \R_OBUF[27]_inst_i_3_n_0\,
      O => R_OBUF(27)
    );
\R_OBUF[27]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(1),
      I1 => A_IBUF(29),
      I2 => B_IBUF(1),
      I3 => A_IBUF(31),
      I4 => B_IBUF(2),
      I5 => A_IBUF(27),
      O => \R_OBUF[27]_inst_i_2_n_0\
    );
\R_OBUF[27]_inst_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => A_IBUF(29),
      I1 => B_IBUF(1),
      I2 => A_IBUF(31),
      I3 => B_IBUF(2),
      I4 => A_IBUF(27),
      O => \R_OBUF[27]_inst_i_3_n_0\
    );
\R_OBUF[28]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(28),
      O => R(28)
    );
\R_OBUF[28]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \R_OBUF[29]_inst_i_2_n_0\,
      I1 => \R_OBUF[29]_inst_i_3_n_0\,
      I2 => B_IBUF(0),
      I3 => \R_OBUF[28]_inst_i_2_n_0\,
      I4 => RorA_IBUF,
      I5 => \R_OBUF[28]_inst_i_3_n_0\,
      O => R_OBUF(28)
    );
\R_OBUF[28]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(2),
      I1 => A_IBUF(30),
      I2 => B_IBUF(1),
      I3 => A_IBUF(0),
      I4 => B_IBUF(2),
      I5 => A_IBUF(28),
      O => \R_OBUF[28]_inst_i_2_n_0\
    );
\R_OBUF[28]_inst_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => A_IBUF(30),
      I1 => B_IBUF(1),
      I2 => A_IBUF(31),
      I3 => B_IBUF(2),
      I4 => A_IBUF(28),
      O => \R_OBUF[28]_inst_i_3_n_0\
    );
\R_OBUF[29]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(29),
      O => R(29)
    );
\R_OBUF[29]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \R_OBUF[30]_inst_i_2_n_0\,
      I1 => \R_OBUF[30]_inst_i_3_n_0\,
      I2 => B_IBUF(0),
      I3 => \R_OBUF[29]_inst_i_2_n_0\,
      I4 => RorA_IBUF,
      I5 => \R_OBUF[29]_inst_i_3_n_0\,
      O => R_OBUF(29)
    );
\R_OBUF[29]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(3),
      I1 => A_IBUF(31),
      I2 => B_IBUF(1),
      I3 => A_IBUF(1),
      I4 => B_IBUF(2),
      I5 => A_IBUF(29),
      O => \R_OBUF[29]_inst_i_2_n_0\
    );
\R_OBUF[29]_inst_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => B_IBUF(1),
      I1 => A_IBUF(31),
      I2 => B_IBUF(2),
      I3 => A_IBUF(29),
      O => \R_OBUF[29]_inst_i_3_n_0\
    );
\R_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(2),
      O => R(2)
    );
\R_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[3]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[2]_inst_i_2_n_0\,
      O => R_OBUF(2)
    );
\R_OBUF[2]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(8),
      I1 => A_IBUF(4),
      I2 => B_IBUF(1),
      I3 => A_IBUF(6),
      I4 => B_IBUF(2),
      I5 => A_IBUF(2),
      O => \R_OBUF[2]_inst_i_2_n_0\
    );
\R_OBUF[30]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(30),
      O => R(30)
    );
\R_OBUF[30]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \R_OBUF[31]_inst_i_3_n_0\,
      I1 => A_IBUF(31),
      I2 => B_IBUF(0),
      I3 => \R_OBUF[30]_inst_i_2_n_0\,
      I4 => RorA_IBUF,
      I5 => \R_OBUF[30]_inst_i_3_n_0\,
      O => R_OBUF(30)
    );
\R_OBUF[30]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(4),
      I1 => A_IBUF(0),
      I2 => B_IBUF(1),
      I3 => A_IBUF(2),
      I4 => B_IBUF(2),
      I5 => A_IBUF(30),
      O => \R_OBUF[30]_inst_i_2_n_0\
    );
\R_OBUF[30]_inst_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => B_IBUF(1),
      I1 => A_IBUF(31),
      I2 => B_IBUF(2),
      I3 => A_IBUF(30),
      O => \R_OBUF[30]_inst_i_3_n_0\
    );
\R_OBUF[31]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(31),
      O => R(31)
    );
\R_OBUF[31]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \R_OBUF[31]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[31]_inst_i_3_n_0\,
      I3 => RorA_IBUF,
      I4 => A_IBUF(31),
      O => R_OBUF(31)
    );
\R_OBUF[31]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(6),
      I1 => A_IBUF(2),
      I2 => B_IBUF(1),
      I3 => A_IBUF(4),
      I4 => B_IBUF(2),
      I5 => A_IBUF(0),
      O => \R_OBUF[31]_inst_i_2_n_0\
    );
\R_OBUF[31]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(5),
      I1 => A_IBUF(1),
      I2 => B_IBUF(1),
      I3 => A_IBUF(3),
      I4 => B_IBUF(2),
      I5 => A_IBUF(31),
      O => \R_OBUF[31]_inst_i_3_n_0\
    );
\R_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(3),
      O => R(3)
    );
\R_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[4]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[3]_inst_i_2_n_0\,
      O => R_OBUF(3)
    );
\R_OBUF[3]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(9),
      I1 => A_IBUF(5),
      I2 => B_IBUF(1),
      I3 => A_IBUF(7),
      I4 => B_IBUF(2),
      I5 => A_IBUF(3),
      O => \R_OBUF[3]_inst_i_2_n_0\
    );
\R_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(4),
      O => R(4)
    );
\R_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[5]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[4]_inst_i_2_n_0\,
      O => R_OBUF(4)
    );
\R_OBUF[4]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(10),
      I1 => A_IBUF(6),
      I2 => B_IBUF(1),
      I3 => A_IBUF(8),
      I4 => B_IBUF(2),
      I5 => A_IBUF(4),
      O => \R_OBUF[4]_inst_i_2_n_0\
    );
\R_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(5),
      O => R(5)
    );
\R_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[6]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[5]_inst_i_2_n_0\,
      O => R_OBUF(5)
    );
\R_OBUF[5]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(11),
      I1 => A_IBUF(7),
      I2 => B_IBUF(1),
      I3 => A_IBUF(9),
      I4 => B_IBUF(2),
      I5 => A_IBUF(5),
      O => \R_OBUF[5]_inst_i_2_n_0\
    );
\R_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(6),
      O => R(6)
    );
\R_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[7]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[6]_inst_i_2_n_0\,
      O => R_OBUF(6)
    );
\R_OBUF[6]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(12),
      I1 => A_IBUF(8),
      I2 => B_IBUF(1),
      I3 => A_IBUF(10),
      I4 => B_IBUF(2),
      I5 => A_IBUF(6),
      O => \R_OBUF[6]_inst_i_2_n_0\
    );
\R_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(7),
      O => R(7)
    );
\R_OBUF[7]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[8]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[7]_inst_i_2_n_0\,
      O => R_OBUF(7)
    );
\R_OBUF[7]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(13),
      I1 => A_IBUF(9),
      I2 => B_IBUF(1),
      I3 => A_IBUF(11),
      I4 => B_IBUF(2),
      I5 => A_IBUF(7),
      O => \R_OBUF[7]_inst_i_2_n_0\
    );
\R_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(8),
      O => R(8)
    );
\R_OBUF[8]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[9]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[8]_inst_i_2_n_0\,
      O => R_OBUF(8)
    );
\R_OBUF[8]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(14),
      I1 => A_IBUF(10),
      I2 => B_IBUF(1),
      I3 => A_IBUF(12),
      I4 => B_IBUF(2),
      I5 => A_IBUF(8),
      O => \R_OBUF[8]_inst_i_2_n_0\
    );
\R_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => R_OBUF(9),
      O => R(9)
    );
\R_OBUF[9]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \R_OBUF[10]_inst_i_2_n_0\,
      I1 => B_IBUF(0),
      I2 => \R_OBUF[9]_inst_i_2_n_0\,
      O => R_OBUF(9)
    );
\R_OBUF[9]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => A_IBUF(15),
      I1 => A_IBUF(11),
      I2 => B_IBUF(1),
      I3 => A_IBUF(13),
      I4 => B_IBUF(2),
      I5 => A_IBUF(9),
      O => \R_OBUF[9]_inst_i_2_n_0\
    );
RorA_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RorA,
      O => RorA_IBUF
    );
end STRUCTURE;
