-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Mon Oct 12 23:10:41 2020
-- Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/mpliax/Documents/workspace/vhdl-m806-final-project-mc/vhdl-m806-final-project.sim/sim_1/synth/func/xsim/PROCESSOR_TB_func_synth.vhd
-- Design      : ARM_PROCESSOR_n
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ADDER_n is
  port (
    PCPlus8 : out STD_LOGIC_VECTOR ( 28 downto 0 );
    \Dout_reg[31]\ : out STD_LOGIC_VECTOR ( 28 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 29 downto 0 );
    \Dout_reg[3]\ : in STD_LOGIC;
    DATA_OUT2 : in STD_LOGIC_VECTOR ( 28 downto 0 )
  );
end ADDER_n;

architecture STRUCTURE of ADDER_n is
  signal \^pcplus8\ : STD_LOGIC_VECTOR ( 28 downto 0 );
  signal \S0_carry__0_n_0\ : STD_LOGIC;
  signal \S0_carry__0_n_1\ : STD_LOGIC;
  signal \S0_carry__0_n_2\ : STD_LOGIC;
  signal \S0_carry__0_n_3\ : STD_LOGIC;
  signal \S0_carry__1_n_0\ : STD_LOGIC;
  signal \S0_carry__1_n_1\ : STD_LOGIC;
  signal \S0_carry__1_n_2\ : STD_LOGIC;
  signal \S0_carry__1_n_3\ : STD_LOGIC;
  signal \S0_carry__2_n_0\ : STD_LOGIC;
  signal \S0_carry__2_n_1\ : STD_LOGIC;
  signal \S0_carry__2_n_2\ : STD_LOGIC;
  signal \S0_carry__2_n_3\ : STD_LOGIC;
  signal \S0_carry__3_n_0\ : STD_LOGIC;
  signal \S0_carry__3_n_1\ : STD_LOGIC;
  signal \S0_carry__3_n_2\ : STD_LOGIC;
  signal \S0_carry__3_n_3\ : STD_LOGIC;
  signal \S0_carry__4_n_0\ : STD_LOGIC;
  signal \S0_carry__4_n_1\ : STD_LOGIC;
  signal \S0_carry__4_n_2\ : STD_LOGIC;
  signal \S0_carry__4_n_3\ : STD_LOGIC;
  signal \S0_carry__5_n_0\ : STD_LOGIC;
  signal \S0_carry__5_n_1\ : STD_LOGIC;
  signal \S0_carry__5_n_2\ : STD_LOGIC;
  signal \S0_carry__5_n_3\ : STD_LOGIC;
  signal S0_carry_n_0 : STD_LOGIC;
  signal S0_carry_n_1 : STD_LOGIC;
  signal S0_carry_n_2 : STD_LOGIC;
  signal S0_carry_n_3 : STD_LOGIC;
  signal \NLW_S0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_S0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[10]_inst_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[11]_inst_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[12]_inst_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[13]_inst_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[14]_inst_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[15]_inst_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[16]_inst_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[17]_inst_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[18]_inst_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[19]_inst_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[20]_inst_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[21]_inst_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[22]_inst_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[23]_inst_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[24]_inst_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[25]_inst_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[26]_inst_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[27]_inst_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[28]_inst_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[29]_inst_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[30]_inst_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[31]_inst_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[3]_inst_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[4]_inst_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[6]_inst_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[7]_inst_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[8]_inst_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[9]_inst_i_1\ : label is "soft_lutpair38";
begin
  PCPlus8(28 downto 0) <= \^pcplus8\(28 downto 0);
S0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => S0_carry_n_0,
      CO(2) => S0_carry_n_1,
      CO(1) => S0_carry_n_2,
      CO(0) => S0_carry_n_3,
      CYINIT => Q(0),
      DI(3 downto 0) => Q(4 downto 1),
      O(3 downto 0) => \^pcplus8\(3 downto 0),
      S(3 downto 0) => Q(4 downto 1)
    );
\S0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => S0_carry_n_0,
      CO(3) => \S0_carry__0_n_0\,
      CO(2) => \S0_carry__0_n_1\,
      CO(1) => \S0_carry__0_n_2\,
      CO(0) => \S0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(8 downto 5),
      O(3 downto 0) => \^pcplus8\(7 downto 4),
      S(3 downto 0) => Q(8 downto 5)
    );
\S0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__0_n_0\,
      CO(3) => \S0_carry__1_n_0\,
      CO(2) => \S0_carry__1_n_1\,
      CO(1) => \S0_carry__1_n_2\,
      CO(0) => \S0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(12 downto 9),
      O(3 downto 0) => \^pcplus8\(11 downto 8),
      S(3 downto 0) => Q(12 downto 9)
    );
\S0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__1_n_0\,
      CO(3) => \S0_carry__2_n_0\,
      CO(2) => \S0_carry__2_n_1\,
      CO(1) => \S0_carry__2_n_2\,
      CO(0) => \S0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(16 downto 13),
      O(3 downto 0) => \^pcplus8\(15 downto 12),
      S(3 downto 0) => Q(16 downto 13)
    );
\S0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__2_n_0\,
      CO(3) => \S0_carry__3_n_0\,
      CO(2) => \S0_carry__3_n_1\,
      CO(1) => \S0_carry__3_n_2\,
      CO(0) => \S0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(20 downto 17),
      O(3 downto 0) => \^pcplus8\(19 downto 16),
      S(3 downto 0) => Q(20 downto 17)
    );
\S0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__3_n_0\,
      CO(3) => \S0_carry__4_n_0\,
      CO(2) => \S0_carry__4_n_1\,
      CO(1) => \S0_carry__4_n_2\,
      CO(0) => \S0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(24 downto 21),
      O(3 downto 0) => \^pcplus8\(23 downto 20),
      S(3 downto 0) => Q(24 downto 21)
    );
\S0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__4_n_0\,
      CO(3) => \S0_carry__5_n_0\,
      CO(2) => \S0_carry__5_n_1\,
      CO(1) => \S0_carry__5_n_2\,
      CO(0) => \S0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(28 downto 25),
      O(3 downto 0) => \^pcplus8\(27 downto 24),
      S(3 downto 0) => Q(28 downto 25)
    );
\S0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__5_n_0\,
      CO(3 downto 0) => \NLW_S0_carry__6_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_S0_carry__6_O_UNCONNECTED\(3 downto 1),
      O(0) => \^pcplus8\(28),
      S(3 downto 1) => B"000",
      S(0) => Q(29)
    );
\WriteData_out_OBUF[10]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(7),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(7),
      O => \Dout_reg[31]\(7)
    );
\WriteData_out_OBUF[11]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(8),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(8),
      O => \Dout_reg[31]\(8)
    );
\WriteData_out_OBUF[12]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(9),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(9),
      O => \Dout_reg[31]\(9)
    );
\WriteData_out_OBUF[13]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(10),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(10),
      O => \Dout_reg[31]\(10)
    );
\WriteData_out_OBUF[14]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(11),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(11),
      O => \Dout_reg[31]\(11)
    );
\WriteData_out_OBUF[15]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(12),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(12),
      O => \Dout_reg[31]\(12)
    );
\WriteData_out_OBUF[16]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(13),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(13),
      O => \Dout_reg[31]\(13)
    );
\WriteData_out_OBUF[17]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(14),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(14),
      O => \Dout_reg[31]\(14)
    );
\WriteData_out_OBUF[18]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(15),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(15),
      O => \Dout_reg[31]\(15)
    );
\WriteData_out_OBUF[19]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(16),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(16),
      O => \Dout_reg[31]\(16)
    );
\WriteData_out_OBUF[20]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(17),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(17),
      O => \Dout_reg[31]\(17)
    );
\WriteData_out_OBUF[21]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(18),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(18),
      O => \Dout_reg[31]\(18)
    );
\WriteData_out_OBUF[22]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(19),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(19),
      O => \Dout_reg[31]\(19)
    );
\WriteData_out_OBUF[23]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(20),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(20),
      O => \Dout_reg[31]\(20)
    );
\WriteData_out_OBUF[24]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(21),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(21),
      O => \Dout_reg[31]\(21)
    );
\WriteData_out_OBUF[25]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(22),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(22),
      O => \Dout_reg[31]\(22)
    );
\WriteData_out_OBUF[26]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(23),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(23),
      O => \Dout_reg[31]\(23)
    );
\WriteData_out_OBUF[27]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(24),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(24),
      O => \Dout_reg[31]\(24)
    );
\WriteData_out_OBUF[28]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(25),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(25),
      O => \Dout_reg[31]\(25)
    );
\WriteData_out_OBUF[29]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(26),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(26),
      O => \Dout_reg[31]\(26)
    );
\WriteData_out_OBUF[30]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(27),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(27),
      O => \Dout_reg[31]\(27)
    );
\WriteData_out_OBUF[31]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(28),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(28),
      O => \Dout_reg[31]\(28)
    );
\WriteData_out_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(0),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(0),
      O => \Dout_reg[31]\(0)
    );
\WriteData_out_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(1),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(1),
      O => \Dout_reg[31]\(1)
    );
\WriteData_out_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(2),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(2),
      O => \Dout_reg[31]\(2)
    );
\WriteData_out_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(3),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(3),
      O => \Dout_reg[31]\(3)
    );
\WriteData_out_OBUF[7]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(4),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(4),
      O => \Dout_reg[31]\(4)
    );
\WriteData_out_OBUF[8]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(5),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(5),
      O => \Dout_reg[31]\(5)
    );
\WriteData_out_OBUF[9]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^pcplus8\(6),
      I1 => \Dout_reg[3]\,
      I2 => DATA_OUT2(6),
      O => \Dout_reg[31]\(6)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ADDER_n_8 is
  port (
    \Dout_reg[30]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    S_Add_in : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \Dout_reg[30]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[30]_1\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    \ALUResult_out_OBUF[0]_inst_i_6\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[4]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[8]_inst_i_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[12]_inst_i_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[16]_inst_i_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[20]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[24]_inst_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 0 to 0 );
    \ALUResult_out_OBUF[28]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[31]_inst_i_3\ : in STD_LOGIC;
    O : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of ADDER_n_8 : entity is "ADDER_n";
end ADDER_n_8;

architecture STRUCTURE of ADDER_n_8 is
  signal \S0_carry__0_n_0\ : STD_LOGIC;
  signal \S0_carry__0_n_1\ : STD_LOGIC;
  signal \S0_carry__0_n_2\ : STD_LOGIC;
  signal \S0_carry__0_n_3\ : STD_LOGIC;
  signal \S0_carry__1_n_0\ : STD_LOGIC;
  signal \S0_carry__1_n_1\ : STD_LOGIC;
  signal \S0_carry__1_n_2\ : STD_LOGIC;
  signal \S0_carry__1_n_3\ : STD_LOGIC;
  signal \S0_carry__2_n_0\ : STD_LOGIC;
  signal \S0_carry__2_n_1\ : STD_LOGIC;
  signal \S0_carry__2_n_2\ : STD_LOGIC;
  signal \S0_carry__2_n_3\ : STD_LOGIC;
  signal \S0_carry__3_n_0\ : STD_LOGIC;
  signal \S0_carry__3_n_1\ : STD_LOGIC;
  signal \S0_carry__3_n_2\ : STD_LOGIC;
  signal \S0_carry__3_n_3\ : STD_LOGIC;
  signal \S0_carry__4_n_0\ : STD_LOGIC;
  signal \S0_carry__4_n_1\ : STD_LOGIC;
  signal \S0_carry__4_n_2\ : STD_LOGIC;
  signal \S0_carry__4_n_3\ : STD_LOGIC;
  signal \S0_carry__5_n_0\ : STD_LOGIC;
  signal \S0_carry__5_n_1\ : STD_LOGIC;
  signal \S0_carry__5_n_2\ : STD_LOGIC;
  signal \S0_carry__5_n_3\ : STD_LOGIC;
  signal \S0_carry__6_n_1\ : STD_LOGIC;
  signal \S0_carry__6_n_2\ : STD_LOGIC;
  signal \S0_carry__6_n_3\ : STD_LOGIC;
  signal S0_carry_n_0 : STD_LOGIC;
  signal S0_carry_n_1 : STD_LOGIC;
  signal S0_carry_n_2 : STD_LOGIC;
  signal S0_carry_n_3 : STD_LOGIC;
  signal \^s_add_in\ : STD_LOGIC_VECTOR ( 13 downto 0 );
begin
  S_Add_in(13 downto 0) <= \^s_add_in\(13 downto 0);
\ALUResult_out_OBUF[31]_inst_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \^s_add_in\(13),
      I1 => \ALUResult_out_OBUF[31]_inst_i_3\,
      I2 => O(0),
      O => \Dout_reg[30]_1\
    );
S0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => S0_carry_n_0,
      CO(2) => S0_carry_n_1,
      CO(1) => S0_carry_n_2,
      CO(0) => S0_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => Q(3 downto 0),
      O(3) => \Dout_reg[30]\(2),
      O(2) => \^s_add_in\(0),
      O(1 downto 0) => \Dout_reg[30]\(1 downto 0),
      S(3 downto 0) => \ALUResult_out_OBUF[0]_inst_i_6\(3 downto 0)
    );
\S0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => S0_carry_n_0,
      CO(3) => \S0_carry__0_n_0\,
      CO(2) => \S0_carry__0_n_1\,
      CO(1) => \S0_carry__0_n_2\,
      CO(0) => \S0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(7 downto 4),
      O(3 downto 2) => \^s_add_in\(2 downto 1),
      O(1 downto 0) => \Dout_reg[30]\(4 downto 3),
      S(3 downto 0) => \ALUResult_out_OBUF[4]_inst_i_4\(3 downto 0)
    );
\S0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__0_n_0\,
      CO(3) => \S0_carry__1_n_0\,
      CO(2) => \S0_carry__1_n_1\,
      CO(1) => \S0_carry__1_n_2\,
      CO(0) => \S0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(11 downto 8),
      O(3) => \Dout_reg[30]\(5),
      O(2 downto 0) => \^s_add_in\(5 downto 3),
      S(3 downto 0) => \ALUResult_out_OBUF[8]_inst_i_5\(3 downto 0)
    );
\S0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__1_n_0\,
      CO(3) => \S0_carry__2_n_0\,
      CO(2) => \S0_carry__2_n_1\,
      CO(1) => \S0_carry__2_n_2\,
      CO(0) => \S0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(15 downto 12),
      O(3 downto 2) => \^s_add_in\(8 downto 7),
      O(1) => \Dout_reg[30]\(6),
      O(0) => \^s_add_in\(6),
      S(3 downto 0) => \ALUResult_out_OBUF[12]_inst_i_5\(3 downto 0)
    );
\S0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__2_n_0\,
      CO(3) => \S0_carry__3_n_0\,
      CO(2) => \S0_carry__3_n_1\,
      CO(1) => \S0_carry__3_n_2\,
      CO(0) => \S0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(19 downto 16),
      O(3 downto 1) => \Dout_reg[30]\(9 downto 7),
      O(0) => \^s_add_in\(9),
      S(3 downto 0) => \ALUResult_out_OBUF[16]_inst_i_5\(3 downto 0)
    );
\S0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__3_n_0\,
      CO(3) => \S0_carry__4_n_0\,
      CO(2) => \S0_carry__4_n_1\,
      CO(1) => \S0_carry__4_n_2\,
      CO(0) => \S0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(23 downto 20),
      O(3) => \^s_add_in\(12),
      O(2) => \Dout_reg[30]\(10),
      O(1 downto 0) => \^s_add_in\(11 downto 10),
      S(3 downto 0) => \ALUResult_out_OBUF[20]_inst_i_4\(3 downto 0)
    );
\S0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__4_n_0\,
      CO(3) => \S0_carry__5_n_0\,
      CO(2) => \S0_carry__5_n_1\,
      CO(1) => \S0_carry__5_n_2\,
      CO(0) => \S0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(27 downto 24),
      O(3 downto 0) => \Dout_reg[30]\(14 downto 11),
      S(3 downto 0) => \ALUResult_out_OBUF[24]_inst_i_3\(3 downto 0)
    );
\S0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__5_n_0\,
      CO(3) => \Dout_reg[30]_0\(0),
      CO(2) => \S0_carry__6_n_1\,
      CO(1) => \S0_carry__6_n_2\,
      CO(0) => \S0_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => DI(0),
      DI(2 downto 0) => Q(30 downto 28),
      O(3) => \^s_add_in\(13),
      O(2 downto 0) => \Dout_reg[30]\(17 downto 15),
      S(3 downto 0) => \ALUResult_out_OBUF[28]_inst_i_4\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity FSM is
  port (
    \FSM_onehot_current_state_reg[1]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_current_state_reg[13]_0\ : out STD_LOGIC;
    FlagsWrite_in : out STD_LOGIC;
    RegWrite_in : out STD_LOGIC;
    \FSM_onehot_current_state_reg[11]_0\ : out STD_LOGIC;
    Instr_out_OBUF : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \FSM_onehot_current_state_reg[2]_0\ : in STD_LOGIC;
    \FSM_onehot_current_state_reg[10]_0\ : in STD_LOGIC;
    \Dout_reg[0]\ : in STD_LOGIC;
    Flags_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    RESET_IBUF : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
end FSM;

architecture STRUCTURE of FSM is
  signal \FSM_onehot_current_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[10]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[4]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[5]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[7]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[7]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[8]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[8]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[9]_i_1_n_0\ : STD_LOGIC;
  signal \^fsm_onehot_current_state_reg[13]_0\ : STD_LOGIC;
  signal \^fsm_onehot_current_state_reg[1]_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[10]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[11]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[12]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[13]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[2]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[4]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[6]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[8]\ : STD_LOGIC;
  signal \^flagswrite_in\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Dout[0]_i_1__3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \Dout[3]_i_4\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[0]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[13]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[4]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[5]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[7]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[7]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[8]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[8]_i_2\ : label is "soft_lutpair2";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[0]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[10]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[11]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[12]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[13]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[1]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[2]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[3]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[4]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[5]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[6]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[7]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[8]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[9]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
begin
  \FSM_onehot_current_state_reg[13]_0\ <= \^fsm_onehot_current_state_reg[13]_0\;
  \FSM_onehot_current_state_reg[1]_0\ <= \^fsm_onehot_current_state_reg[1]_0\;
  FlagsWrite_in <= \^flagswrite_in\;
  Q(4 downto 0) <= \^q\(4 downto 0);
\Dout[0]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55575554"
    )
        port map (
      I0 => \Dout_reg[0]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[11]\,
      I2 => \FSM_onehot_current_state_reg_n_0_[10]\,
      I3 => \^q\(4),
      I4 => Flags_in(0),
      O => \FSM_onehot_current_state_reg[11]_0\
    );
\Dout[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFD"
    )
        port map (
      I0 => \^fsm_onehot_current_state_reg[13]_0\,
      I1 => \^q\(3),
      I2 => \FSM_onehot_current_state_reg_n_0_[2]\,
      I3 => \^q\(2),
      I4 => \FSM_onehot_current_state_reg_n_0_[8]\,
      I5 => \^flagswrite_in\,
      O => E(0)
    );
\Dout[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[13]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[12]\,
      O => \^fsm_onehot_current_state_reg[13]_0\
    );
\Dout[3]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[11]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[10]\,
      I2 => \^q\(4),
      O => \^flagswrite_in\
    );
\FSM_onehot_current_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EE0E0EAA"
    )
        port map (
      I0 => \FSM_onehot_current_state[0]_i_2_n_0\,
      I1 => \^fsm_onehot_current_state_reg[1]_0\,
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => Instr_out_OBUF(2),
      I4 => Instr_out_OBUF(1),
      I5 => \^q\(0),
      O => \FSM_onehot_current_state[0]_i_1_n_0\
    );
\FSM_onehot_current_state[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[4]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[6]\,
      I2 => \^q\(1),
      I3 => \FSM_onehot_current_state_reg_n_0_[1]\,
      O => \FSM_onehot_current_state[0]_i_2_n_0\
    );
\FSM_onehot_current_state[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[6]\,
      I1 => Instr_out_OBUF(0),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I5 => \FSM_onehot_current_state_reg[10]_0\,
      O => \FSM_onehot_current_state[10]_i_1_n_0\
    );
\FSM_onehot_current_state[13]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I1 => \FSM_onehot_current_state_reg[2]_0\,
      O => \^fsm_onehot_current_state_reg[1]_0\
    );
\FSM_onehot_current_state[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg[2]_0\,
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I2 => \^q\(0),
      O => \FSM_onehot_current_state[2]_i_1_n_0\
    );
\FSM_onehot_current_state[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => Instr_out_OBUF(0),
      I1 => \^q\(1),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^q\(0),
      O => \FSM_onehot_current_state[4]_i_1_n_0\
    );
\FSM_onehot_current_state[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => Instr_out_OBUF(0),
      I1 => \^q\(1),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^q\(0),
      O => \FSM_onehot_current_state[5]_i_1_n_0\
    );
\FSM_onehot_current_state[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"20A8"
    )
        port map (
      I0 => \FSM_onehot_current_state[7]_i_2_n_0\,
      I1 => \FSM_onehot_current_state_reg_n_0_[6]\,
      I2 => \FSM_onehot_current_state_reg_n_0_[4]\,
      I3 => Instr_out_OBUF(0),
      O => \FSM_onehot_current_state[7]_i_1_n_0\
    );
\FSM_onehot_current_state[7]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg[10]_0\,
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \FSM_onehot_current_state[7]_i_2_n_0\
    );
\FSM_onehot_current_state[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"20A8"
    )
        port map (
      I0 => \FSM_onehot_current_state[8]_i_2_n_0\,
      I1 => \FSM_onehot_current_state_reg_n_0_[6]\,
      I2 => \FSM_onehot_current_state_reg_n_0_[4]\,
      I3 => Instr_out_OBUF(0),
      O => \FSM_onehot_current_state[8]_i_1_n_0\
    );
\FSM_onehot_current_state[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg[10]_0\,
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \FSM_onehot_current_state[8]_i_2_n_0\
    );
\FSM_onehot_current_state[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[6]\,
      I1 => Instr_out_OBUF(0),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I5 => \FSM_onehot_current_state_reg[10]_0\,
      O => \FSM_onehot_current_state[9]_i_1_n_0\
    );
\FSM_onehot_current_state_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[0]_i_1_n_0\,
      Q => \^q\(0),
      S => RESET_IBUF
    );
\FSM_onehot_current_state_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[10]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[10]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(2),
      Q => \FSM_onehot_current_state_reg_n_0_[11]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(3),
      Q => \FSM_onehot_current_state_reg_n_0_[12]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(4),
      Q => \FSM_onehot_current_state_reg_n_0_[13]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \^q\(0),
      Q => \FSM_onehot_current_state_reg_n_0_[1]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[2]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[2]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(0),
      Q => \^q\(1),
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[4]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[4]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[5]_i_1_n_0\,
      Q => \^q\(2),
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(1),
      Q => \FSM_onehot_current_state_reg_n_0_[6]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[7]_i_1_n_0\,
      Q => \^q\(3),
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[8]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[8]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[9]_i_1_n_0\,
      Q => \^q\(4),
      R => RESET_IBUF
    );
RF_reg_r1_0_15_0_5_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[10]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[8]\,
      I2 => \FSM_onehot_current_state_reg_n_0_[13]\,
      O => RegWrite_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity RAM_ARRAY is
  port (
    ReadData_in_regRD : out STD_LOGIC_VECTOR ( 31 downto 0 );
    Result_out_OBUF : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC;
    RESET_IBUF : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    RAM_reg_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    RAM_reg_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[0]\ : in STD_LOGIC;
    \Dout_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end RAM_ARRAY;

architecture STRUCTURE of RAM_ARRAY is
  signal \^readdata_in_regrd\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_RAM_reg_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 14 );
  signal NLW_RAM_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of RAM_reg : label is "p2_d16";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of RAM_reg : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of RAM_reg : label is 1024;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of RAM_reg : label is "DATAPATH/DATA_MEMORY/RAM";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of RAM_reg : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of RAM_reg : label is 31;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of RAM_reg : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of RAM_reg : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of RAM_reg : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of RAM_reg : label is 31;
  attribute ram_ext_slice_begin : integer;
  attribute ram_ext_slice_begin of RAM_reg : label is 18;
  attribute ram_ext_slice_end : integer;
  attribute ram_ext_slice_end of RAM_reg : label is 31;
  attribute ram_offset : integer;
  attribute ram_offset of RAM_reg : label is 992;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of RAM_reg : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of RAM_reg : label is 17;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Result_out_OBUF[0]_inst_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \Result_out_OBUF[10]_inst_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \Result_out_OBUF[11]_inst_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \Result_out_OBUF[12]_inst_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \Result_out_OBUF[13]_inst_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \Result_out_OBUF[14]_inst_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \Result_out_OBUF[15]_inst_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \Result_out_OBUF[16]_inst_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \Result_out_OBUF[17]_inst_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \Result_out_OBUF[18]_inst_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \Result_out_OBUF[19]_inst_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \Result_out_OBUF[1]_inst_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \Result_out_OBUF[20]_inst_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \Result_out_OBUF[21]_inst_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \Result_out_OBUF[22]_inst_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \Result_out_OBUF[23]_inst_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \Result_out_OBUF[24]_inst_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \Result_out_OBUF[25]_inst_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \Result_out_OBUF[26]_inst_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \Result_out_OBUF[27]_inst_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \Result_out_OBUF[28]_inst_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \Result_out_OBUF[29]_inst_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \Result_out_OBUF[2]_inst_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \Result_out_OBUF[30]_inst_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \Result_out_OBUF[31]_inst_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \Result_out_OBUF[3]_inst_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \Result_out_OBUF[4]_inst_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \Result_out_OBUF[5]_inst_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \Result_out_OBUF[6]_inst_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \Result_out_OBUF[7]_inst_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \Result_out_OBUF[8]_inst_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \Result_out_OBUF[9]_inst_i_1\ : label is "soft_lutpair22";
begin
  ReadData_in_regRD(31 downto 0) <= \^readdata_in_regrd\(31 downto 0);
RAM_reg: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "READ_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 9) => B"01111",
      ADDRARDADDR(8 downto 4) => Q(4 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(13 downto 9) => B"11111",
      ADDRBWRADDR(8 downto 4) => Q(4 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CLKARDCLK => CLK_IBUF_BUFG,
      CLKBWRCLK => CLK_IBUF_BUFG,
      DIADI(15 downto 0) => RAM_reg_0(15 downto 0),
      DIBDI(15 downto 14) => B"11",
      DIBDI(13 downto 0) => RAM_reg_0(31 downto 18),
      DIPADIP(1 downto 0) => RAM_reg_0(17 downto 16),
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 0) => \^readdata_in_regrd\(15 downto 0),
      DOBDO(15 downto 14) => NLW_RAM_reg_DOBDO_UNCONNECTED(15 downto 14),
      DOBDO(13 downto 0) => \^readdata_in_regrd\(31 downto 18),
      DOPADOP(1 downto 0) => \^readdata_in_regrd\(17 downto 16),
      DOPBDOP(1 downto 0) => NLW_RAM_reg_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => '1',
      ENBWREN => '1',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => RESET_IBUF,
      RSTRAMB => RESET_IBUF,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => RAM_reg_1(0),
      WEA(0) => RAM_reg_1(0),
      WEBWE(3 downto 2) => B"00",
      WEBWE(1) => RAM_reg_1(0),
      WEBWE(0) => RAM_reg_1(0)
    );
\Result_out_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(0),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(0),
      O => Result_out_OBUF(0)
    );
\Result_out_OBUF[10]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(10),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(10),
      O => Result_out_OBUF(10)
    );
\Result_out_OBUF[11]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(11),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(11),
      O => Result_out_OBUF(11)
    );
\Result_out_OBUF[12]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(12),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(12),
      O => Result_out_OBUF(12)
    );
\Result_out_OBUF[13]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(13),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(13),
      O => Result_out_OBUF(13)
    );
\Result_out_OBUF[14]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(14),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(14),
      O => Result_out_OBUF(14)
    );
\Result_out_OBUF[15]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(15),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(15),
      O => Result_out_OBUF(15)
    );
\Result_out_OBUF[16]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(16),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(16),
      O => Result_out_OBUF(16)
    );
\Result_out_OBUF[17]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(17),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(17),
      O => Result_out_OBUF(17)
    );
\Result_out_OBUF[18]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(18),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(18),
      O => Result_out_OBUF(18)
    );
\Result_out_OBUF[19]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(19),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(19),
      O => Result_out_OBUF(19)
    );
\Result_out_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(1),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(1),
      O => Result_out_OBUF(1)
    );
\Result_out_OBUF[20]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(20),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(20),
      O => Result_out_OBUF(20)
    );
\Result_out_OBUF[21]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(21),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(21),
      O => Result_out_OBUF(21)
    );
\Result_out_OBUF[22]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(22),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(22),
      O => Result_out_OBUF(22)
    );
\Result_out_OBUF[23]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(23),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(23),
      O => Result_out_OBUF(23)
    );
\Result_out_OBUF[24]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(24),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(24),
      O => Result_out_OBUF(24)
    );
\Result_out_OBUF[25]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(25),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(25),
      O => Result_out_OBUF(25)
    );
\Result_out_OBUF[26]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(26),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(26),
      O => Result_out_OBUF(26)
    );
\Result_out_OBUF[27]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(27),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(27),
      O => Result_out_OBUF(27)
    );
\Result_out_OBUF[28]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(28),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(28),
      O => Result_out_OBUF(28)
    );
\Result_out_OBUF[29]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(29),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(29),
      O => Result_out_OBUF(29)
    );
\Result_out_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(2),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(2),
      O => Result_out_OBUF(2)
    );
\Result_out_OBUF[30]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(30),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(30),
      O => Result_out_OBUF(30)
    );
\Result_out_OBUF[31]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(31),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(31),
      O => Result_out_OBUF(31)
    );
\Result_out_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(3),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(3),
      O => Result_out_OBUF(3)
    );
\Result_out_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(4),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(4),
      O => Result_out_OBUF(4)
    );
\Result_out_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(5),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(5),
      O => Result_out_OBUF(5)
    );
\Result_out_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(6),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(6),
      O => Result_out_OBUF(6)
    );
\Result_out_OBUF[7]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(7),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(7),
      O => Result_out_OBUF(7)
    );
\Result_out_OBUF[8]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(8),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(8),
      O => Result_out_OBUF(8)
    );
\Result_out_OBUF[9]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^readdata_in_regrd\(9),
      I1 => \Dout_reg[0]\,
      I2 => \Dout_reg[31]\(9),
      O => Result_out_OBUF(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGISTER_FILE_n is
  port (
    DATA_OUT1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    DATA_OUT2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC;
    RegWrite_in : in STD_LOGIC;
    DATA_IN : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ADDRA : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ADDRD : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[1]\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end REGISTER_FILE_n;

architecture STRUCTURE of REGISTER_FILE_n is
  signal NLW_RF_reg_r1_0_15_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_12_17_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_18_23_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_24_29_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_30_31_DOB_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_30_31_DOC_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_30_31_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_12_17_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_18_23_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_24_29_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_30_31_DOB_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_30_31_DOC_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_30_31_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_0_5 : label is "";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_0_5 : label is 512;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_0_5 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of RF_reg_r1_0_15_0_5 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of RF_reg_r1_0_15_0_5 : label is 15;
  attribute ram_offset : integer;
  attribute ram_offset of RF_reg_r1_0_15_0_5 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of RF_reg_r1_0_15_0_5 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of RF_reg_r1_0_15_0_5 : label is 5;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_12_17 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_12_17 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_12_17 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r1_0_15_12_17 : label is 0;
  attribute ram_addr_end of RF_reg_r1_0_15_12_17 : label is 15;
  attribute ram_offset of RF_reg_r1_0_15_12_17 : label is 0;
  attribute ram_slice_begin of RF_reg_r1_0_15_12_17 : label is 12;
  attribute ram_slice_end of RF_reg_r1_0_15_12_17 : label is 17;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_18_23 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_18_23 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_18_23 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r1_0_15_18_23 : label is 0;
  attribute ram_addr_end of RF_reg_r1_0_15_18_23 : label is 15;
  attribute ram_offset of RF_reg_r1_0_15_18_23 : label is 0;
  attribute ram_slice_begin of RF_reg_r1_0_15_18_23 : label is 18;
  attribute ram_slice_end of RF_reg_r1_0_15_18_23 : label is 23;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_24_29 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_24_29 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_24_29 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r1_0_15_24_29 : label is 0;
  attribute ram_addr_end of RF_reg_r1_0_15_24_29 : label is 15;
  attribute ram_offset of RF_reg_r1_0_15_24_29 : label is 0;
  attribute ram_slice_begin of RF_reg_r1_0_15_24_29 : label is 24;
  attribute ram_slice_end of RF_reg_r1_0_15_24_29 : label is 29;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_30_31 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_30_31 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_30_31 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r1_0_15_30_31 : label is 0;
  attribute ram_addr_end of RF_reg_r1_0_15_30_31 : label is 15;
  attribute ram_offset of RF_reg_r1_0_15_30_31 : label is 0;
  attribute ram_slice_begin of RF_reg_r1_0_15_30_31 : label is 30;
  attribute ram_slice_end of RF_reg_r1_0_15_30_31 : label is 31;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_6_11 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_6_11 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_6_11 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r1_0_15_6_11 : label is 0;
  attribute ram_addr_end of RF_reg_r1_0_15_6_11 : label is 15;
  attribute ram_offset of RF_reg_r1_0_15_6_11 : label is 0;
  attribute ram_slice_begin of RF_reg_r1_0_15_6_11 : label is 6;
  attribute ram_slice_end of RF_reg_r1_0_15_6_11 : label is 11;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_0_5 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_0_5 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_0_5 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_0_5 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_0_5 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_0_5 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_0_5 : label is 0;
  attribute ram_slice_end of RF_reg_r2_0_15_0_5 : label is 5;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_12_17 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_12_17 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_12_17 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_12_17 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_12_17 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_12_17 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_12_17 : label is 12;
  attribute ram_slice_end of RF_reg_r2_0_15_12_17 : label is 17;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_18_23 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_18_23 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_18_23 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_18_23 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_18_23 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_18_23 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_18_23 : label is 18;
  attribute ram_slice_end of RF_reg_r2_0_15_18_23 : label is 23;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_24_29 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_24_29 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_24_29 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_24_29 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_24_29 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_24_29 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_24_29 : label is 24;
  attribute ram_slice_end of RF_reg_r2_0_15_24_29 : label is 29;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_30_31 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_30_31 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_30_31 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_30_31 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_30_31 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_30_31 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_30_31 : label is 30;
  attribute ram_slice_end of RF_reg_r2_0_15_30_31 : label is 31;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_6_11 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_6_11 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_6_11 : label is "DATAPATH/REG_FILE/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_6_11 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_6_11 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_6_11 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_6_11 : label is 6;
  attribute ram_slice_end of RF_reg_r2_0_15_6_11 : label is 11;
begin
RF_reg_r1_0_15_0_5: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 2) => ADDRA(2 downto 1),
      ADDRA(1) => ADDRA(2),
      ADDRA(0) => ADDRA(0),
      ADDRB(4) => '0',
      ADDRB(3 downto 2) => ADDRA(2 downto 1),
      ADDRB(1) => ADDRA(2),
      ADDRB(0) => ADDRA(0),
      ADDRC(4) => '0',
      ADDRC(3 downto 2) => ADDRA(2 downto 1),
      ADDRC(1) => ADDRA(2),
      ADDRC(0) => ADDRA(0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(1 downto 0),
      DIB(1 downto 0) => DATA_IN(3 downto 2),
      DIC(1 downto 0) => DATA_IN(5 downto 4),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT1(1 downto 0),
      DOB(1 downto 0) => DATA_OUT1(3 downto 2),
      DOC(1 downto 0) => DATA_OUT1(5 downto 4),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r1_0_15_12_17: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 2) => ADDRA(2 downto 1),
      ADDRA(1) => ADDRA(2),
      ADDRA(0) => ADDRA(0),
      ADDRB(4) => '0',
      ADDRB(3 downto 2) => ADDRA(2 downto 1),
      ADDRB(1) => ADDRA(2),
      ADDRB(0) => ADDRA(0),
      ADDRC(4) => '0',
      ADDRC(3 downto 2) => ADDRA(2 downto 1),
      ADDRC(1) => ADDRA(2),
      ADDRC(0) => ADDRA(0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(13 downto 12),
      DIB(1 downto 0) => DATA_IN(15 downto 14),
      DIC(1 downto 0) => DATA_IN(17 downto 16),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT1(13 downto 12),
      DOB(1 downto 0) => DATA_OUT1(15 downto 14),
      DOC(1 downto 0) => DATA_OUT1(17 downto 16),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_12_17_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r1_0_15_18_23: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 2) => ADDRA(2 downto 1),
      ADDRA(1) => ADDRA(2),
      ADDRA(0) => ADDRA(0),
      ADDRB(4) => '0',
      ADDRB(3 downto 2) => ADDRA(2 downto 1),
      ADDRB(1) => ADDRA(2),
      ADDRB(0) => ADDRA(0),
      ADDRC(4) => '0',
      ADDRC(3 downto 2) => ADDRA(2 downto 1),
      ADDRC(1) => ADDRA(2),
      ADDRC(0) => ADDRA(0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(19 downto 18),
      DIB(1 downto 0) => DATA_IN(21 downto 20),
      DIC(1 downto 0) => DATA_IN(23 downto 22),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT1(19 downto 18),
      DOB(1 downto 0) => DATA_OUT1(21 downto 20),
      DOC(1 downto 0) => DATA_OUT1(23 downto 22),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_18_23_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r1_0_15_24_29: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 2) => ADDRA(2 downto 1),
      ADDRA(1) => ADDRA(2),
      ADDRA(0) => ADDRA(0),
      ADDRB(4) => '0',
      ADDRB(3 downto 2) => ADDRA(2 downto 1),
      ADDRB(1) => ADDRA(2),
      ADDRB(0) => ADDRA(0),
      ADDRC(4) => '0',
      ADDRC(3 downto 2) => ADDRA(2 downto 1),
      ADDRC(1) => ADDRA(2),
      ADDRC(0) => ADDRA(0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(25 downto 24),
      DIB(1 downto 0) => DATA_IN(27 downto 26),
      DIC(1 downto 0) => DATA_IN(29 downto 28),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT1(25 downto 24),
      DOB(1 downto 0) => DATA_OUT1(27 downto 26),
      DOC(1 downto 0) => DATA_OUT1(29 downto 28),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_24_29_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r1_0_15_30_31: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 2) => ADDRA(2 downto 1),
      ADDRA(1) => ADDRA(2),
      ADDRA(0) => ADDRA(0),
      ADDRB(4) => '0',
      ADDRB(3 downto 2) => ADDRA(2 downto 1),
      ADDRB(1) => ADDRA(2),
      ADDRB(0) => ADDRA(0),
      ADDRC(4) => '0',
      ADDRC(3 downto 2) => ADDRA(2 downto 1),
      ADDRC(1) => ADDRA(2),
      ADDRC(0) => ADDRA(0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(31 downto 30),
      DIB(1 downto 0) => B"00",
      DIC(1 downto 0) => B"00",
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT1(31 downto 30),
      DOB(1 downto 0) => NLW_RF_reg_r1_0_15_30_31_DOB_UNCONNECTED(1 downto 0),
      DOC(1 downto 0) => NLW_RF_reg_r1_0_15_30_31_DOC_UNCONNECTED(1 downto 0),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_30_31_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r1_0_15_6_11: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 2) => ADDRA(2 downto 1),
      ADDRA(1) => ADDRA(2),
      ADDRA(0) => ADDRA(0),
      ADDRB(4) => '0',
      ADDRB(3 downto 2) => ADDRA(2 downto 1),
      ADDRB(1) => ADDRA(2),
      ADDRB(0) => ADDRA(0),
      ADDRC(4) => '0',
      ADDRC(3 downto 2) => ADDRA(2 downto 1),
      ADDRC(1) => ADDRA(2),
      ADDRC(0) => ADDRA(0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(7 downto 6),
      DIB(1 downto 0) => DATA_IN(9 downto 8),
      DIC(1 downto 0) => DATA_IN(11 downto 10),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT1(7 downto 6),
      DOB(1 downto 0) => DATA_OUT1(9 downto 8),
      DOC(1 downto 0) => DATA_OUT1(11 downto 10),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r2_0_15_0_5: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(1 downto 0),
      DIB(1 downto 0) => DATA_IN(3 downto 2),
      DIC(1 downto 0) => DATA_IN(5 downto 4),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT2(1 downto 0),
      DOB(1 downto 0) => DATA_OUT2(3 downto 2),
      DOC(1 downto 0) => DATA_OUT2(5 downto 4),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r2_0_15_12_17: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(13 downto 12),
      DIB(1 downto 0) => DATA_IN(15 downto 14),
      DIC(1 downto 0) => DATA_IN(17 downto 16),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT2(13 downto 12),
      DOB(1 downto 0) => DATA_OUT2(15 downto 14),
      DOC(1 downto 0) => DATA_OUT2(17 downto 16),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_12_17_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r2_0_15_18_23: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(19 downto 18),
      DIB(1 downto 0) => DATA_IN(21 downto 20),
      DIC(1 downto 0) => DATA_IN(23 downto 22),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT2(19 downto 18),
      DOB(1 downto 0) => DATA_OUT2(21 downto 20),
      DOC(1 downto 0) => DATA_OUT2(23 downto 22),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_18_23_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r2_0_15_24_29: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(25 downto 24),
      DIB(1 downto 0) => DATA_IN(27 downto 26),
      DIC(1 downto 0) => DATA_IN(29 downto 28),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT2(25 downto 24),
      DOB(1 downto 0) => DATA_OUT2(27 downto 26),
      DOC(1 downto 0) => DATA_OUT2(29 downto 28),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_24_29_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r2_0_15_30_31: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(31 downto 30),
      DIB(1 downto 0) => B"00",
      DIC(1 downto 0) => B"00",
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT2(31 downto 30),
      DOB(1 downto 0) => NLW_RF_reg_r2_0_15_30_31_DOB_UNCONNECTED(1 downto 0),
      DOC(1 downto 0) => NLW_RF_reg_r2_0_15_30_31_DOC_UNCONNECTED(1 downto 0),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_30_31_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
RF_reg_r2_0_15_6_11: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      DIA(1 downto 0) => DATA_IN(7 downto 6),
      DIB(1 downto 0) => DATA_IN(9 downto 8),
      DIC(1 downto 0) => DATA_IN(11 downto 10),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => DATA_OUT2(7 downto 6),
      DOB(1 downto 0) => DATA_OUT2(9 downto 8),
      DOC(1 downto 0) => DATA_OUT2(11 downto 10),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK_IBUF_BUFG,
      WE => RegWrite_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n is
  port (
    D : out STD_LOGIC_VECTOR ( 14 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \Dout_reg[4]_0\ : out STD_LOGIC;
    \Dout_reg[31]_0\ : out STD_LOGIC_VECTOR ( 29 downto 0 );
    RESET_IBUF : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[31]_1\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
end REGrwe_n;

architecture STRUCTURE of REGrwe_n is
  signal \Dout_reg[10]_i_1_n_0\ : STD_LOGIC;
  signal \Dout_reg[10]_i_1_n_1\ : STD_LOGIC;
  signal \Dout_reg[10]_i_1_n_2\ : STD_LOGIC;
  signal \Dout_reg[10]_i_1_n_3\ : STD_LOGIC;
  signal \Dout_reg[14]_i_1_n_0\ : STD_LOGIC;
  signal \Dout_reg[14]_i_1_n_1\ : STD_LOGIC;
  signal \Dout_reg[14]_i_1_n_2\ : STD_LOGIC;
  signal \Dout_reg[14]_i_1_n_3\ : STD_LOGIC;
  signal \Dout_reg[18]_i_1_n_0\ : STD_LOGIC;
  signal \Dout_reg[18]_i_1_n_1\ : STD_LOGIC;
  signal \Dout_reg[18]_i_1_n_2\ : STD_LOGIC;
  signal \Dout_reg[18]_i_1_n_3\ : STD_LOGIC;
  signal \Dout_reg[22]_i_1_n_0\ : STD_LOGIC;
  signal \Dout_reg[22]_i_1_n_1\ : STD_LOGIC;
  signal \Dout_reg[22]_i_1_n_2\ : STD_LOGIC;
  signal \Dout_reg[22]_i_1_n_3\ : STD_LOGIC;
  signal \Dout_reg[26]_i_1_n_0\ : STD_LOGIC;
  signal \Dout_reg[26]_i_1_n_1\ : STD_LOGIC;
  signal \Dout_reg[26]_i_1_n_2\ : STD_LOGIC;
  signal \Dout_reg[26]_i_1_n_3\ : STD_LOGIC;
  signal \Dout_reg[30]_i_1_n_0\ : STD_LOGIC;
  signal \Dout_reg[30]_i_1_n_1\ : STD_LOGIC;
  signal \Dout_reg[30]_i_1_n_2\ : STD_LOGIC;
  signal \Dout_reg[30]_i_1_n_3\ : STD_LOGIC;
  signal \Dout_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \Dout_reg[6]_i_1_n_1\ : STD_LOGIC;
  signal \Dout_reg[6]_i_1_n_2\ : STD_LOGIC;
  signal \Dout_reg[6]_i_1_n_3\ : STD_LOGIC;
  signal \Dout_reg_n_0_[10]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[11]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[12]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[13]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[14]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[15]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[16]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[17]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[18]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[19]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[20]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[21]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[22]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[23]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[24]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[25]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[26]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[27]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[28]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[29]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[30]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[31]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[8]\ : STD_LOGIC;
  signal \Dout_reg_n_0_[9]\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_Dout_reg[31]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_Dout_reg[31]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
begin
  Q(7 downto 0) <= \^q\(7 downto 0);
\Dout[0]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000C0290149"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(2),
      I2 => \^q\(4),
      I3 => \^q\(5),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => D(0)
    );
\Dout[16]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000060007B8"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(2),
      I2 => \^q\(4),
      I3 => \^q\(5),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => D(6)
    );
\Dout[18]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000C6A60000"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => D(7)
    );
\Dout[19]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000002200000"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => D(8)
    );
\Dout[20]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000004406240"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(4),
      I2 => \^q\(2),
      I3 => \^q\(6),
      I4 => \^q\(3),
      I5 => \^q\(7),
      O => D(9)
    );
\Dout[22]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000006400836"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \^q\(4),
      I3 => \^q\(5),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => D(10)
    );
\Dout[25]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000001DE91D"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \^q\(5),
      I3 => \^q\(6),
      I4 => \^q\(4),
      I5 => \^q\(7),
      O => D(11)
    );
\Dout[26]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000044060000"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => D(12)
    );
\Dout[27]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000200"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(3),
      I2 => \^q\(2),
      I3 => \^q\(5),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => D(13)
    );
\Dout[27]_rep_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000200"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(3),
      I2 => \^q\(2),
      I3 => \^q\(5),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => \Dout_reg[4]_0\
    );
\Dout[28]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000400"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(5),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \^q\(2),
      I5 => \^q\(7),
      O => D(14)
    );
\Dout[2]_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(2),
      O => \Dout_reg[31]_0\(0)
    );
\Dout[4]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001005"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(5),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(4),
      I5 => \^q\(7),
      O => D(1)
    );
\Dout[5]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000281D5"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(5),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => D(2)
    );
\Dout[6]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000140141"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \^q\(5),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => D(3)
    );
\Dout[7]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000001281D5"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(5),
      I4 => \^q\(6),
      I5 => \^q\(7),
      O => D(4)
    );
\Dout[8]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000400000"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(5),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \^q\(4),
      I5 => \^q\(7),
      O => D(5)
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(0),
      Q => \^q\(0),
      R => RESET_IBUF
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(10),
      Q => \Dout_reg_n_0_[10]\,
      R => RESET_IBUF
    );
\Dout_reg[10]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \Dout_reg[6]_i_1_n_0\,
      CO(3) => \Dout_reg[10]_i_1_n_0\,
      CO(2) => \Dout_reg[10]_i_1_n_1\,
      CO(1) => \Dout_reg[10]_i_1_n_2\,
      CO(0) => \Dout_reg[10]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \Dout_reg_n_0_[10]\,
      DI(2) => \Dout_reg_n_0_[9]\,
      DI(1) => \Dout_reg_n_0_[8]\,
      DI(0) => \^q\(7),
      O(3 downto 0) => \Dout_reg[31]_0\(8 downto 5),
      S(3) => \Dout_reg_n_0_[10]\,
      S(2) => \Dout_reg_n_0_[9]\,
      S(1) => \Dout_reg_n_0_[8]\,
      S(0) => \^q\(7)
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(11),
      Q => \Dout_reg_n_0_[11]\,
      R => RESET_IBUF
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(12),
      Q => \Dout_reg_n_0_[12]\,
      R => RESET_IBUF
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(13),
      Q => \Dout_reg_n_0_[13]\,
      R => RESET_IBUF
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(14),
      Q => \Dout_reg_n_0_[14]\,
      R => RESET_IBUF
    );
\Dout_reg[14]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \Dout_reg[10]_i_1_n_0\,
      CO(3) => \Dout_reg[14]_i_1_n_0\,
      CO(2) => \Dout_reg[14]_i_1_n_1\,
      CO(1) => \Dout_reg[14]_i_1_n_2\,
      CO(0) => \Dout_reg[14]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \Dout_reg_n_0_[14]\,
      DI(2) => \Dout_reg_n_0_[13]\,
      DI(1) => \Dout_reg_n_0_[12]\,
      DI(0) => \Dout_reg_n_0_[11]\,
      O(3 downto 0) => \Dout_reg[31]_0\(12 downto 9),
      S(3) => \Dout_reg_n_0_[14]\,
      S(2) => \Dout_reg_n_0_[13]\,
      S(1) => \Dout_reg_n_0_[12]\,
      S(0) => \Dout_reg_n_0_[11]\
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(15),
      Q => \Dout_reg_n_0_[15]\,
      R => RESET_IBUF
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(16),
      Q => \Dout_reg_n_0_[16]\,
      R => RESET_IBUF
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(17),
      Q => \Dout_reg_n_0_[17]\,
      R => RESET_IBUF
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(18),
      Q => \Dout_reg_n_0_[18]\,
      R => RESET_IBUF
    );
\Dout_reg[18]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \Dout_reg[14]_i_1_n_0\,
      CO(3) => \Dout_reg[18]_i_1_n_0\,
      CO(2) => \Dout_reg[18]_i_1_n_1\,
      CO(1) => \Dout_reg[18]_i_1_n_2\,
      CO(0) => \Dout_reg[18]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \Dout_reg_n_0_[18]\,
      DI(2) => \Dout_reg_n_0_[17]\,
      DI(1) => \Dout_reg_n_0_[16]\,
      DI(0) => \Dout_reg_n_0_[15]\,
      O(3 downto 0) => \Dout_reg[31]_0\(16 downto 13),
      S(3) => \Dout_reg_n_0_[18]\,
      S(2) => \Dout_reg_n_0_[17]\,
      S(1) => \Dout_reg_n_0_[16]\,
      S(0) => \Dout_reg_n_0_[15]\
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(19),
      Q => \Dout_reg_n_0_[19]\,
      R => RESET_IBUF
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(1),
      Q => \^q\(1),
      R => RESET_IBUF
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(20),
      Q => \Dout_reg_n_0_[20]\,
      R => RESET_IBUF
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(21),
      Q => \Dout_reg_n_0_[21]\,
      R => RESET_IBUF
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(22),
      Q => \Dout_reg_n_0_[22]\,
      R => RESET_IBUF
    );
\Dout_reg[22]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \Dout_reg[18]_i_1_n_0\,
      CO(3) => \Dout_reg[22]_i_1_n_0\,
      CO(2) => \Dout_reg[22]_i_1_n_1\,
      CO(1) => \Dout_reg[22]_i_1_n_2\,
      CO(0) => \Dout_reg[22]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \Dout_reg_n_0_[22]\,
      DI(2) => \Dout_reg_n_0_[21]\,
      DI(1) => \Dout_reg_n_0_[20]\,
      DI(0) => \Dout_reg_n_0_[19]\,
      O(3 downto 0) => \Dout_reg[31]_0\(20 downto 17),
      S(3) => \Dout_reg_n_0_[22]\,
      S(2) => \Dout_reg_n_0_[21]\,
      S(1) => \Dout_reg_n_0_[20]\,
      S(0) => \Dout_reg_n_0_[19]\
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(23),
      Q => \Dout_reg_n_0_[23]\,
      R => RESET_IBUF
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(24),
      Q => \Dout_reg_n_0_[24]\,
      R => RESET_IBUF
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(25),
      Q => \Dout_reg_n_0_[25]\,
      R => RESET_IBUF
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(26),
      Q => \Dout_reg_n_0_[26]\,
      R => RESET_IBUF
    );
\Dout_reg[26]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \Dout_reg[22]_i_1_n_0\,
      CO(3) => \Dout_reg[26]_i_1_n_0\,
      CO(2) => \Dout_reg[26]_i_1_n_1\,
      CO(1) => \Dout_reg[26]_i_1_n_2\,
      CO(0) => \Dout_reg[26]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \Dout_reg_n_0_[26]\,
      DI(2) => \Dout_reg_n_0_[25]\,
      DI(1) => \Dout_reg_n_0_[24]\,
      DI(0) => \Dout_reg_n_0_[23]\,
      O(3 downto 0) => \Dout_reg[31]_0\(24 downto 21),
      S(3) => \Dout_reg_n_0_[26]\,
      S(2) => \Dout_reg_n_0_[25]\,
      S(1) => \Dout_reg_n_0_[24]\,
      S(0) => \Dout_reg_n_0_[23]\
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(27),
      Q => \Dout_reg_n_0_[27]\,
      R => RESET_IBUF
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(28),
      Q => \Dout_reg_n_0_[28]\,
      R => RESET_IBUF
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(29),
      Q => \Dout_reg_n_0_[29]\,
      R => RESET_IBUF
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(2),
      Q => \^q\(2),
      R => RESET_IBUF
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(30),
      Q => \Dout_reg_n_0_[30]\,
      R => RESET_IBUF
    );
\Dout_reg[30]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \Dout_reg[26]_i_1_n_0\,
      CO(3) => \Dout_reg[30]_i_1_n_0\,
      CO(2) => \Dout_reg[30]_i_1_n_1\,
      CO(1) => \Dout_reg[30]_i_1_n_2\,
      CO(0) => \Dout_reg[30]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \Dout_reg_n_0_[30]\,
      DI(2) => \Dout_reg_n_0_[29]\,
      DI(1) => \Dout_reg_n_0_[28]\,
      DI(0) => \Dout_reg_n_0_[27]\,
      O(3 downto 0) => \Dout_reg[31]_0\(28 downto 25),
      S(3) => \Dout_reg_n_0_[30]\,
      S(2) => \Dout_reg_n_0_[29]\,
      S(1) => \Dout_reg_n_0_[28]\,
      S(0) => \Dout_reg_n_0_[27]\
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(31),
      Q => \Dout_reg_n_0_[31]\,
      R => RESET_IBUF
    );
\Dout_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \Dout_reg[30]_i_1_n_0\,
      CO(3 downto 0) => \NLW_Dout_reg[31]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_Dout_reg[31]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \Dout_reg[31]_0\(29),
      S(3 downto 1) => B"000",
      S(0) => \Dout_reg_n_0_[31]\
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(3),
      Q => \^q\(3),
      R => RESET_IBUF
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(4),
      Q => \^q\(4),
      R => RESET_IBUF
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(5),
      Q => \^q\(5),
      R => RESET_IBUF
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(6),
      Q => \^q\(6),
      R => RESET_IBUF
    );
\Dout_reg[6]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \Dout_reg[6]_i_1_n_0\,
      CO(2) => \Dout_reg[6]_i_1_n_1\,
      CO(1) => \Dout_reg[6]_i_1_n_2\,
      CO(0) => \Dout_reg[6]_i_1_n_3\,
      CYINIT => \^q\(2),
      DI(3 downto 0) => \^q\(6 downto 3),
      O(3 downto 0) => \Dout_reg[31]_0\(4 downto 1),
      S(3 downto 0) => \^q\(6 downto 3)
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(7),
      Q => \^q\(7),
      R => RESET_IBUF
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(8),
      Q => \Dout_reg_n_0_[8]\,
      R => RESET_IBUF
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[31]_1\(9),
      Q => \Dout_reg_n_0_[9]\,
      R => RESET_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_0 is
  port (
    \Dout_reg[30]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[30]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout[3]_i_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    RESET_IBUF : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_0 : entity is "REGrwe_n";
end REGrwe_n_0;

architecture STRUCTURE of REGrwe_n_0 is
  signal \^q\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_Dout_reg[3]_i_5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_Dout_reg[3]_i_5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_Dout_reg[3]_i_6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_Dout_reg[3]_i_6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  Q(31 downto 0) <= \^q\(31 downto 0);
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(0),
      Q => \^q\(0),
      R => RESET_IBUF
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(10),
      Q => \^q\(10),
      R => RESET_IBUF
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(11),
      Q => \^q\(11),
      R => RESET_IBUF
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(12),
      Q => \^q\(12),
      R => RESET_IBUF
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(13),
      Q => \^q\(13),
      R => RESET_IBUF
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(14),
      Q => \^q\(14),
      R => RESET_IBUF
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(15),
      Q => \^q\(15),
      R => RESET_IBUF
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(16),
      Q => \^q\(16),
      R => RESET_IBUF
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(17),
      Q => \^q\(17),
      R => RESET_IBUF
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(18),
      Q => \^q\(18),
      R => RESET_IBUF
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(19),
      Q => \^q\(19),
      R => RESET_IBUF
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(1),
      Q => \^q\(1),
      R => RESET_IBUF
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(20),
      Q => \^q\(20),
      R => RESET_IBUF
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(21),
      Q => \^q\(21),
      R => RESET_IBUF
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(22),
      Q => \^q\(22),
      R => RESET_IBUF
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(23),
      Q => \^q\(23),
      R => RESET_IBUF
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(24),
      Q => \^q\(24),
      R => RESET_IBUF
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(25),
      Q => \^q\(25),
      R => RESET_IBUF
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(26),
      Q => \^q\(26),
      R => RESET_IBUF
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(27),
      Q => \^q\(27),
      R => RESET_IBUF
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(28),
      Q => \^q\(28),
      R => RESET_IBUF
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(29),
      Q => \^q\(29),
      R => RESET_IBUF
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(2),
      Q => \^q\(2),
      R => RESET_IBUF
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(30),
      Q => \^q\(30),
      R => RESET_IBUF
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(31),
      Q => \^q\(31),
      R => RESET_IBUF
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(3),
      Q => \^q\(3),
      R => RESET_IBUF
    );
\Dout_reg[3]_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => CO(0),
      CO(3 downto 1) => \NLW_Dout_reg[3]_i_5_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \Dout_reg[30]_0\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_Dout_reg[3]_i_5_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\Dout_reg[3]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => \Dout[3]_i_3\(0),
      CO(3 downto 1) => \NLW_Dout_reg[3]_i_6_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \Dout_reg[30]_1\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_Dout_reg[3]_i_6_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(4),
      Q => \^q\(4),
      R => RESET_IBUF
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(5),
      Q => \^q\(5),
      R => RESET_IBUF
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(6),
      Q => \^q\(6),
      R => RESET_IBUF
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(7),
      Q => \^q\(7),
      R => RESET_IBUF
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(8),
      Q => \^q\(8),
      R => RESET_IBUF
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(9),
      Q => \^q\(9),
      R => RESET_IBUF
    );
\S0_carry__6_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(31),
      O => DI(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_1 is
  port (
    \Dout_reg[7]_rep\ : out STD_LOGIC;
    \Dout_reg[25]_0\ : out STD_LOGIC;
    \Dout_reg[7]_rep_0\ : out STD_LOGIC;
    \Dout_reg[26]_0\ : out STD_LOGIC;
    \Dout_reg[25]_1\ : out STD_LOGIC;
    \Dout_reg[7]_rep_1\ : out STD_LOGIC;
    \Dout_reg[24]_0\ : out STD_LOGIC;
    \Dout_reg[18]_0\ : out STD_LOGIC;
    \Dout_reg[19]_0\ : out STD_LOGIC;
    \Dout_reg[7]_rep_2\ : out STD_LOGIC;
    \Dout_reg[12]_0\ : out STD_LOGIC;
    \Dout_reg[7]_0\ : out STD_LOGIC;
    \Dout_reg[7]_rep_3\ : out STD_LOGIC;
    \Dout_reg[7]_rep_4\ : out STD_LOGIC;
    \Dout_reg[3]_0\ : out STD_LOGIC;
    \Dout_reg[7]_rep_5\ : out STD_LOGIC;
    \Dout_reg[0]_0\ : out STD_LOGIC;
    \Dout_reg[7]_rep_6\ : out STD_LOGIC;
    \Dout_reg[7]_rep_7\ : out STD_LOGIC;
    \Dout_reg[10]_0\ : out STD_LOGIC;
    \Dout_reg[14]_0\ : out STD_LOGIC;
    \Dout_reg[15]_0\ : out STD_LOGIC;
    \Dout_reg[7]_rep_8\ : out STD_LOGIC;
    \Dout_reg[14]_1\ : out STD_LOGIC;
    \Dout_reg[13]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[12]_1\ : out STD_LOGIC;
    \Dout_reg[2]_0\ : out STD_LOGIC;
    \Dout_reg[1]_0\ : out STD_LOGIC;
    \Dout_reg[3]_1\ : out STD_LOGIC;
    \Dout_reg[8]_0\ : out STD_LOGIC;
    \Dout_reg[15]_1\ : out STD_LOGIC;
    \Dout_reg[17]_0\ : out STD_LOGIC;
    \Dout_reg[22]_0\ : out STD_LOGIC;
    \Dout_reg[23]_0\ : out STD_LOGIC;
    \Dout_reg[24]_1\ : out STD_LOGIC;
    \Dout_reg[27]_0\ : out STD_LOGIC;
    \Dout_reg[30]_0\ : out STD_LOGIC;
    \Dout_reg[28]_0\ : in STD_LOGIC;
    Instr_out_OBUF : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \Dout_reg[23]_1\ : in STD_LOGIC;
    \Dout_reg[7]_1\ : in STD_LOGIC;
    \Dout_reg[6]_0\ : in STD_LOGIC;
    \Dout_reg[2]_1\ : in STD_LOGIC;
    \Dout_reg[16]_0\ : in STD_LOGIC;
    \Dout_reg[16]_1\ : in STD_LOGIC;
    \ALUResult_out_OBUF[28]_inst_i_2_0\ : in STD_LOGIC_VECTOR ( 21 downto 0 );
    \ALUResult_out_OBUF[28]_inst_i_2_1\ : in STD_LOGIC;
    RESET_IBUF : in STD_LOGIC;
    \Dout_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_1 : entity is "REGrwe_n";
end REGrwe_n_1;

architecture STRUCTURE of REGrwe_n_1 is
  signal \ALUResult_out_OBUF[12]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[21]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[23]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[28]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[28]_inst_i_8_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[28]_inst_i_9_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[2]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[2]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[7]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[7]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[8]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[9]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[9]_inst_i_7_n_0\ : STD_LOGIC;
  signal \^dout_reg[0]_0\ : STD_LOGIC;
  signal \^dout_reg[10]_0\ : STD_LOGIC;
  signal \^dout_reg[12]_0\ : STD_LOGIC;
  signal \^dout_reg[13]_0\ : STD_LOGIC;
  signal \^dout_reg[14]_0\ : STD_LOGIC;
  signal \^dout_reg[14]_1\ : STD_LOGIC;
  signal \^dout_reg[15]_0\ : STD_LOGIC;
  signal \^dout_reg[18]_0\ : STD_LOGIC;
  signal \^dout_reg[19]_0\ : STD_LOGIC;
  signal \^dout_reg[24]_0\ : STD_LOGIC;
  signal \^dout_reg[25]_0\ : STD_LOGIC;
  signal \^dout_reg[25]_1\ : STD_LOGIC;
  signal \^dout_reg[26]_0\ : STD_LOGIC;
  signal \^dout_reg[3]_0\ : STD_LOGIC;
  signal \^dout_reg[7]_0\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[10]_inst_i_6\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[12]_inst_i_6\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[12]_inst_i_7\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[13]_inst_i_6\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[13]_inst_i_8\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[15]_inst_i_7\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[16]_inst_i_6\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[24]_inst_i_7\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[28]_inst_i_6\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[28]_inst_i_9\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[31]_inst_i_8\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[9]_inst_i_6\ : label is "soft_lutpair42";
begin
  \Dout_reg[0]_0\ <= \^dout_reg[0]_0\;
  \Dout_reg[10]_0\ <= \^dout_reg[10]_0\;
  \Dout_reg[12]_0\ <= \^dout_reg[12]_0\;
  \Dout_reg[13]_0\ <= \^dout_reg[13]_0\;
  \Dout_reg[14]_0\ <= \^dout_reg[14]_0\;
  \Dout_reg[14]_1\ <= \^dout_reg[14]_1\;
  \Dout_reg[15]_0\ <= \^dout_reg[15]_0\;
  \Dout_reg[18]_0\ <= \^dout_reg[18]_0\;
  \Dout_reg[19]_0\ <= \^dout_reg[19]_0\;
  \Dout_reg[24]_0\ <= \^dout_reg[24]_0\;
  \Dout_reg[25]_0\ <= \^dout_reg[25]_0\;
  \Dout_reg[25]_1\ <= \^dout_reg[25]_1\;
  \Dout_reg[26]_0\ <= \^dout_reg[26]_0\;
  \Dout_reg[3]_0\ <= \^dout_reg[3]_0\;
  \Dout_reg[7]_0\ <= \^dout_reg[7]_0\;
  Q(31 downto 0) <= \^q\(31 downto 0);
\ALUResult_out_OBUF[0]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(3),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(3),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(1),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(1),
      O => \Dout_reg[3]_1\
    );
\ALUResult_out_OBUF[10]_inst_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44CF77CF"
    )
        port map (
      I0 => \^q\(12),
      I1 => Instr_out_OBUF(1),
      I2 => \ALUResult_out_OBUF[28]_inst_i_2_0\(10),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \^q\(10),
      O => \^dout_reg[12]_0\
    );
\ALUResult_out_OBUF[10]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(7),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(7),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(9),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(9),
      O => \^dout_reg[7]_0\
    );
\ALUResult_out_OBUF[10]_inst_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5050303F5F5F303F"
    )
        port map (
      I0 => \^q\(8),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(8),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(10),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(10),
      O => \Dout_reg[8]_0\
    );
\ALUResult_out_OBUF[12]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"30303F3F5F505F50"
    )
        port map (
      I0 => \^dout_reg[10]_0\,
      I1 => \ALUResult_out_OBUF[12]_inst_i_6_n_0\,
      I2 => \Dout_reg[28]_0\,
      I3 => \^dout_reg[14]_0\,
      I4 => \^dout_reg[15]_0\,
      I5 => Instr_out_OBUF(0),
      O => \Dout_reg[7]_rep_7\
    );
\ALUResult_out_OBUF[12]_inst_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"305F3F5F"
    )
        port map (
      I0 => \ALUResult_out_OBUF[28]_inst_i_2_0\(9),
      I1 => \^q\(9),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \^q\(11),
      O => \ALUResult_out_OBUF[12]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[12]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CFC0A0A0"
    )
        port map (
      I0 => \ALUResult_out_OBUF[28]_inst_i_2_0\(11),
      I1 => \^q\(14),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(12),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      O => \^dout_reg[14]_0\
    );
\ALUResult_out_OBUF[13]_inst_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"305F3F5F"
    )
        port map (
      I0 => \ALUResult_out_OBUF[28]_inst_i_2_0\(12),
      I1 => \^q\(15),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \^q\(13),
      O => \^dout_reg[15]_0\
    );
\ALUResult_out_OBUF[13]_inst_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"305F3F5F"
    )
        port map (
      I0 => \ALUResult_out_OBUF[28]_inst_i_2_0\(10),
      I1 => \^q\(10),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \^q\(12),
      O => \^dout_reg[10]_0\
    );
\ALUResult_out_OBUF[15]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BB308830"
    )
        port map (
      I0 => \^q\(12),
      I1 => Instr_out_OBUF(1),
      I2 => \ALUResult_out_OBUF[28]_inst_i_2_0\(11),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \^q\(14),
      O => \Dout_reg[12]_1\
    );
\ALUResult_out_OBUF[16]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C0C0CFCF5F505F50"
    )
        port map (
      I0 => \^dout_reg[14]_1\,
      I1 => \^dout_reg[13]_0\,
      I2 => \Dout_reg[28]_0\,
      I3 => \Dout_reg[16]_0\,
      I4 => \Dout_reg[16]_1\,
      I5 => Instr_out_OBUF(0),
      O => \Dout_reg[7]_rep_8\
    );
\ALUResult_out_OBUF[16]_inst_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \^q\(13),
      I1 => Instr_out_OBUF(1),
      I2 => \^q\(15),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_0\(12),
      O => \^dout_reg[13]_0\
    );
\ALUResult_out_OBUF[17]_inst_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => \^q\(14),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(11),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(16),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(13),
      O => \^dout_reg[14]_1\
    );
\ALUResult_out_OBUF[18]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFAFCFC0A0A0CFC0"
    )
        port map (
      I0 => \^q\(15),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(12),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(14),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(17),
      O => \Dout_reg[15]_1\
    );
\ALUResult_out_OBUF[20]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5050303F5F5F303F"
    )
        port map (
      I0 => \^q\(17),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(14),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(17),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(19),
      O => \Dout_reg[17]_0\
    );
\ALUResult_out_OBUF[21]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F000FFF55335533"
    )
        port map (
      I0 => \^dout_reg[24]_0\,
      I1 => \ALUResult_out_OBUF[21]_inst_i_6_n_0\,
      I2 => \^dout_reg[18]_0\,
      I3 => Instr_out_OBUF(0),
      I4 => \^dout_reg[19]_0\,
      I5 => \Dout_reg[28]_0\,
      O => \Dout_reg[7]_rep_1\
    );
\ALUResult_out_OBUF[21]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5050303F5F5F303F"
    )
        port map (
      I0 => \^q\(23),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(19),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(17),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(21),
      O => \ALUResult_out_OBUF[21]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[21]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => \^q\(18),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(15),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(20),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(16),
      O => \^dout_reg[18]_0\
    );
\ALUResult_out_OBUF[22]_inst_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"440F770F"
    )
        port map (
      I0 => \^q\(19),
      I1 => Instr_out_OBUF(1),
      I2 => \ALUResult_out_OBUF[28]_inst_i_2_0\(17),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \^q\(21),
      O => \^dout_reg[19]_0\
    );
\ALUResult_out_OBUF[22]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => \^q\(24),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(20),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(22),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(18),
      O => \^dout_reg[24]_0\
    );
\ALUResult_out_OBUF[23]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout_reg[23]_1\,
      I1 => \ALUResult_out_OBUF[23]_inst_i_7_n_0\,
      I2 => \Dout_reg[28]_0\,
      I3 => \^dout_reg[26]_0\,
      I4 => Instr_out_OBUF(0),
      I5 => \^dout_reg[25]_1\,
      O => \Dout_reg[7]_rep_0\
    );
\ALUResult_out_OBUF[23]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFAFCFC0A0A0CFC0"
    )
        port map (
      I0 => \^q\(21),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(17),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(19),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(23),
      O => \ALUResult_out_OBUF[23]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[23]_inst_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFAFCFC0A0A0CFC0"
    )
        port map (
      I0 => \^q\(25),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(21),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(19),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(23),
      O => \^dout_reg[25]_1\
    );
\ALUResult_out_OBUF[24]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFAFCFC0A0A0CFC0"
    )
        port map (
      I0 => \^q\(26),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(21),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(20),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(24),
      O => \^dout_reg[26]_0\
    );
\ALUResult_out_OBUF[24]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"440F770F"
    )
        port map (
      I0 => \^q\(27),
      I1 => Instr_out_OBUF(1),
      I2 => \ALUResult_out_OBUF[28]_inst_i_2_0\(21),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \^q\(25),
      O => \Dout_reg[27]_0\
    );
\ALUResult_out_OBUF[25]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => \^q\(22),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(18),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(24),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(20),
      O => \Dout_reg[22]_0\
    );
\ALUResult_out_OBUF[26]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5050303F5F5F303F"
    )
        port map (
      I0 => \^q\(23),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(19),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(21),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(25),
      O => \Dout_reg[23]_0\
    );
\ALUResult_out_OBUF[27]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5050303F5F5F303F"
    )
        port map (
      I0 => \^q\(24),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(20),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(21),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(26),
      O => \Dout_reg[24]_1\
    );
\ALUResult_out_OBUF[28]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFC05F5FCFC05050"
    )
        port map (
      I0 => \ALUResult_out_OBUF[28]_inst_i_5_n_0\,
      I1 => \^dout_reg[25]_0\,
      I2 => \Dout_reg[28]_0\,
      I3 => \ALUResult_out_OBUF[28]_inst_i_8_n_0\,
      I4 => Instr_out_OBUF(0),
      I5 => \ALUResult_out_OBUF[28]_inst_i_9_n_0\,
      O => \Dout_reg[7]_rep\
    );
\ALUResult_out_OBUF[28]_inst_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \^q\(26),
      I1 => Instr_out_OBUF(1),
      I2 => \^q\(28),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_0\(21),
      O => \ALUResult_out_OBUF[28]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[28]_inst_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"470047FF"
    )
        port map (
      I0 => \^q\(25),
      I1 => Instr_out_OBUF(1),
      I2 => \^q\(27),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_0\(21),
      O => \^dout_reg[25]_0\
    );
\ALUResult_out_OBUF[28]_inst_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"470047FF"
    )
        port map (
      I0 => \^q\(31),
      I1 => Instr_out_OBUF(1),
      I2 => \^q\(29),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_0\(21),
      O => \ALUResult_out_OBUF[28]_inst_i_8_n_0\
    );
\ALUResult_out_OBUF[28]_inst_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"470047FF"
    )
        port map (
      I0 => \^q\(30),
      I1 => Instr_out_OBUF(1),
      I2 => \^q\(28),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_0\(21),
      O => \ALUResult_out_OBUF[28]_inst_i_9_n_0\
    );
\ALUResult_out_OBUF[2]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FAFC0AFCFA0C0A0C"
    )
        port map (
      I0 => \ALUResult_out_OBUF[2]_inst_i_6_n_0\,
      I1 => \ALUResult_out_OBUF[2]_inst_i_7_n_0\,
      I2 => \Dout_reg[28]_0\,
      I3 => Instr_out_OBUF(0),
      I4 => \Dout_reg[2]_1\,
      I5 => \^dout_reg[0]_0\,
      O => \Dout_reg[7]_rep_5\
    );
\ALUResult_out_OBUF[2]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(5),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(5),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(3),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(3),
      O => \ALUResult_out_OBUF[2]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[2]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(4),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(4),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(2),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(2),
      O => \ALUResult_out_OBUF[2]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[31]_inst_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"303F505F"
    )
        port map (
      I0 => \^q\(30),
      I1 => \^q\(28),
      I2 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(21),
      I4 => Instr_out_OBUF(1),
      O => \Dout_reg[30]_0\
    );
\ALUResult_out_OBUF[3]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(0),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(0),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(2),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(2),
      O => \^dout_reg[0]_0\
    );
\ALUResult_out_OBUF[4]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(1),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(1),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(3),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(3),
      O => \Dout_reg[1]_0\
    );
\ALUResult_out_OBUF[5]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(2),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(2),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(4),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(4),
      O => \Dout_reg[2]_0\
    );
\ALUResult_out_OBUF[6]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^dout_reg[3]_0\,
      I1 => \ALUResult_out_OBUF[7]_inst_i_7_n_0\,
      I2 => \Dout_reg[28]_0\,
      I3 => \ALUResult_out_OBUF[7]_inst_i_6_n_0\,
      I4 => Instr_out_OBUF(0),
      I5 => \Dout_reg[6]_0\,
      O => \Dout_reg[7]_rep_4\
    );
\ALUResult_out_OBUF[6]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(3),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(3),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(5),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(5),
      O => \^dout_reg[3]_0\
    );
\ALUResult_out_OBUF[7]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FA0C0A0CFAFC0AFC"
    )
        port map (
      I0 => \ALUResult_out_OBUF[8]_inst_i_7_n_0\,
      I1 => \ALUResult_out_OBUF[7]_inst_i_6_n_0\,
      I2 => \Dout_reg[28]_0\,
      I3 => Instr_out_OBUF(0),
      I4 => \ALUResult_out_OBUF[7]_inst_i_7_n_0\,
      I5 => \Dout_reg[7]_1\,
      O => \Dout_reg[7]_rep_3\
    );
\ALUResult_out_OBUF[7]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(9),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(9),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(7),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(7),
      O => \ALUResult_out_OBUF[7]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[7]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFAFCFC0A0A0CFC0"
    )
        port map (
      I0 => \^q\(4),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(4),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(6),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(6),
      O => \ALUResult_out_OBUF[7]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[8]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"30303F3F5F505F50"
    )
        port map (
      I0 => \ALUResult_out_OBUF[9]_inst_i_7_n_0\,
      I1 => \Dout_reg[7]_1\,
      I2 => \Dout_reg[28]_0\,
      I3 => \ALUResult_out_OBUF[8]_inst_i_7_n_0\,
      I4 => \ALUResult_out_OBUF[9]_inst_i_6_n_0\,
      I5 => Instr_out_OBUF(0),
      O => \Dout_reg[7]_rep_6\
    );
\ALUResult_out_OBUF[8]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFAFCFC0A0A0CFC0"
    )
        port map (
      I0 => \^q\(10),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(10),
      I2 => Instr_out_OBUF(1),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_0\(8),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \^q\(8),
      O => \ALUResult_out_OBUF[8]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[9]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"05F5F3F305F50303"
    )
        port map (
      I0 => \^dout_reg[12]_0\,
      I1 => \ALUResult_out_OBUF[9]_inst_i_6_n_0\,
      I2 => \Dout_reg[28]_0\,
      I3 => \ALUResult_out_OBUF[9]_inst_i_7_n_0\,
      I4 => Instr_out_OBUF(0),
      I5 => \^dout_reg[7]_0\,
      O => \Dout_reg[7]_rep_2\
    );
\ALUResult_out_OBUF[9]_inst_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44CF77CF"
    )
        port map (
      I0 => \^q\(11),
      I1 => Instr_out_OBUF(1),
      I2 => \ALUResult_out_OBUF[28]_inst_i_2_0\(9),
      I3 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I4 => \^q\(9),
      O => \ALUResult_out_OBUF[9]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[9]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => \^q\(6),
      I1 => \ALUResult_out_OBUF[28]_inst_i_2_0\(6),
      I2 => Instr_out_OBUF(1),
      I3 => \^q\(8),
      I4 => \ALUResult_out_OBUF[28]_inst_i_2_1\,
      I5 => \ALUResult_out_OBUF[28]_inst_i_2_0\(8),
      O => \ALUResult_out_OBUF[9]_inst_i_7_n_0\
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(0),
      Q => \^q\(0),
      R => RESET_IBUF
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(10),
      Q => \^q\(10),
      R => RESET_IBUF
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(11),
      Q => \^q\(11),
      R => RESET_IBUF
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(12),
      Q => \^q\(12),
      R => RESET_IBUF
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(13),
      Q => \^q\(13),
      R => RESET_IBUF
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(14),
      Q => \^q\(14),
      R => RESET_IBUF
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(15),
      Q => \^q\(15),
      R => RESET_IBUF
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(16),
      Q => \^q\(16),
      R => RESET_IBUF
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(17),
      Q => \^q\(17),
      R => RESET_IBUF
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(18),
      Q => \^q\(18),
      R => RESET_IBUF
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(19),
      Q => \^q\(19),
      R => RESET_IBUF
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(1),
      Q => \^q\(1),
      R => RESET_IBUF
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(20),
      Q => \^q\(20),
      R => RESET_IBUF
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(21),
      Q => \^q\(21),
      R => RESET_IBUF
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(22),
      Q => \^q\(22),
      R => RESET_IBUF
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(23),
      Q => \^q\(23),
      R => RESET_IBUF
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(24),
      Q => \^q\(24),
      R => RESET_IBUF
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(25),
      Q => \^q\(25),
      R => RESET_IBUF
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(26),
      Q => \^q\(26),
      R => RESET_IBUF
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(27),
      Q => \^q\(27),
      R => RESET_IBUF
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(28),
      Q => \^q\(28),
      R => RESET_IBUF
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(29),
      Q => \^q\(29),
      R => RESET_IBUF
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(2),
      Q => \^q\(2),
      R => RESET_IBUF
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(30),
      Q => \^q\(30),
      R => RESET_IBUF
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(31),
      Q => \^q\(31),
      R => RESET_IBUF
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(3),
      Q => \^q\(3),
      R => RESET_IBUF
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(4),
      Q => \^q\(4),
      R => RESET_IBUF
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(5),
      Q => \^q\(5),
      R => RESET_IBUF
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(6),
      Q => \^q\(6),
      R => RESET_IBUF
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(7),
      Q => \^q\(7),
      R => RESET_IBUF
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(8),
      Q => \^q\(8),
      R => RESET_IBUF
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[31]_0\(9),
      Q => \^q\(9),
      R => RESET_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_2 is
  port (
    \Dout_reg[5]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 21 downto 0 );
    \Dout_reg[8]_0\ : out STD_LOGIC;
    \Dout_reg[16]_0\ : out STD_LOGIC;
    \Dout_reg[16]_1\ : out STD_LOGIC;
    \Dout_reg[18]_0\ : out STD_LOGIC;
    \Dout_reg[21]_0\ : out STD_LOGIC;
    \Dout_reg[20]_0\ : out STD_LOGIC;
    \Dout_reg[20]_1\ : out STD_LOGIC;
    \ALUResult_out_OBUF[23]_inst_i_3\ : in STD_LOGIC_VECTOR ( 10 downto 0 );
    Instr_out_OBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    \ALUResult_out_OBUF[23]_inst_i_3_0\ : in STD_LOGIC;
    RESET_IBUF : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 21 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_2 : entity is "REGrwe_n";
end REGrwe_n_2;

architecture STRUCTURE of REGrwe_n_2 is
  signal \^q\ : STD_LOGIC_VECTOR ( 21 downto 0 );
begin
  Q(21 downto 0) <= \^q\(21 downto 0);
\ALUResult_out_OBUF[13]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCFAFA0C0C0AFA0"
    )
        port map (
      I0 => \^q\(13),
      I1 => \ALUResult_out_OBUF[23]_inst_i_3\(5),
      I2 => Instr_out_OBUF(0),
      I3 => \^q\(11),
      I4 => \ALUResult_out_OBUF[23]_inst_i_3_0\,
      I5 => \ALUResult_out_OBUF[23]_inst_i_3\(4),
      O => \Dout_reg[16]_0\
    );
\ALUResult_out_OBUF[16]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCFAFA0C0C0AFA0"
    )
        port map (
      I0 => \^q\(15),
      I1 => \ALUResult_out_OBUF[23]_inst_i_3\(7),
      I2 => Instr_out_OBUF(0),
      I3 => \^q\(13),
      I4 => \ALUResult_out_OBUF[23]_inst_i_3_0\,
      I5 => \ALUResult_out_OBUF[23]_inst_i_3\(5),
      O => \Dout_reg[18]_0\
    );
\ALUResult_out_OBUF[17]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"303F5050303F5F5F"
    )
        port map (
      I0 => \^q\(17),
      I1 => \ALUResult_out_OBUF[23]_inst_i_3\(8),
      I2 => Instr_out_OBUF(0),
      I3 => \ALUResult_out_OBUF[23]_inst_i_3\(6),
      I4 => \ALUResult_out_OBUF[23]_inst_i_3_0\,
      I5 => \^q\(14),
      O => \Dout_reg[21]_0\
    );
\ALUResult_out_OBUF[17]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCFAFA0C0C0AFA0"
    )
        port map (
      I0 => \^q\(16),
      I1 => \ALUResult_out_OBUF[23]_inst_i_3\(9),
      I2 => Instr_out_OBUF(0),
      I3 => \^q\(15),
      I4 => \ALUResult_out_OBUF[23]_inst_i_3_0\,
      I5 => \ALUResult_out_OBUF[23]_inst_i_3\(7),
      O => \Dout_reg[20]_0\
    );
\ALUResult_out_OBUF[19]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCFAFA0C0C0AFA0"
    )
        port map (
      I0 => \^q\(13),
      I1 => \ALUResult_out_OBUF[23]_inst_i_3\(5),
      I2 => Instr_out_OBUF(0),
      I3 => \^q\(15),
      I4 => \ALUResult_out_OBUF[23]_inst_i_3_0\,
      I5 => \ALUResult_out_OBUF[23]_inst_i_3\(7),
      O => \Dout_reg[16]_1\
    );
\ALUResult_out_OBUF[23]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCFAFA0C0C0AFA0"
    )
        port map (
      I0 => \^q\(16),
      I1 => \ALUResult_out_OBUF[23]_inst_i_3\(9),
      I2 => Instr_out_OBUF(0),
      I3 => \^q\(18),
      I4 => \ALUResult_out_OBUF[23]_inst_i_3_0\,
      I5 => \ALUResult_out_OBUF[23]_inst_i_3\(10),
      O => \Dout_reg[20]_1\
    );
\ALUResult_out_OBUF[6]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCFAFA0C0C0AFA0"
    )
        port map (
      I0 => \^q\(8),
      I1 => \ALUResult_out_OBUF[23]_inst_i_3\(3),
      I2 => Instr_out_OBUF(0),
      I3 => \^q\(6),
      I4 => \ALUResult_out_OBUF[23]_inst_i_3_0\,
      I5 => \ALUResult_out_OBUF[23]_inst_i_3\(1),
      O => \Dout_reg[8]_0\
    );
\ALUResult_out_OBUF[8]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3030505F3F3F505F"
    )
        port map (
      I0 => \^q\(5),
      I1 => \ALUResult_out_OBUF[23]_inst_i_3\(0),
      I2 => Instr_out_OBUF(0),
      I3 => \^q\(7),
      I4 => \ALUResult_out_OBUF[23]_inst_i_3_0\,
      I5 => \ALUResult_out_OBUF[23]_inst_i_3\(2),
      O => \Dout_reg[5]_0\
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(0),
      Q => \^q\(0),
      R => RESET_IBUF
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(10),
      Q => \^q\(10),
      R => RESET_IBUF
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(11),
      Q => \^q\(11),
      R => RESET_IBUF
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(12),
      Q => \^q\(12),
      R => RESET_IBUF
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(13),
      Q => \^q\(13),
      R => RESET_IBUF
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(14),
      Q => \^q\(14),
      R => RESET_IBUF
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(15),
      Q => \^q\(15),
      R => RESET_IBUF
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(1),
      Q => \^q\(1),
      R => RESET_IBUF
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(16),
      Q => \^q\(16),
      R => RESET_IBUF
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(17),
      Q => \^q\(17),
      R => RESET_IBUF
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(18),
      Q => \^q\(18),
      R => RESET_IBUF
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(19),
      Q => \^q\(19),
      R => RESET_IBUF
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(20),
      Q => \^q\(20),
      R => RESET_IBUF
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(2),
      Q => \^q\(2),
      R => RESET_IBUF
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(21),
      Q => \^q\(21),
      R => RESET_IBUF
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(3),
      Q => \^q\(3),
      R => RESET_IBUF
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(4),
      Q => \^q\(4),
      R => RESET_IBUF
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(5),
      Q => \^q\(5),
      R => RESET_IBUF
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(6),
      Q => \^q\(6),
      R => RESET_IBUF
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(7),
      Q => \^q\(7),
      R => RESET_IBUF
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(8),
      Q => \^q\(8),
      R => RESET_IBUF
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(9),
      Q => \^q\(9),
      R => RESET_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_3 is
  port (
    \Dout[1]_i_9_0\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[25]_rep_0\ : out STD_LOGIC;
    \Dout_reg[26]_rep_0\ : out STD_LOGIC;
    \Dout_reg[23]_rep_0\ : out STD_LOGIC;
    \Dout_reg[25]_rep_1\ : out STD_LOGIC;
    \Dout[1]_i_6_0\ : out STD_LOGIC;
    \Dout_reg[21]_rep_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \Dout_reg[8]_rep_0\ : out STD_LOGIC;
    \Dout[1]_i_8_0\ : out STD_LOGIC;
    \Dout[1]_i_7_0\ : out STD_LOGIC;
    \Dout_reg[25]_rep_2\ : out STD_LOGIC;
    \Dout_reg[23]_rep_1\ : out STD_LOGIC;
    \Dout_reg[25]_rep_3\ : out STD_LOGIC;
    \Dout_reg[26]_rep_1\ : out STD_LOGIC;
    \Dout_reg[24]_rep_0\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \Dout_reg[27]_rep__0_0\ : out STD_LOGIC;
    \Dout_reg[26]_rep_2\ : out STD_LOGIC;
    \Dout_reg[27]_rep__0_1\ : out STD_LOGIC_VECTOR ( 21 downto 0 );
    \Dout_reg[20]_rep_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_IN : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[19]_rep_rep_0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ADDRA : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \Dout_reg[15]_rep_0\ : out STD_LOGIC;
    \Dout_reg[15]_rep_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ADDRD : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[28]_rep_0\ : out STD_LOGIC;
    \Dout_reg[15]_rep_2\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[27]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[31]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[11]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[15]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[27]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[31]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[23]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[7]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[11]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[15]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[21]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[19]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[23]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[31]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[31]_3\ : out STD_LOGIC_VECTOR ( 26 downto 0 );
    \Dout_reg[31]_rep_0\ : out STD_LOGIC;
    \Dout_reg[19]_rep_0\ : out STD_LOGIC;
    \Dout_reg[30]_0\ : in STD_LOGIC_VECTOR ( 17 downto 0 );
    \Dout_reg[30]_1\ : in STD_LOGIC_VECTOR ( 17 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[2]_0\ : in STD_LOGIC;
    \Dout_reg[2]_1\ : in STD_LOGIC;
    \Dout_reg[3]_1\ : in STD_LOGIC;
    \Dout_reg[3]_2\ : in STD_LOGIC;
    \Dout_reg[6]_0\ : in STD_LOGIC;
    \Dout_reg[6]_1\ : in STD_LOGIC;
    \Dout_reg[7]_2\ : in STD_LOGIC;
    \Dout_reg[7]_3\ : in STD_LOGIC;
    \Dout_reg[8]_0\ : in STD_LOGIC;
    \Dout_reg[8]_1\ : in STD_LOGIC;
    \Dout_reg[9]\ : in STD_LOGIC;
    \Dout_reg[9]_0\ : in STD_LOGIC;
    \Dout_reg[10]\ : in STD_LOGIC;
    \Dout_reg[12]_0\ : in STD_LOGIC;
    \Dout_reg[12]_1\ : in STD_LOGIC;
    \Dout_reg[14]_0\ : in STD_LOGIC;
    \Dout_reg[15]_2\ : in STD_LOGIC;
    \Dout_reg[16]_0\ : in STD_LOGIC;
    \Dout_reg[16]_1\ : in STD_LOGIC;
    \Dout_reg[20]_0\ : in STD_LOGIC;
    \Dout_reg[21]_1\ : in STD_LOGIC;
    \Dout_reg[21]_2\ : in STD_LOGIC;
    \Dout_reg[23]_2\ : in STD_LOGIC;
    \Dout_reg[23]_3\ : in STD_LOGIC;
    \Dout_reg[28]_0\ : in STD_LOGIC;
    \Dout_reg[26]_0\ : in STD_LOGIC;
    \Dout_reg[26]_1\ : in STD_LOGIC;
    \Dout_reg[22]_0\ : in STD_LOGIC;
    \Dout_reg[22]_1\ : in STD_LOGIC;
    \Dout_reg[22]_2\ : in STD_LOGIC;
    \Dout_reg[22]_3\ : in STD_LOGIC;
    \Dout_reg[17]\ : in STD_LOGIC;
    \Dout_reg[17]_0\ : in STD_LOGIC;
    \Dout_reg[17]_1\ : in STD_LOGIC;
    \Dout_reg[17]_2\ : in STD_LOGIC;
    \Dout_reg[13]_0\ : in STD_LOGIC;
    \Dout_reg[13]_1\ : in STD_LOGIC;
    \Dout_reg[13]_2\ : in STD_LOGIC;
    \Dout_reg[11]_1\ : in STD_LOGIC;
    \Dout_reg[10]_0\ : in STD_LOGIC;
    \Dout_reg[10]_1\ : in STD_LOGIC;
    \Dout_reg[10]_2\ : in STD_LOGIC;
    \Dout_reg[4]_0\ : in STD_LOGIC;
    \Dout_reg[0]_0\ : in STD_LOGIC;
    \Dout_reg[19]_1\ : in STD_LOGIC;
    \Dout_reg[19]_2\ : in STD_LOGIC;
    \Dout_reg[24]_0\ : in STD_LOGIC;
    \Dout_reg[24]_1\ : in STD_LOGIC;
    \ALUResult_out_OBUF[31]_inst_i_2_0\ : in STD_LOGIC;
    \Dout_reg[28]_1\ : in STD_LOGIC_VECTOR ( 21 downto 0 );
    \ALUResult_out_OBUF[29]_inst_i_3_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \ALUResult_out_OBUF[31]_inst_i_4_0\ : in STD_LOGIC;
    \Dout_reg[27]_2\ : in STD_LOGIC;
    \Dout_reg[25]_0\ : in STD_LOGIC;
    \Dout_reg[20]_1\ : in STD_LOGIC;
    \Dout_reg[15]_3\ : in STD_LOGIC;
    \Dout_reg[15]_4\ : in STD_LOGIC;
    \Dout_reg[5]_0\ : in STD_LOGIC;
    \FSM_onehot_current_state_reg[13]\ : in STD_LOGIC;
    \Dout_reg[31]_4\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \Dout_reg[31]_5\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ReadData_in_regRD : in STD_LOGIC_VECTOR ( 31 downto 0 );
    RF_reg_r2_0_15_30_31 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DATA_OUT1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    PCPlus8 : in STD_LOGIC_VECTOR ( 28 downto 0 );
    \FSM_onehot_current_state_reg[2]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    Flags_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \Dout_reg[31]_6\ : in STD_LOGIC;
    Result_out_OBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    RESET_IBUF : in STD_LOGIC;
    \Dout_reg[31]_7\ : in STD_LOGIC_VECTOR ( 26 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC;
    \Dout_reg[27]_rep_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_3 : entity is "REGrwe_n";
end REGrwe_n_3;

architecture STRUCTURE of REGrwe_n_3 is
  signal \ALU/p_0_out\ : STD_LOGIC_VECTOR ( 30 downto 1 );
  signal \ALUControl_out_OBUF[0]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUControl_out_OBUF[1]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[0]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[0]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[0]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[0]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[0]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[0]_inst_i_8_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[10]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[10]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[11]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[11]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[11]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[11]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[11]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[11]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[12]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[12]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[13]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[13]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[13]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[13]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[13]_inst_i_9_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[14]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[14]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[14]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[14]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[15]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[15]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[15]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[16]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[17]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[17]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[17]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[17]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[18]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[18]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[18]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[18]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[19]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[19]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[19]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[19]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[1]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[1]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[1]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[1]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[1]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[20]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[20]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[20]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[21]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[22]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[22]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[22]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[22]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[23]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[24]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[24]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[24]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[24]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[25]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[25]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[25]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[25]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[26]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[26]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[26]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[26]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[27]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[27]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[27]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[27]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[28]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[28]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[29]_inst_i_10_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[29]_inst_i_11_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[29]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[29]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[29]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[29]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[29]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[29]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[29]_inst_i_8_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[29]_inst_i_9_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[2]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_10_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_11_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_12_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_13_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_15_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_16_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_17_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_8_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[30]_inst_i_9_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[31]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[31]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[31]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[31]_inst_i_7_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[3]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[3]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[3]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[3]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[4]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[4]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[4]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[4]_inst_i_6_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[5]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[5]_inst_i_3_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[5]_inst_i_4_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[5]_inst_i_5_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[6]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[7]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[8]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUResult_out_OBUF[9]_inst_i_2_n_0\ : STD_LOGIC;
  signal \^d\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \Dout[1]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_9_n_0\ : STD_LOGIC;
  signal \^dout_reg[15]_rep_1\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^dout_reg[20]_rep_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^dout_reg[21]_rep_0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^dout_reg[23]_rep_0\ : STD_LOGIC;
  signal \^dout_reg[23]_rep_1\ : STD_LOGIC;
  signal \^dout_reg[25]_rep_0\ : STD_LOGIC;
  signal \^dout_reg[25]_rep_1\ : STD_LOGIC;
  signal \^dout_reg[25]_rep_2\ : STD_LOGIC;
  signal \^dout_reg[25]_rep_3\ : STD_LOGIC;
  signal \^dout_reg[26]_rep_0\ : STD_LOGIC;
  signal \^dout_reg[26]_rep_1\ : STD_LOGIC;
  signal \^dout_reg[26]_rep_2\ : STD_LOGIC;
  signal \^dout_reg[27]_rep__0_0\ : STD_LOGIC;
  signal \Dout_reg[27]_rep_n_0\ : STD_LOGIC;
  signal \^dout_reg[8]_rep_0\ : STD_LOGIC;
  signal Instr_out_OBUF : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal SrcB : STD_LOGIC_VECTOR ( 27 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[0]_inst_i_3\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[0]_inst_i_4\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[0]_inst_i_5\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[0]_inst_i_8\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[11]_inst_i_7\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[12]_inst_i_4\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[13]_inst_i_4\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[13]_inst_i_9\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[16]_inst_i_4\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[1]_inst_i_7\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[1]_inst_i_8\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[25]_inst_i_7\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[25]_inst_i_8\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[27]_inst_i_5\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[27]_inst_i_8\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[28]_inst_i_10\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[28]_inst_i_7\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[29]_inst_i_11\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[29]_inst_i_13\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[29]_inst_i_4\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[29]_inst_i_6\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[2]_inst_i_4\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[30]_inst_i_13\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[30]_inst_i_17\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[30]_inst_i_4\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[30]_inst_i_5\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[30]_inst_i_9\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[31]_inst_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \Dout[0]_i_1__0\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \Dout[10]_i_1__0\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \Dout[14]_i_1__0\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \Dout[15]_i_1__0\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \Dout[16]_i_1__0\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \Dout[17]_i_1__0\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \Dout[18]_i_1__0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \Dout[1]_i_1__0\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \Dout[1]_i_3\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \Dout[20]_i_1__0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \Dout[21]_i_1__0\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \Dout[22]_i_1__0\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \Dout[23]_i_1__0\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \Dout[24]_i_1__0\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \Dout[2]_i_1__0\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \Dout[31]_i_1__0\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \Dout[3]_i_1__0\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \Dout[3]_i_2\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \Dout[4]_i_1__0\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \Dout[5]_i_1__0\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \Dout[6]_i_1__0\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \Dout[7]_i_1__0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \Dout[8]_i_1__0\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \Dout[9]_i_1__0\ : label is "soft_lutpair68";
  attribute IOB : string;
  attribute IOB of \Dout_reg[0]\ : label is "TRUE";
  attribute ORIG_CELL_NAME : string;
  attribute ORIG_CELL_NAME of \Dout_reg[0]\ : label is "Dout_reg[0]";
  attribute ORIG_CELL_NAME of \Dout_reg[0]_rep\ : label is "Dout_reg[0]";
  attribute IOB of \Dout_reg[12]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[12]\ : label is "Dout_reg[12]";
  attribute ORIG_CELL_NAME of \Dout_reg[12]_rep\ : label is "Dout_reg[12]";
  attribute IOB of \Dout_reg[13]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[13]\ : label is "Dout_reg[13]";
  attribute ORIG_CELL_NAME of \Dout_reg[13]_rep\ : label is "Dout_reg[13]";
  attribute IOB of \Dout_reg[14]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[14]\ : label is "Dout_reg[14]";
  attribute ORIG_CELL_NAME of \Dout_reg[14]_rep\ : label is "Dout_reg[14]";
  attribute IOB of \Dout_reg[15]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[15]\ : label is "Dout_reg[15]";
  attribute ORIG_CELL_NAME of \Dout_reg[15]_rep\ : label is "Dout_reg[15]";
  attribute IOB of \Dout_reg[16]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[16]\ : label is "Dout_reg[16]";
  attribute ORIG_CELL_NAME of \Dout_reg[16]_rep\ : label is "Dout_reg[16]";
  attribute IOB of \Dout_reg[18]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[18]\ : label is "Dout_reg[18]";
  attribute ORIG_CELL_NAME of \Dout_reg[18]_rep\ : label is "Dout_reg[18]";
  attribute IOB of \Dout_reg[19]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[19]\ : label is "Dout_reg[19]";
  attribute IOB of \Dout_reg[19]_rep\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[19]_rep\ : label is "Dout_reg[19]";
  attribute ORIG_CELL_NAME of \Dout_reg[19]_rep_rep\ : label is "Dout_reg[19]";
  attribute IOB of \Dout_reg[1]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[1]\ : label is "Dout_reg[1]";
  attribute ORIG_CELL_NAME of \Dout_reg[1]_rep\ : label is "Dout_reg[1]";
  attribute IOB of \Dout_reg[20]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[20]\ : label is "Dout_reg[20]";
  attribute ORIG_CELL_NAME of \Dout_reg[20]_rep\ : label is "Dout_reg[20]";
  attribute IOB of \Dout_reg[21]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[21]\ : label is "Dout_reg[21]";
  attribute ORIG_CELL_NAME of \Dout_reg[21]_rep\ : label is "Dout_reg[21]";
  attribute IOB of \Dout_reg[22]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[22]\ : label is "Dout_reg[22]";
  attribute ORIG_CELL_NAME of \Dout_reg[22]_rep\ : label is "Dout_reg[22]";
  attribute IOB of \Dout_reg[23]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[23]\ : label is "Dout_reg[23]";
  attribute ORIG_CELL_NAME of \Dout_reg[23]_rep\ : label is "Dout_reg[23]";
  attribute IOB of \Dout_reg[24]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[24]\ : label is "Dout_reg[24]";
  attribute ORIG_CELL_NAME of \Dout_reg[24]_rep\ : label is "Dout_reg[24]";
  attribute IOB of \Dout_reg[25]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[25]\ : label is "Dout_reg[25]";
  attribute ORIG_CELL_NAME of \Dout_reg[25]_rep\ : label is "Dout_reg[25]";
  attribute IOB of \Dout_reg[26]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[26]\ : label is "Dout_reg[26]";
  attribute ORIG_CELL_NAME of \Dout_reg[26]_rep\ : label is "Dout_reg[26]";
  attribute IOB of \Dout_reg[27]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[27]\ : label is "Dout_reg[27]";
  attribute ORIG_CELL_NAME of \Dout_reg[27]_rep\ : label is "Dout_reg[27]";
  attribute ORIG_CELL_NAME of \Dout_reg[27]_rep__0\ : label is "Dout_reg[27]";
  attribute IOB of \Dout_reg[28]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[28]\ : label is "Dout_reg[28]";
  attribute ORIG_CELL_NAME of \Dout_reg[28]_rep\ : label is "Dout_reg[28]";
  attribute IOB of \Dout_reg[2]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[2]\ : label is "Dout_reg[2]";
  attribute ORIG_CELL_NAME of \Dout_reg[2]_rep\ : label is "Dout_reg[2]";
  attribute IOB of \Dout_reg[30]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[30]\ : label is "Dout_reg[30]";
  attribute ORIG_CELL_NAME of \Dout_reg[30]_rep\ : label is "Dout_reg[30]";
  attribute IOB of \Dout_reg[31]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[31]\ : label is "Dout_reg[31]";
  attribute IOB of \Dout_reg[31]_rep\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[31]_rep\ : label is "Dout_reg[31]";
  attribute ORIG_CELL_NAME of \Dout_reg[31]_rep_rep\ : label is "Dout_reg[31]";
  attribute IOB of \Dout_reg[3]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[3]\ : label is "Dout_reg[3]";
  attribute ORIG_CELL_NAME of \Dout_reg[3]_rep\ : label is "Dout_reg[3]";
  attribute IOB of \Dout_reg[4]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[4]\ : label is "Dout_reg[4]";
  attribute ORIG_CELL_NAME of \Dout_reg[4]_rep\ : label is "Dout_reg[4]";
  attribute IOB of \Dout_reg[5]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[5]\ : label is "Dout_reg[5]";
  attribute ORIG_CELL_NAME of \Dout_reg[5]_rep\ : label is "Dout_reg[5]";
  attribute IOB of \Dout_reg[6]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[6]\ : label is "Dout_reg[6]";
  attribute ORIG_CELL_NAME of \Dout_reg[6]_rep\ : label is "Dout_reg[6]";
  attribute IOB of \Dout_reg[7]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[7]\ : label is "Dout_reg[7]";
  attribute ORIG_CELL_NAME of \Dout_reg[7]_rep\ : label is "Dout_reg[7]";
  attribute IOB of \Dout_reg[8]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[8]\ : label is "Dout_reg[8]";
  attribute ORIG_CELL_NAME of \Dout_reg[8]_rep\ : label is "Dout_reg[8]";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[10]_i_2\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[12]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[13]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[3]_i_1\ : label is "soft_lutpair63";
begin
  D(31 downto 0) <= \^d\(31 downto 0);
  \Dout_reg[15]_rep_1\(3 downto 0) <= \^dout_reg[15]_rep_1\(3 downto 0);
  \Dout_reg[20]_rep_0\(0) <= \^dout_reg[20]_rep_0\(0);
  \Dout_reg[21]_rep_0\(2 downto 0) <= \^dout_reg[21]_rep_0\(2 downto 0);
  \Dout_reg[23]_rep_0\ <= \^dout_reg[23]_rep_0\;
  \Dout_reg[23]_rep_1\ <= \^dout_reg[23]_rep_1\;
  \Dout_reg[25]_rep_0\ <= \^dout_reg[25]_rep_0\;
  \Dout_reg[25]_rep_1\ <= \^dout_reg[25]_rep_1\;
  \Dout_reg[25]_rep_2\ <= \^dout_reg[25]_rep_2\;
  \Dout_reg[25]_rep_3\ <= \^dout_reg[25]_rep_3\;
  \Dout_reg[26]_rep_0\ <= \^dout_reg[26]_rep_0\;
  \Dout_reg[26]_rep_1\ <= \^dout_reg[26]_rep_1\;
  \Dout_reg[26]_rep_2\ <= \^dout_reg[26]_rep_2\;
  \Dout_reg[27]_rep__0_0\ <= \^dout_reg[27]_rep__0_0\;
  \Dout_reg[8]_rep_0\ <= \^dout_reg[8]_rep_0\;
\ALUControl_out_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"005C"
    )
        port map (
      I0 => Instr_out_OBUF(23),
      I1 => \ALUControl_out_OBUF[0]_inst_i_2_n_0\,
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      O => \^dout_reg[23]_rep_0\
    );
\ALUControl_out_OBUF[0]_inst_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFEFAAAA"
    )
        port map (
      I0 => Instr_out_OBUF(22),
      I1 => Instr_out_OBUF(5),
      I2 => \^dout_reg[21]_rep_0\(2),
      I3 => Instr_out_OBUF(25),
      I4 => Instr_out_OBUF(24),
      O => \ALUControl_out_OBUF[0]_inst_i_2_n_0\
    );
\ALUControl_out_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \ALUControl_out_OBUF[1]_inst_i_2_n_0\,
      I1 => \^dout_reg[26]_rep_1\,
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(22),
      O => \^dout_reg[26]_rep_0\
    );
\ALUControl_out_OBUF[1]_inst_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4FF04FFF"
    )
        port map (
      I0 => Instr_out_OBUF(25),
      I1 => Instr_out_OBUF(6),
      I2 => \^dout_reg[21]_rep_0\(2),
      I3 => Instr_out_OBUF(24),
      I4 => Instr_out_OBUF(23),
      O => \ALUControl_out_OBUF[1]_inst_i_2_n_0\
    );
\ALUControl_out_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000FB00"
    )
        port map (
      I0 => Instr_out_OBUF(25),
      I1 => Instr_out_OBUF(24),
      I2 => Instr_out_OBUF(22),
      I3 => \^dout_reg[21]_rep_0\(2),
      I4 => \^dout_reg[26]_rep_1\,
      I5 => \Dout_reg[27]_rep_n_0\,
      O => \^dout_reg[25]_rep_1\
    );
\ALUControl_out_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000400"
    )
        port map (
      I0 => Instr_out_OBUF(25),
      I1 => Instr_out_OBUF(24),
      I2 => Instr_out_OBUF(22),
      I3 => \^dout_reg[21]_rep_0\(2),
      I4 => \^dout_reg[26]_rep_1\,
      I5 => \Dout_reg[27]_rep_n_0\,
      O => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFE0EFEFEFE0EFE0"
    )
        port map (
      I0 => \ALUResult_out_OBUF[0]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[0]_inst_i_3_n_0\,
      I2 => \^dout_reg[25]_rep_0\,
      I3 => \ALUResult_out_OBUF[0]_inst_i_4_n_0\,
      I4 => \ALUResult_out_OBUF[0]_inst_i_5_n_0\,
      I5 => \ALUResult_out_OBUF[0]_inst_i_6_n_0\,
      O => \^d\(0)
    );
\ALUResult_out_OBUF[0]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888B888B8880000"
    )
        port map (
      I0 => \Dout_reg[0]_0\,
      I1 => \^dout_reg[21]_rep_0\(0),
      I2 => \^dout_reg[21]_rep_0\(1),
      I3 => SrcB(2),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[0]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[0]_inst_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000E2"
    )
        port map (
      I0 => \Dout_reg[28]_1\(0),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_3_0\(0),
      I3 => \^dout_reg[21]_rep_0\(0),
      I4 => \^dout_reg[21]_rep_0\(1),
      O => \ALUResult_out_OBUF[0]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[0]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88822282"
    )
        port map (
      I0 => \^dout_reg[25]_rep_1\,
      I1 => \ALUResult_out_OBUF[0]_inst_i_8_n_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \^dout_reg[26]_rep_0\,
      I4 => Q(0),
      O => \ALUResult_out_OBUF[0]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[0]_inst_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEAAEFAA"
    )
        port map (
      I0 => \^dout_reg[25]_rep_1\,
      I1 => \ALUResult_out_OBUF[0]_inst_i_8_n_0\,
      I2 => Q(0),
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \^dout_reg[23]_rep_0\,
      O => \ALUResult_out_OBUF[0]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[0]_inst_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEAE"
    )
        port map (
      I0 => \^dout_reg[26]_rep_0\,
      I1 => \Dout_reg[30]_0\(0),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \Dout_reg[30]_1\(0),
      O => \ALUResult_out_OBUF[0]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[0]_inst_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(0),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(0),
      O => \ALUResult_out_OBUF[0]_inst_i_8_n_0\
    );
\ALUResult_out_OBUF[10]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[10]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[10]_inst_i_3_n_0\,
      O => \^d\(10),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[10]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3C5A3C5AE800E8FF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => Q(10),
      I2 => SrcB(10),
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \Dout_reg[10]\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[10]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[10]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F5030503F5F305F3"
    )
        port map (
      I0 => \ALUResult_out_OBUF[11]_inst_i_6_n_0\,
      I1 => \Dout_reg[10]_0\,
      I2 => \^dout_reg[23]_rep_1\,
      I3 => \^dout_reg[21]_rep_0\(0),
      I4 => \Dout_reg[10]_1\,
      I5 => \Dout_reg[10]_2\,
      O => \ALUResult_out_OBUF[10]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[10]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(10),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(10),
      O => SrcB(10)
    );
\ALUResult_out_OBUF[11]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88BB8B8B"
    )
        port map (
      I0 => \ALUResult_out_OBUF[11]_inst_i_2_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[11]_inst_i_3_n_0\,
      I3 => \ALUResult_out_OBUF[11]_inst_i_4_n_0\,
      I4 => \^dout_reg[25]_rep_1\,
      O => \^d\(11)
    );
\ALUResult_out_OBUF[11]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FD010101FD01FDFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[11]_inst_i_5_n_0\,
      I1 => \^dout_reg[26]_rep_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \Dout_reg[11]_1\,
      I4 => \^dout_reg[21]_rep_0\(0),
      I5 => \ALUResult_out_OBUF[11]_inst_i_6_n_0\,
      O => \ALUResult_out_OBUF[11]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[11]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"101F7070101F7F7F"
    )
        port map (
      I0 => Q(11),
      I1 => \ALUResult_out_OBUF[11]_inst_i_7_n_0\,
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(5),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(5),
      O => \ALUResult_out_OBUF[11]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[11]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88877787"
    )
        port map (
      I0 => \^dout_reg[25]_rep_3\,
      I1 => \ALUResult_out_OBUF[29]_inst_i_3_0\(11),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \^dout_reg[26]_rep_0\,
      I4 => Q(11),
      O => \ALUResult_out_OBUF[11]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[11]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0AF3030A0AF3F3F"
    )
        port map (
      I0 => \ALU/p_0_out\(8),
      I1 => SrcB(10),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => SrcB(9),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => \ALUResult_out_OBUF[11]_inst_i_7_n_0\,
      O => \ALUResult_out_OBUF[11]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[11]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF4FFFFFFF7"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(13),
      I1 => \^dout_reg[21]_rep_0\(1),
      I2 => Instr_out_OBUF(25),
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => \^dout_reg[26]_rep_1\,
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(11),
      O => \ALUResult_out_OBUF[11]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[11]_inst_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(11),
      I1 => \^dout_reg[26]_rep_1\,
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(25),
      O => \ALUResult_out_OBUF[11]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[12]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[12]_inst_i_2_n_0\,
      I1 => \Dout_reg[12]_0\,
      O => \^d\(12),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[12]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3C5A3C5AE800E8FF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => Q(12),
      I2 => \ALUResult_out_OBUF[12]_inst_i_4_n_0\,
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \Dout_reg[12]_1\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[12]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[12]_inst_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(12),
      I1 => \^dout_reg[26]_rep_1\,
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(25),
      O => \ALUResult_out_OBUF[12]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[13]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E0EFE0EFE0E0EFEF"
    )
        port map (
      I0 => \ALUResult_out_OBUF[13]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[13]_inst_i_3_n_0\,
      I2 => \^dout_reg[25]_rep_0\,
      I3 => \ALUResult_out_OBUF[13]_inst_i_4_n_0\,
      I4 => \ALUResult_out_OBUF[13]_inst_i_5_n_0\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \^d\(13)
    );
\ALUResult_out_OBUF[13]_inst_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D1D1D100"
    )
        port map (
      I0 => \Dout_reg[13]_1\,
      I1 => \^dout_reg[21]_rep_0\(0),
      I2 => \Dout_reg[13]_2\,
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[13]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[13]_inst_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000074"
    )
        port map (
      I0 => \Dout_reg[13]_0\,
      I1 => \^dout_reg[21]_rep_0\(0),
      I2 => \ALUResult_out_OBUF[14]_inst_i_7_n_0\,
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[13]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[13]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88877787"
    )
        port map (
      I0 => \^dout_reg[25]_rep_3\,
      I1 => \ALUResult_out_OBUF[29]_inst_i_3_0\(13),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \^dout_reg[26]_rep_0\,
      I4 => Q(13),
      O => \ALUResult_out_OBUF[13]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[13]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"101F7070101F7F7F"
    )
        port map (
      I0 => Q(13),
      I1 => \ALUResult_out_OBUF[13]_inst_i_9_n_0\,
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(6),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(6),
      O => \ALUResult_out_OBUF[13]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[13]_inst_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(13),
      I1 => \^dout_reg[26]_rep_1\,
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(25),
      O => \ALUResult_out_OBUF[13]_inst_i_9_n_0\
    );
\ALUResult_out_OBUF[14]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[14]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[14]_inst_i_3_n_0\,
      O => \^d\(14),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[14]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FA5500FAE44EE44E"
    )
        port map (
      I0 => \^dout_reg[25]_rep_1\,
      I1 => \Dout_reg[14]_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \ALU/p_0_out\(14),
      I4 => Q(14),
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[14]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[14]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CDC8CDCDCDC8C8C8"
    )
        port map (
      I0 => \^dout_reg[26]_rep_0\,
      I1 => \ALUResult_out_OBUF[14]_inst_i_6_n_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \ALUResult_out_OBUF[14]_inst_i_7_n_0\,
      I4 => \^dout_reg[21]_rep_0\(0),
      I5 => \Dout_reg[15]_3\,
      O => \ALUResult_out_OBUF[14]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[14]_inst_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(14),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(11),
      O => \ALU/p_0_out\(14)
    );
\ALUResult_out_OBUF[14]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505FC0C0505FCFCF"
    )
        port map (
      I0 => \ALU/p_0_out\(17),
      I1 => SrcB(15),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \ALU/p_0_out\(16),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => \ALU/p_0_out\(14),
      O => \ALUResult_out_OBUF[14]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[14]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000B8"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(11),
      I1 => \^dout_reg[21]_rep_0\(1),
      I2 => \ALUResult_out_OBUF[29]_inst_i_3_0\(13),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[27]_rep_n_0\,
      I5 => Instr_out_OBUF(25),
      O => \ALUResult_out_OBUF[14]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[15]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[15]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[15]_inst_i_3_n_0\,
      O => \^d\(15),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[15]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55FAFA004E4EE4E4"
    )
        port map (
      I0 => \^dout_reg[25]_rep_1\,
      I1 => \Dout_reg[15]_2\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => Q(15),
      I4 => SrcB(15),
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[15]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[15]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CDC8CDCDCDC8C8C8"
    )
        port map (
      I0 => \^dout_reg[26]_rep_0\,
      I1 => \ALUResult_out_OBUF[15]_inst_i_6_n_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \Dout_reg[15]_3\,
      I4 => \^dout_reg[21]_rep_0\(0),
      I5 => \Dout_reg[15]_4\,
      O => \ALUResult_out_OBUF[15]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[15]_inst_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(15),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(12),
      O => SrcB(15)
    );
\ALUResult_out_OBUF[15]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3F3F505F3030"
    )
        port map (
      I0 => \ALU/p_0_out\(18),
      I1 => \ALU/p_0_out\(16),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \ALU/p_0_out\(17),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => SrcB(15),
      O => \ALUResult_out_OBUF[15]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[16]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[16]_inst_i_2_n_0\,
      I1 => \Dout_reg[16]_0\,
      O => \^d\(16),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[16]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C399C399B200B2FF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => \ALU/p_0_out\(16),
      I2 => Q(16),
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \Dout_reg[16]_1\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[16]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[16]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(16),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(13),
      O => \ALU/p_0_out\(16)
    );
\ALUResult_out_OBUF[17]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E0EFE0EFE0E0EFEF"
    )
        port map (
      I0 => \ALUResult_out_OBUF[17]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[17]_inst_i_3_n_0\,
      I2 => \^dout_reg[25]_rep_0\,
      I3 => \ALUResult_out_OBUF[17]_inst_i_4_n_0\,
      I4 => \ALUResult_out_OBUF[17]_inst_i_5_n_0\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \^d\(17)
    );
\ALUResult_out_OBUF[17]_inst_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D1D1D100"
    )
        port map (
      I0 => \Dout_reg[17]_1\,
      I1 => \^dout_reg[21]_rep_0\(0),
      I2 => \Dout_reg[17]_2\,
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[17]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[17]_inst_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000002E"
    )
        port map (
      I0 => \Dout_reg[17]\,
      I1 => \^dout_reg[21]_rep_0\(0),
      I2 => \Dout_reg[17]_0\,
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[17]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[17]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E2E2E21D1D1DE21D"
    )
        port map (
      I0 => \Dout_reg[28]_1\(14),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_3_0\(17),
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => Q(17),
      O => \ALUResult_out_OBUF[17]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[17]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"202FB0B0202FBFBF"
    )
        port map (
      I0 => \ALU/p_0_out\(17),
      I1 => Q(17),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(7),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(7),
      O => \ALUResult_out_OBUF[17]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[17]_inst_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(17),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(14),
      O => \ALU/p_0_out\(17)
    );
\ALUResult_out_OBUF[18]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \ALUResult_out_OBUF[18]_inst_i_2_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[18]_inst_i_3_n_0\,
      I3 => \^dout_reg[25]_rep_1\,
      I4 => \ALUResult_out_OBUF[18]_inst_i_4_n_0\,
      O => \^d\(18)
    );
\ALUResult_out_OBUF[18]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CDC8CDCDCDC8C8C8"
    )
        port map (
      I0 => \^dout_reg[26]_rep_0\,
      I1 => \ALUResult_out_OBUF[18]_inst_i_5_n_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \Dout_reg[17]\,
      I4 => \^dout_reg[21]_rep_0\(0),
      I5 => \Dout_reg[19]_2\,
      O => \ALUResult_out_OBUF[18]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[18]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1D1D1DE2E2E21DE2"
    )
        port map (
      I0 => \Dout_reg[28]_1\(15),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_3_0\(18),
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => Q(18),
      O => \ALUResult_out_OBUF[18]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[18]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FF00F0ACACACAC"
    )
        port map (
      I0 => \Dout_reg[30]_1\(8),
      I1 => \Dout_reg[30]_0\(8),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \ALU/p_0_out\(18),
      I4 => Q(18),
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[18]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[18]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => \ALU/p_0_out\(21),
      I1 => \ALU/p_0_out\(19),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \ALU/p_0_out\(20),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => \ALU/p_0_out\(18),
      O => \ALUResult_out_OBUF[18]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[18]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(18),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(15),
      O => \ALU/p_0_out\(18)
    );
\ALUResult_out_OBUF[19]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8B8B88BB"
    )
        port map (
      I0 => \ALUResult_out_OBUF[19]_inst_i_2_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[19]_inst_i_3_n_0\,
      I3 => \ALUResult_out_OBUF[19]_inst_i_4_n_0\,
      I4 => \^dout_reg[25]_rep_1\,
      O => \^d\(19)
    );
\ALUResult_out_OBUF[19]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFD1000000D1"
    )
        port map (
      I0 => \Dout_reg[19]_1\,
      I1 => \^dout_reg[21]_rep_0\(0),
      I2 => \Dout_reg[19]_2\,
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => \ALUResult_out_OBUF[19]_inst_i_6_n_0\,
      O => \ALUResult_out_OBUF[19]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[19]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8474747B847"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(19),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \Dout_reg[28]_1\(17),
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => Q(19),
      O => \ALUResult_out_OBUF[19]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[19]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"202FB0B0202FBFBF"
    )
        port map (
      I0 => \ALU/p_0_out\(19),
      I1 => Q(19),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(9),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(9),
      O => \ALUResult_out_OBUF[19]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[19]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F000FFF55335533"
    )
        port map (
      I0 => \ALU/p_0_out\(21),
      I1 => \ALU/p_0_out\(19),
      I2 => \ALU/p_0_out\(22),
      I3 => \^dout_reg[21]_rep_0\(1),
      I4 => \ALU/p_0_out\(20),
      I5 => \^dout_reg[21]_rep_0\(0),
      O => \ALUResult_out_OBUF[19]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[19]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55545557"
    )
        port map (
      I0 => \Dout_reg[28]_1\(17),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_3_0\(19),
      O => \ALU/p_0_out\(19)
    );
\ALUResult_out_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \ALUResult_out_OBUF[1]_inst_i_2_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[1]_inst_i_3_n_0\,
      I3 => \^dout_reg[25]_rep_1\,
      I4 => \ALUResult_out_OBUF[1]_inst_i_4_n_0\,
      O => \^d\(1)
    );
\ALUResult_out_OBUF[1]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CDC8CDCDCDC8C8C8"
    )
        port map (
      I0 => \^dout_reg[26]_rep_0\,
      I1 => \ALUResult_out_OBUF[1]_inst_i_5_n_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \ALUResult_out_OBUF[1]_inst_i_6_n_0\,
      I4 => \^dout_reg[21]_rep_0\(0),
      I5 => \^dout_reg[8]_rep_0\,
      O => \ALUResult_out_OBUF[1]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[1]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1D1D1DE2E2E21DE2"
    )
        port map (
      I0 => \Dout_reg[28]_1\(1),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_3_0\(1),
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => Q(1),
      O => \ALUResult_out_OBUF[1]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[1]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFD04F4FDFD04040"
    )
        port map (
      I0 => \ALU/p_0_out\(1),
      I1 => Q(1),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(1),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(1),
      O => \ALUResult_out_OBUF[1]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[1]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => SrcB(4),
      I1 => SrcB(2),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => SrcB(3),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => SrcB(1),
      O => \ALUResult_out_OBUF[1]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[1]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444544444440"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(1),
      I1 => \Dout_reg[28]_1\(0),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(0),
      O => \ALUResult_out_OBUF[1]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[1]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(1),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(1),
      O => \ALU/p_0_out\(1)
    );
\ALUResult_out_OBUF[1]_inst_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(1),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(1),
      O => SrcB(1)
    );
\ALUResult_out_OBUF[20]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[20]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[20]_inst_i_3_n_0\,
      O => \^d\(20),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[20]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFC0303FC505F5C5"
    )
        port map (
      I0 => \Dout_reg[20]_0\,
      I1 => Q(20),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \ALU/p_0_out\(20),
      I5 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[20]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[20]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF1015BABF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => \Dout_reg[19]_1\,
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \Dout_reg[20]_1\,
      I4 => \ALUResult_out_OBUF[20]_inst_i_7_n_0\,
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[20]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[20]_inst_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(20),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(16),
      O => \ALU/p_0_out\(20)
    );
\ALUResult_out_OBUF[20]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \ALU/p_0_out\(23),
      I1 => \ALU/p_0_out\(21),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \ALU/p_0_out\(22),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => \ALU/p_0_out\(20),
      O => \ALUResult_out_OBUF[20]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[21]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[21]_inst_i_2_n_0\,
      I1 => \Dout_reg[21]_1\,
      O => \^d\(21),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[21]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C399C399B200B2FF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => \ALU/p_0_out\(21),
      I2 => Q(21),
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \Dout_reg[21]_2\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[21]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[21]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55545557"
    )
        port map (
      I0 => \Dout_reg[28]_1\(17),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_3_0\(21),
      O => \ALU/p_0_out\(21)
    );
\ALUResult_out_OBUF[22]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E0EFE0EFE0E0EFEF"
    )
        port map (
      I0 => \ALUResult_out_OBUF[22]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[22]_inst_i_3_n_0\,
      I2 => \^dout_reg[25]_rep_0\,
      I3 => \ALUResult_out_OBUF[22]_inst_i_4_n_0\,
      I4 => \ALUResult_out_OBUF[22]_inst_i_5_n_0\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \^d\(22)
    );
\ALUResult_out_OBUF[22]_inst_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000002E"
    )
        port map (
      I0 => \Dout_reg[22]_2\,
      I1 => \^dout_reg[21]_rep_0\(0),
      I2 => \Dout_reg[22]_3\,
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[22]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[22]_inst_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D1D1D100"
    )
        port map (
      I0 => \Dout_reg[22]_0\,
      I1 => \^dout_reg[21]_rep_0\(0),
      I2 => \Dout_reg[22]_1\,
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[22]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[22]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E2E2E21D1D1DE21D"
    )
        port map (
      I0 => \Dout_reg[28]_1\(18),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_3_0\(22),
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => Q(22),
      O => \ALUResult_out_OBUF[22]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[22]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"202FB0B0202FBFBF"
    )
        port map (
      I0 => \ALU/p_0_out\(22),
      I1 => Q(22),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(10),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(10),
      O => \ALUResult_out_OBUF[22]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[22]_inst_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(22),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(18),
      O => \ALU/p_0_out\(22)
    );
\ALUResult_out_OBUF[23]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[23]_inst_i_2_n_0\,
      I1 => \Dout_reg[23]_2\,
      O => \^d\(23),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[23]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FA5500FAE44EE44E"
    )
        port map (
      I0 => \^dout_reg[25]_rep_1\,
      I1 => \Dout_reg[23]_3\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \ALU/p_0_out\(23),
      I4 => Q(23),
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[23]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[23]_inst_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(23),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(19),
      O => \ALU/p_0_out\(23)
    );
\ALUResult_out_OBUF[24]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88BB8B8B"
    )
        port map (
      I0 => \ALUResult_out_OBUF[24]_inst_i_2_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[24]_inst_i_3_n_0\,
      I3 => \ALUResult_out_OBUF[24]_inst_i_4_n_0\,
      I4 => \^dout_reg[25]_rep_1\,
      O => \^d\(24)
    );
\ALUResult_out_OBUF[24]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0202FEFEFE02FE02"
    )
        port map (
      I0 => \ALUResult_out_OBUF[24]_inst_i_5_n_0\,
      I1 => \^dout_reg[23]_rep_0\,
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[24]_0\,
      I4 => \Dout_reg[24]_1\,
      I5 => \^dout_reg[21]_rep_0\(0),
      O => \ALUResult_out_OBUF[24]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[24]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"202FB0B0202FBFBF"
    )
        port map (
      I0 => \ALU/p_0_out\(24),
      I1 => Q(24),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(11),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(11),
      O => \ALUResult_out_OBUF[24]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[24]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8474747B847"
    )
        port map (
      I0 => Q(24),
      I1 => \^dout_reg[26]_rep_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \Dout_reg[28]_1\(20),
      I4 => \^dout_reg[25]_rep_3\,
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(24),
      O => \ALUResult_out_OBUF[24]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[24]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"05F5030305F5F3F3"
    )
        port map (
      I0 => \ALU/p_0_out\(22),
      I1 => \ALU/p_0_out\(24),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \ALU/p_0_out\(21),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => \ALU/p_0_out\(23),
      O => \ALUResult_out_OBUF[24]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[24]_inst_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(24),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(20),
      O => \ALU/p_0_out\(24)
    );
\ALUResult_out_OBUF[25]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88BB8B8B"
    )
        port map (
      I0 => \ALUResult_out_OBUF[25]_inst_i_2_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[25]_inst_i_3_n_0\,
      I3 => \ALUResult_out_OBUF[25]_inst_i_4_n_0\,
      I4 => \^dout_reg[25]_rep_1\,
      O => \^d\(25)
    );
\ALUResult_out_OBUF[25]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF1015BABF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => \Dout_reg[25]_0\,
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \Dout_reg[26]_0\,
      I4 => \ALUResult_out_OBUF[25]_inst_i_6_n_0\,
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[25]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[25]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"101F7070101F7F7F"
    )
        port map (
      I0 => Q(25),
      I1 => SrcB(25),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(12),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(12),
      O => \ALUResult_out_OBUF[25]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[25]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8474747B847"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(25),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \Dout_reg[28]_1\(21),
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => Q(25),
      O => \ALUResult_out_OBUF[25]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[25]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFC05F5FCFC05050"
    )
        port map (
      I0 => SrcB(26),
      I1 => \ALU/p_0_out\(28),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \ALU/p_0_out\(27),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => \ALU/p_0_out\(25),
      O => \ALUResult_out_OBUF[25]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[25]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(25),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(21),
      O => SrcB(25)
    );
\ALUResult_out_OBUF[25]_inst_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55545557"
    )
        port map (
      I0 => \Dout_reg[28]_1\(21),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_3_0\(25),
      O => \ALU/p_0_out\(25)
    );
\ALUResult_out_OBUF[26]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88BB8B8B"
    )
        port map (
      I0 => \ALUResult_out_OBUF[26]_inst_i_2_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[26]_inst_i_3_n_0\,
      I3 => \ALUResult_out_OBUF[26]_inst_i_4_n_0\,
      I4 => \^dout_reg[25]_rep_1\,
      O => \^d\(26)
    );
\ALUResult_out_OBUF[26]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555555555555303F"
    )
        port map (
      I0 => \ALUResult_out_OBUF[26]_inst_i_5_n_0\,
      I1 => \Dout_reg[26]_0\,
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \Dout_reg[26]_1\,
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[26]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[26]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1010707F1F1F707F"
    )
        port map (
      I0 => Q(26),
      I1 => SrcB(26),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_0\(13),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_1\(13),
      O => \ALUResult_out_OBUF[26]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[26]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B847B8B8B8474747"
    )
        port map (
      I0 => Q(26),
      I1 => \^dout_reg[26]_rep_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \ALUResult_out_OBUF[29]_inst_i_3_0\(26),
      I4 => \^dout_reg[25]_rep_3\,
      I5 => \Dout_reg[28]_1\(21),
      O => \ALUResult_out_OBUF[26]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[26]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFAFA0A0303F303F"
    )
        port map (
      I0 => \ALU/p_0_out\(29),
      I1 => SrcB(27),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => SrcB(26),
      I4 => \ALU/p_0_out\(28),
      I5 => \^dout_reg[21]_rep_0\(1),
      O => \ALUResult_out_OBUF[26]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[26]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(26),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(21),
      O => SrcB(26)
    );
\ALUResult_out_OBUF[27]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[27]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[27]_inst_i_3_n_0\,
      O => \^d\(27),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[27]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC333C355555555"
    )
        port map (
      I0 => \ALUResult_out_OBUF[27]_inst_i_4_n_0\,
      I1 => \ALU/p_0_out\(27),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \^dout_reg[26]_rep_0\,
      I4 => Q(27),
      I5 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[27]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[27]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF1015BABF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => \Dout_reg[26]_1\,
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \Dout_reg[27]_2\,
      I4 => \ALUResult_out_OBUF[27]_inst_i_7_n_0\,
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[27]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[27]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"101F7070101F7F7F"
    )
        port map (
      I0 => Q(27),
      I1 => SrcB(27),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(14),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(14),
      O => \ALUResult_out_OBUF[27]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[27]_inst_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55545557"
    )
        port map (
      I0 => \Dout_reg[28]_1\(21),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_3_0\(27),
      O => \ALU/p_0_out\(27)
    );
\ALUResult_out_OBUF[27]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0C0C0AFA0CFCF"
    )
        port map (
      I0 => \ALU/p_0_out\(30),
      I1 => \ALU/p_0_out\(28),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \ALU/p_0_out\(29),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => SrcB(27),
      O => \ALUResult_out_OBUF[27]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[27]_inst_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(27),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(21),
      O => SrcB(27)
    );
\ALUResult_out_OBUF[28]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"74777444"
    )
        port map (
      I0 => \Dout_reg[28]_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[28]_inst_i_3_n_0\,
      I3 => \^dout_reg[25]_rep_1\,
      I4 => \ALUResult_out_OBUF[28]_inst_i_4_n_0\,
      O => \^d\(28)
    );
\ALUResult_out_OBUF[28]_inst_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => Instr_out_OBUF(25),
      I1 => \Dout_reg[27]_rep_n_0\,
      I2 => \^dout_reg[26]_rep_1\,
      O => \^dout_reg[25]_rep_3\
    );
\ALUResult_out_OBUF[28]_inst_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(28),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(21),
      O => \ALU/p_0_out\(28)
    );
\ALUResult_out_OBUF[28]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1D1D1DE2E2E21DE2"
    )
        port map (
      I0 => \Dout_reg[28]_1\(21),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_3_0\(28),
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => Q(28),
      O => \ALUResult_out_OBUF[28]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[28]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFD04F4FDFD04040"
    )
        port map (
      I0 => \ALU/p_0_out\(28),
      I1 => Q(28),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(15),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(15),
      O => \ALUResult_out_OBUF[28]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[28]_inst_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => \^dout_reg[26]_rep_0\,
      O => \^dout_reg[23]_rep_1\
    );
\ALUResult_out_OBUF[29]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E0EEE0E0EEEEEEEE"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[29]_inst_i_3_n_0\,
      I2 => \^dout_reg[25]_rep_0\,
      I3 => \ALUResult_out_OBUF[29]_inst_i_4_n_0\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_5_n_0\,
      I5 => \ALUResult_out_OBUF[29]_inst_i_6_n_0\,
      O => \^d\(29)
    );
\ALUResult_out_OBUF[29]_inst_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020202A2A2A202A2"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(0),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[25]_rep_3\,
      I3 => \ALUResult_out_OBUF[29]_inst_i_3_0\(28),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(26),
      O => \ALUResult_out_OBUF[29]_inst_i_10_n_0\
    );
\ALUResult_out_OBUF[29]_inst_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(1),
      I1 => \^dout_reg[21]_rep_0\(0),
      O => \ALUResult_out_OBUF[29]_inst_i_11_n_0\
    );
\ALUResult_out_OBUF[29]_inst_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(29),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(21),
      O => \ALU/p_0_out\(29)
    );
\ALUResult_out_OBUF[29]_inst_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(0),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(0),
      O => SrcB(0)
    );
\ALUResult_out_OBUF[29]_inst_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F7777733"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_7_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_8_n_0\,
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \^dout_reg[23]_rep_0\,
      O => \ALUResult_out_OBUF[29]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[29]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000400045504"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => \ALUResult_out_OBUF[29]_inst_i_9_n_0\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_10_n_0\,
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_11_n_0\,
      I5 => \ALUResult_out_OBUF[30]_inst_i_12_n_0\,
      O => \ALUResult_out_OBUF[29]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[29]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEAAEFAA"
    )
        port map (
      I0 => \^dout_reg[25]_rep_1\,
      I1 => \ALU/p_0_out\(29),
      I2 => Q(29),
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \^dout_reg[23]_rep_0\,
      O => \ALUResult_out_OBUF[29]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[29]_inst_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEAE"
    )
        port map (
      I0 => \^dout_reg[26]_rep_0\,
      I1 => \Dout_reg[30]_0\(16),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \Dout_reg[30]_1\(16),
      O => \ALUResult_out_OBUF[29]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[29]_inst_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"47B8FFFF"
    )
        port map (
      I0 => Q(29),
      I1 => \^dout_reg[26]_rep_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \ALU/p_0_out\(29),
      I4 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[29]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[29]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FCBBFC88"
    )
        port map (
      I0 => \ALU/p_0_out\(30),
      I1 => \^dout_reg[21]_rep_0\(0),
      I2 => \ALUResult_out_OBUF[30]_inst_i_12_n_0\,
      I3 => \^dout_reg[21]_rep_0\(1),
      I4 => \ALU/p_0_out\(29),
      O => \ALUResult_out_OBUF[29]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[29]_inst_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF0000B8000000B8"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(29),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \Dout_reg[28]_1\(21),
      I3 => \^dout_reg[21]_rep_0\(1),
      I4 => \^dout_reg[21]_rep_0\(0),
      I5 => SrcB(0),
      O => \ALUResult_out_OBUF[29]_inst_i_8_n_0\
    );
\ALUResult_out_OBUF[29]_inst_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFEFEAEAEAEFEAE"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(0),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[25]_rep_3\,
      I3 => \ALUResult_out_OBUF[29]_inst_i_3_0\(29),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(27),
      O => \ALUResult_out_OBUF[29]_inst_i_9_n_0\
    );
\ALUResult_out_OBUF[2]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[2]_inst_i_2_n_0\,
      I1 => \Dout_reg[2]_0\,
      O => \^d\(2),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[2]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3C5A3C5AE800E8FF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => Q(2),
      I2 => SrcB(2),
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \Dout_reg[2]_1\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[2]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[2]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(2),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(2),
      O => SrcB(2)
    );
\ALUResult_out_OBUF[2]_inst_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444544444440"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(1),
      I1 => \Dout_reg[28]_1\(1),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(1),
      O => \^dout_reg[8]_rep_0\
    );
\ALUResult_out_OBUF[30]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEEE0EEE0E0"
    )
        port map (
      I0 => \ALUResult_out_OBUF[30]_inst_i_2_n_0\,
      I1 => \ALUResult_out_OBUF[30]_inst_i_3_n_0\,
      I2 => \ALUResult_out_OBUF[30]_inst_i_4_n_0\,
      I3 => \ALUResult_out_OBUF[30]_inst_i_5_n_0\,
      I4 => \ALUResult_out_OBUF[30]_inst_i_6_n_0\,
      I5 => \^dout_reg[25]_rep_0\,
      O => \^d\(30)
    );
\ALUResult_out_OBUF[30]_inst_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFABBFAEEFAAAFA"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(0),
      I1 => \^dout_reg[21]_rep_0\(1),
      I2 => \Dout_reg[28]_1\(21),
      I3 => \^dout_reg[25]_rep_3\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_3_0\(28),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(30),
      O => \ALUResult_out_OBUF[30]_inst_i_10_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020202A2A2A202A2"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(0),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[25]_rep_3\,
      I3 => \ALUResult_out_OBUF[29]_inst_i_3_0\(29),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(27),
      O => \ALUResult_out_OBUF[30]_inst_i_11_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(31),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(21),
      O => \ALUResult_out_OBUF[30]_inst_i_12_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(1),
      I1 => \^dout_reg[21]_rep_0\(0),
      O => \ALUResult_out_OBUF[30]_inst_i_13_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(30),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(21),
      O => \ALU/p_0_out\(30)
    );
\ALUResult_out_OBUF[30]_inst_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888A88888880"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(1),
      I1 => \Dout_reg[28]_1\(1),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(1),
      O => \ALUResult_out_OBUF[30]_inst_i_15_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444544444440"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(1),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(31),
      O => \ALUResult_out_OBUF[30]_inst_i_16_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => Instr_out_OBUF(22),
      I1 => \Dout_reg[27]_rep_n_0\,
      I2 => \^dout_reg[26]_rep_1\,
      O => \ALUResult_out_OBUF[30]_inst_i_17_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF4FFF4FFF0F0F0F"
    )
        port map (
      I0 => \ALUResult_out_OBUF[30]_inst_i_7_n_0\,
      I1 => \ALUResult_out_OBUF[30]_inst_i_8_n_0\,
      I2 => \^dout_reg[25]_rep_0\,
      I3 => \ALUResult_out_OBUF[30]_inst_i_9_n_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => \^dout_reg[23]_rep_0\,
      O => \ALUResult_out_OBUF[30]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004550400040004"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => \ALUResult_out_OBUF[30]_inst_i_10_n_0\,
      I2 => \ALUResult_out_OBUF[30]_inst_i_11_n_0\,
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \ALUResult_out_OBUF[30]_inst_i_12_n_0\,
      I5 => \ALUResult_out_OBUF[30]_inst_i_13_n_0\,
      O => \ALUResult_out_OBUF[30]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88822282"
    )
        port map (
      I0 => \^dout_reg[25]_rep_1\,
      I1 => \ALU/p_0_out\(30),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \^dout_reg[26]_rep_0\,
      I4 => Q(30),
      O => \ALUResult_out_OBUF[30]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEAAEFAA"
    )
        port map (
      I0 => \^dout_reg[25]_rep_1\,
      I1 => \ALU/p_0_out\(30),
      I2 => Q(30),
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \^dout_reg[23]_rep_0\,
      O => \ALUResult_out_OBUF[30]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEAE"
    )
        port map (
      I0 => \^dout_reg[26]_rep_0\,
      I1 => \Dout_reg[30]_0\(17),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \Dout_reg[30]_1\(17),
      O => \ALUResult_out_OBUF[30]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFF70000"
    )
        port map (
      I0 => \ALUResult_out_OBUF[30]_inst_i_15_n_0\,
      I1 => \ALUControl_out_OBUF[1]_inst_i_2_n_0\,
      I2 => \^dout_reg[26]_rep_2\,
      I3 => Instr_out_OBUF(22),
      I4 => \^dout_reg[21]_rep_0\(0),
      I5 => \ALUResult_out_OBUF[30]_inst_i_16_n_0\,
      O => \ALUResult_out_OBUF[30]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAABABFAAAA"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(0),
      I1 => \ALUResult_out_OBUF[0]_inst_i_8_n_0\,
      I2 => \^dout_reg[21]_rep_0\(1),
      I3 => \ALU/p_0_out\(30),
      I4 => \ALUControl_out_OBUF[1]_inst_i_2_n_0\,
      I5 => \ALUResult_out_OBUF[30]_inst_i_17_n_0\,
      O => \ALUResult_out_OBUF[30]_inst_i_8_n_0\
    );
\ALUResult_out_OBUF[30]_inst_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000E2"
    )
        port map (
      I0 => \Dout_reg[28]_1\(21),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_3_0\(30),
      I3 => \^dout_reg[21]_rep_0\(0),
      I4 => \^dout_reg[21]_rep_0\(1),
      O => \ALUResult_out_OBUF[30]_inst_i_9_n_0\
    );
\ALUResult_out_OBUF[31]_inst_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^dout_reg[25]_rep_2\,
      O => \^d\(31)
    );
\ALUResult_out_OBUF[31]_inst_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[31]_inst_i_3_n_0\,
      I1 => \ALUResult_out_OBUF[31]_inst_i_4_n_0\,
      O => \^dout_reg[25]_rep_2\,
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[31]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1AF17D7D1AF12828"
    )
        port map (
      I0 => \^dout_reg[25]_rep_1\,
      I1 => \^dout_reg[23]_rep_0\,
      I2 => \ALUResult_out_OBUF[30]_inst_i_12_n_0\,
      I3 => Q(31),
      I4 => \^dout_reg[26]_rep_0\,
      I5 => \ALUResult_out_OBUF[31]_inst_i_2_0\,
      O => \ALUResult_out_OBUF[31]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[31]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3022F0F0"
    )
        port map (
      I0 => \ALUResult_out_OBUF[30]_inst_i_12_n_0\,
      I1 => \ALUResult_out_OBUF[31]_inst_i_6_n_0\,
      I2 => \ALUResult_out_OBUF[31]_inst_i_7_n_0\,
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[31]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[31]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C000C0F050005000"
    )
        port map (
      I0 => \ALUResult_out_OBUF[0]_inst_i_8_n_0\,
      I1 => SrcB(2),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \^dout_reg[21]_rep_0\(0),
      I4 => \ALU/p_0_out\(1),
      I5 => \^dout_reg[21]_rep_0\(1),
      O => \ALUResult_out_OBUF[31]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[31]_inst_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEAEFEAEFEFE0E0"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => \ALUResult_out_OBUF[31]_inst_i_4_0\,
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \ALU/p_0_out\(29),
      I4 => \ALUResult_out_OBUF[30]_inst_i_12_n_0\,
      I5 => \^dout_reg[21]_rep_0\(1),
      O => \ALUResult_out_OBUF[31]_inst_i_7_n_0\
    );
\ALUResult_out_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \ALUResult_out_OBUF[3]_inst_i_2_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[3]_inst_i_3_n_0\,
      I3 => \^dout_reg[25]_rep_1\,
      I4 => \ALUResult_out_OBUF[3]_inst_i_4_n_0\,
      O => \^d\(3)
    );
\ALUResult_out_OBUF[3]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CDC8CDCDCDC8C8C8"
    )
        port map (
      I0 => \^dout_reg[26]_rep_0\,
      I1 => \ALUResult_out_OBUF[3]_inst_i_5_n_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \Dout_reg[3]_1\,
      I4 => \^dout_reg[21]_rep_0\(0),
      I5 => \Dout_reg[3]_2\,
      O => \ALUResult_out_OBUF[3]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[3]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1D1D1DE2E2E21DE2"
    )
        port map (
      I0 => \Dout_reg[28]_1\(3),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_3_0\(3),
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => Q(3),
      O => \ALUResult_out_OBUF[3]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[3]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF0F000ACACACAC"
    )
        port map (
      I0 => \Dout_reg[30]_1\(2),
      I1 => \Dout_reg[30]_0\(2),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => Q(3),
      I4 => SrcB(3),
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[3]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[3]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5F50CFCF5F50C0C0"
    )
        port map (
      I0 => \ALU/p_0_out\(6),
      I1 => SrcB(4),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => SrcB(5),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => SrcB(3),
      O => \ALUResult_out_OBUF[3]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[3]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(3),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(3),
      O => SrcB(3)
    );
\ALUResult_out_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \ALUResult_out_OBUF[4]_inst_i_2_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[4]_inst_i_3_n_0\,
      I3 => \^dout_reg[25]_rep_1\,
      I4 => \ALUResult_out_OBUF[4]_inst_i_4_n_0\,
      O => \^d\(4)
    );
\ALUResult_out_OBUF[4]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFB8000000B8"
    )
        port map (
      I0 => \Dout_reg[3]_2\,
      I1 => \^dout_reg[21]_rep_0\(0),
      I2 => \Dout_reg[4]_0\,
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => \ALUResult_out_OBUF[4]_inst_i_6_n_0\,
      O => \ALUResult_out_OBUF[4]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[4]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1D1D1DE2E2E21DE2"
    )
        port map (
      I0 => \Dout_reg[28]_1\(4),
      I1 => \^dout_reg[25]_rep_3\,
      I2 => \ALUResult_out_OBUF[29]_inst_i_3_0\(4),
      I3 => \^dout_reg[23]_rep_0\,
      I4 => \^dout_reg[26]_rep_0\,
      I5 => Q(4),
      O => \ALUResult_out_OBUF[4]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[4]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF0F000ACACACAC"
    )
        port map (
      I0 => \Dout_reg[30]_1\(3),
      I1 => \Dout_reg[30]_0\(3),
      I2 => \^dout_reg[23]_rep_0\,
      I3 => Q(4),
      I4 => SrcB(4),
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[4]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[4]_inst_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0AFCFCFA0AFC0C0"
    )
        port map (
      I0 => SrcB(7),
      I1 => SrcB(5),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => \ALU/p_0_out\(6),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => SrcB(4),
      O => \ALUResult_out_OBUF[4]_inst_i_6_n_0\
    );
\ALUResult_out_OBUF[4]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(4),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(4),
      O => SrcB(4)
    );
\ALUResult_out_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8B8B88BB"
    )
        port map (
      I0 => \ALUResult_out_OBUF[5]_inst_i_2_n_0\,
      I1 => \^dout_reg[25]_rep_0\,
      I2 => \ALUResult_out_OBUF[5]_inst_i_3_n_0\,
      I3 => \ALUResult_out_OBUF[5]_inst_i_4_n_0\,
      I4 => \^dout_reg[25]_rep_1\,
      O => \^d\(5)
    );
\ALUResult_out_OBUF[5]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CDC8CDCDCDC8C8C8"
    )
        port map (
      I0 => \^dout_reg[26]_rep_0\,
      I1 => \ALUResult_out_OBUF[5]_inst_i_5_n_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \Dout_reg[4]_0\,
      I4 => \^dout_reg[21]_rep_0\(0),
      I5 => \Dout_reg[5]_0\,
      O => \ALUResult_out_OBUF[5]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[5]_inst_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8474747B847"
    )
        port map (
      I0 => Q(5),
      I1 => \^dout_reg[26]_rep_0\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \Dout_reg[28]_1\(5),
      I4 => \^dout_reg[25]_rep_3\,
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(5),
      O => \ALUResult_out_OBUF[5]_inst_i_3_n_0\
    );
\ALUResult_out_OBUF[5]_inst_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"101F7070101F7F7F"
    )
        port map (
      I0 => Q(5),
      I1 => SrcB(5),
      I2 => \^dout_reg[26]_rep_0\,
      I3 => \Dout_reg[30]_1\(4),
      I4 => \^dout_reg[23]_rep_0\,
      I5 => \Dout_reg[30]_0\(4),
      O => \ALUResult_out_OBUF[5]_inst_i_4_n_0\
    );
\ALUResult_out_OBUF[5]_inst_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5F503F3F5F503030"
    )
        port map (
      I0 => \ALU/p_0_out\(8),
      I1 => \ALU/p_0_out\(6),
      I2 => \^dout_reg[21]_rep_0\(0),
      I3 => SrcB(7),
      I4 => \^dout_reg[21]_rep_0\(1),
      I5 => SrcB(5),
      O => \ALUResult_out_OBUF[5]_inst_i_5_n_0\
    );
\ALUResult_out_OBUF[5]_inst_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(5),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(5),
      O => SrcB(5)
    );
\ALUResult_out_OBUF[6]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[6]_inst_i_2_n_0\,
      I1 => \Dout_reg[6]_0\,
      O => \^d\(6),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[6]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FA5500FAE44EE44E"
    )
        port map (
      I0 => \^dout_reg[25]_rep_1\,
      I1 => \Dout_reg[6]_1\,
      I2 => \^dout_reg[23]_rep_0\,
      I3 => \ALU/p_0_out\(6),
      I4 => Q(6),
      I5 => \^dout_reg[26]_rep_0\,
      O => \ALUResult_out_OBUF[6]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[6]_inst_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(6),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(6),
      O => \ALU/p_0_out\(6)
    );
\ALUResult_out_OBUF[7]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[7]_inst_i_2_n_0\,
      I1 => \Dout_reg[7]_2\,
      O => \^d\(7),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[7]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3C5A3C5AE800E8FF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => Q(7),
      I2 => SrcB(7),
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \Dout_reg[7]_3\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[7]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[7]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(7),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(7),
      O => SrcB(7)
    );
\ALUResult_out_OBUF[8]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[8]_inst_i_2_n_0\,
      I1 => \Dout_reg[8]_0\,
      O => \^d\(8),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[8]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C399C399B200B2FF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => \ALU/p_0_out\(8),
      I2 => Q(8),
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \Dout_reg[8]_1\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[8]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[8]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(8),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(8),
      O => \ALU/p_0_out\(8)
    );
\ALUResult_out_OBUF[9]_inst_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \ALUResult_out_OBUF[9]_inst_i_2_n_0\,
      I1 => \Dout_reg[9]\,
      O => \^d\(9),
      S => \^dout_reg[25]_rep_0\
    );
\ALUResult_out_OBUF[9]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3C5A3C5AE800E8FF"
    )
        port map (
      I0 => \^dout_reg[23]_rep_0\,
      I1 => Q(9),
      I2 => SrcB(9),
      I3 => \^dout_reg[26]_rep_0\,
      I4 => \Dout_reg[9]_0\,
      I5 => \^dout_reg[25]_rep_1\,
      O => \ALUResult_out_OBUF[9]_inst_i_2_n_0\
    );
\ALUResult_out_OBUF[9]_inst_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(9),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(9),
      O => SrcB(9)
    );
\Dout[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Instr_out_OBUF(0),
      I1 => \^dout_reg[27]_rep__0_0\,
      O => \Dout_reg[27]_rep__0_1\(0)
    );
\Dout[0]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => \Dout_reg[31]_5\(0),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(0),
      O => \Dout_reg[19]_rep_rep_0\(0)
    );
\Dout[10]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(1),
      I1 => \^dout_reg[27]_rep__0_0\,
      O => \Dout_reg[27]_rep__0_1\(10)
    );
\Dout[10]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(7),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(10),
      O => \Dout_reg[19]_rep_rep_0\(10)
    );
\Dout[11]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(8),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(11),
      O => \Dout_reg[19]_rep_rep_0\(11)
    );
\Dout[12]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(9),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(12),
      O => \Dout_reg[19]_rep_rep_0\(12)
    );
\Dout[13]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(10),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(13),
      O => \Dout_reg[19]_rep_rep_0\(13)
    );
\Dout[14]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(12),
      O => \Dout_reg[27]_rep__0_1\(11)
    );
\Dout[14]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(11),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(14),
      O => \Dout_reg[19]_rep_rep_0\(14)
    );
\Dout[15]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(13),
      O => \Dout_reg[27]_rep__0_1\(12)
    );
\Dout[15]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(12),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(15),
      O => \Dout_reg[19]_rep_rep_0\(15)
    );
\Dout[16]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(14),
      O => \Dout_reg[27]_rep__0_1\(13)
    );
\Dout[16]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(13),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(16),
      O => \Dout_reg[19]_rep_rep_0\(16)
    );
\Dout[17]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(15),
      O => \Dout_reg[27]_rep__0_1\(14)
    );
\Dout[17]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(14),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(17),
      O => \Dout_reg[19]_rep_rep_0\(17)
    );
\Dout[18]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(16),
      O => \Dout_reg[27]_rep__0_1\(15)
    );
\Dout[18]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(15),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(18),
      O => \Dout_reg[19]_rep_rep_0\(18)
    );
\Dout[19]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(16),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(19),
      O => \Dout_reg[19]_rep_rep_0\(19)
    );
\Dout[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Instr_out_OBUF(1),
      I1 => \^dout_reg[27]_rep__0_0\,
      O => \Dout_reg[27]_rep__0_1\(1)
    );
\Dout[1]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => \Dout_reg[31]_5\(1),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(1),
      O => \Dout_reg[19]_rep_rep_0\(1)
    );
\Dout[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^d\(9),
      I1 => \^d\(7),
      I2 => \^d\(23),
      I3 => \^d\(12),
      I4 => \Dout[1]_i_6_n_0\,
      O => \Dout[1]_i_6_0\
    );
\Dout[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => \^d\(20),
      I1 => \^d\(6),
      I2 => \^dout_reg[25]_rep_2\,
      I3 => \^d\(14),
      I4 => \Dout[1]_i_7_n_0\,
      O => \Dout[1]_i_7_0\
    );
\Dout[1]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^d\(10),
      I1 => \^d\(2),
      I2 => \^d\(21),
      I3 => \^d\(8),
      I4 => \Dout[1]_i_8_n_0\,
      O => \Dout[1]_i_8_0\
    );
\Dout[1]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^d\(16),
      I1 => \^d\(15),
      I2 => \^d\(28),
      I3 => \^d\(27),
      I4 => \Dout[1]_i_9_n_0\,
      O => \Dout[1]_i_9_0\
    );
\Dout[1]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^d\(1),
      I1 => \^d\(25),
      I2 => \^d\(5),
      I3 => \^d\(11),
      O => \Dout[1]_i_6_n_0\
    );
\Dout[1]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^d\(4),
      I1 => \^d\(19),
      I2 => \^d\(26),
      I3 => \^d\(30),
      O => \Dout[1]_i_7_n_0\
    );
\Dout[1]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^d\(22),
      I1 => \^d\(24),
      I2 => \^d\(13),
      I3 => \^d\(18),
      O => \Dout[1]_i_8_n_0\
    );
\Dout[1]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^d\(0),
      I1 => \^d\(3),
      I2 => \^d\(17),
      I3 => \^d\(29),
      O => \Dout[1]_i_9_n_0\
    );
\Dout[20]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(18),
      O => \Dout_reg[27]_rep__0_1\(16)
    );
\Dout[20]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(17),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(20),
      O => \Dout_reg[19]_rep_rep_0\(20)
    );
\Dout[21]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(17),
      O => \Dout_reg[27]_rep__0_1\(17)
    );
\Dout[21]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(18),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(21),
      O => \Dout_reg[19]_rep_rep_0\(21)
    );
\Dout[22]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => \^dout_reg[20]_rep_0\(0),
      O => \Dout_reg[27]_rep__0_1\(18)
    );
\Dout[22]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(19),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(22),
      O => \Dout_reg[19]_rep_rep_0\(22)
    );
\Dout[23]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => \^dout_reg[21]_rep_0\(2),
      O => \Dout_reg[27]_rep__0_1\(19)
    );
\Dout[23]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(20),
      I1 => Instr_out_OBUF(17),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(23),
      O => \Dout_reg[19]_rep_rep_0\(23)
    );
\Dout[24]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(22),
      O => \Dout_reg[27]_rep__0_1\(20)
    );
\Dout[24]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(21),
      I1 => Instr_out_OBUF(17),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(24),
      O => \Dout_reg[19]_rep_rep_0\(24)
    );
\Dout[25]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(22),
      I1 => Instr_out_OBUF(17),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(25),
      O => \Dout_reg[19]_rep_rep_0\(25)
    );
\Dout[26]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(23),
      I1 => Instr_out_OBUF(17),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(26),
      O => \Dout_reg[19]_rep_rep_0\(26)
    );
\Dout[27]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(24),
      I1 => Instr_out_OBUF(17),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(27),
      O => \Dout_reg[19]_rep_rep_0\(27)
    );
\Dout[28]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(25),
      I1 => Instr_out_OBUF(17),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(28),
      O => \Dout_reg[19]_rep_rep_0\(28)
    );
\Dout[29]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(26),
      I1 => Instr_out_OBUF(17),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(29),
      O => \Dout_reg[19]_rep_rep_0\(29)
    );
\Dout[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(0),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => Instr_out_OBUF(2),
      O => \Dout_reg[27]_rep__0_1\(2)
    );
\Dout[2]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"575F5F5F54505050"
    )
        port map (
      I0 => \Dout_reg[31]_5\(2),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(2),
      O => \Dout_reg[19]_rep_rep_0\(2)
    );
\Dout[30]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(27),
      I1 => Instr_out_OBUF(17),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(30),
      O => \Dout_reg[19]_rep_rep_0\(30)
    );
\Dout[31]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(23),
      O => \Dout_reg[27]_rep__0_1\(21)
    );
\Dout[31]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(28),
      I1 => Instr_out_OBUF(17),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(31),
      O => \Dout_reg[19]_rep_rep_0\(31)
    );
\Dout[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF55FC5500550C55"
    )
        port map (
      I0 => \^dout_reg[25]_rep_2\,
      I1 => \Dout_reg[31]_5\(31),
      I2 => \Dout_reg[31]_4\(2),
      I3 => \Dout_reg[31]_6\,
      I4 => \Dout_reg[31]_4\(1),
      I5 => Result_out_OBUF(0),
      O => \Dout_reg[31]_2\(0)
    );
\Dout[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(1),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => Instr_out_OBUF(3),
      O => \Dout_reg[27]_rep__0_1\(3)
    );
\Dout[3]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(0),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(3),
      O => \Dout_reg[19]_rep_rep_0\(3)
    );
\Dout[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^dout_reg[26]_rep_1\,
      I1 => \Dout_reg[27]_rep_n_0\,
      O => \^dout_reg[26]_rep_2\
    );
\Dout[4]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(2),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => Instr_out_OBUF(4),
      O => \Dout_reg[27]_rep__0_1\(4)
    );
\Dout[4]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(1),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(4),
      O => \Dout_reg[19]_rep_rep_0\(4)
    );
\Dout[5]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(3),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => Instr_out_OBUF(5),
      O => \Dout_reg[27]_rep__0_1\(5)
    );
\Dout[5]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(2),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(5),
      O => \Dout_reg[19]_rep_rep_0\(5)
    );
\Dout[6]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(4),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => Instr_out_OBUF(6),
      O => \Dout_reg[27]_rep__0_1\(6)
    );
\Dout[6]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(3),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(6),
      O => \Dout_reg[19]_rep_rep_0\(6)
    );
\Dout[7]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(5),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => \^dout_reg[21]_rep_0\(0),
      O => \Dout_reg[27]_rep__0_1\(7)
    );
\Dout[7]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(4),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(7),
      O => \Dout_reg[19]_rep_rep_0\(7)
    );
\Dout[8]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(6),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => \^dout_reg[21]_rep_0\(1),
      O => \Dout_reg[27]_rep__0_1\(8)
    );
\Dout[8]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(5),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(8),
      O => \Dout_reg[19]_rep_rep_0\(8)
    );
\Dout[9]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^dout_reg[21]_rep_0\(0),
      I1 => \^dout_reg[27]_rep__0_0\,
      O => \Dout_reg[27]_rep__0_1\(9)
    );
\Dout[9]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAFAFAFA8A0A0A0"
    )
        port map (
      I0 => PCPlus8(6),
      I1 => Instr_out_OBUF(17),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => Instr_out_OBUF(16),
      I4 => Instr_out_OBUF(18),
      I5 => DATA_OUT1(9),
      O => \Dout_reg[19]_rep_rep_0\(9)
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(0),
      Q => \Dout_reg[31]_3\(0),
      R => RESET_IBUF
    );
\Dout_reg[0]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(0),
      Q => Instr_out_OBUF(0),
      R => RESET_IBUF
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(9),
      Q => \Dout_reg[31]_3\(9),
      R => RESET_IBUF
    );
\Dout_reg[12]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(9),
      Q => Instr_out_OBUF(12),
      R => RESET_IBUF
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(10),
      Q => \Dout_reg[31]_3\(10),
      R => RESET_IBUF
    );
\Dout_reg[13]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(10),
      Q => Instr_out_OBUF(13),
      R => RESET_IBUF
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(11),
      Q => \Dout_reg[31]_3\(11),
      R => RESET_IBUF
    );
\Dout_reg[14]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(11),
      Q => Instr_out_OBUF(14),
      R => RESET_IBUF
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(12),
      Q => \Dout_reg[31]_3\(12),
      R => RESET_IBUF
    );
\Dout_reg[15]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(12),
      Q => Instr_out_OBUF(15),
      R => RESET_IBUF
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(13),
      Q => \Dout_reg[31]_3\(13),
      R => RESET_IBUF
    );
\Dout_reg[16]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(13),
      Q => Instr_out_OBUF(16),
      R => RESET_IBUF
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(14),
      Q => \Dout_reg[31]_3\(14),
      R => RESET_IBUF
    );
\Dout_reg[18]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(14),
      Q => Instr_out_OBUF(18),
      R => RESET_IBUF
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(15),
      Q => \Dout_reg[31]_3\(15),
      R => RESET_IBUF
    );
\Dout_reg[19]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(15),
      Q => \Dout_reg[19]_rep_0\,
      R => RESET_IBUF
    );
\Dout_reg[19]_rep_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(15),
      Q => Instr_out_OBUF(17),
      R => RESET_IBUF
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(1),
      Q => \Dout_reg[31]_3\(1),
      R => RESET_IBUF
    );
\Dout_reg[1]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(1),
      Q => Instr_out_OBUF(1),
      R => RESET_IBUF
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(16),
      Q => \Dout_reg[31]_3\(16),
      R => RESET_IBUF
    );
\Dout_reg[20]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(16),
      Q => \^dout_reg[20]_rep_0\(0),
      R => RESET_IBUF
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(17),
      Q => \Dout_reg[31]_3\(17),
      R => RESET_IBUF
    );
\Dout_reg[21]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(17),
      Q => \^dout_reg[21]_rep_0\(2),
      R => RESET_IBUF
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(18),
      Q => \Dout_reg[31]_3\(18),
      R => RESET_IBUF
    );
\Dout_reg[22]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(18),
      Q => Instr_out_OBUF(22),
      R => RESET_IBUF
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(19),
      Q => \Dout_reg[31]_3\(19),
      R => RESET_IBUF
    );
\Dout_reg[23]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(19),
      Q => Instr_out_OBUF(23),
      R => RESET_IBUF
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(20),
      Q => \Dout_reg[31]_3\(20),
      R => RESET_IBUF
    );
\Dout_reg[24]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(20),
      Q => Instr_out_OBUF(24),
      R => RESET_IBUF
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(21),
      Q => \Dout_reg[31]_3\(21),
      R => RESET_IBUF
    );
\Dout_reg[25]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(21),
      Q => Instr_out_OBUF(25),
      R => RESET_IBUF
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(22),
      Q => \Dout_reg[31]_3\(22),
      R => RESET_IBUF
    );
\Dout_reg[26]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(22),
      Q => \^dout_reg[26]_rep_1\,
      R => RESET_IBUF
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(23),
      Q => \Dout_reg[31]_3\(23),
      R => RESET_IBUF
    );
\Dout_reg[27]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[27]_rep_0\,
      Q => \Dout_reg[27]_rep_n_0\,
      R => RESET_IBUF
    );
\Dout_reg[27]_rep__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(23),
      Q => \^dout_reg[27]_rep__0_0\,
      R => RESET_IBUF
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(24),
      Q => \Dout_reg[31]_3\(24),
      R => RESET_IBUF
    );
\Dout_reg[28]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(24),
      Q => Instr_out_OBUF(28),
      R => RESET_IBUF
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(2),
      Q => \Dout_reg[31]_3\(2),
      R => RESET_IBUF
    );
\Dout_reg[2]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(2),
      Q => Instr_out_OBUF(2),
      R => RESET_IBUF
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(25),
      Q => \Dout_reg[31]_3\(25),
      R => RESET_IBUF
    );
\Dout_reg[30]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(25),
      Q => Instr_out_OBUF(30),
      R => RESET_IBUF
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(26),
      Q => \Dout_reg[31]_3\(26),
      R => RESET_IBUF
    );
\Dout_reg[31]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(26),
      Q => \Dout_reg[31]_rep_0\,
      R => RESET_IBUF
    );
\Dout_reg[31]_rep_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(26),
      Q => Instr_out_OBUF(29),
      R => RESET_IBUF
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(3),
      Q => \Dout_reg[31]_3\(3),
      R => RESET_IBUF
    );
\Dout_reg[3]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(3),
      Q => Instr_out_OBUF(3),
      R => RESET_IBUF
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(4),
      Q => \Dout_reg[31]_3\(4),
      R => RESET_IBUF
    );
\Dout_reg[4]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(4),
      Q => Instr_out_OBUF(4),
      R => RESET_IBUF
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(5),
      Q => \Dout_reg[31]_3\(5),
      R => RESET_IBUF
    );
\Dout_reg[5]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(5),
      Q => Instr_out_OBUF(5),
      R => RESET_IBUF
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(6),
      Q => \Dout_reg[31]_3\(6),
      R => RESET_IBUF
    );
\Dout_reg[6]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(6),
      Q => Instr_out_OBUF(6),
      R => RESET_IBUF
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(7),
      Q => \Dout_reg[31]_3\(7),
      R => RESET_IBUF
    );
\Dout_reg[7]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(7),
      Q => \^dout_reg[21]_rep_0\(0),
      R => RESET_IBUF
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(8),
      Q => \Dout_reg[31]_3\(8),
      R => RESET_IBUF
    );
\Dout_reg[8]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[31]_4\(0),
      D => \Dout_reg[31]_7\(8),
      Q => \^dout_reg[21]_rep_0\(1),
      R => RESET_IBUF
    );
\FSM_onehot_current_state[10]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => Instr_out_OBUF(15),
      I1 => Instr_out_OBUF(12),
      I2 => Instr_out_OBUF(14),
      I3 => Instr_out_OBUF(13),
      O => \Dout_reg[15]_rep_2\
    );
\FSM_onehot_current_state[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000100000"
    )
        port map (
      I0 => \^dout_reg[26]_rep_1\,
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => Instr_out_OBUF(24),
      I3 => Instr_out_OBUF(23),
      I4 => \FSM_onehot_current_state_reg[13]\,
      I5 => \Dout_reg[31]_4\(0),
      O => \Dout_reg[24]_rep_0\(2)
    );
\FSM_onehot_current_state[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000020"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg[13]\,
      I1 => \Dout_reg[31]_4\(0),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => Instr_out_OBUF(24),
      O => \Dout_reg[24]_rep_0\(3)
    );
\FSM_onehot_current_state[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000800"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg[13]\,
      I1 => Instr_out_OBUF(24),
      I2 => \Dout_reg[31]_4\(0),
      I3 => \^dout_reg[27]_rep__0_0\,
      I4 => \^dout_reg[26]_rep_1\,
      O => \Dout_reg[24]_rep_0\(4)
    );
\FSM_onehot_current_state[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"09069A9A09069595"
    )
        port map (
      I0 => Instr_out_OBUF(28),
      I1 => \FSM_onehot_current_state_reg[2]\(0),
      I2 => Instr_out_OBUF(30),
      I3 => Flags_in(1),
      I4 => Instr_out_OBUF(29),
      I5 => Flags_in(0),
      O => \Dout_reg[28]_rep_0\
    );
\FSM_onehot_current_state[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \^dout_reg[26]_rep_1\,
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => \FSM_onehot_current_state_reg[13]\,
      I3 => \Dout_reg[31]_4\(0),
      O => \Dout_reg[24]_rep_0\(0)
    );
\FSM_onehot_current_state[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000D0000"
    )
        port map (
      I0 => Instr_out_OBUF(24),
      I1 => Instr_out_OBUF(23),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \^dout_reg[27]_rep__0_0\,
      I4 => \FSM_onehot_current_state_reg[13]\,
      I5 => \Dout_reg[31]_4\(0),
      O => \Dout_reg[24]_rep_0\(1)
    );
RF_reg_r1_0_15_0_5_i_10: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => Instr_out_OBUF(16),
      I1 => \^dout_reg[27]_rep__0_0\,
      O => ADDRA(0)
    );
RF_reg_r1_0_15_0_5_i_11: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(15),
      O => ADDRD(3)
    );
RF_reg_r1_0_15_0_5_i_12: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(14),
      O => ADDRD(2)
    );
RF_reg_r1_0_15_0_5_i_13: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(13),
      O => ADDRD(1)
    );
RF_reg_r1_0_15_0_5_i_14: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Instr_out_OBUF(12),
      I1 => \^dout_reg[27]_rep__0_0\,
      O => ADDRD(0)
    );
RF_reg_r1_0_15_0_5_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(1),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(1),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(1),
      O => DATA_IN(1)
    );
RF_reg_r1_0_15_0_5_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(0),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(0),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(0),
      O => DATA_IN(0)
    );
RF_reg_r1_0_15_0_5_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(3),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(3),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(3),
      O => DATA_IN(3)
    );
RF_reg_r1_0_15_0_5_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(2),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(2),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(2),
      O => DATA_IN(2)
    );
RF_reg_r1_0_15_0_5_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(5),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(5),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(5),
      O => DATA_IN(5)
    );
RF_reg_r1_0_15_0_5_i_7: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(4),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(4),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(4),
      O => DATA_IN(4)
    );
RF_reg_r1_0_15_0_5_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^dout_reg[27]_rep__0_0\,
      I1 => Instr_out_OBUF(17),
      O => ADDRA(2)
    );
RF_reg_r1_0_15_0_5_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => Instr_out_OBUF(18),
      I1 => \^dout_reg[27]_rep__0_0\,
      O => ADDRA(1)
    );
RF_reg_r1_0_15_12_17_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(13),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(13),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(13),
      O => DATA_IN(13)
    );
RF_reg_r1_0_15_12_17_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(12),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(12),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(12),
      O => DATA_IN(12)
    );
RF_reg_r1_0_15_12_17_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(15),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(15),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(15),
      O => DATA_IN(15)
    );
RF_reg_r1_0_15_12_17_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(14),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(14),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(14),
      O => DATA_IN(14)
    );
RF_reg_r1_0_15_12_17_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(17),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(17),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(17),
      O => DATA_IN(17)
    );
RF_reg_r1_0_15_12_17_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(16),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(16),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(16),
      O => DATA_IN(16)
    );
RF_reg_r1_0_15_18_23_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(19),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(19),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(19),
      O => DATA_IN(19)
    );
RF_reg_r1_0_15_18_23_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(18),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(18),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(18),
      O => DATA_IN(18)
    );
RF_reg_r1_0_15_18_23_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(21),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(21),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(21),
      O => DATA_IN(21)
    );
RF_reg_r1_0_15_18_23_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(20),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(20),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(20),
      O => DATA_IN(20)
    );
RF_reg_r1_0_15_18_23_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(23),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(23),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(23),
      O => DATA_IN(23)
    );
RF_reg_r1_0_15_18_23_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(22),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(22),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(22),
      O => DATA_IN(22)
    );
RF_reg_r1_0_15_24_29_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(25),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(25),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(25),
      O => DATA_IN(25)
    );
RF_reg_r1_0_15_24_29_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(24),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(24),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(24),
      O => DATA_IN(24)
    );
RF_reg_r1_0_15_24_29_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(27),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(27),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(27),
      O => DATA_IN(27)
    );
RF_reg_r1_0_15_24_29_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(26),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(26),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(26),
      O => DATA_IN(26)
    );
RF_reg_r1_0_15_24_29_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(29),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(29),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(29),
      O => DATA_IN(29)
    );
RF_reg_r1_0_15_24_29_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(28),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(28),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(28),
      O => DATA_IN(28)
    );
RF_reg_r1_0_15_30_31_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(31),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(31),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(31),
      O => DATA_IN(31)
    );
RF_reg_r1_0_15_30_31_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(30),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(30),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(30),
      O => DATA_IN(30)
    );
RF_reg_r1_0_15_6_11_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(7),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(7),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(7),
      O => DATA_IN(7)
    );
RF_reg_r1_0_15_6_11_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(6),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(6),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(6),
      O => DATA_IN(6)
    );
RF_reg_r1_0_15_6_11_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(9),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(9),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(9),
      O => DATA_IN(9)
    );
RF_reg_r1_0_15_6_11_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(8),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(8),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(8),
      O => DATA_IN(8)
    );
RF_reg_r1_0_15_6_11_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(11),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(11),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(11),
      O => DATA_IN(11)
    );
RF_reg_r1_0_15_6_11_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout_reg[31]_5\(10),
      I1 => \^dout_reg[27]_rep__0_0\,
      I2 => ReadData_in_regRD(10),
      I3 => \^dout_reg[26]_rep_1\,
      I4 => RF_reg_r2_0_15_30_31(10),
      O => DATA_IN(10)
    );
RF_reg_r2_0_15_0_5_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(15),
      I1 => \^dout_reg[26]_rep_1\,
      I2 => Instr_out_OBUF(3),
      O => \^dout_reg[15]_rep_1\(3)
    );
RF_reg_r2_0_15_0_5_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(14),
      I1 => \^dout_reg[26]_rep_1\,
      I2 => Instr_out_OBUF(2),
      O => \^dout_reg[15]_rep_1\(2)
    );
RF_reg_r2_0_15_0_5_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(13),
      I1 => \^dout_reg[26]_rep_1\,
      I2 => Instr_out_OBUF(1),
      O => \^dout_reg[15]_rep_1\(1)
    );
RF_reg_r2_0_15_0_5_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Instr_out_OBUF(12),
      I1 => \^dout_reg[26]_rep_1\,
      I2 => Instr_out_OBUF(0),
      O => \^dout_reg[15]_rep_1\(0)
    );
\S0_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001FFFDFFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(7),
      I1 => Instr_out_OBUF(25),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(7),
      I5 => Q(7),
      O => \Dout_reg[7]_0\(3)
    );
\S0_carry__0_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(7),
      I1 => \Dout_reg[28]_1\(7),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \^dout_reg[27]_rep__0_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(7),
      O => \Dout_reg[7]_1\(3)
    );
\S0_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(6),
      I1 => Instr_out_OBUF(25),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(6),
      I5 => Q(6),
      O => \Dout_reg[7]_1\(2)
    );
\S0_carry__0_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(6),
      I1 => \Dout_reg[28]_1\(6),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \^dout_reg[27]_rep__0_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(6),
      O => \Dout_reg[7]_0\(2)
    );
\S0_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001FFFDFFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(5),
      I1 => Instr_out_OBUF(25),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(5),
      I5 => Q(5),
      O => \Dout_reg[7]_0\(1)
    );
\S0_carry__0_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(5),
      I1 => \Dout_reg[28]_1\(5),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \^dout_reg[27]_rep__0_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(5),
      O => \Dout_reg[7]_1\(1)
    );
\S0_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(4),
      I1 => \Dout_reg[28]_1\(4),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \^dout_reg[27]_rep__0_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(4),
      O => \Dout_reg[7]_0\(0)
    );
\S0_carry__0_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(4),
      I1 => Instr_out_OBUF(25),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(4),
      I5 => Q(4),
      O => \Dout_reg[7]_1\(0)
    );
\S0_carry__1_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA9AAAA"
    )
        port map (
      I0 => Q(11),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_3_0\(11),
      O => \Dout_reg[11]\(3)
    );
\S0_carry__1_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0002FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(11),
      I1 => \^dout_reg[26]_rep_1\,
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(25),
      I4 => Q(11),
      O => \Dout_reg[11]_0\(3)
    );
\S0_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001FFFDFFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(10),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(10),
      I5 => Q(10),
      O => \Dout_reg[11]\(2)
    );
\S0_carry__1_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(10),
      I1 => \Dout_reg[28]_1\(10),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(10),
      O => \Dout_reg[11]_0\(2)
    );
\S0_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(9),
      I1 => \Dout_reg[28]_1\(9),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(9),
      O => \Dout_reg[11]\(1)
    );
\S0_carry__1_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(9),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(9),
      I5 => Q(9),
      O => \Dout_reg[11]_0\(1)
    );
\S0_carry__1_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(8),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(8),
      I5 => Q(8),
      O => \Dout_reg[11]_0\(0)
    );
\S0_carry__1_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(8),
      I1 => \Dout_reg[28]_1\(8),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(8),
      O => \Dout_reg[11]\(0)
    );
\S0_carry__2_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(15),
      I1 => \Dout_reg[28]_1\(12),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(15),
      O => \Dout_reg[15]_0\(3)
    );
\S0_carry__2_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(15),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(12),
      I5 => Q(15),
      O => \Dout_reg[15]_1\(3)
    );
\S0_carry__2_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(14),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(11),
      I5 => Q(14),
      O => \Dout_reg[15]_1\(2)
    );
\S0_carry__2_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(14),
      I1 => \Dout_reg[28]_1\(11),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(14),
      O => \Dout_reg[15]_0\(2)
    );
\S0_carry__2_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA9AAAA"
    )
        port map (
      I0 => Q(13),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_3_0\(13),
      O => \Dout_reg[15]_0\(1)
    );
\S0_carry__2_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0002FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(13),
      I1 => \^dout_reg[26]_rep_1\,
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(25),
      I4 => Q(13),
      O => \Dout_reg[15]_1\(1)
    );
\S0_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA9AAAA"
    )
        port map (
      I0 => Q(12),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_3_0\(12),
      O => \Dout_reg[15]_0\(0)
    );
\S0_carry__2_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0002FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(12),
      I1 => \^dout_reg[26]_rep_1\,
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => Instr_out_OBUF(25),
      I4 => Q(12),
      O => \Dout_reg[15]_1\(0)
    );
\S0_carry__3_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAABAAA855545557"
    )
        port map (
      I0 => \Dout_reg[28]_1\(17),
      I1 => Instr_out_OBUF(25),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_3_0\(19),
      I5 => Q(19),
      O => \Dout_reg[21]_0\(3)
    );
\S0_carry__3_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555556AAAAAAA6"
    )
        port map (
      I0 => Q(19),
      I1 => \ALUResult_out_OBUF[29]_inst_i_3_0\(19),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \^dout_reg[27]_rep__0_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \Dout_reg[28]_1\(17),
      O => \Dout_reg[19]_0\(3)
    );
\S0_carry__3_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(18),
      I1 => Instr_out_OBUF(25),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(15),
      I5 => Q(18),
      O => \Dout_reg[21]_0\(2)
    );
\S0_carry__3_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(18),
      I1 => \Dout_reg[28]_1\(15),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \^dout_reg[27]_rep__0_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(18),
      O => \Dout_reg[19]_0\(2)
    );
\S0_carry__3_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(17),
      I1 => Instr_out_OBUF(25),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(14),
      I5 => Q(17),
      O => \Dout_reg[21]_0\(1)
    );
\S0_carry__3_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(17),
      I1 => \Dout_reg[28]_1\(14),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \^dout_reg[27]_rep__0_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(17),
      O => \Dout_reg[19]_0\(1)
    );
\S0_carry__3_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(16),
      I1 => Instr_out_OBUF(25),
      I2 => \^dout_reg[27]_rep__0_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(13),
      I5 => Q(16),
      O => \Dout_reg[21]_0\(0)
    );
\S0_carry__3_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(16),
      I1 => \Dout_reg[28]_1\(13),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \^dout_reg[27]_rep__0_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(16),
      O => \Dout_reg[19]_0\(0)
    );
\S0_carry__4_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(23),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(19),
      I5 => Q(23),
      O => \Dout_reg[23]_0\(3)
    );
\S0_carry__4_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(23),
      I1 => \Dout_reg[28]_1\(19),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(23),
      O => \Dout_reg[23]_1\(3)
    );
\S0_carry__4_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(22),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(18),
      I5 => Q(22),
      O => \Dout_reg[23]_0\(2)
    );
\S0_carry__4_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(22),
      I1 => \Dout_reg[28]_1\(18),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(22),
      O => \Dout_reg[23]_1\(2)
    );
\S0_carry__4_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAABAAA855545557"
    )
        port map (
      I0 => \Dout_reg[28]_1\(17),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \ALUResult_out_OBUF[29]_inst_i_3_0\(21),
      I5 => Q(21),
      O => \Dout_reg[23]_0\(1)
    );
\S0_carry__4_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555556AAAAAAA6"
    )
        port map (
      I0 => Q(21),
      I1 => \ALUResult_out_OBUF[29]_inst_i_3_0\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \Dout_reg[28]_1\(17),
      O => \Dout_reg[23]_1\(1)
    );
\S0_carry__4_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(20),
      I1 => \Dout_reg[28]_1\(16),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(20),
      O => \Dout_reg[23]_0\(0)
    );
\S0_carry__4_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001FFFDFFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(20),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(16),
      I5 => Q(20),
      O => \Dout_reg[23]_1\(0)
    );
\S0_carry__5_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA955555559"
    )
        port map (
      I0 => Q(27),
      I1 => \ALUResult_out_OBUF[29]_inst_i_3_0\(27),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \Dout_reg[28]_1\(21),
      O => \Dout_reg[27]_0\(3)
    );
\S0_carry__5_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(27),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(27),
      O => \Dout_reg[27]_1\(3)
    );
\S0_carry__5_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(26),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(26),
      O => \Dout_reg[27]_1\(2)
    );
\S0_carry__5_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA955555559"
    )
        port map (
      I0 => Q(26),
      I1 => \ALUResult_out_OBUF[29]_inst_i_3_0\(26),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \Dout_reg[28]_1\(21),
      O => \Dout_reg[27]_0\(2)
    );
\S0_carry__5_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA955555559"
    )
        port map (
      I0 => Q(25),
      I1 => \ALUResult_out_OBUF[29]_inst_i_3_0\(25),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \Dout_reg[28]_1\(21),
      O => \Dout_reg[27]_0\(1)
    );
\S0_carry__5_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(25),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(25),
      O => \Dout_reg[27]_1\(1)
    );
\S0_carry__5_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(24),
      I1 => \Dout_reg[28]_1\(20),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(24),
      O => \Dout_reg[27]_0\(0)
    );
\S0_carry__5_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001FFFDFFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(24),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(20),
      I5 => Q(24),
      O => \Dout_reg[27]_1\(0)
    );
\S0_carry__6_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001FFFDFFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(31),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(21),
      I5 => Q(31),
      O => \Dout_reg[31]_1\(3)
    );
\S0_carry__6_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001FFFDFFFE0002"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(30),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(21),
      I5 => Q(30),
      O => \Dout_reg[31]_1\(2)
    );
\S0_carry__6_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(31),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(31),
      O => \Dout_reg[31]_0\(3)
    );
\S0_carry__6_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(29),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(29),
      O => \Dout_reg[31]_1\(1)
    );
\S0_carry__6_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(30),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(30),
      O => \Dout_reg[31]_0\(2)
    );
\S0_carry__6_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(29),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(29),
      O => \Dout_reg[31]_0\(1)
    );
\S0_carry__6_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(28),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(28),
      O => \Dout_reg[31]_1\(0)
    );
\S0_carry__6_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(28),
      I1 => \Dout_reg[28]_1\(21),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(28),
      O => \Dout_reg[31]_0\(0)
    );
S0_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(3),
      I1 => \Dout_reg[28]_1\(3),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(3),
      O => \Dout_reg[3]_0\(3)
    );
\S0_carry_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(3),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(3),
      I5 => Q(3),
      O => S(3)
    );
S0_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(2),
      I1 => \Dout_reg[28]_1\(2),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(2),
      O => \Dout_reg[3]_0\(2)
    );
\S0_carry_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE00020001FFFD"
    )
        port map (
      I0 => \ALUResult_out_OBUF[29]_inst_i_3_0\(2),
      I1 => Instr_out_OBUF(25),
      I2 => \Dout_reg[27]_rep_n_0\,
      I3 => \^dout_reg[26]_rep_1\,
      I4 => \Dout_reg[28]_1\(2),
      I5 => Q(2),
      O => S(2)
    );
S0_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(1),
      I1 => \Dout_reg[28]_1\(1),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(1),
      O => S(1)
    );
\S0_carry_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(1),
      I1 => \Dout_reg[28]_1\(1),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(1),
      O => \Dout_reg[3]_0\(1)
    );
S0_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666656666666A"
    )
        port map (
      I0 => Q(0),
      I1 => \Dout_reg[28]_1\(0),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(0),
      O => \Dout_reg[3]_0\(0)
    );
\S0_carry_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999A99999995"
    )
        port map (
      I0 => Q(0),
      I1 => \Dout_reg[28]_1\(0),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => \Dout_reg[27]_rep_n_0\,
      I4 => Instr_out_OBUF(25),
      I5 => \ALUResult_out_OBUF[29]_inst_i_3_0\(0),
      O => S(0)
    );
\WriteData_out_OBUF[31]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A80000000000000"
    )
        port map (
      I0 => \^dout_reg[15]_rep_1\(0),
      I1 => Instr_out_OBUF(15),
      I2 => \^dout_reg[26]_rep_1\,
      I3 => Instr_out_OBUF(3),
      I4 => \^dout_reg[15]_rep_1\(1),
      I5 => \^dout_reg[15]_rep_1\(2),
      O => \Dout_reg[15]_rep_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_4 is
  port (
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 );
    RESET_IBUF : in STD_LOGIC;
    \Dout_reg[2]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_4 : entity is "REGrwe_n";
end REGrwe_n_4;

architecture STRUCTURE of REGrwe_n_4 is
begin
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[2]_0\(0),
      D => D(0),
      Q => Q(0),
      R => RESET_IBUF
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[2]_0\(0),
      D => D(1),
      Q => Q(1),
      R => RESET_IBUF
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[2]_0\(0),
      D => D(2),
      Q => Q(2),
      R => RESET_IBUF
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[2]_0\(0),
      D => D(3),
      Q => Q(3),
      R => RESET_IBUF
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => \Dout_reg[2]_0\(0),
      D => D(4),
      Q => Q(4),
      R => RESET_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_5 is
  port (
    \Dout_reg[30]_0\ : out STD_LOGIC_VECTOR ( 30 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[2]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \Dout_reg[30]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \Dout_reg[30]_2\ : in STD_LOGIC;
    Result_out_OBUF : in STD_LOGIC_VECTOR ( 30 downto 0 );
    ALUResult_out_OBUF : in STD_LOGIC_VECTOR ( 30 downto 0 );
    \Dout_reg[0]_0\ : in STD_LOGIC;
    DATA_OUT2 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    RESET_IBUF : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_5 : entity is "REGrwe_n";
end REGrwe_n_5;

architecture STRUCTURE of REGrwe_n_5 is
  signal \^q\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[0]_inst_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \WriteData_out_OBUF[2]_inst_i_1\ : label is "soft_lutpair75";
begin
  Q(31 downto 0) <= \^q\(31 downto 0);
\Dout[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(0),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(0),
      I5 => ALUResult_out_OBUF(0),
      O => \Dout_reg[30]_0\(0)
    );
\Dout[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(10),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(10),
      I5 => ALUResult_out_OBUF(10),
      O => \Dout_reg[30]_0\(10)
    );
\Dout[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(11),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(11),
      I5 => ALUResult_out_OBUF(11),
      O => \Dout_reg[30]_0\(11)
    );
\Dout[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(12),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(12),
      I5 => ALUResult_out_OBUF(12),
      O => \Dout_reg[30]_0\(12)
    );
\Dout[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(13),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(13),
      I5 => ALUResult_out_OBUF(13),
      O => \Dout_reg[30]_0\(13)
    );
\Dout[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(14),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(14),
      I5 => ALUResult_out_OBUF(14),
      O => \Dout_reg[30]_0\(14)
    );
\Dout[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(15),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(15),
      I5 => ALUResult_out_OBUF(15),
      O => \Dout_reg[30]_0\(15)
    );
\Dout[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(16),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(16),
      I5 => ALUResult_out_OBUF(16),
      O => \Dout_reg[30]_0\(16)
    );
\Dout[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(17),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(17),
      I5 => ALUResult_out_OBUF(17),
      O => \Dout_reg[30]_0\(17)
    );
\Dout[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(18),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(18),
      I5 => ALUResult_out_OBUF(18),
      O => \Dout_reg[30]_0\(18)
    );
\Dout[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(19),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(19),
      I5 => ALUResult_out_OBUF(19),
      O => \Dout_reg[30]_0\(19)
    );
\Dout[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(1),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(1),
      I5 => ALUResult_out_OBUF(1),
      O => \Dout_reg[30]_0\(1)
    );
\Dout[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(20),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(20),
      I5 => ALUResult_out_OBUF(20),
      O => \Dout_reg[30]_0\(20)
    );
\Dout[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(21),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(21),
      I5 => ALUResult_out_OBUF(21),
      O => \Dout_reg[30]_0\(21)
    );
\Dout[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(22),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(22),
      I5 => ALUResult_out_OBUF(22),
      O => \Dout_reg[30]_0\(22)
    );
\Dout[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(23),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(23),
      I5 => ALUResult_out_OBUF(23),
      O => \Dout_reg[30]_0\(23)
    );
\Dout[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(24),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(24),
      I5 => ALUResult_out_OBUF(24),
      O => \Dout_reg[30]_0\(24)
    );
\Dout[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(25),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(25),
      I5 => ALUResult_out_OBUF(25),
      O => \Dout_reg[30]_0\(25)
    );
\Dout[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(26),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(26),
      I5 => ALUResult_out_OBUF(26),
      O => \Dout_reg[30]_0\(26)
    );
\Dout[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(27),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(27),
      I5 => ALUResult_out_OBUF(27),
      O => \Dout_reg[30]_0\(27)
    );
\Dout[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(28),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(28),
      I5 => ALUResult_out_OBUF(28),
      O => \Dout_reg[30]_0\(28)
    );
\Dout[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(29),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(29),
      I5 => ALUResult_out_OBUF(29),
      O => \Dout_reg[30]_0\(29)
    );
\Dout[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(2),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(2),
      I5 => ALUResult_out_OBUF(2),
      O => \Dout_reg[30]_0\(2)
    );
\Dout[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(30),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(30),
      I5 => ALUResult_out_OBUF(30),
      O => \Dout_reg[30]_0\(30)
    );
\Dout[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(3),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(3),
      I5 => ALUResult_out_OBUF(3),
      O => \Dout_reg[30]_0\(3)
    );
\Dout[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(4),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(4),
      I5 => ALUResult_out_OBUF(4),
      O => \Dout_reg[30]_0\(4)
    );
\Dout[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(5),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(5),
      I5 => ALUResult_out_OBUF(5),
      O => \Dout_reg[30]_0\(5)
    );
\Dout[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(6),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(6),
      I5 => ALUResult_out_OBUF(6),
      O => \Dout_reg[30]_0\(6)
    );
\Dout[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(7),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(7),
      I5 => ALUResult_out_OBUF(7),
      O => \Dout_reg[30]_0\(7)
    );
\Dout[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(8),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(8),
      I5 => ALUResult_out_OBUF(8),
      O => \Dout_reg[30]_0\(8)
    );
\Dout[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEF0F2FF0E00020"
    )
        port map (
      I0 => \^q\(9),
      I1 => \Dout_reg[30]_1\(1),
      I2 => \Dout_reg[30]_2\,
      I3 => \Dout_reg[30]_1\(0),
      I4 => Result_out_OBUF(9),
      I5 => ALUResult_out_OBUF(9),
      O => \Dout_reg[30]_0\(9)
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(0),
      Q => \^q\(0),
      R => RESET_IBUF
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(10),
      Q => \^q\(10),
      R => RESET_IBUF
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(11),
      Q => \^q\(11),
      R => RESET_IBUF
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(12),
      Q => \^q\(12),
      R => RESET_IBUF
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(13),
      Q => \^q\(13),
      R => RESET_IBUF
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(14),
      Q => \^q\(14),
      R => RESET_IBUF
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(15),
      Q => \^q\(15),
      R => RESET_IBUF
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(16),
      Q => \^q\(16),
      R => RESET_IBUF
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(17),
      Q => \^q\(17),
      R => RESET_IBUF
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(18),
      Q => \^q\(18),
      R => RESET_IBUF
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(19),
      Q => \^q\(19),
      R => RESET_IBUF
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(1),
      Q => \^q\(1),
      R => RESET_IBUF
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(20),
      Q => \^q\(20),
      R => RESET_IBUF
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(21),
      Q => \^q\(21),
      R => RESET_IBUF
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(22),
      Q => \^q\(22),
      R => RESET_IBUF
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(23),
      Q => \^q\(23),
      R => RESET_IBUF
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(24),
      Q => \^q\(24),
      R => RESET_IBUF
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(25),
      Q => \^q\(25),
      R => RESET_IBUF
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(26),
      Q => \^q\(26),
      R => RESET_IBUF
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(27),
      Q => \^q\(27),
      R => RESET_IBUF
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(28),
      Q => \^q\(28),
      R => RESET_IBUF
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(29),
      Q => \^q\(29),
      R => RESET_IBUF
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(2),
      Q => \^q\(2),
      R => RESET_IBUF
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(30),
      Q => \^q\(30),
      R => RESET_IBUF
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(31),
      Q => \^q\(31),
      R => RESET_IBUF
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(3),
      Q => \^q\(3),
      R => RESET_IBUF
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(4),
      Q => \^q\(4),
      R => RESET_IBUF
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(5),
      Q => \^q\(5),
      R => RESET_IBUF
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(6),
      Q => \^q\(6),
      R => RESET_IBUF
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(7),
      Q => \^q\(7),
      R => RESET_IBUF
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(8),
      Q => \^q\(8),
      R => RESET_IBUF
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(9),
      Q => \^q\(9),
      R => RESET_IBUF
    );
\WriteData_out_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q\(0),
      I1 => \Dout_reg[0]_0\,
      I2 => DATA_OUT2(0),
      O => \Dout_reg[2]_0\(0)
    );
\WriteData_out_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \Dout_reg[0]_0\,
      I2 => DATA_OUT2(1),
      O => \Dout_reg[2]_0\(1)
    );
\WriteData_out_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => \^q\(2),
      I1 => \Dout_reg[0]_0\,
      I2 => DATA_OUT2(2),
      O => \Dout_reg[2]_0\(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_6 is
  port (
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    RESET_IBUF : in STD_LOGIC;
    ALUResult_out_OBUF : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_6 : entity is "REGrwe_n";
end REGrwe_n_6;

architecture STRUCTURE of REGrwe_n_6 is
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(0),
      Q => Q(0),
      R => RESET_IBUF
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(10),
      Q => Q(10),
      R => RESET_IBUF
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(11),
      Q => Q(11),
      R => RESET_IBUF
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(12),
      Q => Q(12),
      R => RESET_IBUF
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(13),
      Q => Q(13),
      R => RESET_IBUF
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(14),
      Q => Q(14),
      R => RESET_IBUF
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(15),
      Q => Q(15),
      R => RESET_IBUF
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(16),
      Q => Q(16),
      R => RESET_IBUF
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(17),
      Q => Q(17),
      R => RESET_IBUF
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(18),
      Q => Q(18),
      R => RESET_IBUF
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(19),
      Q => Q(19),
      R => RESET_IBUF
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(1),
      Q => Q(1),
      R => RESET_IBUF
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(20),
      Q => Q(20),
      R => RESET_IBUF
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(21),
      Q => Q(21),
      R => RESET_IBUF
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(22),
      Q => Q(22),
      R => RESET_IBUF
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(23),
      Q => Q(23),
      R => RESET_IBUF
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(24),
      Q => Q(24),
      R => RESET_IBUF
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(25),
      Q => Q(25),
      R => RESET_IBUF
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(26),
      Q => Q(26),
      R => RESET_IBUF
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(27),
      Q => Q(27),
      R => RESET_IBUF
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(28),
      Q => Q(28),
      R => RESET_IBUF
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(29),
      Q => Q(29),
      R => RESET_IBUF
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(2),
      Q => Q(2),
      R => RESET_IBUF
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(30),
      Q => Q(30),
      R => RESET_IBUF
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(31),
      Q => Q(31),
      R => RESET_IBUF
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(3),
      Q => Q(3),
      R => RESET_IBUF
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(4),
      Q => Q(4),
      R => RESET_IBUF
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(5),
      Q => Q(5),
      R => RESET_IBUF
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(6),
      Q => Q(6),
      R => RESET_IBUF
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(7),
      Q => Q(7),
      R => RESET_IBUF
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(8),
      Q => Q(8),
      R => RESET_IBUF
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_out_OBUF(9),
      Q => Q(9),
      R => RESET_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_7 is
  port (
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    RESET_IBUF : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_7 : entity is "REGrwe_n";
end REGrwe_n_7;

architecture STRUCTURE of REGrwe_n_7 is
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(0),
      Q => Q(0),
      R => RESET_IBUF
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(10),
      Q => Q(10),
      R => RESET_IBUF
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(11),
      Q => Q(11),
      R => RESET_IBUF
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(12),
      Q => Q(12),
      R => RESET_IBUF
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(13),
      Q => Q(13),
      R => RESET_IBUF
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(14),
      Q => Q(14),
      R => RESET_IBUF
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(15),
      Q => Q(15),
      R => RESET_IBUF
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(16),
      Q => Q(16),
      R => RESET_IBUF
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(17),
      Q => Q(17),
      R => RESET_IBUF
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(18),
      Q => Q(18),
      R => RESET_IBUF
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(19),
      Q => Q(19),
      R => RESET_IBUF
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(1),
      Q => Q(1),
      R => RESET_IBUF
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(20),
      Q => Q(20),
      R => RESET_IBUF
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(21),
      Q => Q(21),
      R => RESET_IBUF
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(22),
      Q => Q(22),
      R => RESET_IBUF
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(23),
      Q => Q(23),
      R => RESET_IBUF
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(24),
      Q => Q(24),
      R => RESET_IBUF
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(25),
      Q => Q(25),
      R => RESET_IBUF
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(26),
      Q => Q(26),
      R => RESET_IBUF
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(27),
      Q => Q(27),
      R => RESET_IBUF
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(28),
      Q => Q(28),
      R => RESET_IBUF
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(29),
      Q => Q(29),
      R => RESET_IBUF
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(2),
      Q => Q(2),
      R => RESET_IBUF
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(30),
      Q => Q(30),
      R => RESET_IBUF
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(31),
      Q => Q(31),
      R => RESET_IBUF
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(3),
      Q => Q(3),
      R => RESET_IBUF
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(4),
      Q => Q(4),
      R => RESET_IBUF
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(5),
      Q => Q(5),
      R => RESET_IBUF
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(6),
      Q => Q(6),
      R => RESET_IBUF
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(7),
      Q => Q(7),
      R => RESET_IBUF
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(8),
      Q => Q(8),
      R => RESET_IBUF
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(9),
      Q => Q(9),
      R => RESET_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \REGrwe_n__parameterized1\ is
  port (
    Flags_in : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \Dout_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    ALUControl_out_OBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]_0\ : in STD_LOGIC;
    \Dout_reg[3]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]_2\ : in STD_LOGIC;
    FlagsWrite_in : in STD_LOGIC;
    \Dout_reg[1]_0\ : in STD_LOGIC;
    \Dout_reg[1]_1\ : in STD_LOGIC;
    \Dout_reg[1]_2\ : in STD_LOGIC;
    \Dout_reg[1]_3\ : in STD_LOGIC;
    RESET_IBUF : in STD_LOGIC;
    CLK_IBUF_BUFG : in STD_LOGIC;
    \Dout_reg[0]_1\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \REGrwe_n__parameterized1\ : entity is "REGrwe_n";
end \REGrwe_n__parameterized1\;

architecture STRUCTURE of \REGrwe_n__parameterized1\ is
  signal \Dout[1]_i_1_n_0\ : STD_LOGIC;
  signal \Dout[3]_i_1_n_0\ : STD_LOGIC;
  signal \^flags_in\ : STD_LOGIC_VECTOR ( 1 downto 0 );
begin
  Flags_in(1 downto 0) <= \^flags_in\(1 downto 0);
\Dout[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004FFFF00040000"
    )
        port map (
      I0 => \Dout_reg[1]_0\,
      I1 => \Dout_reg[1]_1\,
      I2 => \Dout_reg[1]_2\,
      I3 => \Dout_reg[1]_3\,
      I4 => FlagsWrite_in,
      I5 => \^flags_in\(0),
      O => \Dout[1]_i_1_n_0\
    );
\Dout[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0045FFFF00450000"
    )
        port map (
      I0 => ALUControl_out_OBUF(0),
      I1 => \Dout_reg[3]_0\,
      I2 => \Dout_reg[3]_1\(0),
      I3 => \Dout_reg[3]_2\,
      I4 => FlagsWrite_in,
      I5 => \^flags_in\(1),
      O => \Dout[3]_i_1_n_0\
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout_reg[0]_1\,
      Q => \Dout_reg[0]_0\(0),
      R => RESET_IBUF
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout[1]_i_1_n_0\,
      Q => \^flags_in\(0),
      R => RESET_IBUF
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \Dout[3]_i_1_n_0\,
      Q => \^flags_in\(1),
      R => RESET_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ROM_ARRAY is
  port (
    D : out STD_LOGIC_VECTOR ( 11 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 )
  );
end ROM_ARRAY;

architecture STRUCTURE of ROM_ARRAY is
begin
\Dout[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0401005304510140"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(3),
      I3 => Q(2),
      I4 => Q(0),
      I5 => Q(1),
      O => D(3)
    );
\Dout[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0051050200005500"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(0),
      I3 => Q(3),
      I4 => Q(2),
      I5 => Q(1),
      O => D(4)
    );
\Dout[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444400004444220"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(1),
      I3 => Q(0),
      I4 => Q(3),
      I5 => Q(2),
      O => D(5)
    );
\Dout[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004044440000200"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(2),
      I5 => Q(3),
      O => D(6)
    );
\Dout[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4100501041111311"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(3),
      I3 => Q(1),
      I4 => Q(2),
      I5 => Q(0),
      O => D(0)
    );
\Dout[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1104100711074007"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(3),
      I3 => Q(2),
      I4 => Q(1),
      I5 => Q(0),
      O => D(7)
    );
\Dout[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5554440401145777"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(1),
      I3 => Q(0),
      I4 => Q(3),
      I5 => Q(2),
      O => D(8)
    );
\Dout[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1404151714175447"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(3),
      I3 => Q(2),
      I4 => Q(1),
      I5 => Q(0),
      O => D(9)
    );
\Dout[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000005041424213"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(3),
      I3 => Q(1),
      I4 => Q(0),
      I5 => Q(2),
      O => D(1)
    );
\Dout[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555555455777"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(1),
      I3 => Q(0),
      I4 => Q(2),
      I5 => Q(3),
      O => D(10)
    );
\Dout[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5554545555475757"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(2),
      I3 => Q(0),
      I4 => Q(1),
      I5 => Q(3),
      O => D(11)
    );
\Dout[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000115319"
    )
        port map (
      I0 => Q(5),
      I1 => Q(0),
      I2 => Q(2),
      I3 => Q(1),
      I4 => Q(3),
      I5 => Q(4),
      O => D(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity SUBTRACTOR_n is
  port (
    \Dout_reg[30]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    O : out STD_LOGIC_VECTOR ( 0 to 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[30]_0\ : out STD_LOGIC;
    \Dout_reg[3]\ : out STD_LOGIC;
    \Dout_reg[7]\ : out STD_LOGIC;
    \Dout_reg[7]_0\ : out STD_LOGIC;
    \Dout_reg[11]\ : out STD_LOGIC;
    \Dout_reg[11]_0\ : out STD_LOGIC;
    \Dout_reg[11]_1\ : out STD_LOGIC;
    \Dout_reg[15]\ : out STD_LOGIC;
    \Dout_reg[15]_0\ : out STD_LOGIC;
    \Dout_reg[15]_1\ : out STD_LOGIC;
    \Dout_reg[19]\ : out STD_LOGIC;
    \Dout_reg[23]\ : out STD_LOGIC;
    \Dout_reg[23]_0\ : out STD_LOGIC;
    \Dout_reg[23]_1\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[4]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[8]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[12]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[16]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[20]_inst_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[24]_inst_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 0 to 0 );
    \ALUResult_out_OBUF[28]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[3]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]_1\ : in STD_LOGIC;
    S_Add_in : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \Dout_reg[3]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end SUBTRACTOR_n;

architecture STRUCTURE of SUBTRACTOR_n is
  signal \^o\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \S0_carry__0_n_0\ : STD_LOGIC;
  signal \S0_carry__0_n_1\ : STD_LOGIC;
  signal \S0_carry__0_n_2\ : STD_LOGIC;
  signal \S0_carry__0_n_3\ : STD_LOGIC;
  signal \S0_carry__1_n_0\ : STD_LOGIC;
  signal \S0_carry__1_n_1\ : STD_LOGIC;
  signal \S0_carry__1_n_2\ : STD_LOGIC;
  signal \S0_carry__1_n_3\ : STD_LOGIC;
  signal \S0_carry__2_n_0\ : STD_LOGIC;
  signal \S0_carry__2_n_1\ : STD_LOGIC;
  signal \S0_carry__2_n_2\ : STD_LOGIC;
  signal \S0_carry__2_n_3\ : STD_LOGIC;
  signal \S0_carry__3_n_0\ : STD_LOGIC;
  signal \S0_carry__3_n_1\ : STD_LOGIC;
  signal \S0_carry__3_n_2\ : STD_LOGIC;
  signal \S0_carry__3_n_3\ : STD_LOGIC;
  signal \S0_carry__4_n_0\ : STD_LOGIC;
  signal \S0_carry__4_n_1\ : STD_LOGIC;
  signal \S0_carry__4_n_2\ : STD_LOGIC;
  signal \S0_carry__4_n_3\ : STD_LOGIC;
  signal \S0_carry__5_n_0\ : STD_LOGIC;
  signal \S0_carry__5_n_1\ : STD_LOGIC;
  signal \S0_carry__5_n_2\ : STD_LOGIC;
  signal \S0_carry__5_n_3\ : STD_LOGIC;
  signal \S0_carry__6_n_1\ : STD_LOGIC;
  signal \S0_carry__6_n_2\ : STD_LOGIC;
  signal \S0_carry__6_n_3\ : STD_LOGIC;
  signal S0_carry_n_0 : STD_LOGIC;
  signal S0_carry_n_1 : STD_LOGIC;
  signal S0_carry_n_2 : STD_LOGIC;
  signal S0_carry_n_3 : STD_LOGIC;
  signal S_Sub_in : STD_LOGIC_VECTOR ( 23 downto 2 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[10]_inst_i_5\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[12]_inst_i_5\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[14]_inst_i_4\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[15]_inst_i_4\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[16]_inst_i_5\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[20]_inst_i_4\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[21]_inst_i_5\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[23]_inst_i_4\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[6]_inst_i_4\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[7]_inst_i_5\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[8]_inst_i_5\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \ALUResult_out_OBUF[9]_inst_i_5\ : label is "soft_lutpair8";
begin
  O(0) <= \^o\(0);
\ALUResult_out_OBUF[10]_inst_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => S_Sub_in(10),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(5),
      O => \Dout_reg[11]_1\
    );
\ALUResult_out_OBUF[12]_inst_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => S_Sub_in(12),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(6),
      O => \Dout_reg[15]\
    );
\ALUResult_out_OBUF[14]_inst_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => S_Sub_in(14),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(7),
      O => \Dout_reg[15]_0\
    );
\ALUResult_out_OBUF[15]_inst_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => S_Sub_in(15),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(8),
      O => \Dout_reg[15]_1\
    );
\ALUResult_out_OBUF[16]_inst_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => S_Sub_in(16),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(9),
      O => \Dout_reg[19]\
    );
\ALUResult_out_OBUF[20]_inst_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => S_Sub_in(20),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(10),
      O => \Dout_reg[23]\
    );
\ALUResult_out_OBUF[21]_inst_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => S_Sub_in(21),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(11),
      O => \Dout_reg[23]_0\
    );
\ALUResult_out_OBUF[23]_inst_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => S_Sub_in(23),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(12),
      O => \Dout_reg[23]_1\
    );
\ALUResult_out_OBUF[2]_inst_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => S_Sub_in(2),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(0),
      O => \Dout_reg[3]\
    );
\ALUResult_out_OBUF[6]_inst_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => S_Sub_in(6),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(1),
      O => \Dout_reg[7]\
    );
\ALUResult_out_OBUF[7]_inst_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => S_Sub_in(7),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(2),
      O => \Dout_reg[7]_0\
    );
\ALUResult_out_OBUF[8]_inst_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => S_Sub_in(8),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(3),
      O => \Dout_reg[11]\
    );
\ALUResult_out_OBUF[9]_inst_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => S_Sub_in(9),
      I1 => \Dout_reg[3]_1\,
      I2 => S_Add_in(4),
      O => \Dout_reg[11]_0\
    );
\Dout[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"606F6F60"
    )
        port map (
      I0 => \^o\(0),
      I1 => \Dout_reg[3]_0\(0),
      I2 => \Dout_reg[3]_1\,
      I3 => S_Add_in(13),
      I4 => \Dout_reg[3]_2\(0),
      O => \Dout_reg[30]_0\
    );
S0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => S0_carry_n_0,
      CO(2) => S0_carry_n_1,
      CO(1) => S0_carry_n_2,
      CO(0) => S0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => Q(3 downto 0),
      O(3) => \Dout_reg[30]\(2),
      O(2) => S_Sub_in(2),
      O(1 downto 0) => \Dout_reg[30]\(1 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\S0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => S0_carry_n_0,
      CO(3) => \S0_carry__0_n_0\,
      CO(2) => \S0_carry__0_n_1\,
      CO(1) => \S0_carry__0_n_2\,
      CO(0) => \S0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(7 downto 4),
      O(3 downto 2) => S_Sub_in(7 downto 6),
      O(1 downto 0) => \Dout_reg[30]\(4 downto 3),
      S(3 downto 0) => \ALUResult_out_OBUF[4]_inst_i_4\(3 downto 0)
    );
\S0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__0_n_0\,
      CO(3) => \S0_carry__1_n_0\,
      CO(2) => \S0_carry__1_n_1\,
      CO(1) => \S0_carry__1_n_2\,
      CO(0) => \S0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(11 downto 8),
      O(3) => \Dout_reg[30]\(5),
      O(2 downto 0) => S_Sub_in(10 downto 8),
      S(3 downto 0) => \ALUResult_out_OBUF[8]_inst_i_5_0\(3 downto 0)
    );
\S0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__1_n_0\,
      CO(3) => \S0_carry__2_n_0\,
      CO(2) => \S0_carry__2_n_1\,
      CO(1) => \S0_carry__2_n_2\,
      CO(0) => \S0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(15 downto 12),
      O(3 downto 2) => S_Sub_in(15 downto 14),
      O(1) => \Dout_reg[30]\(6),
      O(0) => S_Sub_in(12),
      S(3 downto 0) => \ALUResult_out_OBUF[12]_inst_i_5_0\(3 downto 0)
    );
\S0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__2_n_0\,
      CO(3) => \S0_carry__3_n_0\,
      CO(2) => \S0_carry__3_n_1\,
      CO(1) => \S0_carry__3_n_2\,
      CO(0) => \S0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(19 downto 16),
      O(3 downto 1) => \Dout_reg[30]\(9 downto 7),
      O(0) => S_Sub_in(16),
      S(3 downto 0) => \ALUResult_out_OBUF[16]_inst_i_5_0\(3 downto 0)
    );
\S0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__3_n_0\,
      CO(3) => \S0_carry__4_n_0\,
      CO(2) => \S0_carry__4_n_1\,
      CO(1) => \S0_carry__4_n_2\,
      CO(0) => \S0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(23 downto 20),
      O(3) => S_Sub_in(23),
      O(2) => \Dout_reg[30]\(10),
      O(1 downto 0) => S_Sub_in(21 downto 20),
      S(3 downto 0) => \ALUResult_out_OBUF[20]_inst_i_4_0\(3 downto 0)
    );
\S0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__4_n_0\,
      CO(3) => \S0_carry__5_n_0\,
      CO(2) => \S0_carry__5_n_1\,
      CO(1) => \S0_carry__5_n_2\,
      CO(0) => \S0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(27 downto 24),
      O(3 downto 0) => \Dout_reg[30]\(14 downto 11),
      S(3 downto 0) => \ALUResult_out_OBUF[24]_inst_i_3\(3 downto 0)
    );
\S0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__5_n_0\,
      CO(3) => CO(0),
      CO(2) => \S0_carry__6_n_1\,
      CO(1) => \S0_carry__6_n_2\,
      CO(0) => \S0_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => DI(0),
      DI(2 downto 0) => Q(30 downto 28),
      O(3) => \^o\(0),
      O(2 downto 0) => \Dout_reg[30]\(17 downto 15),
      S(3 downto 0) => \ALUResult_out_OBUF[28]_inst_i_4\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ADD_SUB_n is
  port (
    \Dout_reg[30]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[30]_0\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    \Dout_reg[30]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[30]_2\ : out STD_LOGIC;
    \Dout_reg[3]\ : out STD_LOGIC;
    \Dout_reg[7]\ : out STD_LOGIC;
    \Dout_reg[7]_0\ : out STD_LOGIC;
    \Dout_reg[11]\ : out STD_LOGIC;
    \Dout_reg[11]_0\ : out STD_LOGIC;
    \Dout_reg[11]_1\ : out STD_LOGIC;
    \Dout_reg[15]\ : out STD_LOGIC;
    \Dout_reg[15]_0\ : out STD_LOGIC;
    \Dout_reg[15]_1\ : out STD_LOGIC;
    \Dout_reg[19]\ : out STD_LOGIC;
    \Dout_reg[23]\ : out STD_LOGIC;
    \Dout_reg[23]_0\ : out STD_LOGIC;
    \Dout_reg[23]_1\ : out STD_LOGIC;
    \Dout_reg[30]_3\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[4]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[8]_inst_i_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[12]_inst_i_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[16]_inst_i_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[20]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[24]_inst_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 0 to 0 );
    \ALUResult_out_OBUF[28]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[0]_inst_i_6\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[4]_inst_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[8]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[12]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[16]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[20]_inst_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[24]_inst_i_3_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[28]_inst_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[3]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]_1\ : in STD_LOGIC;
    \Dout_reg[3]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end ADD_SUB_n;

architecture STRUCTURE of ADD_SUB_n is
  signal S_Add_in : STD_LOGIC_VECTOR ( 31 downto 2 );
  signal S_Sub_in : STD_LOGIC_VECTOR ( 31 to 31 );
begin
ADD_UNIT: entity work.ADDER_n_8
     port map (
      \ALUResult_out_OBUF[0]_inst_i_6\(3 downto 0) => \ALUResult_out_OBUF[0]_inst_i_6\(3 downto 0),
      \ALUResult_out_OBUF[12]_inst_i_5\(3 downto 0) => \ALUResult_out_OBUF[12]_inst_i_5_0\(3 downto 0),
      \ALUResult_out_OBUF[16]_inst_i_5\(3 downto 0) => \ALUResult_out_OBUF[16]_inst_i_5_0\(3 downto 0),
      \ALUResult_out_OBUF[20]_inst_i_4\(3 downto 0) => \ALUResult_out_OBUF[20]_inst_i_4_0\(3 downto 0),
      \ALUResult_out_OBUF[24]_inst_i_3\(3 downto 0) => \ALUResult_out_OBUF[24]_inst_i_3_0\(3 downto 0),
      \ALUResult_out_OBUF[28]_inst_i_4\(3 downto 0) => \ALUResult_out_OBUF[28]_inst_i_4_0\(3 downto 0),
      \ALUResult_out_OBUF[31]_inst_i_3\ => \Dout_reg[3]_1\,
      \ALUResult_out_OBUF[4]_inst_i_4\(3 downto 0) => \ALUResult_out_OBUF[4]_inst_i_4_0\(3 downto 0),
      \ALUResult_out_OBUF[8]_inst_i_5\(3 downto 0) => \ALUResult_out_OBUF[8]_inst_i_5_0\(3 downto 0),
      DI(0) => DI(0),
      \Dout_reg[30]\(17 downto 0) => \Dout_reg[30]_0\(17 downto 0),
      \Dout_reg[30]_0\(0) => \Dout_reg[30]_1\(0),
      \Dout_reg[30]_1\ => \Dout_reg[30]_3\,
      O(0) => S_Sub_in(31),
      Q(30 downto 0) => Q(30 downto 0),
      S_Add_in(13) => S_Add_in(31),
      S_Add_in(12) => S_Add_in(23),
      S_Add_in(11 downto 10) => S_Add_in(21 downto 20),
      S_Add_in(9 downto 7) => S_Add_in(16 downto 14),
      S_Add_in(6) => S_Add_in(12),
      S_Add_in(5 downto 1) => S_Add_in(10 downto 6),
      S_Add_in(0) => S_Add_in(2)
    );
SUB_UNIT: entity work.SUBTRACTOR_n
     port map (
      \ALUResult_out_OBUF[12]_inst_i_5_0\(3 downto 0) => \ALUResult_out_OBUF[12]_inst_i_5\(3 downto 0),
      \ALUResult_out_OBUF[16]_inst_i_5_0\(3 downto 0) => \ALUResult_out_OBUF[16]_inst_i_5\(3 downto 0),
      \ALUResult_out_OBUF[20]_inst_i_4_0\(3 downto 0) => \ALUResult_out_OBUF[20]_inst_i_4\(3 downto 0),
      \ALUResult_out_OBUF[24]_inst_i_3\(3 downto 0) => \ALUResult_out_OBUF[24]_inst_i_3\(3 downto 0),
      \ALUResult_out_OBUF[28]_inst_i_4\(3 downto 0) => \ALUResult_out_OBUF[28]_inst_i_4\(3 downto 0),
      \ALUResult_out_OBUF[4]_inst_i_4\(3 downto 0) => \ALUResult_out_OBUF[4]_inst_i_4\(3 downto 0),
      \ALUResult_out_OBUF[8]_inst_i_5_0\(3 downto 0) => \ALUResult_out_OBUF[8]_inst_i_5\(3 downto 0),
      CO(0) => CO(0),
      DI(0) => DI(0),
      \Dout_reg[11]\ => \Dout_reg[11]\,
      \Dout_reg[11]_0\ => \Dout_reg[11]_0\,
      \Dout_reg[11]_1\ => \Dout_reg[11]_1\,
      \Dout_reg[15]\ => \Dout_reg[15]\,
      \Dout_reg[15]_0\ => \Dout_reg[15]_0\,
      \Dout_reg[15]_1\ => \Dout_reg[15]_1\,
      \Dout_reg[19]\ => \Dout_reg[19]\,
      \Dout_reg[23]\ => \Dout_reg[23]\,
      \Dout_reg[23]_0\ => \Dout_reg[23]_0\,
      \Dout_reg[23]_1\ => \Dout_reg[23]_1\,
      \Dout_reg[30]\(17 downto 0) => \Dout_reg[30]\(17 downto 0),
      \Dout_reg[30]_0\ => \Dout_reg[30]_2\,
      \Dout_reg[3]\ => \Dout_reg[3]\,
      \Dout_reg[3]_0\(0) => \Dout_reg[3]_0\(0),
      \Dout_reg[3]_1\ => \Dout_reg[3]_1\,
      \Dout_reg[3]_2\(0) => \Dout_reg[3]_2\(0),
      \Dout_reg[7]\ => \Dout_reg[7]\,
      \Dout_reg[7]_0\ => \Dout_reg[7]_0\,
      O(0) => S_Sub_in(31),
      Q(30 downto 0) => Q(30 downto 0),
      S(3 downto 0) => S(3 downto 0),
      S_Add_in(13) => S_Add_in(31),
      S_Add_in(12) => S_Add_in(23),
      S_Add_in(11 downto 10) => S_Add_in(21 downto 20),
      S_Add_in(9 downto 7) => S_Add_in(16 downto 14),
      S_Add_in(6) => S_Add_in(12),
      S_Add_in(5 downto 1) => S_Add_in(10 downto 6),
      S_Add_in(0) => S_Add_in(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity CONTROL_UNIT is
  port (
    \FSM_onehot_current_state_reg[1]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_current_state_reg[13]\ : out STD_LOGIC;
    FlagsWrite_in : out STD_LOGIC;
    RegWrite_in : out STD_LOGIC;
    \FSM_onehot_current_state_reg[11]\ : out STD_LOGIC;
    Instr_out_OBUF : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \FSM_onehot_current_state_reg[2]\ : in STD_LOGIC;
    \FSM_onehot_current_state_reg[10]\ : in STD_LOGIC;
    \Dout_reg[0]\ : in STD_LOGIC;
    Flags_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    RESET_IBUF : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
end CONTROL_UNIT;

architecture STRUCTURE of CONTROL_UNIT is
begin
FSM_UNIT: entity work.FSM
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(4 downto 0) => D(4 downto 0),
      \Dout_reg[0]\ => \Dout_reg[0]\,
      E(0) => E(0),
      \FSM_onehot_current_state_reg[10]_0\ => \FSM_onehot_current_state_reg[10]\,
      \FSM_onehot_current_state_reg[11]_0\ => \FSM_onehot_current_state_reg[11]\,
      \FSM_onehot_current_state_reg[13]_0\ => \FSM_onehot_current_state_reg[13]\,
      \FSM_onehot_current_state_reg[1]_0\ => \FSM_onehot_current_state_reg[1]\,
      \FSM_onehot_current_state_reg[2]_0\ => \FSM_onehot_current_state_reg[2]\,
      FlagsWrite_in => FlagsWrite_in,
      Flags_in(0) => Flags_in(0),
      Instr_out_OBUF(2 downto 0) => Instr_out_OBUF(2 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      RESET_IBUF => RESET_IBUF,
      RegWrite_in => RegWrite_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGISTER_FILE_wR15 is
  port (
    DATA_OUT1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    DATA_OUT2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC;
    RegWrite_in : in STD_LOGIC;
    DATA_IN : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ADDRA : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ADDRD : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[1]\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end REGISTER_FILE_wR15;

architecture STRUCTURE of REGISTER_FILE_wR15 is
begin
REG_FILE_IN: entity work.REGISTER_FILE_n
     port map (
      ADDRA(2 downto 0) => ADDRA(2 downto 0),
      ADDRD(3 downto 0) => ADDRD(3 downto 0),
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      DATA_IN(31 downto 0) => DATA_IN(31 downto 0),
      DATA_OUT1(31 downto 0) => DATA_OUT1(31 downto 0),
      DATA_OUT2(31 downto 0) => DATA_OUT2(31 downto 0),
      \Dout_reg[1]\(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      RegWrite_in => RegWrite_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ALU_n is
  port (
    \Dout_reg[30]\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[30]_0\ : out STD_LOGIC_VECTOR ( 17 downto 0 );
    \Dout_reg[30]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[30]_2\ : out STD_LOGIC;
    \Dout_reg[3]\ : out STD_LOGIC;
    \Dout_reg[7]\ : out STD_LOGIC;
    \Dout_reg[7]_0\ : out STD_LOGIC;
    \Dout_reg[11]\ : out STD_LOGIC;
    \Dout_reg[11]_0\ : out STD_LOGIC;
    \Dout_reg[11]_1\ : out STD_LOGIC;
    \Dout_reg[15]\ : out STD_LOGIC;
    \Dout_reg[15]_0\ : out STD_LOGIC;
    \Dout_reg[15]_1\ : out STD_LOGIC;
    \Dout_reg[19]\ : out STD_LOGIC;
    \Dout_reg[23]\ : out STD_LOGIC;
    \Dout_reg[23]_0\ : out STD_LOGIC;
    \Dout_reg[23]_1\ : out STD_LOGIC;
    \Dout_reg[30]_3\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[4]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[8]_inst_i_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[12]_inst_i_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[16]_inst_i_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[20]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[24]_inst_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 0 to 0 );
    \ALUResult_out_OBUF[28]_inst_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[0]_inst_i_6\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[4]_inst_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[8]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[12]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[16]_inst_i_5_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[20]_inst_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[24]_inst_i_3_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \ALUResult_out_OBUF[28]_inst_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[3]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]_1\ : in STD_LOGIC;
    \Dout_reg[3]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end ALU_n;

architecture STRUCTURE of ALU_n is
begin
ADD_SUB: entity work.ADD_SUB_n
     port map (
      \ALUResult_out_OBUF[0]_inst_i_6\(3 downto 0) => \ALUResult_out_OBUF[0]_inst_i_6\(3 downto 0),
      \ALUResult_out_OBUF[12]_inst_i_5\(3 downto 0) => \ALUResult_out_OBUF[12]_inst_i_5\(3 downto 0),
      \ALUResult_out_OBUF[12]_inst_i_5_0\(3 downto 0) => \ALUResult_out_OBUF[12]_inst_i_5_0\(3 downto 0),
      \ALUResult_out_OBUF[16]_inst_i_5\(3 downto 0) => \ALUResult_out_OBUF[16]_inst_i_5\(3 downto 0),
      \ALUResult_out_OBUF[16]_inst_i_5_0\(3 downto 0) => \ALUResult_out_OBUF[16]_inst_i_5_0\(3 downto 0),
      \ALUResult_out_OBUF[20]_inst_i_4\(3 downto 0) => \ALUResult_out_OBUF[20]_inst_i_4\(3 downto 0),
      \ALUResult_out_OBUF[20]_inst_i_4_0\(3 downto 0) => \ALUResult_out_OBUF[20]_inst_i_4_0\(3 downto 0),
      \ALUResult_out_OBUF[24]_inst_i_3\(3 downto 0) => \ALUResult_out_OBUF[24]_inst_i_3\(3 downto 0),
      \ALUResult_out_OBUF[24]_inst_i_3_0\(3 downto 0) => \ALUResult_out_OBUF[24]_inst_i_3_0\(3 downto 0),
      \ALUResult_out_OBUF[28]_inst_i_4\(3 downto 0) => \ALUResult_out_OBUF[28]_inst_i_4\(3 downto 0),
      \ALUResult_out_OBUF[28]_inst_i_4_0\(3 downto 0) => \ALUResult_out_OBUF[28]_inst_i_4_0\(3 downto 0),
      \ALUResult_out_OBUF[4]_inst_i_4\(3 downto 0) => \ALUResult_out_OBUF[4]_inst_i_4\(3 downto 0),
      \ALUResult_out_OBUF[4]_inst_i_4_0\(3 downto 0) => \ALUResult_out_OBUF[4]_inst_i_4_0\(3 downto 0),
      \ALUResult_out_OBUF[8]_inst_i_5\(3 downto 0) => \ALUResult_out_OBUF[8]_inst_i_5\(3 downto 0),
      \ALUResult_out_OBUF[8]_inst_i_5_0\(3 downto 0) => \ALUResult_out_OBUF[8]_inst_i_5_0\(3 downto 0),
      CO(0) => CO(0),
      DI(0) => DI(0),
      \Dout_reg[11]\ => \Dout_reg[11]\,
      \Dout_reg[11]_0\ => \Dout_reg[11]_0\,
      \Dout_reg[11]_1\ => \Dout_reg[11]_1\,
      \Dout_reg[15]\ => \Dout_reg[15]\,
      \Dout_reg[15]_0\ => \Dout_reg[15]_0\,
      \Dout_reg[15]_1\ => \Dout_reg[15]_1\,
      \Dout_reg[19]\ => \Dout_reg[19]\,
      \Dout_reg[23]\ => \Dout_reg[23]\,
      \Dout_reg[23]_0\ => \Dout_reg[23]_0\,
      \Dout_reg[23]_1\ => \Dout_reg[23]_1\,
      \Dout_reg[30]\(17 downto 0) => \Dout_reg[30]\(17 downto 0),
      \Dout_reg[30]_0\(17 downto 0) => \Dout_reg[30]_0\(17 downto 0),
      \Dout_reg[30]_1\(0) => \Dout_reg[30]_1\(0),
      \Dout_reg[30]_2\ => \Dout_reg[30]_2\,
      \Dout_reg[30]_3\ => \Dout_reg[30]_3\,
      \Dout_reg[3]\ => \Dout_reg[3]\,
      \Dout_reg[3]_0\(0) => \Dout_reg[3]_0\(0),
      \Dout_reg[3]_1\ => \Dout_reg[3]_1\,
      \Dout_reg[3]_2\(0) => \Dout_reg[3]_2\(0),
      \Dout_reg[7]\ => \Dout_reg[7]\,
      \Dout_reg[7]_0\ => \Dout_reg[7]_0\,
      Q(30 downto 0) => Q(30 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity DATAPATH_MC_n is
  port (
    ALUControl_out_OBUF : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Result_out_OBUF : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ALUResult_out_OBUF : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[27]_rep__0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \Dout_reg[25]_rep\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \Dout_reg[31]\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[28]_rep\ : out STD_LOGIC;
    \Dout_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[15]_rep\ : out STD_LOGIC;
    \Dout_reg[31]_0\ : out STD_LOGIC_VECTOR ( 26 downto 0 );
    \Dout_reg[31]_rep\ : out STD_LOGIC;
    \Dout_reg[19]_rep\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \Dout_reg[30]\ : in STD_LOGIC;
    \FSM_onehot_current_state_reg[13]\ : in STD_LOGIC;
    CLK_IBUF_BUFG : in STD_LOGIC;
    RegWrite_in : in STD_LOGIC;
    RESET_IBUF : in STD_LOGIC;
    FlagsWrite_in : in STD_LOGIC;
    \Dout_reg[0]_0\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end DATAPATH_MC_n;

architecture STRUCTURE of DATAPATH_MC_n is
  signal ADDR : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \ADD_SUB/S_Add_in\ : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \ADD_SUB/S_Sub_in\ : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \^alucontrol_out_obuf\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ALUResult_in_regS : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^aluresult_out_obuf\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ALU_n_18 : STD_LOGIC;
  signal ALU_n_37 : STD_LOGIC;
  signal ALU_n_38 : STD_LOGIC;
  signal ALU_n_39 : STD_LOGIC;
  signal ALU_n_40 : STD_LOGIC;
  signal ALU_n_41 : STD_LOGIC;
  signal ALU_n_42 : STD_LOGIC;
  signal ALU_n_43 : STD_LOGIC;
  signal ALU_n_44 : STD_LOGIC;
  signal ALU_n_45 : STD_LOGIC;
  signal ALU_n_46 : STD_LOGIC;
  signal ALU_n_47 : STD_LOGIC;
  signal ALU_n_48 : STD_LOGIC;
  signal ALU_n_49 : STD_LOGIC;
  signal ALU_n_50 : STD_LOGIC;
  signal ALU_n_51 : STD_LOGIC;
  signal ALU_n_52 : STD_LOGIC;
  signal \^dout_reg[0]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^dout_reg[27]_rep__0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^dout_reg[31]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ExtImm : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ExtImm_regI : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Flags_in : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal Instr_in : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal Instr_out_OBUF : STD_LOGIC_VECTOR ( 21 downto 7 );
  signal PCPlus4 : STD_LOGIC_VECTOR ( 31 downto 2 );
  signal PCPlus4_regPCP4 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal PCPlus8 : STD_LOGIC_VECTOR ( 31 downto 3 );
  signal PC_REG_n_0 : STD_LOGIC;
  signal PC_REG_n_1 : STD_LOGIC;
  signal PC_REG_n_10 : STD_LOGIC;
  signal PC_REG_n_11 : STD_LOGIC;
  signal PC_REG_n_12 : STD_LOGIC;
  signal PC_REG_n_13 : STD_LOGIC;
  signal PC_REG_n_14 : STD_LOGIC;
  signal PC_REG_n_15 : STD_LOGIC;
  signal PC_REG_n_16 : STD_LOGIC;
  signal PC_REG_n_17 : STD_LOGIC;
  signal PC_REG_n_18 : STD_LOGIC;
  signal PC_REG_n_19 : STD_LOGIC;
  signal PC_REG_n_2 : STD_LOGIC;
  signal PC_REG_n_20 : STD_LOGIC;
  signal PC_REG_n_21 : STD_LOGIC;
  signal PC_REG_n_22 : STD_LOGIC;
  signal PC_REG_n_23 : STD_LOGIC;
  signal PC_REG_n_3 : STD_LOGIC;
  signal PC_REG_n_4 : STD_LOGIC;
  signal PC_REG_n_5 : STD_LOGIC;
  signal PC_REG_n_6 : STD_LOGIC;
  signal PC_REG_n_7 : STD_LOGIC;
  signal PC_REG_n_8 : STD_LOGIC;
  signal PC_REG_n_9 : STD_LOGIC;
  signal PC_next : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal RA1_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal RA2_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal RA3_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal REG_A_n_0 : STD_LOGIC;
  signal REG_A_n_1 : STD_LOGIC;
  signal REG_A_n_2 : STD_LOGIC;
  signal REG_B_n_0 : STD_LOGIC;
  signal REG_B_n_1 : STD_LOGIC;
  signal REG_B_n_10 : STD_LOGIC;
  signal REG_B_n_11 : STD_LOGIC;
  signal REG_B_n_12 : STD_LOGIC;
  signal REG_B_n_13 : STD_LOGIC;
  signal REG_B_n_14 : STD_LOGIC;
  signal REG_B_n_15 : STD_LOGIC;
  signal REG_B_n_16 : STD_LOGIC;
  signal REG_B_n_17 : STD_LOGIC;
  signal REG_B_n_18 : STD_LOGIC;
  signal REG_B_n_19 : STD_LOGIC;
  signal REG_B_n_2 : STD_LOGIC;
  signal REG_B_n_20 : STD_LOGIC;
  signal REG_B_n_21 : STD_LOGIC;
  signal REG_B_n_22 : STD_LOGIC;
  signal REG_B_n_23 : STD_LOGIC;
  signal REG_B_n_24 : STD_LOGIC;
  signal REG_B_n_3 : STD_LOGIC;
  signal REG_B_n_4 : STD_LOGIC;
  signal REG_B_n_5 : STD_LOGIC;
  signal REG_B_n_57 : STD_LOGIC;
  signal REG_B_n_58 : STD_LOGIC;
  signal REG_B_n_59 : STD_LOGIC;
  signal REG_B_n_6 : STD_LOGIC;
  signal REG_B_n_60 : STD_LOGIC;
  signal REG_B_n_61 : STD_LOGIC;
  signal REG_B_n_62 : STD_LOGIC;
  signal REG_B_n_63 : STD_LOGIC;
  signal REG_B_n_64 : STD_LOGIC;
  signal REG_B_n_65 : STD_LOGIC;
  signal REG_B_n_66 : STD_LOGIC;
  signal REG_B_n_67 : STD_LOGIC;
  signal REG_B_n_68 : STD_LOGIC;
  signal REG_B_n_7 : STD_LOGIC;
  signal REG_B_n_8 : STD_LOGIC;
  signal REG_B_n_9 : STD_LOGIC;
  signal REG_FILE_n_0 : STD_LOGIC;
  signal REG_FILE_n_1 : STD_LOGIC;
  signal REG_FILE_n_10 : STD_LOGIC;
  signal REG_FILE_n_11 : STD_LOGIC;
  signal REG_FILE_n_12 : STD_LOGIC;
  signal REG_FILE_n_13 : STD_LOGIC;
  signal REG_FILE_n_14 : STD_LOGIC;
  signal REG_FILE_n_15 : STD_LOGIC;
  signal REG_FILE_n_16 : STD_LOGIC;
  signal REG_FILE_n_17 : STD_LOGIC;
  signal REG_FILE_n_18 : STD_LOGIC;
  signal REG_FILE_n_19 : STD_LOGIC;
  signal REG_FILE_n_2 : STD_LOGIC;
  signal REG_FILE_n_20 : STD_LOGIC;
  signal REG_FILE_n_21 : STD_LOGIC;
  signal REG_FILE_n_22 : STD_LOGIC;
  signal REG_FILE_n_23 : STD_LOGIC;
  signal REG_FILE_n_24 : STD_LOGIC;
  signal REG_FILE_n_25 : STD_LOGIC;
  signal REG_FILE_n_26 : STD_LOGIC;
  signal REG_FILE_n_27 : STD_LOGIC;
  signal REG_FILE_n_28 : STD_LOGIC;
  signal REG_FILE_n_29 : STD_LOGIC;
  signal REG_FILE_n_3 : STD_LOGIC;
  signal REG_FILE_n_30 : STD_LOGIC;
  signal REG_FILE_n_31 : STD_LOGIC;
  signal REG_FILE_n_32 : STD_LOGIC;
  signal REG_FILE_n_33 : STD_LOGIC;
  signal REG_FILE_n_34 : STD_LOGIC;
  signal REG_FILE_n_35 : STD_LOGIC;
  signal REG_FILE_n_36 : STD_LOGIC;
  signal REG_FILE_n_37 : STD_LOGIC;
  signal REG_FILE_n_38 : STD_LOGIC;
  signal REG_FILE_n_39 : STD_LOGIC;
  signal REG_FILE_n_4 : STD_LOGIC;
  signal REG_FILE_n_40 : STD_LOGIC;
  signal REG_FILE_n_41 : STD_LOGIC;
  signal REG_FILE_n_42 : STD_LOGIC;
  signal REG_FILE_n_43 : STD_LOGIC;
  signal REG_FILE_n_44 : STD_LOGIC;
  signal REG_FILE_n_45 : STD_LOGIC;
  signal REG_FILE_n_46 : STD_LOGIC;
  signal REG_FILE_n_47 : STD_LOGIC;
  signal REG_FILE_n_48 : STD_LOGIC;
  signal REG_FILE_n_49 : STD_LOGIC;
  signal REG_FILE_n_5 : STD_LOGIC;
  signal REG_FILE_n_50 : STD_LOGIC;
  signal REG_FILE_n_51 : STD_LOGIC;
  signal REG_FILE_n_52 : STD_LOGIC;
  signal REG_FILE_n_53 : STD_LOGIC;
  signal REG_FILE_n_54 : STD_LOGIC;
  signal REG_FILE_n_55 : STD_LOGIC;
  signal REG_FILE_n_56 : STD_LOGIC;
  signal REG_FILE_n_57 : STD_LOGIC;
  signal REG_FILE_n_58 : STD_LOGIC;
  signal REG_FILE_n_59 : STD_LOGIC;
  signal REG_FILE_n_6 : STD_LOGIC;
  signal REG_FILE_n_60 : STD_LOGIC;
  signal REG_FILE_n_61 : STD_LOGIC;
  signal REG_FILE_n_62 : STD_LOGIC;
  signal REG_FILE_n_63 : STD_LOGIC;
  signal REG_FILE_n_7 : STD_LOGIC;
  signal REG_FILE_n_8 : STD_LOGIC;
  signal REG_FILE_n_9 : STD_LOGIC;
  signal REG_IR_n_0 : STD_LOGIC;
  signal REG_IR_n_145 : STD_LOGIC;
  signal REG_IR_n_156 : STD_LOGIC;
  signal REG_IR_n_157 : STD_LOGIC;
  signal REG_IR_n_158 : STD_LOGIC;
  signal REG_IR_n_159 : STD_LOGIC;
  signal REG_IR_n_160 : STD_LOGIC;
  signal REG_IR_n_161 : STD_LOGIC;
  signal REG_IR_n_162 : STD_LOGIC;
  signal REG_IR_n_163 : STD_LOGIC;
  signal REG_IR_n_164 : STD_LOGIC;
  signal REG_IR_n_165 : STD_LOGIC;
  signal REG_IR_n_166 : STD_LOGIC;
  signal REG_IR_n_167 : STD_LOGIC;
  signal REG_IR_n_168 : STD_LOGIC;
  signal REG_IR_n_169 : STD_LOGIC;
  signal REG_IR_n_170 : STD_LOGIC;
  signal REG_IR_n_171 : STD_LOGIC;
  signal REG_IR_n_172 : STD_LOGIC;
  signal REG_IR_n_173 : STD_LOGIC;
  signal REG_IR_n_174 : STD_LOGIC;
  signal REG_IR_n_175 : STD_LOGIC;
  signal REG_IR_n_176 : STD_LOGIC;
  signal REG_IR_n_177 : STD_LOGIC;
  signal REG_IR_n_178 : STD_LOGIC;
  signal REG_IR_n_179 : STD_LOGIC;
  signal REG_IR_n_180 : STD_LOGIC;
  signal REG_IR_n_181 : STD_LOGIC;
  signal REG_IR_n_182 : STD_LOGIC;
  signal REG_IR_n_183 : STD_LOGIC;
  signal REG_IR_n_184 : STD_LOGIC;
  signal REG_IR_n_185 : STD_LOGIC;
  signal REG_IR_n_186 : STD_LOGIC;
  signal REG_IR_n_187 : STD_LOGIC;
  signal REG_IR_n_188 : STD_LOGIC;
  signal REG_IR_n_189 : STD_LOGIC;
  signal REG_IR_n_190 : STD_LOGIC;
  signal REG_IR_n_191 : STD_LOGIC;
  signal REG_IR_n_192 : STD_LOGIC;
  signal REG_IR_n_193 : STD_LOGIC;
  signal REG_IR_n_194 : STD_LOGIC;
  signal REG_IR_n_195 : STD_LOGIC;
  signal REG_IR_n_196 : STD_LOGIC;
  signal REG_IR_n_197 : STD_LOGIC;
  signal REG_IR_n_198 : STD_LOGIC;
  signal REG_IR_n_199 : STD_LOGIC;
  signal REG_IR_n_200 : STD_LOGIC;
  signal REG_IR_n_201 : STD_LOGIC;
  signal REG_IR_n_202 : STD_LOGIC;
  signal REG_IR_n_203 : STD_LOGIC;
  signal REG_IR_n_204 : STD_LOGIC;
  signal REG_IR_n_205 : STD_LOGIC;
  signal REG_IR_n_206 : STD_LOGIC;
  signal REG_IR_n_207 : STD_LOGIC;
  signal REG_IR_n_208 : STD_LOGIC;
  signal REG_IR_n_209 : STD_LOGIC;
  signal REG_IR_n_210 : STD_LOGIC;
  signal REG_IR_n_211 : STD_LOGIC;
  signal REG_IR_n_212 : STD_LOGIC;
  signal REG_IR_n_213 : STD_LOGIC;
  signal REG_IR_n_214 : STD_LOGIC;
  signal REG_IR_n_215 : STD_LOGIC;
  signal REG_IR_n_216 : STD_LOGIC;
  signal REG_IR_n_217 : STD_LOGIC;
  signal REG_IR_n_218 : STD_LOGIC;
  signal REG_IR_n_219 : STD_LOGIC;
  signal REG_IR_n_37 : STD_LOGIC;
  signal REG_IR_n_41 : STD_LOGIC;
  signal REG_IR_n_42 : STD_LOGIC;
  signal REG_IR_n_43 : STD_LOGIC;
  signal REG_IR_n_45 : STD_LOGIC;
  signal REG_IR_n_46 : STD_LOGIC;
  signal REG_IR_n_54 : STD_LOGIC;
  signal REG_I_n_0 : STD_LOGIC;
  signal REG_I_n_23 : STD_LOGIC;
  signal REG_I_n_24 : STD_LOGIC;
  signal REG_I_n_25 : STD_LOGIC;
  signal REG_I_n_26 : STD_LOGIC;
  signal REG_I_n_27 : STD_LOGIC;
  signal REG_I_n_28 : STD_LOGIC;
  signal REG_I_n_29 : STD_LOGIC;
  signal REG_WD_n_0 : STD_LOGIC;
  signal REG_WD_n_1 : STD_LOGIC;
  signal REG_WD_n_10 : STD_LOGIC;
  signal REG_WD_n_11 : STD_LOGIC;
  signal REG_WD_n_12 : STD_LOGIC;
  signal REG_WD_n_13 : STD_LOGIC;
  signal REG_WD_n_14 : STD_LOGIC;
  signal REG_WD_n_15 : STD_LOGIC;
  signal REG_WD_n_16 : STD_LOGIC;
  signal REG_WD_n_17 : STD_LOGIC;
  signal REG_WD_n_18 : STD_LOGIC;
  signal REG_WD_n_19 : STD_LOGIC;
  signal REG_WD_n_2 : STD_LOGIC;
  signal REG_WD_n_20 : STD_LOGIC;
  signal REG_WD_n_21 : STD_LOGIC;
  signal REG_WD_n_22 : STD_LOGIC;
  signal REG_WD_n_23 : STD_LOGIC;
  signal REG_WD_n_24 : STD_LOGIC;
  signal REG_WD_n_25 : STD_LOGIC;
  signal REG_WD_n_26 : STD_LOGIC;
  signal REG_WD_n_27 : STD_LOGIC;
  signal REG_WD_n_28 : STD_LOGIC;
  signal REG_WD_n_29 : STD_LOGIC;
  signal REG_WD_n_3 : STD_LOGIC;
  signal REG_WD_n_30 : STD_LOGIC;
  signal REG_WD_n_31 : STD_LOGIC;
  signal REG_WD_n_4 : STD_LOGIC;
  signal REG_WD_n_5 : STD_LOGIC;
  signal REG_WD_n_6 : STD_LOGIC;
  signal REG_WD_n_7 : STD_LOGIC;
  signal REG_WD_n_8 : STD_LOGIC;
  signal REG_WD_n_9 : STD_LOGIC;
  signal ReadData_in_regRD : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^result_out_obuf\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal SrcA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal SrcA_regA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal WD3_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal WriteData_regB : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
  ALUControl_out_OBUF(3 downto 0) <= \^alucontrol_out_obuf\(3 downto 0);
  ALUResult_out_OBUF(31 downto 0) <= \^aluresult_out_obuf\(31 downto 0);
  \Dout_reg[0]\(0) <= \^dout_reg[0]\(0);
  \Dout_reg[27]_rep__0\(2 downto 0) <= \^dout_reg[27]_rep__0\(2 downto 0);
  \Dout_reg[31]\(31 downto 0) <= \^dout_reg[31]\(31 downto 0);
  Result_out_OBUF(31 downto 0) <= \^result_out_obuf\(31 downto 0);
ALU: entity work.ALU_n
     port map (
      \ALUResult_out_OBUF[0]_inst_i_6\(3) => REG_IR_n_168,
      \ALUResult_out_OBUF[0]_inst_i_6\(2) => REG_IR_n_169,
      \ALUResult_out_OBUF[0]_inst_i_6\(1) => REG_IR_n_170,
      \ALUResult_out_OBUF[0]_inst_i_6\(0) => REG_IR_n_171,
      \ALUResult_out_OBUF[12]_inst_i_5\(3) => REG_IR_n_204,
      \ALUResult_out_OBUF[12]_inst_i_5\(2) => REG_IR_n_205,
      \ALUResult_out_OBUF[12]_inst_i_5\(1) => REG_IR_n_206,
      \ALUResult_out_OBUF[12]_inst_i_5\(0) => REG_IR_n_207,
      \ALUResult_out_OBUF[12]_inst_i_5_0\(3) => REG_IR_n_180,
      \ALUResult_out_OBUF[12]_inst_i_5_0\(2) => REG_IR_n_181,
      \ALUResult_out_OBUF[12]_inst_i_5_0\(1) => REG_IR_n_182,
      \ALUResult_out_OBUF[12]_inst_i_5_0\(0) => REG_IR_n_183,
      \ALUResult_out_OBUF[16]_inst_i_5\(3) => REG_IR_n_208,
      \ALUResult_out_OBUF[16]_inst_i_5\(2) => REG_IR_n_209,
      \ALUResult_out_OBUF[16]_inst_i_5\(1) => REG_IR_n_210,
      \ALUResult_out_OBUF[16]_inst_i_5\(0) => REG_IR_n_211,
      \ALUResult_out_OBUF[16]_inst_i_5_0\(3) => REG_IR_n_212,
      \ALUResult_out_OBUF[16]_inst_i_5_0\(2) => REG_IR_n_213,
      \ALUResult_out_OBUF[16]_inst_i_5_0\(1) => REG_IR_n_214,
      \ALUResult_out_OBUF[16]_inst_i_5_0\(0) => REG_IR_n_215,
      \ALUResult_out_OBUF[20]_inst_i_4\(3) => REG_IR_n_192,
      \ALUResult_out_OBUF[20]_inst_i_4\(2) => REG_IR_n_193,
      \ALUResult_out_OBUF[20]_inst_i_4\(1) => REG_IR_n_194,
      \ALUResult_out_OBUF[20]_inst_i_4\(0) => REG_IR_n_195,
      \ALUResult_out_OBUF[20]_inst_i_4_0\(3) => REG_IR_n_216,
      \ALUResult_out_OBUF[20]_inst_i_4_0\(2) => REG_IR_n_217,
      \ALUResult_out_OBUF[20]_inst_i_4_0\(1) => REG_IR_n_218,
      \ALUResult_out_OBUF[20]_inst_i_4_0\(0) => REG_IR_n_219,
      \ALUResult_out_OBUF[24]_inst_i_3\(3) => REG_IR_n_160,
      \ALUResult_out_OBUF[24]_inst_i_3\(2) => REG_IR_n_161,
      \ALUResult_out_OBUF[24]_inst_i_3\(1) => REG_IR_n_162,
      \ALUResult_out_OBUF[24]_inst_i_3\(0) => REG_IR_n_163,
      \ALUResult_out_OBUF[24]_inst_i_3_0\(3) => REG_IR_n_184,
      \ALUResult_out_OBUF[24]_inst_i_3_0\(2) => REG_IR_n_185,
      \ALUResult_out_OBUF[24]_inst_i_3_0\(1) => REG_IR_n_186,
      \ALUResult_out_OBUF[24]_inst_i_3_0\(0) => REG_IR_n_187,
      \ALUResult_out_OBUF[28]_inst_i_4\(3) => REG_IR_n_164,
      \ALUResult_out_OBUF[28]_inst_i_4\(2) => REG_IR_n_165,
      \ALUResult_out_OBUF[28]_inst_i_4\(1) => REG_IR_n_166,
      \ALUResult_out_OBUF[28]_inst_i_4\(0) => REG_IR_n_167,
      \ALUResult_out_OBUF[28]_inst_i_4_0\(3) => REG_IR_n_188,
      \ALUResult_out_OBUF[28]_inst_i_4_0\(2) => REG_IR_n_189,
      \ALUResult_out_OBUF[28]_inst_i_4_0\(1) => REG_IR_n_190,
      \ALUResult_out_OBUF[28]_inst_i_4_0\(0) => REG_IR_n_191,
      \ALUResult_out_OBUF[4]_inst_i_4\(3) => REG_IR_n_196,
      \ALUResult_out_OBUF[4]_inst_i_4\(2) => REG_IR_n_197,
      \ALUResult_out_OBUF[4]_inst_i_4\(1) => REG_IR_n_198,
      \ALUResult_out_OBUF[4]_inst_i_4\(0) => REG_IR_n_199,
      \ALUResult_out_OBUF[4]_inst_i_4_0\(3) => REG_IR_n_172,
      \ALUResult_out_OBUF[4]_inst_i_4_0\(2) => REG_IR_n_173,
      \ALUResult_out_OBUF[4]_inst_i_4_0\(1) => REG_IR_n_174,
      \ALUResult_out_OBUF[4]_inst_i_4_0\(0) => REG_IR_n_175,
      \ALUResult_out_OBUF[8]_inst_i_5\(3) => REG_IR_n_200,
      \ALUResult_out_OBUF[8]_inst_i_5\(2) => REG_IR_n_201,
      \ALUResult_out_OBUF[8]_inst_i_5\(1) => REG_IR_n_202,
      \ALUResult_out_OBUF[8]_inst_i_5\(0) => REG_IR_n_203,
      \ALUResult_out_OBUF[8]_inst_i_5_0\(3) => REG_IR_n_176,
      \ALUResult_out_OBUF[8]_inst_i_5_0\(2) => REG_IR_n_177,
      \ALUResult_out_OBUF[8]_inst_i_5_0\(1) => REG_IR_n_178,
      \ALUResult_out_OBUF[8]_inst_i_5_0\(0) => REG_IR_n_179,
      CO(0) => ALU_n_18,
      DI(0) => REG_A_n_2,
      \Dout_reg[11]\ => ALU_n_42,
      \Dout_reg[11]_0\ => ALU_n_43,
      \Dout_reg[11]_1\ => ALU_n_44,
      \Dout_reg[15]\ => ALU_n_45,
      \Dout_reg[15]_0\ => ALU_n_46,
      \Dout_reg[15]_1\ => ALU_n_47,
      \Dout_reg[19]\ => ALU_n_48,
      \Dout_reg[23]\ => ALU_n_49,
      \Dout_reg[23]_0\ => ALU_n_50,
      \Dout_reg[23]_1\ => ALU_n_51,
      \Dout_reg[30]\(17 downto 11) => \ADD_SUB/S_Sub_in\(30 downto 24),
      \Dout_reg[30]\(10) => \ADD_SUB/S_Sub_in\(22),
      \Dout_reg[30]\(9 downto 7) => \ADD_SUB/S_Sub_in\(19 downto 17),
      \Dout_reg[30]\(6) => \ADD_SUB/S_Sub_in\(13),
      \Dout_reg[30]\(5) => \ADD_SUB/S_Sub_in\(11),
      \Dout_reg[30]\(4 downto 2) => \ADD_SUB/S_Sub_in\(5 downto 3),
      \Dout_reg[30]\(1 downto 0) => \ADD_SUB/S_Sub_in\(1 downto 0),
      \Dout_reg[30]_0\(17 downto 11) => \ADD_SUB/S_Add_in\(30 downto 24),
      \Dout_reg[30]_0\(10) => \ADD_SUB/S_Add_in\(22),
      \Dout_reg[30]_0\(9 downto 7) => \ADD_SUB/S_Add_in\(19 downto 17),
      \Dout_reg[30]_0\(6) => \ADD_SUB/S_Add_in\(13),
      \Dout_reg[30]_0\(5) => \ADD_SUB/S_Add_in\(11),
      \Dout_reg[30]_0\(4 downto 2) => \ADD_SUB/S_Add_in\(5 downto 3),
      \Dout_reg[30]_0\(1 downto 0) => \ADD_SUB/S_Add_in\(1 downto 0),
      \Dout_reg[30]_1\(0) => ALU_n_37,
      \Dout_reg[30]_2\ => ALU_n_38,
      \Dout_reg[30]_3\ => ALU_n_52,
      \Dout_reg[3]\ => ALU_n_39,
      \Dout_reg[3]_0\(0) => REG_A_n_0,
      \Dout_reg[3]_1\ => \^alucontrol_out_obuf\(0),
      \Dout_reg[3]_2\(0) => REG_A_n_1,
      \Dout_reg[7]\ => ALU_n_40,
      \Dout_reg[7]_0\ => ALU_n_41,
      Q(30 downto 0) => SrcA_regA(30 downto 0),
      S(3) => REG_IR_n_156,
      S(2) => REG_IR_n_157,
      S(1) => REG_IR_n_158,
      S(0) => REG_IR_n_159
    );
DATA_MEMORY: entity work.RAM_ARRAY
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      \Dout_reg[0]\ => \^dout_reg[27]_rep__0\(1),
      \Dout_reg[31]\(31 downto 0) => ALUResult_in_regS(31 downto 0),
      Q(4 downto 0) => ADDR(4 downto 0),
      RAM_reg_0(31) => REG_WD_n_0,
      RAM_reg_0(30) => REG_WD_n_1,
      RAM_reg_0(29) => REG_WD_n_2,
      RAM_reg_0(28) => REG_WD_n_3,
      RAM_reg_0(27) => REG_WD_n_4,
      RAM_reg_0(26) => REG_WD_n_5,
      RAM_reg_0(25) => REG_WD_n_6,
      RAM_reg_0(24) => REG_WD_n_7,
      RAM_reg_0(23) => REG_WD_n_8,
      RAM_reg_0(22) => REG_WD_n_9,
      RAM_reg_0(21) => REG_WD_n_10,
      RAM_reg_0(20) => REG_WD_n_11,
      RAM_reg_0(19) => REG_WD_n_12,
      RAM_reg_0(18) => REG_WD_n_13,
      RAM_reg_0(17) => REG_WD_n_14,
      RAM_reg_0(16) => REG_WD_n_15,
      RAM_reg_0(15) => REG_WD_n_16,
      RAM_reg_0(14) => REG_WD_n_17,
      RAM_reg_0(13) => REG_WD_n_18,
      RAM_reg_0(12) => REG_WD_n_19,
      RAM_reg_0(11) => REG_WD_n_20,
      RAM_reg_0(10) => REG_WD_n_21,
      RAM_reg_0(9) => REG_WD_n_22,
      RAM_reg_0(8) => REG_WD_n_23,
      RAM_reg_0(7) => REG_WD_n_24,
      RAM_reg_0(6) => REG_WD_n_25,
      RAM_reg_0(5) => REG_WD_n_26,
      RAM_reg_0(4) => REG_WD_n_27,
      RAM_reg_0(3) => REG_WD_n_28,
      RAM_reg_0(2) => REG_WD_n_29,
      RAM_reg_0(1) => REG_WD_n_30,
      RAM_reg_0(0) => REG_WD_n_31,
      RAM_reg_1(0) => Q(2),
      RESET_IBUF => RESET_IBUF,
      ReadData_in_regRD(31 downto 0) => ReadData_in_regRD(31 downto 0),
      Result_out_OBUF(31 downto 0) => \^result_out_obuf\(31 downto 0)
    );
INC_4_1: entity work.ADDER_n
     port map (
      DATA_OUT2(28) => REG_FILE_n_32,
      DATA_OUT2(27) => REG_FILE_n_33,
      DATA_OUT2(26) => REG_FILE_n_34,
      DATA_OUT2(25) => REG_FILE_n_35,
      DATA_OUT2(24) => REG_FILE_n_36,
      DATA_OUT2(23) => REG_FILE_n_37,
      DATA_OUT2(22) => REG_FILE_n_38,
      DATA_OUT2(21) => REG_FILE_n_39,
      DATA_OUT2(20) => REG_FILE_n_40,
      DATA_OUT2(19) => REG_FILE_n_41,
      DATA_OUT2(18) => REG_FILE_n_42,
      DATA_OUT2(17) => REG_FILE_n_43,
      DATA_OUT2(16) => REG_FILE_n_44,
      DATA_OUT2(15) => REG_FILE_n_45,
      DATA_OUT2(14) => REG_FILE_n_46,
      DATA_OUT2(13) => REG_FILE_n_47,
      DATA_OUT2(12) => REG_FILE_n_48,
      DATA_OUT2(11) => REG_FILE_n_49,
      DATA_OUT2(10) => REG_FILE_n_50,
      DATA_OUT2(9) => REG_FILE_n_51,
      DATA_OUT2(8) => REG_FILE_n_52,
      DATA_OUT2(7) => REG_FILE_n_53,
      DATA_OUT2(6) => REG_FILE_n_54,
      DATA_OUT2(5) => REG_FILE_n_55,
      DATA_OUT2(4) => REG_FILE_n_56,
      DATA_OUT2(3) => REG_FILE_n_57,
      DATA_OUT2(2) => REG_FILE_n_58,
      DATA_OUT2(1) => REG_FILE_n_59,
      DATA_OUT2(0) => REG_FILE_n_60,
      \Dout_reg[31]\(28 downto 0) => \^dout_reg[31]\(31 downto 3),
      \Dout_reg[3]\ => REG_IR_n_145,
      PCPlus8(28 downto 0) => PCPlus8(31 downto 3),
      Q(29 downto 0) => PCPlus4_regPCP4(31 downto 2)
    );
INSTR_MEMORY: entity work.ROM_ARRAY
     port map (
      D(11 downto 10) => Instr_in(31 downto 30),
      D(9 downto 8) => Instr_in(24 downto 23),
      D(7) => Instr_in(21),
      D(6 downto 3) => Instr_in(15 downto 12),
      D(2 downto 0) => Instr_in(3 downto 1),
      Q(5) => PC_REG_n_15,
      Q(4) => PC_REG_n_16,
      Q(3) => PC_REG_n_17,
      Q(2) => PC_REG_n_18,
      Q(1) => PC_REG_n_19,
      Q(0) => PC_REG_n_20
    );
PC_REG: entity work.REGrwe_n
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(14) => PC_REG_n_0,
      D(13) => PC_REG_n_1,
      D(12) => PC_REG_n_2,
      D(11) => PC_REG_n_3,
      D(10) => PC_REG_n_4,
      D(9) => PC_REG_n_5,
      D(8) => PC_REG_n_6,
      D(7) => PC_REG_n_7,
      D(6) => PC_REG_n_8,
      D(5) => PC_REG_n_9,
      D(4) => PC_REG_n_10,
      D(3) => PC_REG_n_11,
      D(2) => PC_REG_n_12,
      D(1) => PC_REG_n_13,
      D(0) => PC_REG_n_14,
      \Dout_reg[31]_0\(29 downto 0) => PCPlus4(31 downto 2),
      \Dout_reg[31]_1\(31 downto 0) => PC_next(31 downto 0),
      \Dout_reg[4]_0\ => PC_REG_n_23,
      E(0) => E(0),
      Q(7) => PC_REG_n_15,
      Q(6) => PC_REG_n_16,
      Q(5) => PC_REG_n_17,
      Q(4) => PC_REG_n_18,
      Q(3) => PC_REG_n_19,
      Q(2) => PC_REG_n_20,
      Q(1) => PC_REG_n_21,
      Q(0) => PC_REG_n_22,
      RESET_IBUF => RESET_IBUF
    );
REG_A: entity work.REGrwe_n_0
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      CO(0) => ALU_n_18,
      D(31 downto 0) => SrcA(31 downto 0),
      DI(0) => REG_A_n_2,
      \Dout[3]_i_3\(0) => ALU_n_37,
      \Dout_reg[30]_0\(0) => REG_A_n_0,
      \Dout_reg[30]_1\(0) => REG_A_n_1,
      Q(31 downto 0) => SrcA_regA(31 downto 0),
      RESET_IBUF => RESET_IBUF
    );
REG_B: entity work.REGrwe_n_1
     port map (
      \ALUResult_out_OBUF[28]_inst_i_2_0\(21) => ExtImm_regI(31),
      \ALUResult_out_OBUF[28]_inst_i_2_0\(20 downto 16) => ExtImm_regI(24 downto 20),
      \ALUResult_out_OBUF[28]_inst_i_2_0\(15 downto 11) => ExtImm_regI(18 downto 14),
      \ALUResult_out_OBUF[28]_inst_i_2_0\(10 downto 0) => ExtImm_regI(10 downto 0),
      \ALUResult_out_OBUF[28]_inst_i_2_1\ => REG_IR_n_46,
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      \Dout_reg[0]_0\ => REG_B_n_16,
      \Dout_reg[10]_0\ => REG_B_n_19,
      \Dout_reg[12]_0\ => REG_B_n_10,
      \Dout_reg[12]_1\ => REG_B_n_57,
      \Dout_reg[13]_0\ => REG_B_n_24,
      \Dout_reg[14]_0\ => REG_B_n_20,
      \Dout_reg[14]_1\ => REG_B_n_23,
      \Dout_reg[15]_0\ => REG_B_n_21,
      \Dout_reg[15]_1\ => REG_B_n_62,
      \Dout_reg[16]_0\ => REG_I_n_26,
      \Dout_reg[16]_1\ => REG_I_n_27,
      \Dout_reg[17]_0\ => REG_B_n_63,
      \Dout_reg[18]_0\ => REG_B_n_7,
      \Dout_reg[19]_0\ => REG_B_n_8,
      \Dout_reg[1]_0\ => REG_B_n_59,
      \Dout_reg[22]_0\ => REG_B_n_64,
      \Dout_reg[23]_0\ => REG_B_n_65,
      \Dout_reg[23]_1\ => REG_I_n_29,
      \Dout_reg[24]_0\ => REG_B_n_6,
      \Dout_reg[24]_1\ => REG_B_n_66,
      \Dout_reg[25]_0\ => REG_B_n_1,
      \Dout_reg[25]_1\ => REG_B_n_4,
      \Dout_reg[26]_0\ => REG_B_n_3,
      \Dout_reg[27]_0\ => REG_B_n_67,
      \Dout_reg[28]_0\ => REG_IR_n_45,
      \Dout_reg[2]_0\ => REG_B_n_58,
      \Dout_reg[2]_1\ => REG_IR_n_41,
      \Dout_reg[30]_0\ => REG_B_n_68,
      \Dout_reg[31]_0\(31 downto 0) => \^dout_reg[31]\(31 downto 0),
      \Dout_reg[3]_0\ => REG_B_n_14,
      \Dout_reg[3]_1\ => REG_B_n_60,
      \Dout_reg[6]_0\ => REG_I_n_23,
      \Dout_reg[7]_0\ => REG_B_n_11,
      \Dout_reg[7]_1\ => REG_I_n_0,
      \Dout_reg[7]_rep\ => REG_B_n_0,
      \Dout_reg[7]_rep_0\ => REG_B_n_2,
      \Dout_reg[7]_rep_1\ => REG_B_n_5,
      \Dout_reg[7]_rep_2\ => REG_B_n_9,
      \Dout_reg[7]_rep_3\ => REG_B_n_12,
      \Dout_reg[7]_rep_4\ => REG_B_n_13,
      \Dout_reg[7]_rep_5\ => REG_B_n_15,
      \Dout_reg[7]_rep_6\ => REG_B_n_17,
      \Dout_reg[7]_rep_7\ => REG_B_n_18,
      \Dout_reg[7]_rep_8\ => REG_B_n_22,
      \Dout_reg[8]_0\ => REG_B_n_61,
      Instr_out_OBUF(1 downto 0) => Instr_out_OBUF(8 downto 7),
      Q(31 downto 0) => WriteData_regB(31 downto 0),
      RESET_IBUF => RESET_IBUF
    );
REG_FILE: entity work.REGISTER_FILE_wR15
     port map (
      ADDRA(2) => RA1_in(1),
      ADDRA(1) => RA1_in(2),
      ADDRA(0) => RA1_in(0),
      ADDRD(3 downto 0) => RA3_in(3 downto 0),
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      DATA_IN(31 downto 0) => WD3_in(31 downto 0),
      DATA_OUT1(31) => REG_FILE_n_0,
      DATA_OUT1(30) => REG_FILE_n_1,
      DATA_OUT1(29) => REG_FILE_n_2,
      DATA_OUT1(28) => REG_FILE_n_3,
      DATA_OUT1(27) => REG_FILE_n_4,
      DATA_OUT1(26) => REG_FILE_n_5,
      DATA_OUT1(25) => REG_FILE_n_6,
      DATA_OUT1(24) => REG_FILE_n_7,
      DATA_OUT1(23) => REG_FILE_n_8,
      DATA_OUT1(22) => REG_FILE_n_9,
      DATA_OUT1(21) => REG_FILE_n_10,
      DATA_OUT1(20) => REG_FILE_n_11,
      DATA_OUT1(19) => REG_FILE_n_12,
      DATA_OUT1(18) => REG_FILE_n_13,
      DATA_OUT1(17) => REG_FILE_n_14,
      DATA_OUT1(16) => REG_FILE_n_15,
      DATA_OUT1(15) => REG_FILE_n_16,
      DATA_OUT1(14) => REG_FILE_n_17,
      DATA_OUT1(13) => REG_FILE_n_18,
      DATA_OUT1(12) => REG_FILE_n_19,
      DATA_OUT1(11) => REG_FILE_n_20,
      DATA_OUT1(10) => REG_FILE_n_21,
      DATA_OUT1(9) => REG_FILE_n_22,
      DATA_OUT1(8) => REG_FILE_n_23,
      DATA_OUT1(7) => REG_FILE_n_24,
      DATA_OUT1(6) => REG_FILE_n_25,
      DATA_OUT1(5) => REG_FILE_n_26,
      DATA_OUT1(4) => REG_FILE_n_27,
      DATA_OUT1(3) => REG_FILE_n_28,
      DATA_OUT1(2) => REG_FILE_n_29,
      DATA_OUT1(1) => REG_FILE_n_30,
      DATA_OUT1(0) => REG_FILE_n_31,
      DATA_OUT2(31) => REG_FILE_n_32,
      DATA_OUT2(30) => REG_FILE_n_33,
      DATA_OUT2(29) => REG_FILE_n_34,
      DATA_OUT2(28) => REG_FILE_n_35,
      DATA_OUT2(27) => REG_FILE_n_36,
      DATA_OUT2(26) => REG_FILE_n_37,
      DATA_OUT2(25) => REG_FILE_n_38,
      DATA_OUT2(24) => REG_FILE_n_39,
      DATA_OUT2(23) => REG_FILE_n_40,
      DATA_OUT2(22) => REG_FILE_n_41,
      DATA_OUT2(21) => REG_FILE_n_42,
      DATA_OUT2(20) => REG_FILE_n_43,
      DATA_OUT2(19) => REG_FILE_n_44,
      DATA_OUT2(18) => REG_FILE_n_45,
      DATA_OUT2(17) => REG_FILE_n_46,
      DATA_OUT2(16) => REG_FILE_n_47,
      DATA_OUT2(15) => REG_FILE_n_48,
      DATA_OUT2(14) => REG_FILE_n_49,
      DATA_OUT2(13) => REG_FILE_n_50,
      DATA_OUT2(12) => REG_FILE_n_51,
      DATA_OUT2(11) => REG_FILE_n_52,
      DATA_OUT2(10) => REG_FILE_n_53,
      DATA_OUT2(9) => REG_FILE_n_54,
      DATA_OUT2(8) => REG_FILE_n_55,
      DATA_OUT2(7) => REG_FILE_n_56,
      DATA_OUT2(6) => REG_FILE_n_57,
      DATA_OUT2(5) => REG_FILE_n_58,
      DATA_OUT2(4) => REG_FILE_n_59,
      DATA_OUT2(3) => REG_FILE_n_60,
      DATA_OUT2(2) => REG_FILE_n_61,
      DATA_OUT2(1) => REG_FILE_n_62,
      DATA_OUT2(0) => REG_FILE_n_63,
      \Dout_reg[1]\(3 downto 0) => RA2_in(3 downto 0),
      RegWrite_in => RegWrite_in
    );
REG_I: entity work.REGrwe_n_2
     port map (
      \ALUResult_out_OBUF[23]_inst_i_3\(10) => WriteData_regB(22),
      \ALUResult_out_OBUF[23]_inst_i_3\(9 downto 5) => WriteData_regB(20 downto 16),
      \ALUResult_out_OBUF[23]_inst_i_3\(4) => WriteData_regB(14),
      \ALUResult_out_OBUF[23]_inst_i_3\(3 downto 0) => WriteData_regB(8 downto 5),
      \ALUResult_out_OBUF[23]_inst_i_3_0\ => REG_IR_n_46,
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(21) => ExtImm(31),
      D(20 downto 16) => ExtImm(24 downto 20),
      D(15 downto 11) => ExtImm(18 downto 14),
      D(10 downto 0) => ExtImm(10 downto 0),
      \Dout_reg[16]_0\ => REG_I_n_24,
      \Dout_reg[16]_1\ => REG_I_n_25,
      \Dout_reg[18]_0\ => REG_I_n_26,
      \Dout_reg[20]_0\ => REG_I_n_28,
      \Dout_reg[20]_1\ => REG_I_n_29,
      \Dout_reg[21]_0\ => REG_I_n_27,
      \Dout_reg[5]_0\ => REG_I_n_0,
      \Dout_reg[8]_0\ => REG_I_n_23,
      Instr_out_OBUF(0) => Instr_out_OBUF(8),
      Q(21) => ExtImm_regI(31),
      Q(20 downto 16) => ExtImm_regI(24 downto 20),
      Q(15 downto 11) => ExtImm_regI(18 downto 14),
      Q(10 downto 0) => ExtImm_regI(10 downto 0),
      RESET_IBUF => RESET_IBUF
    );
REG_IR: entity work.REGrwe_n_3
     port map (
      ADDRA(2) => RA1_in(1),
      ADDRA(1) => RA1_in(2),
      ADDRA(0) => RA1_in(0),
      ADDRD(3 downto 0) => RA3_in(3 downto 0),
      \ALUResult_out_OBUF[29]_inst_i_3_0\(31 downto 0) => WriteData_regB(31 downto 0),
      \ALUResult_out_OBUF[31]_inst_i_2_0\ => ALU_n_52,
      \ALUResult_out_OBUF[31]_inst_i_4_0\ => REG_B_n_68,
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(31 downto 0) => \^aluresult_out_obuf\(31 downto 0),
      DATA_IN(31 downto 0) => WD3_in(31 downto 0),
      DATA_OUT1(31) => REG_FILE_n_0,
      DATA_OUT1(30) => REG_FILE_n_1,
      DATA_OUT1(29) => REG_FILE_n_2,
      DATA_OUT1(28) => REG_FILE_n_3,
      DATA_OUT1(27) => REG_FILE_n_4,
      DATA_OUT1(26) => REG_FILE_n_5,
      DATA_OUT1(25) => REG_FILE_n_6,
      DATA_OUT1(24) => REG_FILE_n_7,
      DATA_OUT1(23) => REG_FILE_n_8,
      DATA_OUT1(22) => REG_FILE_n_9,
      DATA_OUT1(21) => REG_FILE_n_10,
      DATA_OUT1(20) => REG_FILE_n_11,
      DATA_OUT1(19) => REG_FILE_n_12,
      DATA_OUT1(18) => REG_FILE_n_13,
      DATA_OUT1(17) => REG_FILE_n_14,
      DATA_OUT1(16) => REG_FILE_n_15,
      DATA_OUT1(15) => REG_FILE_n_16,
      DATA_OUT1(14) => REG_FILE_n_17,
      DATA_OUT1(13) => REG_FILE_n_18,
      DATA_OUT1(12) => REG_FILE_n_19,
      DATA_OUT1(11) => REG_FILE_n_20,
      DATA_OUT1(10) => REG_FILE_n_21,
      DATA_OUT1(9) => REG_FILE_n_22,
      DATA_OUT1(8) => REG_FILE_n_23,
      DATA_OUT1(7) => REG_FILE_n_24,
      DATA_OUT1(6) => REG_FILE_n_25,
      DATA_OUT1(5) => REG_FILE_n_26,
      DATA_OUT1(4) => REG_FILE_n_27,
      DATA_OUT1(3) => REG_FILE_n_28,
      DATA_OUT1(2) => REG_FILE_n_29,
      DATA_OUT1(1) => REG_FILE_n_30,
      DATA_OUT1(0) => REG_FILE_n_31,
      \Dout[1]_i_6_0\ => REG_IR_n_37,
      \Dout[1]_i_7_0\ => REG_IR_n_43,
      \Dout[1]_i_8_0\ => REG_IR_n_42,
      \Dout[1]_i_9_0\ => REG_IR_n_0,
      \Dout_reg[0]_0\ => REG_B_n_60,
      \Dout_reg[10]\ => ALU_n_44,
      \Dout_reg[10]_0\ => REG_B_n_10,
      \Dout_reg[10]_1\ => REG_B_n_11,
      \Dout_reg[10]_2\ => REG_B_n_61,
      \Dout_reg[11]\(3) => REG_IR_n_176,
      \Dout_reg[11]\(2) => REG_IR_n_177,
      \Dout_reg[11]\(1) => REG_IR_n_178,
      \Dout_reg[11]\(0) => REG_IR_n_179,
      \Dout_reg[11]_0\(3) => REG_IR_n_200,
      \Dout_reg[11]_0\(2) => REG_IR_n_201,
      \Dout_reg[11]_0\(1) => REG_IR_n_202,
      \Dout_reg[11]_0\(0) => REG_IR_n_203,
      \Dout_reg[11]_1\ => REG_B_n_20,
      \Dout_reg[12]_0\ => REG_B_n_18,
      \Dout_reg[12]_1\ => ALU_n_45,
      \Dout_reg[13]_0\ => REG_B_n_19,
      \Dout_reg[13]_1\ => REG_B_n_21,
      \Dout_reg[13]_2\ => REG_I_n_24,
      \Dout_reg[14]_0\ => ALU_n_46,
      \Dout_reg[15]_0\(3) => REG_IR_n_180,
      \Dout_reg[15]_0\(2) => REG_IR_n_181,
      \Dout_reg[15]_0\(1) => REG_IR_n_182,
      \Dout_reg[15]_0\(0) => REG_IR_n_183,
      \Dout_reg[15]_1\(3) => REG_IR_n_204,
      \Dout_reg[15]_1\(2) => REG_IR_n_205,
      \Dout_reg[15]_1\(1) => REG_IR_n_206,
      \Dout_reg[15]_1\(0) => REG_IR_n_207,
      \Dout_reg[15]_2\ => ALU_n_47,
      \Dout_reg[15]_3\ => REG_B_n_57,
      \Dout_reg[15]_4\ => REG_B_n_24,
      \Dout_reg[15]_rep_0\ => REG_IR_n_145,
      \Dout_reg[15]_rep_1\(3 downto 0) => RA2_in(3 downto 0),
      \Dout_reg[15]_rep_2\ => \Dout_reg[15]_rep\,
      \Dout_reg[16]_0\ => REG_B_n_22,
      \Dout_reg[16]_1\ => ALU_n_48,
      \Dout_reg[17]\ => REG_B_n_62,
      \Dout_reg[17]_0\ => REG_B_n_23,
      \Dout_reg[17]_1\ => REG_I_n_27,
      \Dout_reg[17]_2\ => REG_I_n_28,
      \Dout_reg[19]_0\(3) => REG_IR_n_212,
      \Dout_reg[19]_0\(2) => REG_IR_n_213,
      \Dout_reg[19]_0\(1) => REG_IR_n_214,
      \Dout_reg[19]_0\(0) => REG_IR_n_215,
      \Dout_reg[19]_1\ => REG_B_n_63,
      \Dout_reg[19]_2\ => REG_I_n_25,
      \Dout_reg[19]_rep_0\ => \Dout_reg[19]_rep\,
      \Dout_reg[19]_rep_rep_0\(31 downto 0) => SrcA(31 downto 0),
      \Dout_reg[20]_0\ => ALU_n_49,
      \Dout_reg[20]_1\ => REG_B_n_7,
      \Dout_reg[20]_rep_0\(0) => \^dout_reg[27]_rep__0\(0),
      \Dout_reg[21]_0\(3) => REG_IR_n_208,
      \Dout_reg[21]_0\(2) => REG_IR_n_209,
      \Dout_reg[21]_0\(1) => REG_IR_n_210,
      \Dout_reg[21]_0\(0) => REG_IR_n_211,
      \Dout_reg[21]_1\ => REG_B_n_5,
      \Dout_reg[21]_2\ => ALU_n_50,
      \Dout_reg[21]_rep_0\(2) => Instr_out_OBUF(21),
      \Dout_reg[21]_rep_0\(1 downto 0) => Instr_out_OBUF(8 downto 7),
      \Dout_reg[22]_0\ => REG_B_n_6,
      \Dout_reg[22]_1\ => REG_B_n_4,
      \Dout_reg[22]_2\ => REG_I_n_29,
      \Dout_reg[22]_3\ => REG_B_n_8,
      \Dout_reg[23]_0\(3) => REG_IR_n_192,
      \Dout_reg[23]_0\(2) => REG_IR_n_193,
      \Dout_reg[23]_0\(1) => REG_IR_n_194,
      \Dout_reg[23]_0\(0) => REG_IR_n_195,
      \Dout_reg[23]_1\(3) => REG_IR_n_216,
      \Dout_reg[23]_1\(2) => REG_IR_n_217,
      \Dout_reg[23]_1\(1) => REG_IR_n_218,
      \Dout_reg[23]_1\(0) => REG_IR_n_219,
      \Dout_reg[23]_2\ => REG_B_n_2,
      \Dout_reg[23]_3\ => ALU_n_51,
      \Dout_reg[23]_rep_0\ => \^alucontrol_out_obuf\(0),
      \Dout_reg[23]_rep_1\ => REG_IR_n_45,
      \Dout_reg[24]_0\ => REG_B_n_3,
      \Dout_reg[24]_1\ => REG_B_n_67,
      \Dout_reg[24]_rep_0\(4 downto 0) => D(4 downto 0),
      \Dout_reg[25]_0\ => REG_B_n_64,
      \Dout_reg[25]_rep_0\ => \^alucontrol_out_obuf\(3),
      \Dout_reg[25]_rep_1\ => \^alucontrol_out_obuf\(2),
      \Dout_reg[25]_rep_2\ => \Dout_reg[25]_rep\,
      \Dout_reg[25]_rep_3\ => REG_IR_n_46,
      \Dout_reg[26]_0\ => REG_B_n_65,
      \Dout_reg[26]_1\ => REG_B_n_66,
      \Dout_reg[26]_rep_0\ => \^alucontrol_out_obuf\(1),
      \Dout_reg[26]_rep_1\ => \^dout_reg[27]_rep__0\(1),
      \Dout_reg[26]_rep_2\ => REG_IR_n_54,
      \Dout_reg[27]_0\(3) => REG_IR_n_160,
      \Dout_reg[27]_0\(2) => REG_IR_n_161,
      \Dout_reg[27]_0\(1) => REG_IR_n_162,
      \Dout_reg[27]_0\(0) => REG_IR_n_163,
      \Dout_reg[27]_1\(3) => REG_IR_n_184,
      \Dout_reg[27]_1\(2) => REG_IR_n_185,
      \Dout_reg[27]_1\(1) => REG_IR_n_186,
      \Dout_reg[27]_1\(0) => REG_IR_n_187,
      \Dout_reg[27]_2\ => REG_B_n_1,
      \Dout_reg[27]_rep_0\ => PC_REG_n_23,
      \Dout_reg[27]_rep__0_0\ => \^dout_reg[27]_rep__0\(2),
      \Dout_reg[27]_rep__0_1\(21) => ExtImm(31),
      \Dout_reg[27]_rep__0_1\(20 downto 16) => ExtImm(24 downto 20),
      \Dout_reg[27]_rep__0_1\(15 downto 11) => ExtImm(18 downto 14),
      \Dout_reg[27]_rep__0_1\(10 downto 0) => ExtImm(10 downto 0),
      \Dout_reg[28]_0\ => REG_B_n_0,
      \Dout_reg[28]_1\(21) => ExtImm_regI(31),
      \Dout_reg[28]_1\(20 downto 16) => ExtImm_regI(24 downto 20),
      \Dout_reg[28]_1\(15 downto 11) => ExtImm_regI(18 downto 14),
      \Dout_reg[28]_1\(10 downto 0) => ExtImm_regI(10 downto 0),
      \Dout_reg[28]_rep_0\ => \Dout_reg[28]_rep\,
      \Dout_reg[2]_0\ => REG_B_n_15,
      \Dout_reg[2]_1\ => ALU_n_39,
      \Dout_reg[30]_0\(17 downto 11) => \ADD_SUB/S_Add_in\(30 downto 24),
      \Dout_reg[30]_0\(10) => \ADD_SUB/S_Add_in\(22),
      \Dout_reg[30]_0\(9 downto 7) => \ADD_SUB/S_Add_in\(19 downto 17),
      \Dout_reg[30]_0\(6) => \ADD_SUB/S_Add_in\(13),
      \Dout_reg[30]_0\(5) => \ADD_SUB/S_Add_in\(11),
      \Dout_reg[30]_0\(4 downto 2) => \ADD_SUB/S_Add_in\(5 downto 3),
      \Dout_reg[30]_0\(1 downto 0) => \ADD_SUB/S_Add_in\(1 downto 0),
      \Dout_reg[30]_1\(17 downto 11) => \ADD_SUB/S_Sub_in\(30 downto 24),
      \Dout_reg[30]_1\(10) => \ADD_SUB/S_Sub_in\(22),
      \Dout_reg[30]_1\(9 downto 7) => \ADD_SUB/S_Sub_in\(19 downto 17),
      \Dout_reg[30]_1\(6) => \ADD_SUB/S_Sub_in\(13),
      \Dout_reg[30]_1\(5) => \ADD_SUB/S_Sub_in\(11),
      \Dout_reg[30]_1\(4 downto 2) => \ADD_SUB/S_Sub_in\(5 downto 3),
      \Dout_reg[30]_1\(1 downto 0) => \ADD_SUB/S_Sub_in\(1 downto 0),
      \Dout_reg[31]_0\(3) => REG_IR_n_164,
      \Dout_reg[31]_0\(2) => REG_IR_n_165,
      \Dout_reg[31]_0\(1) => REG_IR_n_166,
      \Dout_reg[31]_0\(0) => REG_IR_n_167,
      \Dout_reg[31]_1\(3) => REG_IR_n_188,
      \Dout_reg[31]_1\(2) => REG_IR_n_189,
      \Dout_reg[31]_1\(1) => REG_IR_n_190,
      \Dout_reg[31]_1\(0) => REG_IR_n_191,
      \Dout_reg[31]_2\(0) => PC_next(31),
      \Dout_reg[31]_3\(26 downto 0) => \Dout_reg[31]_0\(26 downto 0),
      \Dout_reg[31]_4\(2 downto 1) => Q(4 downto 3),
      \Dout_reg[31]_4\(0) => Q(0),
      \Dout_reg[31]_5\(31 downto 0) => PCPlus4_regPCP4(31 downto 0),
      \Dout_reg[31]_6\ => \Dout_reg[30]\,
      \Dout_reg[31]_7\(26 downto 25) => Instr_in(31 downto 30),
      \Dout_reg[31]_7\(24) => PC_REG_n_0,
      \Dout_reg[31]_7\(23) => PC_REG_n_1,
      \Dout_reg[31]_7\(22) => PC_REG_n_2,
      \Dout_reg[31]_7\(21) => PC_REG_n_3,
      \Dout_reg[31]_7\(20 downto 19) => Instr_in(24 downto 23),
      \Dout_reg[31]_7\(18) => PC_REG_n_4,
      \Dout_reg[31]_7\(17) => Instr_in(21),
      \Dout_reg[31]_7\(16) => PC_REG_n_5,
      \Dout_reg[31]_7\(15) => PC_REG_n_6,
      \Dout_reg[31]_7\(14) => PC_REG_n_7,
      \Dout_reg[31]_7\(13) => PC_REG_n_8,
      \Dout_reg[31]_7\(12 downto 9) => Instr_in(15 downto 12),
      \Dout_reg[31]_7\(8) => PC_REG_n_9,
      \Dout_reg[31]_7\(7) => PC_REG_n_10,
      \Dout_reg[31]_7\(6) => PC_REG_n_11,
      \Dout_reg[31]_7\(5) => PC_REG_n_12,
      \Dout_reg[31]_7\(4) => PC_REG_n_13,
      \Dout_reg[31]_7\(3 downto 1) => Instr_in(3 downto 1),
      \Dout_reg[31]_7\(0) => PC_REG_n_14,
      \Dout_reg[31]_rep_0\ => \Dout_reg[31]_rep\,
      \Dout_reg[3]_0\(3) => REG_IR_n_168,
      \Dout_reg[3]_0\(2) => REG_IR_n_169,
      \Dout_reg[3]_0\(1) => REG_IR_n_170,
      \Dout_reg[3]_0\(0) => REG_IR_n_171,
      \Dout_reg[3]_1\ => REG_B_n_16,
      \Dout_reg[3]_2\ => REG_B_n_59,
      \Dout_reg[4]_0\ => REG_B_n_58,
      \Dout_reg[5]_0\ => REG_B_n_14,
      \Dout_reg[6]_0\ => REG_B_n_13,
      \Dout_reg[6]_1\ => ALU_n_40,
      \Dout_reg[7]_0\(3) => REG_IR_n_172,
      \Dout_reg[7]_0\(2) => REG_IR_n_173,
      \Dout_reg[7]_0\(1) => REG_IR_n_174,
      \Dout_reg[7]_0\(0) => REG_IR_n_175,
      \Dout_reg[7]_1\(3) => REG_IR_n_196,
      \Dout_reg[7]_1\(2) => REG_IR_n_197,
      \Dout_reg[7]_1\(1) => REG_IR_n_198,
      \Dout_reg[7]_1\(0) => REG_IR_n_199,
      \Dout_reg[7]_2\ => REG_B_n_12,
      \Dout_reg[7]_3\ => ALU_n_41,
      \Dout_reg[8]_0\ => REG_B_n_17,
      \Dout_reg[8]_1\ => ALU_n_42,
      \Dout_reg[8]_rep_0\ => REG_IR_n_41,
      \Dout_reg[9]\ => REG_B_n_9,
      \Dout_reg[9]_0\ => ALU_n_43,
      \FSM_onehot_current_state_reg[13]\ => \FSM_onehot_current_state_reg[13]\,
      \FSM_onehot_current_state_reg[2]\(0) => \^dout_reg[0]\(0),
      Flags_in(1) => Flags_in(3),
      Flags_in(0) => Flags_in(1),
      PCPlus8(28 downto 0) => PCPlus8(31 downto 3),
      Q(31 downto 0) => SrcA_regA(31 downto 0),
      RESET_IBUF => RESET_IBUF,
      RF_reg_r2_0_15_30_31(31 downto 0) => ALUResult_in_regS(31 downto 0),
      ReadData_in_regRD(31 downto 0) => ReadData_in_regRD(31 downto 0),
      Result_out_OBUF(0) => \^result_out_obuf\(31),
      S(3) => REG_IR_n_156,
      S(2) => REG_IR_n_157,
      S(1) => REG_IR_n_158,
      S(0) => REG_IR_n_159
    );
REG_MA: entity work.REGrwe_n_4
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(4 downto 0) => \^aluresult_out_obuf\(6 downto 2),
      \Dout_reg[2]_0\(0) => Q(1),
      Q(4 downto 0) => ADDR(4 downto 0),
      RESET_IBUF => RESET_IBUF
    );
REG_PCP4: entity work.REGrwe_n_5
     port map (
      ALUResult_out_OBUF(30 downto 0) => \^aluresult_out_obuf\(30 downto 0),
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(31 downto 2) => PCPlus4(31 downto 2),
      D(1) => PC_REG_n_21,
      D(0) => PC_REG_n_22,
      DATA_OUT2(2) => REG_FILE_n_61,
      DATA_OUT2(1) => REG_FILE_n_62,
      DATA_OUT2(0) => REG_FILE_n_63,
      \Dout_reg[0]_0\ => REG_IR_n_145,
      \Dout_reg[2]_0\(2 downto 0) => \^dout_reg[31]\(2 downto 0),
      \Dout_reg[30]_0\(30 downto 0) => PC_next(30 downto 0),
      \Dout_reg[30]_1\(1 downto 0) => Q(4 downto 3),
      \Dout_reg[30]_2\ => \Dout_reg[30]\,
      Q(31 downto 0) => PCPlus4_regPCP4(31 downto 0),
      RESET_IBUF => RESET_IBUF,
      Result_out_OBUF(30 downto 0) => \^result_out_obuf\(30 downto 0)
    );
REG_S: entity work.REGrwe_n_6
     port map (
      ALUResult_out_OBUF(31 downto 0) => \^aluresult_out_obuf\(31 downto 0),
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      Q(31 downto 0) => ALUResult_in_regS(31 downto 0),
      RESET_IBUF => RESET_IBUF
    );
REG_WD: entity work.REGrwe_n_7
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(31 downto 0) => WriteData_regB(31 downto 0),
      Q(31) => REG_WD_n_0,
      Q(30) => REG_WD_n_1,
      Q(29) => REG_WD_n_2,
      Q(28) => REG_WD_n_3,
      Q(27) => REG_WD_n_4,
      Q(26) => REG_WD_n_5,
      Q(25) => REG_WD_n_6,
      Q(24) => REG_WD_n_7,
      Q(23) => REG_WD_n_8,
      Q(22) => REG_WD_n_9,
      Q(21) => REG_WD_n_10,
      Q(20) => REG_WD_n_11,
      Q(19) => REG_WD_n_12,
      Q(18) => REG_WD_n_13,
      Q(17) => REG_WD_n_14,
      Q(16) => REG_WD_n_15,
      Q(15) => REG_WD_n_16,
      Q(14) => REG_WD_n_17,
      Q(13) => REG_WD_n_18,
      Q(12) => REG_WD_n_19,
      Q(11) => REG_WD_n_20,
      Q(10) => REG_WD_n_21,
      Q(9) => REG_WD_n_22,
      Q(8) => REG_WD_n_23,
      Q(7) => REG_WD_n_24,
      Q(6) => REG_WD_n_25,
      Q(5) => REG_WD_n_26,
      Q(4) => REG_WD_n_27,
      Q(3) => REG_WD_n_28,
      Q(2) => REG_WD_n_29,
      Q(1) => REG_WD_n_30,
      Q(0) => REG_WD_n_31,
      RESET_IBUF => RESET_IBUF
    );
STATUS_REG: entity work.\REGrwe_n__parameterized1\
     port map (
      ALUControl_out_OBUF(0) => \^alucontrol_out_obuf\(1),
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      \Dout_reg[0]_0\(0) => \^dout_reg[0]\(0),
      \Dout_reg[0]_1\ => \Dout_reg[0]_0\,
      \Dout_reg[1]_0\ => REG_IR_n_37,
      \Dout_reg[1]_1\ => REG_IR_n_43,
      \Dout_reg[1]_2\ => REG_IR_n_42,
      \Dout_reg[1]_3\ => REG_IR_n_0,
      \Dout_reg[3]_0\ => REG_IR_n_54,
      \Dout_reg[3]_1\(0) => Instr_out_OBUF(21),
      \Dout_reg[3]_2\ => ALU_n_38,
      FlagsWrite_in => FlagsWrite_in,
      Flags_in(1) => Flags_in(3),
      Flags_in(0) => Flags_in(1),
      RESET_IBUF => RESET_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ARM_PROCESSOR_n is
  port (
    CLK : in STD_LOGIC;
    RESET : in STD_LOGIC;
    ALUControl_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Instr_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ALUResult_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    Result_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    WriteData_out : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of ARM_PROCESSOR_n : entity is true;
end ARM_PROCESSOR_n;

architecture STRUCTURE of ARM_PROCESSOR_n is
  signal ALUControl_out_OBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ALUResult_out_OBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal CLK_IBUF : STD_LOGIC;
  signal CLK_IBUF_BUFG : STD_LOGIC;
  signal CONTROL_n_0 : STD_LOGIC;
  signal CONTROL_n_1 : STD_LOGIC;
  signal CONTROL_n_10 : STD_LOGIC;
  signal CONTROL_n_2 : STD_LOGIC;
  signal CONTROL_n_7 : STD_LOGIC;
  signal DATAPATH_n_109 : STD_LOGIC;
  signal DATAPATH_n_111 : STD_LOGIC;
  signal DATAPATH_n_112 : STD_LOGIC;
  signal DATAPATH_n_113 : STD_LOGIC;
  signal DATAPATH_n_114 : STD_LOGIC;
  signal DATAPATH_n_115 : STD_LOGIC;
  signal DATAPATH_n_116 : STD_LOGIC;
  signal DATAPATH_n_117 : STD_LOGIC;
  signal DATAPATH_n_118 : STD_LOGIC;
  signal DATAPATH_n_119 : STD_LOGIC;
  signal DATAPATH_n_120 : STD_LOGIC;
  signal DATAPATH_n_121 : STD_LOGIC;
  signal DATAPATH_n_122 : STD_LOGIC;
  signal DATAPATH_n_123 : STD_LOGIC;
  signal DATAPATH_n_124 : STD_LOGIC;
  signal DATAPATH_n_125 : STD_LOGIC;
  signal DATAPATH_n_126 : STD_LOGIC;
  signal DATAPATH_n_127 : STD_LOGIC;
  signal DATAPATH_n_128 : STD_LOGIC;
  signal DATAPATH_n_129 : STD_LOGIC;
  signal DATAPATH_n_130 : STD_LOGIC;
  signal DATAPATH_n_131 : STD_LOGIC;
  signal DATAPATH_n_132 : STD_LOGIC;
  signal DATAPATH_n_133 : STD_LOGIC;
  signal DATAPATH_n_134 : STD_LOGIC;
  signal DATAPATH_n_135 : STD_LOGIC;
  signal DATAPATH_n_136 : STD_LOGIC;
  signal DATAPATH_n_137 : STD_LOGIC;
  signal DATAPATH_n_138 : STD_LOGIC;
  signal DATAPATH_n_139 : STD_LOGIC;
  signal DATAPATH_n_140 : STD_LOGIC;
  signal DATAPATH_n_71 : STD_LOGIC;
  signal DATAPATH_n_72 : STD_LOGIC;
  signal DATAPATH_n_73 : STD_LOGIC;
  signal DATAPATH_n_74 : STD_LOGIC;
  signal DATAPATH_n_75 : STD_LOGIC;
  signal DATAPATH_n_76 : STD_LOGIC;
  signal FlagsWrite_in : STD_LOGIC;
  signal Flags_in : STD_LOGIC_VECTOR ( 0 to 0 );
  signal IRWrite_in : STD_LOGIC;
  signal Instr_out_OBUF : STD_LOGIC_VECTOR ( 27 downto 20 );
  signal MAWrite_in : STD_LOGIC;
  signal MemWrite_in : STD_LOGIC;
  signal PCWrite_in : STD_LOGIC;
  signal RESET_IBUF : STD_LOGIC;
  signal RegWrite_in : STD_LOGIC;
  signal Result_out_OBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal WriteData_out_OBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute IOB : string;
  attribute IOB of \ALUControl_out_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \ALUControl_out_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \ALUControl_out_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of \ALUControl_out_OBUF[3]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[10]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[11]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[12]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[13]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[14]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[15]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[16]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[17]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[18]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[19]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[20]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[21]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[22]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[23]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[24]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[25]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[26]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[27]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[28]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[29]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[30]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[31]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[3]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[4]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[5]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[6]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[7]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[8]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_out_OBUF[9]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[10]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[11]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[12]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[13]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[14]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[15]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[16]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[17]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[18]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[19]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[20]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[21]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[22]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[23]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[24]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[25]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[26]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[27]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[28]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[29]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[30]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[31]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[3]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[4]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[5]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[6]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[7]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[8]_inst\ : label is "TRUE";
  attribute IOB of \Instr_out_OBUF[9]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[10]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[11]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[12]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[13]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[14]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[15]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[16]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[17]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[18]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[19]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[20]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[21]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[22]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[23]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[24]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[25]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[26]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[27]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[28]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[29]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[30]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[31]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[3]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[4]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[5]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[6]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[7]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[8]_inst\ : label is "TRUE";
  attribute IOB of \Result_out_OBUF[9]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[10]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[11]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[12]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[13]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[14]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[15]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[16]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[17]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[18]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[19]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[20]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[21]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[22]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[23]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[24]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[25]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[26]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[27]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[28]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[29]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[30]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[31]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[3]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[4]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[5]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[6]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[7]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[8]_inst\ : label is "TRUE";
  attribute IOB of \WriteData_out_OBUF[9]_inst\ : label is "TRUE";
begin
\ALUControl_out_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUControl_out_OBUF(0),
      O => ALUControl_out(0)
    );
\ALUControl_out_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUControl_out_OBUF(1),
      O => ALUControl_out(1)
    );
\ALUControl_out_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUControl_out_OBUF(2),
      O => ALUControl_out(2)
    );
\ALUControl_out_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUControl_out_OBUF(3),
      O => ALUControl_out(3)
    );
\ALUResult_out_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(0),
      O => ALUResult_out(0)
    );
\ALUResult_out_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(10),
      O => ALUResult_out(10)
    );
\ALUResult_out_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(11),
      O => ALUResult_out(11)
    );
\ALUResult_out_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(12),
      O => ALUResult_out(12)
    );
\ALUResult_out_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(13),
      O => ALUResult_out(13)
    );
\ALUResult_out_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(14),
      O => ALUResult_out(14)
    );
\ALUResult_out_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(15),
      O => ALUResult_out(15)
    );
\ALUResult_out_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(16),
      O => ALUResult_out(16)
    );
\ALUResult_out_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(17),
      O => ALUResult_out(17)
    );
\ALUResult_out_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(18),
      O => ALUResult_out(18)
    );
\ALUResult_out_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(19),
      O => ALUResult_out(19)
    );
\ALUResult_out_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(1),
      O => ALUResult_out(1)
    );
\ALUResult_out_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(20),
      O => ALUResult_out(20)
    );
\ALUResult_out_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(21),
      O => ALUResult_out(21)
    );
\ALUResult_out_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(22),
      O => ALUResult_out(22)
    );
\ALUResult_out_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(23),
      O => ALUResult_out(23)
    );
\ALUResult_out_OBUF[24]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(24),
      O => ALUResult_out(24)
    );
\ALUResult_out_OBUF[25]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(25),
      O => ALUResult_out(25)
    );
\ALUResult_out_OBUF[26]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(26),
      O => ALUResult_out(26)
    );
\ALUResult_out_OBUF[27]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(27),
      O => ALUResult_out(27)
    );
\ALUResult_out_OBUF[28]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(28),
      O => ALUResult_out(28)
    );
\ALUResult_out_OBUF[29]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(29),
      O => ALUResult_out(29)
    );
\ALUResult_out_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(2),
      O => ALUResult_out(2)
    );
\ALUResult_out_OBUF[30]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(30),
      O => ALUResult_out(30)
    );
\ALUResult_out_OBUF[31]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(31),
      O => ALUResult_out(31)
    );
\ALUResult_out_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(3),
      O => ALUResult_out(3)
    );
\ALUResult_out_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(4),
      O => ALUResult_out(4)
    );
\ALUResult_out_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(5),
      O => ALUResult_out(5)
    );
\ALUResult_out_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(6),
      O => ALUResult_out(6)
    );
\ALUResult_out_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(7),
      O => ALUResult_out(7)
    );
\ALUResult_out_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(8),
      O => ALUResult_out(8)
    );
\ALUResult_out_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_out_OBUF(9),
      O => ALUResult_out(9)
    );
CLK_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => CLK_IBUF,
      O => CLK_IBUF_BUFG
    );
CLK_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => CLK,
      O => CLK_IBUF
    );
CONTROL: entity work.CONTROL_UNIT
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(4) => DATAPATH_n_72,
      D(3) => DATAPATH_n_73,
      D(2) => DATAPATH_n_74,
      D(1) => DATAPATH_n_75,
      D(0) => DATAPATH_n_76,
      \Dout_reg[0]\ => DATAPATH_n_71,
      E(0) => PCWrite_in,
      \FSM_onehot_current_state_reg[10]\ => DATAPATH_n_111,
      \FSM_onehot_current_state_reg[11]\ => CONTROL_n_10,
      \FSM_onehot_current_state_reg[13]\ => CONTROL_n_7,
      \FSM_onehot_current_state_reg[1]\ => CONTROL_n_0,
      \FSM_onehot_current_state_reg[2]\ => DATAPATH_n_109,
      FlagsWrite_in => FlagsWrite_in,
      Flags_in(0) => Flags_in(0),
      Instr_out_OBUF(2 downto 1) => Instr_out_OBUF(27 downto 26),
      Instr_out_OBUF(0) => Instr_out_OBUF(20),
      Q(4) => CONTROL_n_1,
      Q(3) => CONTROL_n_2,
      Q(2) => MemWrite_in,
      Q(1) => MAWrite_in,
      Q(0) => IRWrite_in,
      RESET_IBUF => RESET_IBUF,
      RegWrite_in => RegWrite_in
    );
DATAPATH: entity work.DATAPATH_MC_n
     port map (
      ALUControl_out_OBUF(3 downto 0) => ALUControl_out_OBUF(3 downto 0),
      ALUResult_out_OBUF(31 downto 0) => ALUResult_out_OBUF(31 downto 0),
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(4) => DATAPATH_n_72,
      D(3) => DATAPATH_n_73,
      D(2) => DATAPATH_n_74,
      D(1) => DATAPATH_n_75,
      D(0) => DATAPATH_n_76,
      \Dout_reg[0]\(0) => Flags_in(0),
      \Dout_reg[0]_0\ => CONTROL_n_10,
      \Dout_reg[15]_rep\ => DATAPATH_n_111,
      \Dout_reg[19]_rep\ => DATAPATH_n_140,
      \Dout_reg[25]_rep\ => DATAPATH_n_71,
      \Dout_reg[27]_rep__0\(2 downto 1) => Instr_out_OBUF(27 downto 26),
      \Dout_reg[27]_rep__0\(0) => Instr_out_OBUF(20),
      \Dout_reg[28]_rep\ => DATAPATH_n_109,
      \Dout_reg[30]\ => CONTROL_n_7,
      \Dout_reg[31]\(31 downto 0) => WriteData_out_OBUF(31 downto 0),
      \Dout_reg[31]_0\(26) => DATAPATH_n_112,
      \Dout_reg[31]_0\(25) => DATAPATH_n_113,
      \Dout_reg[31]_0\(24) => DATAPATH_n_114,
      \Dout_reg[31]_0\(23) => DATAPATH_n_115,
      \Dout_reg[31]_0\(22) => DATAPATH_n_116,
      \Dout_reg[31]_0\(21) => DATAPATH_n_117,
      \Dout_reg[31]_0\(20) => DATAPATH_n_118,
      \Dout_reg[31]_0\(19) => DATAPATH_n_119,
      \Dout_reg[31]_0\(18) => DATAPATH_n_120,
      \Dout_reg[31]_0\(17) => DATAPATH_n_121,
      \Dout_reg[31]_0\(16) => DATAPATH_n_122,
      \Dout_reg[31]_0\(15) => DATAPATH_n_123,
      \Dout_reg[31]_0\(14) => DATAPATH_n_124,
      \Dout_reg[31]_0\(13) => DATAPATH_n_125,
      \Dout_reg[31]_0\(12) => DATAPATH_n_126,
      \Dout_reg[31]_0\(11) => DATAPATH_n_127,
      \Dout_reg[31]_0\(10) => DATAPATH_n_128,
      \Dout_reg[31]_0\(9) => DATAPATH_n_129,
      \Dout_reg[31]_0\(8) => DATAPATH_n_130,
      \Dout_reg[31]_0\(7) => DATAPATH_n_131,
      \Dout_reg[31]_0\(6) => DATAPATH_n_132,
      \Dout_reg[31]_0\(5) => DATAPATH_n_133,
      \Dout_reg[31]_0\(4) => DATAPATH_n_134,
      \Dout_reg[31]_0\(3) => DATAPATH_n_135,
      \Dout_reg[31]_0\(2) => DATAPATH_n_136,
      \Dout_reg[31]_0\(1) => DATAPATH_n_137,
      \Dout_reg[31]_0\(0) => DATAPATH_n_138,
      \Dout_reg[31]_rep\ => DATAPATH_n_139,
      E(0) => PCWrite_in,
      \FSM_onehot_current_state_reg[13]\ => CONTROL_n_0,
      FlagsWrite_in => FlagsWrite_in,
      Q(4) => CONTROL_n_1,
      Q(3) => CONTROL_n_2,
      Q(2) => MemWrite_in,
      Q(1) => MAWrite_in,
      Q(0) => IRWrite_in,
      RESET_IBUF => RESET_IBUF,
      RegWrite_in => RegWrite_in,
      Result_out_OBUF(31 downto 0) => Result_out_OBUF(31 downto 0)
    );
\Instr_out_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_138,
      O => Instr_out(0)
    );
\Instr_out_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => Instr_out(10)
    );
\Instr_out_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => Instr_out(11)
    );
\Instr_out_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_129,
      O => Instr_out(12)
    );
\Instr_out_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_128,
      O => Instr_out(13)
    );
\Instr_out_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_127,
      O => Instr_out(14)
    );
\Instr_out_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_126,
      O => Instr_out(15)
    );
\Instr_out_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_125,
      O => Instr_out(16)
    );
\Instr_out_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_140,
      O => Instr_out(17)
    );
\Instr_out_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_124,
      O => Instr_out(18)
    );
\Instr_out_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_123,
      O => Instr_out(19)
    );
\Instr_out_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_137,
      O => Instr_out(1)
    );
\Instr_out_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_122,
      O => Instr_out(20)
    );
\Instr_out_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_121,
      O => Instr_out(21)
    );
\Instr_out_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_120,
      O => Instr_out(22)
    );
\Instr_out_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_119,
      O => Instr_out(23)
    );
\Instr_out_OBUF[24]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_118,
      O => Instr_out(24)
    );
\Instr_out_OBUF[25]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_117,
      O => Instr_out(25)
    );
\Instr_out_OBUF[26]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_116,
      O => Instr_out(26)
    );
\Instr_out_OBUF[27]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_115,
      O => Instr_out(27)
    );
\Instr_out_OBUF[28]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_114,
      O => Instr_out(28)
    );
\Instr_out_OBUF[29]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_139,
      O => Instr_out(29)
    );
\Instr_out_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_136,
      O => Instr_out(2)
    );
\Instr_out_OBUF[30]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_113,
      O => Instr_out(30)
    );
\Instr_out_OBUF[31]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_112,
      O => Instr_out(31)
    );
\Instr_out_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_135,
      O => Instr_out(3)
    );
\Instr_out_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_134,
      O => Instr_out(4)
    );
\Instr_out_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_133,
      O => Instr_out(5)
    );
\Instr_out_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_132,
      O => Instr_out(6)
    );
\Instr_out_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_131,
      O => Instr_out(7)
    );
\Instr_out_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATAPATH_n_130,
      O => Instr_out(8)
    );
\Instr_out_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => Instr_out(9)
    );
RESET_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RESET,
      O => RESET_IBUF
    );
\Result_out_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(0),
      O => Result_out(0)
    );
\Result_out_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(10),
      O => Result_out(10)
    );
\Result_out_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(11),
      O => Result_out(11)
    );
\Result_out_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(12),
      O => Result_out(12)
    );
\Result_out_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(13),
      O => Result_out(13)
    );
\Result_out_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(14),
      O => Result_out(14)
    );
\Result_out_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(15),
      O => Result_out(15)
    );
\Result_out_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(16),
      O => Result_out(16)
    );
\Result_out_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(17),
      O => Result_out(17)
    );
\Result_out_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(18),
      O => Result_out(18)
    );
\Result_out_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(19),
      O => Result_out(19)
    );
\Result_out_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(1),
      O => Result_out(1)
    );
\Result_out_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(20),
      O => Result_out(20)
    );
\Result_out_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(21),
      O => Result_out(21)
    );
\Result_out_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(22),
      O => Result_out(22)
    );
\Result_out_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(23),
      O => Result_out(23)
    );
\Result_out_OBUF[24]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(24),
      O => Result_out(24)
    );
\Result_out_OBUF[25]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(25),
      O => Result_out(25)
    );
\Result_out_OBUF[26]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(26),
      O => Result_out(26)
    );
\Result_out_OBUF[27]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(27),
      O => Result_out(27)
    );
\Result_out_OBUF[28]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(28),
      O => Result_out(28)
    );
\Result_out_OBUF[29]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(29),
      O => Result_out(29)
    );
\Result_out_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(2),
      O => Result_out(2)
    );
\Result_out_OBUF[30]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(30),
      O => Result_out(30)
    );
\Result_out_OBUF[31]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(31),
      O => Result_out(31)
    );
\Result_out_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(3),
      O => Result_out(3)
    );
\Result_out_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(4),
      O => Result_out(4)
    );
\Result_out_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(5),
      O => Result_out(5)
    );
\Result_out_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(6),
      O => Result_out(6)
    );
\Result_out_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(7),
      O => Result_out(7)
    );
\Result_out_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(8),
      O => Result_out(8)
    );
\Result_out_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Result_out_OBUF(9),
      O => Result_out(9)
    );
\WriteData_out_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(0),
      O => WriteData_out(0)
    );
\WriteData_out_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(10),
      O => WriteData_out(10)
    );
\WriteData_out_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(11),
      O => WriteData_out(11)
    );
\WriteData_out_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(12),
      O => WriteData_out(12)
    );
\WriteData_out_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(13),
      O => WriteData_out(13)
    );
\WriteData_out_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(14),
      O => WriteData_out(14)
    );
\WriteData_out_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(15),
      O => WriteData_out(15)
    );
\WriteData_out_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(16),
      O => WriteData_out(16)
    );
\WriteData_out_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(17),
      O => WriteData_out(17)
    );
\WriteData_out_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(18),
      O => WriteData_out(18)
    );
\WriteData_out_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(19),
      O => WriteData_out(19)
    );
\WriteData_out_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(1),
      O => WriteData_out(1)
    );
\WriteData_out_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(20),
      O => WriteData_out(20)
    );
\WriteData_out_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(21),
      O => WriteData_out(21)
    );
\WriteData_out_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(22),
      O => WriteData_out(22)
    );
\WriteData_out_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(23),
      O => WriteData_out(23)
    );
\WriteData_out_OBUF[24]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(24),
      O => WriteData_out(24)
    );
\WriteData_out_OBUF[25]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(25),
      O => WriteData_out(25)
    );
\WriteData_out_OBUF[26]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(26),
      O => WriteData_out(26)
    );
\WriteData_out_OBUF[27]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(27),
      O => WriteData_out(27)
    );
\WriteData_out_OBUF[28]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(28),
      O => WriteData_out(28)
    );
\WriteData_out_OBUF[29]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(29),
      O => WriteData_out(29)
    );
\WriteData_out_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(2),
      O => WriteData_out(2)
    );
\WriteData_out_OBUF[30]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(30),
      O => WriteData_out(30)
    );
\WriteData_out_OBUF[31]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(31),
      O => WriteData_out(31)
    );
\WriteData_out_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(3),
      O => WriteData_out(3)
    );
\WriteData_out_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(4),
      O => WriteData_out(4)
    );
\WriteData_out_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(5),
      O => WriteData_out(5)
    );
\WriteData_out_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(6),
      O => WriteData_out(6)
    );
\WriteData_out_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(7),
      O => WriteData_out(7)
    );
\WriteData_out_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(8),
      O => WriteData_out(8)
    );
\WriteData_out_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => WriteData_out_OBUF(9),
      O => WriteData_out(9)
    );
end STRUCTURE;
