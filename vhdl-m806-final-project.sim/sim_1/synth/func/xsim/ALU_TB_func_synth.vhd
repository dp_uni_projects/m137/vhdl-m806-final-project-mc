-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Sun Oct 11 16:58:11 2020
-- Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/mpliax/Documents/workspace/vhdl-m806-final-project-mc/vhdl-m806-final-project.sim/sim_1/synth/func/xsim/ALU_TB_func_synth.vhd
-- Design      : ALU_REGWE_32
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ADDER_n is
  port (
    \Dout_reg[30]\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \Dout_reg[30]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[1]\ : out STD_LOGIC;
    \Dout_reg[1]_0\ : out STD_LOGIC;
    \Dout_reg[1]_1\ : out STD_LOGIC;
    \Dout_reg[1]_2\ : out STD_LOGIC;
    \Dout_reg[1]_3\ : out STD_LOGIC;
    \Dout_reg[1]_4\ : out STD_LOGIC;
    \Dout_reg[1]_5\ : out STD_LOGIC;
    \Dout_reg[1]_6\ : out STD_LOGIC;
    \Dout_reg[1]_7\ : out STD_LOGIC;
    \Dout_reg[1]_8\ : out STD_LOGIC;
    \Dout_reg[1]_9\ : out STD_LOGIC;
    \Dout_reg[1]_10\ : out STD_LOGIC;
    \Dout_reg[1]_11\ : out STD_LOGIC;
    \Dout_reg[1]_12\ : out STD_LOGIC;
    \Dout_reg[1]_13\ : out STD_LOGIC;
    \Dout_reg[1]_14\ : out STD_LOGIC;
    \Dout_reg[1]_15\ : out STD_LOGIC;
    \Dout_reg[1]_16\ : out STD_LOGIC;
    \Dout_reg[1]_17\ : out STD_LOGIC;
    \Dout_reg[1]_18\ : out STD_LOGIC;
    \Dout_reg[1]_19\ : out STD_LOGIC;
    \Dout_reg[1]_20\ : out STD_LOGIC;
    \Dout_reg[1]_21\ : out STD_LOGIC;
    \Dout_reg[1]_22\ : out STD_LOGIC;
    \Dout_reg[1]_23\ : out STD_LOGIC;
    \Dout_reg[1]_24\ : out STD_LOGIC;
    \Dout_reg[1]_25\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout[0]_i_2__0_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[4]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[8]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[12]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[16]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[20]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[24]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[0]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \Dout_reg[0]_0\ : in STD_LOGIC_VECTOR ( 26 downto 0 );
    S_Sub_in : in STD_LOGIC_VECTOR ( 22 downto 0 )
  );
end ADDER_n;

architecture STRUCTURE of ADDER_n is
  signal \^dout_reg[30]\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \S0_carry__0_n_0\ : STD_LOGIC;
  signal \S0_carry__0_n_1\ : STD_LOGIC;
  signal \S0_carry__0_n_2\ : STD_LOGIC;
  signal \S0_carry__0_n_3\ : STD_LOGIC;
  signal \S0_carry__1_n_0\ : STD_LOGIC;
  signal \S0_carry__1_n_1\ : STD_LOGIC;
  signal \S0_carry__1_n_2\ : STD_LOGIC;
  signal \S0_carry__1_n_3\ : STD_LOGIC;
  signal \S0_carry__2_n_0\ : STD_LOGIC;
  signal \S0_carry__2_n_1\ : STD_LOGIC;
  signal \S0_carry__2_n_2\ : STD_LOGIC;
  signal \S0_carry__2_n_3\ : STD_LOGIC;
  signal \S0_carry__3_n_0\ : STD_LOGIC;
  signal \S0_carry__3_n_1\ : STD_LOGIC;
  signal \S0_carry__3_n_2\ : STD_LOGIC;
  signal \S0_carry__3_n_3\ : STD_LOGIC;
  signal \S0_carry__4_n_0\ : STD_LOGIC;
  signal \S0_carry__4_n_1\ : STD_LOGIC;
  signal \S0_carry__4_n_2\ : STD_LOGIC;
  signal \S0_carry__4_n_3\ : STD_LOGIC;
  signal \S0_carry__5_n_0\ : STD_LOGIC;
  signal \S0_carry__5_n_1\ : STD_LOGIC;
  signal \S0_carry__5_n_2\ : STD_LOGIC;
  signal \S0_carry__5_n_3\ : STD_LOGIC;
  signal \S0_carry__6_n_1\ : STD_LOGIC;
  signal \S0_carry__6_n_2\ : STD_LOGIC;
  signal \S0_carry__6_n_3\ : STD_LOGIC;
  signal S0_carry_n_0 : STD_LOGIC;
  signal S0_carry_n_1 : STD_LOGIC;
  signal S0_carry_n_2 : STD_LOGIC;
  signal S0_carry_n_3 : STD_LOGIC;
  signal S_Add_in : STD_LOGIC_VECTOR ( 30 downto 0 );
begin
  \Dout_reg[30]\(5 downto 0) <= \^dout_reg[30]\(5 downto 0);
\Dout[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFAFC0AFC0A0C0A"
    )
        port map (
      I0 => \^dout_reg[30]\(5),
      I1 => O(3),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]\(0),
      I4 => \Dout_reg[0]_0\(26),
      I5 => Q(31),
      O => \Dout_reg[1]\
    );
\Dout[0]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(0),
      I1 => S_Sub_in(0),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(0),
      I4 => Q(0),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_0\
    );
\Dout[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(10),
      I1 => S_Sub_in(5),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(5),
      I4 => Q(10),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_5\
    );
\Dout[11]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(11),
      I1 => S_Sub_in(6),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(6),
      I4 => Q(11),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_6\
    );
\Dout[12]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(12),
      I1 => S_Sub_in(7),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(7),
      I4 => Q(12),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_7\
    );
\Dout[13]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(13),
      I1 => S_Sub_in(8),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(8),
      I4 => Q(13),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_8\
    );
\Dout[14]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(14),
      I1 => S_Sub_in(9),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(9),
      I4 => Q(14),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_9\
    );
\Dout[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(15),
      I1 => S_Sub_in(10),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(10),
      I4 => Q(15),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_10\
    );
\Dout[16]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(16),
      I1 => S_Sub_in(11),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(11),
      I4 => Q(16),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_11\
    );
\Dout[17]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(17),
      I1 => S_Sub_in(12),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(12),
      I4 => Q(17),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_12\
    );
\Dout[18]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(18),
      I1 => S_Sub_in(13),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(13),
      I4 => Q(18),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_13\
    );
\Dout[19]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(19),
      I1 => S_Sub_in(14),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(14),
      I4 => Q(19),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_14\
    );
\Dout[20]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(20),
      I1 => S_Sub_in(15),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(15),
      I4 => Q(20),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_15\
    );
\Dout[21]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(21),
      I1 => S_Sub_in(16),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(16),
      I4 => Q(21),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_16\
    );
\Dout[22]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(22),
      I1 => S_Sub_in(17),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(17),
      I4 => Q(22),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_17\
    );
\Dout[23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(23),
      I1 => S_Sub_in(18),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(18),
      I4 => Q(23),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_18\
    );
\Dout[24]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(24),
      I1 => S_Sub_in(19),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(19),
      I4 => Q(24),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_19\
    );
\Dout[25]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(25),
      I1 => S_Sub_in(20),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(20),
      I4 => Q(25),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_20\
    );
\Dout[26]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(26),
      I1 => S_Sub_in(21),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(21),
      I4 => Q(26),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_21\
    );
\Dout[27]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(27),
      I1 => S_Sub_in(22),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(22),
      I4 => Q(27),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_22\
    );
\Dout[28]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(28),
      I1 => O(0),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(23),
      I4 => Q(28),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_23\
    );
\Dout[29]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(29),
      I1 => O(1),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(24),
      I4 => Q(29),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_24\
    );
\Dout[30]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(30),
      I1 => O(2),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(25),
      I4 => Q(30),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_25\
    );
\Dout[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(6),
      I1 => S_Sub_in(1),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(1),
      I4 => Q(6),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_1\
    );
\Dout[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(7),
      I1 => S_Sub_in(2),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(2),
      I4 => Q(7),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_2\
    );
\Dout[8]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(8),
      I1 => S_Sub_in(3),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(3),
      I4 => Q(8),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_3\
    );
\Dout[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFC0CFA0A0A0A"
    )
        port map (
      I0 => S_Add_in(9),
      I1 => S_Sub_in(4),
      I2 => \Dout_reg[0]\(1),
      I3 => \Dout_reg[0]_0\(4),
      I4 => Q(9),
      I5 => \Dout_reg[0]\(0),
      O => \Dout_reg[1]_4\
    );
S0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => S0_carry_n_0,
      CO(2) => S0_carry_n_1,
      CO(1) => S0_carry_n_2,
      CO(0) => S0_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => Q(3 downto 0),
      O(3 downto 1) => \^dout_reg[30]\(2 downto 0),
      O(0) => S_Add_in(0),
      S(3 downto 0) => \Dout[0]_i_2__0_0\(3 downto 0)
    );
\S0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => S0_carry_n_0,
      CO(3) => \S0_carry__0_n_0\,
      CO(2) => \S0_carry__0_n_1\,
      CO(1) => \S0_carry__0_n_2\,
      CO(0) => \S0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(7 downto 4),
      O(3 downto 2) => S_Add_in(7 downto 6),
      O(1 downto 0) => \^dout_reg[30]\(4 downto 3),
      S(3 downto 0) => \Dout[4]_i_4\(3 downto 0)
    );
\S0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__0_n_0\,
      CO(3) => \S0_carry__1_n_0\,
      CO(2) => \S0_carry__1_n_1\,
      CO(1) => \S0_carry__1_n_2\,
      CO(0) => \S0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(11 downto 8),
      O(3 downto 0) => S_Add_in(11 downto 8),
      S(3 downto 0) => \Dout[8]_i_4_0\(3 downto 0)
    );
\S0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__1_n_0\,
      CO(3) => \S0_carry__2_n_0\,
      CO(2) => \S0_carry__2_n_1\,
      CO(1) => \S0_carry__2_n_2\,
      CO(0) => \S0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(15 downto 12),
      O(3 downto 0) => S_Add_in(15 downto 12),
      S(3 downto 0) => \Dout[12]_i_4_0\(3 downto 0)
    );
\S0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__2_n_0\,
      CO(3) => \S0_carry__3_n_0\,
      CO(2) => \S0_carry__3_n_1\,
      CO(1) => \S0_carry__3_n_2\,
      CO(0) => \S0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(19 downto 16),
      O(3 downto 0) => S_Add_in(19 downto 16),
      S(3 downto 0) => \Dout[16]_i_4_0\(3 downto 0)
    );
\S0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__3_n_0\,
      CO(3) => \S0_carry__4_n_0\,
      CO(2) => \S0_carry__4_n_1\,
      CO(1) => \S0_carry__4_n_2\,
      CO(0) => \S0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(23 downto 20),
      O(3 downto 0) => S_Add_in(23 downto 20),
      S(3 downto 0) => \Dout[20]_i_4_0\(3 downto 0)
    );
\S0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__4_n_0\,
      CO(3) => \S0_carry__5_n_0\,
      CO(2) => \S0_carry__5_n_1\,
      CO(1) => \S0_carry__5_n_2\,
      CO(0) => \S0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(27 downto 24),
      O(3 downto 0) => S_Add_in(27 downto 24),
      S(3 downto 0) => \Dout[24]_i_4_0\(3 downto 0)
    );
\S0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__5_n_0\,
      CO(3) => \Dout_reg[30]_0\(0),
      CO(2) => \S0_carry__6_n_1\,
      CO(1) => \S0_carry__6_n_2\,
      CO(0) => \S0_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => DI(0),
      DI(2 downto 0) => Q(30 downto 28),
      O(3) => \^dout_reg[30]\(5),
      O(2 downto 0) => S_Add_in(30 downto 28),
      S(3 downto 0) => \Dout_reg[3]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n is
  port (
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ALUResult_in : out STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \Dout_reg[3]_0\ : out STD_LOGIC;
    \Dout_reg[3]_1\ : out STD_LOGIC;
    \Dout_reg[2]_0\ : out STD_LOGIC;
    \Dout_reg[3]_2\ : out STD_LOGIC;
    \Dout_reg[1]_0\ : out STD_LOGIC;
    \Dout_reg[0]_0\ : in STD_LOGIC;
    \Dout_reg[0]_1\ : in STD_LOGIC;
    \Dout_reg[0]_2\ : in STD_LOGIC;
    \Dout_reg[6]\ : in STD_LOGIC;
    \Dout_reg[4]\ : in STD_LOGIC;
    \Dout_reg[4]_0\ : in STD_LOGIC;
    \Dout_reg[1]_1\ : in STD_LOGIC;
    \Dout_reg[7]\ : in STD_LOGIC;
    \Dout[1]_i_5_0\ : in STD_LOGIC;
    \Dout_reg[5]\ : in STD_LOGIC;
    \Dout_reg[5]_0\ : in STD_LOGIC;
    \Dout_reg[2]_1\ : in STD_LOGIC;
    \Dout_reg[2]_2\ : in STD_LOGIC;
    \Dout_reg[3]_3\ : in STD_LOGIC;
    \Dout_reg[3]_4\ : in STD_LOGIC;
    \Dout_reg[6]_0\ : in STD_LOGIC;
    \Dout_reg[7]_0\ : in STD_LOGIC;
    \Dout_reg[7]_1\ : in STD_LOGIC;
    \Dout_reg[8]\ : in STD_LOGIC;
    \Dout_reg[8]_0\ : in STD_LOGIC;
    \Dout_reg[9]\ : in STD_LOGIC;
    \Dout_reg[9]_0\ : in STD_LOGIC;
    \Dout_reg[9]_1\ : in STD_LOGIC;
    \Dout_reg[10]\ : in STD_LOGIC;
    \Dout_reg[10]_0\ : in STD_LOGIC;
    \Dout_reg[10]_1\ : in STD_LOGIC;
    \Dout_reg[11]\ : in STD_LOGIC;
    \Dout_reg[11]_0\ : in STD_LOGIC;
    \Dout_reg[11]_1\ : in STD_LOGIC;
    \Dout_reg[12]\ : in STD_LOGIC;
    \Dout_reg[12]_0\ : in STD_LOGIC;
    \Dout_reg[12]_1\ : in STD_LOGIC;
    \Dout_reg[13]\ : in STD_LOGIC;
    \Dout_reg[13]_0\ : in STD_LOGIC;
    \Dout_reg[13]_1\ : in STD_LOGIC;
    \Dout_reg[14]\ : in STD_LOGIC;
    \Dout_reg[14]_0\ : in STD_LOGIC;
    \Dout_reg[14]_1\ : in STD_LOGIC;
    \Dout_reg[15]\ : in STD_LOGIC;
    \Dout_reg[15]_0\ : in STD_LOGIC;
    \Dout_reg[15]_1\ : in STD_LOGIC;
    \Dout_reg[16]\ : in STD_LOGIC;
    \Dout_reg[16]_0\ : in STD_LOGIC;
    \Dout_reg[16]_1\ : in STD_LOGIC;
    \Dout_reg[17]\ : in STD_LOGIC;
    \Dout_reg[17]_0\ : in STD_LOGIC;
    \Dout_reg[17]_1\ : in STD_LOGIC;
    \Dout_reg[18]\ : in STD_LOGIC;
    \Dout_reg[18]_0\ : in STD_LOGIC;
    \Dout_reg[18]_1\ : in STD_LOGIC;
    \Dout_reg[19]\ : in STD_LOGIC;
    \Dout_reg[19]_0\ : in STD_LOGIC;
    \Dout_reg[19]_1\ : in STD_LOGIC;
    \Dout_reg[20]\ : in STD_LOGIC;
    \Dout_reg[20]_0\ : in STD_LOGIC;
    \Dout_reg[20]_1\ : in STD_LOGIC;
    \Dout_reg[21]\ : in STD_LOGIC;
    \Dout_reg[21]_0\ : in STD_LOGIC;
    \Dout_reg[21]_1\ : in STD_LOGIC;
    \Dout_reg[22]\ : in STD_LOGIC;
    \Dout_reg[22]_0\ : in STD_LOGIC;
    \Dout_reg[22]_1\ : in STD_LOGIC;
    \Dout_reg[23]\ : in STD_LOGIC;
    \Dout_reg[23]_0\ : in STD_LOGIC;
    \Dout_reg[24]\ : in STD_LOGIC;
    \Dout_reg[24]_0\ : in STD_LOGIC;
    \Dout_reg[25]\ : in STD_LOGIC;
    \Dout_reg[25]_0\ : in STD_LOGIC;
    \Dout_reg[26]\ : in STD_LOGIC;
    \Dout_reg[26]_0\ : in STD_LOGIC;
    \Dout_reg[26]_1\ : in STD_LOGIC;
    \Dout_reg[27]\ : in STD_LOGIC;
    \Dout_reg[27]_0\ : in STD_LOGIC;
    \Dout_reg[28]\ : in STD_LOGIC;
    \Dout_reg[28]_0\ : in STD_LOGIC;
    \Dout_reg[29]\ : in STD_LOGIC;
    \Dout_reg[29]_0\ : in STD_LOGIC;
    \Dout_reg[30]\ : in STD_LOGIC;
    \Dout_reg[30]_0\ : in STD_LOGIC;
    \Dout_reg[0]_3\ : in STD_LOGIC;
    \Dout_reg[0]_4\ : in STD_LOGIC;
    \Dout_reg[0]_5\ : in STD_LOGIC;
    \Dout_reg[1]_2\ : in STD_LOGIC;
    \Dout_reg[1]_3\ : in STD_LOGIC;
    \Dout_reg[5]_1\ : in STD_LOGIC;
    \Dout_reg[5]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[5]_3\ : in STD_LOGIC;
    \Dout_reg[5]_4\ : in STD_LOGIC;
    \Dout[1]_i_10_0\ : in STD_LOGIC;
    \Dout_reg[6]_1\ : in STD_LOGIC;
    \Dout_reg[6]_2\ : in STD_LOGIC;
    \Dout_reg[6]_3\ : in STD_LOGIC;
    \Dout[1]_i_8\ : in STD_LOGIC;
    \Dout_reg[8]_1\ : in STD_LOGIC;
    \Dout_reg[8]_2\ : in STD_LOGIC;
    \Dout_reg[8]_3\ : in STD_LOGIC;
    \Dout_reg[1]_4\ : in STD_LOGIC;
    \Dout_reg[1]_5\ : in STD_LOGIC;
    \Dout_reg[1]_6\ : in STD_LOGIC;
    \Dout_reg[2]_3\ : in STD_LOGIC;
    \Dout_reg[2]_4\ : in STD_LOGIC;
    \Dout_reg[3]_5\ : in STD_LOGIC;
    \Dout_reg[3]_6\ : in STD_LOGIC;
    \Dout_reg[4]_1\ : in STD_LOGIC;
    \Dout_reg[23]_1\ : in STD_LOGIC;
    \Dout_reg[23]_2\ : in STD_LOGIC;
    \Dout_reg[23]_3\ : in STD_LOGIC;
    \Dout_reg[24]_1\ : in STD_LOGIC;
    \Dout_reg[24]_2\ : in STD_LOGIC;
    \Dout_reg[25]_1\ : in STD_LOGIC;
    \Dout_reg[25]_2\ : in STD_LOGIC;
    \Dout_reg[27]_1\ : in STD_LOGIC;
    \Dout_reg[27]_2\ : in STD_LOGIC;
    \Dout_reg[27]_3\ : in STD_LOGIC;
    \Dout_reg[28]_1\ : in STD_LOGIC;
    \Dout_reg[28]_2\ : in STD_LOGIC;
    \Dout_reg[29]_1\ : in STD_LOGIC;
    \Dout_reg[29]_2\ : in STD_LOGIC;
    \Dout_reg[30]_1\ : in STD_LOGIC;
    \Dout_reg[30]_2\ : in STD_LOGIC;
    \Dout_reg[0]_6\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[0]_7\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[3]_7\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \Dout_reg[3]_8\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    O : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[2]_5\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]_9\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]_10\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
end REGrwe_n;

architecture STRUCTURE of REGrwe_n is
  signal ALUControl_in : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \^aluresult_in\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \Dout[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \Dout[0]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[10]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[11]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[12]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_11__0_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_16_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_2__0_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_3__0_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_4__0_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_4_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_5_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_9__0_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[23]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[23]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[24]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[24]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[25]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[25]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[26]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[27]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[27]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[28]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[28]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[29]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[29]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[2]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[2]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[2]_i_4_n_0\ : STD_LOGIC;
  signal \Dout[2]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_5_n_0\ : STD_LOGIC;
  signal \Dout[3]_i_2__0_n_0\ : STD_LOGIC;
  signal \Dout[3]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[3]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[3]_i_4_n_0\ : STD_LOGIC;
  signal \Dout[3]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[4]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[4]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[4]_i_4_n_0\ : STD_LOGIC;
  signal \Dout[4]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_4_n_0\ : STD_LOGIC;
  signal \Dout[6]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[6]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[7]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[8]_i_2_n_0\ : STD_LOGIC;
  signal \Dout[8]_i_3_n_0\ : STD_LOGIC;
  signal \Dout[9]_i_3_n_0\ : STD_LOGIC;
  signal \^dout_reg[3]_0\ : STD_LOGIC;
  signal \^dout_reg[3]_1\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Dout[0]_i_9\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \Dout[1]_i_15\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \Dout[2]_i_9\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \Dout[30]_i_8\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \Dout[3]_i_2__0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \Dout[3]_i_9\ : label is "soft_lutpair2";
begin
  ALUResult_in(31 downto 0) <= \^aluresult_in\(31 downto 0);
  \Dout_reg[3]_0\ <= \^dout_reg[3]_0\;
  \Dout_reg[3]_1\ <= \^dout_reg[3]_1\;
  Q(1 downto 0) <= \^q\(1 downto 0);
\Dout[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF4FFF4FFF4F0F4"
    )
        port map (
      I0 => ALUControl_in(2),
      I1 => \Dout_reg[0]_0\,
      I2 => \Dout[0]_i_3__0_n_0\,
      I3 => ALUControl_in(3),
      I4 => \Dout_reg[0]_1\,
      I5 => \Dout_reg[0]_2\,
      O => \^aluresult_in\(31)
    );
\Dout[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF4FFF4FFF4F0F4"
    )
        port map (
      I0 => ALUControl_in(2),
      I1 => \Dout_reg[0]_3\,
      I2 => \Dout[0]_i_3_n_0\,
      I3 => ALUControl_in(3),
      I4 => \Dout_reg[0]_4\,
      I5 => \Dout_reg[0]_5\,
      O => \^aluresult_in\(0)
    );
\Dout[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(0),
      I3 => \Dout_reg[0]_7\(0),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[0]_i_3_n_0\
    );
\Dout[0]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(31),
      I3 => \Dout_reg[0]_7\(31),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[0]_i_3__0_n_0\
    );
\Dout[0]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => \Dout_reg[1]_0\
    );
\Dout[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[10]\,
      I1 => \Dout[10]_i_3_n_0\,
      I2 => \Dout_reg[10]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[10]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(10)
    );
\Dout[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(10),
      I3 => \Dout_reg[0]_7\(10),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[10]_i_3_n_0\
    );
\Dout[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[11]\,
      I1 => \Dout[11]_i_3_n_0\,
      I2 => \Dout_reg[11]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[11]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(11)
    );
\Dout[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(11),
      I3 => \Dout_reg[0]_7\(11),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[11]_i_3_n_0\
    );
\Dout[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[12]\,
      I1 => \Dout[12]_i_3_n_0\,
      I2 => \Dout_reg[12]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[12]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(12)
    );
\Dout[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(12),
      I3 => \Dout_reg[0]_7\(12),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[12]_i_3_n_0\
    );
\Dout[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[13]\,
      I1 => \Dout[13]_i_3_n_0\,
      I2 => \Dout_reg[13]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[13]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(13)
    );
\Dout[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(13),
      I3 => \Dout_reg[0]_7\(13),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[13]_i_3_n_0\
    );
\Dout[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[14]\,
      I1 => \Dout[14]_i_3_n_0\,
      I2 => \Dout_reg[14]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[14]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(14)
    );
\Dout[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(14),
      I3 => \Dout_reg[0]_7\(14),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[14]_i_3_n_0\
    );
\Dout[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[15]\,
      I1 => \Dout[15]_i_3_n_0\,
      I2 => \Dout_reg[15]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[15]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(15)
    );
\Dout[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(15),
      I3 => \Dout_reg[0]_7\(15),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[15]_i_3_n_0\
    );
\Dout[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[16]\,
      I1 => \Dout[16]_i_3_n_0\,
      I2 => \Dout_reg[16]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[16]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(16)
    );
\Dout[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(16),
      I3 => \Dout_reg[0]_7\(16),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[16]_i_3_n_0\
    );
\Dout[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[17]\,
      I1 => \Dout[17]_i_3_n_0\,
      I2 => \Dout_reg[17]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[17]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(17)
    );
\Dout[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(17),
      I3 => \Dout_reg[0]_7\(17),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[17]_i_3_n_0\
    );
\Dout[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[18]_0\,
      I1 => \Dout[18]_i_3_n_0\,
      I2 => \Dout_reg[18]_1\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[18]\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(18)
    );
\Dout[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(18),
      I3 => \Dout_reg[0]_7\(18),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[18]_i_3_n_0\
    );
\Dout[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[19]\,
      I1 => \Dout[19]_i_3_n_0\,
      I2 => \Dout_reg[19]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[19]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(19)
    );
\Dout[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(19),
      I3 => \Dout_reg[0]_7\(19),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[19]_i_3_n_0\
    );
\Dout[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000004000"
    )
        port map (
      I0 => \^aluresult_in\(31),
      I1 => \Dout[1]_i_2_n_0\,
      I2 => \Dout[1]_i_3_n_0\,
      I3 => \Dout[1]_i_4_n_0\,
      I4 => \Dout[1]_i_5_n_0\,
      I5 => \Dout[1]_i_6_n_0\,
      O => D(0)
    );
\Dout[1]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \^aluresult_in\(4),
      I1 => \Dout[1]_i_16_n_0\,
      I2 => \Dout_reg[6]\,
      I3 => \^dout_reg[3]_0\,
      I4 => \^aluresult_in\(2),
      I5 => \^aluresult_in\(3),
      O => \Dout[1]_i_10_n_0\
    );
\Dout[1]_i_11__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAAE"
    )
        port map (
      I0 => \Dout[7]_i_3_n_0\,
      I1 => \Dout_reg[7]_1\,
      I2 => ALUControl_in(3),
      I3 => ALUControl_in(2),
      O => \Dout[1]_i_11__0_n_0\
    );
\Dout[1]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAAE"
    )
        port map (
      I0 => \Dout[9]_i_3_n_0\,
      I1 => \Dout_reg[9]_0\,
      I2 => ALUControl_in(3),
      I3 => ALUControl_in(2),
      O => \Dout_reg[3]_2\
    );
\Dout[1]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0F4F4FFF0F4F4"
    )
        port map (
      I0 => ALUControl_in(2),
      I1 => \Dout_reg[8]\,
      I2 => \Dout[8]_i_3_n_0\,
      I3 => \Dout[1]_i_8\,
      I4 => ALUControl_in(3),
      I5 => \^q\(1),
      O => \Dout_reg[2]_0\
    );
\Dout[1]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAAE"
    )
        port map (
      I0 => \Dout[18]_i_3_n_0\,
      I1 => \Dout_reg[18]_1\,
      I2 => ALUControl_in(3),
      I3 => ALUControl_in(2),
      O => \Dout[1]_i_15_n_0\
    );
\Dout[1]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0F4F4FFF0F4F4"
    )
        port map (
      I0 => ALUControl_in(2),
      I1 => \Dout_reg[6]_0\,
      I2 => \Dout[6]_i_3_n_0\,
      I3 => \Dout[1]_i_10_0\,
      I4 => ALUControl_in(3),
      I5 => \^q\(1),
      O => \Dout[1]_i_16_n_0\
    );
\Dout[1]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFEFEFEFE"
    )
        port map (
      I0 => \Dout[1]_i_2__0_n_0\,
      I1 => \Dout[1]_i_3__0_n_0\,
      I2 => \Dout[1]_i_4__0_n_0\,
      I3 => \Dout_reg[1]_2\,
      I4 => \Dout_reg[1]_3\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(1)
    );
\Dout[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \^aluresult_in\(19),
      I1 => \^aluresult_in\(20),
      I2 => \^aluresult_in\(21),
      I3 => \^aluresult_in\(30),
      O => \Dout[1]_i_2_n_0\
    );
\Dout[1]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAA8080000"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[1]_4\,
      I2 => \Dout_reg[5]_2\(0),
      I3 => \Dout_reg[1]_5\,
      I4 => \^q\(0),
      I5 => \Dout_reg[1]_6\,
      O => \Dout[1]_i_2__0_n_0\
    );
\Dout[1]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \^aluresult_in\(25),
      I1 => \^aluresult_in\(24),
      I2 => \^aluresult_in\(23),
      I3 => \^aluresult_in\(22),
      O => \Dout[1]_i_3_n_0\
    );
\Dout[1]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(1),
      I3 => \Dout_reg[0]_7\(1),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[1]_i_3__0_n_0\
    );
\Dout[1]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \^aluresult_in\(29),
      I1 => \^aluresult_in\(28),
      I2 => \^aluresult_in\(27),
      I3 => \^aluresult_in\(26),
      O => \Dout[1]_i_4_n_0\
    );
\Dout[1]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88A888AA88A88888"
    )
        port map (
      I0 => \Dout[30]_i_5_n_0\,
      I1 => \Dout[1]_i_9__0_n_0\,
      I2 => \Dout_reg[3]_7\(0),
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \Dout_reg[3]_8\(0),
      O => \Dout[1]_i_4__0_n_0\
    );
\Dout[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \^aluresult_in\(11),
      I1 => \^aluresult_in\(10),
      I2 => \^aluresult_in\(13),
      I3 => \^aluresult_in\(12),
      I4 => \Dout[1]_i_7_n_0\,
      I5 => \Dout_reg[1]_1\,
      O => \Dout[1]_i_5_n_0\
    );
\Dout[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \Dout[1]_i_9_n_0\,
      I1 => \Dout[1]_i_10_n_0\,
      I2 => \^aluresult_in\(15),
      I3 => \^aluresult_in\(14),
      I4 => \^aluresult_in\(17),
      I5 => \^aluresult_in\(16),
      O => \Dout[1]_i_6_n_0\
    );
\Dout[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFC4FF80"
    )
        port map (
      I0 => \^q\(1),
      I1 => ALUControl_in(3),
      I2 => \Dout_reg[7]\,
      I3 => \Dout[1]_i_11__0_n_0\,
      I4 => \Dout[1]_i_5_0\,
      I5 => \^aluresult_in\(5),
      O => \Dout[1]_i_7_n_0\
    );
\Dout[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \^aluresult_in\(0),
      I1 => \^aluresult_in\(1),
      I2 => \^dout_reg[3]_0\,
      I3 => \Dout_reg[18]\,
      I4 => \Dout[1]_i_15_n_0\,
      I5 => \Dout_reg[18]_0\,
      O => \Dout[1]_i_9_n_0\
    );
\Dout[1]_i_9__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E800"
    )
        port map (
      I0 => \^q\(0),
      I1 => \Dout_reg[0]_6\(1),
      I2 => \Dout_reg[0]_7\(1),
      I3 => \^q\(1),
      O => \Dout[1]_i_9__0_n_0\
    );
\Dout[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[20]\,
      I1 => \Dout[20]_i_3_n_0\,
      I2 => \Dout_reg[20]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[20]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(20)
    );
\Dout[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(20),
      I3 => \Dout_reg[0]_7\(20),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[20]_i_3_n_0\
    );
\Dout[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[21]\,
      I1 => \Dout[21]_i_3_n_0\,
      I2 => \Dout_reg[21]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[21]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(21)
    );
\Dout[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(21),
      I3 => \Dout_reg[0]_7\(21),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[21]_i_3_n_0\
    );
\Dout[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[22]\,
      I1 => \Dout[22]_i_3_n_0\,
      I2 => \Dout_reg[22]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[22]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(22)
    );
\Dout[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(22),
      I3 => \Dout_reg[0]_7\(22),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[22]_i_3_n_0\
    );
\Dout[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout[23]_i_2_n_0\,
      I1 => \Dout[23]_i_3_n_0\,
      I2 => \Dout_reg[23]\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[23]_0\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(23)
    );
\Dout[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[23]_1\,
      I2 => \^q\(0),
      I3 => \Dout_reg[23]_2\,
      I4 => \Dout_reg[23]_3\,
      I5 => \Dout_reg[5]_2\(0),
      O => \Dout[23]_i_2_n_0\
    );
\Dout[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(23),
      I3 => \Dout_reg[0]_7\(23),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[23]_i_3_n_0\
    );
\Dout[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout[24]_i_2_n_0\,
      I1 => \Dout[24]_i_3_n_0\,
      I2 => \Dout_reg[24]\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[24]_0\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(24)
    );
\Dout[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[24]_1\,
      I2 => \^q\(0),
      I3 => \Dout_reg[23]_3\,
      I4 => \Dout_reg[24]_2\,
      I5 => \Dout_reg[5]_2\(0),
      O => \Dout[24]_i_2_n_0\
    );
\Dout[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(24),
      I3 => \Dout_reg[0]_7\(24),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[24]_i_3_n_0\
    );
\Dout[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout[25]_i_2_n_0\,
      I1 => \Dout[25]_i_3_n_0\,
      I2 => \Dout_reg[25]\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[25]_0\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(25)
    );
\Dout[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[25]_1\,
      I2 => \^q\(0),
      I3 => \Dout_reg[24]_2\,
      I4 => \Dout_reg[25]_2\,
      I5 => \Dout_reg[5]_2\(0),
      O => \Dout[25]_i_2_n_0\
    );
\Dout[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(25),
      I3 => \Dout_reg[0]_7\(25),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[25]_i_3_n_0\
    );
\Dout[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[26]\,
      I1 => \Dout[26]_i_3_n_0\,
      I2 => \Dout_reg[26]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[26]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(26)
    );
\Dout[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(26),
      I3 => \Dout_reg[0]_7\(26),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[26]_i_3_n_0\
    );
\Dout[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout[27]_i_2_n_0\,
      I1 => \Dout[27]_i_3_n_0\,
      I2 => \Dout_reg[27]\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[27]_0\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(27)
    );
\Dout[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[27]_1\,
      I2 => \^q\(0),
      I3 => \Dout_reg[27]_2\,
      I4 => \Dout_reg[27]_3\,
      I5 => \Dout_reg[5]_2\(0),
      O => \Dout[27]_i_2_n_0\
    );
\Dout[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(27),
      I3 => \Dout_reg[0]_7\(27),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[27]_i_3_n_0\
    );
\Dout[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout[28]_i_2_n_0\,
      I1 => \Dout[28]_i_3_n_0\,
      I2 => \Dout_reg[28]\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[28]_0\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(28)
    );
\Dout[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[28]_1\,
      I2 => \^q\(0),
      I3 => \Dout_reg[27]_3\,
      I4 => \Dout_reg[28]_2\,
      I5 => \Dout_reg[5]_2\(0),
      O => \Dout[28]_i_2_n_0\
    );
\Dout[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(28),
      I3 => \Dout_reg[0]_7\(28),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[28]_i_3_n_0\
    );
\Dout[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout[29]_i_2_n_0\,
      I1 => \Dout[29]_i_3_n_0\,
      I2 => \Dout_reg[29]\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[29]_0\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(29)
    );
\Dout[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[29]_1\,
      I2 => \^q\(0),
      I3 => \Dout_reg[28]_2\,
      I4 => \Dout_reg[29]_2\,
      I5 => \Dout_reg[5]_2\(0),
      O => \Dout[29]_i_2_n_0\
    );
\Dout[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(29),
      I3 => \Dout_reg[0]_7\(29),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[29]_i_3_n_0\
    );
\Dout[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFEFEFEFE"
    )
        port map (
      I0 => \Dout[2]_i_2_n_0\,
      I1 => \Dout[2]_i_3_n_0\,
      I2 => \Dout[2]_i_4_n_0\,
      I3 => \Dout_reg[2]_1\,
      I4 => \Dout_reg[2]_2\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(2)
    );
\Dout[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000200030000"
    )
        port map (
      I0 => O(0),
      I1 => ALUControl_in(2),
      I2 => ALUControl_in(3),
      I3 => \^q\(1),
      I4 => \Dout_reg[2]_5\(0),
      I5 => \^q\(0),
      O => D(1)
    );
\Dout[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAA8080000"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[1]_5\,
      I2 => \Dout_reg[5]_2\(0),
      I3 => \Dout_reg[2]_3\,
      I4 => \^q\(0),
      I5 => \Dout_reg[2]_4\,
      O => \Dout[2]_i_2_n_0\
    );
\Dout[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(2),
      I3 => \Dout_reg[0]_7\(2),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[2]_i_3_n_0\
    );
\Dout[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88A888AA88A88888"
    )
        port map (
      I0 => \Dout[30]_i_5_n_0\,
      I1 => \Dout[2]_i_9_n_0\,
      I2 => \Dout_reg[3]_7\(1),
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \Dout_reg[3]_8\(1),
      O => \Dout[2]_i_4_n_0\
    );
\Dout[2]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E800"
    )
        port map (
      I0 => \^q\(0),
      I1 => \Dout_reg[0]_6\(2),
      I2 => \Dout_reg[0]_7\(2),
      I3 => \^q\(1),
      O => \Dout[2]_i_9_n_0\
    );
\Dout[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout[30]_i_2_n_0\,
      I1 => \Dout[30]_i_3_n_0\,
      I2 => \Dout_reg[30]\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[30]_0\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(30)
    );
\Dout[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[30]_1\,
      I2 => \^q\(0),
      I3 => \Dout_reg[29]_2\,
      I4 => \Dout_reg[30]_2\,
      I5 => \Dout_reg[5]_2\(0),
      O => \Dout[30]_i_2_n_0\
    );
\Dout[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(30),
      I3 => \Dout_reg[0]_7\(30),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[30]_i_3_n_0\
    );
\Dout[30]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      O => \Dout[30]_i_5_n_0\
    );
\Dout[30]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => \^q\(1),
      O => \^dout_reg[3]_0\
    );
\Dout[30]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => \^q\(1),
      O => \^dout_reg[3]_1\
    );
\Dout[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFEFEFEFE"
    )
        port map (
      I0 => \Dout[3]_i_2_n_0\,
      I1 => \Dout[3]_i_3_n_0\,
      I2 => \Dout[3]_i_4_n_0\,
      I3 => \Dout_reg[3]_3\,
      I4 => \Dout_reg[3]_4\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(3)
    );
\Dout[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A88A20022002A88A"
    )
        port map (
      I0 => \Dout[3]_i_2__0_n_0\,
      I1 => \^q\(0),
      I2 => CO(0),
      I3 => \Dout_reg[3]_8\(5),
      I4 => \Dout_reg[3]_9\(0),
      I5 => \Dout_reg[3]_7\(5),
      O => D(2)
    );
\Dout[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAA8080000"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[2]_3\,
      I2 => \Dout_reg[5]_2\(0),
      I3 => \Dout_reg[3]_5\,
      I4 => \^q\(0),
      I5 => \Dout_reg[3]_6\,
      O => \Dout[3]_i_2_n_0\
    );
\Dout[3]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ALUControl_in(2),
      I1 => ALUControl_in(3),
      I2 => \^q\(1),
      O => \Dout[3]_i_2__0_n_0\
    );
\Dout[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(3),
      I3 => \Dout_reg[0]_7\(3),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[3]_i_3_n_0\
    );
\Dout[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88A888AA88A88888"
    )
        port map (
      I0 => \Dout[30]_i_5_n_0\,
      I1 => \Dout[3]_i_9_n_0\,
      I2 => \Dout_reg[3]_7\(2),
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \Dout_reg[3]_8\(2),
      O => \Dout[3]_i_4_n_0\
    );
\Dout[3]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E800"
    )
        port map (
      I0 => \^q\(0),
      I1 => \Dout_reg[0]_6\(3),
      I2 => \Dout_reg[0]_7\(3),
      I3 => \^q\(1),
      O => \Dout[3]_i_9_n_0\
    );
\Dout[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFEFEFEFE"
    )
        port map (
      I0 => \Dout[4]_i_2_n_0\,
      I1 => \Dout[4]_i_3_n_0\,
      I2 => \Dout[4]_i_4_n_0\,
      I3 => \Dout_reg[4]\,
      I4 => \Dout_reg[4]_0\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(4)
    );
\Dout[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAA8080000"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[3]_5\,
      I2 => \Dout_reg[5]_2\(0),
      I3 => \Dout_reg[5]_1\,
      I4 => \^q\(0),
      I5 => \Dout_reg[4]_1\,
      O => \Dout[4]_i_2_n_0\
    );
\Dout[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(4),
      I3 => \Dout_reg[0]_7\(4),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[4]_i_3_n_0\
    );
\Dout[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88A888AA88A88888"
    )
        port map (
      I0 => \Dout[30]_i_5_n_0\,
      I1 => \Dout[4]_i_9_n_0\,
      I2 => \Dout_reg[3]_7\(3),
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \Dout_reg[3]_8\(3),
      O => \Dout[4]_i_4_n_0\
    );
\Dout[4]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E800"
    )
        port map (
      I0 => \^q\(0),
      I1 => \Dout_reg[0]_6\(4),
      I2 => \Dout_reg[0]_7\(4),
      I3 => \^q\(1),
      O => \Dout[4]_i_9_n_0\
    );
\Dout[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFEFEFEFE"
    )
        port map (
      I0 => \Dout[5]_i_2_n_0\,
      I1 => \Dout[5]_i_3_n_0\,
      I2 => \Dout[5]_i_4_n_0\,
      I3 => \Dout_reg[5]\,
      I4 => \Dout_reg[5]_0\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(5)
    );
\Dout[5]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E800"
    )
        port map (
      I0 => \^q\(0),
      I1 => \Dout_reg[0]_6\(5),
      I2 => \Dout_reg[0]_7\(5),
      I3 => \^q\(1),
      O => \Dout[5]_i_10_n_0\
    );
\Dout[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAA8080000"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[5]_1\,
      I2 => \Dout_reg[5]_2\(0),
      I3 => \Dout_reg[5]_3\,
      I4 => \^q\(0),
      I5 => \Dout_reg[5]_4\,
      O => \Dout[5]_i_2_n_0\
    );
\Dout[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(5),
      I3 => \Dout_reg[0]_7\(5),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[5]_i_3_n_0\
    );
\Dout[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88A888AA88A88888"
    )
        port map (
      I0 => \Dout[30]_i_5_n_0\,
      I1 => \Dout[5]_i_10_n_0\,
      I2 => \Dout_reg[3]_7\(4),
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \Dout_reg[3]_8\(4),
      O => \Dout[5]_i_4_n_0\
    );
\Dout[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout[6]_i_2_n_0\,
      I1 => \Dout[6]_i_3_n_0\,
      I2 => \Dout_reg[6]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[6]\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(6)
    );
\Dout[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[6]_1\,
      I2 => \^q\(0),
      I3 => \Dout_reg[6]_2\,
      I4 => \Dout_reg[6]_3\,
      I5 => \Dout_reg[5]_2\(0),
      O => \Dout[6]_i_2_n_0\
    );
\Dout[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(6),
      I3 => \Dout_reg[0]_7\(6),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[6]_i_3_n_0\
    );
\Dout[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[7]_0\,
      I1 => \Dout[7]_i_3_n_0\,
      I2 => \Dout_reg[7]_1\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[7]\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(7)
    );
\Dout[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(7),
      I3 => \Dout_reg[0]_7\(7),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[7]_i_3_n_0\
    );
\Dout[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout[8]_i_2_n_0\,
      I1 => \Dout[8]_i_3_n_0\,
      I2 => \Dout_reg[8]\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[8]_0\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(8)
    );
\Dout[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \^dout_reg[3]_1\,
      I1 => \Dout_reg[8]_1\,
      I2 => \^q\(0),
      I3 => \Dout_reg[8]_2\,
      I4 => \Dout_reg[8]_3\,
      I5 => \Dout_reg[5]_2\(0),
      O => \Dout[8]_i_2_n_0\
    );
\Dout[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(8),
      I3 => \Dout_reg[0]_7\(8),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[8]_i_3_n_0\
    );
\Dout[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \Dout_reg[9]\,
      I1 => \Dout[9]_i_3_n_0\,
      I2 => \Dout_reg[9]_0\,
      I3 => \Dout[30]_i_5_n_0\,
      I4 => \Dout_reg[9]_1\,
      I5 => \^dout_reg[3]_0\,
      O => \^aluresult_in\(9)
    );
\Dout[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0440044000444400"
    )
        port map (
      I0 => ALUControl_in(3),
      I1 => ALUControl_in(2),
      I2 => \Dout_reg[0]_6\(9),
      I3 => \Dout_reg[0]_7\(9),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[9]_i_3_n_0\
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[3]_10\(0),
      Q => \^q\(0),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[3]_10\(1),
      Q => \^q\(1),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[3]_10\(2),
      Q => ALUControl_in(2),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => \Dout_reg[3]_10\(3),
      Q => ALUControl_in(3),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_1 is
  port (
    \Dout_reg[0]_0\ : out STD_LOGIC;
    ALUResult_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    ALUResult_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_IBUF_BUFG : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_1 : entity is "REGrwe_n";
end REGrwe_n_1;

architecture STRUCTURE of REGrwe_n_1 is
  attribute IOB : string;
  attribute IOB of \Dout_reg[0]\ : label is "TRUE";
  attribute ORIG_CELL_NAME : string;
  attribute ORIG_CELL_NAME of \Dout_reg[0]\ : label is "Dout_reg[0]";
  attribute IOB of \Dout_reg[0]_rep\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \Dout_reg[0]_rep\ : label is "Dout_reg[0]";
  attribute IOB of \Dout_reg[1]\ : label is "TRUE";
  attribute IOB of \Dout_reg[2]\ : label is "TRUE";
  attribute IOB of \Dout_reg[3]\ : label is "TRUE";
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_in(0),
      Q => \Dout_reg[0]_0\,
      R => SR(0)
    );
\Dout_reg[0]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => ALUResult_in(0),
      Q => ALUResult_OBUF(0),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(0),
      Q => Q(0),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(1),
      Q => Q(1),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(2),
      Q => Q(2),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \REGrwe_n__parameterized1\ is
  port (
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[11]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[15]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[19]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[23]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[27]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[31]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[3]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \S0_carry__6\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \REGrwe_n__parameterized1\ : entity is "REGrwe_n";
end \REGrwe_n__parameterized1\;

architecture STRUCTURE of \REGrwe_n__parameterized1\ is
  signal \^q\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_Dout_reg[3]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_Dout_reg[3]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  Q(31 downto 0) <= \^q\(31 downto 0);
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(0),
      Q => \^q\(0),
      R => SR(0)
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(10),
      Q => \^q\(10),
      R => SR(0)
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(11),
      Q => \^q\(11),
      R => SR(0)
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(12),
      Q => \^q\(12),
      R => SR(0)
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(13),
      Q => \^q\(13),
      R => SR(0)
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(14),
      Q => \^q\(14),
      R => SR(0)
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(15),
      Q => \^q\(15),
      R => SR(0)
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(16),
      Q => \^q\(16),
      R => SR(0)
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(17),
      Q => \^q\(17),
      R => SR(0)
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(18),
      Q => \^q\(18),
      R => SR(0)
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(19),
      Q => \^q\(19),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(1),
      Q => \^q\(1),
      R => SR(0)
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(20),
      Q => \^q\(20),
      R => SR(0)
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(21),
      Q => \^q\(21),
      R => SR(0)
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(22),
      Q => \^q\(22),
      R => SR(0)
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(23),
      Q => \^q\(23),
      R => SR(0)
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(24),
      Q => \^q\(24),
      R => SR(0)
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(25),
      Q => \^q\(25),
      R => SR(0)
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(26),
      Q => \^q\(26),
      R => SR(0)
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(27),
      Q => \^q\(27),
      R => SR(0)
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(28),
      Q => \^q\(28),
      R => SR(0)
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(29),
      Q => \^q\(29),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(2),
      Q => \^q\(2),
      R => SR(0)
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(30),
      Q => \^q\(30),
      R => SR(0)
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(31),
      Q => \^q\(31),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(3),
      Q => \^q\(3),
      R => SR(0)
    );
\Dout_reg[3]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \Dout_reg[3]_1\(0),
      CO(3 downto 1) => \NLW_Dout_reg[3]_i_3_CO_UNCONNECTED\(3 downto 1),
      CO(0) => CO(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_Dout_reg[3]_i_3_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(4),
      Q => \^q\(4),
      R => SR(0)
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(5),
      Q => \^q\(5),
      R => SR(0)
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(6),
      Q => \^q\(6),
      R => SR(0)
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(7),
      Q => \^q\(7),
      R => SR(0)
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(8),
      Q => \^q\(8),
      R => SR(0)
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(9),
      Q => \^q\(9),
      R => SR(0)
    );
\S0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(7),
      I1 => \S0_carry__6\(7),
      O => \Dout_reg[7]_0\(3)
    );
\S0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(6),
      I1 => \S0_carry__6\(6),
      O => \Dout_reg[7]_0\(2)
    );
\S0_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(5),
      I1 => \S0_carry__6\(5),
      O => \Dout_reg[7]_0\(1)
    );
\S0_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(4),
      I1 => \S0_carry__6\(4),
      O => \Dout_reg[7]_0\(0)
    );
\S0_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(11),
      I1 => \S0_carry__6\(11),
      O => \Dout_reg[11]_0\(3)
    );
\S0_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(10),
      I1 => \S0_carry__6\(10),
      O => \Dout_reg[11]_0\(2)
    );
\S0_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(9),
      I1 => \S0_carry__6\(9),
      O => \Dout_reg[11]_0\(1)
    );
\S0_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(8),
      I1 => \S0_carry__6\(8),
      O => \Dout_reg[11]_0\(0)
    );
\S0_carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(15),
      I1 => \S0_carry__6\(15),
      O => \Dout_reg[15]_0\(3)
    );
\S0_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(14),
      I1 => \S0_carry__6\(14),
      O => \Dout_reg[15]_0\(2)
    );
\S0_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(13),
      I1 => \S0_carry__6\(13),
      O => \Dout_reg[15]_0\(1)
    );
\S0_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(12),
      I1 => \S0_carry__6\(12),
      O => \Dout_reg[15]_0\(0)
    );
\S0_carry__3_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(19),
      I1 => \S0_carry__6\(19),
      O => \Dout_reg[19]_0\(3)
    );
\S0_carry__3_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(18),
      I1 => \S0_carry__6\(18),
      O => \Dout_reg[19]_0\(2)
    );
\S0_carry__3_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(17),
      I1 => \S0_carry__6\(17),
      O => \Dout_reg[19]_0\(1)
    );
\S0_carry__3_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(16),
      I1 => \S0_carry__6\(16),
      O => \Dout_reg[19]_0\(0)
    );
\S0_carry__4_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(23),
      I1 => \S0_carry__6\(23),
      O => \Dout_reg[23]_0\(3)
    );
\S0_carry__4_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(22),
      I1 => \S0_carry__6\(22),
      O => \Dout_reg[23]_0\(2)
    );
\S0_carry__4_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(21),
      I1 => \S0_carry__6\(21),
      O => \Dout_reg[23]_0\(1)
    );
\S0_carry__4_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(20),
      I1 => \S0_carry__6\(20),
      O => \Dout_reg[23]_0\(0)
    );
\S0_carry__5_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(27),
      I1 => \S0_carry__6\(27),
      O => \Dout_reg[27]_0\(3)
    );
\S0_carry__5_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(26),
      I1 => \S0_carry__6\(26),
      O => \Dout_reg[27]_0\(2)
    );
\S0_carry__5_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(25),
      I1 => \S0_carry__6\(25),
      O => \Dout_reg[27]_0\(1)
    );
\S0_carry__5_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(24),
      I1 => \S0_carry__6\(24),
      O => \Dout_reg[27]_0\(0)
    );
\S0_carry__6_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(31),
      O => DI(0)
    );
\S0_carry__6_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(31),
      I1 => \S0_carry__6\(31),
      O => \Dout_reg[31]_0\(3)
    );
\S0_carry__6_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(30),
      I1 => \S0_carry__6\(30),
      O => \Dout_reg[31]_0\(2)
    );
\S0_carry__6_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(29),
      I1 => \S0_carry__6\(29),
      O => \Dout_reg[31]_0\(1)
    );
\S0_carry__6_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(28),
      I1 => \S0_carry__6\(28),
      O => \Dout_reg[31]_0\(0)
    );
S0_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(3),
      I1 => \S0_carry__6\(3),
      O => \Dout_reg[3]_0\(3)
    );
S0_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(2),
      I1 => \S0_carry__6\(2),
      O => \Dout_reg[3]_0\(2)
    );
S0_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(1),
      I1 => \S0_carry__6\(1),
      O => \Dout_reg[3]_0\(1)
    );
S0_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \S0_carry__6\(0),
      O => \Dout_reg[3]_0\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \REGrwe_n__parameterized1_0\ is
  port (
    \Dout_reg[31]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    O : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[31]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[0]_0\ : out STD_LOGIC;
    \Dout_reg[1]_0\ : out STD_LOGIC;
    \Dout_reg[1]_1\ : out STD_LOGIC;
    \Dout_reg[0]_1\ : out STD_LOGIC;
    \Dout_reg[1]_2\ : out STD_LOGIC;
    \Dout_reg[31]_2\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \Dout_reg[0]_2\ : out STD_LOGIC;
    \Dout_reg[0]_3\ : out STD_LOGIC;
    \Dout_reg[0]_4\ : out STD_LOGIC;
    \Dout_reg[0]_5\ : out STD_LOGIC;
    \Dout_reg[0]_6\ : out STD_LOGIC;
    \Dout_reg[0]_7\ : out STD_LOGIC;
    \Dout_reg[0]_8\ : out STD_LOGIC;
    \Dout_reg[0]_9\ : out STD_LOGIC;
    \Dout_reg[0]_10\ : out STD_LOGIC;
    \Dout_reg[0]_11\ : out STD_LOGIC;
    \Dout_reg[0]_12\ : out STD_LOGIC;
    \Dout_reg[0]_13\ : out STD_LOGIC;
    \Dout_reg[0]_14\ : out STD_LOGIC;
    \Dout_reg[0]_15\ : out STD_LOGIC;
    \Dout_reg[0]_16\ : out STD_LOGIC;
    \Dout_reg[0]_17\ : out STD_LOGIC;
    \Dout_reg[0]_18\ : out STD_LOGIC;
    \Dout_reg[0]_19\ : out STD_LOGIC;
    \Dout_reg[0]_20\ : out STD_LOGIC;
    \Dout_reg[0]_21\ : out STD_LOGIC;
    \Dout_reg[0]_22\ : out STD_LOGIC;
    \Dout_reg[9]_0\ : out STD_LOGIC;
    \Dout_reg[10]_0\ : out STD_LOGIC;
    \Dout_reg[0]_23\ : out STD_LOGIC;
    \Dout_reg[12]_0\ : out STD_LOGIC;
    \Dout_reg[11]_0\ : out STD_LOGIC;
    \Dout_reg[25]_0\ : out STD_LOGIC;
    \Dout_reg[13]_0\ : out STD_LOGIC;
    \Dout_reg[27]_0\ : out STD_LOGIC;
    \Dout_reg[14]_0\ : out STD_LOGIC;
    \Dout_reg[15]_0\ : out STD_LOGIC;
    \Dout_reg[29]_0\ : out STD_LOGIC;
    \Dout_reg[30]_0\ : out STD_LOGIC;
    \Dout_reg[31]_3\ : out STD_LOGIC;
    \Dout_reg[2]_0\ : out STD_LOGIC;
    \Dout_reg[0]_24\ : out STD_LOGIC;
    \Dout_reg[0]_25\ : out STD_LOGIC;
    \Dout_reg[0]_26\ : out STD_LOGIC;
    \Dout_reg[0]_27\ : out STD_LOGIC;
    \Dout_reg[3]_0\ : out STD_LOGIC;
    \Dout_reg[0]_28\ : out STD_LOGIC;
    \Dout_reg[0]_29\ : out STD_LOGIC;
    \Dout_reg[1]_3\ : out STD_LOGIC;
    \Dout_reg[0]_30\ : out STD_LOGIC;
    \Dout_reg[0]_31\ : out STD_LOGIC;
    \Dout_reg[0]_32\ : out STD_LOGIC;
    \Dout_reg[0]_33\ : out STD_LOGIC;
    \Dout_reg[1]_4\ : out STD_LOGIC;
    \Dout_reg[0]_34\ : out STD_LOGIC;
    \Dout_reg[1]_5\ : out STD_LOGIC;
    \Dout_reg[1]_6\ : out STD_LOGIC;
    \Dout_reg[0]_35\ : out STD_LOGIC;
    \Dout_reg[0]_36\ : out STD_LOGIC;
    \Dout_reg[0]_37\ : out STD_LOGIC;
    \Dout_reg[0]_38\ : out STD_LOGIC;
    \Dout_reg[0]_39\ : out STD_LOGIC;
    \Dout_reg[0]_40\ : out STD_LOGIC;
    \Dout_reg[0]_41\ : out STD_LOGIC;
    \Dout_reg[0]_42\ : out STD_LOGIC;
    \Dout_reg[0]_43\ : out STD_LOGIC;
    \Dout_reg[0]_44\ : out STD_LOGIC;
    \Dout_reg[0]_45\ : out STD_LOGIC;
    \Dout_reg[1]_7\ : out STD_LOGIC;
    \Dout_reg[29]_1\ : out STD_LOGIC;
    \Dout_reg[1]_8\ : out STD_LOGIC;
    \Dout_reg[0]_46\ : out STD_LOGIC;
    \Dout_reg[1]_9\ : out STD_LOGIC;
    \Dout_reg[1]_10\ : out STD_LOGIC;
    \Dout_reg[31]_4\ : out STD_LOGIC;
    \Dout_reg[2]_1\ : out STD_LOGIC;
    \Dout_reg[1]_11\ : out STD_LOGIC;
    \Dout_reg[29]_2\ : out STD_LOGIC;
    \Dout_reg[28]_0\ : out STD_LOGIC;
    \Dout_reg[1]_12\ : out STD_LOGIC;
    \Dout_reg[1]_13\ : out STD_LOGIC;
    \Dout_reg[30]_1\ : out STD_LOGIC;
    \Dout_reg[29]_3\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[11]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[15]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[19]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[23]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[27]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[31]_5\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[2]_2\ : out STD_LOGIC;
    \Dout_reg[2]_3\ : out STD_LOGIC;
    \Dout_reg[2]_4\ : out STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[2]_5\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \Dout_reg[4]_0\ : in STD_LOGIC;
    \Dout_reg[0]_47\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ROTATE_RIGHT1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[16]_i_9_0\ : in STD_LOGIC;
    \Dout_reg[6]_0\ : in STD_LOGIC;
    \Dout[1]_i_5\ : in STD_LOGIC;
    \Dout[1]_i_5_0\ : in STD_LOGIC;
    \Dout[1]_i_5_1\ : in STD_LOGIC;
    \Dout_reg[8]_0\ : in STD_LOGIC;
    \Dout_reg[23]_1\ : in STD_LOGIC;
    \Dout_reg[26]_0\ : in STD_LOGIC;
    \Dout[16]_i_9_1\ : in STD_LOGIC;
    \Dout_reg[9]_1\ : in STD_LOGIC;
    \Dout[25]_i_6\ : in STD_LOGIC;
    \Dout[1]_i_16\ : in STD_LOGIC;
    \Dout[1]_i_14\ : in STD_LOGIC;
    \Dout_reg[0]_48\ : in STD_LOGIC;
    \Dout_reg[2]_i_2_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \REGrwe_n__parameterized1_0\ : entity is "REGrwe_n";
end \REGrwe_n__parameterized1_0\;

architecture STRUCTURE of \REGrwe_n__parameterized1_0\ is
  signal \Dout[0]_i_12__0_n_0\ : STD_LOGIC;
  signal \Dout[0]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[10]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[10]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[10]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[10]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[10]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[10]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[11]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[11]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[11]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[11]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[11]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[11]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[11]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[11]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[12]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[12]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[12]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[12]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[12]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[12]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[12]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[12]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[13]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[14]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_16_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[15]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_16_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_17_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[16]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_16_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_17_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[17]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_16_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_17_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[18]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_16_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_17_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[19]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_16_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[20]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_16_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[21]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[22]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[23]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[23]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[23]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[23]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[23]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[24]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[24]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[24]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[25]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[25]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[26]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[26]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[26]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[27]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[27]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[28]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[28]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[29]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[29]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[2]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[2]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[2]_i_4__0_n_0\ : STD_LOGIC;
  signal \Dout[2]_i_5__0_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_16_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_17_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_18_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_19_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_20_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_21_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_22_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_23_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_25_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_31_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_32_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_33_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_34_n_0\ : STD_LOGIC;
  signal \Dout[3]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[3]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[4]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[4]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_16_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_17_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_18_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_19_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_20_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_21_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_26_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_27_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_28_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_29_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_30_n_0\ : STD_LOGIC;
  signal \Dout[6]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[6]_i_9_n_0\ : STD_LOGIC;
  signal \Dout[7]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[7]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[8]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[8]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[8]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[9]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[9]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[9]_i_6_n_0\ : STD_LOGIC;
  signal \Dout[9]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[9]_i_9_n_0\ : STD_LOGIC;
  signal \^dout_reg[0]_3\ : STD_LOGIC;
  signal \^dout_reg[0]_5\ : STD_LOGIC;
  signal \^dout_reg[0]_6\ : STD_LOGIC;
  signal \^dout_reg[0]_7\ : STD_LOGIC;
  signal \^dout_reg[0]_8\ : STD_LOGIC;
  signal \^dout_reg[11]_0\ : STD_LOGIC;
  signal \^dout_reg[12]_0\ : STD_LOGIC;
  signal \^dout_reg[1]_0\ : STD_LOGIC;
  signal \^dout_reg[1]_1\ : STD_LOGIC;
  signal \^dout_reg[1]_10\ : STD_LOGIC;
  signal \^dout_reg[1]_13\ : STD_LOGIC;
  signal \^dout_reg[1]_2\ : STD_LOGIC;
  signal \^dout_reg[1]_3\ : STD_LOGIC;
  signal \^dout_reg[1]_7\ : STD_LOGIC;
  signal \^dout_reg[1]_9\ : STD_LOGIC;
  signal \^dout_reg[25]_0\ : STD_LOGIC;
  signal \^dout_reg[27]_0\ : STD_LOGIC;
  signal \^dout_reg[28]_0\ : STD_LOGIC;
  signal \^dout_reg[29]_0\ : STD_LOGIC;
  signal \^dout_reg[29]_1\ : STD_LOGIC;
  signal \^dout_reg[29]_2\ : STD_LOGIC;
  signal \^dout_reg[29]_3\ : STD_LOGIC;
  signal \^dout_reg[2]_0\ : STD_LOGIC;
  signal \^dout_reg[2]_1\ : STD_LOGIC;
  signal \Dout_reg[2]_i_2_n_3\ : STD_LOGIC;
  signal \Dout_reg[2]_i_3_n_3\ : STD_LOGIC;
  signal \^dout_reg[30]_1\ : STD_LOGIC;
  signal \^dout_reg[31]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^dout_reg[31]_2\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^dout_reg[3]_0\ : STD_LOGIC;
  signal \^dout_reg[9]_0\ : STD_LOGIC;
  signal \NLW_Dout_reg[2]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_Dout_reg[2]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_Dout_reg[2]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_Dout_reg[2]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_Dout_reg[3]_i_4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_Dout_reg[3]_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Dout[10]_i_6\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \Dout[10]_i_8\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \Dout[11]_i_6\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \Dout[11]_i_7\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \Dout[11]_i_8\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \Dout[12]_i_6\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \Dout[12]_i_7\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \Dout[12]_i_8\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \Dout[13]_i_14\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \Dout[13]_i_6\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \Dout[13]_i_7\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \Dout[14]_i_14\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \Dout[14]_i_6\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \Dout[14]_i_7\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \Dout[14]_i_8\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \Dout[15]_i_14\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \Dout[15]_i_15\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \Dout[15]_i_16\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \Dout[15]_i_6\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \Dout[15]_i_7\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \Dout[15]_i_8\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \Dout[16]_i_14\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \Dout[16]_i_15\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \Dout[16]_i_16\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \Dout[16]_i_17\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \Dout[16]_i_6\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \Dout[16]_i_8\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \Dout[17]_i_14\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \Dout[17]_i_15\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \Dout[17]_i_16\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \Dout[17]_i_17\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \Dout[17]_i_6\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \Dout[17]_i_7\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \Dout[17]_i_8\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \Dout[18]_i_14\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \Dout[18]_i_15\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \Dout[18]_i_16\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \Dout[18]_i_17\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \Dout[18]_i_6\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \Dout[18]_i_7\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \Dout[18]_i_8\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \Dout[19]_i_14\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \Dout[19]_i_15\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \Dout[19]_i_16\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \Dout[19]_i_7\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \Dout[19]_i_8\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \Dout[1]_i_5__0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \Dout[20]_i_15\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \Dout[20]_i_7\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \Dout[20]_i_8\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \Dout[21]_i_15\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \Dout[21]_i_8\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \Dout[22]_i_14\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \Dout[24]_i_12\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \Dout[26]_i_6\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \Dout[27]_i_11\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \Dout[28]_i_11\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \Dout[29]_i_11\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \Dout[2]_i_5\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \Dout[30]_i_16\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \Dout[3]_i_5\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \Dout[4]_i_5\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \Dout[5]_i_14\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \Dout[5]_i_16\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \Dout[5]_i_18\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \Dout[5]_i_19\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \Dout[5]_i_20\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \Dout[5]_i_5\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \Dout[6]_i_6\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \Dout[6]_i_8\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \Dout[7]_i_6\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \Dout[7]_i_8\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \Dout[8]_i_6\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \Dout[8]_i_8\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \Dout[9]_i_6\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \Dout[9]_i_8\ : label is "soft_lutpair20";
begin
  \Dout_reg[0]_3\ <= \^dout_reg[0]_3\;
  \Dout_reg[0]_5\ <= \^dout_reg[0]_5\;
  \Dout_reg[0]_6\ <= \^dout_reg[0]_6\;
  \Dout_reg[0]_7\ <= \^dout_reg[0]_7\;
  \Dout_reg[0]_8\ <= \^dout_reg[0]_8\;
  \Dout_reg[11]_0\ <= \^dout_reg[11]_0\;
  \Dout_reg[12]_0\ <= \^dout_reg[12]_0\;
  \Dout_reg[1]_0\ <= \^dout_reg[1]_0\;
  \Dout_reg[1]_1\ <= \^dout_reg[1]_1\;
  \Dout_reg[1]_10\ <= \^dout_reg[1]_10\;
  \Dout_reg[1]_13\ <= \^dout_reg[1]_13\;
  \Dout_reg[1]_2\ <= \^dout_reg[1]_2\;
  \Dout_reg[1]_3\ <= \^dout_reg[1]_3\;
  \Dout_reg[1]_7\ <= \^dout_reg[1]_7\;
  \Dout_reg[1]_9\ <= \^dout_reg[1]_9\;
  \Dout_reg[25]_0\ <= \^dout_reg[25]_0\;
  \Dout_reg[27]_0\ <= \^dout_reg[27]_0\;
  \Dout_reg[28]_0\ <= \^dout_reg[28]_0\;
  \Dout_reg[29]_0\ <= \^dout_reg[29]_0\;
  \Dout_reg[29]_1\ <= \^dout_reg[29]_1\;
  \Dout_reg[29]_2\ <= \^dout_reg[29]_2\;
  \Dout_reg[29]_3\ <= \^dout_reg[29]_3\;
  \Dout_reg[2]_0\ <= \^dout_reg[2]_0\;
  \Dout_reg[2]_1\ <= \^dout_reg[2]_1\;
  \Dout_reg[30]_1\ <= \^dout_reg[30]_1\;
  \Dout_reg[31]_0\(0) <= \^dout_reg[31]_0\(0);
  \Dout_reg[31]_2\(31 downto 0) <= \^dout_reg[31]_2\(31 downto 0);
  \Dout_reg[3]_0\ <= \^dout_reg[3]_0\;
  \Dout_reg[9]_0\ <= \^dout_reg[9]_0\;
\Dout[0]_i_11__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(31),
      I1 => \^dout_reg[31]_2\(15),
      I2 => ROTATE_RIGHT1(2),
      I3 => \^dout_reg[31]_2\(7),
      I4 => ROTATE_RIGHT1(3),
      I5 => \^dout_reg[31]_2\(23),
      O => \Dout_reg[31]_3\
    );
\Dout[0]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(0),
      I1 => \^dout_reg[31]_2\(16),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(24),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(8),
      O => \Dout[0]_i_12_n_0\
    );
\Dout[0]_i_12__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(31),
      I1 => \^dout_reg[31]_2\(15),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(7),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(23),
      O => \Dout[0]_i_12__0_n_0\
    );
\Dout[0]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"30303030BBB8B8B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(31),
      I1 => \Dout_reg[0]_47\(1),
      I2 => \Dout_reg[0]_48\,
      I3 => \^dout_reg[2]_1\,
      I4 => Q(0),
      I5 => \Dout_reg[0]_47\(0),
      O => \Dout_reg[31]_4\
    );
\Dout[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \Dout[0]_i_12_n_0\,
      I1 => \Dout[4]_i_12_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_21_n_0\,
      I4 => Q(2),
      I5 => \Dout[2]_i_12_n_0\,
      O => \Dout_reg[1]_5\
    );
\Dout[0]_i_9__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8B8FFCC3300"
    )
        port map (
      I0 => \Dout[30]_i_18_n_0\,
      I1 => Q(2),
      I2 => \Dout[30]_i_19_n_0\,
      I3 => \Dout[0]_i_12__0_n_0\,
      I4 => \Dout[30]_i_17_n_0\,
      I5 => Q(1),
      O => \Dout_reg[2]_2\
    );
\Dout[10]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[16]_i_15_n_0\,
      I1 => \Dout[5]_i_18_n_0\,
      I2 => Q(1),
      I3 => \Dout[14]_i_14_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_20_n_0\,
      O => \Dout[10]_i_10_n_0\
    );
\Dout[10]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[16]_i_17_n_0\,
      I1 => \Dout[5]_i_26_n_0\,
      I2 => Q(1),
      I3 => \Dout[14]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_28_n_0\,
      O => \Dout[10]_i_11_n_0\
    );
\Dout[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[10]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[10]_i_7_n_0\,
      I4 => \Dout[11]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_24\
    );
\Dout[10]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[10]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[10]_i_6_n_0\,
      I3 => \Dout[11]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[10]_i_9_n_0\,
      O => \Dout_reg[0]_9\
    );
\Dout[10]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[11]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[10]_i_10_n_0\,
      O => \Dout[10]_i_6_n_0\
    );
\Dout[10]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(3),
      I1 => Q(2),
      I2 => \Dout[25]_i_6\,
      I3 => \^dout_reg[31]_2\(7),
      I4 => Q(1),
      I5 => \Dout[12]_i_11_n_0\,
      O => \Dout[10]_i_7_n_0\
    );
\Dout[10]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[11]_i_12_n_0\,
      I1 => \Dout[10]_i_11_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[10]_i_8_n_0\
    );
\Dout[10]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(3),
      I1 => ROTATE_RIGHT1(1),
      I2 => \Dout[16]_i_9_0\,
      I3 => \^dout_reg[31]_2\(7),
      I4 => ROTATE_RIGHT1(0),
      I5 => \Dout[12]_i_13_n_0\,
      O => \Dout[10]_i_9_n_0\
    );
\Dout[11]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[17]_i_15_n_0\,
      I1 => \Dout[13]_i_14_n_0\,
      I2 => Q(1),
      I3 => \Dout[15]_i_14_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_14_n_0\,
      O => \Dout[11]_i_10_n_0\
    );
\Dout[11]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000030300000BB88"
    )
        port map (
      I0 => \^dout_reg[31]_2\(4),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(0),
      I3 => \^dout_reg[31]_2\(8),
      I4 => Q(4),
      I5 => Q(3),
      O => \Dout[11]_i_11_n_0\
    );
\Dout[11]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[17]_i_17_n_0\,
      I1 => \Dout[13]_i_15_n_0\,
      I2 => Q(1),
      I3 => \Dout[15]_i_16_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_29_n_0\,
      O => \Dout[11]_i_12_n_0\
    );
\Dout[11]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBB88830003000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(4),
      I1 => ROTATE_RIGHT1(1),
      I2 => \^dout_reg[31]_2\(0),
      I3 => \Dout[16]_i_9_1\,
      I4 => \^dout_reg[31]_2\(8),
      I5 => \Dout[16]_i_9_0\,
      O => \Dout[11]_i_13_n_0\
    );
\Dout[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[11]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[11]_i_7_n_0\,
      I4 => \Dout[12]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_25\
    );
\Dout[11]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[11]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[11]_i_6_n_0\,
      I3 => \Dout[12]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[11]_i_9_n_0\,
      O => \Dout_reg[0]_10\
    );
\Dout[11]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[12]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[11]_i_10_n_0\,
      O => \Dout[11]_i_6_n_0\
    );
\Dout[11]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[11]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[13]_i_11_n_0\,
      O => \Dout[11]_i_7_n_0\
    );
\Dout[11]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[12]_i_12_n_0\,
      I1 => \Dout[11]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[11]_i_8_n_0\
    );
\Dout[11]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EB28"
    )
        port map (
      I0 => \Dout[11]_i_13_n_0\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \Dout[13]_i_13_n_0\,
      O => \Dout[11]_i_9_n_0\
    );
\Dout[12]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[18]_i_15_n_0\,
      I1 => \Dout[14]_i_14_n_0\,
      I2 => Q(1),
      I3 => \Dout[16]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_18_n_0\,
      O => \Dout[12]_i_10_n_0\
    );
\Dout[12]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000030300000BB88"
    )
        port map (
      I0 => \^dout_reg[31]_2\(5),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(1),
      I3 => \^dout_reg[31]_2\(9),
      I4 => Q(4),
      I5 => Q(3),
      O => \Dout[12]_i_11_n_0\
    );
\Dout[12]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[18]_i_17_n_0\,
      I1 => \Dout[14]_i_15_n_0\,
      I2 => Q(1),
      I3 => \Dout[16]_i_17_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_26_n_0\,
      O => \Dout[12]_i_12_n_0\
    );
\Dout[12]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBB88830003000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(5),
      I1 => ROTATE_RIGHT1(1),
      I2 => \^dout_reg[31]_2\(1),
      I3 => \Dout[16]_i_9_1\,
      I4 => \^dout_reg[31]_2\(9),
      I5 => \Dout[16]_i_9_0\,
      O => \Dout[12]_i_13_n_0\
    );
\Dout[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[12]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[12]_i_7_n_0\,
      I4 => \Dout[13]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_26\
    );
\Dout[12]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[12]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[12]_i_6_n_0\,
      I3 => \Dout[13]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[12]_i_9_n_0\,
      O => \Dout_reg[0]_11\
    );
\Dout[12]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[13]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[12]_i_10_n_0\,
      O => \Dout[12]_i_6_n_0\
    );
\Dout[12]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[12]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[14]_i_11_n_0\,
      O => \Dout[12]_i_7_n_0\
    );
\Dout[12]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[13]_i_12_n_0\,
      I1 => \Dout[12]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[12]_i_8_n_0\
    );
\Dout[12]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EB28"
    )
        port map (
      I0 => \Dout[12]_i_13_n_0\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \Dout[14]_i_13_n_0\,
      O => \Dout[12]_i_9_n_0\
    );
\Dout[13]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[19]_i_15_n_0\,
      I1 => \Dout[15]_i_14_n_0\,
      I2 => Q(1),
      I3 => \Dout[17]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[13]_i_14_n_0\,
      O => \Dout[13]_i_10_n_0\
    );
\Dout[13]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000030300000BB88"
    )
        port map (
      I0 => \^dout_reg[31]_2\(6),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(2),
      I3 => \^dout_reg[31]_2\(10),
      I4 => Q(4),
      I5 => Q(3),
      O => \Dout[13]_i_11_n_0\
    );
\Dout[13]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[19]_i_17_n_0\,
      I1 => \Dout[15]_i_16_n_0\,
      I2 => Q(1),
      I3 => \Dout[17]_i_17_n_0\,
      I4 => Q(2),
      I5 => \Dout[13]_i_15_n_0\,
      O => \Dout[13]_i_12_n_0\
    );
\Dout[13]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBB88830003000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(6),
      I1 => ROTATE_RIGHT1(1),
      I2 => \^dout_reg[31]_2\(2),
      I3 => \Dout[16]_i_9_1\,
      I4 => \^dout_reg[31]_2\(10),
      I5 => \Dout[16]_i_9_0\,
      O => \Dout[13]_i_13_n_0\
    );
\Dout[13]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(21),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(29),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(13),
      O => \Dout[13]_i_14_n_0\
    );
\Dout[13]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFC0AFAFCFC0A0A0"
    )
        port map (
      I0 => \^dout_reg[31]_2\(21),
      I1 => \^dout_reg[31]_2\(31),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(29),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(13),
      O => \Dout[13]_i_15_n_0\
    );
\Dout[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[13]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[13]_i_7_n_0\,
      I4 => \Dout[14]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_34\
    );
\Dout[13]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[13]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[13]_i_6_n_0\,
      I3 => \Dout[14]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[13]_i_9_n_0\,
      O => \Dout_reg[0]_12\
    );
\Dout[13]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[14]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[13]_i_10_n_0\,
      O => \Dout[13]_i_6_n_0\
    );
\Dout[13]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[13]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[15]_i_11_n_0\,
      O => \Dout[13]_i_7_n_0\
    );
\Dout[13]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[14]_i_12_n_0\,
      I1 => \Dout[13]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[13]_i_8_n_0\
    );
\Dout[13]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBE8EB2B2B28E828"
    )
        port map (
      I0 => \Dout[13]_i_13_n_0\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \Dout[15]_i_13_n_0\,
      I4 => Q(2),
      I5 => \Dout[19]_i_13_n_0\,
      O => \Dout[13]_i_9_n_0\
    );
\Dout[14]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[16]_i_14_n_0\,
      I1 => \Dout[16]_i_15_n_0\,
      I2 => Q(1),
      I3 => \Dout[18]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[14]_i_14_n_0\,
      O => \Dout[14]_i_10_n_0\
    );
\Dout[14]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000030300000BB88"
    )
        port map (
      I0 => \^dout_reg[31]_2\(7),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(3),
      I3 => \^dout_reg[31]_2\(11),
      I4 => Q(4),
      I5 => Q(3),
      O => \Dout[14]_i_11_n_0\
    );
\Dout[14]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[20]_i_16_n_0\,
      I1 => \Dout[16]_i_17_n_0\,
      I2 => Q(1),
      I3 => \Dout[18]_i_17_n_0\,
      I4 => Q(2),
      I5 => \Dout[14]_i_15_n_0\,
      O => \Dout[14]_i_12_n_0\
    );
\Dout[14]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBB88830003000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(7),
      I1 => ROTATE_RIGHT1(1),
      I2 => \^dout_reg[31]_2\(3),
      I3 => \Dout[16]_i_9_1\,
      I4 => \^dout_reg[31]_2\(11),
      I5 => \Dout[16]_i_9_0\,
      O => \Dout[14]_i_13_n_0\
    );
\Dout[14]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(22),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(30),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(14),
      O => \Dout[14]_i_14_n_0\
    );
\Dout[14]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFC0AFAFCFC0A0A0"
    )
        port map (
      I0 => \^dout_reg[31]_2\(22),
      I1 => \^dout_reg[31]_2\(31),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(30),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(14),
      O => \Dout[14]_i_15_n_0\
    );
\Dout[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[14]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[14]_i_7_n_0\,
      I4 => \Dout[15]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_35\
    );
\Dout[14]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[14]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[14]_i_6_n_0\,
      I3 => \Dout[15]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[14]_i_9_n_0\,
      O => \Dout_reg[0]_13\
    );
\Dout[14]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[15]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[14]_i_10_n_0\,
      O => \Dout[14]_i_6_n_0\
    );
\Dout[14]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[14]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[16]_i_11_n_0\,
      O => \Dout[14]_i_7_n_0\
    );
\Dout[14]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[15]_i_12_n_0\,
      I1 => \Dout[14]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[14]_i_8_n_0\
    );
\Dout[14]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBE8EB2B2B28E828"
    )
        port map (
      I0 => \Dout[14]_i_13_n_0\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \Dout[16]_i_13_n_0\,
      I4 => Q(2),
      I5 => \Dout[20]_i_14_n_0\,
      O => \Dout[14]_i_9_n_0\
    );
\Dout[15]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[17]_i_14_n_0\,
      I1 => \Dout[17]_i_15_n_0\,
      I2 => Q(1),
      I3 => \Dout[19]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[15]_i_14_n_0\,
      O => \Dout[15]_i_10_n_0\
    );
\Dout[15]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0CFFFF0A0C0000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(0),
      I1 => \^dout_reg[31]_2\(8),
      I2 => Q(4),
      I3 => Q(3),
      I4 => Q(2),
      I5 => \Dout[15]_i_15_n_0\,
      O => \Dout[15]_i_11_n_0\
    );
\Dout[15]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[21]_i_16_n_0\,
      I1 => \Dout[17]_i_17_n_0\,
      I2 => Q(1),
      I3 => \Dout[19]_i_17_n_0\,
      I4 => Q(2),
      I5 => \Dout[15]_i_16_n_0\,
      O => \Dout[15]_i_12_n_0\
    );
\Dout[15]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^dout_reg[31]_2\(0),
      I1 => \Dout[16]_i_9_1\,
      I2 => \^dout_reg[31]_2\(8),
      I3 => \Dout[16]_i_9_0\,
      O => \Dout[15]_i_13_n_0\
    );
\Dout[15]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3300B8B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(23),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(15),
      I3 => \^dout_reg[31]_2\(31),
      I4 => Q(4),
      O => \Dout[15]_i_14_n_0\
    );
\Dout[15]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(4),
      I1 => \^dout_reg[31]_2\(12),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[15]_i_15_n_0\
    );
\Dout[15]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(23),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(15),
      I3 => \^dout_reg[31]_2\(31),
      I4 => Q(4),
      O => \Dout[15]_i_16_n_0\
    );
\Dout[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[15]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[15]_i_7_n_0\,
      I4 => \Dout[16]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_38\
    );
\Dout[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[15]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[15]_i_6_n_0\,
      I3 => \Dout[16]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[15]_i_9_n_0\,
      O => \Dout_reg[0]_14\
    );
\Dout[15]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[16]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[15]_i_10_n_0\,
      O => \Dout[15]_i_6_n_0\
    );
\Dout[15]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[15]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[17]_i_11_n_0\,
      O => \Dout[15]_i_7_n_0\
    );
\Dout[15]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[16]_i_12_n_0\,
      I1 => \Dout[15]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[15]_i_8_n_0\
    );
\Dout[15]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[15]_i_13_n_0\,
      I1 => \Dout[19]_i_13_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[17]_i_13_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[21]_i_14_n_0\,
      O => \Dout[15]_i_9_n_0\
    );
\Dout[16]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[18]_i_14_n_0\,
      I1 => \Dout[18]_i_15_n_0\,
      I2 => Q(1),
      I3 => \Dout[16]_i_14_n_0\,
      I4 => Q(2),
      I5 => \Dout[16]_i_15_n_0\,
      O => \Dout[16]_i_10_n_0\
    );
\Dout[16]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0CFFFF0A0C0000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(1),
      I1 => \^dout_reg[31]_2\(9),
      I2 => Q(4),
      I3 => Q(3),
      I4 => Q(2),
      I5 => \Dout[16]_i_16_n_0\,
      O => \Dout[16]_i_11_n_0\
    );
\Dout[16]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[22]_i_15_n_0\,
      I1 => \Dout[18]_i_17_n_0\,
      I2 => Q(1),
      I3 => \Dout[20]_i_16_n_0\,
      I4 => Q(2),
      I5 => \Dout[16]_i_17_n_0\,
      O => \Dout[16]_i_12_n_0\
    );
\Dout[16]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^dout_reg[31]_2\(1),
      I1 => \Dout[16]_i_9_1\,
      I2 => \^dout_reg[31]_2\(9),
      I3 => \Dout[16]_i_9_0\,
      O => \Dout[16]_i_13_n_0\
    );
\Dout[16]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(28),
      I1 => \^dout_reg[31]_2\(20),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[16]_i_14_n_0\
    );
\Dout[16]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(24),
      I1 => \^dout_reg[31]_2\(16),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[16]_i_15_n_0\
    );
\Dout[16]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(5),
      I1 => \^dout_reg[31]_2\(13),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[16]_i_16_n_0\
    );
\Dout[16]_i_17\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(24),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(16),
      I3 => \^dout_reg[31]_2\(31),
      I4 => Q(4),
      O => \Dout[16]_i_17_n_0\
    );
\Dout[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[16]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[16]_i_7_n_0\,
      I4 => \Dout[17]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_39\
    );
\Dout[16]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[16]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[16]_i_6_n_0\,
      I3 => \Dout[17]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[16]_i_9_n_0\,
      O => \Dout_reg[0]_15\
    );
\Dout[16]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[17]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[16]_i_10_n_0\,
      O => \Dout[16]_i_6_n_0\
    );
\Dout[16]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[16]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[18]_i_11_n_0\,
      O => \Dout[16]_i_7_n_0\
    );
\Dout[16]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[17]_i_12_n_0\,
      I1 => \Dout[16]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[16]_i_8_n_0\
    );
\Dout[16]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[16]_i_13_n_0\,
      I1 => \Dout[20]_i_14_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[18]_i_13_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[22]_i_13_n_0\,
      O => \Dout[16]_i_9_n_0\
    );
\Dout[17]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[19]_i_14_n_0\,
      I1 => \Dout[19]_i_15_n_0\,
      I2 => Q(1),
      I3 => \Dout[17]_i_14_n_0\,
      I4 => Q(2),
      I5 => \Dout[17]_i_15_n_0\,
      O => \Dout[17]_i_10_n_0\
    );
\Dout[17]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0CFFFF0A0C0000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(2),
      I1 => \^dout_reg[31]_2\(10),
      I2 => Q(4),
      I3 => Q(3),
      I4 => Q(2),
      I5 => \Dout[17]_i_16_n_0\,
      O => \Dout[17]_i_11_n_0\
    );
\Dout[17]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[19]_i_16_n_0\,
      I1 => \Dout[19]_i_17_n_0\,
      I2 => Q(1),
      I3 => \Dout[21]_i_16_n_0\,
      I4 => Q(2),
      I5 => \Dout[17]_i_17_n_0\,
      O => \Dout[17]_i_12_n_0\
    );
\Dout[17]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^dout_reg[31]_2\(2),
      I1 => \Dout[16]_i_9_1\,
      I2 => \^dout_reg[31]_2\(10),
      I3 => \Dout[16]_i_9_0\,
      O => \Dout[17]_i_13_n_0\
    );
\Dout[17]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(29),
      I1 => \^dout_reg[31]_2\(21),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[17]_i_14_n_0\
    );
\Dout[17]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(25),
      I1 => \^dout_reg[31]_2\(17),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[17]_i_15_n_0\
    );
\Dout[17]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(6),
      I1 => \^dout_reg[31]_2\(14),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[17]_i_16_n_0\
    );
\Dout[17]_i_17\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(25),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(17),
      I3 => \^dout_reg[31]_2\(31),
      I4 => Q(4),
      O => \Dout[17]_i_17_n_0\
    );
\Dout[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[17]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[17]_i_7_n_0\,
      I4 => \Dout[18]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_40\
    );
\Dout[17]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[17]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[17]_i_6_n_0\,
      I3 => \Dout[18]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[17]_i_9_n_0\,
      O => \Dout_reg[0]_16\
    );
\Dout[17]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[18]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[17]_i_10_n_0\,
      O => \Dout[17]_i_6_n_0\
    );
\Dout[17]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[17]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[19]_i_11_n_0\,
      O => \Dout[17]_i_7_n_0\
    );
\Dout[17]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[18]_i_12_n_0\,
      I1 => \Dout[17]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[17]_i_8_n_0\
    );
\Dout[17]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[17]_i_13_n_0\,
      I1 => \Dout[21]_i_14_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[19]_i_13_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[23]_i_13_n_0\,
      O => \Dout[17]_i_9_n_0\
    );
\Dout[18]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout[20]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[18]_i_14_n_0\,
      I3 => Q(2),
      I4 => \Dout[18]_i_15_n_0\,
      O => \Dout[18]_i_10_n_0\
    );
\Dout[18]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0CFFFF0A0C0000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(3),
      I1 => \^dout_reg[31]_2\(11),
      I2 => Q(4),
      I3 => Q(3),
      I4 => Q(2),
      I5 => \Dout[18]_i_16_n_0\,
      O => \Dout[18]_i_11_n_0\
    );
\Dout[18]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[20]_i_15_n_0\,
      I1 => \Dout[20]_i_16_n_0\,
      I2 => Q(1),
      I3 => \Dout[22]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[18]_i_17_n_0\,
      O => \Dout[18]_i_12_n_0\
    );
\Dout[18]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^dout_reg[31]_2\(3),
      I1 => \Dout[16]_i_9_1\,
      I2 => \^dout_reg[31]_2\(11),
      I3 => \Dout[16]_i_9_0\,
      O => \Dout[18]_i_13_n_0\
    );
\Dout[18]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(30),
      I1 => \^dout_reg[31]_2\(22),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[18]_i_14_n_0\
    );
\Dout[18]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(26),
      I1 => \^dout_reg[31]_2\(18),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[18]_i_15_n_0\
    );
\Dout[18]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(7),
      I1 => \^dout_reg[31]_2\(15),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[18]_i_16_n_0\
    );
\Dout[18]_i_17\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(26),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(18),
      I3 => \^dout_reg[31]_2\(31),
      I4 => Q(4),
      O => \Dout[18]_i_17_n_0\
    );
\Dout[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[18]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[18]_i_7_n_0\,
      I4 => \Dout[19]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_41\
    );
\Dout[18]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[18]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[18]_i_6_n_0\,
      I3 => \Dout[19]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[18]_i_9_n_0\,
      O => \Dout_reg[0]_17\
    );
\Dout[18]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[19]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[18]_i_10_n_0\,
      O => \Dout[18]_i_6_n_0\
    );
\Dout[18]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[18]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[20]_i_12_n_0\,
      O => \Dout[18]_i_7_n_0\
    );
\Dout[18]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[19]_i_12_n_0\,
      I1 => \Dout[18]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[18]_i_8_n_0\
    );
\Dout[18]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[18]_i_13_n_0\,
      I1 => \Dout[22]_i_13_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[20]_i_14_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[24]_i_13_n_0\,
      O => \Dout[18]_i_9_n_0\
    );
\Dout[19]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout[21]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[19]_i_14_n_0\,
      I3 => Q(2),
      I4 => \Dout[19]_i_15_n_0\,
      O => \Dout[19]_i_10_n_0\
    );
\Dout[19]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0CFFFF0A0C0000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(4),
      I1 => \^dout_reg[31]_2\(12),
      I2 => Q(4),
      I3 => Q(3),
      I4 => Q(2),
      I5 => \Dout[23]_i_11_n_0\,
      O => \Dout[19]_i_11_n_0\
    );
\Dout[19]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[21]_i_15_n_0\,
      I1 => \Dout[21]_i_16_n_0\,
      I2 => Q(1),
      I3 => \Dout[19]_i_16_n_0\,
      I4 => Q(2),
      I5 => \Dout[19]_i_17_n_0\,
      O => \Dout[19]_i_12_n_0\
    );
\Dout[19]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^dout_reg[31]_2\(4),
      I1 => \Dout[16]_i_9_1\,
      I2 => \^dout_reg[31]_2\(12),
      I3 => \Dout[16]_i_9_0\,
      O => \Dout[19]_i_13_n_0\
    );
\Dout[19]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(31),
      I1 => \^dout_reg[31]_2\(23),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[19]_i_14_n_0\
    );
\Dout[19]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0A0C"
    )
        port map (
      I0 => \^dout_reg[31]_2\(27),
      I1 => \^dout_reg[31]_2\(19),
      I2 => Q(4),
      I3 => Q(3),
      O => \Dout[19]_i_15_n_0\
    );
\Dout[19]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F0E2"
    )
        port map (
      I0 => \^dout_reg[31]_2\(23),
      I1 => Q(4),
      I2 => \^dout_reg[31]_2\(31),
      I3 => Q(3),
      O => \Dout[19]_i_16_n_0\
    );
\Dout[19]_i_17\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(27),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(19),
      I3 => \^dout_reg[31]_2\(31),
      I4 => Q(4),
      O => \Dout[19]_i_17_n_0\
    );
\Dout[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[19]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[19]_i_7_n_0\,
      I4 => \Dout[20]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_42\
    );
\Dout[19]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[19]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[19]_i_6_n_0\,
      I3 => \Dout[20]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[19]_i_9_n_0\,
      O => \Dout_reg[0]_18\
    );
\Dout[19]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \Dout[20]_i_10_n_0\,
      I1 => Q(1),
      I2 => \Dout[20]_i_11_n_0\,
      I3 => Q(0),
      I4 => \Dout[19]_i_10_n_0\,
      O => \Dout[19]_i_6_n_0\
    );
\Dout[19]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[19]_i_11_n_0\,
      I1 => Q(1),
      I2 => \Dout[21]_i_12_n_0\,
      O => \Dout[19]_i_7_n_0\
    );
\Dout[19]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[20]_i_13_n_0\,
      I1 => \Dout[19]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[19]_i_8_n_0\
    );
\Dout[19]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[19]_i_13_n_0\,
      I1 => \Dout[23]_i_13_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[21]_i_14_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[25]_i_13_n_0\,
      O => \Dout[19]_i_9_n_0\
    );
\Dout[1]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(1),
      I1 => \^dout_reg[31]_2\(17),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(25),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(9),
      O => \Dout[1]_i_11_n_0\
    );
\Dout[1]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[2]_0\,
      I1 => \Dout[1]_i_14\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[9]_i_10_n_0\,
      I4 => Q(0),
      I5 => \Dout[8]_i_10_n_0\,
      O => \Dout_reg[0]_28\
    );
\Dout[1]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \Dout[1]_i_16\,
      I1 => \^dout_reg[3]_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[7]_i_10_n_0\,
      I4 => Q(0),
      I5 => \^dout_reg[1]_2\,
      O => \Dout_reg[0]_27\
    );
\Dout[1]_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[2]_i_10_n_0\,
      I1 => \^dout_reg[1]_3\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout_reg[0]_29\
    );
\Dout[1]_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \Dout[1]_i_11_n_0\,
      I1 => \Dout[5]_i_17_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[3]_i_12_n_0\,
      O => \^dout_reg[1]_3\
    );
\Dout[1]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFCFEFC"
    )
        port map (
      I0 => \^dout_reg[0]_5\,
      I1 => \Dout[1]_i_5\,
      I2 => \^dout_reg[0]_6\,
      I3 => \Dout[1]_i_5_0\,
      I4 => \^dout_reg[0]_7\,
      I5 => \Dout[1]_i_5_1\,
      O => \Dout_reg[0]_4\
    );
\Dout[20]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000030300000BB88"
    )
        port map (
      I0 => \^dout_reg[31]_2\(26),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(30),
      I3 => \^dout_reg[31]_2\(22),
      I4 => Q(4),
      I5 => Q(3),
      O => \Dout[20]_i_10_n_0\
    );
\Dout[20]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000030300000BB88"
    )
        port map (
      I0 => \^dout_reg[31]_2\(24),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(28),
      I3 => \^dout_reg[31]_2\(20),
      I4 => Q(4),
      I5 => Q(3),
      O => \Dout[20]_i_11_n_0\
    );
\Dout[20]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0CFFFF0A0C0000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(5),
      I1 => \^dout_reg[31]_2\(13),
      I2 => Q(4),
      I3 => Q(3),
      I4 => Q(2),
      I5 => \Dout[24]_i_11_n_0\,
      O => \Dout[20]_i_12_n_0\
    );
\Dout[20]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[22]_i_14_n_0\,
      I1 => \Dout[22]_i_15_n_0\,
      I2 => Q(1),
      I3 => \Dout[20]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[20]_i_16_n_0\,
      O => \Dout[20]_i_13_n_0\
    );
\Dout[20]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^dout_reg[31]_2\(5),
      I1 => \Dout[16]_i_9_1\,
      I2 => \^dout_reg[31]_2\(13),
      I3 => \Dout[16]_i_9_0\,
      O => \Dout[20]_i_14_n_0\
    );
\Dout[20]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F0E2"
    )
        port map (
      I0 => \^dout_reg[31]_2\(24),
      I1 => Q(4),
      I2 => \^dout_reg[31]_2\(31),
      I3 => Q(3),
      O => \Dout[20]_i_15_n_0\
    );
\Dout[20]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(28),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(20),
      I3 => \^dout_reg[31]_2\(31),
      I4 => Q(4),
      O => \Dout[20]_i_16_n_0\
    );
\Dout[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[20]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[20]_i_7_n_0\,
      I4 => \Dout[21]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_43\
    );
\Dout[20]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[20]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[20]_i_6_n_0\,
      I3 => \Dout[21]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[20]_i_9_n_0\,
      O => \Dout_reg[0]_19\
    );
\Dout[20]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[21]_i_10_n_0\,
      I1 => \Dout[21]_i_11_n_0\,
      I2 => Q(0),
      I3 => \Dout[20]_i_10_n_0\,
      I4 => Q(1),
      I5 => \Dout[20]_i_11_n_0\,
      O => \Dout[20]_i_6_n_0\
    );
\Dout[20]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[20]_i_12_n_0\,
      I1 => Q(1),
      I2 => \Dout[22]_i_11_n_0\,
      O => \Dout[20]_i_7_n_0\
    );
\Dout[20]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[21]_i_13_n_0\,
      I1 => \Dout[20]_i_13_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[20]_i_8_n_0\
    );
\Dout[20]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[20]_i_14_n_0\,
      I1 => \Dout[24]_i_13_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[22]_i_13_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[26]_i_13_n_0\,
      O => \Dout[20]_i_9_n_0\
    );
\Dout[21]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000030300000BB88"
    )
        port map (
      I0 => \^dout_reg[31]_2\(27),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(31),
      I3 => \^dout_reg[31]_2\(23),
      I4 => Q(4),
      I5 => Q(3),
      O => \Dout[21]_i_10_n_0\
    );
\Dout[21]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000030300000BB88"
    )
        port map (
      I0 => \^dout_reg[31]_2\(25),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(29),
      I3 => \^dout_reg[31]_2\(21),
      I4 => Q(4),
      I5 => Q(3),
      O => \Dout[21]_i_11_n_0\
    );
\Dout[21]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0CFFFF0A0C0000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(6),
      I1 => \^dout_reg[31]_2\(14),
      I2 => Q(4),
      I3 => Q(3),
      I4 => Q(2),
      I5 => \Dout[25]_i_11_n_0\,
      O => \Dout[21]_i_12_n_0\
    );
\Dout[21]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout[23]_i_12_n_0\,
      I1 => Q(1),
      I2 => \Dout[21]_i_15_n_0\,
      I3 => Q(2),
      I4 => \Dout[21]_i_16_n_0\,
      O => \Dout[21]_i_13_n_0\
    );
\Dout[21]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^dout_reg[31]_2\(6),
      I1 => \Dout[16]_i_9_1\,
      I2 => \^dout_reg[31]_2\(14),
      I3 => \Dout[16]_i_9_0\,
      O => \Dout[21]_i_14_n_0\
    );
\Dout[21]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F0E2"
    )
        port map (
      I0 => \^dout_reg[31]_2\(25),
      I1 => Q(4),
      I2 => \^dout_reg[31]_2\(31),
      I3 => Q(3),
      O => \Dout[21]_i_15_n_0\
    );
\Dout[21]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(29),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(21),
      I3 => \^dout_reg[31]_2\(31),
      I4 => Q(4),
      O => \Dout[21]_i_16_n_0\
    );
\Dout[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[21]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[21]_i_7_n_0\,
      I4 => \Dout[22]_i_7_n_0\,
      I5 => Q(0),
      O => \Dout_reg[0]_44\
    );
\Dout[21]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[21]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[21]_i_6_n_0\,
      I3 => \Dout[22]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[21]_i_9_n_0\,
      O => \Dout_reg[0]_20\
    );
\Dout[21]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \Dout[21]_i_10_n_0\,
      I1 => Q(1),
      I2 => \Dout[21]_i_11_n_0\,
      I3 => \Dout[22]_i_10_n_0\,
      I4 => Q(0),
      O => \Dout[21]_i_6_n_0\
    );
\Dout[21]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \Dout[23]_i_11_n_0\,
      I1 => Q(2),
      I2 => \Dout[27]_i_11_n_0\,
      I3 => \Dout[21]_i_12_n_0\,
      I4 => Q(1),
      O => \Dout[21]_i_7_n_0\
    );
\Dout[21]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[22]_i_12_n_0\,
      I1 => \Dout[21]_i_13_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[21]_i_8_n_0\
    );
\Dout[21]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[21]_i_14_n_0\,
      I1 => \Dout[25]_i_13_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[23]_i_13_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[27]_i_15_n_0\,
      O => \Dout[21]_i_9_n_0\
    );
\Dout[22]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(28),
      I1 => Q(2),
      I2 => \Dout[25]_i_6\,
      I3 => \^dout_reg[31]_2\(24),
      I4 => Q(1),
      I5 => \Dout[20]_i_10_n_0\,
      O => \Dout[22]_i_10_n_0\
    );
\Dout[22]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0CFFFF0A0C0000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(7),
      I1 => \^dout_reg[31]_2\(15),
      I2 => Q(4),
      I3 => Q(3),
      I4 => Q(2),
      I5 => \Dout[26]_i_11_n_0\,
      O => \Dout[22]_i_11_n_0\
    );
\Dout[22]_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \Dout[24]_i_15_n_0\,
      I1 => Q(1),
      I2 => \Dout[22]_i_14_n_0\,
      I3 => Q(2),
      I4 => \Dout[22]_i_15_n_0\,
      O => \Dout[22]_i_12_n_0\
    );
\Dout[22]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^dout_reg[31]_2\(7),
      I1 => \Dout[16]_i_9_1\,
      I2 => \^dout_reg[31]_2\(15),
      I3 => \Dout[16]_i_9_0\,
      O => \Dout[22]_i_13_n_0\
    );
\Dout[22]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F0E2"
    )
        port map (
      I0 => \^dout_reg[31]_2\(26),
      I1 => Q(4),
      I2 => \^dout_reg[31]_2\(31),
      I3 => Q(3),
      O => \Dout[22]_i_14_n_0\
    );
\Dout[22]_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(30),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(22),
      I3 => \^dout_reg[31]_2\(31),
      I4 => Q(4),
      O => \Dout[22]_i_15_n_0\
    );
\Dout[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[22]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \Dout[22]_i_7_n_0\,
      I4 => \^dout_reg[1]_7\,
      I5 => Q(0),
      O => \Dout_reg[0]_45\
    );
\Dout[22]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[22]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[22]_i_6_n_0\,
      I3 => \Dout[23]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[22]_i_9_n_0\,
      O => \Dout_reg[0]_21\
    );
\Dout[22]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^dout_reg[29]_1\,
      I1 => Q(0),
      I2 => \Dout[22]_i_10_n_0\,
      O => \Dout[22]_i_6_n_0\
    );
\Dout[22]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00B8B8"
    )
        port map (
      I0 => \Dout[24]_i_11_n_0\,
      I1 => Q(2),
      I2 => \Dout[28]_i_11_n_0\,
      I3 => \Dout[22]_i_11_n_0\,
      I4 => Q(1),
      O => \Dout[22]_i_7_n_0\
    );
\Dout[22]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000B8B8FF00"
    )
        port map (
      I0 => \^dout_reg[29]_3\,
      I1 => Q(1),
      I2 => \Dout[23]_i_12_n_0\,
      I3 => \Dout[22]_i_12_n_0\,
      I4 => Q(0),
      I5 => \Dout_reg[0]_47\(0),
      O => \Dout[22]_i_8_n_0\
    );
\Dout[22]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[22]_i_13_n_0\,
      I1 => \Dout[26]_i_13_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[24]_i_13_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[28]_i_14_n_0\,
      O => \Dout[22]_i_9_n_0\
    );
\Dout[23]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(29),
      I1 => Q(2),
      I2 => \Dout[25]_i_6\,
      I3 => \^dout_reg[31]_2\(25),
      I4 => Q(1),
      I5 => \Dout[21]_i_10_n_0\,
      O => \^dout_reg[29]_1\
    );
\Dout[23]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(8),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(0),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(16),
      O => \Dout[23]_i_11_n_0\
    );
\Dout[23]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFB800B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(27),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(23),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(31),
      I5 => Q(3),
      O => \Dout[23]_i_12_n_0\
    );
\Dout[23]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(8),
      I1 => ROTATE_RIGHT1(2),
      I2 => \^dout_reg[31]_2\(0),
      I3 => ROTATE_RIGHT1(3),
      I4 => \^dout_reg[31]_2\(16),
      O => \Dout[23]_i_13_n_0\
    );
\Dout[23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[23]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout_reg[23]_1\,
      I3 => \^dout_reg[9]_0\,
      I4 => Q(0),
      I5 => \Dout[23]_i_9_n_0\,
      O => \Dout_reg[0]_22\
    );
\Dout[23]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[23]_i_11_n_0\,
      I1 => \Dout[27]_i_11_n_0\,
      I2 => Q(1),
      I3 => \Dout[25]_i_11_n_0\,
      I4 => Q(2),
      I5 => \Dout[29]_i_11_n_0\,
      O => \^dout_reg[1]_7\
    );
\Dout[23]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FF00B8B8"
    )
        port map (
      I0 => \^dout_reg[29]_3\,
      I1 => Q(1),
      I2 => \Dout[23]_i_12_n_0\,
      I3 => \^dout_reg[1]_13\,
      I4 => Q(0),
      I5 => \Dout_reg[0]_47\(0),
      O => \Dout[23]_i_8_n_0\
    );
\Dout[23]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[23]_i_13_n_0\,
      I1 => \Dout[27]_i_15_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[25]_i_13_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[29]_i_13_n_0\,
      O => \Dout[23]_i_9_n_0\
    );
\Dout[24]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(9),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(1),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(17),
      O => \Dout[24]_i_11_n_0\
    );
\Dout[24]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^dout_reg[30]_1\,
      I1 => Q(1),
      I2 => \Dout[24]_i_15_n_0\,
      O => \^dout_reg[1]_13\
    );
\Dout[24]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(9),
      I1 => ROTATE_RIGHT1(2),
      I2 => \^dout_reg[31]_2\(1),
      I3 => ROTATE_RIGHT1(3),
      I4 => \^dout_reg[31]_2\(17),
      O => \Dout[24]_i_13_n_0\
    );
\Dout[24]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFB800B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(28),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(24),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(31),
      I5 => Q(3),
      O => \Dout[24]_i_15_n_0\
    );
\Dout[24]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[24]_i_11_n_0\,
      I1 => \Dout[28]_i_11_n_0\,
      I2 => Q(1),
      I3 => \Dout[26]_i_11_n_0\,
      I4 => Q(2),
      I5 => \Dout[30]_i_16_n_0\,
      O => \Dout_reg[1]_12\
    );
\Dout[24]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[24]_i_13_n_0\,
      I1 => \Dout[28]_i_14_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[26]_i_13_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[30]_i_25_n_0\,
      O => \^dout_reg[9]_0\
    );
\Dout[25]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(10),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(2),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(18),
      O => \Dout[25]_i_11_n_0\
    );
\Dout[25]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFB800B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(29),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(25),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(31),
      I5 => Q(3),
      O => \^dout_reg[29]_3\
    );
\Dout[25]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(10),
      I1 => ROTATE_RIGHT1(2),
      I2 => \^dout_reg[31]_2\(2),
      I3 => ROTATE_RIGHT1(3),
      I4 => \^dout_reg[31]_2\(18),
      O => \Dout[25]_i_13_n_0\
    );
\Dout[25]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[25]_i_11_n_0\,
      I1 => \Dout[29]_i_11_n_0\,
      I2 => Q(1),
      I3 => \Dout[27]_i_11_n_0\,
      I4 => Q(2),
      I5 => \Dout[30]_i_20_n_0\,
      O => \Dout_reg[1]_8\
    );
\Dout[25]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[25]_i_13_n_0\,
      I1 => \Dout[29]_i_13_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[27]_i_15_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[30]_i_33_n_0\,
      O => \Dout_reg[10]_0\
    );
\Dout[26]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"30BB000030880000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(28),
      I1 => Q(1),
      I2 => \^dout_reg[31]_2\(30),
      I3 => Q(2),
      I4 => \Dout[25]_i_6\,
      I5 => \^dout_reg[31]_2\(26),
      O => \^dout_reg[28]_0\
    );
\Dout[26]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(11),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(3),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(19),
      O => \Dout[26]_i_11_n_0\
    );
\Dout[26]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(11),
      I1 => ROTATE_RIGHT1(2),
      I2 => \^dout_reg[31]_2\(3),
      I3 => ROTATE_RIGHT1(3),
      I4 => \^dout_reg[31]_2\(19),
      O => \Dout[26]_i_13_n_0\
    );
\Dout[26]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFB800B8"
    )
        port map (
      I0 => \^dout_reg[31]_2\(30),
      I1 => Q(2),
      I2 => \^dout_reg[31]_2\(26),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(31),
      I5 => Q(3),
      O => \^dout_reg[30]_1\
    );
\Dout[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[26]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \^dout_reg[1]_9\,
      I4 => \^dout_reg[1]_10\,
      I5 => Q(0),
      O => \Dout_reg[0]_46\
    );
\Dout[26]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout_reg[26]_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[26]_i_6_n_0\,
      I3 => \^dout_reg[12]_0\,
      I4 => Q(0),
      I5 => \^dout_reg[11]_0\,
      O => \Dout_reg[0]_23\
    );
\Dout[26]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^dout_reg[29]_2\,
      I1 => Q(0),
      I2 => \^dout_reg[28]_0\,
      O => \Dout[26]_i_6_n_0\
    );
\Dout[26]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[26]_i_11_n_0\,
      I1 => \Dout[30]_i_16_n_0\,
      I2 => Q(1),
      I3 => \Dout[28]_i_11_n_0\,
      I4 => Q(2),
      I5 => \Dout[30]_i_18_n_0\,
      O => \^dout_reg[1]_9\
    );
\Dout[26]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[26]_i_13_n_0\,
      I1 => \Dout[30]_i_25_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[28]_i_14_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \^dout_reg[25]_0\,
      O => \^dout_reg[11]_0\
    );
\Dout[27]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"30BB000030880000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(29),
      I1 => Q(1),
      I2 => \^dout_reg[31]_2\(31),
      I3 => Q(2),
      I4 => \Dout[25]_i_6\,
      I5 => \^dout_reg[31]_2\(27),
      O => \^dout_reg[29]_2\
    );
\Dout[27]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(12),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(4),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(20),
      O => \Dout[27]_i_11_n_0\
    );
\Dout[27]_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(12),
      I1 => ROTATE_RIGHT1(2),
      I2 => \^dout_reg[31]_2\(4),
      I3 => ROTATE_RIGHT1(3),
      I4 => \^dout_reg[31]_2\(20),
      O => \Dout[27]_i_15_n_0\
    );
\Dout[27]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[27]_i_11_n_0\,
      I1 => \Dout[30]_i_20_n_0\,
      I2 => Q(1),
      I3 => \Dout[29]_i_11_n_0\,
      I4 => Q(2),
      I5 => \Dout[30]_i_23_n_0\,
      O => \^dout_reg[1]_10\
    );
\Dout[27]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[27]_i_15_n_0\,
      I1 => \Dout[30]_i_33_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[29]_i_13_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[30]_i_32_n_0\,
      O => \^dout_reg[12]_0\
    );
\Dout[28]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(13),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(5),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(21),
      O => \Dout[28]_i_11_n_0\
    );
\Dout[28]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(13),
      I1 => ROTATE_RIGHT1(2),
      I2 => \^dout_reg[31]_2\(5),
      I3 => ROTATE_RIGHT1(3),
      I4 => \^dout_reg[31]_2\(21),
      O => \Dout[28]_i_14_n_0\
    );
\Dout[28]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[28]_i_11_n_0\,
      I1 => \Dout[30]_i_18_n_0\,
      I2 => Q(1),
      I3 => \Dout[30]_i_16_n_0\,
      I4 => Q(2),
      I5 => \Dout[30]_i_17_n_0\,
      O => \Dout_reg[1]_11\
    );
\Dout[28]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[28]_i_14_n_0\,
      I1 => \^dout_reg[25]_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[30]_i_25_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \^dout_reg[27]_0\,
      O => \Dout_reg[13]_0\
    );
\Dout[29]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(14),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(6),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(22),
      O => \Dout[29]_i_11_n_0\
    );
\Dout[29]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(14),
      I1 => ROTATE_RIGHT1(2),
      I2 => \^dout_reg[31]_2\(6),
      I3 => ROTATE_RIGHT1(3),
      I4 => \^dout_reg[31]_2\(22),
      O => \Dout[29]_i_13_n_0\
    );
\Dout[29]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8B8FF33CC00"
    )
        port map (
      I0 => \Dout[29]_i_11_n_0\,
      I1 => Q(2),
      I2 => \Dout[30]_i_23_n_0\,
      I3 => \Dout[30]_i_20_n_0\,
      I4 => \Dout[30]_i_21_n_0\,
      I5 => Q(1),
      O => \Dout_reg[2]_4\
    );
\Dout[29]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[29]_i_13_n_0\,
      I1 => \Dout[30]_i_32_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[30]_i_33_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[30]_i_34_n_0\,
      O => \Dout_reg[14]_0\
    );
\Dout[2]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[5]_i_27_n_0\,
      I1 => \Dout[4]_i_12_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_21_n_0\,
      I4 => Q(2),
      I5 => \Dout[2]_i_12_n_0\,
      O => \Dout[2]_i_10_n_0\
    );
\Dout[2]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(2),
      I1 => \^dout_reg[31]_2\(18),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(26),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(10),
      O => \Dout[2]_i_12_n_0\
    );
\Dout[2]_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(31),
      I1 => \Dout_reg[2]_i_2_0\(31),
      O => \Dout[2]_i_4__0_n_0\
    );
\Dout[2]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[3]_i_10_n_0\,
      I1 => \Dout[2]_i_10_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout_reg[0]_30\
    );
\Dout[2]_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(31),
      I1 => \Dout_reg[2]_i_2_0\(31),
      O => \Dout[2]_i_5__0_n_0\
    );
\Dout[2]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[5]_i_19_n_0\,
      I1 => \Dout[4]_i_12_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_21_n_0\,
      I4 => Q(2),
      I5 => \Dout[2]_i_12_n_0\,
      O => \Dout_reg[1]_6\
    );
\Dout[30]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8B8FF33CC00"
    )
        port map (
      I0 => \Dout[30]_i_16_n_0\,
      I1 => Q(2),
      I2 => \Dout[30]_i_17_n_0\,
      I3 => \Dout[30]_i_18_n_0\,
      I4 => \Dout[30]_i_19_n_0\,
      I5 => Q(1),
      O => \Dout_reg[2]_3\
    );
\Dout[30]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8B8FFCC3300"
    )
        port map (
      I0 => \Dout[30]_i_20_n_0\,
      I1 => Q(2),
      I2 => \Dout[30]_i_21_n_0\,
      I3 => \Dout[30]_i_22_n_0\,
      I4 => \Dout[30]_i_23_n_0\,
      I5 => Q(1),
      O => \^dout_reg[2]_1\
    );
\Dout[30]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[30]_i_25_n_0\,
      I1 => \^dout_reg[27]_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \^dout_reg[25]_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \^dout_reg[29]_0\,
      O => \Dout_reg[15]_0\
    );
\Dout[30]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \Dout[30]_i_31_n_0\,
      I1 => \Dout[30]_i_32_n_0\,
      I2 => ROTATE_RIGHT1(0),
      I3 => \Dout[30]_i_33_n_0\,
      I4 => ROTATE_RIGHT1(1),
      I5 => \Dout[30]_i_34_n_0\,
      O => \Dout_reg[30]_0\
    );
\Dout[30]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(15),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(7),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(23),
      O => \Dout[30]_i_16_n_0\
    );
\Dout[30]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(27),
      I1 => \^dout_reg[31]_2\(11),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(3),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(19),
      O => \Dout[30]_i_17_n_0\
    );
\Dout[30]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(25),
      I1 => \^dout_reg[31]_2\(9),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(1),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(17),
      O => \Dout[30]_i_18_n_0\
    );
\Dout[30]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(29),
      I1 => \^dout_reg[31]_2\(13),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(5),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(21),
      O => \Dout[30]_i_19_n_0\
    );
\Dout[30]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(24),
      I1 => \^dout_reg[31]_2\(8),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(0),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(16),
      O => \Dout[30]_i_20_n_0\
    );
\Dout[30]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(28),
      I1 => \^dout_reg[31]_2\(12),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(4),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(20),
      O => \Dout[30]_i_21_n_0\
    );
\Dout[30]_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(30),
      I1 => \^dout_reg[31]_2\(14),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(6),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(22),
      O => \Dout[30]_i_22_n_0\
    );
\Dout[30]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(26),
      I1 => \^dout_reg[31]_2\(10),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(2),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(18),
      O => \Dout[30]_i_23_n_0\
    );
\Dout[30]_i_25\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(15),
      I1 => ROTATE_RIGHT1(2),
      I2 => \^dout_reg[31]_2\(7),
      I3 => ROTATE_RIGHT1(3),
      I4 => \^dout_reg[31]_2\(23),
      O => \Dout[30]_i_25_n_0\
    );
\Dout[30]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(27),
      I1 => \^dout_reg[31]_2\(11),
      I2 => ROTATE_RIGHT1(2),
      I3 => \^dout_reg[31]_2\(3),
      I4 => ROTATE_RIGHT1(3),
      I5 => \^dout_reg[31]_2\(19),
      O => \^dout_reg[27]_0\
    );
\Dout[30]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(25),
      I1 => \^dout_reg[31]_2\(9),
      I2 => ROTATE_RIGHT1(2),
      I3 => \^dout_reg[31]_2\(1),
      I4 => ROTATE_RIGHT1(3),
      I5 => \^dout_reg[31]_2\(17),
      O => \^dout_reg[25]_0\
    );
\Dout[30]_i_30\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(29),
      I1 => \^dout_reg[31]_2\(13),
      I2 => ROTATE_RIGHT1(2),
      I3 => \^dout_reg[31]_2\(5),
      I4 => ROTATE_RIGHT1(3),
      I5 => \^dout_reg[31]_2\(21),
      O => \^dout_reg[29]_0\
    );
\Dout[30]_i_31\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(30),
      I1 => \^dout_reg[31]_2\(14),
      I2 => ROTATE_RIGHT1(2),
      I3 => \^dout_reg[31]_2\(6),
      I4 => ROTATE_RIGHT1(3),
      I5 => \^dout_reg[31]_2\(22),
      O => \Dout[30]_i_31_n_0\
    );
\Dout[30]_i_32\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(26),
      I1 => \^dout_reg[31]_2\(10),
      I2 => ROTATE_RIGHT1(2),
      I3 => \^dout_reg[31]_2\(2),
      I4 => ROTATE_RIGHT1(3),
      I5 => \^dout_reg[31]_2\(18),
      O => \Dout[30]_i_32_n_0\
    );
\Dout[30]_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(24),
      I1 => \^dout_reg[31]_2\(8),
      I2 => ROTATE_RIGHT1(2),
      I3 => \^dout_reg[31]_2\(0),
      I4 => ROTATE_RIGHT1(3),
      I5 => \^dout_reg[31]_2\(16),
      O => \Dout[30]_i_33_n_0\
    );
\Dout[30]_i_34\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(28),
      I1 => \^dout_reg[31]_2\(12),
      I2 => ROTATE_RIGHT1(2),
      I3 => \^dout_reg[31]_2\(4),
      I4 => ROTATE_RIGHT1(3),
      I5 => \^dout_reg[31]_2\(20),
      O => \Dout[30]_i_34_n_0\
    );
\Dout[3]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[5]_i_30_n_0\,
      I1 => \Dout[5]_i_17_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[3]_i_12_n_0\,
      O => \Dout[3]_i_10_n_0\
    );
\Dout[3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(3),
      I1 => \^dout_reg[31]_2\(19),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(27),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(11),
      O => \Dout[3]_i_12_n_0\
    );
\Dout[3]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[4]_i_10_n_0\,
      I1 => \Dout[3]_i_10_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout_reg[0]_31\
    );
\Dout[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[5]_i_16_n_0\,
      I1 => \Dout[5]_i_17_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[3]_i_12_n_0\,
      O => \Dout_reg[1]_4\
    );
\Dout[4]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[5]_i_28_n_0\,
      I1 => \Dout[5]_i_21_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_27_n_0\,
      I4 => Q(2),
      I5 => \Dout[4]_i_12_n_0\,
      O => \Dout[4]_i_10_n_0\
    );
\Dout[4]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(4),
      I1 => \^dout_reg[31]_2\(20),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(28),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(12),
      O => \Dout[4]_i_12_n_0\
    );
\Dout[4]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[5]_i_12_n_0\,
      I1 => \Dout[4]_i_10_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout_reg[0]_32\
    );
\Dout[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEEF3E200000000"
    )
        port map (
      I0 => \Dout[5]_i_13_n_0\,
      I1 => Q(0),
      I2 => \Dout_reg[4]_0\,
      I3 => \^dout_reg[1]_0\,
      I4 => \^dout_reg[1]_1\,
      I5 => \Dout_reg[0]_47\(0),
      O => \Dout_reg[0]_0\
    );
\Dout[4]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[5]_i_20_n_0\,
      I1 => \Dout[5]_i_21_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_19_n_0\,
      I4 => Q(2),
      I5 => \Dout[4]_i_12_n_0\,
      O => \^dout_reg[1]_0\
    );
\Dout[5]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[5]_i_26_n_0\,
      I1 => \Dout[5]_i_27_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_28_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_21_n_0\,
      O => \Dout[5]_i_11_n_0\
    );
\Dout[5]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[5]_i_29_n_0\,
      I1 => \Dout[5]_i_15_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_30_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_17_n_0\,
      O => \Dout[5]_i_12_n_0\
    );
\Dout[5]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"30BB000030880000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(2),
      I1 => ROTATE_RIGHT1(0),
      I2 => \^dout_reg[31]_2\(0),
      I3 => ROTATE_RIGHT1(1),
      I4 => \Dout[16]_i_9_0\,
      I5 => \^dout_reg[31]_2\(4),
      O => \Dout[5]_i_13_n_0\
    );
\Dout[5]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(19),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(27),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(11),
      O => \Dout[5]_i_14_n_0\
    );
\Dout[5]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFC0C0CFA0AFA0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(7),
      I1 => \^dout_reg[31]_2\(23),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(15),
      I4 => \^dout_reg[31]_2\(31),
      I5 => Q(4),
      O => \Dout[5]_i_15_n_0\
    );
\Dout[5]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(17),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(25),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(9),
      O => \Dout[5]_i_16_n_0\
    );
\Dout[5]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(5),
      I1 => \^dout_reg[31]_2\(21),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(29),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(13),
      O => \Dout[5]_i_17_n_0\
    );
\Dout[5]_i_18\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(20),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(28),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(12),
      O => \Dout[5]_i_18_n_0\
    );
\Dout[5]_i_19\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(16),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(24),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(8),
      O => \Dout[5]_i_19_n_0\
    );
\Dout[5]_i_20\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^dout_reg[31]_2\(18),
      I1 => Q(3),
      I2 => \^dout_reg[31]_2\(26),
      I3 => Q(4),
      I4 => \^dout_reg[31]_2\(10),
      O => \Dout[5]_i_20_n_0\
    );
\Dout[5]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC0CFAFAFC0C0A0A"
    )
        port map (
      I0 => \^dout_reg[31]_2\(6),
      I1 => \^dout_reg[31]_2\(22),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(30),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(14),
      O => \Dout[5]_i_21_n_0\
    );
\Dout[5]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFC0AFAFCFC0A0A0"
    )
        port map (
      I0 => \^dout_reg[31]_2\(20),
      I1 => \^dout_reg[31]_2\(31),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(28),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(12),
      O => \Dout[5]_i_26_n_0\
    );
\Dout[5]_i_27\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFC0AFAFCFC0A0A0"
    )
        port map (
      I0 => \^dout_reg[31]_2\(16),
      I1 => \^dout_reg[31]_2\(31),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(24),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(8),
      O => \Dout[5]_i_27_n_0\
    );
\Dout[5]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFC0AFAFCFC0A0A0"
    )
        port map (
      I0 => \^dout_reg[31]_2\(18),
      I1 => \^dout_reg[31]_2\(31),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(26),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(10),
      O => \Dout[5]_i_28_n_0\
    );
\Dout[5]_i_29\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFC0AFAFCFC0A0A0"
    )
        port map (
      I0 => \^dout_reg[31]_2\(19),
      I1 => \^dout_reg[31]_2\(31),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(27),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(11),
      O => \Dout[5]_i_29_n_0\
    );
\Dout[5]_i_30\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFC0AFAFCFC0A0A0"
    )
        port map (
      I0 => \^dout_reg[31]_2\(17),
      I1 => \^dout_reg[31]_2\(31),
      I2 => Q(3),
      I3 => \^dout_reg[31]_2\(25),
      I4 => Q(4),
      I5 => \^dout_reg[31]_2\(9),
      O => \Dout[5]_i_30_n_0\
    );
\Dout[5]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[5]_i_11_n_0\,
      I1 => \Dout[5]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout_reg[0]_33\
    );
\Dout[5]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFCBBB800000000"
    )
        port map (
      I0 => \Dout[5]_i_13_n_0\,
      I1 => Q(0),
      I2 => \Dout[6]_i_9_n_0\,
      I3 => \^dout_reg[1]_1\,
      I4 => \^dout_reg[1]_2\,
      I5 => \Dout_reg[0]_47\(0),
      O => \Dout_reg[0]_1\
    );
\Dout[5]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[5]_i_14_n_0\,
      I1 => \Dout[5]_i_15_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_16_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_17_n_0\,
      O => \^dout_reg[1]_1\
    );
\Dout[5]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[5]_i_18_n_0\,
      I1 => \Dout[5]_i_19_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_20_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_21_n_0\,
      O => \^dout_reg[1]_2\
    );
\Dout[6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[6]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \^dout_reg[0]_3\,
      I3 => \Dout_reg[6]_0\,
      I4 => Q(0),
      I5 => \Dout[6]_i_9_n_0\,
      O => \Dout_reg[0]_2\
    );
\Dout[6]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[7]_i_10_n_0\,
      I1 => Q(0),
      I2 => \^dout_reg[1]_2\,
      O => \^dout_reg[0]_3\
    );
\Dout[6]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"30BB000030880000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(3),
      I1 => Q(1),
      I2 => \^dout_reg[31]_2\(1),
      I3 => Q(2),
      I4 => \Dout[25]_i_6\,
      I5 => \^dout_reg[31]_2\(5),
      O => \^dout_reg[3]_0\
    );
\Dout[6]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[7]_i_12_n_0\,
      I1 => \Dout[5]_i_11_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[6]_i_8_n_0\
    );
\Dout[6]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"30BB000030880000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(3),
      I1 => ROTATE_RIGHT1(0),
      I2 => \^dout_reg[31]_2\(1),
      I3 => ROTATE_RIGHT1(1),
      I4 => \Dout[16]_i_9_0\,
      I5 => \^dout_reg[31]_2\(5),
      O => \Dout[6]_i_9_n_0\
    );
\Dout[7]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[13]_i_14_n_0\,
      I1 => \Dout[5]_i_16_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_14_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_15_n_0\,
      O => \Dout[7]_i_10_n_0\
    );
\Dout[7]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[13]_i_15_n_0\,
      I1 => \Dout[5]_i_30_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_29_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_15_n_0\,
      O => \Dout[7]_i_12_n_0\
    );
\Dout[7]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[8]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[7]_i_10_n_0\,
      O => \Dout_reg[0]_37\
    );
\Dout[7]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[8]_i_12_n_0\,
      I1 => \Dout[7]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout_reg[0]_36\
    );
\Dout[8]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[14]_i_14_n_0\,
      I1 => \Dout[5]_i_20_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_18_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_19_n_0\,
      O => \Dout[8]_i_10_n_0\
    );
\Dout[8]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[14]_i_15_n_0\,
      I1 => \Dout[5]_i_28_n_0\,
      I2 => Q(1),
      I3 => \Dout[5]_i_26_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_27_n_0\,
      O => \Dout[8]_i_12_n_0\
    );
\Dout[8]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[8]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \^dout_reg[0]_8\,
      I3 => \Dout[9]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout_reg[8]_0\,
      O => \^dout_reg[0]_7\
    );
\Dout[8]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[9]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[8]_i_10_n_0\,
      O => \^dout_reg[0]_8\
    );
\Dout[8]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[9]_i_11_n_0\,
      I1 => \Dout[8]_i_12_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[8]_i_8_n_0\
    );
\Dout[9]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[15]_i_14_n_0\,
      I1 => \Dout[5]_i_14_n_0\,
      I2 => Q(1),
      I3 => \Dout[13]_i_14_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_16_n_0\,
      O => \Dout[9]_i_10_n_0\
    );
\Dout[9]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \Dout[15]_i_16_n_0\,
      I1 => \Dout[5]_i_29_n_0\,
      I2 => Q(1),
      I3 => \Dout[13]_i_15_n_0\,
      I4 => Q(2),
      I5 => \Dout[5]_i_30_n_0\,
      O => \Dout[9]_i_11_n_0\
    );
\Dout[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[9]_1\,
      I1 => \Dout[9]_i_6_n_0\,
      I2 => \Dout_reg[0]_47\(0),
      I3 => \^dout_reg[2]_0\,
      I4 => \Dout[10]_i_7_n_0\,
      I5 => Q(0),
      O => \^dout_reg[0]_6\
    );
\Dout[9]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[9]_i_8_n_0\,
      I1 => \Dout_reg[0]_47\(0),
      I2 => \Dout[9]_i_6_n_0\,
      I3 => \Dout[10]_i_9_n_0\,
      I4 => Q(0),
      I5 => \Dout[9]_i_9_n_0\,
      O => \^dout_reg[0]_5\
    );
\Dout[9]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[10]_i_10_n_0\,
      I1 => Q(0),
      I2 => \Dout[9]_i_10_n_0\,
      O => \Dout[9]_i_6_n_0\
    );
\Dout[9]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(2),
      I1 => Q(2),
      I2 => \Dout[25]_i_6\,
      I3 => \^dout_reg[31]_2\(6),
      I4 => Q(1),
      I5 => \Dout[11]_i_11_n_0\,
      O => \^dout_reg[2]_0\
    );
\Dout[9]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[10]_i_11_n_0\,
      I1 => \Dout[9]_i_11_n_0\,
      I2 => Q(0),
      I3 => \Dout_reg[0]_47\(0),
      O => \Dout[9]_i_8_n_0\
    );
\Dout[9]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \^dout_reg[31]_2\(2),
      I1 => ROTATE_RIGHT1(1),
      I2 => \Dout[16]_i_9_0\,
      I3 => \^dout_reg[31]_2\(6),
      I4 => ROTATE_RIGHT1(0),
      I5 => \Dout[11]_i_13_n_0\,
      O => \Dout[9]_i_9_n_0\
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(0),
      Q => \^dout_reg[31]_2\(0),
      R => SR(0)
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(10),
      Q => \^dout_reg[31]_2\(10),
      R => SR(0)
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(11),
      Q => \^dout_reg[31]_2\(11),
      R => SR(0)
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(12),
      Q => \^dout_reg[31]_2\(12),
      R => SR(0)
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(13),
      Q => \^dout_reg[31]_2\(13),
      R => SR(0)
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(14),
      Q => \^dout_reg[31]_2\(14),
      R => SR(0)
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(15),
      Q => \^dout_reg[31]_2\(15),
      R => SR(0)
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(16),
      Q => \^dout_reg[31]_2\(16),
      R => SR(0)
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(17),
      Q => \^dout_reg[31]_2\(17),
      R => SR(0)
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(18),
      Q => \^dout_reg[31]_2\(18),
      R => SR(0)
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(19),
      Q => \^dout_reg[31]_2\(19),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(1),
      Q => \^dout_reg[31]_2\(1),
      R => SR(0)
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(20),
      Q => \^dout_reg[31]_2\(20),
      R => SR(0)
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(21),
      Q => \^dout_reg[31]_2\(21),
      R => SR(0)
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(22),
      Q => \^dout_reg[31]_2\(22),
      R => SR(0)
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(23),
      Q => \^dout_reg[31]_2\(23),
      R => SR(0)
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(24),
      Q => \^dout_reg[31]_2\(24),
      R => SR(0)
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(25),
      Q => \^dout_reg[31]_2\(25),
      R => SR(0)
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(26),
      Q => \^dout_reg[31]_2\(26),
      R => SR(0)
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(27),
      Q => \^dout_reg[31]_2\(27),
      R => SR(0)
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(28),
      Q => \^dout_reg[31]_2\(28),
      R => SR(0)
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(29),
      Q => \^dout_reg[31]_2\(29),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(2),
      Q => \^dout_reg[31]_2\(2),
      R => SR(0)
    );
\Dout_reg[2]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 1) => \NLW_Dout_reg[2]_i_2_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \Dout_reg[2]_i_2_n_3\,
      CYINIT => \^dout_reg[31]_0\(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_Dout_reg[2]_i_2_O_UNCONNECTED\(3 downto 2),
      O(1) => O(0),
      O(0) => \NLW_Dout_reg[2]_i_2_O_UNCONNECTED\(0),
      S(3 downto 2) => B"00",
      S(1) => \Dout[2]_i_4__0_n_0\,
      S(0) => '1'
    );
\Dout_reg[2]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 1) => \NLW_Dout_reg[2]_i_3_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \Dout_reg[2]_i_3_n_3\,
      CYINIT => \Dout_reg[2]_5\(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_Dout_reg[2]_i_3_O_UNCONNECTED\(3 downto 2),
      O(1) => \Dout_reg[31]_1\(0),
      O(0) => \NLW_Dout_reg[2]_i_3_O_UNCONNECTED\(0),
      S(3 downto 2) => B"00",
      S(1) => \Dout[2]_i_5__0_n_0\,
      S(0) => '1'
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(30),
      Q => \^dout_reg[31]_2\(30),
      R => SR(0)
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(31),
      Q => \^dout_reg[31]_2\(31),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(3),
      Q => \^dout_reg[31]_2\(3),
      R => SR(0)
    );
\Dout_reg[3]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => CO(0),
      CO(3 downto 1) => \NLW_Dout_reg[3]_i_4_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \^dout_reg[31]_0\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_Dout_reg[3]_i_4_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(4),
      Q => \^dout_reg[31]_2\(4),
      R => SR(0)
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(5),
      Q => \^dout_reg[31]_2\(5),
      R => SR(0)
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(6),
      Q => \^dout_reg[31]_2\(6),
      R => SR(0)
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(7),
      Q => \^dout_reg[31]_2\(7),
      R => SR(0)
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(8),
      Q => \^dout_reg[31]_2\(8),
      R => SR(0)
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(9),
      Q => \^dout_reg[31]_2\(9),
      R => SR(0)
    );
\S0_carry__0_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(7),
      I1 => \Dout_reg[2]_i_2_0\(7),
      O => \Dout_reg[7]_0\(3)
    );
\S0_carry__0_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(6),
      I1 => \Dout_reg[2]_i_2_0\(6),
      O => \Dout_reg[7]_0\(2)
    );
\S0_carry__0_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(5),
      I1 => \Dout_reg[2]_i_2_0\(5),
      O => \Dout_reg[7]_0\(1)
    );
\S0_carry__0_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(4),
      I1 => \Dout_reg[2]_i_2_0\(4),
      O => \Dout_reg[7]_0\(0)
    );
\S0_carry__1_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(11),
      I1 => \Dout_reg[2]_i_2_0\(11),
      O => \Dout_reg[11]_1\(3)
    );
\S0_carry__1_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(10),
      I1 => \Dout_reg[2]_i_2_0\(10),
      O => \Dout_reg[11]_1\(2)
    );
\S0_carry__1_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(9),
      I1 => \Dout_reg[2]_i_2_0\(9),
      O => \Dout_reg[11]_1\(1)
    );
\S0_carry__1_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(8),
      I1 => \Dout_reg[2]_i_2_0\(8),
      O => \Dout_reg[11]_1\(0)
    );
\S0_carry__2_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(15),
      I1 => \Dout_reg[2]_i_2_0\(15),
      O => \Dout_reg[15]_1\(3)
    );
\S0_carry__2_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(14),
      I1 => \Dout_reg[2]_i_2_0\(14),
      O => \Dout_reg[15]_1\(2)
    );
\S0_carry__2_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(13),
      I1 => \Dout_reg[2]_i_2_0\(13),
      O => \Dout_reg[15]_1\(1)
    );
\S0_carry__2_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(12),
      I1 => \Dout_reg[2]_i_2_0\(12),
      O => \Dout_reg[15]_1\(0)
    );
\S0_carry__3_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(19),
      I1 => \Dout_reg[2]_i_2_0\(19),
      O => \Dout_reg[19]_0\(3)
    );
\S0_carry__3_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(18),
      I1 => \Dout_reg[2]_i_2_0\(18),
      O => \Dout_reg[19]_0\(2)
    );
\S0_carry__3_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(17),
      I1 => \Dout_reg[2]_i_2_0\(17),
      O => \Dout_reg[19]_0\(1)
    );
\S0_carry__3_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(16),
      I1 => \Dout_reg[2]_i_2_0\(16),
      O => \Dout_reg[19]_0\(0)
    );
\S0_carry__4_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(23),
      I1 => \Dout_reg[2]_i_2_0\(23),
      O => \Dout_reg[23]_0\(3)
    );
\S0_carry__4_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(22),
      I1 => \Dout_reg[2]_i_2_0\(22),
      O => \Dout_reg[23]_0\(2)
    );
\S0_carry__4_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(21),
      I1 => \Dout_reg[2]_i_2_0\(21),
      O => \Dout_reg[23]_0\(1)
    );
\S0_carry__4_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(20),
      I1 => \Dout_reg[2]_i_2_0\(20),
      O => \Dout_reg[23]_0\(0)
    );
\S0_carry__5_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(27),
      I1 => \Dout_reg[2]_i_2_0\(27),
      O => \Dout_reg[27]_1\(3)
    );
\S0_carry__5_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(26),
      I1 => \Dout_reg[2]_i_2_0\(26),
      O => \Dout_reg[27]_1\(2)
    );
\S0_carry__5_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(25),
      I1 => \Dout_reg[2]_i_2_0\(25),
      O => \Dout_reg[27]_1\(1)
    );
\S0_carry__5_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(24),
      I1 => \Dout_reg[2]_i_2_0\(24),
      O => \Dout_reg[27]_1\(0)
    );
\S0_carry__6_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(31),
      I1 => \Dout_reg[2]_i_2_0\(31),
      O => \Dout_reg[31]_5\(3)
    );
\S0_carry__6_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(30),
      I1 => \Dout_reg[2]_i_2_0\(30),
      O => \Dout_reg[31]_5\(2)
    );
\S0_carry__6_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(29),
      I1 => \Dout_reg[2]_i_2_0\(29),
      O => \Dout_reg[31]_5\(1)
    );
\S0_carry__6_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(28),
      I1 => \Dout_reg[2]_i_2_0\(28),
      O => \Dout_reg[31]_5\(0)
    );
\S0_carry_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(3),
      I1 => \Dout_reg[2]_i_2_0\(3),
      O => S(3)
    );
\S0_carry_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(2),
      I1 => \Dout_reg[2]_i_2_0\(2),
      O => S(2)
    );
\S0_carry_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(1),
      I1 => \Dout_reg[2]_i_2_0\(1),
      O => S(1)
    );
\S0_carry_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^dout_reg[31]_2\(0),
      I1 => \Dout_reg[2]_i_2_0\(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \REGrwe_n__parameterized1_2\ is
  port (
    Q : out STD_LOGIC_VECTOR ( 30 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 30 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \REGrwe_n__parameterized1_2\ : entity is "REGrwe_n";
end \REGrwe_n__parameterized1_2\;

architecture STRUCTURE of \REGrwe_n__parameterized1_2\ is
  attribute IOB : string;
  attribute IOB of \Dout_reg[0]\ : label is "TRUE";
  attribute IOB of \Dout_reg[10]\ : label is "TRUE";
  attribute IOB of \Dout_reg[11]\ : label is "TRUE";
  attribute IOB of \Dout_reg[12]\ : label is "TRUE";
  attribute IOB of \Dout_reg[13]\ : label is "TRUE";
  attribute IOB of \Dout_reg[14]\ : label is "TRUE";
  attribute IOB of \Dout_reg[15]\ : label is "TRUE";
  attribute IOB of \Dout_reg[16]\ : label is "TRUE";
  attribute IOB of \Dout_reg[17]\ : label is "TRUE";
  attribute IOB of \Dout_reg[18]\ : label is "TRUE";
  attribute IOB of \Dout_reg[19]\ : label is "TRUE";
  attribute IOB of \Dout_reg[1]\ : label is "TRUE";
  attribute IOB of \Dout_reg[20]\ : label is "TRUE";
  attribute IOB of \Dout_reg[21]\ : label is "TRUE";
  attribute IOB of \Dout_reg[22]\ : label is "TRUE";
  attribute IOB of \Dout_reg[23]\ : label is "TRUE";
  attribute IOB of \Dout_reg[24]\ : label is "TRUE";
  attribute IOB of \Dout_reg[25]\ : label is "TRUE";
  attribute IOB of \Dout_reg[26]\ : label is "TRUE";
  attribute IOB of \Dout_reg[27]\ : label is "TRUE";
  attribute IOB of \Dout_reg[28]\ : label is "TRUE";
  attribute IOB of \Dout_reg[29]\ : label is "TRUE";
  attribute IOB of \Dout_reg[2]\ : label is "TRUE";
  attribute IOB of \Dout_reg[30]\ : label is "TRUE";
  attribute IOB of \Dout_reg[3]\ : label is "TRUE";
  attribute IOB of \Dout_reg[4]\ : label is "TRUE";
  attribute IOB of \Dout_reg[5]\ : label is "TRUE";
  attribute IOB of \Dout_reg[6]\ : label is "TRUE";
  attribute IOB of \Dout_reg[7]\ : label is "TRUE";
  attribute IOB of \Dout_reg[8]\ : label is "TRUE";
  attribute IOB of \Dout_reg[9]\ : label is "TRUE";
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(0),
      Q => Q(0),
      R => SR(0)
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(10),
      Q => Q(10),
      R => SR(0)
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(11),
      Q => Q(11),
      R => SR(0)
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(12),
      Q => Q(12),
      R => SR(0)
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(13),
      Q => Q(13),
      R => SR(0)
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(14),
      Q => Q(14),
      R => SR(0)
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(15),
      Q => Q(15),
      R => SR(0)
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(16),
      Q => Q(16),
      R => SR(0)
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(17),
      Q => Q(17),
      R => SR(0)
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(18),
      Q => Q(18),
      R => SR(0)
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(19),
      Q => Q(19),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(1),
      Q => Q(1),
      R => SR(0)
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(20),
      Q => Q(20),
      R => SR(0)
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(21),
      Q => Q(21),
      R => SR(0)
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(22),
      Q => Q(22),
      R => SR(0)
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(23),
      Q => Q(23),
      R => SR(0)
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(24),
      Q => Q(24),
      R => SR(0)
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(25),
      Q => Q(25),
      R => SR(0)
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(26),
      Q => Q(26),
      R => SR(0)
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(27),
      Q => Q(27),
      R => SR(0)
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(28),
      Q => Q(28),
      R => SR(0)
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(29),
      Q => Q(29),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(2),
      Q => Q(2),
      R => SR(0)
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(30),
      Q => Q(30),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(3),
      Q => Q(3),
      R => SR(0)
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(4),
      Q => Q(4),
      R => SR(0)
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(5),
      Q => Q(5),
      R => SR(0)
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(6),
      Q => Q(6),
      R => SR(0)
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(7),
      Q => Q(7),
      R => SR(0)
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(8),
      Q => Q(8),
      R => SR(0)
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => D(9),
      Q => Q(9),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \REGrwe_n__parameterized3\ is
  port (
    \Dout_reg[0]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \Dout_reg[0]_1\ : out STD_LOGIC;
    \Dout_reg[1]_0\ : out STD_LOGIC;
    \Dout_reg[4]_0\ : out STD_LOGIC;
    \Dout_reg[0]_2\ : out STD_LOGIC;
    \Dout_reg[1]_1\ : out STD_LOGIC;
    \Dout_reg[0]_3\ : out STD_LOGIC;
    ROTATE_RIGHT1 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[0]_4\ : out STD_LOGIC;
    \Dout_reg[0]_5\ : out STD_LOGIC;
    \Dout_reg[0]_6\ : out STD_LOGIC;
    \Dout_reg[0]_7\ : out STD_LOGIC;
    \Dout_reg[0]_8\ : out STD_LOGIC;
    \Dout_reg[0]_9\ : out STD_LOGIC;
    \Dout_reg[0]_10\ : out STD_LOGIC;
    \Dout_reg[0]_11\ : out STD_LOGIC;
    \Dout_reg[0]_12\ : out STD_LOGIC;
    \Dout_reg[1]_2\ : out STD_LOGIC;
    \Dout_reg[0]_13\ : out STD_LOGIC;
    \Dout_reg[31]\ : out STD_LOGIC;
    \Dout_reg[0]_14\ : out STD_LOGIC;
    \Dout_reg[0]_15\ : out STD_LOGIC;
    \Dout_reg[0]_16\ : out STD_LOGIC;
    \Dout_reg[4]_1\ : out STD_LOGIC;
    \Dout_reg[0]_17\ : out STD_LOGIC;
    \Dout_reg[0]_18\ : out STD_LOGIC;
    \Dout_reg[0]_19\ : out STD_LOGIC;
    \Dout_reg[1]_3\ : out STD_LOGIC;
    \Dout_reg[0]_20\ : out STD_LOGIC;
    \Dout_reg[4]_2\ : out STD_LOGIC;
    \Dout_reg[0]_21\ : out STD_LOGIC;
    \Dout_reg[0]_22\ : out STD_LOGIC;
    \Dout_reg[1]_4\ : out STD_LOGIC;
    \Dout_reg[0]_23\ : out STD_LOGIC;
    \Dout_reg[0]_24\ : out STD_LOGIC;
    \Dout_reg[0]_25\ : out STD_LOGIC;
    \Dout_reg[31]_0\ : out STD_LOGIC;
    \Dout_reg[1]_5\ : out STD_LOGIC;
    \Dout_reg[2]_0\ : in STD_LOGIC;
    \Dout_reg[2]_1\ : in STD_LOGIC;
    \Dout_reg[0]_26\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \Dout_reg[3]_0\ : in STD_LOGIC;
    \Dout[28]_i_8_0\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \Dout_reg[7]\ : in STD_LOGIC;
    \Dout_reg[7]_0\ : in STD_LOGIC;
    \Dout_reg[24]\ : in STD_LOGIC;
    \Dout_reg[24]_0\ : in STD_LOGIC;
    \Dout_reg[25]\ : in STD_LOGIC;
    \Dout_reg[27]\ : in STD_LOGIC;
    \Dout_reg[27]_0\ : in STD_LOGIC;
    \Dout_reg[28]\ : in STD_LOGIC;
    \Dout_reg[29]\ : in STD_LOGIC;
    \Dout_reg[30]\ : in STD_LOGIC;
    \Dout[0]_i_5_0\ : in STD_LOGIC;
    \Dout[0]_i_5_1\ : in STD_LOGIC;
    \Dout_reg[1]_6\ : in STD_LOGIC;
    \Dout[0]_i_8_0\ : in STD_LOGIC;
    \Dout[0]_i_8_1\ : in STD_LOGIC;
    \Dout_reg[7]_1\ : in STD_LOGIC;
    \Dout_reg[0]_27\ : in STD_LOGIC;
    \Dout_reg[0]_28\ : in STD_LOGIC;
    \Dout[23]_i_2\ : in STD_LOGIC;
    \Dout[0]_i_4__0\ : in STD_LOGIC;
    \Dout[25]_i_2\ : in STD_LOGIC;
    \Dout[27]_i_2\ : in STD_LOGIC;
    \Dout[25]_i_8_0\ : in STD_LOGIC;
    \Dout[24]_i_5_0\ : in STD_LOGIC;
    \Dout[24]_i_5_1\ : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \REGrwe_n__parameterized3\ : entity is "REGrwe_n";
end \REGrwe_n__parameterized3\;

architecture STRUCTURE of \REGrwe_n__parameterized3\ is
  signal \Dout[0]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[0]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[0]_i_7__0_n_0\ : STD_LOGIC;
  signal \Dout[0]_i_7_n_0\ : STD_LOGIC;
  signal \Dout[0]_i_8__0_n_0\ : STD_LOGIC;
  signal \Dout[0]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[1]_i_10__0_n_0\ : STD_LOGIC;
  signal \Dout[24]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[24]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[24]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[25]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[25]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[25]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[26]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[27]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[27]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[27]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[27]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[28]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[28]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[28]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[28]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[29]_i_10_n_0\ : STD_LOGIC;
  signal \Dout[29]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[29]_i_8_n_0\ : STD_LOGIC;
  signal \Dout[2]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_12_n_0\ : STD_LOGIC;
  signal \Dout[30]_i_24_n_0\ : STD_LOGIC;
  signal \Dout[3]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[3]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[4]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[4]_i_14_n_0\ : STD_LOGIC;
  signal \Dout[4]_i_15_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_22_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_23_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_24_n_0\ : STD_LOGIC;
  signal \Dout[5]_i_25_n_0\ : STD_LOGIC;
  signal \Dout[7]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[7]_i_13_n_0\ : STD_LOGIC;
  signal \Dout[8]_i_11_n_0\ : STD_LOGIC;
  signal \Dout[8]_i_13_n_0\ : STD_LOGIC;
  signal \^dout_reg[0]_11\ : STD_LOGIC;
  signal \^dout_reg[0]_19\ : STD_LOGIC;
  signal \^dout_reg[0]_3\ : STD_LOGIC;
  signal \^dout_reg[0]_5\ : STD_LOGIC;
  signal \^dout_reg[0]_7\ : STD_LOGIC;
  signal \^dout_reg[0]_9\ : STD_LOGIC;
  signal \^dout_reg[1]_0\ : STD_LOGIC;
  signal \^dout_reg[1]_1\ : STD_LOGIC;
  signal \^dout_reg[1]_2\ : STD_LOGIC;
  signal \^dout_reg[1]_3\ : STD_LOGIC;
  signal \^dout_reg[31]\ : STD_LOGIC;
  signal \^dout_reg[4]_0\ : STD_LOGIC;
  signal \^dout_reg[4]_2\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^rotate_right1\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Dout[0]_i_10\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \Dout[0]_i_10__0\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \Dout[0]_i_11\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \Dout[0]_i_7__0\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \Dout[22]_i_16\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \Dout[23]_i_6\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \Dout[24]_i_14\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \Dout[24]_i_6\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \Dout[25]_i_6\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \Dout[26]_i_12\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \Dout[27]_i_14\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \Dout[27]_i_6\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \Dout[28]_i_6\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \Dout[29]_i_8\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \Dout[30]_i_12\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \Dout[30]_i_15\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \Dout[30]_i_27\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \Dout[30]_i_29\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \Dout[30]_i_35\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \Dout[30]_i_36\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \Dout[3]_i_8\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \Dout[4]_i_14\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \Dout[4]_i_15\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \Dout[5]_i_22\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \Dout[5]_i_24\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \Dout[7]_i_11\ : label is "soft_lutpair41";
begin
  \Dout_reg[0]_11\ <= \^dout_reg[0]_11\;
  \Dout_reg[0]_19\ <= \^dout_reg[0]_19\;
  \Dout_reg[0]_3\ <= \^dout_reg[0]_3\;
  \Dout_reg[0]_5\ <= \^dout_reg[0]_5\;
  \Dout_reg[0]_7\ <= \^dout_reg[0]_7\;
  \Dout_reg[0]_9\ <= \^dout_reg[0]_9\;
  \Dout_reg[1]_0\ <= \^dout_reg[1]_0\;
  \Dout_reg[1]_1\ <= \^dout_reg[1]_1\;
  \Dout_reg[1]_2\ <= \^dout_reg[1]_2\;
  \Dout_reg[1]_3\ <= \^dout_reg[1]_3\;
  \Dout_reg[31]\ <= \^dout_reg[31]\;
  \Dout_reg[4]_0\ <= \^dout_reg[4]_0\;
  \Dout_reg[4]_2\ <= \^dout_reg[4]_2\;
  Q(4 downto 0) <= \^q\(4 downto 0);
  ROTATE_RIGHT1(3 downto 0) <= \^rotate_right1\(3 downto 0);
\Dout[0]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEAB02A8"
    )
        port map (
      I0 => \Dout[0]_i_8_0\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \Dout[0]_i_8_1\,
      O => \Dout[0]_i_10_n_0\
    );
\Dout[0]_i_10__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA80001"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(3),
      O => \^dout_reg[4]_0\
    );
\Dout[0]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"81"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      O => \Dout[0]_i_11_n_0\
    );
\Dout[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAA0000CCCCF000"
    )
        port map (
      I0 => \Dout_reg[1]_6\,
      I1 => \Dout_reg[0]_27\,
      I2 => \Dout[0]_i_7__0_n_0\,
      I3 => \Dout[0]_i_8__0_n_0\,
      I4 => \Dout_reg[0]_28\,
      I5 => \^q\(0),
      O => \Dout_reg[0]_21\
    );
\Dout[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888880888000"
    )
        port map (
      I0 => \Dout_reg[0]_26\(0),
      I1 => \Dout_reg[0]_26\(1),
      I2 => \Dout_reg[30]\,
      I3 => \^q\(0),
      I4 => \Dout[0]_i_7_n_0\,
      I5 => \Dout[0]_i_8_n_0\,
      O => \Dout_reg[0]_14\
    );
\Dout[0]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \Dout_reg[0]_26\(0),
      I2 => \Dout_reg[0]_26\(1),
      I3 => \^dout_reg[4]_0\,
      I4 => \Dout[28]_i_8_0\(0),
      I5 => \Dout[0]_i_11_n_0\,
      O => \Dout_reg[0]_15\
    );
\Dout[0]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000080800000FF00"
    )
        port map (
      I0 => \Dout[0]_i_7__0_n_0\,
      I1 => \Dout[28]_i_8_0\(15),
      I2 => \^dout_reg[4]_2\,
      I3 => \Dout[0]_i_4__0\,
      I4 => \^q\(0),
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout_reg[31]_0\
    );
\Dout[0]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \Dout[28]_i_8_0\(15),
      I3 => \^q\(4),
      I4 => \^q\(3),
      O => \Dout[0]_i_7_n_0\
    );
\Dout[0]_i_7__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      O => \Dout[0]_i_7__0_n_0\
    );
\Dout[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000B8B8BB88"
    )
        port map (
      I0 => \Dout[0]_i_10_n_0\,
      I1 => \^q\(1),
      I2 => \Dout[0]_i_5_0\,
      I3 => \Dout[0]_i_5_1\,
      I4 => \^q\(2),
      I5 => \^q\(0),
      O => \Dout[0]_i_8_n_0\
    );
\Dout[0]_i_8__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(4),
      I2 => \Dout[28]_i_8_0\(0),
      O => \Dout[0]_i_8__0_n_0\
    );
\Dout[1]_i_10__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000002"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(0),
      I1 => \^q\(4),
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => \^q\(3),
      O => \Dout[1]_i_10__0_n_0\
    );
\Dout[1]_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE400E4"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^dout_reg[1]_3\,
      I2 => \^dout_reg[0]_19\,
      I3 => \Dout_reg[0]_26\(0),
      I4 => \Dout_reg[7]_0\,
      O => \Dout_reg[0]_20\
    );
\Dout[1]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFCBBB800000000"
    )
        port map (
      I0 => \Dout[1]_i_10__0_n_0\,
      I1 => \^q\(0),
      I2 => \Dout[2]_i_11_n_0\,
      I3 => \Dout_reg[1]_6\,
      I4 => \Dout_reg[2]_0\,
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout_reg[0]_16\
    );
\Dout[1]_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000008800C000"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(0),
      I1 => \Dout[0]_i_7__0_n_0\,
      I2 => \Dout[28]_i_8_0\(1),
      I3 => \^dout_reg[4]_2\,
      I4 => \^q\(0),
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout_reg[0]_22\
    );
\Dout[22]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0002AAA8"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(3),
      O => \Dout_reg[4]_1\
    );
\Dout[23]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[24]_i_10_n_0\,
      I1 => \^q\(0),
      I2 => \Dout[23]_i_2\,
      O => \Dout_reg[0]_25\
    );
\Dout[24]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(14),
      I1 => \^q\(2),
      I2 => \^dout_reg[4]_2\,
      I3 => \Dout[28]_i_8_0\(10),
      I4 => \^q\(1),
      I5 => \Dout[24]_i_14_n_0\,
      O => \Dout[24]_i_10_n_0\
    );
\Dout[24]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000B0008"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(12),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \^q\(4),
      I4 => \Dout[28]_i_8_0\(8),
      O => \Dout[24]_i_14_n_0\
    );
\Dout[24]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[24]_i_8_n_0\,
      I1 => \Dout_reg[0]_26\(0),
      I2 => \^dout_reg[0]_5\,
      I3 => \Dout_reg[24]\,
      I4 => \^q\(0),
      I5 => \Dout_reg[24]_0\,
      O => \Dout_reg[0]_4\
    );
\Dout[24]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[25]_i_10_n_0\,
      I1 => \^q\(0),
      I2 => \Dout[24]_i_10_n_0\,
      O => \^dout_reg[0]_5\
    );
\Dout[24]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000B8B8FF00"
    )
        port map (
      I0 => \Dout[27]_i_13_n_0\,
      I1 => \^q\(1),
      I2 => \Dout[24]_i_5_0\,
      I3 => \Dout[24]_i_5_1\,
      I4 => \^q\(0),
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout[24]_i_8_n_0\
    );
\Dout[25]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(15),
      I1 => \^q\(2),
      I2 => \^dout_reg[4]_2\,
      I3 => \Dout[28]_i_8_0\(11),
      I4 => \^q\(1),
      I5 => \Dout[25]_i_14_n_0\,
      O => \Dout[25]_i_10_n_0\
    );
\Dout[25]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000B0008"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(13),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \^q\(4),
      I4 => \Dout[28]_i_8_0\(9),
      O => \Dout[25]_i_14_n_0\
    );
\Dout[25]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[25]_i_8_n_0\,
      I1 => \Dout_reg[0]_26\(0),
      I2 => \^dout_reg[0]_7\,
      I3 => \Dout_reg[25]\,
      I4 => \^q\(0),
      I5 => \Dout_reg[24]\,
      O => \Dout_reg[0]_6\
    );
\Dout[25]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[25]_i_2\,
      I1 => \^q\(0),
      I2 => \Dout[25]_i_10_n_0\,
      O => \^dout_reg[0]_7\
    );
\Dout[25]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FF00B8B8"
    )
        port map (
      I0 => \Dout[27]_i_13_n_0\,
      I1 => \^q\(1),
      I2 => \Dout[24]_i_5_0\,
      I3 => \Dout[26]_i_12_n_0\,
      I4 => \^q\(0),
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout[25]_i_8_n_0\
    );
\Dout[26]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[28]_i_13_n_0\,
      I1 => \^q\(1),
      I2 => \Dout[25]_i_8_0\,
      O => \Dout[26]_i_12_n_0\
    );
\Dout[26]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000B8B8FF00"
    )
        port map (
      I0 => \Dout[27]_i_12_n_0\,
      I1 => \^q\(1),
      I2 => \Dout[27]_i_13_n_0\,
      I3 => \Dout[26]_i_12_n_0\,
      I4 => \^q\(0),
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout_reg[1]_5\
    );
\Dout[27]_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00FE02"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(13),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \Dout[28]_i_8_0\(15),
      I4 => \^q\(2),
      O => \Dout[27]_i_12_n_0\
    );
\Dout[27]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00FE02"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(11),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \Dout[28]_i_8_0\(15),
      I4 => \^q\(2),
      O => \Dout[27]_i_13_n_0\
    );
\Dout[27]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[28]_i_12_n_0\,
      I1 => \^q\(1),
      I2 => \Dout[28]_i_13_n_0\,
      O => \Dout[27]_i_14_n_0\
    );
\Dout[27]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[27]_i_8_n_0\,
      I1 => \Dout_reg[0]_26\(0),
      I2 => \^dout_reg[0]_9\,
      I3 => \Dout_reg[27]\,
      I4 => \^q\(0),
      I5 => \Dout_reg[27]_0\,
      O => \Dout_reg[0]_8\
    );
\Dout[27]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[28]_i_10_n_0\,
      I1 => \^q\(0),
      I2 => \Dout[27]_i_2\,
      O => \^dout_reg[0]_9\
    );
\Dout[27]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FF00B8B8"
    )
        port map (
      I0 => \Dout[27]_i_12_n_0\,
      I1 => \^q\(1),
      I2 => \Dout[27]_i_13_n_0\,
      I3 => \Dout[27]_i_14_n_0\,
      I4 => \^q\(0),
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout[27]_i_8_n_0\
    );
\Dout[28]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000020200000300"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(14),
      I1 => \^q\(3),
      I2 => \^q\(4),
      I3 => \Dout[28]_i_8_0\(12),
      I4 => \^q\(2),
      I5 => \^q\(1),
      O => \Dout[28]_i_10_n_0\
    );
\Dout[28]_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00FE02"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(14),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \Dout[28]_i_8_0\(15),
      I4 => \^q\(2),
      O => \Dout[28]_i_12_n_0\
    );
\Dout[28]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00FE02"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(12),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \Dout[28]_i_8_0\(15),
      I4 => \^q\(2),
      O => \Dout[28]_i_13_n_0\
    );
\Dout[28]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[28]_i_8_n_0\,
      I1 => \Dout_reg[0]_26\(0),
      I2 => \^dout_reg[0]_11\,
      I3 => \Dout_reg[28]\,
      I4 => \^q\(0),
      I5 => \Dout_reg[27]\,
      O => \Dout_reg[0]_10\
    );
\Dout[28]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \Dout[29]_i_10_n_0\,
      I1 => \^q\(0),
      I2 => \Dout[28]_i_10_n_0\,
      O => \^dout_reg[0]_11\
    );
\Dout[28]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAACFC0"
    )
        port map (
      I0 => \Dout[29]_i_12_n_0\,
      I1 => \Dout[28]_i_12_n_0\,
      I2 => \^q\(1),
      I3 => \Dout[28]_i_13_n_0\,
      I4 => \^q\(0),
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout[28]_i_8_n_0\
    );
\Dout[29]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000020200000300"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(15),
      I1 => \^q\(3),
      I2 => \^q\(4),
      I3 => \Dout[28]_i_8_0\(13),
      I4 => \^q\(2),
      I5 => \^q\(1),
      O => \Dout[29]_i_10_n_0\
    );
\Dout[29]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0004"
    )
        port map (
      I0 => \^q\(1),
      I1 => \Dout[28]_i_8_0\(13),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \Dout[28]_i_8_0\(15),
      I5 => \^q\(2),
      O => \Dout[29]_i_12_n_0\
    );
\Dout[29]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout[29]_i_8_n_0\,
      I1 => \Dout_reg[0]_26\(0),
      I2 => \^dout_reg[1]_2\,
      I3 => \Dout_reg[29]\,
      I4 => \^q\(0),
      I5 => \Dout_reg[28]\,
      O => \Dout_reg[0]_12\
    );
\Dout[29]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000FFFF10000000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \Dout[28]_i_8_0\(14),
      I3 => \^dout_reg[4]_2\,
      I4 => \^q\(0),
      I5 => \Dout[29]_i_10_n_0\,
      O => \^dout_reg[1]_2\
    );
\Dout[29]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[30]_i_24_n_0\,
      I1 => \Dout[29]_i_12_n_0\,
      I2 => \^q\(0),
      I3 => \Dout_reg[0]_26\(0),
      O => \Dout[29]_i_8_n_0\
    );
\Dout[2]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000002"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(1),
      I1 => \^q\(4),
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => \^q\(3),
      O => \Dout[2]_i_11_n_0\
    );
\Dout[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEEF3E200000000"
    )
        port map (
      I0 => \Dout[3]_i_11_n_0\,
      I1 => \^q\(0),
      I2 => \Dout[2]_i_11_n_0\,
      I3 => \Dout_reg[2]_0\,
      I4 => \Dout_reg[2]_1\,
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout_reg[0]_0\
    );
\Dout[2]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000008080FF00"
    )
        port map (
      I0 => \Dout[0]_i_7__0_n_0\,
      I1 => \Dout[28]_i_8_0\(1),
      I2 => \^dout_reg[4]_2\,
      I3 => \Dout[3]_i_13_n_0\,
      I4 => \^q\(0),
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout_reg[1]_4\
    );
\Dout[30]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(15),
      I1 => \Dout[30]_i_24_n_0\,
      I2 => \^q\(0),
      I3 => \Dout_reg[0]_26\(0),
      O => \Dout[30]_i_12_n_0\
    );
\Dout[30]_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(3),
      O => \^dout_reg[4]_2\
    );
\Dout[30]_i_24\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0004"
    )
        port map (
      I0 => \^q\(1),
      I1 => \Dout[28]_i_8_0\(14),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \Dout[28]_i_8_0\(15),
      I5 => \^q\(2),
      O => \Dout[30]_i_24_n_0\
    );
\Dout[30]_i_27\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => \^rotate_right1\(0)
    );
\Dout[30]_i_29\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => \^rotate_right1\(1)
    );
\Dout[30]_i_35\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"01FE"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(3),
      O => \^rotate_right1\(2)
    );
\Dout[30]_i_36\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFE"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \^q\(4),
      O => \^rotate_right1\(3)
    );
\Dout[30]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEAEEEEEEEAEAEA"
    )
        port map (
      I0 => \Dout[30]_i_12_n_0\,
      I1 => \Dout_reg[0]_26\(0),
      I2 => \^dout_reg[31]\,
      I3 => \Dout_reg[29]\,
      I4 => \^q\(0),
      I5 => \Dout_reg[30]\,
      O => \Dout_reg[0]_13\
    );
\Dout[30]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000B000800000000"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(15),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(2),
      I4 => \Dout[28]_i_8_0\(14),
      I5 => \^dout_reg[4]_2\,
      O => \^dout_reg[31]\
    );
\Dout[3]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C0008800880000C0"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(0),
      I1 => \^dout_reg[4]_0\,
      I2 => \Dout[28]_i_8_0\(2),
      I3 => \^q\(2),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \Dout[3]_i_11_n_0\
    );
\Dout[3]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000020200000300"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(0),
      I1 => \^q\(3),
      I2 => \^q\(4),
      I3 => \Dout[28]_i_8_0\(2),
      I4 => \^q\(2),
      I5 => \^q\(1),
      O => \Dout[3]_i_13_n_0\
    );
\Dout[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFCBBB800000000"
    )
        port map (
      I0 => \Dout[3]_i_11_n_0\,
      I1 => \^q\(0),
      I2 => \^dout_reg[1]_0\,
      I3 => \Dout_reg[2]_1\,
      I4 => \Dout_reg[3]_0\,
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout_reg[0]_1\
    );
\Dout[3]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AC"
    )
        port map (
      I0 => \Dout[3]_i_13_n_0\,
      I1 => \Dout[4]_i_13_n_0\,
      I2 => \^q\(0),
      I3 => \Dout_reg[0]_26\(0),
      O => \Dout_reg[0]_23\
    );
\Dout[4]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C0008800880000C0"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(1),
      I1 => \^dout_reg[4]_0\,
      I2 => \Dout[28]_i_8_0\(3),
      I3 => \^q\(2),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \^dout_reg[1]_0\
    );
\Dout[4]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000020200000300"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(1),
      I1 => \^q\(3),
      I2 => \^q\(4),
      I3 => \Dout[28]_i_8_0\(3),
      I4 => \^q\(2),
      I5 => \^q\(1),
      O => \Dout[4]_i_13_n_0\
    );
\Dout[4]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(4),
      I2 => \Dout[28]_i_8_0\(2),
      O => \Dout[4]_i_14_n_0\
    );
\Dout[4]_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      O => \Dout[4]_i_15_n_0\
    );
\Dout[4]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAFFC0"
    )
        port map (
      I0 => \Dout[4]_i_13_n_0\,
      I1 => \Dout[4]_i_14_n_0\,
      I2 => \Dout[4]_i_15_n_0\,
      I3 => \Dout[5]_i_23_n_0\,
      I4 => \^q\(0),
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout_reg[0]_24\
    );
\Dout[5]_i_22\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000040"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \Dout[28]_i_8_0\(2),
      I3 => \^q\(4),
      I4 => \^q\(3),
      O => \Dout[5]_i_22_n_0\
    );
\Dout[5]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000003020002"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(4),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \Dout[28]_i_8_0\(0),
      I5 => \^q\(1),
      O => \Dout[5]_i_23_n_0\
    );
\Dout[5]_i_24\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000040"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \Dout[28]_i_8_0\(3),
      I3 => \^q\(4),
      I4 => \^q\(3),
      O => \Dout[5]_i_24_n_0\
    );
\Dout[5]_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000003020002"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(5),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => \Dout[28]_i_8_0\(1),
      I5 => \^q\(1),
      O => \Dout[5]_i_25_n_0\
    );
\Dout[5]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000EEEEFFF0"
    )
        port map (
      I0 => \Dout[5]_i_22_n_0\,
      I1 => \Dout[5]_i_23_n_0\,
      I2 => \Dout[5]_i_24_n_0\,
      I3 => \Dout[5]_i_25_n_0\,
      I4 => \^q\(0),
      I5 => \Dout_reg[0]_26\(0),
      O => \Dout_reg[0]_17\
    );
\Dout[7]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000B0008"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(2),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \^q\(4),
      I4 => \Dout[28]_i_8_0\(6),
      O => \Dout[7]_i_11_n_0\
    );
\Dout[7]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEAB000002A80000"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \^dout_reg[4]_0\,
      I5 => \Dout[28]_i_8_0\(6),
      O => \Dout[7]_i_13_n_0\
    );
\Dout[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A808A808A8A8080"
    )
        port map (
      I0 => \Dout_reg[7]_1\,
      I1 => \Dout_reg[7]_0\,
      I2 => \Dout_reg[0]_26\(0),
      I3 => \^dout_reg[0]_19\,
      I4 => \^dout_reg[1]_3\,
      I5 => \^q\(0),
      O => \Dout_reg[0]_18\
    );
\Dout[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAEAEAEEEA"
    )
        port map (
      I0 => \Dout_reg[7]\,
      I1 => \Dout_reg[0]_26\(0),
      I2 => \Dout_reg[7]_0\,
      I3 => \^dout_reg[1]_1\,
      I4 => \^q\(0),
      I5 => \^dout_reg[0]_3\,
      O => \Dout_reg[0]_2\
    );
\Dout[7]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(0),
      I1 => \^q\(2),
      I2 => \^dout_reg[4]_2\,
      I3 => \Dout[28]_i_8_0\(4),
      I4 => \^q\(1),
      I5 => \Dout[7]_i_11_n_0\,
      O => \^dout_reg[0]_19\
    );
\Dout[7]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(0),
      I1 => \^rotate_right1\(1),
      I2 => \^dout_reg[4]_0\,
      I3 => \Dout[28]_i_8_0\(4),
      I4 => \^rotate_right1\(0),
      I5 => \Dout[7]_i_13_n_0\,
      O => \^dout_reg[0]_3\
    );
\Dout[8]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000B0008"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(3),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \^q\(4),
      I4 => \Dout[28]_i_8_0\(7),
      O => \Dout[8]_i_11_n_0\
    );
\Dout[8]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEAB000002A80000"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(3),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \^dout_reg[4]_0\,
      I5 => \Dout[28]_i_8_0\(7),
      O => \Dout[8]_i_13_n_0\
    );
\Dout[8]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(1),
      I1 => \^q\(2),
      I2 => \^dout_reg[4]_2\,
      I3 => \Dout[28]_i_8_0\(5),
      I4 => \^q\(1),
      I5 => \Dout[8]_i_11_n_0\,
      O => \^dout_reg[1]_3\
    );
\Dout[8]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B080FFFFB0800000"
    )
        port map (
      I0 => \Dout[28]_i_8_0\(1),
      I1 => \^rotate_right1\(1),
      I2 => \^dout_reg[4]_0\,
      I3 => \Dout[28]_i_8_0\(5),
      I4 => \^rotate_right1\(0),
      I5 => \Dout[8]_i_13_n_0\,
      O => \^dout_reg[1]_1\
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(0),
      Q => \^q\(0),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(1),
      Q => \^q\(1),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(2),
      Q => \^q\(2),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(3),
      Q => \^q\(3),
      R => SR(0)
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => E(0),
      D => D(4),
      Q => \^q\(4),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity SUBTRACTOR_n is
  port (
    \Dout_reg[31]\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_Sub_in : out STD_LOGIC_VECTOR ( 25 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[4]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[8]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[12]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[16]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[20]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[24]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end SUBTRACTOR_n;

architecture STRUCTURE of SUBTRACTOR_n is
  signal \S0_carry__0_n_0\ : STD_LOGIC;
  signal \S0_carry__0_n_1\ : STD_LOGIC;
  signal \S0_carry__0_n_2\ : STD_LOGIC;
  signal \S0_carry__0_n_3\ : STD_LOGIC;
  signal \S0_carry__1_n_0\ : STD_LOGIC;
  signal \S0_carry__1_n_1\ : STD_LOGIC;
  signal \S0_carry__1_n_2\ : STD_LOGIC;
  signal \S0_carry__1_n_3\ : STD_LOGIC;
  signal \S0_carry__2_n_0\ : STD_LOGIC;
  signal \S0_carry__2_n_1\ : STD_LOGIC;
  signal \S0_carry__2_n_2\ : STD_LOGIC;
  signal \S0_carry__2_n_3\ : STD_LOGIC;
  signal \S0_carry__3_n_0\ : STD_LOGIC;
  signal \S0_carry__3_n_1\ : STD_LOGIC;
  signal \S0_carry__3_n_2\ : STD_LOGIC;
  signal \S0_carry__3_n_3\ : STD_LOGIC;
  signal \S0_carry__4_n_0\ : STD_LOGIC;
  signal \S0_carry__4_n_1\ : STD_LOGIC;
  signal \S0_carry__4_n_2\ : STD_LOGIC;
  signal \S0_carry__4_n_3\ : STD_LOGIC;
  signal \S0_carry__5_n_0\ : STD_LOGIC;
  signal \S0_carry__5_n_1\ : STD_LOGIC;
  signal \S0_carry__5_n_2\ : STD_LOGIC;
  signal \S0_carry__5_n_3\ : STD_LOGIC;
  signal \S0_carry__6_n_1\ : STD_LOGIC;
  signal \S0_carry__6_n_2\ : STD_LOGIC;
  signal \S0_carry__6_n_3\ : STD_LOGIC;
  signal S0_carry_n_0 : STD_LOGIC;
  signal S0_carry_n_1 : STD_LOGIC;
  signal S0_carry_n_2 : STD_LOGIC;
  signal S0_carry_n_3 : STD_LOGIC;
begin
S0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => S0_carry_n_0,
      CO(2) => S0_carry_n_1,
      CO(1) => S0_carry_n_2,
      CO(0) => S0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => Q(3 downto 0),
      O(3 downto 1) => \Dout_reg[31]\(2 downto 0),
      O(0) => S_Sub_in(0),
      S(3 downto 0) => S(3 downto 0)
    );
\S0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => S0_carry_n_0,
      CO(3) => \S0_carry__0_n_0\,
      CO(2) => \S0_carry__0_n_1\,
      CO(1) => \S0_carry__0_n_2\,
      CO(0) => \S0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(7 downto 4),
      O(3 downto 2) => S_Sub_in(2 downto 1),
      O(1 downto 0) => \Dout_reg[31]\(4 downto 3),
      S(3 downto 0) => \Dout[4]_i_4\(3 downto 0)
    );
\S0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__0_n_0\,
      CO(3) => \S0_carry__1_n_0\,
      CO(2) => \S0_carry__1_n_1\,
      CO(1) => \S0_carry__1_n_2\,
      CO(0) => \S0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(11 downto 8),
      O(3 downto 0) => S_Sub_in(6 downto 3),
      S(3 downto 0) => \Dout[8]_i_4\(3 downto 0)
    );
\S0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__1_n_0\,
      CO(3) => \S0_carry__2_n_0\,
      CO(2) => \S0_carry__2_n_1\,
      CO(1) => \S0_carry__2_n_2\,
      CO(0) => \S0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(15 downto 12),
      O(3 downto 0) => S_Sub_in(10 downto 7),
      S(3 downto 0) => \Dout[12]_i_4\(3 downto 0)
    );
\S0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__2_n_0\,
      CO(3) => \S0_carry__3_n_0\,
      CO(2) => \S0_carry__3_n_1\,
      CO(1) => \S0_carry__3_n_2\,
      CO(0) => \S0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(19 downto 16),
      O(3 downto 0) => S_Sub_in(14 downto 11),
      S(3 downto 0) => \Dout[16]_i_4\(3 downto 0)
    );
\S0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__3_n_0\,
      CO(3) => \S0_carry__4_n_0\,
      CO(2) => \S0_carry__4_n_1\,
      CO(1) => \S0_carry__4_n_2\,
      CO(0) => \S0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(23 downto 20),
      O(3 downto 0) => S_Sub_in(18 downto 15),
      S(3 downto 0) => \Dout[20]_i_4\(3 downto 0)
    );
\S0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__4_n_0\,
      CO(3) => \S0_carry__5_n_0\,
      CO(2) => \S0_carry__5_n_1\,
      CO(1) => \S0_carry__5_n_2\,
      CO(0) => \S0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(27 downto 24),
      O(3 downto 0) => S_Sub_in(22 downto 19),
      S(3 downto 0) => \Dout[24]_i_4\(3 downto 0)
    );
\S0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \S0_carry__5_n_0\,
      CO(3) => CO(0),
      CO(2) => \S0_carry__6_n_1\,
      CO(1) => \S0_carry__6_n_2\,
      CO(0) => \S0_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => \Dout_reg[3]\(0),
      DI(2 downto 0) => Q(30 downto 28),
      O(3) => \Dout_reg[31]\(5),
      O(2 downto 0) => S_Sub_in(25 downto 23),
      S(3 downto 0) => \Dout_reg[3]_0\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ADD_SUB_n is
  port (
    \Dout_reg[31]\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[30]\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \Dout_reg[30]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[1]\ : out STD_LOGIC;
    \Dout_reg[1]_0\ : out STD_LOGIC;
    \Dout_reg[1]_1\ : out STD_LOGIC;
    \Dout_reg[1]_2\ : out STD_LOGIC;
    \Dout_reg[1]_3\ : out STD_LOGIC;
    \Dout_reg[1]_4\ : out STD_LOGIC;
    \Dout_reg[1]_5\ : out STD_LOGIC;
    \Dout_reg[1]_6\ : out STD_LOGIC;
    \Dout_reg[1]_7\ : out STD_LOGIC;
    \Dout_reg[1]_8\ : out STD_LOGIC;
    \Dout_reg[1]_9\ : out STD_LOGIC;
    \Dout_reg[1]_10\ : out STD_LOGIC;
    \Dout_reg[1]_11\ : out STD_LOGIC;
    \Dout_reg[1]_12\ : out STD_LOGIC;
    \Dout_reg[1]_13\ : out STD_LOGIC;
    \Dout_reg[1]_14\ : out STD_LOGIC;
    \Dout_reg[1]_15\ : out STD_LOGIC;
    \Dout_reg[1]_16\ : out STD_LOGIC;
    \Dout_reg[1]_17\ : out STD_LOGIC;
    \Dout_reg[1]_18\ : out STD_LOGIC;
    \Dout_reg[1]_19\ : out STD_LOGIC;
    \Dout_reg[1]_20\ : out STD_LOGIC;
    \Dout_reg[1]_21\ : out STD_LOGIC;
    \Dout_reg[1]_22\ : out STD_LOGIC;
    \Dout_reg[1]_23\ : out STD_LOGIC;
    \Dout_reg[1]_24\ : out STD_LOGIC;
    \Dout_reg[1]_25\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[4]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[8]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[12]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[16]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[20]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[24]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[3]\ : in STD_LOGIC_VECTOR ( 26 downto 0 );
    \Dout_reg[3]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[0]_i_2__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[4]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[8]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[12]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[16]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[20]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[24]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[0]\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end ADD_SUB_n;

architecture STRUCTURE of ADD_SUB_n is
  signal \^dout_reg[31]\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_Sub_in : STD_LOGIC_VECTOR ( 30 downto 0 );
begin
  \Dout_reg[31]\(5 downto 0) <= \^dout_reg[31]\(5 downto 0);
ADD_UNIT: entity work.ADDER_n
     port map (
      DI(0) => DI(0),
      \Dout[0]_i_2__0_0\(3 downto 0) => \Dout[0]_i_2__0\(3 downto 0),
      \Dout[12]_i_4_0\(3 downto 0) => \Dout[12]_i_4_0\(3 downto 0),
      \Dout[16]_i_4_0\(3 downto 0) => \Dout[16]_i_4_0\(3 downto 0),
      \Dout[20]_i_4_0\(3 downto 0) => \Dout[20]_i_4_0\(3 downto 0),
      \Dout[24]_i_4_0\(3 downto 0) => \Dout[24]_i_4_0\(3 downto 0),
      \Dout[4]_i_4\(3 downto 0) => \Dout[4]_i_4_0\(3 downto 0),
      \Dout[8]_i_4_0\(3 downto 0) => \Dout[8]_i_4_0\(3 downto 0),
      \Dout_reg[0]\(1 downto 0) => \Dout_reg[0]\(1 downto 0),
      \Dout_reg[0]_0\(26 downto 0) => \Dout_reg[3]\(26 downto 0),
      \Dout_reg[1]\ => \Dout_reg[1]\,
      \Dout_reg[1]_0\ => \Dout_reg[1]_0\,
      \Dout_reg[1]_1\ => \Dout_reg[1]_1\,
      \Dout_reg[1]_10\ => \Dout_reg[1]_10\,
      \Dout_reg[1]_11\ => \Dout_reg[1]_11\,
      \Dout_reg[1]_12\ => \Dout_reg[1]_12\,
      \Dout_reg[1]_13\ => \Dout_reg[1]_13\,
      \Dout_reg[1]_14\ => \Dout_reg[1]_14\,
      \Dout_reg[1]_15\ => \Dout_reg[1]_15\,
      \Dout_reg[1]_16\ => \Dout_reg[1]_16\,
      \Dout_reg[1]_17\ => \Dout_reg[1]_17\,
      \Dout_reg[1]_18\ => \Dout_reg[1]_18\,
      \Dout_reg[1]_19\ => \Dout_reg[1]_19\,
      \Dout_reg[1]_2\ => \Dout_reg[1]_2\,
      \Dout_reg[1]_20\ => \Dout_reg[1]_20\,
      \Dout_reg[1]_21\ => \Dout_reg[1]_21\,
      \Dout_reg[1]_22\ => \Dout_reg[1]_22\,
      \Dout_reg[1]_23\ => \Dout_reg[1]_23\,
      \Dout_reg[1]_24\ => \Dout_reg[1]_24\,
      \Dout_reg[1]_25\ => \Dout_reg[1]_25\,
      \Dout_reg[1]_3\ => \Dout_reg[1]_3\,
      \Dout_reg[1]_4\ => \Dout_reg[1]_4\,
      \Dout_reg[1]_5\ => \Dout_reg[1]_5\,
      \Dout_reg[1]_6\ => \Dout_reg[1]_6\,
      \Dout_reg[1]_7\ => \Dout_reg[1]_7\,
      \Dout_reg[1]_8\ => \Dout_reg[1]_8\,
      \Dout_reg[1]_9\ => \Dout_reg[1]_9\,
      \Dout_reg[30]\(5 downto 0) => \Dout_reg[30]\(5 downto 0),
      \Dout_reg[30]_0\(0) => \Dout_reg[30]_0\(0),
      \Dout_reg[3]\(3 downto 0) => \Dout_reg[3]_1\(3 downto 0),
      O(3) => \^dout_reg[31]\(5),
      O(2 downto 0) => S_Sub_in(30 downto 28),
      Q(31 downto 0) => Q(31 downto 0),
      S_Sub_in(22 downto 1) => S_Sub_in(27 downto 6),
      S_Sub_in(0) => S_Sub_in(0)
    );
SUB_UNIT: entity work.SUBTRACTOR_n
     port map (
      CO(0) => CO(0),
      \Dout[12]_i_4\(3 downto 0) => \Dout[12]_i_4\(3 downto 0),
      \Dout[16]_i_4\(3 downto 0) => \Dout[16]_i_4\(3 downto 0),
      \Dout[20]_i_4\(3 downto 0) => \Dout[20]_i_4\(3 downto 0),
      \Dout[24]_i_4\(3 downto 0) => \Dout[24]_i_4\(3 downto 0),
      \Dout[4]_i_4\(3 downto 0) => \Dout[4]_i_4\(3 downto 0),
      \Dout[8]_i_4\(3 downto 0) => \Dout[8]_i_4\(3 downto 0),
      \Dout_reg[31]\(5 downto 0) => \^dout_reg[31]\(5 downto 0),
      \Dout_reg[3]\(0) => \Dout_reg[3]\(26),
      \Dout_reg[3]_0\(3 downto 0) => \Dout_reg[3]_0\(3 downto 0),
      Q(30 downto 0) => Q(30 downto 0),
      S(3 downto 0) => S(3 downto 0),
      S_Sub_in(25 downto 1) => S_Sub_in(30 downto 6),
      S_Sub_in(0) => S_Sub_in(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ALU_n is
  port (
    \Dout_reg[31]\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[30]\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \Dout_reg[30]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[1]\ : out STD_LOGIC;
    \Dout_reg[1]_0\ : out STD_LOGIC;
    \Dout_reg[1]_1\ : out STD_LOGIC;
    \Dout_reg[1]_2\ : out STD_LOGIC;
    \Dout_reg[1]_3\ : out STD_LOGIC;
    \Dout_reg[1]_4\ : out STD_LOGIC;
    \Dout_reg[1]_5\ : out STD_LOGIC;
    \Dout_reg[1]_6\ : out STD_LOGIC;
    \Dout_reg[1]_7\ : out STD_LOGIC;
    \Dout_reg[1]_8\ : out STD_LOGIC;
    \Dout_reg[1]_9\ : out STD_LOGIC;
    \Dout_reg[1]_10\ : out STD_LOGIC;
    \Dout_reg[1]_11\ : out STD_LOGIC;
    \Dout_reg[1]_12\ : out STD_LOGIC;
    \Dout_reg[1]_13\ : out STD_LOGIC;
    \Dout_reg[1]_14\ : out STD_LOGIC;
    \Dout_reg[1]_15\ : out STD_LOGIC;
    \Dout_reg[1]_16\ : out STD_LOGIC;
    \Dout_reg[1]_17\ : out STD_LOGIC;
    \Dout_reg[1]_18\ : out STD_LOGIC;
    \Dout_reg[1]_19\ : out STD_LOGIC;
    \Dout_reg[1]_20\ : out STD_LOGIC;
    \Dout_reg[1]_21\ : out STD_LOGIC;
    \Dout_reg[1]_22\ : out STD_LOGIC;
    \Dout_reg[1]_23\ : out STD_LOGIC;
    \Dout_reg[1]_24\ : out STD_LOGIC;
    \Dout_reg[1]_25\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[4]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[8]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[12]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[16]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[20]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[24]_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[3]\ : in STD_LOGIC_VECTOR ( 26 downto 0 );
    \Dout_reg[3]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[0]_i_2__0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[4]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[8]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[12]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[16]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[20]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout[24]_i_4_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[3]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[0]\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end ALU_n;

architecture STRUCTURE of ALU_n is
begin
ADD_SUB: entity work.ADD_SUB_n
     port map (
      CO(0) => CO(0),
      DI(0) => DI(0),
      \Dout[0]_i_2__0\(3 downto 0) => \Dout[0]_i_2__0\(3 downto 0),
      \Dout[12]_i_4\(3 downto 0) => \Dout[12]_i_4\(3 downto 0),
      \Dout[12]_i_4_0\(3 downto 0) => \Dout[12]_i_4_0\(3 downto 0),
      \Dout[16]_i_4\(3 downto 0) => \Dout[16]_i_4\(3 downto 0),
      \Dout[16]_i_4_0\(3 downto 0) => \Dout[16]_i_4_0\(3 downto 0),
      \Dout[20]_i_4\(3 downto 0) => \Dout[20]_i_4\(3 downto 0),
      \Dout[20]_i_4_0\(3 downto 0) => \Dout[20]_i_4_0\(3 downto 0),
      \Dout[24]_i_4\(3 downto 0) => \Dout[24]_i_4\(3 downto 0),
      \Dout[24]_i_4_0\(3 downto 0) => \Dout[24]_i_4_0\(3 downto 0),
      \Dout[4]_i_4\(3 downto 0) => \Dout[4]_i_4\(3 downto 0),
      \Dout[4]_i_4_0\(3 downto 0) => \Dout[4]_i_4_0\(3 downto 0),
      \Dout[8]_i_4\(3 downto 0) => \Dout[8]_i_4\(3 downto 0),
      \Dout[8]_i_4_0\(3 downto 0) => \Dout[8]_i_4_0\(3 downto 0),
      \Dout_reg[0]\(1 downto 0) => \Dout_reg[0]\(1 downto 0),
      \Dout_reg[1]\ => \Dout_reg[1]\,
      \Dout_reg[1]_0\ => \Dout_reg[1]_0\,
      \Dout_reg[1]_1\ => \Dout_reg[1]_1\,
      \Dout_reg[1]_10\ => \Dout_reg[1]_10\,
      \Dout_reg[1]_11\ => \Dout_reg[1]_11\,
      \Dout_reg[1]_12\ => \Dout_reg[1]_12\,
      \Dout_reg[1]_13\ => \Dout_reg[1]_13\,
      \Dout_reg[1]_14\ => \Dout_reg[1]_14\,
      \Dout_reg[1]_15\ => \Dout_reg[1]_15\,
      \Dout_reg[1]_16\ => \Dout_reg[1]_16\,
      \Dout_reg[1]_17\ => \Dout_reg[1]_17\,
      \Dout_reg[1]_18\ => \Dout_reg[1]_18\,
      \Dout_reg[1]_19\ => \Dout_reg[1]_19\,
      \Dout_reg[1]_2\ => \Dout_reg[1]_2\,
      \Dout_reg[1]_20\ => \Dout_reg[1]_20\,
      \Dout_reg[1]_21\ => \Dout_reg[1]_21\,
      \Dout_reg[1]_22\ => \Dout_reg[1]_22\,
      \Dout_reg[1]_23\ => \Dout_reg[1]_23\,
      \Dout_reg[1]_24\ => \Dout_reg[1]_24\,
      \Dout_reg[1]_25\ => \Dout_reg[1]_25\,
      \Dout_reg[1]_3\ => \Dout_reg[1]_3\,
      \Dout_reg[1]_4\ => \Dout_reg[1]_4\,
      \Dout_reg[1]_5\ => \Dout_reg[1]_5\,
      \Dout_reg[1]_6\ => \Dout_reg[1]_6\,
      \Dout_reg[1]_7\ => \Dout_reg[1]_7\,
      \Dout_reg[1]_8\ => \Dout_reg[1]_8\,
      \Dout_reg[1]_9\ => \Dout_reg[1]_9\,
      \Dout_reg[30]\(5 downto 0) => \Dout_reg[30]\(5 downto 0),
      \Dout_reg[30]_0\(0) => \Dout_reg[30]_0\(0),
      \Dout_reg[31]\(5 downto 0) => \Dout_reg[31]\(5 downto 0),
      \Dout_reg[3]\(26 downto 0) => \Dout_reg[3]\(26 downto 0),
      \Dout_reg[3]_0\(3 downto 0) => \Dout_reg[3]_0\(3 downto 0),
      \Dout_reg[3]_1\(3 downto 0) => \Dout_reg[3]_1\(3 downto 0),
      Q(31 downto 0) => Q(31 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ALU_REGWE_32 is
  port (
    SRC_A : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SRC_B : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ALUControl : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SHAMT5 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    Flags : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ALUResult : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK : in STD_LOGIC;
    RESET : in STD_LOGIC;
    WE : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of ALU_REGWE_32 : entity is true;
end ALU_REGWE_32;

architecture STRUCTURE of ALU_REGWE_32 is
  signal \ADD_SUB/C_Add_in\ : STD_LOGIC;
  signal \ADD_SUB/C_Sub_in\ : STD_LOGIC;
  signal \ADD_SUB/S_Add_in\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \ADD_SUB/S_Sub_in\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal ALUControl_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ALUControl_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal ALUResult_OBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ALUResult_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal CLK_IBUF : STD_LOGIC;
  signal CLK_IBUF_BUFG : STD_LOGIC;
  signal Flags_OBUF : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal Flags_in : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal RESET_IBUF : STD_LOGIC;
  signal ROTATE_RIGHT1 : STD_LOGIC_VECTOR ( 4 downto 1 );
  signal SHAMT5_IBUF : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal SRC_A_IBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal SRC_A_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal SRC_B_IBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal SRC_B_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal WE_IBUF : STD_LOGIC;
  signal Z : STD_LOGIC;
  signal alu_in_n_13 : STD_LOGIC;
  signal alu_in_n_14 : STD_LOGIC;
  signal alu_in_n_15 : STD_LOGIC;
  signal alu_in_n_16 : STD_LOGIC;
  signal alu_in_n_17 : STD_LOGIC;
  signal alu_in_n_18 : STD_LOGIC;
  signal alu_in_n_19 : STD_LOGIC;
  signal alu_in_n_20 : STD_LOGIC;
  signal alu_in_n_21 : STD_LOGIC;
  signal alu_in_n_22 : STD_LOGIC;
  signal alu_in_n_23 : STD_LOGIC;
  signal alu_in_n_24 : STD_LOGIC;
  signal alu_in_n_25 : STD_LOGIC;
  signal alu_in_n_26 : STD_LOGIC;
  signal alu_in_n_27 : STD_LOGIC;
  signal alu_in_n_28 : STD_LOGIC;
  signal alu_in_n_29 : STD_LOGIC;
  signal alu_in_n_30 : STD_LOGIC;
  signal alu_in_n_31 : STD_LOGIC;
  signal alu_in_n_32 : STD_LOGIC;
  signal alu_in_n_33 : STD_LOGIC;
  signal alu_in_n_34 : STD_LOGIC;
  signal alu_in_n_35 : STD_LOGIC;
  signal alu_in_n_36 : STD_LOGIC;
  signal alu_in_n_37 : STD_LOGIC;
  signal alu_in_n_38 : STD_LOGIC;
  signal alu_in_n_39 : STD_LOGIC;
  signal alu_in_n_40 : STD_LOGIC;
  signal alu_in_n_6 : STD_LOGIC;
  signal reg_a_n_0 : STD_LOGIC;
  signal reg_a_n_1 : STD_LOGIC;
  signal reg_a_n_34 : STD_LOGIC;
  signal reg_a_n_35 : STD_LOGIC;
  signal reg_a_n_36 : STD_LOGIC;
  signal reg_a_n_37 : STD_LOGIC;
  signal reg_a_n_38 : STD_LOGIC;
  signal reg_a_n_39 : STD_LOGIC;
  signal reg_a_n_40 : STD_LOGIC;
  signal reg_a_n_41 : STD_LOGIC;
  signal reg_a_n_42 : STD_LOGIC;
  signal reg_a_n_43 : STD_LOGIC;
  signal reg_a_n_44 : STD_LOGIC;
  signal reg_a_n_45 : STD_LOGIC;
  signal reg_a_n_46 : STD_LOGIC;
  signal reg_a_n_47 : STD_LOGIC;
  signal reg_a_n_48 : STD_LOGIC;
  signal reg_a_n_49 : STD_LOGIC;
  signal reg_a_n_50 : STD_LOGIC;
  signal reg_a_n_51 : STD_LOGIC;
  signal reg_a_n_52 : STD_LOGIC;
  signal reg_a_n_53 : STD_LOGIC;
  signal reg_a_n_54 : STD_LOGIC;
  signal reg_a_n_55 : STD_LOGIC;
  signal reg_a_n_56 : STD_LOGIC;
  signal reg_a_n_57 : STD_LOGIC;
  signal reg_a_n_58 : STD_LOGIC;
  signal reg_a_n_59 : STD_LOGIC;
  signal reg_a_n_60 : STD_LOGIC;
  signal reg_a_n_61 : STD_LOGIC;
  signal reg_a_n_62 : STD_LOGIC;
  signal reg_a_n_63 : STD_LOGIC;
  signal reg_a_n_64 : STD_LOGIC;
  signal reg_a_n_65 : STD_LOGIC;
  signal reg_alu_control_n_37 : STD_LOGIC;
  signal reg_alu_control_n_38 : STD_LOGIC;
  signal reg_alu_control_n_39 : STD_LOGIC;
  signal reg_alu_control_n_40 : STD_LOGIC;
  signal reg_alu_control_n_41 : STD_LOGIC;
  signal reg_b_n_0 : STD_LOGIC;
  signal reg_b_n_100 : STD_LOGIC;
  signal reg_b_n_101 : STD_LOGIC;
  signal reg_b_n_102 : STD_LOGIC;
  signal reg_b_n_103 : STD_LOGIC;
  signal reg_b_n_104 : STD_LOGIC;
  signal reg_b_n_105 : STD_LOGIC;
  signal reg_b_n_106 : STD_LOGIC;
  signal reg_b_n_107 : STD_LOGIC;
  signal reg_b_n_108 : STD_LOGIC;
  signal reg_b_n_109 : STD_LOGIC;
  signal reg_b_n_110 : STD_LOGIC;
  signal reg_b_n_111 : STD_LOGIC;
  signal reg_b_n_112 : STD_LOGIC;
  signal reg_b_n_113 : STD_LOGIC;
  signal reg_b_n_114 : STD_LOGIC;
  signal reg_b_n_115 : STD_LOGIC;
  signal reg_b_n_116 : STD_LOGIC;
  signal reg_b_n_117 : STD_LOGIC;
  signal reg_b_n_118 : STD_LOGIC;
  signal reg_b_n_119 : STD_LOGIC;
  signal reg_b_n_120 : STD_LOGIC;
  signal reg_b_n_121 : STD_LOGIC;
  signal reg_b_n_122 : STD_LOGIC;
  signal reg_b_n_123 : STD_LOGIC;
  signal reg_b_n_124 : STD_LOGIC;
  signal reg_b_n_125 : STD_LOGIC;
  signal reg_b_n_126 : STD_LOGIC;
  signal reg_b_n_127 : STD_LOGIC;
  signal reg_b_n_128 : STD_LOGIC;
  signal reg_b_n_129 : STD_LOGIC;
  signal reg_b_n_130 : STD_LOGIC;
  signal reg_b_n_131 : STD_LOGIC;
  signal reg_b_n_132 : STD_LOGIC;
  signal reg_b_n_133 : STD_LOGIC;
  signal reg_b_n_134 : STD_LOGIC;
  signal reg_b_n_135 : STD_LOGIC;
  signal reg_b_n_136 : STD_LOGIC;
  signal reg_b_n_137 : STD_LOGIC;
  signal reg_b_n_138 : STD_LOGIC;
  signal reg_b_n_139 : STD_LOGIC;
  signal reg_b_n_140 : STD_LOGIC;
  signal reg_b_n_141 : STD_LOGIC;
  signal reg_b_n_142 : STD_LOGIC;
  signal reg_b_n_143 : STD_LOGIC;
  signal reg_b_n_144 : STD_LOGIC;
  signal reg_b_n_145 : STD_LOGIC;
  signal reg_b_n_146 : STD_LOGIC;
  signal reg_b_n_147 : STD_LOGIC;
  signal reg_b_n_148 : STD_LOGIC;
  signal reg_b_n_149 : STD_LOGIC;
  signal reg_b_n_150 : STD_LOGIC;
  signal reg_b_n_151 : STD_LOGIC;
  signal reg_b_n_3 : STD_LOGIC;
  signal reg_b_n_4 : STD_LOGIC;
  signal reg_b_n_40 : STD_LOGIC;
  signal reg_b_n_41 : STD_LOGIC;
  signal reg_b_n_42 : STD_LOGIC;
  signal reg_b_n_43 : STD_LOGIC;
  signal reg_b_n_44 : STD_LOGIC;
  signal reg_b_n_45 : STD_LOGIC;
  signal reg_b_n_46 : STD_LOGIC;
  signal reg_b_n_47 : STD_LOGIC;
  signal reg_b_n_48 : STD_LOGIC;
  signal reg_b_n_49 : STD_LOGIC;
  signal reg_b_n_5 : STD_LOGIC;
  signal reg_b_n_50 : STD_LOGIC;
  signal reg_b_n_51 : STD_LOGIC;
  signal reg_b_n_52 : STD_LOGIC;
  signal reg_b_n_53 : STD_LOGIC;
  signal reg_b_n_54 : STD_LOGIC;
  signal reg_b_n_55 : STD_LOGIC;
  signal reg_b_n_56 : STD_LOGIC;
  signal reg_b_n_57 : STD_LOGIC;
  signal reg_b_n_58 : STD_LOGIC;
  signal reg_b_n_59 : STD_LOGIC;
  signal reg_b_n_6 : STD_LOGIC;
  signal reg_b_n_60 : STD_LOGIC;
  signal reg_b_n_61 : STD_LOGIC;
  signal reg_b_n_62 : STD_LOGIC;
  signal reg_b_n_63 : STD_LOGIC;
  signal reg_b_n_64 : STD_LOGIC;
  signal reg_b_n_65 : STD_LOGIC;
  signal reg_b_n_66 : STD_LOGIC;
  signal reg_b_n_67 : STD_LOGIC;
  signal reg_b_n_68 : STD_LOGIC;
  signal reg_b_n_69 : STD_LOGIC;
  signal reg_b_n_7 : STD_LOGIC;
  signal reg_b_n_70 : STD_LOGIC;
  signal reg_b_n_71 : STD_LOGIC;
  signal reg_b_n_72 : STD_LOGIC;
  signal reg_b_n_73 : STD_LOGIC;
  signal reg_b_n_74 : STD_LOGIC;
  signal reg_b_n_75 : STD_LOGIC;
  signal reg_b_n_76 : STD_LOGIC;
  signal reg_b_n_77 : STD_LOGIC;
  signal reg_b_n_78 : STD_LOGIC;
  signal reg_b_n_79 : STD_LOGIC;
  signal reg_b_n_80 : STD_LOGIC;
  signal reg_b_n_81 : STD_LOGIC;
  signal reg_b_n_82 : STD_LOGIC;
  signal reg_b_n_83 : STD_LOGIC;
  signal reg_b_n_84 : STD_LOGIC;
  signal reg_b_n_85 : STD_LOGIC;
  signal reg_b_n_86 : STD_LOGIC;
  signal reg_b_n_87 : STD_LOGIC;
  signal reg_b_n_88 : STD_LOGIC;
  signal reg_b_n_89 : STD_LOGIC;
  signal reg_b_n_90 : STD_LOGIC;
  signal reg_b_n_91 : STD_LOGIC;
  signal reg_b_n_92 : STD_LOGIC;
  signal reg_b_n_93 : STD_LOGIC;
  signal reg_b_n_94 : STD_LOGIC;
  signal reg_b_n_95 : STD_LOGIC;
  signal reg_b_n_96 : STD_LOGIC;
  signal reg_b_n_97 : STD_LOGIC;
  signal reg_b_n_98 : STD_LOGIC;
  signal reg_b_n_99 : STD_LOGIC;
  signal reg_flags_n_0 : STD_LOGIC;
  signal reg_shamt_n_0 : STD_LOGIC;
  signal reg_shamt_n_1 : STD_LOGIC;
  signal reg_shamt_n_10 : STD_LOGIC;
  signal reg_shamt_n_11 : STD_LOGIC;
  signal reg_shamt_n_16 : STD_LOGIC;
  signal reg_shamt_n_17 : STD_LOGIC;
  signal reg_shamt_n_18 : STD_LOGIC;
  signal reg_shamt_n_19 : STD_LOGIC;
  signal reg_shamt_n_2 : STD_LOGIC;
  signal reg_shamt_n_20 : STD_LOGIC;
  signal reg_shamt_n_21 : STD_LOGIC;
  signal reg_shamt_n_22 : STD_LOGIC;
  signal reg_shamt_n_23 : STD_LOGIC;
  signal reg_shamt_n_24 : STD_LOGIC;
  signal reg_shamt_n_25 : STD_LOGIC;
  signal reg_shamt_n_26 : STD_LOGIC;
  signal reg_shamt_n_27 : STD_LOGIC;
  signal reg_shamt_n_28 : STD_LOGIC;
  signal reg_shamt_n_29 : STD_LOGIC;
  signal reg_shamt_n_3 : STD_LOGIC;
  signal reg_shamt_n_30 : STD_LOGIC;
  signal reg_shamt_n_31 : STD_LOGIC;
  signal reg_shamt_n_32 : STD_LOGIC;
  signal reg_shamt_n_33 : STD_LOGIC;
  signal reg_shamt_n_34 : STD_LOGIC;
  signal reg_shamt_n_35 : STD_LOGIC;
  signal reg_shamt_n_36 : STD_LOGIC;
  signal reg_shamt_n_37 : STD_LOGIC;
  signal reg_shamt_n_38 : STD_LOGIC;
  signal reg_shamt_n_39 : STD_LOGIC;
  signal reg_shamt_n_4 : STD_LOGIC;
  signal reg_shamt_n_40 : STD_LOGIC;
  signal reg_shamt_n_41 : STD_LOGIC;
  signal reg_shamt_n_42 : STD_LOGIC;
  signal reg_shamt_n_43 : STD_LOGIC;
  signal reg_shamt_n_44 : STD_LOGIC;
  signal reg_shamt_n_45 : STD_LOGIC;
  signal reg_shamt_n_5 : STD_LOGIC;
  signal reg_shamt_n_6 : STD_LOGIC;
  signal reg_shamt_n_7 : STD_LOGIC;
  signal reg_shamt_n_8 : STD_LOGIC;
  signal reg_shamt_n_9 : STD_LOGIC;
  attribute IOB : string;
  attribute IOB of \ALUResult_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[10]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[11]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[12]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[13]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[14]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[15]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[16]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[17]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[18]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[19]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[20]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[21]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[22]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[23]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[24]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[25]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[26]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[27]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[28]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[29]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[30]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[31]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[3]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[4]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[5]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[6]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[7]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[8]_inst\ : label is "TRUE";
  attribute IOB of \ALUResult_OBUF[9]_inst\ : label is "TRUE";
  attribute IOB of \Flags_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \Flags_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \Flags_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of \Flags_OBUF[3]_inst\ : label is "TRUE";
begin
\ALUControl_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ALUControl(0),
      O => ALUControl_IBUF(0)
    );
\ALUControl_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ALUControl(1),
      O => ALUControl_IBUF(1)
    );
\ALUControl_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ALUControl(2),
      O => ALUControl_IBUF(2)
    );
\ALUControl_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ALUControl(3),
      O => ALUControl_IBUF(3)
    );
\ALUResult_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(0),
      O => ALUResult(0)
    );
\ALUResult_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(10),
      O => ALUResult(10)
    );
\ALUResult_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(11),
      O => ALUResult(11)
    );
\ALUResult_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(12),
      O => ALUResult(12)
    );
\ALUResult_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(13),
      O => ALUResult(13)
    );
\ALUResult_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(14),
      O => ALUResult(14)
    );
\ALUResult_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(15),
      O => ALUResult(15)
    );
\ALUResult_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(16),
      O => ALUResult(16)
    );
\ALUResult_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(17),
      O => ALUResult(17)
    );
\ALUResult_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(18),
      O => ALUResult(18)
    );
\ALUResult_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(19),
      O => ALUResult(19)
    );
\ALUResult_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(1),
      O => ALUResult(1)
    );
\ALUResult_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(20),
      O => ALUResult(20)
    );
\ALUResult_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(21),
      O => ALUResult(21)
    );
\ALUResult_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(22),
      O => ALUResult(22)
    );
\ALUResult_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(23),
      O => ALUResult(23)
    );
\ALUResult_OBUF[24]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(24),
      O => ALUResult(24)
    );
\ALUResult_OBUF[25]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(25),
      O => ALUResult(25)
    );
\ALUResult_OBUF[26]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(26),
      O => ALUResult(26)
    );
\ALUResult_OBUF[27]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(27),
      O => ALUResult(27)
    );
\ALUResult_OBUF[28]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(28),
      O => ALUResult(28)
    );
\ALUResult_OBUF[29]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(29),
      O => ALUResult(29)
    );
\ALUResult_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(2),
      O => ALUResult(2)
    );
\ALUResult_OBUF[30]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(30),
      O => ALUResult(30)
    );
\ALUResult_OBUF[31]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(31),
      O => ALUResult(31)
    );
\ALUResult_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(3),
      O => ALUResult(3)
    );
\ALUResult_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(4),
      O => ALUResult(4)
    );
\ALUResult_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(5),
      O => ALUResult(5)
    );
\ALUResult_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(6),
      O => ALUResult(6)
    );
\ALUResult_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(7),
      O => ALUResult(7)
    );
\ALUResult_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(8),
      O => ALUResult(8)
    );
\ALUResult_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUResult_OBUF(9),
      O => ALUResult(9)
    );
CLK_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => CLK_IBUF,
      O => CLK_IBUF_BUFG
    );
CLK_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => CLK,
      O => CLK_IBUF
    );
\Flags_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => reg_flags_n_0,
      O => Flags(0)
    );
\Flags_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Flags_OBUF(1),
      O => Flags(1)
    );
\Flags_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Flags_OBUF(2),
      O => Flags(2)
    );
\Flags_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => Flags_OBUF(3),
      O => Flags(3)
    );
RESET_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RESET,
      O => RESET_IBUF
    );
\SHAMT5_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SHAMT5(0),
      O => SHAMT5_IBUF(0)
    );
\SHAMT5_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SHAMT5(1),
      O => SHAMT5_IBUF(1)
    );
\SHAMT5_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SHAMT5(2),
      O => SHAMT5_IBUF(2)
    );
\SHAMT5_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SHAMT5(3),
      O => SHAMT5_IBUF(3)
    );
\SHAMT5_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SHAMT5(4),
      O => SHAMT5_IBUF(4)
    );
\SRC_A_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(0),
      O => SRC_A_IBUF(0)
    );
\SRC_A_IBUF[10]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(10),
      O => SRC_A_IBUF(10)
    );
\SRC_A_IBUF[11]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(11),
      O => SRC_A_IBUF(11)
    );
\SRC_A_IBUF[12]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(12),
      O => SRC_A_IBUF(12)
    );
\SRC_A_IBUF[13]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(13),
      O => SRC_A_IBUF(13)
    );
\SRC_A_IBUF[14]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(14),
      O => SRC_A_IBUF(14)
    );
\SRC_A_IBUF[15]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(15),
      O => SRC_A_IBUF(15)
    );
\SRC_A_IBUF[16]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(16),
      O => SRC_A_IBUF(16)
    );
\SRC_A_IBUF[17]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(17),
      O => SRC_A_IBUF(17)
    );
\SRC_A_IBUF[18]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(18),
      O => SRC_A_IBUF(18)
    );
\SRC_A_IBUF[19]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(19),
      O => SRC_A_IBUF(19)
    );
\SRC_A_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(1),
      O => SRC_A_IBUF(1)
    );
\SRC_A_IBUF[20]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(20),
      O => SRC_A_IBUF(20)
    );
\SRC_A_IBUF[21]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(21),
      O => SRC_A_IBUF(21)
    );
\SRC_A_IBUF[22]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(22),
      O => SRC_A_IBUF(22)
    );
\SRC_A_IBUF[23]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(23),
      O => SRC_A_IBUF(23)
    );
\SRC_A_IBUF[24]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(24),
      O => SRC_A_IBUF(24)
    );
\SRC_A_IBUF[25]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(25),
      O => SRC_A_IBUF(25)
    );
\SRC_A_IBUF[26]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(26),
      O => SRC_A_IBUF(26)
    );
\SRC_A_IBUF[27]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(27),
      O => SRC_A_IBUF(27)
    );
\SRC_A_IBUF[28]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(28),
      O => SRC_A_IBUF(28)
    );
\SRC_A_IBUF[29]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(29),
      O => SRC_A_IBUF(29)
    );
\SRC_A_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(2),
      O => SRC_A_IBUF(2)
    );
\SRC_A_IBUF[30]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(30),
      O => SRC_A_IBUF(30)
    );
\SRC_A_IBUF[31]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(31),
      O => SRC_A_IBUF(31)
    );
\SRC_A_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(3),
      O => SRC_A_IBUF(3)
    );
\SRC_A_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(4),
      O => SRC_A_IBUF(4)
    );
\SRC_A_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(5),
      O => SRC_A_IBUF(5)
    );
\SRC_A_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(6),
      O => SRC_A_IBUF(6)
    );
\SRC_A_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(7),
      O => SRC_A_IBUF(7)
    );
\SRC_A_IBUF[8]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(8),
      O => SRC_A_IBUF(8)
    );
\SRC_A_IBUF[9]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_A(9),
      O => SRC_A_IBUF(9)
    );
\SRC_B_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(0),
      O => SRC_B_IBUF(0)
    );
\SRC_B_IBUF[10]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(10),
      O => SRC_B_IBUF(10)
    );
\SRC_B_IBUF[11]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(11),
      O => SRC_B_IBUF(11)
    );
\SRC_B_IBUF[12]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(12),
      O => SRC_B_IBUF(12)
    );
\SRC_B_IBUF[13]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(13),
      O => SRC_B_IBUF(13)
    );
\SRC_B_IBUF[14]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(14),
      O => SRC_B_IBUF(14)
    );
\SRC_B_IBUF[15]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(15),
      O => SRC_B_IBUF(15)
    );
\SRC_B_IBUF[16]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(16),
      O => SRC_B_IBUF(16)
    );
\SRC_B_IBUF[17]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(17),
      O => SRC_B_IBUF(17)
    );
\SRC_B_IBUF[18]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(18),
      O => SRC_B_IBUF(18)
    );
\SRC_B_IBUF[19]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(19),
      O => SRC_B_IBUF(19)
    );
\SRC_B_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(1),
      O => SRC_B_IBUF(1)
    );
\SRC_B_IBUF[20]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(20),
      O => SRC_B_IBUF(20)
    );
\SRC_B_IBUF[21]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(21),
      O => SRC_B_IBUF(21)
    );
\SRC_B_IBUF[22]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(22),
      O => SRC_B_IBUF(22)
    );
\SRC_B_IBUF[23]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(23),
      O => SRC_B_IBUF(23)
    );
\SRC_B_IBUF[24]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(24),
      O => SRC_B_IBUF(24)
    );
\SRC_B_IBUF[25]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(25),
      O => SRC_B_IBUF(25)
    );
\SRC_B_IBUF[26]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(26),
      O => SRC_B_IBUF(26)
    );
\SRC_B_IBUF[27]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(27),
      O => SRC_B_IBUF(27)
    );
\SRC_B_IBUF[28]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(28),
      O => SRC_B_IBUF(28)
    );
\SRC_B_IBUF[29]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(29),
      O => SRC_B_IBUF(29)
    );
\SRC_B_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(2),
      O => SRC_B_IBUF(2)
    );
\SRC_B_IBUF[30]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(30),
      O => SRC_B_IBUF(30)
    );
\SRC_B_IBUF[31]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(31),
      O => SRC_B_IBUF(31)
    );
\SRC_B_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(3),
      O => SRC_B_IBUF(3)
    );
\SRC_B_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(4),
      O => SRC_B_IBUF(4)
    );
\SRC_B_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(5),
      O => SRC_B_IBUF(5)
    );
\SRC_B_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(6),
      O => SRC_B_IBUF(6)
    );
\SRC_B_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(7),
      O => SRC_B_IBUF(7)
    );
\SRC_B_IBUF[8]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(8),
      O => SRC_B_IBUF(8)
    );
\SRC_B_IBUF[9]_inst\: unisim.vcomponents.IBUF
     port map (
      I => SRC_B(9),
      O => SRC_B_IBUF(9)
    );
WE_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => WE,
      O => WE_IBUF
    );
alu_in: entity work.ALU_n
     port map (
      CO(0) => alu_in_n_6,
      DI(0) => reg_a_n_1,
      \Dout[0]_i_2__0\(3) => reg_a_n_34,
      \Dout[0]_i_2__0\(2) => reg_a_n_35,
      \Dout[0]_i_2__0\(1) => reg_a_n_36,
      \Dout[0]_i_2__0\(0) => reg_a_n_37,
      \Dout[12]_i_4\(3) => reg_b_n_129,
      \Dout[12]_i_4\(2) => reg_b_n_130,
      \Dout[12]_i_4\(1) => reg_b_n_131,
      \Dout[12]_i_4\(0) => reg_b_n_132,
      \Dout[12]_i_4_0\(3) => reg_a_n_46,
      \Dout[12]_i_4_0\(2) => reg_a_n_47,
      \Dout[12]_i_4_0\(1) => reg_a_n_48,
      \Dout[12]_i_4_0\(0) => reg_a_n_49,
      \Dout[16]_i_4\(3) => reg_b_n_133,
      \Dout[16]_i_4\(2) => reg_b_n_134,
      \Dout[16]_i_4\(1) => reg_b_n_135,
      \Dout[16]_i_4\(0) => reg_b_n_136,
      \Dout[16]_i_4_0\(3) => reg_a_n_50,
      \Dout[16]_i_4_0\(2) => reg_a_n_51,
      \Dout[16]_i_4_0\(1) => reg_a_n_52,
      \Dout[16]_i_4_0\(0) => reg_a_n_53,
      \Dout[20]_i_4\(3) => reg_b_n_137,
      \Dout[20]_i_4\(2) => reg_b_n_138,
      \Dout[20]_i_4\(1) => reg_b_n_139,
      \Dout[20]_i_4\(0) => reg_b_n_140,
      \Dout[20]_i_4_0\(3) => reg_a_n_54,
      \Dout[20]_i_4_0\(2) => reg_a_n_55,
      \Dout[20]_i_4_0\(1) => reg_a_n_56,
      \Dout[20]_i_4_0\(0) => reg_a_n_57,
      \Dout[24]_i_4\(3) => reg_b_n_141,
      \Dout[24]_i_4\(2) => reg_b_n_142,
      \Dout[24]_i_4\(1) => reg_b_n_143,
      \Dout[24]_i_4\(0) => reg_b_n_144,
      \Dout[24]_i_4_0\(3) => reg_a_n_58,
      \Dout[24]_i_4_0\(2) => reg_a_n_59,
      \Dout[24]_i_4_0\(1) => reg_a_n_60,
      \Dout[24]_i_4_0\(0) => reg_a_n_61,
      \Dout[4]_i_4\(3) => reg_b_n_121,
      \Dout[4]_i_4\(2) => reg_b_n_122,
      \Dout[4]_i_4\(1) => reg_b_n_123,
      \Dout[4]_i_4\(0) => reg_b_n_124,
      \Dout[4]_i_4_0\(3) => reg_a_n_38,
      \Dout[4]_i_4_0\(2) => reg_a_n_39,
      \Dout[4]_i_4_0\(1) => reg_a_n_40,
      \Dout[4]_i_4_0\(0) => reg_a_n_41,
      \Dout[8]_i_4\(3) => reg_b_n_125,
      \Dout[8]_i_4\(2) => reg_b_n_126,
      \Dout[8]_i_4\(1) => reg_b_n_127,
      \Dout[8]_i_4\(0) => reg_b_n_128,
      \Dout[8]_i_4_0\(3) => reg_a_n_42,
      \Dout[8]_i_4_0\(2) => reg_a_n_43,
      \Dout[8]_i_4_0\(1) => reg_a_n_44,
      \Dout[8]_i_4_0\(0) => reg_a_n_45,
      \Dout_reg[0]\(1 downto 0) => ALUControl_in(1 downto 0),
      \Dout_reg[1]\ => alu_in_n_14,
      \Dout_reg[1]_0\ => alu_in_n_15,
      \Dout_reg[1]_1\ => alu_in_n_16,
      \Dout_reg[1]_10\ => alu_in_n_25,
      \Dout_reg[1]_11\ => alu_in_n_26,
      \Dout_reg[1]_12\ => alu_in_n_27,
      \Dout_reg[1]_13\ => alu_in_n_28,
      \Dout_reg[1]_14\ => alu_in_n_29,
      \Dout_reg[1]_15\ => alu_in_n_30,
      \Dout_reg[1]_16\ => alu_in_n_31,
      \Dout_reg[1]_17\ => alu_in_n_32,
      \Dout_reg[1]_18\ => alu_in_n_33,
      \Dout_reg[1]_19\ => alu_in_n_34,
      \Dout_reg[1]_2\ => alu_in_n_17,
      \Dout_reg[1]_20\ => alu_in_n_35,
      \Dout_reg[1]_21\ => alu_in_n_36,
      \Dout_reg[1]_22\ => alu_in_n_37,
      \Dout_reg[1]_23\ => alu_in_n_38,
      \Dout_reg[1]_24\ => alu_in_n_39,
      \Dout_reg[1]_25\ => alu_in_n_40,
      \Dout_reg[1]_3\ => alu_in_n_18,
      \Dout_reg[1]_4\ => alu_in_n_19,
      \Dout_reg[1]_5\ => alu_in_n_20,
      \Dout_reg[1]_6\ => alu_in_n_21,
      \Dout_reg[1]_7\ => alu_in_n_22,
      \Dout_reg[1]_8\ => alu_in_n_23,
      \Dout_reg[1]_9\ => alu_in_n_24,
      \Dout_reg[30]\(5) => \ADD_SUB/S_Add_in\(31),
      \Dout_reg[30]\(4 downto 0) => \ADD_SUB/S_Add_in\(5 downto 1),
      \Dout_reg[30]_0\(0) => alu_in_n_13,
      \Dout_reg[31]\(5) => \ADD_SUB/S_Sub_in\(31),
      \Dout_reg[31]\(4 downto 0) => \ADD_SUB/S_Sub_in\(5 downto 1),
      \Dout_reg[3]\(26 downto 1) => SRC_B_in(31 downto 6),
      \Dout_reg[3]\(0) => SRC_B_in(0),
      \Dout_reg[3]_0\(3) => reg_b_n_145,
      \Dout_reg[3]_0\(2) => reg_b_n_146,
      \Dout_reg[3]_0\(1) => reg_b_n_147,
      \Dout_reg[3]_0\(0) => reg_b_n_148,
      \Dout_reg[3]_1\(3) => reg_a_n_62,
      \Dout_reg[3]_1\(2) => reg_a_n_63,
      \Dout_reg[3]_1\(1) => reg_a_n_64,
      \Dout_reg[3]_1\(0) => reg_a_n_65,
      Q(31 downto 0) => SRC_A_in(31 downto 0),
      S(3) => reg_b_n_117,
      S(2) => reg_b_n_118,
      S(1) => reg_b_n_119,
      S(0) => reg_b_n_120
    );
reg_a: entity work.\REGrwe_n__parameterized1\
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      CO(0) => reg_a_n_0,
      D(31 downto 0) => SRC_A_IBUF(31 downto 0),
      DI(0) => reg_a_n_1,
      \Dout_reg[11]_0\(3) => reg_a_n_42,
      \Dout_reg[11]_0\(2) => reg_a_n_43,
      \Dout_reg[11]_0\(1) => reg_a_n_44,
      \Dout_reg[11]_0\(0) => reg_a_n_45,
      \Dout_reg[15]_0\(3) => reg_a_n_46,
      \Dout_reg[15]_0\(2) => reg_a_n_47,
      \Dout_reg[15]_0\(1) => reg_a_n_48,
      \Dout_reg[15]_0\(0) => reg_a_n_49,
      \Dout_reg[19]_0\(3) => reg_a_n_50,
      \Dout_reg[19]_0\(2) => reg_a_n_51,
      \Dout_reg[19]_0\(1) => reg_a_n_52,
      \Dout_reg[19]_0\(0) => reg_a_n_53,
      \Dout_reg[23]_0\(3) => reg_a_n_54,
      \Dout_reg[23]_0\(2) => reg_a_n_55,
      \Dout_reg[23]_0\(1) => reg_a_n_56,
      \Dout_reg[23]_0\(0) => reg_a_n_57,
      \Dout_reg[27]_0\(3) => reg_a_n_58,
      \Dout_reg[27]_0\(2) => reg_a_n_59,
      \Dout_reg[27]_0\(1) => reg_a_n_60,
      \Dout_reg[27]_0\(0) => reg_a_n_61,
      \Dout_reg[31]_0\(3) => reg_a_n_62,
      \Dout_reg[31]_0\(2) => reg_a_n_63,
      \Dout_reg[31]_0\(1) => reg_a_n_64,
      \Dout_reg[31]_0\(0) => reg_a_n_65,
      \Dout_reg[3]_0\(3) => reg_a_n_34,
      \Dout_reg[3]_0\(2) => reg_a_n_35,
      \Dout_reg[3]_0\(1) => reg_a_n_36,
      \Dout_reg[3]_0\(0) => reg_a_n_37,
      \Dout_reg[3]_1\(0) => alu_in_n_13,
      \Dout_reg[7]_0\(3) => reg_a_n_38,
      \Dout_reg[7]_0\(2) => reg_a_n_39,
      \Dout_reg[7]_0\(1) => reg_a_n_40,
      \Dout_reg[7]_0\(0) => reg_a_n_41,
      E(0) => WE_IBUF,
      Q(31 downto 0) => SRC_A_in(31 downto 0),
      \S0_carry__6\(31 downto 0) => SRC_B_in(31 downto 0),
      SR(0) => RESET_IBUF
    );
reg_alu_control: entity work.REGrwe_n
     port map (
      ALUResult_in(31 downto 0) => ALUResult_in(31 downto 0),
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      CO(0) => reg_a_n_0,
      D(2 downto 1) => Flags_in(3 downto 2),
      D(0) => Z,
      \Dout[1]_i_10_0\ => reg_b_n_78,
      \Dout[1]_i_5_0\ => reg_shamt_n_36,
      \Dout[1]_i_8\ => reg_b_n_80,
      \Dout_reg[0]_0\ => alu_in_n_14,
      \Dout_reg[0]_1\ => reg_b_n_108,
      \Dout_reg[0]_2\ => reg_shamt_n_28,
      \Dout_reg[0]_3\ => alu_in_n_15,
      \Dout_reg[0]_4\ => reg_shamt_n_38,
      \Dout_reg[0]_5\ => reg_shamt_n_29,
      \Dout_reg[0]_6\(31 downto 0) => SRC_A_in(31 downto 0),
      \Dout_reg[0]_7\(31 downto 0) => SRC_B_in(31 downto 0),
      \Dout_reg[10]\ => reg_b_n_75,
      \Dout_reg[10]_0\ => alu_in_n_20,
      \Dout_reg[10]_1\ => reg_b_n_47,
      \Dout_reg[11]\ => reg_b_n_76,
      \Dout_reg[11]_0\ => alu_in_n_21,
      \Dout_reg[11]_1\ => reg_b_n_48,
      \Dout_reg[12]\ => reg_b_n_77,
      \Dout_reg[12]_0\ => alu_in_n_22,
      \Dout_reg[12]_1\ => reg_b_n_49,
      \Dout_reg[13]\ => reg_b_n_88,
      \Dout_reg[13]_0\ => alu_in_n_23,
      \Dout_reg[13]_1\ => reg_b_n_50,
      \Dout_reg[14]\ => reg_b_n_91,
      \Dout_reg[14]_0\ => alu_in_n_24,
      \Dout_reg[14]_1\ => reg_b_n_51,
      \Dout_reg[15]\ => reg_b_n_94,
      \Dout_reg[15]_0\ => alu_in_n_25,
      \Dout_reg[15]_1\ => reg_b_n_52,
      \Dout_reg[16]\ => reg_b_n_95,
      \Dout_reg[16]_0\ => alu_in_n_26,
      \Dout_reg[16]_1\ => reg_b_n_53,
      \Dout_reg[17]\ => reg_b_n_96,
      \Dout_reg[17]_0\ => alu_in_n_27,
      \Dout_reg[17]_1\ => reg_b_n_54,
      \Dout_reg[18]\ => reg_b_n_55,
      \Dout_reg[18]_0\ => reg_b_n_97,
      \Dout_reg[18]_1\ => alu_in_n_28,
      \Dout_reg[19]\ => reg_b_n_98,
      \Dout_reg[19]_0\ => alu_in_n_29,
      \Dout_reg[19]_1\ => reg_b_n_56,
      \Dout_reg[1]_0\ => reg_alu_control_n_41,
      \Dout_reg[1]_1\ => reg_b_n_42,
      \Dout_reg[1]_2\ => reg_b_n_81,
      \Dout_reg[1]_3\ => reg_shamt_n_30,
      \Dout_reg[1]_4\ => reg_b_n_82,
      \Dout_reg[1]_5\ => reg_b_n_90,
      \Dout_reg[1]_6\ => reg_shamt_n_39,
      \Dout_reg[20]\ => reg_b_n_99,
      \Dout_reg[20]_0\ => alu_in_n_30,
      \Dout_reg[20]_1\ => reg_b_n_57,
      \Dout_reg[21]\ => reg_b_n_100,
      \Dout_reg[21]_0\ => alu_in_n_31,
      \Dout_reg[21]_1\ => reg_b_n_58,
      \Dout_reg[22]\ => reg_b_n_101,
      \Dout_reg[22]_0\ => alu_in_n_32,
      \Dout_reg[22]_1\ => reg_b_n_59,
      \Dout_reg[23]\ => alu_in_n_33,
      \Dout_reg[23]_0\ => reg_b_n_60,
      \Dout_reg[23]_1\ => reg_shamt_n_43,
      \Dout_reg[23]_2\ => reg_b_n_102,
      \Dout_reg[23]_3\ => reg_b_n_113,
      \Dout_reg[24]\ => alu_in_n_34,
      \Dout_reg[24]_0\ => reg_shamt_n_16,
      \Dout_reg[24]_1\ => reg_shamt_n_17,
      \Dout_reg[24]_2\ => reg_b_n_104,
      \Dout_reg[25]\ => alu_in_n_35,
      \Dout_reg[25]_0\ => reg_shamt_n_18,
      \Dout_reg[25]_1\ => reg_shamt_n_19,
      \Dout_reg[25]_2\ => reg_b_n_106,
      \Dout_reg[26]\ => reg_b_n_105,
      \Dout_reg[26]_0\ => alu_in_n_36,
      \Dout_reg[26]_1\ => reg_b_n_63,
      \Dout_reg[27]\ => alu_in_n_37,
      \Dout_reg[27]_0\ => reg_shamt_n_20,
      \Dout_reg[27]_1\ => reg_shamt_n_21,
      \Dout_reg[27]_2\ => reg_b_n_107,
      \Dout_reg[27]_3\ => reg_b_n_110,
      \Dout_reg[28]\ => alu_in_n_38,
      \Dout_reg[28]_0\ => reg_shamt_n_22,
      \Dout_reg[28]_1\ => reg_shamt_n_23,
      \Dout_reg[28]_2\ => reg_b_n_151,
      \Dout_reg[29]\ => alu_in_n_39,
      \Dout_reg[29]_0\ => reg_shamt_n_24,
      \Dout_reg[29]_1\ => reg_shamt_n_25,
      \Dout_reg[29]_2\ => reg_b_n_150,
      \Dout_reg[2]_0\ => reg_alu_control_n_39,
      \Dout_reg[2]_1\ => reg_b_n_83,
      \Dout_reg[2]_2\ => reg_shamt_n_0,
      \Dout_reg[2]_3\ => reg_b_n_87,
      \Dout_reg[2]_4\ => reg_shamt_n_40,
      \Dout_reg[2]_5\(0) => \ADD_SUB/C_Add_in\,
      \Dout_reg[30]\ => alu_in_n_40,
      \Dout_reg[30]_0\ => reg_shamt_n_26,
      \Dout_reg[30]_1\ => reg_shamt_n_27,
      \Dout_reg[30]_2\ => reg_b_n_109,
      \Dout_reg[3]_0\ => reg_alu_control_n_37,
      \Dout_reg[3]_1\ => reg_alu_control_n_38,
      \Dout_reg[3]_10\(3 downto 0) => ALUControl_IBUF(3 downto 0),
      \Dout_reg[3]_2\ => reg_alu_control_n_40,
      \Dout_reg[3]_3\ => reg_b_n_84,
      \Dout_reg[3]_4\ => reg_shamt_n_6,
      \Dout_reg[3]_5\ => reg_b_n_4,
      \Dout_reg[3]_6\ => reg_shamt_n_41,
      \Dout_reg[3]_7\(5) => \ADD_SUB/S_Sub_in\(31),
      \Dout_reg[3]_7\(4 downto 0) => \ADD_SUB/S_Sub_in\(5 downto 1),
      \Dout_reg[3]_8\(5) => \ADD_SUB/S_Add_in\(31),
      \Dout_reg[3]_8\(4 downto 0) => \ADD_SUB/S_Add_in\(5 downto 1),
      \Dout_reg[3]_9\(0) => reg_b_n_0,
      \Dout_reg[4]\ => reg_b_n_85,
      \Dout_reg[4]_0\ => reg_b_n_3,
      \Dout_reg[4]_1\ => reg_shamt_n_42,
      \Dout_reg[5]\ => reg_b_n_86,
      \Dout_reg[5]_0\ => reg_b_n_6,
      \Dout_reg[5]_1\ => reg_b_n_5,
      \Dout_reg[5]_2\(0) => reg_shamt_n_5,
      \Dout_reg[5]_3\ => reg_b_n_7,
      \Dout_reg[5]_4\ => reg_shamt_n_32,
      \Dout_reg[6]\ => reg_b_n_40,
      \Dout_reg[6]_0\ => alu_in_n_16,
      \Dout_reg[6]_1\ => reg_b_n_41,
      \Dout_reg[6]_2\ => reg_b_n_79,
      \Dout_reg[6]_3\ => reg_shamt_n_34,
      \Dout_reg[7]\ => reg_shamt_n_9,
      \Dout_reg[7]_0\ => reg_shamt_n_33,
      \Dout_reg[7]_1\ => alu_in_n_17,
      \Dout_reg[8]\ => alu_in_n_18,
      \Dout_reg[8]_0\ => reg_b_n_45,
      \Dout_reg[8]_1\ => reg_b_n_46,
      \Dout_reg[8]_2\ => reg_shamt_n_35,
      \Dout_reg[8]_3\ => reg_b_n_74,
      \Dout_reg[9]\ => reg_b_n_44,
      \Dout_reg[9]_0\ => alu_in_n_19,
      \Dout_reg[9]_1\ => reg_b_n_43,
      E(0) => WE_IBUF,
      O(0) => \ADD_SUB/C_Sub_in\,
      Q(1 downto 0) => ALUControl_in(1 downto 0),
      SR(0) => RESET_IBUF
    );
reg_b: entity work.\REGrwe_n__parameterized1_0\
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      CO(0) => alu_in_n_6,
      D(31 downto 0) => SRC_B_IBUF(31 downto 0),
      \Dout[16]_i_9_0\ => reg_shamt_n_8,
      \Dout[16]_i_9_1\ => reg_shamt_n_31,
      \Dout[1]_i_14\ => reg_shamt_n_35,
      \Dout[1]_i_16\ => reg_shamt_n_34,
      \Dout[1]_i_5\ => reg_alu_control_n_40,
      \Dout[1]_i_5_0\ => reg_alu_control_n_37,
      \Dout[1]_i_5_1\ => reg_alu_control_n_39,
      \Dout[25]_i_6\ => reg_shamt_n_37,
      \Dout_reg[0]_0\ => reg_b_n_3,
      \Dout_reg[0]_1\ => reg_b_n_6,
      \Dout_reg[0]_10\ => reg_b_n_48,
      \Dout_reg[0]_11\ => reg_b_n_49,
      \Dout_reg[0]_12\ => reg_b_n_50,
      \Dout_reg[0]_13\ => reg_b_n_51,
      \Dout_reg[0]_14\ => reg_b_n_52,
      \Dout_reg[0]_15\ => reg_b_n_53,
      \Dout_reg[0]_16\ => reg_b_n_54,
      \Dout_reg[0]_17\ => reg_b_n_55,
      \Dout_reg[0]_18\ => reg_b_n_56,
      \Dout_reg[0]_19\ => reg_b_n_57,
      \Dout_reg[0]_2\ => reg_b_n_40,
      \Dout_reg[0]_20\ => reg_b_n_58,
      \Dout_reg[0]_21\ => reg_b_n_59,
      \Dout_reg[0]_22\ => reg_b_n_60,
      \Dout_reg[0]_23\ => reg_b_n_63,
      \Dout_reg[0]_24\ => reg_b_n_75,
      \Dout_reg[0]_25\ => reg_b_n_76,
      \Dout_reg[0]_26\ => reg_b_n_77,
      \Dout_reg[0]_27\ => reg_b_n_78,
      \Dout_reg[0]_28\ => reg_b_n_80,
      \Dout_reg[0]_29\ => reg_b_n_81,
      \Dout_reg[0]_3\ => reg_b_n_41,
      \Dout_reg[0]_30\ => reg_b_n_83,
      \Dout_reg[0]_31\ => reg_b_n_84,
      \Dout_reg[0]_32\ => reg_b_n_85,
      \Dout_reg[0]_33\ => reg_b_n_86,
      \Dout_reg[0]_34\ => reg_b_n_88,
      \Dout_reg[0]_35\ => reg_b_n_91,
      \Dout_reg[0]_36\ => reg_b_n_92,
      \Dout_reg[0]_37\ => reg_b_n_93,
      \Dout_reg[0]_38\ => reg_b_n_94,
      \Dout_reg[0]_39\ => reg_b_n_95,
      \Dout_reg[0]_4\ => reg_b_n_42,
      \Dout_reg[0]_40\ => reg_b_n_96,
      \Dout_reg[0]_41\ => reg_b_n_97,
      \Dout_reg[0]_42\ => reg_b_n_98,
      \Dout_reg[0]_43\ => reg_b_n_99,
      \Dout_reg[0]_44\ => reg_b_n_100,
      \Dout_reg[0]_45\ => reg_b_n_101,
      \Dout_reg[0]_46\ => reg_b_n_105,
      \Dout_reg[0]_47\(1 downto 0) => ALUControl_in(1 downto 0),
      \Dout_reg[0]_48\ => reg_shamt_n_44,
      \Dout_reg[0]_5\ => reg_b_n_43,
      \Dout_reg[0]_6\ => reg_b_n_44,
      \Dout_reg[0]_7\ => reg_b_n_45,
      \Dout_reg[0]_8\ => reg_b_n_46,
      \Dout_reg[0]_9\ => reg_b_n_47,
      \Dout_reg[10]_0\ => reg_b_n_62,
      \Dout_reg[11]_0\ => reg_b_n_65,
      \Dout_reg[11]_1\(3) => reg_b_n_125,
      \Dout_reg[11]_1\(2) => reg_b_n_126,
      \Dout_reg[11]_1\(1) => reg_b_n_127,
      \Dout_reg[11]_1\(0) => reg_b_n_128,
      \Dout_reg[12]_0\ => reg_b_n_64,
      \Dout_reg[13]_0\ => reg_b_n_67,
      \Dout_reg[14]_0\ => reg_b_n_69,
      \Dout_reg[15]_0\ => reg_b_n_70,
      \Dout_reg[15]_1\(3) => reg_b_n_129,
      \Dout_reg[15]_1\(2) => reg_b_n_130,
      \Dout_reg[15]_1\(1) => reg_b_n_131,
      \Dout_reg[15]_1\(0) => reg_b_n_132,
      \Dout_reg[19]_0\(3) => reg_b_n_133,
      \Dout_reg[19]_0\(2) => reg_b_n_134,
      \Dout_reg[19]_0\(1) => reg_b_n_135,
      \Dout_reg[19]_0\(0) => reg_b_n_136,
      \Dout_reg[1]_0\ => reg_b_n_4,
      \Dout_reg[1]_1\ => reg_b_n_5,
      \Dout_reg[1]_10\ => reg_b_n_107,
      \Dout_reg[1]_11\ => reg_b_n_110,
      \Dout_reg[1]_12\ => reg_b_n_113,
      \Dout_reg[1]_13\ => reg_b_n_114,
      \Dout_reg[1]_2\ => reg_b_n_7,
      \Dout_reg[1]_3\ => reg_b_n_82,
      \Dout_reg[1]_4\ => reg_b_n_87,
      \Dout_reg[1]_5\ => reg_b_n_89,
      \Dout_reg[1]_6\ => reg_b_n_90,
      \Dout_reg[1]_7\ => reg_b_n_102,
      \Dout_reg[1]_8\ => reg_b_n_104,
      \Dout_reg[1]_9\ => reg_b_n_106,
      \Dout_reg[23]_0\(3) => reg_b_n_137,
      \Dout_reg[23]_0\(2) => reg_b_n_138,
      \Dout_reg[23]_0\(1) => reg_b_n_139,
      \Dout_reg[23]_0\(0) => reg_b_n_140,
      \Dout_reg[23]_1\ => reg_shamt_n_43,
      \Dout_reg[25]_0\ => reg_b_n_66,
      \Dout_reg[26]_0\ => reg_shamt_n_45,
      \Dout_reg[27]_0\ => reg_b_n_68,
      \Dout_reg[27]_1\(3) => reg_b_n_141,
      \Dout_reg[27]_1\(2) => reg_b_n_142,
      \Dout_reg[27]_1\(1) => reg_b_n_143,
      \Dout_reg[27]_1\(0) => reg_b_n_144,
      \Dout_reg[28]_0\ => reg_b_n_112,
      \Dout_reg[29]_0\ => reg_b_n_71,
      \Dout_reg[29]_1\ => reg_b_n_103,
      \Dout_reg[29]_2\ => reg_b_n_111,
      \Dout_reg[29]_3\ => reg_b_n_116,
      \Dout_reg[2]_0\ => reg_b_n_74,
      \Dout_reg[2]_1\ => reg_b_n_109,
      \Dout_reg[2]_2\ => reg_b_n_149,
      \Dout_reg[2]_3\ => reg_b_n_150,
      \Dout_reg[2]_4\ => reg_b_n_151,
      \Dout_reg[2]_5\(0) => reg_a_n_0,
      \Dout_reg[2]_i_2_0\(31 downto 0) => SRC_A_in(31 downto 0),
      \Dout_reg[30]_0\ => reg_b_n_72,
      \Dout_reg[30]_1\ => reg_b_n_115,
      \Dout_reg[31]_0\(0) => reg_b_n_0,
      \Dout_reg[31]_1\(0) => \ADD_SUB/C_Add_in\,
      \Dout_reg[31]_2\(31 downto 0) => SRC_B_in(31 downto 0),
      \Dout_reg[31]_3\ => reg_b_n_73,
      \Dout_reg[31]_4\ => reg_b_n_108,
      \Dout_reg[31]_5\(3) => reg_b_n_145,
      \Dout_reg[31]_5\(2) => reg_b_n_146,
      \Dout_reg[31]_5\(1) => reg_b_n_147,
      \Dout_reg[31]_5\(0) => reg_b_n_148,
      \Dout_reg[3]_0\ => reg_b_n_79,
      \Dout_reg[4]_0\ => reg_shamt_n_7,
      \Dout_reg[6]_0\ => reg_shamt_n_11,
      \Dout_reg[7]_0\(3) => reg_b_n_121,
      \Dout_reg[7]_0\(2) => reg_b_n_122,
      \Dout_reg[7]_0\(1) => reg_b_n_123,
      \Dout_reg[7]_0\(0) => reg_b_n_124,
      \Dout_reg[8]_0\ => reg_shamt_n_10,
      \Dout_reg[9]_0\ => reg_b_n_61,
      \Dout_reg[9]_1\ => reg_alu_control_n_38,
      E(0) => WE_IBUF,
      O(0) => \ADD_SUB/C_Sub_in\,
      Q(4) => reg_shamt_n_1,
      Q(3) => reg_shamt_n_2,
      Q(2) => reg_shamt_n_3,
      Q(1) => reg_shamt_n_4,
      Q(0) => reg_shamt_n_5,
      ROTATE_RIGHT1(3 downto 0) => ROTATE_RIGHT1(4 downto 1),
      S(3) => reg_b_n_117,
      S(2) => reg_b_n_118,
      S(1) => reg_b_n_119,
      S(0) => reg_b_n_120,
      SR(0) => RESET_IBUF
    );
reg_flags: entity work.REGrwe_n_1
     port map (
      ALUResult_OBUF(0) => ALUResult_OBUF(31),
      ALUResult_in(0) => ALUResult_in(31),
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(2 downto 1) => Flags_in(3 downto 2),
      D(0) => Z,
      \Dout_reg[0]_0\ => reg_flags_n_0,
      Q(2 downto 0) => Flags_OBUF(3 downto 1),
      SR(0) => RESET_IBUF
    );
reg_result: entity work.\REGrwe_n__parameterized1_2\
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(30 downto 0) => ALUResult_in(30 downto 0),
      Q(30 downto 0) => ALUResult_OBUF(30 downto 0),
      SR(0) => RESET_IBUF
    );
reg_shamt: entity work.\REGrwe_n__parameterized3\
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      D(4 downto 0) => SHAMT5_IBUF(4 downto 0),
      \Dout[0]_i_4__0\ => reg_b_n_149,
      \Dout[0]_i_5_0\ => reg_b_n_68,
      \Dout[0]_i_5_1\ => reg_b_n_73,
      \Dout[0]_i_8_0\ => reg_b_n_66,
      \Dout[0]_i_8_1\ => reg_b_n_71,
      \Dout[23]_i_2\ => reg_b_n_103,
      \Dout[24]_i_5_0\ => reg_b_n_116,
      \Dout[24]_i_5_1\ => reg_b_n_114,
      \Dout[25]_i_2\ => reg_b_n_112,
      \Dout[25]_i_8_0\ => reg_b_n_115,
      \Dout[27]_i_2\ => reg_b_n_111,
      \Dout[28]_i_8_0\(15 downto 8) => SRC_B_in(31 downto 24),
      \Dout[28]_i_8_0\(7 downto 0) => SRC_B_in(7 downto 0),
      \Dout_reg[0]_0\ => reg_shamt_n_0,
      \Dout_reg[0]_1\ => reg_shamt_n_6,
      \Dout_reg[0]_10\ => reg_shamt_n_22,
      \Dout_reg[0]_11\ => reg_shamt_n_23,
      \Dout_reg[0]_12\ => reg_shamt_n_24,
      \Dout_reg[0]_13\ => reg_shamt_n_26,
      \Dout_reg[0]_14\ => reg_shamt_n_28,
      \Dout_reg[0]_15\ => reg_shamt_n_29,
      \Dout_reg[0]_16\ => reg_shamt_n_30,
      \Dout_reg[0]_17\ => reg_shamt_n_32,
      \Dout_reg[0]_18\ => reg_shamt_n_33,
      \Dout_reg[0]_19\ => reg_shamt_n_34,
      \Dout_reg[0]_2\ => reg_shamt_n_9,
      \Dout_reg[0]_20\ => reg_shamt_n_36,
      \Dout_reg[0]_21\ => reg_shamt_n_38,
      \Dout_reg[0]_22\ => reg_shamt_n_39,
      \Dout_reg[0]_23\ => reg_shamt_n_41,
      \Dout_reg[0]_24\ => reg_shamt_n_42,
      \Dout_reg[0]_25\ => reg_shamt_n_43,
      \Dout_reg[0]_26\(1 downto 0) => ALUControl_in(1 downto 0),
      \Dout_reg[0]_27\ => reg_b_n_89,
      \Dout_reg[0]_28\ => reg_alu_control_n_41,
      \Dout_reg[0]_3\ => reg_shamt_n_11,
      \Dout_reg[0]_4\ => reg_shamt_n_16,
      \Dout_reg[0]_5\ => reg_shamt_n_17,
      \Dout_reg[0]_6\ => reg_shamt_n_18,
      \Dout_reg[0]_7\ => reg_shamt_n_19,
      \Dout_reg[0]_8\ => reg_shamt_n_20,
      \Dout_reg[0]_9\ => reg_shamt_n_21,
      \Dout_reg[1]_0\ => reg_shamt_n_7,
      \Dout_reg[1]_1\ => reg_shamt_n_10,
      \Dout_reg[1]_2\ => reg_shamt_n_25,
      \Dout_reg[1]_3\ => reg_shamt_n_35,
      \Dout_reg[1]_4\ => reg_shamt_n_40,
      \Dout_reg[1]_5\ => reg_shamt_n_45,
      \Dout_reg[1]_6\ => reg_b_n_82,
      \Dout_reg[24]\ => reg_b_n_62,
      \Dout_reg[24]_0\ => reg_b_n_61,
      \Dout_reg[25]\ => reg_b_n_65,
      \Dout_reg[27]\ => reg_b_n_67,
      \Dout_reg[27]_0\ => reg_b_n_64,
      \Dout_reg[28]\ => reg_b_n_69,
      \Dout_reg[29]\ => reg_b_n_70,
      \Dout_reg[2]_0\ => reg_b_n_90,
      \Dout_reg[2]_1\ => reg_b_n_87,
      \Dout_reg[30]\ => reg_b_n_72,
      \Dout_reg[31]\ => reg_shamt_n_27,
      \Dout_reg[31]_0\ => reg_shamt_n_44,
      \Dout_reg[3]_0\ => reg_b_n_4,
      \Dout_reg[4]_0\ => reg_shamt_n_8,
      \Dout_reg[4]_1\ => reg_shamt_n_31,
      \Dout_reg[4]_2\ => reg_shamt_n_37,
      \Dout_reg[7]\ => reg_b_n_92,
      \Dout_reg[7]_0\ => reg_b_n_93,
      \Dout_reg[7]_1\ => reg_alu_control_n_38,
      E(0) => WE_IBUF,
      Q(4) => reg_shamt_n_1,
      Q(3) => reg_shamt_n_2,
      Q(2) => reg_shamt_n_3,
      Q(1) => reg_shamt_n_4,
      Q(0) => reg_shamt_n_5,
      ROTATE_RIGHT1(3 downto 0) => ROTATE_RIGHT1(4 downto 1),
      SR(0) => RESET_IBUF
    );
end STRUCTURE;
