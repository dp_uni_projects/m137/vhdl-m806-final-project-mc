-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Wed Sep 16 19:08:46 2020
-- Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/mpliax/Documents/workspace/vhdl-m806-final-project-sc/vhdl-m806-final-project.sim/sim_1/synth/func/xsim/ADDER_REG_8_func_synth.vhd
-- Design      : ADDER_REG_8
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ADDER_n is
  port (
    D : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \Dout_reg[6]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[6]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 6 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end ADDER_n;

architecture STRUCTURE of ADDER_n is
  signal \^d\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \S0_carry__0_n_1\ : STD_LOGIC;
  signal \S0_carry__0_n_2\ : STD_LOGIC;
  signal \S0_carry__0_n_3\ : STD_LOGIC;
  signal S0_carry_n_0 : STD_LOGIC;
  signal S0_carry_n_1 : STD_LOGIC;
  signal S0_carry_n_2 : STD_LOGIC;
  signal S0_carry_n_3 : STD_LOGIC;
begin
  D(7 downto 0) <= \^d\(7 downto 0);
\Dout[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^d\(7),
      I1 => CO(0),
      O => \Dout_reg[6]_0\(0)
    );
S0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => S0_carry_n_0,
      CO(2) => S0_carry_n_1,
      CO(1) => S0_carry_n_2,
      CO(0) => S0_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => Q(3 downto 0),
      O(3 downto 0) => \^d\(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\S0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => S0_carry_n_0,
      CO(3) => \Dout_reg[6]\(0),
      CO(2) => \S0_carry__0_n_1\,
      CO(1) => \S0_carry__0_n_2\,
      CO(0) => \S0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => DI(0),
      DI(2 downto 0) => Q(6 downto 4),
      O(3 downto 0) => \^d\(7 downto 4),
      S(3 downto 0) => \Dout_reg[7]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n is
  port (
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \Dout_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[1]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[7]_1\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 7 downto 0 );
    CLK : in STD_LOGIC
  );
end REGrwe_n;

architecture STRUCTURE of REGrwe_n is
  signal \^q\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_Dout_reg[1]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_Dout_reg[1]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  Q(7 downto 0) <= \^q\(7 downto 0);
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(0),
      Q => \^q\(0),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(1),
      Q => \^q\(1),
      R => SR(0)
    );
\Dout_reg[1]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \Dout_reg[1]_0\(0),
      CO(3 downto 1) => \NLW_Dout_reg[1]_i_2_CO_UNCONNECTED\(3 downto 1),
      CO(0) => CO(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_Dout_reg[1]_i_2_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(2),
      Q => \^q\(2),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(3),
      Q => \^q\(3),
      R => SR(0)
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(4),
      Q => \^q\(4),
      R => SR(0)
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(5),
      Q => \^q\(5),
      R => SR(0)
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(6),
      Q => \^q\(6),
      R => SR(0)
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(7),
      Q => \^q\(7),
      R => SR(0)
    );
\S0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(7),
      O => DI(0)
    );
\S0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(7),
      I1 => \Dout_reg[7]_1\(7),
      O => \Dout_reg[7]_0\(3)
    );
\S0_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(6),
      I1 => \Dout_reg[7]_1\(6),
      O => \Dout_reg[7]_0\(2)
    );
\S0_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(5),
      I1 => \Dout_reg[7]_1\(5),
      O => \Dout_reg[7]_0\(1)
    );
\S0_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(4),
      I1 => \Dout_reg[7]_1\(4),
      O => \Dout_reg[7]_0\(0)
    );
S0_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(3),
      I1 => \Dout_reg[7]_1\(3),
      O => S(3)
    );
S0_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(2),
      I1 => \Dout_reg[7]_1\(2),
      O => S(2)
    );
S0_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(1),
      I1 => \Dout_reg[7]_1\(1),
      O => S(1)
    );
S0_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \Dout_reg[7]_1\(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_0 is
  port (
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \Dout_reg[7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_0 : entity is "REGrwe_n";
end REGrwe_n_0;

architecture STRUCTURE of REGrwe_n_0 is
  signal \Dout[0]_i_2_n_0\ : STD_LOGIC;
  signal \Dout_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_Dout_reg[0]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_Dout_reg[0]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  Q(7 downto 0) <= \^q\(7 downto 0);
\Dout[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(7),
      I1 => \Dout_reg[0]_0\(0),
      O => \Dout[0]_i_2_n_0\
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \Dout_reg[7]_0\(0),
      Q => \^q\(0),
      R => SR(0)
    );
\Dout_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 1) => \NLW_Dout_reg[0]_i_1_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \Dout_reg[0]_i_1_n_3\,
      CYINIT => CO(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_Dout_reg[0]_i_1_O_UNCONNECTED\(3 downto 2),
      O(1) => D(0),
      O(0) => \NLW_Dout_reg[0]_i_1_O_UNCONNECTED\(0),
      S(3 downto 2) => B"00",
      S(1) => \Dout[0]_i_2_n_0\,
      S(0) => '1'
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \Dout_reg[7]_0\(1),
      Q => \^q\(1),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \Dout_reg[7]_0\(2),
      Q => \^q\(2),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \Dout_reg[7]_0\(3),
      Q => \^q\(3),
      R => SR(0)
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \Dout_reg[7]_0\(4),
      Q => \^q\(4),
      R => SR(0)
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \Dout_reg[7]_0\(5),
      Q => \^q\(5),
      R => SR(0)
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \Dout_reg[7]_0\(6),
      Q => \^q\(6),
      R => SR(0)
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => \Dout_reg[7]_0\(7),
      Q => \^q\(7),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_1 is
  port (
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 7 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_1 : entity is "REGrwe_n";
end REGrwe_n_1;

architecture STRUCTURE of REGrwe_n_1 is
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => Q(0),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => Q(1),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => Q(2),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => Q(3),
      R => SR(0)
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(4),
      Q => Q(4),
      R => SR(0)
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(5),
      Q => Q(5),
      R => SR(0)
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(6),
      Q => Q(6),
      R => SR(0)
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(7),
      Q => Q(7),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \REGrwe_n__parameterized2\ is
  port (
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \REGrwe_n__parameterized2\ : entity is "REGrwe_n";
end \REGrwe_n__parameterized2\;

architecture STRUCTURE of \REGrwe_n__parameterized2\ is
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => Q(0),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => Q(1),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ADDER_REG_8 is
  port (
    CLK : in STD_LOGIC;
    RESET : in STD_LOGIC;
    WE : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 7 downto 0 );
    B : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Cout : out STD_LOGIC;
    OV : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of ADDER_REG_8 : entity is true;
end ADDER_REG_8;

architecture STRUCTURE of ADDER_REG_8 is
  signal A_IBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal B_IBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal CLK_IBUF : STD_LOGIC;
  signal CLK_IBUF_BUFG : STD_LOGIC;
  signal Cout_OBUF : STD_LOGIC;
  signal OV_OBUF : STD_LOGIC;
  signal RESET_IBUF : STD_LOGIC;
  signal S_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal S_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal U1_n_0 : STD_LOGIC;
  signal U1_n_1 : STD_LOGIC;
  signal U1_n_10 : STD_LOGIC;
  signal U1_n_11 : STD_LOGIC;
  signal U1_n_12 : STD_LOGIC;
  signal U1_n_13 : STD_LOGIC;
  signal U1_n_14 : STD_LOGIC;
  signal U1_n_15 : STD_LOGIC;
  signal U1_n_16 : STD_LOGIC;
  signal U1_n_17 : STD_LOGIC;
  signal U1_n_2 : STD_LOGIC;
  signal U1_n_3 : STD_LOGIC;
  signal U1_n_4 : STD_LOGIC;
  signal U1_n_5 : STD_LOGIC;
  signal U1_n_6 : STD_LOGIC;
  signal U1_n_7 : STD_LOGIC;
  signal U1_n_8 : STD_LOGIC;
  signal U1_n_9 : STD_LOGIC;
  signal U2_n_0 : STD_LOGIC;
  signal U2_n_1 : STD_LOGIC;
  signal U2_n_2 : STD_LOGIC;
  signal U2_n_3 : STD_LOGIC;
  signal U2_n_4 : STD_LOGIC;
  signal U2_n_5 : STD_LOGIC;
  signal U2_n_6 : STD_LOGIC;
  signal U2_n_7 : STD_LOGIC;
  signal U2_n_8 : STD_LOGIC;
  signal U3_n_8 : STD_LOGIC;
  signal U3_n_9 : STD_LOGIC;
  signal WE_IBUF : STD_LOGIC;
begin
\A_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(0),
      O => A_IBUF(0)
    );
\A_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(1),
      O => A_IBUF(1)
    );
\A_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(2),
      O => A_IBUF(2)
    );
\A_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(3),
      O => A_IBUF(3)
    );
\A_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(4),
      O => A_IBUF(4)
    );
\A_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(5),
      O => A_IBUF(5)
    );
\A_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(6),
      O => A_IBUF(6)
    );
\A_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => A(7),
      O => A_IBUF(7)
    );
\B_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(0),
      O => B_IBUF(0)
    );
\B_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(1),
      O => B_IBUF(1)
    );
\B_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(2),
      O => B_IBUF(2)
    );
\B_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(3),
      O => B_IBUF(3)
    );
\B_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(4),
      O => B_IBUF(4)
    );
\B_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(5),
      O => B_IBUF(5)
    );
\B_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(6),
      O => B_IBUF(6)
    );
\B_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => B(7),
      O => B_IBUF(7)
    );
CLK_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => CLK_IBUF,
      O => CLK_IBUF_BUFG
    );
CLK_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => CLK,
      O => CLK_IBUF
    );
Cout_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => Cout_OBUF,
      O => Cout
    );
OV_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => OV_OBUF,
      O => OV
    );
RESET_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RESET,
      O => RESET_IBUF
    );
\S_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => S_OBUF(0),
      O => S(0)
    );
\S_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => S_OBUF(1),
      O => S(1)
    );
\S_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => S_OBUF(2),
      O => S(2)
    );
\S_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => S_OBUF(3),
      O => S(3)
    );
\S_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => S_OBUF(4),
      O => S(4)
    );
\S_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => S_OBUF(5),
      O => S(5)
    );
\S_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => S_OBUF(6),
      O => S(6)
    );
\S_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => S_OBUF(7),
      O => S(7)
    );
U1: entity work.REGrwe_n
     port map (
      CLK => CLK_IBUF_BUFG,
      CO(0) => U1_n_0,
      D(7 downto 0) => A_IBUF(7 downto 0),
      DI(0) => U1_n_17,
      \Dout_reg[1]_0\(0) => U3_n_8,
      \Dout_reg[7]_0\(3) => U1_n_13,
      \Dout_reg[7]_0\(2) => U1_n_14,
      \Dout_reg[7]_0\(1) => U1_n_15,
      \Dout_reg[7]_0\(0) => U1_n_16,
      \Dout_reg[7]_1\(7) => U2_n_1,
      \Dout_reg[7]_1\(6) => U2_n_2,
      \Dout_reg[7]_1\(5) => U2_n_3,
      \Dout_reg[7]_1\(4) => U2_n_4,
      \Dout_reg[7]_1\(3) => U2_n_5,
      \Dout_reg[7]_1\(2) => U2_n_6,
      \Dout_reg[7]_1\(1) => U2_n_7,
      \Dout_reg[7]_1\(0) => U2_n_8,
      E(0) => WE_IBUF,
      Q(7) => U1_n_5,
      Q(6) => U1_n_6,
      Q(5) => U1_n_7,
      Q(4) => U1_n_8,
      Q(3) => U1_n_9,
      Q(2) => U1_n_10,
      Q(1) => U1_n_11,
      Q(0) => U1_n_12,
      S(3) => U1_n_1,
      S(2) => U1_n_2,
      S(1) => U1_n_3,
      S(0) => U1_n_4,
      SR(0) => RESET_IBUF
    );
U2: entity work.REGrwe_n_0
     port map (
      CLK => CLK_IBUF_BUFG,
      CO(0) => U1_n_0,
      D(0) => U2_n_0,
      \Dout_reg[0]_0\(0) => U1_n_5,
      \Dout_reg[7]_0\(7 downto 0) => B_IBUF(7 downto 0),
      E(0) => WE_IBUF,
      Q(7) => U2_n_1,
      Q(6) => U2_n_2,
      Q(5) => U2_n_3,
      Q(4) => U2_n_4,
      Q(3) => U2_n_5,
      Q(2) => U2_n_6,
      Q(1) => U2_n_7,
      Q(0) => U2_n_8,
      SR(0) => RESET_IBUF
    );
U3: entity work.ADDER_n
     port map (
      CO(0) => U1_n_0,
      D(7 downto 0) => S_in(7 downto 0),
      DI(0) => U1_n_17,
      \Dout_reg[6]\(0) => U3_n_8,
      \Dout_reg[6]_0\(0) => U3_n_9,
      \Dout_reg[7]\(3) => U1_n_13,
      \Dout_reg[7]\(2) => U1_n_14,
      \Dout_reg[7]\(1) => U1_n_15,
      \Dout_reg[7]\(0) => U1_n_16,
      Q(6) => U1_n_6,
      Q(5) => U1_n_7,
      Q(4) => U1_n_8,
      Q(3) => U1_n_9,
      Q(2) => U1_n_10,
      Q(1) => U1_n_11,
      Q(0) => U1_n_12,
      S(3) => U1_n_1,
      S(2) => U1_n_2,
      S(1) => U1_n_3,
      S(0) => U1_n_4
    );
U4: entity work.REGrwe_n_1
     port map (
      CLK => CLK_IBUF_BUFG,
      D(7 downto 0) => S_in(7 downto 0),
      Q(7 downto 0) => S_OBUF(7 downto 0),
      SR(0) => RESET_IBUF
    );
U5: entity work.\REGrwe_n__parameterized2\
     port map (
      CLK => CLK_IBUF_BUFG,
      D(1) => U3_n_9,
      D(0) => U2_n_0,
      Q(1) => OV_OBUF,
      Q(0) => Cout_OBUF,
      SR(0) => RESET_IBUF
    );
WE_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => WE,
      O => WE_IBUF
    );
end STRUCTURE;
