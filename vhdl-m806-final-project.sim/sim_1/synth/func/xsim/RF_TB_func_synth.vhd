-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Sun Oct 11 15:22:59 2020
-- Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/mpliax/Documents/workspace/vhdl-m806-final-project-mc/vhdl-m806-final-project.sim/sim_1/synth/func/xsim/RF_TB_func_synth.vhd
-- Design      : RF_REGrwe
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGISTER_FILE_n is
  port (
    \DATA_OUT1__0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \DATA_OUT2__0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK : in STD_LOGIC;
    REG_WRITE_in : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ADDR_R1_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[1]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ADDR_R2_in : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end REGISTER_FILE_n;

architecture STRUCTURE of REGISTER_FILE_n is
  signal NLW_RF_reg_r1_0_15_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_12_17_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_18_23_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_24_29_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_30_31_DOB_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_30_31_DOC_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_30_31_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r1_0_15_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_12_17_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_18_23_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_24_29_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_30_31_DOB_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_30_31_DOC_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_30_31_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_RF_reg_r2_0_15_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_0_5 : label is "";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_0_5 : label is 512;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_0_5 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of RF_reg_r1_0_15_0_5 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of RF_reg_r1_0_15_0_5 : label is 15;
  attribute ram_offset : integer;
  attribute ram_offset of RF_reg_r1_0_15_0_5 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of RF_reg_r1_0_15_0_5 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of RF_reg_r1_0_15_0_5 : label is 5;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_12_17 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_12_17 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_12_17 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r1_0_15_12_17 : label is 0;
  attribute ram_addr_end of RF_reg_r1_0_15_12_17 : label is 15;
  attribute ram_offset of RF_reg_r1_0_15_12_17 : label is 0;
  attribute ram_slice_begin of RF_reg_r1_0_15_12_17 : label is 12;
  attribute ram_slice_end of RF_reg_r1_0_15_12_17 : label is 17;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_18_23 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_18_23 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_18_23 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r1_0_15_18_23 : label is 0;
  attribute ram_addr_end of RF_reg_r1_0_15_18_23 : label is 15;
  attribute ram_offset of RF_reg_r1_0_15_18_23 : label is 0;
  attribute ram_slice_begin of RF_reg_r1_0_15_18_23 : label is 18;
  attribute ram_slice_end of RF_reg_r1_0_15_18_23 : label is 23;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_24_29 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_24_29 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_24_29 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r1_0_15_24_29 : label is 0;
  attribute ram_addr_end of RF_reg_r1_0_15_24_29 : label is 15;
  attribute ram_offset of RF_reg_r1_0_15_24_29 : label is 0;
  attribute ram_slice_begin of RF_reg_r1_0_15_24_29 : label is 24;
  attribute ram_slice_end of RF_reg_r1_0_15_24_29 : label is 29;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_30_31 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_30_31 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_30_31 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r1_0_15_30_31 : label is 0;
  attribute ram_addr_end of RF_reg_r1_0_15_30_31 : label is 15;
  attribute ram_offset of RF_reg_r1_0_15_30_31 : label is 0;
  attribute ram_slice_begin of RF_reg_r1_0_15_30_31 : label is 30;
  attribute ram_slice_end of RF_reg_r1_0_15_30_31 : label is 31;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r1_0_15_6_11 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r1_0_15_6_11 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r1_0_15_6_11 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r1_0_15_6_11 : label is 0;
  attribute ram_addr_end of RF_reg_r1_0_15_6_11 : label is 15;
  attribute ram_offset of RF_reg_r1_0_15_6_11 : label is 0;
  attribute ram_slice_begin of RF_reg_r1_0_15_6_11 : label is 6;
  attribute ram_slice_end of RF_reg_r1_0_15_6_11 : label is 11;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_0_5 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_0_5 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_0_5 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_0_5 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_0_5 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_0_5 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_0_5 : label is 0;
  attribute ram_slice_end of RF_reg_r2_0_15_0_5 : label is 5;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_12_17 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_12_17 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_12_17 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_12_17 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_12_17 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_12_17 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_12_17 : label is 12;
  attribute ram_slice_end of RF_reg_r2_0_15_12_17 : label is 17;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_18_23 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_18_23 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_18_23 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_18_23 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_18_23 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_18_23 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_18_23 : label is 18;
  attribute ram_slice_end of RF_reg_r2_0_15_18_23 : label is 23;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_24_29 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_24_29 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_24_29 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_24_29 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_24_29 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_24_29 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_24_29 : label is 24;
  attribute ram_slice_end of RF_reg_r2_0_15_24_29 : label is 29;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_30_31 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_30_31 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_30_31 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_30_31 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_30_31 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_30_31 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_30_31 : label is 30;
  attribute ram_slice_end of RF_reg_r2_0_15_30_31 : label is 31;
  attribute METHODOLOGY_DRC_VIOS of RF_reg_r2_0_15_6_11 : label is "";
  attribute RTL_RAM_BITS of RF_reg_r2_0_15_6_11 : label is 512;
  attribute RTL_RAM_NAME of RF_reg_r2_0_15_6_11 : label is "rf/REG_FILE_IN/RF";
  attribute ram_addr_begin of RF_reg_r2_0_15_6_11 : label is 0;
  attribute ram_addr_end of RF_reg_r2_0_15_6_11 : label is 15;
  attribute ram_offset of RF_reg_r2_0_15_6_11 : label is 0;
  attribute ram_slice_begin of RF_reg_r2_0_15_6_11 : label is 6;
  attribute ram_slice_end of RF_reg_r2_0_15_6_11 : label is 11;
begin
RF_reg_r1_0_15_0_5: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(1 downto 0),
      DIB(1 downto 0) => Q(3 downto 2),
      DIC(1 downto 0) => Q(5 downto 4),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT1__0\(1 downto 0),
      DOB(1 downto 0) => \DATA_OUT1__0\(3 downto 2),
      DOC(1 downto 0) => \DATA_OUT1__0\(5 downto 4),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r1_0_15_12_17: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(13 downto 12),
      DIB(1 downto 0) => Q(15 downto 14),
      DIC(1 downto 0) => Q(17 downto 16),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT1__0\(13 downto 12),
      DOB(1 downto 0) => \DATA_OUT1__0\(15 downto 14),
      DOC(1 downto 0) => \DATA_OUT1__0\(17 downto 16),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_12_17_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r1_0_15_18_23: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(19 downto 18),
      DIB(1 downto 0) => Q(21 downto 20),
      DIC(1 downto 0) => Q(23 downto 22),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT1__0\(19 downto 18),
      DOB(1 downto 0) => \DATA_OUT1__0\(21 downto 20),
      DOC(1 downto 0) => \DATA_OUT1__0\(23 downto 22),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_18_23_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r1_0_15_24_29: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(25 downto 24),
      DIB(1 downto 0) => Q(27 downto 26),
      DIC(1 downto 0) => Q(29 downto 28),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT1__0\(25 downto 24),
      DOB(1 downto 0) => \DATA_OUT1__0\(27 downto 26),
      DOC(1 downto 0) => \DATA_OUT1__0\(29 downto 28),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_24_29_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r1_0_15_30_31: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(31 downto 30),
      DIB(1 downto 0) => B"00",
      DIC(1 downto 0) => B"00",
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT1__0\(31 downto 30),
      DOB(1 downto 0) => NLW_RF_reg_r1_0_15_30_31_DOB_UNCONNECTED(1 downto 0),
      DOC(1 downto 0) => NLW_RF_reg_r1_0_15_30_31_DOC_UNCONNECTED(1 downto 0),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_30_31_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r1_0_15_6_11: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(7 downto 6),
      DIB(1 downto 0) => Q(9 downto 8),
      DIC(1 downto 0) => Q(11 downto 10),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT1__0\(7 downto 6),
      DOB(1 downto 0) => \DATA_OUT1__0\(9 downto 8),
      DOC(1 downto 0) => \DATA_OUT1__0\(11 downto 10),
      DOD(1 downto 0) => NLW_RF_reg_r1_0_15_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r2_0_15_0_5: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(1 downto 0),
      DIB(1 downto 0) => Q(3 downto 2),
      DIC(1 downto 0) => Q(5 downto 4),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT2__0\(1 downto 0),
      DOB(1 downto 0) => \DATA_OUT2__0\(3 downto 2),
      DOC(1 downto 0) => \DATA_OUT2__0\(5 downto 4),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r2_0_15_12_17: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(13 downto 12),
      DIB(1 downto 0) => Q(15 downto 14),
      DIC(1 downto 0) => Q(17 downto 16),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT2__0\(13 downto 12),
      DOB(1 downto 0) => \DATA_OUT2__0\(15 downto 14),
      DOC(1 downto 0) => \DATA_OUT2__0\(17 downto 16),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_12_17_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r2_0_15_18_23: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(19 downto 18),
      DIB(1 downto 0) => Q(21 downto 20),
      DIC(1 downto 0) => Q(23 downto 22),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT2__0\(19 downto 18),
      DOB(1 downto 0) => \DATA_OUT2__0\(21 downto 20),
      DOC(1 downto 0) => \DATA_OUT2__0\(23 downto 22),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_18_23_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r2_0_15_24_29: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(25 downto 24),
      DIB(1 downto 0) => Q(27 downto 26),
      DIC(1 downto 0) => Q(29 downto 28),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT2__0\(25 downto 24),
      DOB(1 downto 0) => \DATA_OUT2__0\(27 downto 26),
      DOC(1 downto 0) => \DATA_OUT2__0\(29 downto 28),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_24_29_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r2_0_15_30_31: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(31 downto 30),
      DIB(1 downto 0) => B"00",
      DIC(1 downto 0) => B"00",
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT2__0\(31 downto 30),
      DOB(1 downto 0) => NLW_RF_reg_r2_0_15_30_31_DOB_UNCONNECTED(1 downto 0),
      DOC(1 downto 0) => NLW_RF_reg_r2_0_15_30_31_DOC_UNCONNECTED(1 downto 0),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_30_31_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
RF_reg_r2_0_15_6_11: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => ADDR_R2_in(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      DIA(1 downto 0) => Q(7 downto 6),
      DIB(1 downto 0) => Q(9 downto 8),
      DIC(1 downto 0) => Q(11 downto 10),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \DATA_OUT2__0\(7 downto 6),
      DOB(1 downto 0) => \DATA_OUT2__0\(9 downto 8),
      DOC(1 downto 0) => \DATA_OUT2__0\(11 downto 10),
      DOD(1 downto 0) => NLW_RF_reg_r2_0_15_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => CLK,
      WE => REG_WRITE_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n is
  port (
    ADDR_R1_in : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    ADDR_R1_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \DATA_OUT1__0\ : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end REGrwe_n;

architecture STRUCTURE of REGrwe_n is
  signal \^addr_r1_in\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  ADDR_R1_in(3 downto 0) <= \^addr_r1_in\(3 downto 0);
\Dout[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(0),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(0),
      O => D(0)
    );
\Dout[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(10),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(10),
      O => D(10)
    );
\Dout[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(11),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(11),
      O => D(11)
    );
\Dout[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(12),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(12),
      O => D(12)
    );
\Dout[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(13),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(13),
      O => D(13)
    );
\Dout[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(14),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(14),
      O => D(14)
    );
\Dout[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(15),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(15),
      O => D(15)
    );
\Dout[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(16),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(16),
      O => D(16)
    );
\Dout[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(17),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(17),
      O => D(17)
    );
\Dout[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(18),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(18),
      O => D(18)
    );
\Dout[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(19),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(19),
      O => D(19)
    );
\Dout[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(1),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(1),
      O => D(1)
    );
\Dout[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(20),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(20),
      O => D(20)
    );
\Dout[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(21),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(21),
      O => D(21)
    );
\Dout[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(22),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(22),
      O => D(22)
    );
\Dout[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(23),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(23),
      O => D(23)
    );
\Dout[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(24),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(24),
      O => D(24)
    );
\Dout[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(25),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(25),
      O => D(25)
    );
\Dout[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(26),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(26),
      O => D(26)
    );
\Dout[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(27),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(27),
      O => D(27)
    );
\Dout[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(28),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(28),
      O => D(28)
    );
\Dout[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(29),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(29),
      O => D(29)
    );
\Dout[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(2),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(2),
      O => D(2)
    );
\Dout[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(30),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(30),
      O => D(30)
    );
\Dout[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(31),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(31),
      O => D(31)
    );
\Dout[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(3),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(3),
      O => D(3)
    );
\Dout[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(4),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(4),
      O => D(4)
    );
\Dout[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(5),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(5),
      O => D(5)
    );
\Dout[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(6),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(6),
      O => D(6)
    );
\Dout[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(7),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(7),
      O => D(7)
    );
\Dout[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(8),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(8),
      O => D(8)
    );
\Dout[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(9),
      I1 => \^addr_r1_in\(2),
      I2 => \^addr_r1_in\(3),
      I3 => \^addr_r1_in\(0),
      I4 => \^addr_r1_in\(1),
      I5 => \DATA_OUT1__0\(9),
      O => D(9)
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => ADDR_R1_IBUF(0),
      Q => \^addr_r1_in\(0),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => ADDR_R1_IBUF(1),
      Q => \^addr_r1_in\(1),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => ADDR_R1_IBUF(2),
      Q => \^addr_r1_in\(2),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => ADDR_R1_IBUF(3),
      Q => \^addr_r1_in\(3),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_0 is
  port (
    ADDR_R2_in : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    ADDR_R2_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \DATA_OUT2__0\ : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_0 : entity is "REGrwe_n";
end REGrwe_n_0;

architecture STRUCTURE of REGrwe_n_0 is
  signal \^addr_r2_in\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  ADDR_R2_in(3 downto 0) <= \^addr_r2_in\(3 downto 0);
\Dout[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(0),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(0),
      O => D(0)
    );
\Dout[10]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(10),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(10),
      O => D(10)
    );
\Dout[11]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(11),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(11),
      O => D(11)
    );
\Dout[12]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(12),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(12),
      O => D(12)
    );
\Dout[13]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(13),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(13),
      O => D(13)
    );
\Dout[14]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(14),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(14),
      O => D(14)
    );
\Dout[15]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(15),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(15),
      O => D(15)
    );
\Dout[16]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(16),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(16),
      O => D(16)
    );
\Dout[17]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(17),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(17),
      O => D(17)
    );
\Dout[18]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(18),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(18),
      O => D(18)
    );
\Dout[19]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(19),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(19),
      O => D(19)
    );
\Dout[1]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(1),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(1),
      O => D(1)
    );
\Dout[20]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(20),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(20),
      O => D(20)
    );
\Dout[21]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(21),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(21),
      O => D(21)
    );
\Dout[22]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(22),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(22),
      O => D(22)
    );
\Dout[23]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(23),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(23),
      O => D(23)
    );
\Dout[24]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(24),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(24),
      O => D(24)
    );
\Dout[25]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(25),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(25),
      O => D(25)
    );
\Dout[26]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(26),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(26),
      O => D(26)
    );
\Dout[27]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(27),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(27),
      O => D(27)
    );
\Dout[28]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(28),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(28),
      O => D(28)
    );
\Dout[29]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(29),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(29),
      O => D(29)
    );
\Dout[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(2),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(2),
      O => D(2)
    );
\Dout[30]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(30),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(30),
      O => D(30)
    );
\Dout[31]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(31),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(31),
      O => D(31)
    );
\Dout[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(3),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(3),
      O => D(3)
    );
\Dout[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(4),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(4),
      O => D(4)
    );
\Dout[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(5),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(5),
      O => D(5)
    );
\Dout[6]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(6),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(6),
      O => D(6)
    );
\Dout[7]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(7),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(7),
      O => D(7)
    );
\Dout[8]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(8),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(8),
      O => D(8)
    );
\Dout[9]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => Q(9),
      I1 => \^addr_r2_in\(2),
      I2 => \^addr_r2_in\(3),
      I3 => \^addr_r2_in\(0),
      I4 => \^addr_r2_in\(1),
      I5 => \DATA_OUT2__0\(9),
      O => D(9)
    );
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => ADDR_R2_IBUF(0),
      Q => \^addr_r2_in\(0),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => ADDR_R2_IBUF(1),
      Q => \^addr_r2_in\(1),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => ADDR_R2_IBUF(2),
      Q => \^addr_r2_in\(2),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => ADDR_R2_IBUF(3),
      Q => \^addr_r2_in\(3),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGrwe_n_1 is
  port (
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of REGrwe_n_1 : entity is "REGrwe_n";
end REGrwe_n_1;

architecture STRUCTURE of REGrwe_n_1 is
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(0),
      Q => Q(0),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(1),
      Q => Q(1),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(2),
      Q => Q(2),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(3),
      Q => Q(3),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \REGrwe_n__parameterized2\ is
  port (
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \REGrwe_n__parameterized2\ : entity is "REGrwe_n";
end \REGrwe_n__parameterized2\;

architecture STRUCTURE of \REGrwe_n__parameterized2\ is
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(0),
      Q => Q(0),
      R => SR(0)
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(10),
      Q => Q(10),
      R => SR(0)
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(11),
      Q => Q(11),
      R => SR(0)
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(12),
      Q => Q(12),
      R => SR(0)
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(13),
      Q => Q(13),
      R => SR(0)
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(14),
      Q => Q(14),
      R => SR(0)
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(15),
      Q => Q(15),
      R => SR(0)
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(16),
      Q => Q(16),
      R => SR(0)
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(17),
      Q => Q(17),
      R => SR(0)
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(18),
      Q => Q(18),
      R => SR(0)
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(19),
      Q => Q(19),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(1),
      Q => Q(1),
      R => SR(0)
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(20),
      Q => Q(20),
      R => SR(0)
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(21),
      Q => Q(21),
      R => SR(0)
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(22),
      Q => Q(22),
      R => SR(0)
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(23),
      Q => Q(23),
      R => SR(0)
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(24),
      Q => Q(24),
      R => SR(0)
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(25),
      Q => Q(25),
      R => SR(0)
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(26),
      Q => Q(26),
      R => SR(0)
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(27),
      Q => Q(27),
      R => SR(0)
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(28),
      Q => Q(28),
      R => SR(0)
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(29),
      Q => Q(29),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(2),
      Q => Q(2),
      R => SR(0)
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(30),
      Q => Q(30),
      R => SR(0)
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(31),
      Q => Q(31),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(3),
      Q => Q(3),
      R => SR(0)
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(4),
      Q => Q(4),
      R => SR(0)
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(5),
      Q => Q(5),
      R => SR(0)
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(6),
      Q => Q(6),
      R => SR(0)
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(7),
      Q => Q(7),
      R => SR(0)
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(8),
      Q => Q(8),
      R => SR(0)
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(9),
      Q => Q(9),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \REGrwe_n__parameterized2_2\ is
  port (
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \REGrwe_n__parameterized2_2\ : entity is "REGrwe_n";
end \REGrwe_n__parameterized2_2\;

architecture STRUCTURE of \REGrwe_n__parameterized2_2\ is
  attribute IOB : string;
  attribute IOB of \Dout_reg[0]\ : label is "TRUE";
  attribute IOB of \Dout_reg[10]\ : label is "TRUE";
  attribute IOB of \Dout_reg[11]\ : label is "TRUE";
  attribute IOB of \Dout_reg[12]\ : label is "TRUE";
  attribute IOB of \Dout_reg[13]\ : label is "TRUE";
  attribute IOB of \Dout_reg[14]\ : label is "TRUE";
  attribute IOB of \Dout_reg[15]\ : label is "TRUE";
  attribute IOB of \Dout_reg[16]\ : label is "TRUE";
  attribute IOB of \Dout_reg[17]\ : label is "TRUE";
  attribute IOB of \Dout_reg[18]\ : label is "TRUE";
  attribute IOB of \Dout_reg[19]\ : label is "TRUE";
  attribute IOB of \Dout_reg[1]\ : label is "TRUE";
  attribute IOB of \Dout_reg[20]\ : label is "TRUE";
  attribute IOB of \Dout_reg[21]\ : label is "TRUE";
  attribute IOB of \Dout_reg[22]\ : label is "TRUE";
  attribute IOB of \Dout_reg[23]\ : label is "TRUE";
  attribute IOB of \Dout_reg[24]\ : label is "TRUE";
  attribute IOB of \Dout_reg[25]\ : label is "TRUE";
  attribute IOB of \Dout_reg[26]\ : label is "TRUE";
  attribute IOB of \Dout_reg[27]\ : label is "TRUE";
  attribute IOB of \Dout_reg[28]\ : label is "TRUE";
  attribute IOB of \Dout_reg[29]\ : label is "TRUE";
  attribute IOB of \Dout_reg[2]\ : label is "TRUE";
  attribute IOB of \Dout_reg[30]\ : label is "TRUE";
  attribute IOB of \Dout_reg[31]\ : label is "TRUE";
  attribute IOB of \Dout_reg[3]\ : label is "TRUE";
  attribute IOB of \Dout_reg[4]\ : label is "TRUE";
  attribute IOB of \Dout_reg[5]\ : label is "TRUE";
  attribute IOB of \Dout_reg[6]\ : label is "TRUE";
  attribute IOB of \Dout_reg[7]\ : label is "TRUE";
  attribute IOB of \Dout_reg[8]\ : label is "TRUE";
  attribute IOB of \Dout_reg[9]\ : label is "TRUE";
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(0),
      Q => Q(0),
      R => SR(0)
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(10),
      Q => Q(10),
      R => SR(0)
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(11),
      Q => Q(11),
      R => SR(0)
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(12),
      Q => Q(12),
      R => SR(0)
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(13),
      Q => Q(13),
      R => SR(0)
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(14),
      Q => Q(14),
      R => SR(0)
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(15),
      Q => Q(15),
      R => SR(0)
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(16),
      Q => Q(16),
      R => SR(0)
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(17),
      Q => Q(17),
      R => SR(0)
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(18),
      Q => Q(18),
      R => SR(0)
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(19),
      Q => Q(19),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(1),
      Q => Q(1),
      R => SR(0)
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(20),
      Q => Q(20),
      R => SR(0)
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(21),
      Q => Q(21),
      R => SR(0)
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(22),
      Q => Q(22),
      R => SR(0)
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(23),
      Q => Q(23),
      R => SR(0)
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(24),
      Q => Q(24),
      R => SR(0)
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(25),
      Q => Q(25),
      R => SR(0)
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(26),
      Q => Q(26),
      R => SR(0)
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(27),
      Q => Q(27),
      R => SR(0)
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(28),
      Q => Q(28),
      R => SR(0)
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(29),
      Q => Q(29),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(2),
      Q => Q(2),
      R => SR(0)
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(30),
      Q => Q(30),
      R => SR(0)
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(31),
      Q => Q(31),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(3),
      Q => Q(3),
      R => SR(0)
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(4),
      Q => Q(4),
      R => SR(0)
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(5),
      Q => Q(5),
      R => SR(0)
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(6),
      Q => Q(6),
      R => SR(0)
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(7),
      Q => Q(7),
      R => SR(0)
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(8),
      Q => Q(8),
      R => SR(0)
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(9),
      Q => Q(9),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \REGrwe_n__parameterized2_3\ is
  port (
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    WE : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \REGrwe_n__parameterized2_3\ : entity is "REGrwe_n";
end \REGrwe_n__parameterized2_3\;

architecture STRUCTURE of \REGrwe_n__parameterized2_3\ is
  attribute IOB : string;
  attribute IOB of \Dout_reg[0]\ : label is "TRUE";
  attribute IOB of \Dout_reg[10]\ : label is "TRUE";
  attribute IOB of \Dout_reg[11]\ : label is "TRUE";
  attribute IOB of \Dout_reg[12]\ : label is "TRUE";
  attribute IOB of \Dout_reg[13]\ : label is "TRUE";
  attribute IOB of \Dout_reg[14]\ : label is "TRUE";
  attribute IOB of \Dout_reg[15]\ : label is "TRUE";
  attribute IOB of \Dout_reg[16]\ : label is "TRUE";
  attribute IOB of \Dout_reg[17]\ : label is "TRUE";
  attribute IOB of \Dout_reg[18]\ : label is "TRUE";
  attribute IOB of \Dout_reg[19]\ : label is "TRUE";
  attribute IOB of \Dout_reg[1]\ : label is "TRUE";
  attribute IOB of \Dout_reg[20]\ : label is "TRUE";
  attribute IOB of \Dout_reg[21]\ : label is "TRUE";
  attribute IOB of \Dout_reg[22]\ : label is "TRUE";
  attribute IOB of \Dout_reg[23]\ : label is "TRUE";
  attribute IOB of \Dout_reg[24]\ : label is "TRUE";
  attribute IOB of \Dout_reg[25]\ : label is "TRUE";
  attribute IOB of \Dout_reg[26]\ : label is "TRUE";
  attribute IOB of \Dout_reg[27]\ : label is "TRUE";
  attribute IOB of \Dout_reg[28]\ : label is "TRUE";
  attribute IOB of \Dout_reg[29]\ : label is "TRUE";
  attribute IOB of \Dout_reg[2]\ : label is "TRUE";
  attribute IOB of \Dout_reg[30]\ : label is "TRUE";
  attribute IOB of \Dout_reg[31]\ : label is "TRUE";
  attribute IOB of \Dout_reg[3]\ : label is "TRUE";
  attribute IOB of \Dout_reg[4]\ : label is "TRUE";
  attribute IOB of \Dout_reg[5]\ : label is "TRUE";
  attribute IOB of \Dout_reg[6]\ : label is "TRUE";
  attribute IOB of \Dout_reg[7]\ : label is "TRUE";
  attribute IOB of \Dout_reg[8]\ : label is "TRUE";
  attribute IOB of \Dout_reg[9]\ : label is "TRUE";
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(0),
      Q => Q(0),
      R => SR(0)
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(10),
      Q => Q(10),
      R => SR(0)
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(11),
      Q => Q(11),
      R => SR(0)
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(12),
      Q => Q(12),
      R => SR(0)
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(13),
      Q => Q(13),
      R => SR(0)
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(14),
      Q => Q(14),
      R => SR(0)
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(15),
      Q => Q(15),
      R => SR(0)
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(16),
      Q => Q(16),
      R => SR(0)
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(17),
      Q => Q(17),
      R => SR(0)
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(18),
      Q => Q(18),
      R => SR(0)
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(19),
      Q => Q(19),
      R => SR(0)
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(1),
      Q => Q(1),
      R => SR(0)
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(20),
      Q => Q(20),
      R => SR(0)
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(21),
      Q => Q(21),
      R => SR(0)
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(22),
      Q => Q(22),
      R => SR(0)
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(23),
      Q => Q(23),
      R => SR(0)
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(24),
      Q => Q(24),
      R => SR(0)
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(25),
      Q => Q(25),
      R => SR(0)
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(26),
      Q => Q(26),
      R => SR(0)
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(27),
      Q => Q(27),
      R => SR(0)
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(28),
      Q => Q(28),
      R => SR(0)
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(29),
      Q => Q(29),
      R => SR(0)
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(2),
      Q => Q(2),
      R => SR(0)
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(30),
      Q => Q(30),
      R => SR(0)
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(31),
      Q => Q(31),
      R => SR(0)
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(3),
      Q => Q(3),
      R => SR(0)
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(4),
      Q => Q(4),
      R => SR(0)
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(5),
      Q => Q(5),
      R => SR(0)
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(6),
      Q => Q(6),
      R => SR(0)
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(7),
      Q => Q(7),
      R => SR(0)
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(8),
      Q => Q(8),
      R => SR(0)
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(9),
      Q => Q(9),
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \REGrwe_n__parameterized2_4\ is
  port (
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    RESET : in STD_LOGIC;
    WE : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \REGrwe_n__parameterized2_4\ : entity is "REGrwe_n";
end \REGrwe_n__parameterized2_4\;

architecture STRUCTURE of \REGrwe_n__parameterized2_4\ is
begin
\Dout_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(0),
      Q => Q(0),
      R => RESET
    );
\Dout_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(10),
      Q => Q(10),
      R => RESET
    );
\Dout_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(11),
      Q => Q(11),
      R => RESET
    );
\Dout_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(12),
      Q => Q(12),
      R => RESET
    );
\Dout_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(13),
      Q => Q(13),
      R => RESET
    );
\Dout_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(14),
      Q => Q(14),
      R => RESET
    );
\Dout_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(15),
      Q => Q(15),
      R => RESET
    );
\Dout_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(16),
      Q => Q(16),
      R => RESET
    );
\Dout_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(17),
      Q => Q(17),
      R => RESET
    );
\Dout_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(18),
      Q => Q(18),
      R => RESET
    );
\Dout_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(19),
      Q => Q(19),
      R => RESET
    );
\Dout_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(1),
      Q => Q(1),
      R => RESET
    );
\Dout_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(20),
      Q => Q(20),
      R => RESET
    );
\Dout_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(21),
      Q => Q(21),
      R => RESET
    );
\Dout_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(22),
      Q => Q(22),
      R => RESET
    );
\Dout_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(23),
      Q => Q(23),
      R => RESET
    );
\Dout_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(24),
      Q => Q(24),
      R => RESET
    );
\Dout_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(25),
      Q => Q(25),
      R => RESET
    );
\Dout_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(26),
      Q => Q(26),
      R => RESET
    );
\Dout_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(27),
      Q => Q(27),
      R => RESET
    );
\Dout_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(28),
      Q => Q(28),
      R => RESET
    );
\Dout_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(29),
      Q => Q(29),
      R => RESET
    );
\Dout_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(2),
      Q => Q(2),
      R => RESET
    );
\Dout_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(30),
      Q => Q(30),
      R => RESET
    );
\Dout_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(31),
      Q => Q(31),
      R => RESET
    );
\Dout_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(3),
      Q => Q(3),
      R => RESET
    );
\Dout_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(4),
      Q => Q(4),
      R => RESET
    );
\Dout_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(5),
      Q => Q(5),
      R => RESET
    );
\Dout_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(6),
      Q => Q(6),
      R => RESET
    );
\Dout_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(7),
      Q => Q(7),
      R => RESET
    );
\Dout_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(8),
      Q => Q(8),
      R => RESET
    );
\Dout_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => WE,
      D => D(9),
      Q => Q(9),
      R => RESET
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity REGISTER_FILE_wR15 is
  port (
    \DATA_OUT1__0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \DATA_OUT2__0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK : in STD_LOGIC;
    REG_WRITE_in : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ADDR_R1_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \Dout_reg[1]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ADDR_R2_in : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end REGISTER_FILE_wR15;

architecture STRUCTURE of REGISTER_FILE_wR15 is
begin
REG_FILE_IN: entity work.REGISTER_FILE_n
     port map (
      ADDR_R1_in(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDR_R2_in(3 downto 0) => ADDR_R2_in(3 downto 0),
      CLK => CLK,
      \DATA_OUT1__0\(31 downto 0) => \DATA_OUT1__0\(31 downto 0),
      \DATA_OUT2__0\(31 downto 0) => \DATA_OUT2__0\(31 downto 0),
      \Dout_reg[1]\(3 downto 0) => \Dout_reg[1]\(3 downto 0),
      Q(31 downto 0) => Q(31 downto 0),
      REG_WRITE_in => REG_WRITE_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity RF_REGrwe is
  port (
    CLK : in STD_LOGIC;
    WE : in STD_LOGIC;
    REG_WRITE : in STD_LOGIC;
    ADDR_W : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ADDR_R1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ADDR_R2 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DATA_IN : in STD_LOGIC_VECTOR ( 31 downto 0 );
    R15 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    RESET : in STD_LOGIC;
    DATA_OUT1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    DATA_OUT2 : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of RF_REGrwe : entity is true;
end RF_REGrwe;

architecture STRUCTURE of RF_REGrwe is
  signal ADDR_R1_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ADDR_R1_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ADDR_R2_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ADDR_R2_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ADDR_W_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal CLK_IBUF : STD_LOGIC;
  signal CLK_IBUF_BUFG : STD_LOGIC;
  signal DATA_IN_IBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DATA_OUT1_OBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \DATA_OUT1__0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal DATA_OUT2_OBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \DATA_OUT2__0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Din : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dout : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal R15_IBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal REG_WRITE_IBUF : STD_LOGIC;
  signal REG_WRITE_in : STD_LOGIC;
  signal RESET_IBUF : STD_LOGIC;
  signal WE_IBUF : STD_LOGIC;
  signal reg_addr_r2_n_10 : STD_LOGIC;
  signal reg_addr_r2_n_11 : STD_LOGIC;
  signal reg_addr_r2_n_12 : STD_LOGIC;
  signal reg_addr_r2_n_13 : STD_LOGIC;
  signal reg_addr_r2_n_14 : STD_LOGIC;
  signal reg_addr_r2_n_15 : STD_LOGIC;
  signal reg_addr_r2_n_16 : STD_LOGIC;
  signal reg_addr_r2_n_17 : STD_LOGIC;
  signal reg_addr_r2_n_18 : STD_LOGIC;
  signal reg_addr_r2_n_19 : STD_LOGIC;
  signal reg_addr_r2_n_20 : STD_LOGIC;
  signal reg_addr_r2_n_21 : STD_LOGIC;
  signal reg_addr_r2_n_22 : STD_LOGIC;
  signal reg_addr_r2_n_23 : STD_LOGIC;
  signal reg_addr_r2_n_24 : STD_LOGIC;
  signal reg_addr_r2_n_25 : STD_LOGIC;
  signal reg_addr_r2_n_26 : STD_LOGIC;
  signal reg_addr_r2_n_27 : STD_LOGIC;
  signal reg_addr_r2_n_28 : STD_LOGIC;
  signal reg_addr_r2_n_29 : STD_LOGIC;
  signal reg_addr_r2_n_30 : STD_LOGIC;
  signal reg_addr_r2_n_31 : STD_LOGIC;
  signal reg_addr_r2_n_32 : STD_LOGIC;
  signal reg_addr_r2_n_33 : STD_LOGIC;
  signal reg_addr_r2_n_34 : STD_LOGIC;
  signal reg_addr_r2_n_35 : STD_LOGIC;
  signal reg_addr_r2_n_4 : STD_LOGIC;
  signal reg_addr_r2_n_5 : STD_LOGIC;
  signal reg_addr_r2_n_6 : STD_LOGIC;
  signal reg_addr_r2_n_7 : STD_LOGIC;
  signal reg_addr_r2_n_8 : STD_LOGIC;
  signal reg_addr_r2_n_9 : STD_LOGIC;
  signal reg_data_in_n_0 : STD_LOGIC;
  signal reg_data_in_n_1 : STD_LOGIC;
  signal reg_data_in_n_10 : STD_LOGIC;
  signal reg_data_in_n_11 : STD_LOGIC;
  signal reg_data_in_n_12 : STD_LOGIC;
  signal reg_data_in_n_13 : STD_LOGIC;
  signal reg_data_in_n_14 : STD_LOGIC;
  signal reg_data_in_n_15 : STD_LOGIC;
  signal reg_data_in_n_16 : STD_LOGIC;
  signal reg_data_in_n_17 : STD_LOGIC;
  signal reg_data_in_n_18 : STD_LOGIC;
  signal reg_data_in_n_19 : STD_LOGIC;
  signal reg_data_in_n_2 : STD_LOGIC;
  signal reg_data_in_n_20 : STD_LOGIC;
  signal reg_data_in_n_21 : STD_LOGIC;
  signal reg_data_in_n_22 : STD_LOGIC;
  signal reg_data_in_n_23 : STD_LOGIC;
  signal reg_data_in_n_24 : STD_LOGIC;
  signal reg_data_in_n_25 : STD_LOGIC;
  signal reg_data_in_n_26 : STD_LOGIC;
  signal reg_data_in_n_27 : STD_LOGIC;
  signal reg_data_in_n_28 : STD_LOGIC;
  signal reg_data_in_n_29 : STD_LOGIC;
  signal reg_data_in_n_3 : STD_LOGIC;
  signal reg_data_in_n_30 : STD_LOGIC;
  signal reg_data_in_n_31 : STD_LOGIC;
  signal reg_data_in_n_4 : STD_LOGIC;
  signal reg_data_in_n_5 : STD_LOGIC;
  signal reg_data_in_n_6 : STD_LOGIC;
  signal reg_data_in_n_7 : STD_LOGIC;
  signal reg_data_in_n_8 : STD_LOGIC;
  signal reg_data_in_n_9 : STD_LOGIC;
  signal reg_r15_n_0 : STD_LOGIC;
  signal reg_r15_n_1 : STD_LOGIC;
  signal reg_r15_n_10 : STD_LOGIC;
  signal reg_r15_n_11 : STD_LOGIC;
  signal reg_r15_n_12 : STD_LOGIC;
  signal reg_r15_n_13 : STD_LOGIC;
  signal reg_r15_n_14 : STD_LOGIC;
  signal reg_r15_n_15 : STD_LOGIC;
  signal reg_r15_n_16 : STD_LOGIC;
  signal reg_r15_n_17 : STD_LOGIC;
  signal reg_r15_n_18 : STD_LOGIC;
  signal reg_r15_n_19 : STD_LOGIC;
  signal reg_r15_n_2 : STD_LOGIC;
  signal reg_r15_n_20 : STD_LOGIC;
  signal reg_r15_n_21 : STD_LOGIC;
  signal reg_r15_n_22 : STD_LOGIC;
  signal reg_r15_n_23 : STD_LOGIC;
  signal reg_r15_n_24 : STD_LOGIC;
  signal reg_r15_n_25 : STD_LOGIC;
  signal reg_r15_n_26 : STD_LOGIC;
  signal reg_r15_n_27 : STD_LOGIC;
  signal reg_r15_n_28 : STD_LOGIC;
  signal reg_r15_n_29 : STD_LOGIC;
  signal reg_r15_n_3 : STD_LOGIC;
  signal reg_r15_n_30 : STD_LOGIC;
  signal reg_r15_n_31 : STD_LOGIC;
  signal reg_r15_n_4 : STD_LOGIC;
  signal reg_r15_n_5 : STD_LOGIC;
  signal reg_r15_n_6 : STD_LOGIC;
  signal reg_r15_n_7 : STD_LOGIC;
  signal reg_r15_n_8 : STD_LOGIC;
  signal reg_r15_n_9 : STD_LOGIC;
  attribute IOB : string;
  attribute IOB of \DATA_OUT1_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[10]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[11]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[12]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[13]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[14]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[15]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[16]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[17]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[18]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[19]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[20]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[21]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[22]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[23]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[24]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[25]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[26]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[27]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[28]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[29]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[30]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[31]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[3]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[4]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[5]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[6]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[7]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[8]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT1_OBUF[9]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[10]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[11]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[12]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[13]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[14]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[15]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[16]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[17]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[18]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[19]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[20]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[21]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[22]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[23]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[24]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[25]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[26]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[27]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[28]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[29]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[30]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[31]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[3]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[4]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[5]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[6]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[7]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[8]_inst\ : label is "TRUE";
  attribute IOB of \DATA_OUT2_OBUF[9]_inst\ : label is "TRUE";
begin
\ADDR_R1_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_R1(0),
      O => ADDR_R1_IBUF(0)
    );
\ADDR_R1_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_R1(1),
      O => ADDR_R1_IBUF(1)
    );
\ADDR_R1_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_R1(2),
      O => ADDR_R1_IBUF(2)
    );
\ADDR_R1_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_R1(3),
      O => ADDR_R1_IBUF(3)
    );
\ADDR_R2_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_R2(0),
      O => ADDR_R2_IBUF(0)
    );
\ADDR_R2_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_R2(1),
      O => ADDR_R2_IBUF(1)
    );
\ADDR_R2_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_R2(2),
      O => ADDR_R2_IBUF(2)
    );
\ADDR_R2_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_R2(3),
      O => ADDR_R2_IBUF(3)
    );
\ADDR_W_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_W(0),
      O => ADDR_W_IBUF(0)
    );
\ADDR_W_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_W(1),
      O => ADDR_W_IBUF(1)
    );
\ADDR_W_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_W(2),
      O => ADDR_W_IBUF(2)
    );
\ADDR_W_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ADDR_W(3),
      O => ADDR_W_IBUF(3)
    );
CLK_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => CLK_IBUF,
      O => CLK_IBUF_BUFG
    );
CLK_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => CLK,
      O => CLK_IBUF
    );
\DATA_IN_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(0),
      O => DATA_IN_IBUF(0)
    );
\DATA_IN_IBUF[10]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(10),
      O => DATA_IN_IBUF(10)
    );
\DATA_IN_IBUF[11]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(11),
      O => DATA_IN_IBUF(11)
    );
\DATA_IN_IBUF[12]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(12),
      O => DATA_IN_IBUF(12)
    );
\DATA_IN_IBUF[13]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(13),
      O => DATA_IN_IBUF(13)
    );
\DATA_IN_IBUF[14]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(14),
      O => DATA_IN_IBUF(14)
    );
\DATA_IN_IBUF[15]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(15),
      O => DATA_IN_IBUF(15)
    );
\DATA_IN_IBUF[16]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(16),
      O => DATA_IN_IBUF(16)
    );
\DATA_IN_IBUF[17]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(17),
      O => DATA_IN_IBUF(17)
    );
\DATA_IN_IBUF[18]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(18),
      O => DATA_IN_IBUF(18)
    );
\DATA_IN_IBUF[19]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(19),
      O => DATA_IN_IBUF(19)
    );
\DATA_IN_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(1),
      O => DATA_IN_IBUF(1)
    );
\DATA_IN_IBUF[20]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(20),
      O => DATA_IN_IBUF(20)
    );
\DATA_IN_IBUF[21]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(21),
      O => DATA_IN_IBUF(21)
    );
\DATA_IN_IBUF[22]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(22),
      O => DATA_IN_IBUF(22)
    );
\DATA_IN_IBUF[23]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(23),
      O => DATA_IN_IBUF(23)
    );
\DATA_IN_IBUF[24]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(24),
      O => DATA_IN_IBUF(24)
    );
\DATA_IN_IBUF[25]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(25),
      O => DATA_IN_IBUF(25)
    );
\DATA_IN_IBUF[26]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(26),
      O => DATA_IN_IBUF(26)
    );
\DATA_IN_IBUF[27]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(27),
      O => DATA_IN_IBUF(27)
    );
\DATA_IN_IBUF[28]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(28),
      O => DATA_IN_IBUF(28)
    );
\DATA_IN_IBUF[29]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(29),
      O => DATA_IN_IBUF(29)
    );
\DATA_IN_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(2),
      O => DATA_IN_IBUF(2)
    );
\DATA_IN_IBUF[30]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(30),
      O => DATA_IN_IBUF(30)
    );
\DATA_IN_IBUF[31]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(31),
      O => DATA_IN_IBUF(31)
    );
\DATA_IN_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(3),
      O => DATA_IN_IBUF(3)
    );
\DATA_IN_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(4),
      O => DATA_IN_IBUF(4)
    );
\DATA_IN_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(5),
      O => DATA_IN_IBUF(5)
    );
\DATA_IN_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(6),
      O => DATA_IN_IBUF(6)
    );
\DATA_IN_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(7),
      O => DATA_IN_IBUF(7)
    );
\DATA_IN_IBUF[8]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(8),
      O => DATA_IN_IBUF(8)
    );
\DATA_IN_IBUF[9]_inst\: unisim.vcomponents.IBUF
     port map (
      I => DATA_IN(9),
      O => DATA_IN_IBUF(9)
    );
\DATA_OUT1_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(0),
      O => DATA_OUT1(0)
    );
\DATA_OUT1_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(10),
      O => DATA_OUT1(10)
    );
\DATA_OUT1_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(11),
      O => DATA_OUT1(11)
    );
\DATA_OUT1_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(12),
      O => DATA_OUT1(12)
    );
\DATA_OUT1_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(13),
      O => DATA_OUT1(13)
    );
\DATA_OUT1_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(14),
      O => DATA_OUT1(14)
    );
\DATA_OUT1_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(15),
      O => DATA_OUT1(15)
    );
\DATA_OUT1_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(16),
      O => DATA_OUT1(16)
    );
\DATA_OUT1_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(17),
      O => DATA_OUT1(17)
    );
\DATA_OUT1_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(18),
      O => DATA_OUT1(18)
    );
\DATA_OUT1_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(19),
      O => DATA_OUT1(19)
    );
\DATA_OUT1_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(1),
      O => DATA_OUT1(1)
    );
\DATA_OUT1_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(20),
      O => DATA_OUT1(20)
    );
\DATA_OUT1_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(21),
      O => DATA_OUT1(21)
    );
\DATA_OUT1_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(22),
      O => DATA_OUT1(22)
    );
\DATA_OUT1_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(23),
      O => DATA_OUT1(23)
    );
\DATA_OUT1_OBUF[24]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(24),
      O => DATA_OUT1(24)
    );
\DATA_OUT1_OBUF[25]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(25),
      O => DATA_OUT1(25)
    );
\DATA_OUT1_OBUF[26]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(26),
      O => DATA_OUT1(26)
    );
\DATA_OUT1_OBUF[27]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(27),
      O => DATA_OUT1(27)
    );
\DATA_OUT1_OBUF[28]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(28),
      O => DATA_OUT1(28)
    );
\DATA_OUT1_OBUF[29]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(29),
      O => DATA_OUT1(29)
    );
\DATA_OUT1_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(2),
      O => DATA_OUT1(2)
    );
\DATA_OUT1_OBUF[30]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(30),
      O => DATA_OUT1(30)
    );
\DATA_OUT1_OBUF[31]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(31),
      O => DATA_OUT1(31)
    );
\DATA_OUT1_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(3),
      O => DATA_OUT1(3)
    );
\DATA_OUT1_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(4),
      O => DATA_OUT1(4)
    );
\DATA_OUT1_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(5),
      O => DATA_OUT1(5)
    );
\DATA_OUT1_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(6),
      O => DATA_OUT1(6)
    );
\DATA_OUT1_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(7),
      O => DATA_OUT1(7)
    );
\DATA_OUT1_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(8),
      O => DATA_OUT1(8)
    );
\DATA_OUT1_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT1_OBUF(9),
      O => DATA_OUT1(9)
    );
\DATA_OUT2_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(0),
      O => DATA_OUT2(0)
    );
\DATA_OUT2_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(10),
      O => DATA_OUT2(10)
    );
\DATA_OUT2_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(11),
      O => DATA_OUT2(11)
    );
\DATA_OUT2_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(12),
      O => DATA_OUT2(12)
    );
\DATA_OUT2_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(13),
      O => DATA_OUT2(13)
    );
\DATA_OUT2_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(14),
      O => DATA_OUT2(14)
    );
\DATA_OUT2_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(15),
      O => DATA_OUT2(15)
    );
\DATA_OUT2_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(16),
      O => DATA_OUT2(16)
    );
\DATA_OUT2_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(17),
      O => DATA_OUT2(17)
    );
\DATA_OUT2_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(18),
      O => DATA_OUT2(18)
    );
\DATA_OUT2_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(19),
      O => DATA_OUT2(19)
    );
\DATA_OUT2_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(1),
      O => DATA_OUT2(1)
    );
\DATA_OUT2_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(20),
      O => DATA_OUT2(20)
    );
\DATA_OUT2_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(21),
      O => DATA_OUT2(21)
    );
\DATA_OUT2_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(22),
      O => DATA_OUT2(22)
    );
\DATA_OUT2_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(23),
      O => DATA_OUT2(23)
    );
\DATA_OUT2_OBUF[24]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(24),
      O => DATA_OUT2(24)
    );
\DATA_OUT2_OBUF[25]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(25),
      O => DATA_OUT2(25)
    );
\DATA_OUT2_OBUF[26]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(26),
      O => DATA_OUT2(26)
    );
\DATA_OUT2_OBUF[27]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(27),
      O => DATA_OUT2(27)
    );
\DATA_OUT2_OBUF[28]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(28),
      O => DATA_OUT2(28)
    );
\DATA_OUT2_OBUF[29]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(29),
      O => DATA_OUT2(29)
    );
\DATA_OUT2_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(2),
      O => DATA_OUT2(2)
    );
\DATA_OUT2_OBUF[30]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(30),
      O => DATA_OUT2(30)
    );
\DATA_OUT2_OBUF[31]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(31),
      O => DATA_OUT2(31)
    );
\DATA_OUT2_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(3),
      O => DATA_OUT2(3)
    );
\DATA_OUT2_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(4),
      O => DATA_OUT2(4)
    );
\DATA_OUT2_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(5),
      O => DATA_OUT2(5)
    );
\DATA_OUT2_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(6),
      O => DATA_OUT2(6)
    );
\DATA_OUT2_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(7),
      O => DATA_OUT2(7)
    );
\DATA_OUT2_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(8),
      O => DATA_OUT2(8)
    );
\DATA_OUT2_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => DATA_OUT2_OBUF(9),
      O => DATA_OUT2(9)
    );
\R15_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(0),
      O => R15_IBUF(0)
    );
\R15_IBUF[10]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(10),
      O => R15_IBUF(10)
    );
\R15_IBUF[11]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(11),
      O => R15_IBUF(11)
    );
\R15_IBUF[12]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(12),
      O => R15_IBUF(12)
    );
\R15_IBUF[13]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(13),
      O => R15_IBUF(13)
    );
\R15_IBUF[14]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(14),
      O => R15_IBUF(14)
    );
\R15_IBUF[15]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(15),
      O => R15_IBUF(15)
    );
\R15_IBUF[16]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(16),
      O => R15_IBUF(16)
    );
\R15_IBUF[17]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(17),
      O => R15_IBUF(17)
    );
\R15_IBUF[18]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(18),
      O => R15_IBUF(18)
    );
\R15_IBUF[19]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(19),
      O => R15_IBUF(19)
    );
\R15_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(1),
      O => R15_IBUF(1)
    );
\R15_IBUF[20]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(20),
      O => R15_IBUF(20)
    );
\R15_IBUF[21]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(21),
      O => R15_IBUF(21)
    );
\R15_IBUF[22]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(22),
      O => R15_IBUF(22)
    );
\R15_IBUF[23]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(23),
      O => R15_IBUF(23)
    );
\R15_IBUF[24]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(24),
      O => R15_IBUF(24)
    );
\R15_IBUF[25]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(25),
      O => R15_IBUF(25)
    );
\R15_IBUF[26]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(26),
      O => R15_IBUF(26)
    );
\R15_IBUF[27]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(27),
      O => R15_IBUF(27)
    );
\R15_IBUF[28]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(28),
      O => R15_IBUF(28)
    );
\R15_IBUF[29]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(29),
      O => R15_IBUF(29)
    );
\R15_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(2),
      O => R15_IBUF(2)
    );
\R15_IBUF[30]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(30),
      O => R15_IBUF(30)
    );
\R15_IBUF[31]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(31),
      O => R15_IBUF(31)
    );
\R15_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(3),
      O => R15_IBUF(3)
    );
\R15_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(4),
      O => R15_IBUF(4)
    );
\R15_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(5),
      O => R15_IBUF(5)
    );
\R15_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(6),
      O => R15_IBUF(6)
    );
\R15_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(7),
      O => R15_IBUF(7)
    );
\R15_IBUF[8]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(8),
      O => R15_IBUF(8)
    );
\R15_IBUF[9]_inst\: unisim.vcomponents.IBUF
     port map (
      I => R15(9),
      O => R15_IBUF(9)
    );
REG_WRITE_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => REG_WRITE,
      O => REG_WRITE_IBUF
    );
REG_WRITE_in_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => WE_IBUF,
      D => REG_WRITE_IBUF,
      Q => REG_WRITE_in,
      R => RESET_IBUF
    );
RESET_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RESET,
      O => RESET_IBUF
    );
WE_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => WE,
      O => WE_IBUF
    );
reg_addr_r1: entity work.REGrwe_n
     port map (
      ADDR_R1_IBUF(3 downto 0) => ADDR_R1_IBUF(3 downto 0),
      ADDR_R1_in(3 downto 0) => ADDR_R1_in(3 downto 0),
      CLK => CLK_IBUF_BUFG,
      D(31 downto 0) => Din(31 downto 0),
      \DATA_OUT1__0\(31 downto 0) => \DATA_OUT1__0\(31 downto 0),
      E(0) => WE_IBUF,
      Q(31) => reg_r15_n_0,
      Q(30) => reg_r15_n_1,
      Q(29) => reg_r15_n_2,
      Q(28) => reg_r15_n_3,
      Q(27) => reg_r15_n_4,
      Q(26) => reg_r15_n_5,
      Q(25) => reg_r15_n_6,
      Q(24) => reg_r15_n_7,
      Q(23) => reg_r15_n_8,
      Q(22) => reg_r15_n_9,
      Q(21) => reg_r15_n_10,
      Q(20) => reg_r15_n_11,
      Q(19) => reg_r15_n_12,
      Q(18) => reg_r15_n_13,
      Q(17) => reg_r15_n_14,
      Q(16) => reg_r15_n_15,
      Q(15) => reg_r15_n_16,
      Q(14) => reg_r15_n_17,
      Q(13) => reg_r15_n_18,
      Q(12) => reg_r15_n_19,
      Q(11) => reg_r15_n_20,
      Q(10) => reg_r15_n_21,
      Q(9) => reg_r15_n_22,
      Q(8) => reg_r15_n_23,
      Q(7) => reg_r15_n_24,
      Q(6) => reg_r15_n_25,
      Q(5) => reg_r15_n_26,
      Q(4) => reg_r15_n_27,
      Q(3) => reg_r15_n_28,
      Q(2) => reg_r15_n_29,
      Q(1) => reg_r15_n_30,
      Q(0) => reg_r15_n_31,
      SR(0) => RESET_IBUF
    );
reg_addr_r2: entity work.REGrwe_n_0
     port map (
      ADDR_R2_IBUF(3 downto 0) => ADDR_R2_IBUF(3 downto 0),
      ADDR_R2_in(3 downto 0) => ADDR_R2_in(3 downto 0),
      CLK => CLK_IBUF_BUFG,
      D(31) => reg_addr_r2_n_4,
      D(30) => reg_addr_r2_n_5,
      D(29) => reg_addr_r2_n_6,
      D(28) => reg_addr_r2_n_7,
      D(27) => reg_addr_r2_n_8,
      D(26) => reg_addr_r2_n_9,
      D(25) => reg_addr_r2_n_10,
      D(24) => reg_addr_r2_n_11,
      D(23) => reg_addr_r2_n_12,
      D(22) => reg_addr_r2_n_13,
      D(21) => reg_addr_r2_n_14,
      D(20) => reg_addr_r2_n_15,
      D(19) => reg_addr_r2_n_16,
      D(18) => reg_addr_r2_n_17,
      D(17) => reg_addr_r2_n_18,
      D(16) => reg_addr_r2_n_19,
      D(15) => reg_addr_r2_n_20,
      D(14) => reg_addr_r2_n_21,
      D(13) => reg_addr_r2_n_22,
      D(12) => reg_addr_r2_n_23,
      D(11) => reg_addr_r2_n_24,
      D(10) => reg_addr_r2_n_25,
      D(9) => reg_addr_r2_n_26,
      D(8) => reg_addr_r2_n_27,
      D(7) => reg_addr_r2_n_28,
      D(6) => reg_addr_r2_n_29,
      D(5) => reg_addr_r2_n_30,
      D(4) => reg_addr_r2_n_31,
      D(3) => reg_addr_r2_n_32,
      D(2) => reg_addr_r2_n_33,
      D(1) => reg_addr_r2_n_34,
      D(0) => reg_addr_r2_n_35,
      \DATA_OUT2__0\(31 downto 0) => \DATA_OUT2__0\(31 downto 0),
      E(0) => WE_IBUF,
      Q(31) => reg_r15_n_0,
      Q(30) => reg_r15_n_1,
      Q(29) => reg_r15_n_2,
      Q(28) => reg_r15_n_3,
      Q(27) => reg_r15_n_4,
      Q(26) => reg_r15_n_5,
      Q(25) => reg_r15_n_6,
      Q(24) => reg_r15_n_7,
      Q(23) => reg_r15_n_8,
      Q(22) => reg_r15_n_9,
      Q(21) => reg_r15_n_10,
      Q(20) => reg_r15_n_11,
      Q(19) => reg_r15_n_12,
      Q(18) => reg_r15_n_13,
      Q(17) => reg_r15_n_14,
      Q(16) => reg_r15_n_15,
      Q(15) => reg_r15_n_16,
      Q(14) => reg_r15_n_17,
      Q(13) => reg_r15_n_18,
      Q(12) => reg_r15_n_19,
      Q(11) => reg_r15_n_20,
      Q(10) => reg_r15_n_21,
      Q(9) => reg_r15_n_22,
      Q(8) => reg_r15_n_23,
      Q(7) => reg_r15_n_24,
      Q(6) => reg_r15_n_25,
      Q(5) => reg_r15_n_26,
      Q(4) => reg_r15_n_27,
      Q(3) => reg_r15_n_28,
      Q(2) => reg_r15_n_29,
      Q(1) => reg_r15_n_30,
      Q(0) => reg_r15_n_31,
      SR(0) => RESET_IBUF
    );
reg_addr_w: entity work.REGrwe_n_1
     port map (
      CLK => CLK_IBUF_BUFG,
      D(3 downto 0) => ADDR_W_IBUF(3 downto 0),
      E(0) => WE_IBUF,
      Q(3 downto 0) => Dout(3 downto 0),
      SR(0) => RESET_IBUF
    );
reg_data_in: entity work.\REGrwe_n__parameterized2\
     port map (
      CLK => CLK_IBUF_BUFG,
      D(31 downto 0) => DATA_IN_IBUF(31 downto 0),
      E(0) => WE_IBUF,
      Q(31) => reg_data_in_n_0,
      Q(30) => reg_data_in_n_1,
      Q(29) => reg_data_in_n_2,
      Q(28) => reg_data_in_n_3,
      Q(27) => reg_data_in_n_4,
      Q(26) => reg_data_in_n_5,
      Q(25) => reg_data_in_n_6,
      Q(24) => reg_data_in_n_7,
      Q(23) => reg_data_in_n_8,
      Q(22) => reg_data_in_n_9,
      Q(21) => reg_data_in_n_10,
      Q(20) => reg_data_in_n_11,
      Q(19) => reg_data_in_n_12,
      Q(18) => reg_data_in_n_13,
      Q(17) => reg_data_in_n_14,
      Q(16) => reg_data_in_n_15,
      Q(15) => reg_data_in_n_16,
      Q(14) => reg_data_in_n_17,
      Q(13) => reg_data_in_n_18,
      Q(12) => reg_data_in_n_19,
      Q(11) => reg_data_in_n_20,
      Q(10) => reg_data_in_n_21,
      Q(9) => reg_data_in_n_22,
      Q(8) => reg_data_in_n_23,
      Q(7) => reg_data_in_n_24,
      Q(6) => reg_data_in_n_25,
      Q(5) => reg_data_in_n_26,
      Q(4) => reg_data_in_n_27,
      Q(3) => reg_data_in_n_28,
      Q(2) => reg_data_in_n_29,
      Q(1) => reg_data_in_n_30,
      Q(0) => reg_data_in_n_31,
      SR(0) => RESET_IBUF
    );
reg_data_out1: entity work.\REGrwe_n__parameterized2_2\
     port map (
      CLK => CLK_IBUF_BUFG,
      D(31 downto 0) => Din(31 downto 0),
      E(0) => WE_IBUF,
      Q(31 downto 0) => DATA_OUT1_OBUF(31 downto 0),
      SR(0) => RESET_IBUF
    );
reg_data_out2: entity work.\REGrwe_n__parameterized2_3\
     port map (
      CLK => CLK_IBUF_BUFG,
      D(31) => reg_addr_r2_n_4,
      D(30) => reg_addr_r2_n_5,
      D(29) => reg_addr_r2_n_6,
      D(28) => reg_addr_r2_n_7,
      D(27) => reg_addr_r2_n_8,
      D(26) => reg_addr_r2_n_9,
      D(25) => reg_addr_r2_n_10,
      D(24) => reg_addr_r2_n_11,
      D(23) => reg_addr_r2_n_12,
      D(22) => reg_addr_r2_n_13,
      D(21) => reg_addr_r2_n_14,
      D(20) => reg_addr_r2_n_15,
      D(19) => reg_addr_r2_n_16,
      D(18) => reg_addr_r2_n_17,
      D(17) => reg_addr_r2_n_18,
      D(16) => reg_addr_r2_n_19,
      D(15) => reg_addr_r2_n_20,
      D(14) => reg_addr_r2_n_21,
      D(13) => reg_addr_r2_n_22,
      D(12) => reg_addr_r2_n_23,
      D(11) => reg_addr_r2_n_24,
      D(10) => reg_addr_r2_n_25,
      D(9) => reg_addr_r2_n_26,
      D(8) => reg_addr_r2_n_27,
      D(7) => reg_addr_r2_n_28,
      D(6) => reg_addr_r2_n_29,
      D(5) => reg_addr_r2_n_30,
      D(4) => reg_addr_r2_n_31,
      D(3) => reg_addr_r2_n_32,
      D(2) => reg_addr_r2_n_33,
      D(1) => reg_addr_r2_n_34,
      D(0) => reg_addr_r2_n_35,
      Q(31 downto 0) => DATA_OUT2_OBUF(31 downto 0),
      SR(0) => RESET_IBUF,
      WE => WE_IBUF
    );
reg_r15: entity work.\REGrwe_n__parameterized2_4\
     port map (
      CLK => CLK_IBUF_BUFG,
      D(31 downto 0) => R15_IBUF(31 downto 0),
      Q(31) => reg_r15_n_0,
      Q(30) => reg_r15_n_1,
      Q(29) => reg_r15_n_2,
      Q(28) => reg_r15_n_3,
      Q(27) => reg_r15_n_4,
      Q(26) => reg_r15_n_5,
      Q(25) => reg_r15_n_6,
      Q(24) => reg_r15_n_7,
      Q(23) => reg_r15_n_8,
      Q(22) => reg_r15_n_9,
      Q(21) => reg_r15_n_10,
      Q(20) => reg_r15_n_11,
      Q(19) => reg_r15_n_12,
      Q(18) => reg_r15_n_13,
      Q(17) => reg_r15_n_14,
      Q(16) => reg_r15_n_15,
      Q(15) => reg_r15_n_16,
      Q(14) => reg_r15_n_17,
      Q(13) => reg_r15_n_18,
      Q(12) => reg_r15_n_19,
      Q(11) => reg_r15_n_20,
      Q(10) => reg_r15_n_21,
      Q(9) => reg_r15_n_22,
      Q(8) => reg_r15_n_23,
      Q(7) => reg_r15_n_24,
      Q(6) => reg_r15_n_25,
      Q(5) => reg_r15_n_26,
      Q(4) => reg_r15_n_27,
      Q(3) => reg_r15_n_28,
      Q(2) => reg_r15_n_29,
      Q(1) => reg_r15_n_30,
      Q(0) => reg_r15_n_31,
      RESET => RESET_IBUF,
      WE => WE_IBUF
    );
rf: entity work.REGISTER_FILE_wR15
     port map (
      ADDR_R1_in(3 downto 0) => ADDR_R1_in(3 downto 0),
      ADDR_R2_in(3 downto 0) => ADDR_R2_in(3 downto 0),
      CLK => CLK_IBUF_BUFG,
      \DATA_OUT1__0\(31 downto 0) => \DATA_OUT1__0\(31 downto 0),
      \DATA_OUT2__0\(31 downto 0) => \DATA_OUT2__0\(31 downto 0),
      \Dout_reg[1]\(3 downto 0) => Dout(3 downto 0),
      Q(31) => reg_data_in_n_0,
      Q(30) => reg_data_in_n_1,
      Q(29) => reg_data_in_n_2,
      Q(28) => reg_data_in_n_3,
      Q(27) => reg_data_in_n_4,
      Q(26) => reg_data_in_n_5,
      Q(25) => reg_data_in_n_6,
      Q(24) => reg_data_in_n_7,
      Q(23) => reg_data_in_n_8,
      Q(22) => reg_data_in_n_9,
      Q(21) => reg_data_in_n_10,
      Q(20) => reg_data_in_n_11,
      Q(19) => reg_data_in_n_12,
      Q(18) => reg_data_in_n_13,
      Q(17) => reg_data_in_n_14,
      Q(16) => reg_data_in_n_15,
      Q(15) => reg_data_in_n_16,
      Q(14) => reg_data_in_n_17,
      Q(13) => reg_data_in_n_18,
      Q(12) => reg_data_in_n_19,
      Q(11) => reg_data_in_n_20,
      Q(10) => reg_data_in_n_21,
      Q(9) => reg_data_in_n_22,
      Q(8) => reg_data_in_n_23,
      Q(7) => reg_data_in_n_24,
      Q(6) => reg_data_in_n_25,
      Q(5) => reg_data_in_n_26,
      Q(4) => reg_data_in_n_27,
      Q(3) => reg_data_in_n_28,
      Q(2) => reg_data_in_n_29,
      Q(1) => reg_data_in_n_30,
      Q(0) => reg_data_in_n_31,
      REG_WRITE_in => REG_WRITE_in
    );
end STRUCTURE;
