-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Mon Oct 12 18:30:55 2020
-- Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/mpliax/Documents/workspace/vhdl-m806-final-project-mc/vhdl-m806-final-project.sim/sim_1/synth/func/xsim/CONTROL_UNIT_TB_func_synth.vhd
-- Design      : CONTROL_UNIT
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity FSM is
  port (
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    PcSrc_OBUF : out STD_LOGIC_VECTOR ( 1 downto 0 );
    FlagsWrite_OBUF : out STD_LOGIC;
    RegWrite_OBUF : out STD_LOGIC;
    PCWrite_OBUF : out STD_LOGIC;
    RegSrc_OBUF : in STD_LOGIC_VECTOR ( 1 downto 0 );
    INSTR_IBUF : in STD_LOGIC_VECTOR ( 10 downto 0 );
    FLAGS_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 );
    RESET_IBUF : in STD_LOGIC;
    CLK_IBUF_BUFG : in STD_LOGIC
  );
end FSM;

architecture STRUCTURE of FSM is
  signal \FSM_onehot_current_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[10]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[10]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[11]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[11]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[12]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[13]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[13]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[13]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[13]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[13]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[4]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[5]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[6]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[7]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[8]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[8]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[9]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg[13]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg[13]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg[13]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[10]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[11]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[12]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[13]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[2]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[4]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[6]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[7]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[8]\ : STD_LOGIC;
  signal \FSM_onehot_current_state_reg_n_0_[9]\ : STD_LOGIC;
  signal IRWrite_OBUF : STD_LOGIC;
  signal MAWrite_OBUF : STD_LOGIC;
  signal MemWrite_OBUF : STD_LOGIC;
  signal PCWrite_OBUF_inst_i_2_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[0]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[2]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[4]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[5]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[7]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[8]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[8]_i_2\ : label is "soft_lutpair4";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[0]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute IOB : string;
  attribute IOB of \FSM_onehot_current_state_reg[0]\ : label is "TRUE";
  attribute ORIG_CELL_NAME : string;
  attribute ORIG_CELL_NAME of \FSM_onehot_current_state_reg[0]\ : label is "FSM_onehot_current_state_reg[0]";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[0]_rep\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute ORIG_CELL_NAME of \FSM_onehot_current_state_reg[0]_rep\ : label is "FSM_onehot_current_state_reg[0]";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[10]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[11]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[12]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[13]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[1]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[2]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[3]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute IOB of \FSM_onehot_current_state_reg[3]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \FSM_onehot_current_state_reg[3]\ : label is "FSM_onehot_current_state_reg[3]";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[3]_rep\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute ORIG_CELL_NAME of \FSM_onehot_current_state_reg[3]_rep\ : label is "FSM_onehot_current_state_reg[3]";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[4]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[5]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute IOB of \FSM_onehot_current_state_reg[5]\ : label is "TRUE";
  attribute ORIG_CELL_NAME of \FSM_onehot_current_state_reg[5]\ : label is "FSM_onehot_current_state_reg[5]";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[5]_rep\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute ORIG_CELL_NAME of \FSM_onehot_current_state_reg[5]_rep\ : label is "FSM_onehot_current_state_reg[5]";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[6]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[7]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[8]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[9]\ : label is "s2b:00000001000000,s3:00000000010000,s4i:10000000000000,s2a:00000000001000,s4h:01000000000000,s4g:00100000000000,s4f:00001000000000,s1:00000000000010,s0:00000000000001,s4e:00010000000000,s4c:00000000000100,s4b:00000010000000,s4d:00000000100000,s4a:00000100000000";
  attribute SOFT_HLUTNM of FlagsWrite_OBUF_inst_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \PcSrc_OBUF[0]_inst_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \PcSrc_OBUF[1]_inst_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of RegWrite_OBUF_inst_i_1 : label is "soft_lutpair5";
begin
\FSM_onehot_current_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000C000AAAA"
    )
        port map (
      I0 => \FSM_onehot_current_state[0]_i_2_n_0\,
      I1 => \FSM_onehot_current_state_reg[13]_i_2_n_0\,
      I2 => RegSrc_OBUF(0),
      I3 => RegSrc_OBUF(1),
      I4 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I5 => IRWrite_OBUF,
      O => \FSM_onehot_current_state[0]_i_1_n_0\
    );
\FSM_onehot_current_state[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[6]\,
      I1 => MAWrite_OBUF,
      I2 => \FSM_onehot_current_state_reg_n_0_[4]\,
      O => \FSM_onehot_current_state[0]_i_2_n_0\
    );
\FSM_onehot_current_state[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => INSTR_IBUF(4),
      I1 => \FSM_onehot_current_state_reg_n_0_[6]\,
      I2 => MAWrite_OBUF,
      I3 => IRWrite_OBUF,
      I4 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I5 => \FSM_onehot_current_state[10]_i_2_n_0\,
      O => \FSM_onehot_current_state[10]_i_1_n_0\
    );
\FSM_onehot_current_state[10]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => INSTR_IBUF(3),
      I1 => INSTR_IBUF(0),
      I2 => INSTR_IBUF(2),
      I3 => INSTR_IBUF(1),
      O => \FSM_onehot_current_state[10]_i_2_n_0\
    );
\FSM_onehot_current_state[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => INSTR_IBUF(5),
      I1 => INSTR_IBUF(6),
      I2 => \FSM_onehot_current_state[11]_i_2_n_0\,
      I3 => \FSM_onehot_current_state_reg[13]_i_2_n_0\,
      I4 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I5 => IRWrite_OBUF,
      O => \FSM_onehot_current_state[11]_i_1_n_0\
    );
\FSM_onehot_current_state[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => RegSrc_OBUF(1),
      I1 => RegSrc_OBUF(0),
      O => \FSM_onehot_current_state[11]_i_2_n_0\
    );
\FSM_onehot_current_state[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I1 => \FSM_onehot_current_state_reg[13]_i_2_n_0\,
      I2 => IRWrite_OBUF,
      I3 => RegSrc_OBUF(0),
      I4 => RegSrc_OBUF(1),
      I5 => INSTR_IBUF(6),
      O => \FSM_onehot_current_state[12]_i_1_n_0\
    );
\FSM_onehot_current_state[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000800000"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I1 => \FSM_onehot_current_state_reg[13]_i_2_n_0\,
      I2 => INSTR_IBUF(6),
      I3 => IRWrite_OBUF,
      I4 => RegSrc_OBUF(0),
      I5 => RegSrc_OBUF(1),
      O => \FSM_onehot_current_state[13]_i_1_n_0\
    );
\FSM_onehot_current_state[13]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"47B8"
    )
        port map (
      I0 => FLAGS_IBUF(2),
      I1 => INSTR_IBUF(8),
      I2 => FLAGS_IBUF(1),
      I3 => INSTR_IBUF(7),
      O => \FSM_onehot_current_state[13]_i_5_n_0\
    );
\FSM_onehot_current_state[13]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"47B8"
    )
        port map (
      I0 => FLAGS_IBUF(3),
      I1 => INSTR_IBUF(8),
      I2 => FLAGS_IBUF(0),
      I3 => INSTR_IBUF(7),
      O => \FSM_onehot_current_state[13]_i_6_n_0\
    );
\FSM_onehot_current_state[13]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55A6AAA6AAA655A6"
    )
        port map (
      I0 => INSTR_IBUF(7),
      I1 => FLAGS_IBUF(2),
      I2 => FLAGS_IBUF(1),
      I3 => INSTR_IBUF(8),
      I4 => FLAGS_IBUF(3),
      I5 => FLAGS_IBUF(0),
      O => \FSM_onehot_current_state[13]_i_7_n_0\
    );
\FSM_onehot_current_state[13]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFEBAAB"
    )
        port map (
      I0 => INSTR_IBUF(8),
      I1 => FLAGS_IBUF(1),
      I2 => FLAGS_IBUF(3),
      I3 => FLAGS_IBUF(0),
      I4 => INSTR_IBUF(7),
      O => \FSM_onehot_current_state[13]_i_8_n_0\
    );
\FSM_onehot_current_state[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => IRWrite_OBUF,
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I2 => \FSM_onehot_current_state_reg[13]_i_2_n_0\,
      O => \FSM_onehot_current_state[2]_i_1_n_0\
    );
\FSM_onehot_current_state[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00004000"
    )
        port map (
      I0 => RegSrc_OBUF(0),
      I1 => RegSrc_OBUF(1),
      I2 => \FSM_onehot_current_state_reg[13]_i_2_n_0\,
      I3 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I4 => IRWrite_OBUF,
      O => \FSM_onehot_current_state[3]_i_1_n_0\
    );
\FSM_onehot_current_state[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => INSTR_IBUF(4),
      I1 => MAWrite_OBUF,
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => IRWrite_OBUF,
      O => \FSM_onehot_current_state[4]_i_1_n_0\
    );
\FSM_onehot_current_state[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => INSTR_IBUF(4),
      I1 => MAWrite_OBUF,
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => IRWrite_OBUF,
      O => \FSM_onehot_current_state[5]_i_1_n_0\
    );
\FSM_onehot_current_state[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000B0000000"
    )
        port map (
      I0 => INSTR_IBUF(5),
      I1 => INSTR_IBUF(6),
      I2 => \FSM_onehot_current_state[11]_i_2_n_0\,
      I3 => \FSM_onehot_current_state_reg[13]_i_2_n_0\,
      I4 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I5 => IRWrite_OBUF,
      O => \FSM_onehot_current_state[6]_i_1_n_0\
    );
\FSM_onehot_current_state[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000100"
    )
        port map (
      I0 => MAWrite_OBUF,
      I1 => IRWrite_OBUF,
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \FSM_onehot_current_state[10]_i_2_n_0\,
      I4 => \FSM_onehot_current_state[8]_i_2_n_0\,
      O => \FSM_onehot_current_state[7]_i_1_n_0\
    );
\FSM_onehot_current_state[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => MAWrite_OBUF,
      I1 => IRWrite_OBUF,
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \FSM_onehot_current_state[10]_i_2_n_0\,
      I4 => \FSM_onehot_current_state[8]_i_2_n_0\,
      O => \FSM_onehot_current_state[8]_i_1_n_0\
    );
\FSM_onehot_current_state[8]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A3"
    )
        port map (
      I0 => INSTR_IBUF(4),
      I1 => \FSM_onehot_current_state_reg_n_0_[4]\,
      I2 => \FSM_onehot_current_state_reg_n_0_[6]\,
      O => \FSM_onehot_current_state[8]_i_2_n_0\
    );
\FSM_onehot_current_state[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => INSTR_IBUF(4),
      I1 => \FSM_onehot_current_state_reg_n_0_[6]\,
      I2 => MAWrite_OBUF,
      I3 => IRWrite_OBUF,
      I4 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I5 => \FSM_onehot_current_state[10]_i_2_n_0\,
      O => \FSM_onehot_current_state[9]_i_1_n_0\
    );
\FSM_onehot_current_state_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[0]_i_1_n_0\,
      Q => Q(0),
      S => RESET_IBUF
    );
\FSM_onehot_current_state_reg[0]_rep\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[0]_i_1_n_0\,
      Q => IRWrite_OBUF,
      S => RESET_IBUF
    );
\FSM_onehot_current_state_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[10]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[10]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[11]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[11]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[12]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[12]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[13]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[13]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[13]_i_2\: unisim.vcomponents.MUXF8
     port map (
      I0 => \FSM_onehot_current_state_reg[13]_i_3_n_0\,
      I1 => \FSM_onehot_current_state_reg[13]_i_4_n_0\,
      O => \FSM_onehot_current_state_reg[13]_i_2_n_0\,
      S => INSTR_IBUF(10)
    );
\FSM_onehot_current_state_reg[13]_i_3\: unisim.vcomponents.MUXF7
     port map (
      I0 => \FSM_onehot_current_state[13]_i_5_n_0\,
      I1 => \FSM_onehot_current_state[13]_i_6_n_0\,
      O => \FSM_onehot_current_state_reg[13]_i_3_n_0\,
      S => INSTR_IBUF(9)
    );
\FSM_onehot_current_state_reg[13]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \FSM_onehot_current_state[13]_i_7_n_0\,
      I1 => \FSM_onehot_current_state[13]_i_8_n_0\,
      O => \FSM_onehot_current_state_reg[13]_i_4_n_0\,
      S => INSTR_IBUF(9)
    );
\FSM_onehot_current_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => IRWrite_OBUF,
      Q => \FSM_onehot_current_state_reg_n_0_[1]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[2]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[2]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[3]_i_1_n_0\,
      Q => Q(1),
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[3]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[3]_i_1_n_0\,
      Q => MAWrite_OBUF,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[4]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[4]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[5]_i_1_n_0\,
      Q => Q(2),
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[5]_rep\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[5]_i_1_n_0\,
      Q => MemWrite_OBUF,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[6]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[6]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[7]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[7]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[8]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[8]\,
      R => RESET_IBUF
    );
\FSM_onehot_current_state_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_current_state[9]_i_1_n_0\,
      Q => \FSM_onehot_current_state_reg_n_0_[9]\,
      R => RESET_IBUF
    );
FlagsWrite_OBUF_inst_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[10]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[9]\,
      I2 => \FSM_onehot_current_state_reg_n_0_[11]\,
      O => FlagsWrite_OBUF
    );
PCWrite_OBUF_inst_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[12]\,
      I1 => PCWrite_OBUF_inst_i_2_n_0,
      I2 => \FSM_onehot_current_state_reg_n_0_[11]\,
      I3 => \FSM_onehot_current_state_reg_n_0_[13]\,
      I4 => \FSM_onehot_current_state_reg_n_0_[9]\,
      I5 => \FSM_onehot_current_state_reg_n_0_[10]\,
      O => PCWrite_OBUF
    );
PCWrite_OBUF_inst_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[7]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[8]\,
      I2 => \FSM_onehot_current_state_reg_n_0_[2]\,
      I3 => MemWrite_OBUF,
      O => PCWrite_OBUF_inst_i_2_n_0
    );
\PcSrc_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[12]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[13]\,
      O => PcSrc_OBUF(0)
    );
\PcSrc_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[13]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[12]\,
      I2 => \FSM_onehot_current_state_reg_n_0_[7]\,
      I3 => \FSM_onehot_current_state_reg_n_0_[9]\,
      O => PcSrc_OBUF(1)
    );
RegWrite_OBUF_inst_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg_n_0_[10]\,
      I1 => \FSM_onehot_current_state_reg_n_0_[8]\,
      I2 => \FSM_onehot_current_state_reg_n_0_[13]\,
      O => RegWrite_OBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity CONTROL_UNIT is
  port (
    INSTR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    FLAGS : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC;
    RESET : in STD_LOGIC;
    PcSrc : out STD_LOGIC_VECTOR ( 1 downto 0 );
    MemToReg : out STD_LOGIC;
    MemWrite : out STD_LOGIC;
    ALUControl : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ALUSrc : out STD_LOGIC;
    ImmSrc : out STD_LOGIC;
    RegWrite : out STD_LOGIC;
    RegSrc : out STD_LOGIC_VECTOR ( 2 downto 0 );
    FlagsWrite : out STD_LOGIC;
    PCWrite : out STD_LOGIC;
    IRWrite : out STD_LOGIC;
    MAWrite : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of CONTROL_UNIT : entity is true;
end CONTROL_UNIT;

architecture STRUCTURE of CONTROL_UNIT is
  signal ALUControl_OBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \ALUControl_OBUF[0]_inst_i_2_n_0\ : STD_LOGIC;
  signal \ALUControl_OBUF[1]_inst_i_2_n_0\ : STD_LOGIC;
  signal ALUSrc_OBUF : STD_LOGIC;
  signal CLK_IBUF : STD_LOGIC;
  signal CLK_IBUF_BUFG : STD_LOGIC;
  signal FLAGS_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal FSM_UNIT_n_0 : STD_LOGIC;
  signal FSM_UNIT_n_1 : STD_LOGIC;
  signal FSM_UNIT_n_2 : STD_LOGIC;
  signal FlagsWrite_OBUF : STD_LOGIC;
  signal INSTR_IBUF : STD_LOGIC_VECTOR ( 31 downto 5 );
  signal PCWrite_OBUF : STD_LOGIC;
  signal PcSrc_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal RESET_IBUF : STD_LOGIC;
  signal RegSrc_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal RegWrite_OBUF : STD_LOGIC;
  attribute IOB : string;
  attribute IOB of \ALUControl_OBUF[0]_inst\ : label is "TRUE";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ALUControl_OBUF[0]_inst_i_1\ : label is "soft_lutpair6";
  attribute IOB of \ALUControl_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \ALUControl_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of \ALUControl_OBUF[3]_inst\ : label is "TRUE";
  attribute IOB of ALUSrc_OBUF_inst : label is "TRUE";
  attribute SOFT_HLUTNM of ALUSrc_OBUF_inst_i_1 : label is "soft_lutpair6";
  attribute IOB of FlagsWrite_OBUF_inst : label is "TRUE";
  attribute IOB of IRWrite_OBUF_inst : label is "TRUE";
  attribute IOB of ImmSrc_OBUF_inst : label is "TRUE";
  attribute IOB of MAWrite_OBUF_inst : label is "TRUE";
  attribute IOB of MemToReg_OBUF_inst : label is "TRUE";
  attribute IOB of MemWrite_OBUF_inst : label is "TRUE";
  attribute IOB of PCWrite_OBUF_inst : label is "TRUE";
  attribute IOB of \PcSrc_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \PcSrc_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \RegSrc_OBUF[0]_inst\ : label is "TRUE";
  attribute IOB of \RegSrc_OBUF[1]_inst\ : label is "TRUE";
  attribute IOB of \RegSrc_OBUF[2]_inst\ : label is "TRUE";
  attribute IOB of RegWrite_OBUF_inst : label is "TRUE";
begin
\ALUControl_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUControl_OBUF(0),
      O => ALUControl(0)
    );
\ALUControl_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0053"
    )
        port map (
      I0 => INSTR_IBUF(23),
      I1 => \ALUControl_OBUF[0]_inst_i_2_n_0\,
      I2 => RegSrc_OBUF(1),
      I3 => RegSrc_OBUF(0),
      O => ALUControl_OBUF(0)
    );
\ALUControl_OBUF[0]_inst_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000DF0F"
    )
        port map (
      I0 => INSTR_IBUF(5),
      I1 => INSTR_IBUF(25),
      I2 => INSTR_IBUF(24),
      I3 => INSTR_IBUF(21),
      I4 => INSTR_IBUF(22),
      O => \ALUControl_OBUF[0]_inst_i_2_n_0\
    );
\ALUControl_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUControl_OBUF(1),
      O => ALUControl(1)
    );
\ALUControl_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \ALUControl_OBUF[1]_inst_i_2_n_0\,
      I1 => RegSrc_OBUF(1),
      I2 => RegSrc_OBUF(0),
      I3 => INSTR_IBUF(22),
      O => ALUControl_OBUF(1)
    );
\ALUControl_OBUF[1]_inst_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4FF04FFF"
    )
        port map (
      I0 => INSTR_IBUF(25),
      I1 => INSTR_IBUF(6),
      I2 => INSTR_IBUF(21),
      I3 => INSTR_IBUF(24),
      I4 => INSTR_IBUF(23),
      O => \ALUControl_OBUF[1]_inst_i_2_n_0\
    );
\ALUControl_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUControl_OBUF(2),
      O => ALUControl(2)
    );
\ALUControl_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000FB00000000"
    )
        port map (
      I0 => INSTR_IBUF(25),
      I1 => INSTR_IBUF(24),
      I2 => INSTR_IBUF(22),
      I3 => RegSrc_OBUF(1),
      I4 => RegSrc_OBUF(0),
      I5 => INSTR_IBUF(21),
      O => ALUControl_OBUF(2)
    );
\ALUControl_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => ALUControl_OBUF(3),
      O => ALUControl(3)
    );
\ALUControl_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => INSTR_IBUF(25),
      I1 => INSTR_IBUF(24),
      I2 => INSTR_IBUF(22),
      I3 => RegSrc_OBUF(1),
      I4 => RegSrc_OBUF(0),
      I5 => INSTR_IBUF(21),
      O => ALUControl_OBUF(3)
    );
ALUSrc_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => ALUSrc_OBUF,
      O => ALUSrc
    );
ALUSrc_OBUF_inst_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => INSTR_IBUF(25),
      I1 => RegSrc_OBUF(0),
      I2 => RegSrc_OBUF(1),
      O => ALUSrc_OBUF
    );
CLK_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => CLK_IBUF,
      O => CLK_IBUF_BUFG
    );
CLK_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => CLK,
      O => CLK_IBUF
    );
\FLAGS_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => FLAGS(0),
      O => FLAGS_IBUF(0)
    );
\FLAGS_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => FLAGS(1),
      O => FLAGS_IBUF(1)
    );
\FLAGS_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => FLAGS(2),
      O => FLAGS_IBUF(2)
    );
\FLAGS_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => FLAGS(3),
      O => FLAGS_IBUF(3)
    );
FSM_UNIT: entity work.FSM
     port map (
      CLK_IBUF_BUFG => CLK_IBUF_BUFG,
      FLAGS_IBUF(3 downto 0) => FLAGS_IBUF(3 downto 0),
      FlagsWrite_OBUF => FlagsWrite_OBUF,
      INSTR_IBUF(10 downto 7) => INSTR_IBUF(31 downto 28),
      INSTR_IBUF(6 downto 5) => INSTR_IBUF(24 downto 23),
      INSTR_IBUF(4) => INSTR_IBUF(20),
      INSTR_IBUF(3 downto 0) => INSTR_IBUF(15 downto 12),
      PCWrite_OBUF => PCWrite_OBUF,
      PcSrc_OBUF(1 downto 0) => PcSrc_OBUF(1 downto 0),
      Q(2) => FSM_UNIT_n_0,
      Q(1) => FSM_UNIT_n_1,
      Q(0) => FSM_UNIT_n_2,
      RESET_IBUF => RESET_IBUF,
      RegSrc_OBUF(1 downto 0) => RegSrc_OBUF(1 downto 0),
      RegWrite_OBUF => RegWrite_OBUF
    );
FlagsWrite_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => FlagsWrite_OBUF,
      O => FlagsWrite
    );
\INSTR_IBUF[12]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(12),
      O => INSTR_IBUF(12)
    );
\INSTR_IBUF[13]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(13),
      O => INSTR_IBUF(13)
    );
\INSTR_IBUF[14]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(14),
      O => INSTR_IBUF(14)
    );
\INSTR_IBUF[15]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(15),
      O => INSTR_IBUF(15)
    );
\INSTR_IBUF[20]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(20),
      O => INSTR_IBUF(20)
    );
\INSTR_IBUF[21]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(21),
      O => INSTR_IBUF(21)
    );
\INSTR_IBUF[22]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(22),
      O => INSTR_IBUF(22)
    );
\INSTR_IBUF[23]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(23),
      O => INSTR_IBUF(23)
    );
\INSTR_IBUF[24]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(24),
      O => INSTR_IBUF(24)
    );
\INSTR_IBUF[25]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(25),
      O => INSTR_IBUF(25)
    );
\INSTR_IBUF[26]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(26),
      O => RegSrc_OBUF(1)
    );
\INSTR_IBUF[27]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(27),
      O => RegSrc_OBUF(0)
    );
\INSTR_IBUF[28]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(28),
      O => INSTR_IBUF(28)
    );
\INSTR_IBUF[29]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(29),
      O => INSTR_IBUF(29)
    );
\INSTR_IBUF[30]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(30),
      O => INSTR_IBUF(30)
    );
\INSTR_IBUF[31]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(31),
      O => INSTR_IBUF(31)
    );
\INSTR_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(5),
      O => INSTR_IBUF(5)
    );
\INSTR_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => INSTR(6),
      O => INSTR_IBUF(6)
    );
IRWrite_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => FSM_UNIT_n_2,
      O => IRWrite
    );
ImmSrc_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => RegSrc_OBUF(0),
      O => ImmSrc
    );
MAWrite_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => FSM_UNIT_n_1,
      O => MAWrite
    );
MemToReg_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => RegSrc_OBUF(1),
      O => MemToReg
    );
MemWrite_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => FSM_UNIT_n_0,
      O => MemWrite
    );
PCWrite_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => PCWrite_OBUF,
      O => PCWrite
    );
\PcSrc_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => PcSrc_OBUF(0),
      O => PcSrc(0)
    );
\PcSrc_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => PcSrc_OBUF(1),
      O => PcSrc(1)
    );
RESET_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RESET,
      O => RESET_IBUF
    );
\RegSrc_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => RegSrc_OBUF(0),
      O => RegSrc(0)
    );
\RegSrc_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => RegSrc_OBUF(1),
      O => RegSrc(1)
    );
\RegSrc_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => RegSrc_OBUF(0),
      O => RegSrc(2)
    );
RegWrite_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => RegWrite_OBUF,
      O => RegWrite
    );
end STRUCTURE;
