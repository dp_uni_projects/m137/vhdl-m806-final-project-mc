// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Oct  6 20:21:35 2020
// Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/mpliax/Documents/workspace/vhdl-m806-final-project-mc/vhdl-m806-final-project.sim/sim_1/synth/timing/xsim/PROCESSOR_TB_time_synth.v
// Design      : ARM_PROCESSOR_n
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module RAM32M_UNIQ_BASE_
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD1
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD10
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD11
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD2
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD3
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD4
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD5
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD6
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD7
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD8
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module RAM32M_HD9
   (DOA,
    DOB,
    DOC,
    DOD,
    ADDRA,
    ADDRB,
    ADDRC,
    ADDRD,
    DIA,
    DIB,
    DIC,
    DID,
    WCLK,
    WE);
  output [1:0]DOA;
  output [1:0]DOB;
  output [1:0]DOC;
  output [1:0]DOD;
  input [4:0]ADDRA;
  input [4:0]ADDRB;
  input [4:0]ADDRC;
  input [4:0]ADDRD;
  input [1:0]DIA;
  input [1:0]DIB;
  input [1:0]DIC;
  input [1:0]DID;
  input WCLK;
  input WE;

  wire ADDRA0;
  wire ADDRA1;
  wire ADDRA2;
  wire ADDRA3;
  wire ADDRA4;
  wire ADDRB0;
  wire ADDRB1;
  wire ADDRB2;
  wire ADDRB3;
  wire ADDRB4;
  wire ADDRC0;
  wire ADDRC1;
  wire ADDRC2;
  wire ADDRC3;
  wire ADDRC4;
  wire ADDRD0;
  wire ADDRD1;
  wire ADDRD2;
  wire ADDRD3;
  wire ADDRD4;
  wire DIA0;
  wire DIA1;
  wire DIB0;
  wire DIB1;
  wire DIC0;
  wire DIC1;
  wire DID0;
  wire DID1;
  wire DOA0;
  wire DOA1;
  wire DOB0;
  wire DOB1;
  wire DOC0;
  wire DOC1;
  wire DOD0;
  wire DOD1;
  wire WCLK;
  wire WE;

  assign ADDRA0 = ADDRA[0];
  assign ADDRA1 = ADDRA[1];
  assign ADDRA2 = ADDRA[2];
  assign ADDRA3 = ADDRA[3];
  assign ADDRA4 = ADDRA[4];
  assign ADDRB0 = ADDRB[0];
  assign ADDRB1 = ADDRB[1];
  assign ADDRB2 = ADDRB[2];
  assign ADDRB3 = ADDRB[3];
  assign ADDRB4 = ADDRB[4];
  assign ADDRC0 = ADDRC[0];
  assign ADDRC1 = ADDRC[1];
  assign ADDRC2 = ADDRC[2];
  assign ADDRC3 = ADDRC[3];
  assign ADDRC4 = ADDRC[4];
  assign ADDRD0 = ADDRD[0];
  assign ADDRD1 = ADDRD[1];
  assign ADDRD2 = ADDRD[2];
  assign ADDRD3 = ADDRD[3];
  assign ADDRD4 = ADDRD[4];
  assign DIA0 = DIA[0];
  assign DIA1 = DIA[1];
  assign DIB0 = DIB[0];
  assign DIB1 = DIB[1];
  assign DIC0 = DIC[0];
  assign DIC1 = DIC[1];
  assign DID0 = DID[0];
  assign DID1 = DID[1];
  assign DOA[1] = DOA1;
  assign DOA[0] = DOA0;
  assign DOB[1] = DOB1;
  assign DOB[0] = DOB0;
  assign DOC[1] = DOC1;
  assign DOC[0] = DOC0;
  assign DOD[1] = DOD1;
  assign DOD[0] = DOD0;
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA
       (.CLK(WCLK),
        .I(DIA0),
        .O(DOA0),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMA_D1
       (.CLK(WCLK),
        .I(DIA1),
        .O(DOA1),
        .RADR0(ADDRA0),
        .RADR1(ADDRA1),
        .RADR2(ADDRA2),
        .RADR3(ADDRA3),
        .RADR4(ADDRA4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB
       (.CLK(WCLK),
        .I(DIB0),
        .O(DOB0),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMB_D1
       (.CLK(WCLK),
        .I(DIB1),
        .O(DOB1),
        .RADR0(ADDRB0),
        .RADR1(ADDRB1),
        .RADR2(ADDRB2),
        .RADR3(ADDRB3),
        .RADR4(ADDRB4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC
       (.CLK(WCLK),
        .I(DIC0),
        .O(DOC0),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMD32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMC_D1
       (.CLK(WCLK),
        .I(DIC1),
        .O(DOC1),
        .RADR0(ADDRC0),
        .RADR1(ADDRC1),
        .RADR2(ADDRC2),
        .RADR3(ADDRC3),
        .RADR4(ADDRC4),
        .WADR0(ADDRD0),
        .WADR1(ADDRD1),
        .WADR2(ADDRD2),
        .WADR3(ADDRD3),
        .WADR4(ADDRD4),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID0),
        .O(DOD0),
        .WE(WE));
  RAMS32 #(
    .INIT(32'h00000000),
    .IS_CLK_INVERTED(1'b0)) 
    RAMD_D1
       (.ADR0(ADDRD0),
        .ADR1(ADDRD1),
        .ADR2(ADDRD2),
        .ADR3(ADDRD3),
        .ADR4(ADDRD4),
        .CLK(WCLK),
        .I(DID1),
        .O(DOD1),
        .WE(WE));
endmodule

module ADDER_n
   (PCPlus8,
    D,
    Q,
    \Dout_reg[31] ,
    DATA_OUT1);
  output [28:0]PCPlus8;
  output [28:0]D;
  input [29:0]Q;
  input \Dout_reg[31] ;
  input [28:0]DATA_OUT1;

  wire [28:0]D;
  wire [28:0]DATA_OUT1;
  wire \Dout_reg[31] ;
  wire [28:0]PCPlus8;
  wire [29:0]Q;
  wire S0_carry__0_n_0;
  wire S0_carry__0_n_1;
  wire S0_carry__0_n_2;
  wire S0_carry__0_n_3;
  wire S0_carry__1_n_0;
  wire S0_carry__1_n_1;
  wire S0_carry__1_n_2;
  wire S0_carry__1_n_3;
  wire S0_carry__2_n_0;
  wire S0_carry__2_n_1;
  wire S0_carry__2_n_2;
  wire S0_carry__2_n_3;
  wire S0_carry__3_n_0;
  wire S0_carry__3_n_1;
  wire S0_carry__3_n_2;
  wire S0_carry__3_n_3;
  wire S0_carry__4_n_0;
  wire S0_carry__4_n_1;
  wire S0_carry__4_n_2;
  wire S0_carry__4_n_3;
  wire S0_carry__5_n_0;
  wire S0_carry__5_n_1;
  wire S0_carry__5_n_2;
  wire S0_carry__5_n_3;
  wire S0_carry_n_0;
  wire S0_carry_n_1;
  wire S0_carry_n_2;
  wire S0_carry_n_3;
  wire [3:0]NLW_S0_carry__6_CO_UNCONNECTED;
  wire [3:1]NLW_S0_carry__6_O_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[10]_i_1__1 
       (.I0(PCPlus8[7]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[7]),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[11]_i_1__0 
       (.I0(PCPlus8[8]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[8]),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[12]_i_1__0 
       (.I0(PCPlus8[9]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[9]),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[13]_i_1__0 
       (.I0(PCPlus8[10]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[10]),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[14]_i_1__1 
       (.I0(PCPlus8[11]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[11]),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[15]_i_1__1 
       (.I0(PCPlus8[12]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[12]),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[16]_i_1__1 
       (.I0(PCPlus8[13]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[13]),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[17]_i_1__1 
       (.I0(PCPlus8[14]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[14]),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[18]_i_1__0 
       (.I0(PCPlus8[15]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[15]),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[19]_i_1__0 
       (.I0(PCPlus8[16]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[16]),
        .O(D[16]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[20]_i_1__1 
       (.I0(PCPlus8[17]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[17]),
        .O(D[17]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[21]_i_1__0 
       (.I0(PCPlus8[18]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[18]),
        .O(D[18]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[22]_i_1__0 
       (.I0(PCPlus8[19]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[19]),
        .O(D[19]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[23]_i_1__1 
       (.I0(PCPlus8[20]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[20]),
        .O(D[20]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[24]_i_1__0 
       (.I0(PCPlus8[21]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[21]),
        .O(D[21]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[25]_i_1__0 
       (.I0(PCPlus8[22]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[22]),
        .O(D[22]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[26]_i_1__0 
       (.I0(PCPlus8[23]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[23]),
        .O(D[23]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[27]_i_1__0 
       (.I0(PCPlus8[24]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[24]),
        .O(D[24]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[28]_i_1__0 
       (.I0(PCPlus8[25]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[25]),
        .O(D[25]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[29]_i_1__0 
       (.I0(PCPlus8[26]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[26]),
        .O(D[26]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[30]_i_1__0 
       (.I0(PCPlus8[27]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[27]),
        .O(D[27]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[31]_i_1__1 
       (.I0(PCPlus8[28]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[28]),
        .O(D[28]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[3]_i_1__1 
       (.I0(PCPlus8[0]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[4]_i_1__1 
       (.I0(PCPlus8[1]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[1]),
        .O(D[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[5]_i_1__1 
       (.I0(PCPlus8[2]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[2]),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[6]_i_1__1 
       (.I0(PCPlus8[3]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[3]),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[7]_i_1__0 
       (.I0(PCPlus8[4]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[4]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[8]_i_1__0 
       (.I0(PCPlus8[5]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[5]),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[9]_i_1__0 
       (.I0(PCPlus8[6]),
        .I1(\Dout_reg[31] ),
        .I2(DATA_OUT1[6]),
        .O(D[6]));
  CARRY4 S0_carry
       (.CI(1'b0),
        .CO({S0_carry_n_0,S0_carry_n_1,S0_carry_n_2,S0_carry_n_3}),
        .CYINIT(Q[0]),
        .DI(Q[4:1]),
        .O(PCPlus8[3:0]),
        .S(Q[4:1]));
  CARRY4 S0_carry__0
       (.CI(S0_carry_n_0),
        .CO({S0_carry__0_n_0,S0_carry__0_n_1,S0_carry__0_n_2,S0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[8:5]),
        .O(PCPlus8[7:4]),
        .S(Q[8:5]));
  CARRY4 S0_carry__1
       (.CI(S0_carry__0_n_0),
        .CO({S0_carry__1_n_0,S0_carry__1_n_1,S0_carry__1_n_2,S0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[12:9]),
        .O(PCPlus8[11:8]),
        .S(Q[12:9]));
  CARRY4 S0_carry__2
       (.CI(S0_carry__1_n_0),
        .CO({S0_carry__2_n_0,S0_carry__2_n_1,S0_carry__2_n_2,S0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(Q[16:13]),
        .O(PCPlus8[15:12]),
        .S(Q[16:13]));
  CARRY4 S0_carry__3
       (.CI(S0_carry__2_n_0),
        .CO({S0_carry__3_n_0,S0_carry__3_n_1,S0_carry__3_n_2,S0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(Q[20:17]),
        .O(PCPlus8[19:16]),
        .S(Q[20:17]));
  CARRY4 S0_carry__4
       (.CI(S0_carry__3_n_0),
        .CO({S0_carry__4_n_0,S0_carry__4_n_1,S0_carry__4_n_2,S0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(Q[24:21]),
        .O(PCPlus8[23:20]),
        .S(Q[24:21]));
  CARRY4 S0_carry__5
       (.CI(S0_carry__4_n_0),
        .CO({S0_carry__5_n_0,S0_carry__5_n_1,S0_carry__5_n_2,S0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(Q[28:25]),
        .O(PCPlus8[27:24]),
        .S(Q[28:25]));
  CARRY4 S0_carry__6
       (.CI(S0_carry__5_n_0),
        .CO(NLW_S0_carry__6_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_S0_carry__6_O_UNCONNECTED[3:1],PCPlus8[28]}),
        .S({1'b0,1'b0,1'b0,Q[29]}));
endmodule

(* ORIG_REF_NAME = "ADDER_n" *) 
module ADDER_n_6
   (S_Add_in,
    Q,
    S,
    \ALUResult_out_OBUF[4]_inst_i_2 ,
    \Dout_reg[11] ,
    \Dout_reg[13] ,
    \ALUResult_out_OBUF[16]_inst_i_2 ,
    \Dout_reg[23] ,
    \ALUResult_out_OBUF[24]_inst_i_2 ,
    \Dout[28]_i_4 );
  output [31:0]S_Add_in;
  input [30:0]Q;
  input [3:0]S;
  input [3:0]\ALUResult_out_OBUF[4]_inst_i_2 ;
  input [3:0]\Dout_reg[11] ;
  input [3:0]\Dout_reg[13] ;
  input [3:0]\ALUResult_out_OBUF[16]_inst_i_2 ;
  input [3:0]\Dout_reg[23] ;
  input [3:0]\ALUResult_out_OBUF[24]_inst_i_2 ;
  input [3:0]\Dout[28]_i_4 ;

  wire [3:0]\ALUResult_out_OBUF[16]_inst_i_2 ;
  wire [3:0]\ALUResult_out_OBUF[24]_inst_i_2 ;
  wire [3:0]\ALUResult_out_OBUF[4]_inst_i_2 ;
  wire [3:0]\Dout[28]_i_4 ;
  wire [3:0]\Dout_reg[11] ;
  wire [3:0]\Dout_reg[13] ;
  wire [3:0]\Dout_reg[23] ;
  wire [30:0]Q;
  wire [3:0]S;
  wire S0_carry__0_n_0;
  wire S0_carry__0_n_1;
  wire S0_carry__0_n_2;
  wire S0_carry__0_n_3;
  wire S0_carry__1_n_0;
  wire S0_carry__1_n_1;
  wire S0_carry__1_n_2;
  wire S0_carry__1_n_3;
  wire S0_carry__2_n_0;
  wire S0_carry__2_n_1;
  wire S0_carry__2_n_2;
  wire S0_carry__2_n_3;
  wire S0_carry__3_n_0;
  wire S0_carry__3_n_1;
  wire S0_carry__3_n_2;
  wire S0_carry__3_n_3;
  wire S0_carry__4_n_0;
  wire S0_carry__4_n_1;
  wire S0_carry__4_n_2;
  wire S0_carry__4_n_3;
  wire S0_carry__5_n_0;
  wire S0_carry__5_n_1;
  wire S0_carry__5_n_2;
  wire S0_carry__5_n_3;
  wire S0_carry__6_n_1;
  wire S0_carry__6_n_2;
  wire S0_carry__6_n_3;
  wire S0_carry_n_0;
  wire S0_carry_n_1;
  wire S0_carry_n_2;
  wire S0_carry_n_3;
  wire [31:0]S_Add_in;
  wire [3:3]NLW_S0_carry__6_CO_UNCONNECTED;

  CARRY4 S0_carry
       (.CI(1'b0),
        .CO({S0_carry_n_0,S0_carry_n_1,S0_carry_n_2,S0_carry_n_3}),
        .CYINIT(1'b0),
        .DI(Q[3:0]),
        .O(S_Add_in[3:0]),
        .S(S));
  CARRY4 S0_carry__0
       (.CI(S0_carry_n_0),
        .CO({S0_carry__0_n_0,S0_carry__0_n_1,S0_carry__0_n_2,S0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O(S_Add_in[7:4]),
        .S(\ALUResult_out_OBUF[4]_inst_i_2 ));
  CARRY4 S0_carry__1
       (.CI(S0_carry__0_n_0),
        .CO({S0_carry__1_n_0,S0_carry__1_n_1,S0_carry__1_n_2,S0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[11:8]),
        .O(S_Add_in[11:8]),
        .S(\Dout_reg[11] ));
  CARRY4 S0_carry__2
       (.CI(S0_carry__1_n_0),
        .CO({S0_carry__2_n_0,S0_carry__2_n_1,S0_carry__2_n_2,S0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(Q[15:12]),
        .O(S_Add_in[15:12]),
        .S(\Dout_reg[13] ));
  CARRY4 S0_carry__3
       (.CI(S0_carry__2_n_0),
        .CO({S0_carry__3_n_0,S0_carry__3_n_1,S0_carry__3_n_2,S0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(Q[19:16]),
        .O(S_Add_in[19:16]),
        .S(\ALUResult_out_OBUF[16]_inst_i_2 ));
  CARRY4 S0_carry__4
       (.CI(S0_carry__3_n_0),
        .CO({S0_carry__4_n_0,S0_carry__4_n_1,S0_carry__4_n_2,S0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(Q[23:20]),
        .O(S_Add_in[23:20]),
        .S(\Dout_reg[23] ));
  CARRY4 S0_carry__5
       (.CI(S0_carry__4_n_0),
        .CO({S0_carry__5_n_0,S0_carry__5_n_1,S0_carry__5_n_2,S0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(Q[27:24]),
        .O(S_Add_in[27:24]),
        .S(\ALUResult_out_OBUF[24]_inst_i_2 ));
  CARRY4 S0_carry__6
       (.CI(S0_carry__5_n_0),
        .CO({NLW_S0_carry__6_CO_UNCONNECTED[3],S0_carry__6_n_1,S0_carry__6_n_2,S0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,Q[30:28]}),
        .O(S_Add_in[31:28]),
        .S(\Dout[28]_i_4 ));
endmodule

module ADD_SUB_n
   (S_Add_in,
    Q,
    S,
    \ALUResult_out_OBUF[4]_inst_i_2 ,
    \Dout_reg[11] ,
    \Dout_reg[13] ,
    \ALUResult_out_OBUF[16]_inst_i_2 ,
    \Dout_reg[23] ,
    \ALUResult_out_OBUF[24]_inst_i_2 ,
    \Dout[28]_i_4 );
  output [31:0]S_Add_in;
  input [30:0]Q;
  input [3:0]S;
  input [3:0]\ALUResult_out_OBUF[4]_inst_i_2 ;
  input [3:0]\Dout_reg[11] ;
  input [3:0]\Dout_reg[13] ;
  input [3:0]\ALUResult_out_OBUF[16]_inst_i_2 ;
  input [3:0]\Dout_reg[23] ;
  input [3:0]\ALUResult_out_OBUF[24]_inst_i_2 ;
  input [3:0]\Dout[28]_i_4 ;

  wire [3:0]\ALUResult_out_OBUF[16]_inst_i_2 ;
  wire [3:0]\ALUResult_out_OBUF[24]_inst_i_2 ;
  wire [3:0]\ALUResult_out_OBUF[4]_inst_i_2 ;
  wire [3:0]\Dout[28]_i_4 ;
  wire [3:0]\Dout_reg[11] ;
  wire [3:0]\Dout_reg[13] ;
  wire [3:0]\Dout_reg[23] ;
  wire [30:0]Q;
  wire [3:0]S;
  wire [31:0]S_Add_in;

  ADDER_n_6 ADD_UNIT
       (.\ALUResult_out_OBUF[16]_inst_i_2 (\ALUResult_out_OBUF[16]_inst_i_2 ),
        .\ALUResult_out_OBUF[24]_inst_i_2 (\ALUResult_out_OBUF[24]_inst_i_2 ),
        .\ALUResult_out_OBUF[4]_inst_i_2 (\ALUResult_out_OBUF[4]_inst_i_2 ),
        .\Dout[28]_i_4 (\Dout[28]_i_4 ),
        .\Dout_reg[11] (\Dout_reg[11] ),
        .\Dout_reg[13] (\Dout_reg[13] ),
        .\Dout_reg[23] (\Dout_reg[23] ),
        .Q(Q),
        .S(S),
        .S_Add_in(S_Add_in));
endmodule

module ALU_n
   (S_Add_in,
    Q,
    S,
    \ALUResult_out_OBUF[4]_inst_i_2 ,
    \Dout_reg[11] ,
    \Dout_reg[13] ,
    \ALUResult_out_OBUF[16]_inst_i_2 ,
    \Dout_reg[23] ,
    \ALUResult_out_OBUF[24]_inst_i_2 ,
    \Dout[28]_i_4 );
  output [31:0]S_Add_in;
  input [30:0]Q;
  input [3:0]S;
  input [3:0]\ALUResult_out_OBUF[4]_inst_i_2 ;
  input [3:0]\Dout_reg[11] ;
  input [3:0]\Dout_reg[13] ;
  input [3:0]\ALUResult_out_OBUF[16]_inst_i_2 ;
  input [3:0]\Dout_reg[23] ;
  input [3:0]\ALUResult_out_OBUF[24]_inst_i_2 ;
  input [3:0]\Dout[28]_i_4 ;

  wire [3:0]\ALUResult_out_OBUF[16]_inst_i_2 ;
  wire [3:0]\ALUResult_out_OBUF[24]_inst_i_2 ;
  wire [3:0]\ALUResult_out_OBUF[4]_inst_i_2 ;
  wire [3:0]\Dout[28]_i_4 ;
  wire [3:0]\Dout_reg[11] ;
  wire [3:0]\Dout_reg[13] ;
  wire [3:0]\Dout_reg[23] ;
  wire [30:0]Q;
  wire [3:0]S;
  wire [31:0]S_Add_in;

  ADD_SUB_n ADD_SUB
       (.\ALUResult_out_OBUF[16]_inst_i_2 (\ALUResult_out_OBUF[16]_inst_i_2 ),
        .\ALUResult_out_OBUF[24]_inst_i_2 (\ALUResult_out_OBUF[24]_inst_i_2 ),
        .\ALUResult_out_OBUF[4]_inst_i_2 (\ALUResult_out_OBUF[4]_inst_i_2 ),
        .\Dout[28]_i_4 (\Dout[28]_i_4 ),
        .\Dout_reg[11] (\Dout_reg[11] ),
        .\Dout_reg[13] (\Dout_reg[13] ),
        .\Dout_reg[23] (\Dout_reg[23] ),
        .Q(Q),
        .S(S),
        .S_Add_in(S_Add_in));
endmodule

(* NotValidForBitStream *)
module ARM_PROCESSOR_n
   (CLK,
    RESET,
    ALUControl_out,
    Instr_out,
    PC_out,
    ALUResult_out,
    Result_out,
    WriteData_out);
  input CLK;
  input RESET;
  output [3:0]ALUControl_out;
  output [31:0]Instr_out;
  output [31:0]PC_out;
  output [31:0]ALUResult_out;
  output [31:0]Result_out;
  output [31:0]WriteData_out;

  wire [3:0]ALUControl_out;
  wire [3:0]ALUControl_out_OBUF;
  wire [31:0]ALUResult_out;
  wire [31:0]ALUResult_out_OBUF;
  wire CLK;
  wire CLK_IBUF;
  wire CLK_IBUF_BUFG;
  wire CONTROL_n_1;
  wire CONTROL_n_10;
  wire CONTROL_n_11;
  wire CONTROL_n_12;
  wire CONTROL_n_13;
  wire CONTROL_n_14;
  wire CONTROL_n_15;
  wire CONTROL_n_16;
  wire CONTROL_n_17;
  wire CONTROL_n_18;
  wire CONTROL_n_19;
  wire CONTROL_n_2;
  wire CONTROL_n_20;
  wire CONTROL_n_21;
  wire CONTROL_n_3;
  wire CONTROL_n_4;
  wire CONTROL_n_7;
  wire CONTROL_n_8;
  wire CONTROL_n_9;
  wire DATAPATH_n_132;
  wire DATAPATH_n_133;
  wire DATAPATH_n_134;
  wire DATAPATH_n_135;
  wire DATAPATH_n_99;
  wire IRWrite_in;
  wire [31:0]Instr_out;
  wire [29:0]Instr_out_OBUF;
  wire [30:1]PCPlus4_regPCP4;
  wire PCWrite_in;
  wire [31:0]PC_out;
  wire [31:0]PC_out_OBUF;
  wire RESET;
  wire RESET_IBUF;
  wire RegWrite_in;
  wire [31:0]Result_out;
  wire [31:0]Result_out_OBUF;
  wire [31:0]WriteData_out;
  wire [31:0]WriteData_out_OBUF;

initial begin
 $sdf_annotate("PROCESSOR_TB_time_synth.sdf",,,,"tool_control");
end
  OBUF \ALUControl_out_OBUF[0]_inst 
       (.I(ALUControl_out_OBUF[0]),
        .O(ALUControl_out[0]));
  OBUF \ALUControl_out_OBUF[1]_inst 
       (.I(ALUControl_out_OBUF[1]),
        .O(ALUControl_out[1]));
  OBUF \ALUControl_out_OBUF[2]_inst 
       (.I(ALUControl_out_OBUF[2]),
        .O(ALUControl_out[2]));
  OBUF \ALUControl_out_OBUF[3]_inst 
       (.I(ALUControl_out_OBUF[3]),
        .O(ALUControl_out[3]));
  OBUF \ALUResult_out_OBUF[0]_inst 
       (.I(ALUResult_out_OBUF[0]),
        .O(ALUResult_out[0]));
  OBUF \ALUResult_out_OBUF[10]_inst 
       (.I(ALUResult_out_OBUF[10]),
        .O(ALUResult_out[10]));
  OBUF \ALUResult_out_OBUF[11]_inst 
       (.I(ALUResult_out_OBUF[11]),
        .O(ALUResult_out[11]));
  OBUF \ALUResult_out_OBUF[12]_inst 
       (.I(ALUResult_out_OBUF[12]),
        .O(ALUResult_out[12]));
  OBUF \ALUResult_out_OBUF[13]_inst 
       (.I(ALUResult_out_OBUF[13]),
        .O(ALUResult_out[13]));
  OBUF \ALUResult_out_OBUF[14]_inst 
       (.I(ALUResult_out_OBUF[14]),
        .O(ALUResult_out[14]));
  OBUF \ALUResult_out_OBUF[15]_inst 
       (.I(ALUResult_out_OBUF[15]),
        .O(ALUResult_out[15]));
  OBUF \ALUResult_out_OBUF[16]_inst 
       (.I(ALUResult_out_OBUF[16]),
        .O(ALUResult_out[16]));
  OBUF \ALUResult_out_OBUF[17]_inst 
       (.I(ALUResult_out_OBUF[17]),
        .O(ALUResult_out[17]));
  OBUF \ALUResult_out_OBUF[18]_inst 
       (.I(ALUResult_out_OBUF[18]),
        .O(ALUResult_out[18]));
  OBUF \ALUResult_out_OBUF[19]_inst 
       (.I(ALUResult_out_OBUF[19]),
        .O(ALUResult_out[19]));
  OBUF \ALUResult_out_OBUF[1]_inst 
       (.I(ALUResult_out_OBUF[1]),
        .O(ALUResult_out[1]));
  OBUF \ALUResult_out_OBUF[20]_inst 
       (.I(ALUResult_out_OBUF[20]),
        .O(ALUResult_out[20]));
  OBUF \ALUResult_out_OBUF[21]_inst 
       (.I(ALUResult_out_OBUF[21]),
        .O(ALUResult_out[21]));
  OBUF \ALUResult_out_OBUF[22]_inst 
       (.I(ALUResult_out_OBUF[22]),
        .O(ALUResult_out[22]));
  OBUF \ALUResult_out_OBUF[23]_inst 
       (.I(ALUResult_out_OBUF[23]),
        .O(ALUResult_out[23]));
  OBUF \ALUResult_out_OBUF[24]_inst 
       (.I(ALUResult_out_OBUF[24]),
        .O(ALUResult_out[24]));
  OBUF \ALUResult_out_OBUF[25]_inst 
       (.I(ALUResult_out_OBUF[25]),
        .O(ALUResult_out[25]));
  OBUF \ALUResult_out_OBUF[26]_inst 
       (.I(ALUResult_out_OBUF[26]),
        .O(ALUResult_out[26]));
  OBUF \ALUResult_out_OBUF[27]_inst 
       (.I(ALUResult_out_OBUF[27]),
        .O(ALUResult_out[27]));
  OBUF \ALUResult_out_OBUF[28]_inst 
       (.I(ALUResult_out_OBUF[28]),
        .O(ALUResult_out[28]));
  OBUF \ALUResult_out_OBUF[29]_inst 
       (.I(ALUResult_out_OBUF[29]),
        .O(ALUResult_out[29]));
  OBUF \ALUResult_out_OBUF[2]_inst 
       (.I(ALUResult_out_OBUF[2]),
        .O(ALUResult_out[2]));
  OBUF \ALUResult_out_OBUF[30]_inst 
       (.I(ALUResult_out_OBUF[30]),
        .O(ALUResult_out[30]));
  OBUF \ALUResult_out_OBUF[31]_inst 
       (.I(ALUResult_out_OBUF[31]),
        .O(ALUResult_out[31]));
  OBUF \ALUResult_out_OBUF[3]_inst 
       (.I(ALUResult_out_OBUF[3]),
        .O(ALUResult_out[3]));
  OBUF \ALUResult_out_OBUF[4]_inst 
       (.I(ALUResult_out_OBUF[4]),
        .O(ALUResult_out[4]));
  OBUF \ALUResult_out_OBUF[5]_inst 
       (.I(ALUResult_out_OBUF[5]),
        .O(ALUResult_out[5]));
  OBUF \ALUResult_out_OBUF[6]_inst 
       (.I(ALUResult_out_OBUF[6]),
        .O(ALUResult_out[6]));
  OBUF \ALUResult_out_OBUF[7]_inst 
       (.I(ALUResult_out_OBUF[7]),
        .O(ALUResult_out[7]));
  OBUF \ALUResult_out_OBUF[8]_inst 
       (.I(ALUResult_out_OBUF[8]),
        .O(ALUResult_out[8]));
  OBUF \ALUResult_out_OBUF[9]_inst 
       (.I(ALUResult_out_OBUF[9]),
        .O(ALUResult_out[9]));
  BUFG CLK_IBUF_BUFG_inst
       (.I(CLK_IBUF),
        .O(CLK_IBUF_BUFG));
  IBUF CLK_IBUF_inst
       (.I(CLK),
        .O(CLK_IBUF));
  CONTROL_UNIT CONTROL
       (.CLK(CLK_IBUF_BUFG),
        .D({DATAPATH_n_132,DATAPATH_n_133,DATAPATH_n_134}),
        .\Dout[30]_i_4 ({PCPlus4_regPCP4[30:25],PCPlus4_regPCP4[23],PCPlus4_regPCP4[20],PCPlus4_regPCP4[15:14],PCPlus4_regPCP4[6],PCPlus4_regPCP4[3:1]}),
        .\Dout[30]_i_4_0 ({Result_out_OBUF[30:25],Result_out_OBUF[23],Result_out_OBUF[20],Result_out_OBUF[15:14],Result_out_OBUF[6],Result_out_OBUF[3:1]}),
        .\Dout_reg[14] (CONTROL_n_16),
        .\Dout_reg[15] (CONTROL_n_15),
        .\Dout_reg[1] (CONTROL_n_20),
        .\Dout_reg[20] (CONTROL_n_14),
        .\Dout_reg[23] (CONTROL_n_13),
        .\Dout_reg[25] (CONTROL_n_12),
        .\Dout_reg[26] (CONTROL_n_11),
        .\Dout_reg[27] (CONTROL_n_10),
        .\Dout_reg[28] (CONTROL_n_9),
        .\Dout_reg[29] (CONTROL_n_8),
        .\Dout_reg[2] (CONTROL_n_19),
        .\Dout_reg[30] (CONTROL_n_7),
        .\Dout_reg[3] (CONTROL_n_18),
        .\Dout_reg[6] (CONTROL_n_17),
        .E(PCWrite_in),
        .\FSM_onehot_current_state_reg[0] (DATAPATH_n_99),
        .\FSM_onehot_current_state_reg[12] (CONTROL_n_21),
        .\FSM_onehot_current_state_reg[2] ({Instr_out_OBUF[29],Instr_out_OBUF[24]}),
        .\FSM_onehot_current_state_reg[8] (DATAPATH_n_135),
        .Q({CONTROL_n_1,CONTROL_n_2,CONTROL_n_3,CONTROL_n_4,IRWrite_in}),
        .RegWrite_in(RegWrite_in),
        .SR(RESET_IBUF));
  DATAPATH_MC_n DATAPATH
       (.ALUControl_out_OBUF(ALUControl_out_OBUF),
        .CLK(CLK_IBUF_BUFG),
        .D(ALUResult_out_OBUF),
        .\Dout_reg[0] (WriteData_out_OBUF),
        .\Dout_reg[12] (DATAPATH_n_135),
        .\Dout_reg[13] ({CONTROL_n_1,CONTROL_n_2,CONTROL_n_3,CONTROL_n_4,IRWrite_in}),
        .\Dout_reg[14] (CONTROL_n_16),
        .\Dout_reg[15] (CONTROL_n_15),
        .\Dout_reg[1] (CONTROL_n_20),
        .\Dout_reg[20] (CONTROL_n_14),
        .\Dout_reg[23] (CONTROL_n_13),
        .\Dout_reg[24] ({DATAPATH_n_132,DATAPATH_n_133,DATAPATH_n_134}),
        .\Dout_reg[25] (CONTROL_n_12),
        .\Dout_reg[26] (CONTROL_n_11),
        .\Dout_reg[27] (DATAPATH_n_99),
        .\Dout_reg[27]_0 (CONTROL_n_10),
        .\Dout_reg[28] (CONTROL_n_9),
        .\Dout_reg[29] (CONTROL_n_8),
        .\Dout_reg[2] (CONTROL_n_19),
        .\Dout_reg[30] (CONTROL_n_7),
        .\Dout_reg[31] ({Instr_out_OBUF[29],Instr_out_OBUF[27],Instr_out_OBUF[25:23],Instr_out_OBUF[21],Instr_out_OBUF[18],Instr_out_OBUF[15:12],Instr_out_OBUF[5:0]}),
        .\Dout_reg[31]_0 (Result_out_OBUF),
        .\Dout_reg[31]_1 (PC_out_OBUF),
        .\Dout_reg[31]_2 (CONTROL_n_21),
        .\Dout_reg[3] (CONTROL_n_18),
        .\Dout_reg[6] (CONTROL_n_17),
        .E(PCWrite_in),
        .Q({PCPlus4_regPCP4[30:25],PCPlus4_regPCP4[23],PCPlus4_regPCP4[20],PCPlus4_regPCP4[15:14],PCPlus4_regPCP4[6],PCPlus4_regPCP4[3:1]}),
        .RegWrite_in(RegWrite_in),
        .SR(RESET_IBUF));
  OBUF \Instr_out_OBUF[0]_inst 
       (.I(Instr_out_OBUF[0]),
        .O(Instr_out[0]));
  OBUF \Instr_out_OBUF[10]_inst 
       (.I(1'b0),
        .O(Instr_out[10]));
  OBUF \Instr_out_OBUF[11]_inst 
       (.I(1'b0),
        .O(Instr_out[11]));
  OBUF \Instr_out_OBUF[12]_inst 
       (.I(Instr_out_OBUF[12]),
        .O(Instr_out[12]));
  OBUF \Instr_out_OBUF[13]_inst 
       (.I(Instr_out_OBUF[13]),
        .O(Instr_out[13]));
  OBUF \Instr_out_OBUF[14]_inst 
       (.I(Instr_out_OBUF[14]),
        .O(Instr_out[14]));
  OBUF \Instr_out_OBUF[15]_inst 
       (.I(Instr_out_OBUF[15]),
        .O(Instr_out[15]));
  OBUF \Instr_out_OBUF[16]_inst 
       (.I(1'b0),
        .O(Instr_out[16]));
  OBUF \Instr_out_OBUF[17]_inst 
       (.I(1'b0),
        .O(Instr_out[17]));
  OBUF \Instr_out_OBUF[18]_inst 
       (.I(Instr_out_OBUF[18]),
        .O(Instr_out[18]));
  OBUF \Instr_out_OBUF[19]_inst 
       (.I(1'b0),
        .O(Instr_out[19]));
  OBUF \Instr_out_OBUF[1]_inst 
       (.I(Instr_out_OBUF[1]),
        .O(Instr_out[1]));
  OBUF \Instr_out_OBUF[20]_inst 
       (.I(1'b0),
        .O(Instr_out[20]));
  OBUF \Instr_out_OBUF[21]_inst 
       (.I(Instr_out_OBUF[21]),
        .O(Instr_out[21]));
  OBUF \Instr_out_OBUF[22]_inst 
       (.I(1'b0),
        .O(Instr_out[22]));
  OBUF \Instr_out_OBUF[23]_inst 
       (.I(Instr_out_OBUF[23]),
        .O(Instr_out[23]));
  OBUF \Instr_out_OBUF[24]_inst 
       (.I(Instr_out_OBUF[24]),
        .O(Instr_out[24]));
  OBUF \Instr_out_OBUF[25]_inst 
       (.I(Instr_out_OBUF[25]),
        .O(Instr_out[25]));
  OBUF \Instr_out_OBUF[26]_inst 
       (.I(1'b0),
        .O(Instr_out[26]));
  OBUF \Instr_out_OBUF[27]_inst 
       (.I(Instr_out_OBUF[27]),
        .O(Instr_out[27]));
  OBUF \Instr_out_OBUF[28]_inst 
       (.I(1'b0),
        .O(Instr_out[28]));
  OBUF \Instr_out_OBUF[29]_inst 
       (.I(Instr_out_OBUF[29]),
        .O(Instr_out[29]));
  OBUF \Instr_out_OBUF[2]_inst 
       (.I(Instr_out_OBUF[2]),
        .O(Instr_out[2]));
  OBUF \Instr_out_OBUF[30]_inst 
       (.I(Instr_out_OBUF[29]),
        .O(Instr_out[30]));
  OBUF \Instr_out_OBUF[31]_inst 
       (.I(Instr_out_OBUF[29]),
        .O(Instr_out[31]));
  OBUF \Instr_out_OBUF[3]_inst 
       (.I(Instr_out_OBUF[3]),
        .O(Instr_out[3]));
  OBUF \Instr_out_OBUF[4]_inst 
       (.I(Instr_out_OBUF[4]),
        .O(Instr_out[4]));
  OBUF \Instr_out_OBUF[5]_inst 
       (.I(Instr_out_OBUF[5]),
        .O(Instr_out[5]));
  OBUF \Instr_out_OBUF[6]_inst 
       (.I(Instr_out_OBUF[5]),
        .O(Instr_out[6]));
  OBUF \Instr_out_OBUF[7]_inst 
       (.I(Instr_out_OBUF[5]),
        .O(Instr_out[7]));
  OBUF \Instr_out_OBUF[8]_inst 
       (.I(Instr_out_OBUF[5]),
        .O(Instr_out[8]));
  OBUF \Instr_out_OBUF[9]_inst 
       (.I(1'b0),
        .O(Instr_out[9]));
  OBUF \PC_out_OBUF[0]_inst 
       (.I(PC_out_OBUF[0]),
        .O(PC_out[0]));
  OBUF \PC_out_OBUF[10]_inst 
       (.I(PC_out_OBUF[10]),
        .O(PC_out[10]));
  OBUF \PC_out_OBUF[11]_inst 
       (.I(PC_out_OBUF[11]),
        .O(PC_out[11]));
  OBUF \PC_out_OBUF[12]_inst 
       (.I(PC_out_OBUF[12]),
        .O(PC_out[12]));
  OBUF \PC_out_OBUF[13]_inst 
       (.I(PC_out_OBUF[13]),
        .O(PC_out[13]));
  OBUF \PC_out_OBUF[14]_inst 
       (.I(PC_out_OBUF[14]),
        .O(PC_out[14]));
  OBUF \PC_out_OBUF[15]_inst 
       (.I(PC_out_OBUF[15]),
        .O(PC_out[15]));
  OBUF \PC_out_OBUF[16]_inst 
       (.I(PC_out_OBUF[16]),
        .O(PC_out[16]));
  OBUF \PC_out_OBUF[17]_inst 
       (.I(PC_out_OBUF[17]),
        .O(PC_out[17]));
  OBUF \PC_out_OBUF[18]_inst 
       (.I(PC_out_OBUF[18]),
        .O(PC_out[18]));
  OBUF \PC_out_OBUF[19]_inst 
       (.I(PC_out_OBUF[19]),
        .O(PC_out[19]));
  OBUF \PC_out_OBUF[1]_inst 
       (.I(PC_out_OBUF[1]),
        .O(PC_out[1]));
  OBUF \PC_out_OBUF[20]_inst 
       (.I(PC_out_OBUF[20]),
        .O(PC_out[20]));
  OBUF \PC_out_OBUF[21]_inst 
       (.I(PC_out_OBUF[21]),
        .O(PC_out[21]));
  OBUF \PC_out_OBUF[22]_inst 
       (.I(PC_out_OBUF[22]),
        .O(PC_out[22]));
  OBUF \PC_out_OBUF[23]_inst 
       (.I(PC_out_OBUF[23]),
        .O(PC_out[23]));
  OBUF \PC_out_OBUF[24]_inst 
       (.I(PC_out_OBUF[24]),
        .O(PC_out[24]));
  OBUF \PC_out_OBUF[25]_inst 
       (.I(PC_out_OBUF[25]),
        .O(PC_out[25]));
  OBUF \PC_out_OBUF[26]_inst 
       (.I(PC_out_OBUF[26]),
        .O(PC_out[26]));
  OBUF \PC_out_OBUF[27]_inst 
       (.I(PC_out_OBUF[27]),
        .O(PC_out[27]));
  OBUF \PC_out_OBUF[28]_inst 
       (.I(PC_out_OBUF[28]),
        .O(PC_out[28]));
  OBUF \PC_out_OBUF[29]_inst 
       (.I(PC_out_OBUF[29]),
        .O(PC_out[29]));
  OBUF \PC_out_OBUF[2]_inst 
       (.I(PC_out_OBUF[2]),
        .O(PC_out[2]));
  OBUF \PC_out_OBUF[30]_inst 
       (.I(PC_out_OBUF[30]),
        .O(PC_out[30]));
  OBUF \PC_out_OBUF[31]_inst 
       (.I(PC_out_OBUF[31]),
        .O(PC_out[31]));
  OBUF \PC_out_OBUF[3]_inst 
       (.I(PC_out_OBUF[3]),
        .O(PC_out[3]));
  OBUF \PC_out_OBUF[4]_inst 
       (.I(PC_out_OBUF[4]),
        .O(PC_out[4]));
  OBUF \PC_out_OBUF[5]_inst 
       (.I(PC_out_OBUF[5]),
        .O(PC_out[5]));
  OBUF \PC_out_OBUF[6]_inst 
       (.I(PC_out_OBUF[6]),
        .O(PC_out[6]));
  OBUF \PC_out_OBUF[7]_inst 
       (.I(PC_out_OBUF[7]),
        .O(PC_out[7]));
  OBUF \PC_out_OBUF[8]_inst 
       (.I(PC_out_OBUF[8]),
        .O(PC_out[8]));
  OBUF \PC_out_OBUF[9]_inst 
       (.I(PC_out_OBUF[9]),
        .O(PC_out[9]));
  IBUF RESET_IBUF_inst
       (.I(RESET),
        .O(RESET_IBUF));
  OBUF \Result_out_OBUF[0]_inst 
       (.I(Result_out_OBUF[0]),
        .O(Result_out[0]));
  OBUF \Result_out_OBUF[10]_inst 
       (.I(Result_out_OBUF[10]),
        .O(Result_out[10]));
  OBUF \Result_out_OBUF[11]_inst 
       (.I(Result_out_OBUF[11]),
        .O(Result_out[11]));
  OBUF \Result_out_OBUF[12]_inst 
       (.I(Result_out_OBUF[12]),
        .O(Result_out[12]));
  OBUF \Result_out_OBUF[13]_inst 
       (.I(Result_out_OBUF[13]),
        .O(Result_out[13]));
  OBUF \Result_out_OBUF[14]_inst 
       (.I(Result_out_OBUF[14]),
        .O(Result_out[14]));
  OBUF \Result_out_OBUF[15]_inst 
       (.I(Result_out_OBUF[15]),
        .O(Result_out[15]));
  OBUF \Result_out_OBUF[16]_inst 
       (.I(Result_out_OBUF[16]),
        .O(Result_out[16]));
  OBUF \Result_out_OBUF[17]_inst 
       (.I(Result_out_OBUF[17]),
        .O(Result_out[17]));
  OBUF \Result_out_OBUF[18]_inst 
       (.I(Result_out_OBUF[18]),
        .O(Result_out[18]));
  OBUF \Result_out_OBUF[19]_inst 
       (.I(Result_out_OBUF[19]),
        .O(Result_out[19]));
  OBUF \Result_out_OBUF[1]_inst 
       (.I(Result_out_OBUF[1]),
        .O(Result_out[1]));
  OBUF \Result_out_OBUF[20]_inst 
       (.I(Result_out_OBUF[20]),
        .O(Result_out[20]));
  OBUF \Result_out_OBUF[21]_inst 
       (.I(Result_out_OBUF[21]),
        .O(Result_out[21]));
  OBUF \Result_out_OBUF[22]_inst 
       (.I(Result_out_OBUF[22]),
        .O(Result_out[22]));
  OBUF \Result_out_OBUF[23]_inst 
       (.I(Result_out_OBUF[23]),
        .O(Result_out[23]));
  OBUF \Result_out_OBUF[24]_inst 
       (.I(Result_out_OBUF[24]),
        .O(Result_out[24]));
  OBUF \Result_out_OBUF[25]_inst 
       (.I(Result_out_OBUF[25]),
        .O(Result_out[25]));
  OBUF \Result_out_OBUF[26]_inst 
       (.I(Result_out_OBUF[26]),
        .O(Result_out[26]));
  OBUF \Result_out_OBUF[27]_inst 
       (.I(Result_out_OBUF[27]),
        .O(Result_out[27]));
  OBUF \Result_out_OBUF[28]_inst 
       (.I(Result_out_OBUF[28]),
        .O(Result_out[28]));
  OBUF \Result_out_OBUF[29]_inst 
       (.I(Result_out_OBUF[29]),
        .O(Result_out[29]));
  OBUF \Result_out_OBUF[2]_inst 
       (.I(Result_out_OBUF[2]),
        .O(Result_out[2]));
  OBUF \Result_out_OBUF[30]_inst 
       (.I(Result_out_OBUF[30]),
        .O(Result_out[30]));
  OBUF \Result_out_OBUF[31]_inst 
       (.I(Result_out_OBUF[31]),
        .O(Result_out[31]));
  OBUF \Result_out_OBUF[3]_inst 
       (.I(Result_out_OBUF[3]),
        .O(Result_out[3]));
  OBUF \Result_out_OBUF[4]_inst 
       (.I(Result_out_OBUF[4]),
        .O(Result_out[4]));
  OBUF \Result_out_OBUF[5]_inst 
       (.I(Result_out_OBUF[5]),
        .O(Result_out[5]));
  OBUF \Result_out_OBUF[6]_inst 
       (.I(Result_out_OBUF[6]),
        .O(Result_out[6]));
  OBUF \Result_out_OBUF[7]_inst 
       (.I(Result_out_OBUF[7]),
        .O(Result_out[7]));
  OBUF \Result_out_OBUF[8]_inst 
       (.I(Result_out_OBUF[8]),
        .O(Result_out[8]));
  OBUF \Result_out_OBUF[9]_inst 
       (.I(Result_out_OBUF[9]),
        .O(Result_out[9]));
  OBUF \WriteData_out_OBUF[0]_inst 
       (.I(WriteData_out_OBUF[0]),
        .O(WriteData_out[0]));
  OBUF \WriteData_out_OBUF[10]_inst 
       (.I(WriteData_out_OBUF[10]),
        .O(WriteData_out[10]));
  OBUF \WriteData_out_OBUF[11]_inst 
       (.I(WriteData_out_OBUF[11]),
        .O(WriteData_out[11]));
  OBUF \WriteData_out_OBUF[12]_inst 
       (.I(WriteData_out_OBUF[12]),
        .O(WriteData_out[12]));
  OBUF \WriteData_out_OBUF[13]_inst 
       (.I(WriteData_out_OBUF[13]),
        .O(WriteData_out[13]));
  OBUF \WriteData_out_OBUF[14]_inst 
       (.I(WriteData_out_OBUF[14]),
        .O(WriteData_out[14]));
  OBUF \WriteData_out_OBUF[15]_inst 
       (.I(WriteData_out_OBUF[15]),
        .O(WriteData_out[15]));
  OBUF \WriteData_out_OBUF[16]_inst 
       (.I(WriteData_out_OBUF[16]),
        .O(WriteData_out[16]));
  OBUF \WriteData_out_OBUF[17]_inst 
       (.I(WriteData_out_OBUF[17]),
        .O(WriteData_out[17]));
  OBUF \WriteData_out_OBUF[18]_inst 
       (.I(WriteData_out_OBUF[18]),
        .O(WriteData_out[18]));
  OBUF \WriteData_out_OBUF[19]_inst 
       (.I(WriteData_out_OBUF[19]),
        .O(WriteData_out[19]));
  OBUF \WriteData_out_OBUF[1]_inst 
       (.I(WriteData_out_OBUF[1]),
        .O(WriteData_out[1]));
  OBUF \WriteData_out_OBUF[20]_inst 
       (.I(WriteData_out_OBUF[20]),
        .O(WriteData_out[20]));
  OBUF \WriteData_out_OBUF[21]_inst 
       (.I(WriteData_out_OBUF[21]),
        .O(WriteData_out[21]));
  OBUF \WriteData_out_OBUF[22]_inst 
       (.I(WriteData_out_OBUF[22]),
        .O(WriteData_out[22]));
  OBUF \WriteData_out_OBUF[23]_inst 
       (.I(WriteData_out_OBUF[23]),
        .O(WriteData_out[23]));
  OBUF \WriteData_out_OBUF[24]_inst 
       (.I(WriteData_out_OBUF[24]),
        .O(WriteData_out[24]));
  OBUF \WriteData_out_OBUF[25]_inst 
       (.I(WriteData_out_OBUF[25]),
        .O(WriteData_out[25]));
  OBUF \WriteData_out_OBUF[26]_inst 
       (.I(WriteData_out_OBUF[26]),
        .O(WriteData_out[26]));
  OBUF \WriteData_out_OBUF[27]_inst 
       (.I(WriteData_out_OBUF[27]),
        .O(WriteData_out[27]));
  OBUF \WriteData_out_OBUF[28]_inst 
       (.I(WriteData_out_OBUF[28]),
        .O(WriteData_out[28]));
  OBUF \WriteData_out_OBUF[29]_inst 
       (.I(WriteData_out_OBUF[29]),
        .O(WriteData_out[29]));
  OBUF \WriteData_out_OBUF[2]_inst 
       (.I(WriteData_out_OBUF[2]),
        .O(WriteData_out[2]));
  OBUF \WriteData_out_OBUF[30]_inst 
       (.I(WriteData_out_OBUF[30]),
        .O(WriteData_out[30]));
  OBUF \WriteData_out_OBUF[31]_inst 
       (.I(WriteData_out_OBUF[31]),
        .O(WriteData_out[31]));
  OBUF \WriteData_out_OBUF[3]_inst 
       (.I(WriteData_out_OBUF[3]),
        .O(WriteData_out[3]));
  OBUF \WriteData_out_OBUF[4]_inst 
       (.I(WriteData_out_OBUF[4]),
        .O(WriteData_out[4]));
  OBUF \WriteData_out_OBUF[5]_inst 
       (.I(WriteData_out_OBUF[5]),
        .O(WriteData_out[5]));
  OBUF \WriteData_out_OBUF[6]_inst 
       (.I(WriteData_out_OBUF[6]),
        .O(WriteData_out[6]));
  OBUF \WriteData_out_OBUF[7]_inst 
       (.I(WriteData_out_OBUF[7]),
        .O(WriteData_out[7]));
  OBUF \WriteData_out_OBUF[8]_inst 
       (.I(WriteData_out_OBUF[8]),
        .O(WriteData_out[8]));
  OBUF \WriteData_out_OBUF[9]_inst 
       (.I(WriteData_out_OBUF[9]),
        .O(WriteData_out[9]));
endmodule

module CONTROL_UNIT
   (RegWrite_in,
    Q,
    E,
    \Dout_reg[30] ,
    \Dout_reg[29] ,
    \Dout_reg[28] ,
    \Dout_reg[27] ,
    \Dout_reg[26] ,
    \Dout_reg[25] ,
    \Dout_reg[23] ,
    \Dout_reg[20] ,
    \Dout_reg[15] ,
    \Dout_reg[14] ,
    \Dout_reg[6] ,
    \Dout_reg[3] ,
    \Dout_reg[2] ,
    \Dout_reg[1] ,
    \FSM_onehot_current_state_reg[12] ,
    \FSM_onehot_current_state_reg[0] ,
    \FSM_onehot_current_state_reg[2] ,
    \Dout[30]_i_4 ,
    \Dout[30]_i_4_0 ,
    \FSM_onehot_current_state_reg[8] ,
    SR,
    D,
    CLK);
  output RegWrite_in;
  output [4:0]Q;
  output [0:0]E;
  output \Dout_reg[30] ;
  output \Dout_reg[29] ;
  output \Dout_reg[28] ;
  output \Dout_reg[27] ;
  output \Dout_reg[26] ;
  output \Dout_reg[25] ;
  output \Dout_reg[23] ;
  output \Dout_reg[20] ;
  output \Dout_reg[15] ;
  output \Dout_reg[14] ;
  output \Dout_reg[6] ;
  output \Dout_reg[3] ;
  output \Dout_reg[2] ;
  output \Dout_reg[1] ;
  output \FSM_onehot_current_state_reg[12] ;
  input \FSM_onehot_current_state_reg[0] ;
  input [1:0]\FSM_onehot_current_state_reg[2] ;
  input [13:0]\Dout[30]_i_4 ;
  input [13:0]\Dout[30]_i_4_0 ;
  input \FSM_onehot_current_state_reg[8] ;
  input [0:0]SR;
  input [2:0]D;
  input CLK;

  wire CLK;
  wire [2:0]D;
  wire [13:0]\Dout[30]_i_4 ;
  wire [13:0]\Dout[30]_i_4_0 ;
  wire \Dout_reg[14] ;
  wire \Dout_reg[15] ;
  wire \Dout_reg[1] ;
  wire \Dout_reg[20] ;
  wire \Dout_reg[23] ;
  wire \Dout_reg[25] ;
  wire \Dout_reg[26] ;
  wire \Dout_reg[27] ;
  wire \Dout_reg[28] ;
  wire \Dout_reg[29] ;
  wire \Dout_reg[2] ;
  wire \Dout_reg[30] ;
  wire \Dout_reg[3] ;
  wire \Dout_reg[6] ;
  wire [0:0]E;
  wire \FSM_onehot_current_state_reg[0] ;
  wire \FSM_onehot_current_state_reg[12] ;
  wire [1:0]\FSM_onehot_current_state_reg[2] ;
  wire \FSM_onehot_current_state_reg[8] ;
  wire [4:0]Q;
  wire RegWrite_in;
  wire [0:0]SR;

  FSM FSM_UNIT
       (.CLK(CLK),
        .D(D),
        .\Dout[30]_i_4 (\Dout[30]_i_4 ),
        .\Dout[30]_i_4_0 (\Dout[30]_i_4_0 ),
        .\Dout_reg[14] (\Dout_reg[14] ),
        .\Dout_reg[15] (\Dout_reg[15] ),
        .\Dout_reg[1] (\Dout_reg[1] ),
        .\Dout_reg[20] (\Dout_reg[20] ),
        .\Dout_reg[23] (\Dout_reg[23] ),
        .\Dout_reg[25] (\Dout_reg[25] ),
        .\Dout_reg[26] (\Dout_reg[26] ),
        .\Dout_reg[27] (\Dout_reg[27] ),
        .\Dout_reg[28] (\Dout_reg[28] ),
        .\Dout_reg[29] (\Dout_reg[29] ),
        .\Dout_reg[2] (\Dout_reg[2] ),
        .\Dout_reg[30] (\Dout_reg[30] ),
        .\Dout_reg[3] (\Dout_reg[3] ),
        .\Dout_reg[6] (\Dout_reg[6] ),
        .E(E),
        .\FSM_onehot_current_state_reg[0]_0 (\FSM_onehot_current_state_reg[0] ),
        .\FSM_onehot_current_state_reg[12]_0 (\FSM_onehot_current_state_reg[12] ),
        .\FSM_onehot_current_state_reg[2]_0 (\FSM_onehot_current_state_reg[2] ),
        .\FSM_onehot_current_state_reg[8]_0 (\FSM_onehot_current_state_reg[8] ),
        .Q(Q),
        .RegWrite_in(RegWrite_in),
        .SR(SR));
endmodule

module DATAPATH_MC_n
   (Q,
    \Dout_reg[31] ,
    ALUControl_out_OBUF,
    D,
    \Dout_reg[31]_0 ,
    \Dout_reg[27] ,
    \Dout_reg[0] ,
    \Dout_reg[24] ,
    \Dout_reg[12] ,
    \Dout_reg[31]_1 ,
    \Dout_reg[31]_2 ,
    \Dout_reg[13] ,
    \Dout_reg[1] ,
    \Dout_reg[2] ,
    \Dout_reg[3] ,
    \Dout_reg[6] ,
    \Dout_reg[14] ,
    \Dout_reg[15] ,
    \Dout_reg[23] ,
    \Dout_reg[29] ,
    \Dout_reg[25] ,
    \Dout_reg[20] ,
    \Dout_reg[26] ,
    \Dout_reg[27]_0 ,
    \Dout_reg[28] ,
    \Dout_reg[30] ,
    CLK,
    RegWrite_in,
    SR,
    E);
  output [13:0]Q;
  output [16:0]\Dout_reg[31] ;
  output [3:0]ALUControl_out_OBUF;
  output [31:0]D;
  output [31:0]\Dout_reg[31]_0 ;
  output \Dout_reg[27] ;
  output [31:0]\Dout_reg[0] ;
  output [2:0]\Dout_reg[24] ;
  output \Dout_reg[12] ;
  output [31:0]\Dout_reg[31]_1 ;
  input \Dout_reg[31]_2 ;
  input [4:0]\Dout_reg[13] ;
  input \Dout_reg[1] ;
  input \Dout_reg[2] ;
  input \Dout_reg[3] ;
  input \Dout_reg[6] ;
  input \Dout_reg[14] ;
  input \Dout_reg[15] ;
  input \Dout_reg[23] ;
  input \Dout_reg[29] ;
  input \Dout_reg[25] ;
  input \Dout_reg[20] ;
  input \Dout_reg[26] ;
  input \Dout_reg[27]_0 ;
  input \Dout_reg[28] ;
  input \Dout_reg[30] ;
  input CLK;
  input RegWrite_in;
  input [0:0]SR;
  input [0:0]E;

  wire [31:0]\ADD_SUB/S_Add_in ;
  wire [3:0]ALUControl_out_OBUF;
  wire CLK;
  wire [31:0]D;
  wire [31:0]\Dout_reg[0] ;
  wire \Dout_reg[12] ;
  wire [4:0]\Dout_reg[13] ;
  wire \Dout_reg[14] ;
  wire \Dout_reg[15] ;
  wire \Dout_reg[1] ;
  wire \Dout_reg[20] ;
  wire \Dout_reg[23] ;
  wire [2:0]\Dout_reg[24] ;
  wire \Dout_reg[25] ;
  wire \Dout_reg[26] ;
  wire \Dout_reg[27] ;
  wire \Dout_reg[27]_0 ;
  wire \Dout_reg[28] ;
  wire \Dout_reg[29] ;
  wire \Dout_reg[2] ;
  wire \Dout_reg[30] ;
  wire [16:0]\Dout_reg[31] ;
  wire [31:0]\Dout_reg[31]_0 ;
  wire [31:0]\Dout_reg[31]_1 ;
  wire \Dout_reg[31]_2 ;
  wire \Dout_reg[3] ;
  wire \Dout_reg[6] ;
  wire [0:0]E;
  wire [31:0]ExtImm;
  wire [31:0]ExtImm_regI;
  wire INSTR_MEMORY_n_15;
  wire INSTR_MEMORY_n_16;
  wire [31:2]PCPlus4;
  wire [31:0]PCPlus4_regPCP4;
  wire [31:3]PCPlus8;
  wire PC_REG_n_0;
  wire PC_REG_n_1;
  wire [31:0]PC_next;
  wire [13:0]Q;
  wire [2:2]RA1_in;
  wire [3:0]RA3_in;
  wire REG_A_n_0;
  wire REG_A_n_33;
  wire REG_B_n_0;
  wire REG_B_n_33;
  wire REG_B_n_34;
  wire REG_B_n_35;
  wire REG_B_n_36;
  wire REG_B_n_37;
  wire REG_B_n_38;
  wire REG_B_n_39;
  wire REG_FILE_n_0;
  wire REG_FILE_n_1;
  wire REG_FILE_n_10;
  wire REG_FILE_n_11;
  wire REG_FILE_n_12;
  wire REG_FILE_n_13;
  wire REG_FILE_n_14;
  wire REG_FILE_n_15;
  wire REG_FILE_n_16;
  wire REG_FILE_n_17;
  wire REG_FILE_n_18;
  wire REG_FILE_n_19;
  wire REG_FILE_n_2;
  wire REG_FILE_n_20;
  wire REG_FILE_n_21;
  wire REG_FILE_n_22;
  wire REG_FILE_n_23;
  wire REG_FILE_n_24;
  wire REG_FILE_n_25;
  wire REG_FILE_n_26;
  wire REG_FILE_n_27;
  wire REG_FILE_n_28;
  wire REG_FILE_n_29;
  wire REG_FILE_n_3;
  wire REG_FILE_n_30;
  wire REG_FILE_n_31;
  wire REG_FILE_n_32;
  wire REG_FILE_n_33;
  wire REG_FILE_n_34;
  wire REG_FILE_n_35;
  wire REG_FILE_n_36;
  wire REG_FILE_n_37;
  wire REG_FILE_n_38;
  wire REG_FILE_n_39;
  wire REG_FILE_n_4;
  wire REG_FILE_n_40;
  wire REG_FILE_n_41;
  wire REG_FILE_n_42;
  wire REG_FILE_n_43;
  wire REG_FILE_n_44;
  wire REG_FILE_n_45;
  wire REG_FILE_n_46;
  wire REG_FILE_n_47;
  wire REG_FILE_n_48;
  wire REG_FILE_n_49;
  wire REG_FILE_n_5;
  wire REG_FILE_n_50;
  wire REG_FILE_n_51;
  wire REG_FILE_n_52;
  wire REG_FILE_n_53;
  wire REG_FILE_n_54;
  wire REG_FILE_n_55;
  wire REG_FILE_n_56;
  wire REG_FILE_n_57;
  wire REG_FILE_n_58;
  wire REG_FILE_n_59;
  wire REG_FILE_n_6;
  wire REG_FILE_n_60;
  wire REG_FILE_n_61;
  wire REG_FILE_n_62;
  wire REG_FILE_n_63;
  wire REG_FILE_n_7;
  wire REG_FILE_n_8;
  wire REG_FILE_n_9;
  wire REG_IR_n_0;
  wire REG_IR_n_145;
  wire REG_IR_n_146;
  wire REG_IR_n_147;
  wire REG_IR_n_148;
  wire REG_IR_n_149;
  wire REG_IR_n_150;
  wire REG_IR_n_151;
  wire REG_IR_n_152;
  wire REG_IR_n_153;
  wire REG_IR_n_154;
  wire REG_IR_n_155;
  wire REG_IR_n_156;
  wire REG_IR_n_157;
  wire REG_IR_n_158;
  wire REG_IR_n_159;
  wire REG_IR_n_160;
  wire REG_IR_n_161;
  wire REG_IR_n_162;
  wire REG_IR_n_163;
  wire REG_IR_n_164;
  wire REG_IR_n_165;
  wire REG_IR_n_166;
  wire REG_IR_n_167;
  wire REG_IR_n_168;
  wire REG_IR_n_169;
  wire REG_IR_n_170;
  wire REG_IR_n_171;
  wire REG_IR_n_172;
  wire REG_IR_n_173;
  wire REG_IR_n_174;
  wire REG_IR_n_175;
  wire REG_IR_n_176;
  wire REG_IR_n_84;
  wire REG_IR_n_85;
  wire REG_IR_n_86;
  wire REG_IR_n_87;
  wire REG_IR_n_88;
  wire REG_I_n_0;
  wire REG_I_n_17;
  wire REG_S_n_35;
  wire REG_S_n_36;
  wire [31:1]\ROM[0]_0 ;
  wire RegWrite_in;
  wire [0:0]SR;
  wire [31:0]SrcA;
  wire [31:0]SrcA_regA;
  wire [31:0]WD3_in;
  wire [31:0]WriteData_regB;

  ALU_n ALU
       (.\ALUResult_out_OBUF[16]_inst_i_2 ({REG_IR_n_165,REG_IR_n_166,REG_IR_n_167,REG_IR_n_168}),
        .\ALUResult_out_OBUF[24]_inst_i_2 ({REG_IR_n_173,REG_IR_n_174,REG_IR_n_175,REG_IR_n_176}),
        .\ALUResult_out_OBUF[4]_inst_i_2 ({REG_IR_n_153,REG_IR_n_154,REG_IR_n_155,REG_IR_n_156}),
        .\Dout[28]_i_4 ({REG_IR_n_145,REG_IR_n_146,REG_IR_n_147,REG_IR_n_148}),
        .\Dout_reg[11] ({REG_IR_n_157,REG_IR_n_158,REG_IR_n_159,REG_IR_n_160}),
        .\Dout_reg[13] ({REG_IR_n_161,REG_IR_n_162,REG_IR_n_163,REG_IR_n_164}),
        .\Dout_reg[23] ({REG_IR_n_169,REG_IR_n_170,REG_IR_n_171,REG_IR_n_172}),
        .Q(SrcA_regA[30:0]),
        .S({REG_IR_n_149,REG_IR_n_150,REG_IR_n_151,REG_IR_n_152}),
        .S_Add_in(\ADD_SUB/S_Add_in ));
  ADDER_n INC_4_1
       (.D(SrcA[31:3]),
        .DATA_OUT1({REG_FILE_n_0,REG_FILE_n_1,REG_FILE_n_2,REG_FILE_n_3,REG_FILE_n_4,REG_FILE_n_5,REG_FILE_n_6,REG_FILE_n_7,REG_FILE_n_8,REG_FILE_n_9,REG_FILE_n_10,REG_FILE_n_11,REG_FILE_n_12,REG_FILE_n_13,REG_FILE_n_14,REG_FILE_n_15,REG_FILE_n_16,REG_FILE_n_17,REG_FILE_n_18,REG_FILE_n_19,REG_FILE_n_20,REG_FILE_n_21,REG_FILE_n_22,REG_FILE_n_23,REG_FILE_n_24,REG_FILE_n_25,REG_FILE_n_26,REG_FILE_n_27,REG_FILE_n_28}),
        .\Dout_reg[31] (REG_IR_n_88),
        .PCPlus8(PCPlus8),
        .Q({PCPlus4_regPCP4[31],Q[13:8],PCPlus4_regPCP4[24],Q[7],PCPlus4_regPCP4[22:21],Q[6],PCPlus4_regPCP4[19:16],Q[5:4],PCPlus4_regPCP4[13:7],Q[3],PCPlus4_regPCP4[5:4],Q[2:1]}));
  ROM_ARRAY INSTR_MEMORY
       (.D({\ROM[0]_0 [31],\ROM[0]_0 [27],\ROM[0]_0 [24:23],\ROM[0]_0 [21],\ROM[0]_0 [18],\ROM[0]_0 [15:12],\ROM[0]_0 [6],\ROM[0]_0 [4:1]}),
        .\Dout_reg[5] (INSTR_MEMORY_n_15),
        .\Dout_reg[5]_0 (INSTR_MEMORY_n_16),
        .Q(\Dout_reg[31]_1 [7:2]));
  REGrwe_n PC_REG
       (.CLK(CLK),
        .D({PC_REG_n_0,PC_REG_n_1}),
        .\Dout_reg[31]_0 (PCPlus4),
        .\Dout_reg[31]_1 (PC_next),
        .E(E),
        .Q(\Dout_reg[31]_1 ),
        .SR(SR));
  REGrwe_n_0 REG_A
       (.CLK(CLK),
        .D(SrcA),
        .\Dout[21]_i_3 ({WriteData_regB[21],WriteData_regB[18]}),
        .\Dout_reg[18]_0 (REG_A_n_33),
        .\Dout_reg[21]_0 (REG_A_n_0),
        .Q(SrcA_regA),
        .SR(SR));
  REGrwe_n_1 REG_B
       (.CLK(CLK),
        .\Dout_reg[15]_0 (REG_B_n_36),
        .\Dout_reg[26]_0 (REG_B_n_37),
        .\Dout_reg[26]_1 (REG_IR_n_86),
        .\Dout_reg[28]_0 (REG_B_n_38),
        .\Dout_reg[2]_0 (REG_B_n_0),
        .\Dout_reg[2]_1 (REG_IR_n_85),
        .\Dout_reg[2]_2 (REG_IR_n_84),
        .\Dout_reg[30]_0 (REG_B_n_39),
        .\Dout_reg[30]_1 ({SrcA_regA[30],SrcA_regA[28],SrcA_regA[26],SrcA_regA[15],SrcA_regA[6:5],SrcA_regA[3:2]}),
        .\Dout_reg[31]_0 (\Dout_reg[0] ),
        .\Dout_reg[3]_0 (REG_B_n_33),
        .\Dout_reg[5]_0 (REG_B_n_34),
        .\Dout_reg[6]_0 (REG_B_n_35),
        .Q(WriteData_regB),
        .SR(SR));
  REGISTER_FILE_wR15 REG_FILE
       (.ADDRD(RA3_in),
        .ADDR_R1({REG_IR_n_0,RA1_in}),
        .CLK(CLK),
        .DATA_IN(WD3_in),
        .DATA_OUT1({REG_FILE_n_0,REG_FILE_n_1,REG_FILE_n_2,REG_FILE_n_3,REG_FILE_n_4,REG_FILE_n_5,REG_FILE_n_6,REG_FILE_n_7,REG_FILE_n_8,REG_FILE_n_9,REG_FILE_n_10,REG_FILE_n_11,REG_FILE_n_12,REG_FILE_n_13,REG_FILE_n_14,REG_FILE_n_15,REG_FILE_n_16,REG_FILE_n_17,REG_FILE_n_18,REG_FILE_n_19,REG_FILE_n_20,REG_FILE_n_21,REG_FILE_n_22,REG_FILE_n_23,REG_FILE_n_24,REG_FILE_n_25,REG_FILE_n_26,REG_FILE_n_27,REG_FILE_n_28,REG_FILE_n_29,REG_FILE_n_30,REG_FILE_n_31}),
        .DATA_OUT2({REG_FILE_n_32,REG_FILE_n_33,REG_FILE_n_34,REG_FILE_n_35,REG_FILE_n_36,REG_FILE_n_37,REG_FILE_n_38,REG_FILE_n_39,REG_FILE_n_40,REG_FILE_n_41,REG_FILE_n_42,REG_FILE_n_43,REG_FILE_n_44,REG_FILE_n_45,REG_FILE_n_46,REG_FILE_n_47,REG_FILE_n_48,REG_FILE_n_49,REG_FILE_n_50,REG_FILE_n_51,REG_FILE_n_52,REG_FILE_n_53,REG_FILE_n_54,REG_FILE_n_55,REG_FILE_n_56,REG_FILE_n_57,REG_FILE_n_58,REG_FILE_n_59,REG_FILE_n_60,REG_FILE_n_61,REG_FILE_n_62,REG_FILE_n_63}),
        .Q(\Dout_reg[31] [3:0]),
        .RegWrite_in(RegWrite_in));
  REGrwe_n_2 REG_I
       (.CLK(CLK),
        .D({ExtImm[31],ExtImm[23],ExtImm[20],ExtImm[17:14],ExtImm[10],\Dout_reg[31] [5],ExtImm[6:0]}),
        .\Dout_reg[16]_0 (REG_I_n_0),
        .\Dout_reg[16]_1 ({SrcA_regA[16],SrcA_regA[4]}),
        .\Dout_reg[4]_0 (REG_I_n_17),
        .Q({ExtImm_regI[31],ExtImm_regI[23],ExtImm_regI[20],ExtImm_regI[17:14],ExtImm_regI[10],ExtImm_regI[8],ExtImm_regI[6:0]}),
        .SR(SR));
  REGrwe_n_3 REG_IR
       (.ADDRD(RA3_in),
        .ADDR_R1({REG_IR_n_0,RA1_in}),
        .ALUControl_out_OBUF(ALUControl_out_OBUF),
        .CLK(CLK),
        .D(D),
        .DATA_OUT2({REG_FILE_n_32,REG_FILE_n_33,REG_FILE_n_34,REG_FILE_n_35,REG_FILE_n_36,REG_FILE_n_37,REG_FILE_n_38,REG_FILE_n_39,REG_FILE_n_40,REG_FILE_n_41,REG_FILE_n_42,REG_FILE_n_43,REG_FILE_n_44,REG_FILE_n_45,REG_FILE_n_46,REG_FILE_n_47,REG_FILE_n_48,REG_FILE_n_49,REG_FILE_n_50,REG_FILE_n_51,REG_FILE_n_52,REG_FILE_n_53,REG_FILE_n_54,REG_FILE_n_55,REG_FILE_n_56,REG_FILE_n_57,REG_FILE_n_58,REG_FILE_n_59,REG_FILE_n_60,REG_FILE_n_61,REG_FILE_n_62,REG_FILE_n_63}),
        .\Dout_reg[0]_0 (\Dout_reg[0] ),
        .\Dout_reg[11] ({REG_IR_n_157,REG_IR_n_158,REG_IR_n_159,REG_IR_n_160}),
        .\Dout_reg[12]_0 (\Dout_reg[12] ),
        .\Dout_reg[14]_0 (\Dout_reg[14] ),
        .\Dout_reg[15]_0 ({REG_IR_n_161,REG_IR_n_162,REG_IR_n_163,REG_IR_n_164}),
        .\Dout_reg[15]_1 (\Dout_reg[15] ),
        .\Dout_reg[15]_2 (REG_B_n_36),
        .\Dout_reg[16] (REG_I_n_0),
        .\Dout_reg[18]_0 (REG_S_n_36),
        .\Dout_reg[19] ({REG_IR_n_165,REG_IR_n_166,REG_IR_n_167,REG_IR_n_168}),
        .\Dout_reg[1]_0 (\Dout_reg[1] ),
        .\Dout_reg[20] (\Dout_reg[20] ),
        .\Dout_reg[21]_0 (\Dout_reg[13] ),
        .\Dout_reg[21]_1 (REG_S_n_35),
        .\Dout_reg[23]_0 (REG_IR_n_85),
        .\Dout_reg[23]_1 (REG_IR_n_87),
        .\Dout_reg[23]_2 ({REG_IR_n_169,REG_IR_n_170,REG_IR_n_171,REG_IR_n_172}),
        .\Dout_reg[23]_3 (\Dout_reg[23] ),
        .\Dout_reg[24]_0 (REG_IR_n_84),
        .\Dout_reg[24]_1 (REG_IR_n_86),
        .\Dout_reg[24]_2 (\Dout_reg[24] ),
        .\Dout_reg[25]_0 (\Dout_reg[25] ),
        .\Dout_reg[26] ({ExtImm_regI[31],ExtImm_regI[23],ExtImm_regI[20],ExtImm_regI[17:14],ExtImm_regI[10],ExtImm_regI[8],ExtImm_regI[6:0]}),
        .\Dout_reg[26]_0 (REG_B_n_37),
        .\Dout_reg[26]_1 (\Dout_reg[26] ),
        .\Dout_reg[27]_0 (\Dout_reg[27] ),
        .\Dout_reg[27]_1 ({REG_IR_n_173,REG_IR_n_174,REG_IR_n_175,REG_IR_n_176}),
        .\Dout_reg[27]_2 (\Dout_reg[27]_0 ),
        .\Dout_reg[27]_rep_0 (REG_IR_n_88),
        .\Dout_reg[27]_rep_1 ({ExtImm[31],ExtImm[23],ExtImm[20],ExtImm[17:14],ExtImm[10],ExtImm[6:0]}),
        .\Dout_reg[27]_rep_2 (INSTR_MEMORY_n_15),
        .\Dout_reg[27]_rep__0_0 (INSTR_MEMORY_n_16),
        .\Dout_reg[28] (REG_B_n_38),
        .\Dout_reg[28]_0 (\Dout_reg[28] ),
        .\Dout_reg[29] (\Dout_reg[29] ),
        .\Dout_reg[2]_0 (REG_B_n_0),
        .\Dout_reg[2]_1 (\Dout_reg[2] ),
        .\Dout_reg[30] (REG_B_n_39),
        .\Dout_reg[30]_0 (\Dout_reg[30] ),
        .\Dout_reg[31]_0 (\Dout_reg[31] ),
        .\Dout_reg[31]_1 ({PC_next[31:14],PC_next[10:0]}),
        .\Dout_reg[31]_2 ({REG_IR_n_145,REG_IR_n_146,REG_IR_n_147,REG_IR_n_148}),
        .\Dout_reg[31]_3 (\Dout_reg[31]_2 ),
        .\Dout_reg[31]_4 (WriteData_regB),
        .\Dout_reg[31]_5 ({\Dout_reg[31]_0 [31],\Dout_reg[31]_0 [24],\Dout_reg[31]_0 [22],\Dout_reg[31]_0 [19],\Dout_reg[31]_0 [17:16],\Dout_reg[31]_0 [10:7],\Dout_reg[31]_0 [5:4],\Dout_reg[31]_0 [0]}),
        .\Dout_reg[31]_6 ({PCPlus4_regPCP4[31],PCPlus4_regPCP4[24],PCPlus4_regPCP4[22],PCPlus4_regPCP4[19],PCPlus4_regPCP4[17:16],PCPlus4_regPCP4[10:7],PCPlus4_regPCP4[5:4],Q[1:0],PCPlus4_regPCP4[0]}),
        .\Dout_reg[31]_7 ({\ROM[0]_0 [31],\ROM[0]_0 [27],PC_REG_n_0,\ROM[0]_0 [24:23],\ROM[0]_0 [21],\ROM[0]_0 [18],\ROM[0]_0 [15:12],\ROM[0]_0 [6],\ROM[0]_0 [4:1],PC_REG_n_1}),
        .\Dout_reg[3]_0 (\Dout_reg[3] ),
        .\Dout_reg[3]_1 (REG_B_n_33),
        .\Dout_reg[4]_0 (REG_I_n_17),
        .\Dout_reg[5] (REG_B_n_34),
        .\Dout_reg[6]_0 (\Dout_reg[6] ),
        .\Dout_reg[6]_1 (REG_B_n_35),
        .\Dout_reg[7] ({REG_IR_n_153,REG_IR_n_154,REG_IR_n_155,REG_IR_n_156}),
        .PCPlus8(PCPlus8),
        .Q(SrcA_regA),
        .S({REG_IR_n_149,REG_IR_n_150,REG_IR_n_151,REG_IR_n_152}),
        .SR(SR),
        .S_Add_in(\ADD_SUB/S_Add_in ));
  REGrwe_n_4 REG_PCP4
       (.ADDR_R1(REG_IR_n_0),
        .CLK(CLK),
        .D(SrcA[2:0]),
        .DATA_IN(WD3_in),
        .DATA_OUT1({REG_FILE_n_29,REG_FILE_n_30,REG_FILE_n_31}),
        .\Dout_reg[2]_0 (REG_IR_n_88),
        .\Dout_reg[31]_0 ({PCPlus4,\Dout_reg[31]_1 [1:0]}),
        .Q({PCPlus4_regPCP4[31],Q[13:8],PCPlus4_regPCP4[24],Q[7],PCPlus4_regPCP4[22:21],Q[6],PCPlus4_regPCP4[19:16],Q[5:4],PCPlus4_regPCP4[13:7],Q[3],PCPlus4_regPCP4[5:4],Q[2:0],PCPlus4_regPCP4[0]}),
        .RF_reg_r2_0_15_30_31(\Dout_reg[31]_0 ),
        .SR(SR));
  REGrwe_n_5 REG_S
       (.CLK(CLK),
        .D(D),
        .\Dout_reg[13]_0 (PC_next[13:11]),
        .\Dout_reg[13]_1 (\Dout_reg[13] [4:2]),
        .\Dout_reg[18]_0 (REG_S_n_36),
        .\Dout_reg[18]_1 (\Dout_reg[31]_2 ),
        .\Dout_reg[18]_2 (REG_A_n_33),
        .\Dout_reg[21]_0 (REG_S_n_35),
        .\Dout_reg[21]_1 (REG_IR_n_87),
        .\Dout_reg[21]_2 (REG_A_n_0),
        .\Dout_reg[31]_0 (\Dout_reg[31]_0 ),
        .Q({PCPlus4_regPCP4[21],PCPlus4_regPCP4[18],PCPlus4_regPCP4[13:11]}),
        .SR(SR));
endmodule

module FSM
   (RegWrite_in,
    Q,
    E,
    \Dout_reg[30] ,
    \Dout_reg[29] ,
    \Dout_reg[28] ,
    \Dout_reg[27] ,
    \Dout_reg[26] ,
    \Dout_reg[25] ,
    \Dout_reg[23] ,
    \Dout_reg[20] ,
    \Dout_reg[15] ,
    \Dout_reg[14] ,
    \Dout_reg[6] ,
    \Dout_reg[3] ,
    \Dout_reg[2] ,
    \Dout_reg[1] ,
    \FSM_onehot_current_state_reg[12]_0 ,
    \FSM_onehot_current_state_reg[0]_0 ,
    \FSM_onehot_current_state_reg[2]_0 ,
    \Dout[30]_i_4 ,
    \Dout[30]_i_4_0 ,
    \FSM_onehot_current_state_reg[8]_0 ,
    SR,
    D,
    CLK);
  output RegWrite_in;
  output [4:0]Q;
  output [0:0]E;
  output \Dout_reg[30] ;
  output \Dout_reg[29] ;
  output \Dout_reg[28] ;
  output \Dout_reg[27] ;
  output \Dout_reg[26] ;
  output \Dout_reg[25] ;
  output \Dout_reg[23] ;
  output \Dout_reg[20] ;
  output \Dout_reg[15] ;
  output \Dout_reg[14] ;
  output \Dout_reg[6] ;
  output \Dout_reg[3] ;
  output \Dout_reg[2] ;
  output \Dout_reg[1] ;
  output \FSM_onehot_current_state_reg[12]_0 ;
  input \FSM_onehot_current_state_reg[0]_0 ;
  input [1:0]\FSM_onehot_current_state_reg[2]_0 ;
  input [13:0]\Dout[30]_i_4 ;
  input [13:0]\Dout[30]_i_4_0 ;
  input \FSM_onehot_current_state_reg[8]_0 ;
  input [0:0]SR;
  input [2:0]D;
  input CLK;

  wire CLK;
  wire [2:0]D;
  wire [13:0]\Dout[30]_i_4 ;
  wire [13:0]\Dout[30]_i_4_0 ;
  wire \Dout_reg[14] ;
  wire \Dout_reg[15] ;
  wire \Dout_reg[1] ;
  wire \Dout_reg[20] ;
  wire \Dout_reg[23] ;
  wire \Dout_reg[25] ;
  wire \Dout_reg[26] ;
  wire \Dout_reg[27] ;
  wire \Dout_reg[28] ;
  wire \Dout_reg[29] ;
  wire \Dout_reg[2] ;
  wire \Dout_reg[30] ;
  wire \Dout_reg[3] ;
  wire \Dout_reg[6] ;
  wire [0:0]E;
  wire \FSM_onehot_current_state[0]_i_1_n_0 ;
  wire \FSM_onehot_current_state[2]_i_1_n_0 ;
  wire \FSM_onehot_current_state[7]_i_1_n_0 ;
  wire \FSM_onehot_current_state[8]_i_1_n_0 ;
  wire \FSM_onehot_current_state_reg[0]_0 ;
  wire \FSM_onehot_current_state_reg[12]_0 ;
  wire [1:0]\FSM_onehot_current_state_reg[2]_0 ;
  wire \FSM_onehot_current_state_reg[8]_0 ;
  wire \FSM_onehot_current_state_reg_n_0_[2] ;
  wire \FSM_onehot_current_state_reg_n_0_[6] ;
  wire \FSM_onehot_current_state_reg_n_0_[8] ;
  wire [4:0]Q;
  wire RegWrite_in;
  wire [0:0]SR;

  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[14]_i_2 
       (.I0(\Dout[30]_i_4 [4]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [4]),
        .O(\Dout_reg[14] ));
  LUT5 #(
    .INIT(32'h000B0008)) 
    \Dout[15]_i_2 
       (.I0(\Dout[30]_i_4_0 [5]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(\Dout[30]_i_4 [5]),
        .O(\Dout_reg[15] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[1]_i_2 
       (.I0(\Dout[30]_i_4 [0]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [0]),
        .O(\Dout_reg[1] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[20]_i_3 
       (.I0(\Dout[30]_i_4 [6]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [6]),
        .O(\Dout_reg[20] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[23]_i_3 
       (.I0(\Dout[30]_i_4 [7]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [7]),
        .O(\Dout_reg[23] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[25]_i_4 
       (.I0(\Dout[30]_i_4 [8]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [8]),
        .O(\Dout_reg[25] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[26]_i_5 
       (.I0(\Dout[30]_i_4 [9]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [9]),
        .O(\Dout_reg[26] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[27]_i_5 
       (.I0(\Dout[30]_i_4 [10]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [10]),
        .O(\Dout_reg[27] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[28]_i_5 
       (.I0(\Dout[30]_i_4 [11]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [11]),
        .O(\Dout_reg[28] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[29]_i_4 
       (.I0(\Dout[30]_i_4 [12]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [12]),
        .O(\Dout_reg[29] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[2]_i_2 
       (.I0(\Dout[30]_i_4 [1]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [1]),
        .O(\Dout_reg[2] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[30]_i_5 
       (.I0(\Dout[30]_i_4 [13]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [13]),
        .O(\Dout_reg[30] ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \Dout[31]_i_1 
       (.I0(Q[4]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(\FSM_onehot_current_state_reg_n_0_[2] ),
        .I4(\FSM_onehot_current_state_reg_n_0_[8] ),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \Dout[31]_i_4 
       (.I0(Q[4]),
        .I1(Q[3]),
        .O(\FSM_onehot_current_state_reg[12]_0 ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[3]_i_2 
       (.I0(\Dout[30]_i_4 [2]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [2]),
        .O(\Dout_reg[3] ));
  LUT5 #(
    .INIT(32'h03020002)) 
    \Dout[6]_i_2 
       (.I0(\Dout[30]_i_4 [3]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\Dout[30]_i_4_0 [3]),
        .O(\Dout_reg[6] ));
  LUT6 #(
    .INIT(64'h0000000030005555)) 
    \FSM_onehot_current_state[0]_i_1 
       (.I0(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I1(\FSM_onehot_current_state_reg[0]_0 ),
        .I2(\FSM_onehot_current_state_reg[2]_0 [0]),
        .I3(\FSM_onehot_current_state_reg[2]_0 [1]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(\FSM_onehot_current_state[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \FSM_onehot_current_state[2]_i_1 
       (.I0(\FSM_onehot_current_state_reg[2]_0 [1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(\FSM_onehot_current_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \FSM_onehot_current_state[7]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I3(\FSM_onehot_current_state_reg[8]_0 ),
        .O(\FSM_onehot_current_state[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \FSM_onehot_current_state[8]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\FSM_onehot_current_state_reg_n_0_[6] ),
        .I3(\FSM_onehot_current_state_reg[8]_0 ),
        .O(\FSM_onehot_current_state[8]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "s2b:0000001000000,s3:0000000010000,s4i:1000000000000,s2a:0000000001000,s4g:1011,s4h:0100000000000,s4f:0001000000000,s1:0000000000010,s0:0000000000001,s4c:0000000000100,s4e:0010000000000,s4b:0000010000000,s4d:0000000100000,s4a:0000100000000" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_current_state_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[0]_i_1_n_0 ),
        .Q(Q[0]),
        .S(SR));
  (* FSM_ENCODED_STATES = "s2b:0000001000000,s3:0000000010000,s4i:1000000000000,s2a:0000000001000,s4g:1011,s4h:0100000000000,s4f:0001000000000,s1:0000000000010,s0:0000000000001,s4c:0000000000100,s4e:0010000000000,s4b:0000010000000,s4d:0000000100000,s4a:0000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[3]),
        .R(SR));
  (* FSM_ENCODED_STATES = "s2b:0000001000000,s3:0000000010000,s4i:1000000000000,s2a:0000000001000,s4g:1011,s4h:0100000000000,s4f:0001000000000,s1:0000000000010,s0:0000000000001,s4c:0000000000100,s4e:0010000000000,s4b:0000010000000,s4d:0000000100000,s4a:0000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[4]),
        .R(SR));
  (* FSM_ENCODED_STATES = "s2b:0000001000000,s3:0000000010000,s4i:1000000000000,s2a:0000000001000,s4g:1011,s4h:0100000000000,s4f:0001000000000,s1:0000000000010,s0:0000000000001,s4c:0000000000100,s4e:0010000000000,s4b:0000010000000,s4d:0000000100000,s4a:0000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(Q[0]),
        .Q(Q[1]),
        .R(SR));
  (* FSM_ENCODED_STATES = "s2b:0000001000000,s3:0000000010000,s4i:1000000000000,s2a:0000000001000,s4g:1011,s4h:0100000000000,s4f:0001000000000,s1:0000000000010,s0:0000000000001,s4c:0000000000100,s4e:0010000000000,s4b:0000010000000,s4d:0000000100000,s4a:0000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[2]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[2] ),
        .R(SR));
  (* FSM_ENCODED_STATES = "s2b:0000001000000,s3:0000000010000,s4i:1000000000000,s2a:0000000001000,s4g:1011,s4h:0100000000000,s4f:0001000000000,s1:0000000000010,s0:0000000000001,s4c:0000000000100,s4e:0010000000000,s4b:0000010000000,s4d:0000000100000,s4a:0000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\FSM_onehot_current_state_reg_n_0_[6] ),
        .R(SR));
  (* FSM_ENCODED_STATES = "s2b:0000001000000,s3:0000000010000,s4i:1000000000000,s2a:0000000001000,s4g:1011,s4h:0100000000000,s4f:0001000000000,s1:0000000000010,s0:0000000000001,s4c:0000000000100,s4e:0010000000000,s4b:0000010000000,s4d:0000000100000,s4a:0000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[7]_i_1_n_0 ),
        .Q(Q[2]),
        .R(SR));
  (* FSM_ENCODED_STATES = "s2b:0000001000000,s3:0000000010000,s4i:1000000000000,s2a:0000000001000,s4g:1011,s4h:0100000000000,s4f:0001000000000,s1:0000000000010,s0:0000000000001,s4c:0000000000100,s4e:0010000000000,s4b:0000010000000,s4d:0000000100000,s4a:0000100000000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_state_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_onehot_current_state[8]_i_1_n_0 ),
        .Q(\FSM_onehot_current_state_reg_n_0_[8] ),
        .R(SR));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_1
       (.I0(\FSM_onehot_current_state_reg_n_0_[8] ),
        .I1(Q[4]),
        .O(RegWrite_in));
endmodule

module REGISTER_FILE_n
   (DATA_OUT1,
    DATA_OUT2,
    CLK,
    RegWrite_in,
    DATA_IN,
    ADDR_R1,
    ADDRD,
    Q);
  output [31:0]DATA_OUT1;
  output [31:0]DATA_OUT2;
  input CLK;
  input RegWrite_in;
  input [31:0]DATA_IN;
  input [1:0]ADDR_R1;
  input [3:0]ADDRD;
  input [3:0]Q;

  wire [3:0]ADDRD;
  wire [1:0]ADDR_R1;
  wire CLK;
  wire [31:0]DATA_IN;
  wire [31:0]DATA_OUT1;
  wire [31:0]DATA_OUT2;
  wire [3:0]Q;
  wire RegWrite_in;
  wire [1:0]NLW_RF_reg_r1_0_15_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r1_0_15_6_11_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_RF_reg_r2_0_15_6_11_DOD_UNCONNECTED;

  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M_UNIQ_BASE_ RF_reg_r1_0_15_0_5
       (.ADDRA({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRB({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRC({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[1:0]),
        .DIB(DATA_IN[3:2]),
        .DIC(DATA_IN[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[1:0]),
        .DOB(DATA_OUT1[3:2]),
        .DOC(DATA_OUT1[5:4]),
        .DOD(NLW_RF_reg_r1_0_15_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "17" *) 
  RAM32M_HD1 RF_reg_r1_0_15_12_17
       (.ADDRA({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRB({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRC({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[13:12]),
        .DIB(DATA_IN[15:14]),
        .DIC(DATA_IN[17:16]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[13:12]),
        .DOB(DATA_OUT1[15:14]),
        .DOC(DATA_OUT1[17:16]),
        .DOD(NLW_RF_reg_r1_0_15_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAM32M_HD2 RF_reg_r1_0_15_18_23
       (.ADDRA({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRB({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRC({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[19:18]),
        .DIB(DATA_IN[21:20]),
        .DIC(DATA_IN[23:22]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[19:18]),
        .DOB(DATA_OUT1[21:20]),
        .DOC(DATA_OUT1[23:22]),
        .DOD(NLW_RF_reg_r1_0_15_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "29" *) 
  RAM32M_HD3 RF_reg_r1_0_15_24_29
       (.ADDRA({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRB({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRC({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[25:24]),
        .DIB(DATA_IN[27:26]),
        .DIC(DATA_IN[29:28]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[25:24]),
        .DOB(DATA_OUT1[27:26]),
        .DOC(DATA_OUT1[29:28]),
        .DOD(NLW_RF_reg_r1_0_15_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "31" *) 
  RAM32M_HD4 RF_reg_r1_0_15_30_31
       (.ADDRA({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRB({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRC({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[31:30]),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[31:30]),
        .DOB(NLW_RF_reg_r1_0_15_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_RF_reg_r1_0_15_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_RF_reg_r1_0_15_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M_HD5 RF_reg_r1_0_15_6_11
       (.ADDRA({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRB({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRC({1'b0,ADDR_R1,ADDR_R1[1],ADDR_R1[1]}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[7:6]),
        .DIB(DATA_IN[9:8]),
        .DIC(DATA_IN[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT1[7:6]),
        .DOB(DATA_OUT1[9:8]),
        .DOC(DATA_OUT1[11:10]),
        .DOD(NLW_RF_reg_r1_0_15_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M_HD6 RF_reg_r2_0_15_0_5
       (.ADDRA({1'b0,Q}),
        .ADDRB({1'b0,Q}),
        .ADDRC({1'b0,Q}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[1:0]),
        .DIB(DATA_IN[3:2]),
        .DIC(DATA_IN[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[1:0]),
        .DOB(DATA_OUT2[3:2]),
        .DOC(DATA_OUT2[5:4]),
        .DOD(NLW_RF_reg_r2_0_15_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "17" *) 
  RAM32M_HD7 RF_reg_r2_0_15_12_17
       (.ADDRA({1'b0,Q}),
        .ADDRB({1'b0,Q}),
        .ADDRC({1'b0,Q}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[13:12]),
        .DIB(DATA_IN[15:14]),
        .DIC(DATA_IN[17:16]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[13:12]),
        .DOB(DATA_OUT2[15:14]),
        .DOC(DATA_OUT2[17:16]),
        .DOD(NLW_RF_reg_r2_0_15_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAM32M_HD8 RF_reg_r2_0_15_18_23
       (.ADDRA({1'b0,Q}),
        .ADDRB({1'b0,Q}),
        .ADDRC({1'b0,Q}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[19:18]),
        .DIB(DATA_IN[21:20]),
        .DIC(DATA_IN[23:22]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[19:18]),
        .DOB(DATA_OUT2[21:20]),
        .DOC(DATA_OUT2[23:22]),
        .DOD(NLW_RF_reg_r2_0_15_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "29" *) 
  RAM32M_HD9 RF_reg_r2_0_15_24_29
       (.ADDRA({1'b0,Q}),
        .ADDRB({1'b0,Q}),
        .ADDRC({1'b0,Q}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[25:24]),
        .DIB(DATA_IN[27:26]),
        .DIC(DATA_IN[29:28]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[25:24]),
        .DOB(DATA_OUT2[27:26]),
        .DOC(DATA_OUT2[29:28]),
        .DOD(NLW_RF_reg_r2_0_15_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "31" *) 
  RAM32M_HD10 RF_reg_r2_0_15_30_31
       (.ADDRA({1'b0,Q}),
        .ADDRB({1'b0,Q}),
        .ADDRC({1'b0,Q}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[31:30]),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[31:30]),
        .DOB(NLW_RF_reg_r2_0_15_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_RF_reg_r2_0_15_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_RF_reg_r2_0_15_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
  (* INIT_A = "64'h0000000000000000" *) 
  (* INIT_B = "64'h0000000000000000" *) 
  (* INIT_C = "64'h0000000000000000" *) 
  (* INIT_D = "64'h0000000000000000" *) 
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "DATAPATH/REG_FILE/REG_FILE_IN/RF" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "15" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M_HD11 RF_reg_r2_0_15_6_11
       (.ADDRA({1'b0,Q}),
        .ADDRB({1'b0,Q}),
        .ADDRC({1'b0,Q}),
        .ADDRD({1'b0,ADDRD}),
        .DIA(DATA_IN[7:6]),
        .DIB(DATA_IN[9:8]),
        .DIC(DATA_IN[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(DATA_OUT2[7:6]),
        .DOB(DATA_OUT2[9:8]),
        .DOC(DATA_OUT2[11:10]),
        .DOD(NLW_RF_reg_r2_0_15_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(CLK),
        .WE(RegWrite_in));
endmodule

module REGISTER_FILE_wR15
   (DATA_OUT1,
    DATA_OUT2,
    CLK,
    RegWrite_in,
    DATA_IN,
    ADDR_R1,
    ADDRD,
    Q);
  output [31:0]DATA_OUT1;
  output [31:0]DATA_OUT2;
  input CLK;
  input RegWrite_in;
  input [31:0]DATA_IN;
  input [1:0]ADDR_R1;
  input [3:0]ADDRD;
  input [3:0]Q;

  wire [3:0]ADDRD;
  wire [1:0]ADDR_R1;
  wire CLK;
  wire [31:0]DATA_IN;
  wire [31:0]DATA_OUT1;
  wire [31:0]DATA_OUT2;
  wire [3:0]Q;
  wire RegWrite_in;

  REGISTER_FILE_n REG_FILE_IN
       (.ADDRD(ADDRD),
        .ADDR_R1(ADDR_R1),
        .CLK(CLK),
        .DATA_IN(DATA_IN),
        .DATA_OUT1(DATA_OUT1),
        .DATA_OUT2(DATA_OUT2),
        .Q(Q),
        .RegWrite_in(RegWrite_in));
endmodule

module REGrwe_n
   (D,
    Q,
    \Dout_reg[31]_0 ,
    SR,
    E,
    \Dout_reg[31]_1 ,
    CLK);
  output [1:0]D;
  output [31:0]Q;
  output [29:0]\Dout_reg[31]_0 ;
  input [0:0]SR;
  input [0:0]E;
  input [31:0]\Dout_reg[31]_1 ;
  input CLK;

  wire CLK;
  wire [1:0]D;
  wire \Dout_reg[10]_i_1_n_0 ;
  wire \Dout_reg[10]_i_1_n_1 ;
  wire \Dout_reg[10]_i_1_n_2 ;
  wire \Dout_reg[10]_i_1_n_3 ;
  wire \Dout_reg[14]_i_1_n_0 ;
  wire \Dout_reg[14]_i_1_n_1 ;
  wire \Dout_reg[14]_i_1_n_2 ;
  wire \Dout_reg[14]_i_1_n_3 ;
  wire \Dout_reg[18]_i_1_n_0 ;
  wire \Dout_reg[18]_i_1_n_1 ;
  wire \Dout_reg[18]_i_1_n_2 ;
  wire \Dout_reg[18]_i_1_n_3 ;
  wire \Dout_reg[22]_i_1_n_0 ;
  wire \Dout_reg[22]_i_1_n_1 ;
  wire \Dout_reg[22]_i_1_n_2 ;
  wire \Dout_reg[22]_i_1_n_3 ;
  wire \Dout_reg[26]_i_1_n_0 ;
  wire \Dout_reg[26]_i_1_n_1 ;
  wire \Dout_reg[26]_i_1_n_2 ;
  wire \Dout_reg[26]_i_1_n_3 ;
  wire \Dout_reg[30]_i_1_n_0 ;
  wire \Dout_reg[30]_i_1_n_1 ;
  wire \Dout_reg[30]_i_1_n_2 ;
  wire \Dout_reg[30]_i_1_n_3 ;
  wire [29:0]\Dout_reg[31]_0 ;
  wire [31:0]\Dout_reg[31]_1 ;
  wire \Dout_reg[6]_i_1_n_0 ;
  wire \Dout_reg[6]_i_1_n_1 ;
  wire \Dout_reg[6]_i_1_n_2 ;
  wire \Dout_reg[6]_i_1_n_3 ;
  wire [0:0]E;
  wire [31:0]Q;
  wire [0:0]SR;
  wire [3:0]\NLW_Dout_reg[31]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_Dout_reg[31]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h00000000000060D1)) 
    \Dout[0]_i_1__2 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[7]),
        .I5(Q[6]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h0000000000001077)) 
    \Dout[25]_i_1__1 
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[2]),
        .I3(Q[5]),
        .I4(Q[7]),
        .I5(Q[6]),
        .O(D[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \Dout[2]_i_1__2 
       (.I0(Q[2]),
        .O(\Dout_reg[31]_0 [0]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [10]),
        .Q(Q[10]),
        .R(SR));
  CARRY4 \Dout_reg[10]_i_1 
       (.CI(\Dout_reg[6]_i_1_n_0 ),
        .CO({\Dout_reg[10]_i_1_n_0 ,\Dout_reg[10]_i_1_n_1 ,\Dout_reg[10]_i_1_n_2 ,\Dout_reg[10]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(Q[10:7]),
        .O(\Dout_reg[31]_0 [8:5]),
        .S(Q[10:7]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [11]),
        .Q(Q[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [12]),
        .Q(Q[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [13]),
        .Q(Q[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [14]),
        .Q(Q[14]),
        .R(SR));
  CARRY4 \Dout_reg[14]_i_1 
       (.CI(\Dout_reg[10]_i_1_n_0 ),
        .CO({\Dout_reg[14]_i_1_n_0 ,\Dout_reg[14]_i_1_n_1 ,\Dout_reg[14]_i_1_n_2 ,\Dout_reg[14]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(Q[14:11]),
        .O(\Dout_reg[31]_0 [12:9]),
        .S(Q[14:11]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [15]),
        .Q(Q[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [16]),
        .Q(Q[16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [17]),
        .Q(Q[17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [18]),
        .Q(Q[18]),
        .R(SR));
  CARRY4 \Dout_reg[18]_i_1 
       (.CI(\Dout_reg[14]_i_1_n_0 ),
        .CO({\Dout_reg[18]_i_1_n_0 ,\Dout_reg[18]_i_1_n_1 ,\Dout_reg[18]_i_1_n_2 ,\Dout_reg[18]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(Q[18:15]),
        .O(\Dout_reg[31]_0 [16:13]),
        .S(Q[18:15]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [19]),
        .Q(Q[19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [20]),
        .Q(Q[20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [21]),
        .Q(Q[21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [22]),
        .Q(Q[22]),
        .R(SR));
  CARRY4 \Dout_reg[22]_i_1 
       (.CI(\Dout_reg[18]_i_1_n_0 ),
        .CO({\Dout_reg[22]_i_1_n_0 ,\Dout_reg[22]_i_1_n_1 ,\Dout_reg[22]_i_1_n_2 ,\Dout_reg[22]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(Q[22:19]),
        .O(\Dout_reg[31]_0 [20:17]),
        .S(Q[22:19]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [23]),
        .Q(Q[23]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [24]),
        .Q(Q[24]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [25]),
        .Q(Q[25]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [26]),
        .Q(Q[26]),
        .R(SR));
  CARRY4 \Dout_reg[26]_i_1 
       (.CI(\Dout_reg[22]_i_1_n_0 ),
        .CO({\Dout_reg[26]_i_1_n_0 ,\Dout_reg[26]_i_1_n_1 ,\Dout_reg[26]_i_1_n_2 ,\Dout_reg[26]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(Q[26:23]),
        .O(\Dout_reg[31]_0 [24:21]),
        .S(Q[26:23]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [27]),
        .Q(Q[27]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [28]),
        .Q(Q[28]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [29]),
        .Q(Q[29]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [30]),
        .Q(Q[30]),
        .R(SR));
  CARRY4 \Dout_reg[30]_i_1 
       (.CI(\Dout_reg[26]_i_1_n_0 ),
        .CO({\Dout_reg[30]_i_1_n_0 ,\Dout_reg[30]_i_1_n_1 ,\Dout_reg[30]_i_1_n_2 ,\Dout_reg[30]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(Q[30:27]),
        .O(\Dout_reg[31]_0 [28:25]),
        .S(Q[30:27]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [31]),
        .Q(Q[31]),
        .R(SR));
  CARRY4 \Dout_reg[31]_i_1 
       (.CI(\Dout_reg[30]_i_1_n_0 ),
        .CO(\NLW_Dout_reg[31]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_Dout_reg[31]_i_1_O_UNCONNECTED [3:1],\Dout_reg[31]_0 [29]}),
        .S({1'b0,1'b0,1'b0,Q[31]}));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [3]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [4]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [5]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [6]),
        .Q(Q[6]),
        .R(SR));
  CARRY4 \Dout_reg[6]_i_1 
       (.CI(1'b0),
        .CO({\Dout_reg[6]_i_1_n_0 ,\Dout_reg[6]_i_1_n_1 ,\Dout_reg[6]_i_1_n_2 ,\Dout_reg[6]_i_1_n_3 }),
        .CYINIT(Q[2]),
        .DI(Q[6:3]),
        .O(\Dout_reg[31]_0 [4:1]),
        .S(Q[6:3]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [7]),
        .Q(Q[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [8]),
        .Q(Q[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK),
        .CE(E),
        .D(\Dout_reg[31]_1 [9]),
        .Q(Q[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_0
   (\Dout_reg[21]_0 ,
    Q,
    \Dout_reg[18]_0 ,
    \Dout[21]_i_3 ,
    SR,
    D,
    CLK);
  output \Dout_reg[21]_0 ;
  output [31:0]Q;
  output \Dout_reg[18]_0 ;
  input [1:0]\Dout[21]_i_3 ;
  input [0:0]SR;
  input [31:0]D;
  input CLK;

  wire CLK;
  wire [31:0]D;
  wire [1:0]\Dout[21]_i_3 ;
  wire \Dout_reg[18]_0 ;
  wire \Dout_reg[21]_0 ;
  wire [31:0]Q;
  wire [0:0]SR;

  LUT2 #(
    .INIT(4'h2)) 
    \Dout[18]_i_4 
       (.I0(Q[18]),
        .I1(\Dout[21]_i_3 [0]),
        .O(\Dout_reg[18]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Dout[21]_i_4 
       (.I0(Q[21]),
        .I1(\Dout[21]_i_3 [1]),
        .O(\Dout_reg[21]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[11]),
        .Q(Q[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[15]),
        .Q(Q[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[16]),
        .Q(Q[16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[17]),
        .Q(Q[17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[18]),
        .Q(Q[18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[19]),
        .Q(Q[19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[20]),
        .Q(Q[20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[21]),
        .Q(Q[21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[22]),
        .Q(Q[22]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[23]),
        .Q(Q[23]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[24]),
        .Q(Q[24]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[25]),
        .Q(Q[25]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[26]),
        .Q(Q[26]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[27]),
        .Q(Q[27]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[28]),
        .Q(Q[28]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[29]),
        .Q(Q[29]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[30]),
        .Q(Q[30]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[31]),
        .Q(Q[31]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[7]),
        .Q(Q[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[9]),
        .Q(Q[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_1
   (\Dout_reg[2]_0 ,
    Q,
    \Dout_reg[3]_0 ,
    \Dout_reg[5]_0 ,
    \Dout_reg[6]_0 ,
    \Dout_reg[15]_0 ,
    \Dout_reg[26]_0 ,
    \Dout_reg[28]_0 ,
    \Dout_reg[30]_0 ,
    \Dout_reg[30]_1 ,
    \Dout_reg[2]_1 ,
    \Dout_reg[2]_2 ,
    \Dout_reg[26]_1 ,
    SR,
    \Dout_reg[31]_0 ,
    CLK);
  output \Dout_reg[2]_0 ;
  output [31:0]Q;
  output \Dout_reg[3]_0 ;
  output \Dout_reg[5]_0 ;
  output \Dout_reg[6]_0 ;
  output \Dout_reg[15]_0 ;
  output \Dout_reg[26]_0 ;
  output \Dout_reg[28]_0 ;
  output \Dout_reg[30]_0 ;
  input [7:0]\Dout_reg[30]_1 ;
  input \Dout_reg[2]_1 ;
  input \Dout_reg[2]_2 ;
  input \Dout_reg[26]_1 ;
  input [0:0]SR;
  input [31:0]\Dout_reg[31]_0 ;
  input CLK;

  wire CLK;
  wire \Dout_reg[15]_0 ;
  wire \Dout_reg[26]_0 ;
  wire \Dout_reg[26]_1 ;
  wire \Dout_reg[28]_0 ;
  wire \Dout_reg[2]_0 ;
  wire \Dout_reg[2]_1 ;
  wire \Dout_reg[2]_2 ;
  wire \Dout_reg[30]_0 ;
  wire [7:0]\Dout_reg[30]_1 ;
  wire [31:0]\Dout_reg[31]_0 ;
  wire \Dout_reg[3]_0 ;
  wire \Dout_reg[5]_0 ;
  wire \Dout_reg[6]_0 ;
  wire [31:0]Q;
  wire [0:0]SR;

  LUT5 #(
    .INIT(32'h80FF8080)) 
    \ALUResult_out_OBUF[15]_inst_i_4 
       (.I0(Q[15]),
        .I1(\Dout_reg[30]_1 [4]),
        .I2(\Dout_reg[2]_1 ),
        .I3(\Dout_reg[2]_2 ),
        .I4(Q[18]),
        .O(\Dout_reg[15]_0 ));
  LUT5 #(
    .INIT(32'h80FF8080)) 
    \ALUResult_out_OBUF[2]_inst_i_4 
       (.I0(Q[2]),
        .I1(\Dout_reg[30]_1 [0]),
        .I2(\Dout_reg[2]_1 ),
        .I3(\Dout_reg[2]_2 ),
        .I4(Q[5]),
        .O(\Dout_reg[2]_0 ));
  LUT5 #(
    .INIT(32'h80FF8080)) 
    \ALUResult_out_OBUF[3]_inst_i_4 
       (.I0(Q[3]),
        .I1(\Dout_reg[30]_1 [1]),
        .I2(\Dout_reg[2]_1 ),
        .I3(\Dout_reg[2]_2 ),
        .I4(Q[6]),
        .O(\Dout_reg[3]_0 ));
  LUT5 #(
    .INIT(32'h60FF6060)) 
    \ALUResult_out_OBUF[5]_inst_i_3 
       (.I0(Q[5]),
        .I1(\Dout_reg[30]_1 [2]),
        .I2(\Dout_reg[26]_1 ),
        .I3(\Dout_reg[2]_2 ),
        .I4(Q[8]),
        .O(\Dout_reg[5]_0 ));
  LUT5 #(
    .INIT(32'h80FF8080)) 
    \ALUResult_out_OBUF[6]_inst_i_4 
       (.I0(Q[6]),
        .I1(\Dout_reg[30]_1 [3]),
        .I2(\Dout_reg[2]_1 ),
        .I3(\Dout_reg[2]_2 ),
        .I4(Q[9]),
        .O(\Dout_reg[6]_0 ));
  LUT5 #(
    .INIT(32'h60FF6060)) 
    \Dout[26]_i_3 
       (.I0(Q[26]),
        .I1(\Dout_reg[30]_1 [5]),
        .I2(\Dout_reg[26]_1 ),
        .I3(\Dout_reg[2]_2 ),
        .I4(Q[29]),
        .O(\Dout_reg[26]_0 ));
  LUT5 #(
    .INIT(32'h60FF6060)) 
    \Dout[28]_i_3 
       (.I0(Q[28]),
        .I1(\Dout_reg[30]_1 [6]),
        .I2(\Dout_reg[26]_1 ),
        .I3(\Dout_reg[2]_2 ),
        .I4(Q[31]),
        .O(\Dout_reg[28]_0 ));
  LUT5 #(
    .INIT(32'h60FF6060)) 
    \Dout[30]_i_3 
       (.I0(Q[30]),
        .I1(\Dout_reg[30]_1 [7]),
        .I2(\Dout_reg[26]_1 ),
        .I3(\Dout_reg[2]_2 ),
        .I4(Q[1]),
        .O(\Dout_reg[30]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [10]),
        .Q(Q[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [11]),
        .Q(Q[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [12]),
        .Q(Q[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [13]),
        .Q(Q[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [14]),
        .Q(Q[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [15]),
        .Q(Q[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [16]),
        .Q(Q[16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [17]),
        .Q(Q[17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [18]),
        .Q(Q[18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [19]),
        .Q(Q[19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [20]),
        .Q(Q[20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [21]),
        .Q(Q[21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [22]),
        .Q(Q[22]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [23]),
        .Q(Q[23]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [24]),
        .Q(Q[24]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [25]),
        .Q(Q[25]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [26]),
        .Q(Q[26]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [27]),
        .Q(Q[27]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [28]),
        .Q(Q[28]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [29]),
        .Q(Q[29]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [30]),
        .Q(Q[30]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [31]),
        .Q(Q[31]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [3]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [4]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [5]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [6]),
        .Q(Q[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [7]),
        .Q(Q[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [8]),
        .Q(Q[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [9]),
        .Q(Q[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_2
   (\Dout_reg[16]_0 ,
    Q,
    \Dout_reg[4]_0 ,
    \Dout_reg[16]_1 ,
    SR,
    D,
    CLK);
  output \Dout_reg[16]_0 ;
  output [15:0]Q;
  output \Dout_reg[4]_0 ;
  input [1:0]\Dout_reg[16]_1 ;
  input [0:0]SR;
  input [15:0]D;
  input CLK;

  wire CLK;
  wire [15:0]D;
  wire \Dout_reg[16]_0 ;
  wire [1:0]\Dout_reg[16]_1 ;
  wire \Dout_reg[4]_0 ;
  wire [15:0]Q;
  wire [0:0]SR;

  LUT2 #(
    .INIT(4'hB)) 
    \Dout[16]_i_2 
       (.I0(Q[11]),
        .I1(\Dout_reg[16]_1 [1]),
        .O(\Dout_reg[16]_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \Dout[4]_i_2 
       (.I0(Q[4]),
        .I1(\Dout_reg[16]_1 [0]),
        .O(\Dout_reg[4]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[8]),
        .Q(Q[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[9]),
        .Q(Q[9]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[10]),
        .Q(Q[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[11]),
        .Q(Q[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[12]),
        .Q(Q[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[13]),
        .Q(Q[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[14]),
        .Q(Q[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[15]),
        .Q(Q[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[7]),
        .Q(Q[7]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_3
   (ADDR_R1,
    \Dout_reg[31]_0 ,
    ALUControl_out_OBUF,
    \Dout_reg[31]_1 ,
    D,
    \Dout_reg[24]_0 ,
    \Dout_reg[23]_0 ,
    \Dout_reg[24]_1 ,
    \Dout_reg[23]_1 ,
    \Dout_reg[27]_rep_0 ,
    \Dout_reg[27]_0 ,
    \Dout_reg[27]_rep_1 ,
    ADDRD,
    \Dout_reg[0]_0 ,
    \Dout_reg[24]_2 ,
    \Dout_reg[12]_0 ,
    \Dout_reg[31]_2 ,
    S,
    \Dout_reg[7] ,
    \Dout_reg[11] ,
    \Dout_reg[15]_0 ,
    \Dout_reg[19] ,
    \Dout_reg[23]_2 ,
    \Dout_reg[27]_1 ,
    \Dout_reg[31]_3 ,
    Q,
    \Dout_reg[31]_4 ,
    \Dout_reg[26] ,
    \Dout_reg[21]_0 ,
    S_Add_in,
    \Dout_reg[31]_5 ,
    \Dout_reg[31]_6 ,
    \Dout_reg[1]_0 ,
    \Dout_reg[2]_0 ,
    \Dout_reg[2]_1 ,
    \Dout_reg[3]_0 ,
    \Dout_reg[3]_1 ,
    \Dout_reg[4]_0 ,
    \Dout_reg[5] ,
    \Dout_reg[6]_0 ,
    \Dout_reg[6]_1 ,
    \Dout_reg[14]_0 ,
    \Dout_reg[15]_1 ,
    \Dout_reg[15]_2 ,
    \Dout_reg[16] ,
    \Dout_reg[18]_0 ,
    \Dout_reg[21]_1 ,
    \Dout_reg[26]_0 ,
    \Dout_reg[28] ,
    \Dout_reg[30] ,
    \Dout_reg[23]_3 ,
    \Dout_reg[29] ,
    \Dout_reg[25]_0 ,
    \Dout_reg[20] ,
    \Dout_reg[26]_1 ,
    \Dout_reg[27]_2 ,
    \Dout_reg[28]_0 ,
    \Dout_reg[30]_0 ,
    DATA_OUT2,
    PCPlus8,
    SR,
    \Dout_reg[31]_7 ,
    CLK,
    \Dout_reg[27]_rep_2 ,
    \Dout_reg[27]_rep__0_0 );
  output [1:0]ADDR_R1;
  output [16:0]\Dout_reg[31]_0 ;
  output [3:0]ALUControl_out_OBUF;
  output [28:0]\Dout_reg[31]_1 ;
  output [31:0]D;
  output \Dout_reg[24]_0 ;
  output \Dout_reg[23]_0 ;
  output \Dout_reg[24]_1 ;
  output \Dout_reg[23]_1 ;
  output \Dout_reg[27]_rep_0 ;
  output \Dout_reg[27]_0 ;
  output [14:0]\Dout_reg[27]_rep_1 ;
  output [3:0]ADDRD;
  output [31:0]\Dout_reg[0]_0 ;
  output [2:0]\Dout_reg[24]_2 ;
  output \Dout_reg[12]_0 ;
  output [3:0]\Dout_reg[31]_2 ;
  output [3:0]S;
  output [3:0]\Dout_reg[7] ;
  output [3:0]\Dout_reg[11] ;
  output [3:0]\Dout_reg[15]_0 ;
  output [3:0]\Dout_reg[19] ;
  output [3:0]\Dout_reg[23]_2 ;
  output [3:0]\Dout_reg[27]_1 ;
  input \Dout_reg[31]_3 ;
  input [31:0]Q;
  input [31:0]\Dout_reg[31]_4 ;
  input [15:0]\Dout_reg[26] ;
  input [4:0]\Dout_reg[21]_0 ;
  input [31:0]S_Add_in;
  input [12:0]\Dout_reg[31]_5 ;
  input [14:0]\Dout_reg[31]_6 ;
  input \Dout_reg[1]_0 ;
  input \Dout_reg[2]_0 ;
  input \Dout_reg[2]_1 ;
  input \Dout_reg[3]_0 ;
  input \Dout_reg[3]_1 ;
  input \Dout_reg[4]_0 ;
  input \Dout_reg[5] ;
  input \Dout_reg[6]_0 ;
  input \Dout_reg[6]_1 ;
  input \Dout_reg[14]_0 ;
  input \Dout_reg[15]_1 ;
  input \Dout_reg[15]_2 ;
  input \Dout_reg[16] ;
  input \Dout_reg[18]_0 ;
  input \Dout_reg[21]_1 ;
  input \Dout_reg[26]_0 ;
  input \Dout_reg[28] ;
  input \Dout_reg[30] ;
  input \Dout_reg[23]_3 ;
  input \Dout_reg[29] ;
  input \Dout_reg[25]_0 ;
  input \Dout_reg[20] ;
  input \Dout_reg[26]_1 ;
  input \Dout_reg[27]_2 ;
  input \Dout_reg[28]_0 ;
  input \Dout_reg[30]_0 ;
  input [31:0]DATA_OUT2;
  input [28:0]PCPlus8;
  input [0:0]SR;
  input [16:0]\Dout_reg[31]_7 ;
  input CLK;
  input \Dout_reg[27]_rep_2 ;
  input \Dout_reg[27]_rep__0_0 ;

  wire [3:0]ADDRD;
  wire [1:0]ADDR_R1;
  wire [3:0]ALUControl_out_OBUF;
  wire \ALUResult_out_OBUF[0]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[0]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[0]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[0]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[0]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[10]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[10]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[10]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[10]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[11]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[11]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[12]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[12]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[13]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[13]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[14]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[14]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[14]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[14]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[15]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[15]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[16]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[16]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[16]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[16]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[17]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[17]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[17]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[17]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[17]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[18]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[18]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[19]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[19]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[1]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[1]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[1]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[1]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[20]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[20]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[20]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[20]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[20]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[20]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[21]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[21]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[22]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[22]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[23]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[23]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[23]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[23]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[23]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[23]_inst_i_7_n_0 ;
  wire \ALUResult_out_OBUF[23]_inst_i_8_n_0 ;
  wire \ALUResult_out_OBUF[24]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[24]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[24]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[25]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[25]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[25]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[25]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[26]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[27]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[28]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[29]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[2]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[2]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[2]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[30]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[31]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[31]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[31]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[31]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[3]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[3]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[4]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[4]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[4]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[4]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[5]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[5]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[5]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[6]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[6]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[7]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[7]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[7]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[7]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[7]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[8]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[8]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[8]_inst_i_4_n_0 ;
  wire \ALUResult_out_OBUF[8]_inst_i_5_n_0 ;
  wire \ALUResult_out_OBUF[8]_inst_i_6_n_0 ;
  wire \ALUResult_out_OBUF[9]_inst_i_2_n_0 ;
  wire \ALUResult_out_OBUF[9]_inst_i_3_n_0 ;
  wire \ALUResult_out_OBUF[9]_inst_i_4_n_0 ;
  wire CLK;
  wire [31:0]D;
  wire [31:0]DATA_OUT2;
  wire \Dout[0]_i_2_n_0 ;
  wire \Dout[10]_i_2_n_0 ;
  wire \Dout[10]_i_3_n_0 ;
  wire \Dout[10]_i_4_n_0 ;
  wire \Dout[10]_i_5_n_0 ;
  wire \Dout[14]_i_3_n_0 ;
  wire \Dout[15]_i_3_n_0 ;
  wire \Dout[16]_i_3_n_0 ;
  wire \Dout[17]_i_2_n_0 ;
  wire \Dout[17]_i_3_n_0 ;
  wire \Dout[17]_i_4_n_0 ;
  wire \Dout[17]_i_5_n_0 ;
  wire \Dout[18]_i_2_n_0 ;
  wire \Dout[19]_i_2_n_0 ;
  wire \Dout[1]_i_3_n_0 ;
  wire \Dout[20]_i_2_n_0 ;
  wire \Dout[21]_i_2_n_0 ;
  wire \Dout[22]_i_2_n_0 ;
  wire \Dout[23]_i_2_n_0 ;
  wire \Dout[24]_i_2_n_0 ;
  wire \Dout[25]_i_2_n_0 ;
  wire \Dout[25]_i_3_n_0 ;
  wire \Dout[26]_i_2_n_0 ;
  wire \Dout[26]_i_4_n_0 ;
  wire \Dout[27]_i_2_n_0 ;
  wire \Dout[27]_i_3_n_0 ;
  wire \Dout[27]_i_4_n_0 ;
  wire \Dout[28]_i_2_n_0 ;
  wire \Dout[28]_i_4_n_0 ;
  wire \Dout[29]_i_2_n_0 ;
  wire \Dout[29]_i_3_n_0 ;
  wire \Dout[29]_i_5_n_0 ;
  wire \Dout[30]_i_2_n_0 ;
  wire \Dout[30]_i_4_n_0 ;
  wire \Dout[31]_i_3_n_0 ;
  wire \Dout[31]_i_5_n_0 ;
  wire \Dout[31]_i_6_n_0 ;
  wire \Dout[31]_i_7_n_0 ;
  wire \Dout[31]_i_8_n_0 ;
  wire \Dout[31]_i_9_n_0 ;
  wire \Dout[3]_i_3_n_0 ;
  wire \Dout[4]_i_3_n_0 ;
  wire \Dout[5]_i_2_n_0 ;
  wire \Dout[6]_i_3_n_0 ;
  wire \Dout[7]_i_3_n_0 ;
  wire \Dout[7]_i_4_n_0 ;
  wire \Dout[7]_i_5_n_0 ;
  wire \Dout[8]_i_2_n_0 ;
  wire \Dout[8]_i_3_n_0 ;
  wire \Dout[9]_i_2_n_0 ;
  wire \Dout[9]_i_3_n_0 ;
  wire \Dout[9]_i_4_n_0 ;
  wire [31:0]\Dout_reg[0]_0 ;
  wire [3:0]\Dout_reg[11] ;
  wire \Dout_reg[12]_0 ;
  wire \Dout_reg[14]_0 ;
  wire [3:0]\Dout_reg[15]_0 ;
  wire \Dout_reg[15]_1 ;
  wire \Dout_reg[15]_2 ;
  wire \Dout_reg[16] ;
  wire \Dout_reg[18]_0 ;
  wire [3:0]\Dout_reg[19] ;
  wire \Dout_reg[1]_0 ;
  wire \Dout_reg[20] ;
  wire [4:0]\Dout_reg[21]_0 ;
  wire \Dout_reg[21]_1 ;
  wire \Dout_reg[23]_0 ;
  wire \Dout_reg[23]_1 ;
  wire [3:0]\Dout_reg[23]_2 ;
  wire \Dout_reg[23]_3 ;
  wire \Dout_reg[24]_0 ;
  wire \Dout_reg[24]_1 ;
  wire [2:0]\Dout_reg[24]_2 ;
  wire \Dout_reg[25]_0 ;
  wire [15:0]\Dout_reg[26] ;
  wire \Dout_reg[26]_0 ;
  wire \Dout_reg[26]_1 ;
  wire \Dout_reg[27]_0 ;
  wire [3:0]\Dout_reg[27]_1 ;
  wire \Dout_reg[27]_2 ;
  wire \Dout_reg[27]_rep_0 ;
  wire [14:0]\Dout_reg[27]_rep_1 ;
  wire \Dout_reg[27]_rep_2 ;
  wire \Dout_reg[27]_rep__0_0 ;
  wire \Dout_reg[28] ;
  wire \Dout_reg[28]_0 ;
  wire \Dout_reg[29] ;
  wire \Dout_reg[2]_0 ;
  wire \Dout_reg[2]_1 ;
  wire \Dout_reg[30] ;
  wire \Dout_reg[30]_0 ;
  wire [16:0]\Dout_reg[31]_0 ;
  wire [28:0]\Dout_reg[31]_1 ;
  wire [3:0]\Dout_reg[31]_2 ;
  wire \Dout_reg[31]_3 ;
  wire [31:0]\Dout_reg[31]_4 ;
  wire [12:0]\Dout_reg[31]_5 ;
  wire [14:0]\Dout_reg[31]_6 ;
  wire [16:0]\Dout_reg[31]_7 ;
  wire \Dout_reg[3]_0 ;
  wire \Dout_reg[3]_1 ;
  wire \Dout_reg[4]_0 ;
  wire \Dout_reg[5] ;
  wire \Dout_reg[6]_0 ;
  wire \Dout_reg[6]_1 ;
  wire [3:0]\Dout_reg[7] ;
  wire [28:0]PCPlus8;
  wire [31:0]Q;
  wire [3:0]S;
  wire [0:0]SR;
  wire [31:0]S_Add_in;

  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h005D0000)) 
    \ALUControl_out_OBUF[0]_inst_i_1 
       (.I0(\Dout_reg[31]_0 [11]),
        .I1(\Dout_reg[31]_0 [5]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [15]),
        .I4(\Dout_reg[31]_0 [13]),
        .O(ALUControl_out_OBUF[0]));
  LUT6 #(
    .INIT(64'h0000000020AAFFFF)) 
    \ALUControl_out_OBUF[1]_inst_i_1 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[31]_0 [14]),
        .I2(\Dout_reg[31]_0 [5]),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(\Dout_reg[31]_0 [12]),
        .I5(\Dout_reg[31]_0 [15]),
        .O(ALUControl_out_OBUF[1]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h00D0)) 
    \ALUControl_out_OBUF[2]_inst_i_1 
       (.I0(\Dout_reg[31]_0 [12]),
        .I1(\Dout_reg[31]_0 [14]),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [15]),
        .O(ALUControl_out_OBUF[2]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \ALUControl_out_OBUF[3]_inst_i_1 
       (.I0(\Dout_reg[31]_0 [11]),
        .I1(\Dout_reg[31]_0 [13]),
        .I2(\Dout_reg[31]_0 [15]),
        .I3(\Dout_reg[31]_0 [14]),
        .O(ALUControl_out_OBUF[3]));
  LUT5 #(
    .INIT(32'h5700FFFF)) 
    \ALUResult_out_OBUF[0]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I1(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I2(\Dout_reg[26] [0]),
        .I3(Q[0]),
        .I4(\ALUResult_out_OBUF[0]_inst_i_2_n_0 ),
        .O(D[0]));
  LUT5 #(
    .INIT(32'h00000051)) 
    \ALUResult_out_OBUF[0]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[0]_inst_i_3_n_0 ),
        .I1(S_Add_in[0]),
        .I2(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I3(\ALUResult_out_OBUF[0]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[0]_inst_i_5_n_0 ),
        .O(\ALUResult_out_OBUF[0]_inst_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \ALUResult_out_OBUF[0]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I1(\Dout_reg[26] [0]),
        .I2(\Dout_reg[24]_0 ),
        .I3(\Dout_reg[31]_4 [3]),
        .O(\ALUResult_out_OBUF[0]_inst_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h28)) 
    \ALUResult_out_OBUF[0]_inst_i_4 
       (.I0(\Dout_reg[24]_1 ),
        .I1(Q[0]),
        .I2(\Dout_reg[31]_4 [0]),
        .O(\ALUResult_out_OBUF[0]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hF4554455FFFF4455)) 
    \ALUResult_out_OBUF[0]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[0]_inst_i_6_n_0 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_8_n_0 ),
        .I2(\Dout_reg[23]_0 ),
        .I3(Q[0]),
        .I4(\Dout_reg[31]_4 [0]),
        .I5(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[0]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h51515155FFFFFFFF)) 
    \ALUResult_out_OBUF[0]_inst_i_6 
       (.I0(Q[0]),
        .I1(\Dout_reg[31]_0 [14]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [13]),
        .I4(\Dout_reg[31]_0 [11]),
        .I5(\Dout_reg[26] [0]),
        .O(\ALUResult_out_OBUF[0]_inst_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h4FFF)) 
    \ALUResult_out_OBUF[10]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[10]_inst_i_2_n_0 ),
        .I1(Q[10]),
        .I2(\ALUResult_out_OBUF[10]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[10]_inst_i_4_n_0 ),
        .O(D[10]));
  LUT6 #(
    .INIT(64'hFFFFDDCDFFFFDD1D)) 
    \ALUResult_out_OBUF[10]_inst_i_2 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[31]_0 [11]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(\Dout_reg[27]_rep_0 ),
        .I5(\Dout_reg[26] [8]),
        .O(\ALUResult_out_OBUF[10]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAF00AF8F008FAF8F)) 
    \ALUResult_out_OBUF[10]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I1(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I2(\Dout_reg[26] [8]),
        .I3(Q[10]),
        .I4(\Dout_reg[24]_1 ),
        .I5(\Dout_reg[31]_4 [10]),
        .O(\ALUResult_out_OBUF[10]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hA2A200A2)) 
    \ALUResult_out_OBUF[10]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[10]_inst_i_5_n_0 ),
        .I1(S_Add_in[10]),
        .I2(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I3(\Dout_reg[31]_4 [13]),
        .I4(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[10]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFD0000FFFFFFFF)) 
    \ALUResult_out_OBUF[10]_inst_i_5 
       (.I0(Q[10]),
        .I1(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I5(\Dout_reg[31]_4 [10]),
        .O(\ALUResult_out_OBUF[10]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF4F44FFFFFFFF)) 
    \ALUResult_out_OBUF[11]_inst_i_1 
       (.I0(\Dout_reg[24]_0 ),
        .I1(\Dout_reg[31]_4 [14]),
        .I2(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I3(S_Add_in[11]),
        .I4(\ALUResult_out_OBUF[11]_inst_i_2_n_0 ),
        .I5(\ALUResult_out_OBUF[11]_inst_i_3_n_0 ),
        .O(D[11]));
  LUT4 #(
    .INIT(16'h08AA)) 
    \ALUResult_out_OBUF[11]_inst_i_2 
       (.I0(Q[11]),
        .I1(\Dout_reg[23]_1 ),
        .I2(\Dout_reg[31]_4 [11]),
        .I3(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[11]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hD010FFFF)) 
    \ALUResult_out_OBUF[11]_inst_i_3 
       (.I0(\Dout_reg[24]_1 ),
        .I1(Q[11]),
        .I2(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I3(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ),
        .I4(\Dout_reg[31]_4 [11]),
        .O(\ALUResult_out_OBUF[11]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF4F44FFFFFFFF)) 
    \ALUResult_out_OBUF[12]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I1(S_Add_in[12]),
        .I2(\Dout_reg[24]_0 ),
        .I3(\Dout_reg[31]_4 [15]),
        .I4(\ALUResult_out_OBUF[12]_inst_i_2_n_0 ),
        .I5(\ALUResult_out_OBUF[12]_inst_i_3_n_0 ),
        .O(D[12]));
  LUT4 #(
    .INIT(16'h08AA)) 
    \ALUResult_out_OBUF[12]_inst_i_2 
       (.I0(Q[12]),
        .I1(\Dout_reg[23]_1 ),
        .I2(\Dout_reg[31]_4 [12]),
        .I3(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[12]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFD557555)) 
    \ALUResult_out_OBUF[12]_inst_i_3 
       (.I0(\Dout_reg[31]_4 [12]),
        .I1(Q[12]),
        .I2(\Dout[29]_i_2_n_0 ),
        .I3(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ),
        .O(\ALUResult_out_OBUF[12]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF4F44FFFFFFFF)) 
    \ALUResult_out_OBUF[13]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I1(S_Add_in[13]),
        .I2(\Dout_reg[24]_0 ),
        .I3(\Dout_reg[31]_4 [16]),
        .I4(\ALUResult_out_OBUF[13]_inst_i_3_n_0 ),
        .I5(\ALUResult_out_OBUF[13]_inst_i_4_n_0 ),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hFDFFFFFF)) 
    \ALUResult_out_OBUF[13]_inst_i_2 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [5]),
        .I4(\Dout_reg[31]_0 [11]),
        .O(\Dout_reg[24]_0 ));
  LUT4 #(
    .INIT(16'h08AA)) 
    \ALUResult_out_OBUF[13]_inst_i_3 
       (.I0(Q[13]),
        .I1(\Dout_reg[23]_1 ),
        .I2(\Dout_reg[31]_4 [13]),
        .I3(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[13]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFD557555)) 
    \ALUResult_out_OBUF[13]_inst_i_4 
       (.I0(\Dout_reg[31]_4 [13]),
        .I1(Q[13]),
        .I2(\Dout[29]_i_2_n_0 ),
        .I3(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ),
        .O(\ALUResult_out_OBUF[13]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h555DDDDDFFFFFFFF)) 
    \ALUResult_out_OBUF[14]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[14]_inst_i_2_n_0 ),
        .I1(Q[14]),
        .I2(\Dout_reg[26] [9]),
        .I3(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I5(\ALUResult_out_OBUF[14]_inst_i_3_n_0 ),
        .O(D[14]));
  LUT6 #(
    .INIT(64'h00F400F4000000F4)) 
    \ALUResult_out_OBUF[14]_inst_i_2 
       (.I0(Q[14]),
        .I1(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I2(\ALUResult_out_OBUF[14]_inst_i_4_n_0 ),
        .I3(\ALUResult_out_OBUF[14]_inst_i_5_n_0 ),
        .I4(\Dout_reg[31]_4 [17]),
        .I5(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[14]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8A8ACF008A00CFCF)) 
    \ALUResult_out_OBUF[14]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I2(S_Add_in[14]),
        .I3(\Dout[29]_i_2_n_0 ),
        .I4(\Dout_reg[31]_4 [14]),
        .I5(Q[14]),
        .O(\ALUResult_out_OBUF[14]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFEF0000FFFFFFFF)) 
    \ALUResult_out_OBUF[14]_inst_i_4 
       (.I0(\Dout_reg[31]_0 [12]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(Q[14]),
        .I5(\Dout_reg[26] [9]),
        .O(\ALUResult_out_OBUF[14]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hF4444444)) 
    \ALUResult_out_OBUF[14]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I1(\Dout_reg[26] [9]),
        .I2(\Dout_reg[31]_4 [14]),
        .I3(Q[14]),
        .I4(\Dout_reg[23]_0 ),
        .O(\ALUResult_out_OBUF[14]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h555DDDDDFFFFFFFF)) 
    \ALUResult_out_OBUF[15]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[15]_inst_i_2_n_0 ),
        .I1(Q[15]),
        .I2(\Dout_reg[26] [10]),
        .I3(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I5(\ALUResult_out_OBUF[15]_inst_i_3_n_0 ),
        .O(D[15]));
  LUT6 #(
    .INIT(64'h000000005DFD5555)) 
    \ALUResult_out_OBUF[15]_inst_i_2 
       (.I0(\Dout_reg[26] [10]),
        .I1(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I2(Q[15]),
        .I3(\ALUResult_out_OBUF[23]_inst_i_8_n_0 ),
        .I4(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I5(\Dout_reg[15]_2 ),
        .O(\ALUResult_out_OBUF[15]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8A8ACF008A00CFCF)) 
    \ALUResult_out_OBUF[15]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I2(S_Add_in[15]),
        .I3(\Dout[29]_i_2_n_0 ),
        .I4(\Dout_reg[31]_4 [15]),
        .I5(Q[15]),
        .O(\ALUResult_out_OBUF[15]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF57FF5555)) 
    \ALUResult_out_OBUF[16]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[16]_inst_i_2_n_0 ),
        .I1(\Dout_reg[26] [11]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I4(Q[16]),
        .I5(\ALUResult_out_OBUF[16]_inst_i_3_n_0 ),
        .O(D[16]));
  LUT5 #(
    .INIT(32'h0000D0DD)) 
    \ALUResult_out_OBUF[16]_inst_i_2 
       (.I0(\Dout_reg[31]_4 [16]),
        .I1(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I2(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I3(S_Add_in[16]),
        .I4(\ALUResult_out_OBUF[16]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[16]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF14FF14FFFFFF14)) 
    \ALUResult_out_OBUF[16]_inst_i_3 
       (.I0(\Dout[29]_i_2_n_0 ),
        .I1(\Dout_reg[31]_4 [16]),
        .I2(Q[16]),
        .I3(\ALUResult_out_OBUF[16]_inst_i_5_n_0 ),
        .I4(\Dout_reg[31]_4 [19]),
        .I5(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[16]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00A0000000080000)) 
    \ALUResult_out_OBUF[16]_inst_i_4 
       (.I0(\Dout_reg[26] [11]),
        .I1(Q[16]),
        .I2(\Dout_reg[31]_0 [12]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [14]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[16]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h80808F80)) 
    \ALUResult_out_OBUF[16]_inst_i_5 
       (.I0(\Dout_reg[31]_4 [16]),
        .I1(\Dout_reg[23]_0 ),
        .I2(Q[16]),
        .I3(\Dout_reg[26] [11]),
        .I4(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[16]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFDDFDFFFFFFFF)) 
    \ALUResult_out_OBUF[17]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[17]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[17]_inst_i_3_n_0 ),
        .I2(Q[17]),
        .I3(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[17]_inst_i_4_n_0 ),
        .I5(\ALUResult_out_OBUF[17]_inst_i_5_n_0 ),
        .O(D[17]));
  LUT6 #(
    .INIT(64'hD0DD0000D0DDD0DD)) 
    \ALUResult_out_OBUF[17]_inst_i_2 
       (.I0(\Dout_reg[31]_4 [17]),
        .I1(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I2(\Dout_reg[24]_0 ),
        .I3(\Dout_reg[31]_4 [20]),
        .I4(\ALUResult_out_OBUF[17]_inst_i_6_n_0 ),
        .I5(\Dout_reg[26] [12]),
        .O(\ALUResult_out_OBUF[17]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004800)) 
    \ALUResult_out_OBUF[17]_inst_i_3 
       (.I0(\Dout_reg[26] [12]),
        .I1(Q[17]),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(\Dout_reg[31]_0 [12]),
        .I5(\Dout_reg[27]_rep_0 ),
        .O(\ALUResult_out_OBUF[17]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    \ALUResult_out_OBUF[17]_inst_i_4 
       (.I0(\Dout_reg[31]_0 [14]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(Q[17]),
        .I5(\Dout_reg[31]_4 [17]),
        .O(\ALUResult_out_OBUF[17]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hDDD0D0DD)) 
    \ALUResult_out_OBUF[17]_inst_i_5 
       (.I0(S_Add_in[17]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I2(\Dout[29]_i_2_n_0 ),
        .I3(\Dout_reg[31]_4 [17]),
        .I4(Q[17]),
        .O(\ALUResult_out_OBUF[17]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFF4FFF4FFFCFFFFF)) 
    \ALUResult_out_OBUF[17]_inst_i_6 
       (.I0(\Dout_reg[31]_0 [12]),
        .I1(Q[17]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [13]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[17]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h7555F5D5FDDDFDDD)) 
    \ALUResult_out_OBUF[18]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[18]_inst_i_2_n_0 ),
        .I1(\Dout_reg[31]_4 [18]),
        .I2(Q[18]),
        .I3(\Dout_reg[23]_1 ),
        .I4(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ),
        .I5(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .O(D[18]));
  LUT5 #(
    .INIT(32'h0000D0DD)) 
    \ALUResult_out_OBUF[18]_inst_i_2 
       (.I0(S_Add_in[18]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I2(\Dout_reg[24]_0 ),
        .I3(\Dout_reg[31]_4 [21]),
        .I4(\ALUResult_out_OBUF[18]_inst_i_3_n_0 ),
        .O(\ALUResult_out_OBUF[18]_inst_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0F88)) 
    \ALUResult_out_OBUF[18]_inst_i_3 
       (.I0(\Dout_reg[31]_4 [18]),
        .I1(\Dout_reg[24]_1 ),
        .I2(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .I3(Q[18]),
        .O(\ALUResult_out_OBUF[18]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h5D5D55D5FFFF55D5)) 
    \ALUResult_out_OBUF[19]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[19]_inst_i_2_n_0 ),
        .I1(\Dout_reg[23]_1 ),
        .I2(\Dout_reg[31]_4 [19]),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(Q[19]),
        .I5(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .O(D[19]));
  LUT6 #(
    .INIT(64'h7707770700007707)) 
    \ALUResult_out_OBUF[19]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[19]_inst_i_3_n_0 ),
        .I1(\Dout_reg[31]_4 [19]),
        .I2(S_Add_in[19]),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(\Dout_reg[31]_4 [22]),
        .I5(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[19]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000044CF000044CC)) 
    \ALUResult_out_OBUF[19]_inst_i_3 
       (.I0(\Dout_reg[31]_0 [5]),
        .I1(\Dout_reg[31]_0 [13]),
        .I2(\Dout_reg[31]_0 [12]),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I5(Q[19]),
        .O(\ALUResult_out_OBUF[19]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h555DDDDDFFFFFFFF)) 
    \ALUResult_out_OBUF[1]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[1]_inst_i_2_n_0 ),
        .I1(Q[1]),
        .I2(\Dout_reg[26] [1]),
        .I3(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I5(\ALUResult_out_OBUF[1]_inst_i_3_n_0 ),
        .O(D[1]));
  LUT6 #(
    .INIT(64'h00F400F4000000F4)) 
    \ALUResult_out_OBUF[1]_inst_i_2 
       (.I0(Q[1]),
        .I1(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I2(\ALUResult_out_OBUF[1]_inst_i_4_n_0 ),
        .I3(\ALUResult_out_OBUF[1]_inst_i_5_n_0 ),
        .I4(\Dout_reg[31]_4 [4]),
        .I5(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[1]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8A8ACF008A00CFCF)) 
    \ALUResult_out_OBUF[1]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I2(S_Add_in[1]),
        .I3(\Dout[29]_i_2_n_0 ),
        .I4(\Dout_reg[31]_4 [1]),
        .I5(Q[1]),
        .O(\ALUResult_out_OBUF[1]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFEF0000FFFFFFFF)) 
    \ALUResult_out_OBUF[1]_inst_i_4 
       (.I0(\Dout_reg[31]_0 [12]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(Q[1]),
        .I5(\Dout_reg[26] [1]),
        .O(\ALUResult_out_OBUF[1]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hF4444444)) 
    \ALUResult_out_OBUF[1]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I1(\Dout_reg[26] [1]),
        .I2(\Dout_reg[31]_4 [1]),
        .I3(Q[1]),
        .I4(\Dout_reg[23]_0 ),
        .O(\ALUResult_out_OBUF[1]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hABFFAAAAFFFFFFFF)) 
    \ALUResult_out_OBUF[20]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[20]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I2(\Dout_reg[26] [13]),
        .I3(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I4(Q[20]),
        .I5(\ALUResult_out_OBUF[20]_inst_i_5_n_0 ),
        .O(D[20]));
  LUT5 #(
    .INIT(32'hFFFF22F2)) 
    \ALUResult_out_OBUF[20]_inst_i_2 
       (.I0(\Dout_reg[31]_4 [20]),
        .I1(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I2(S_Add_in[20]),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_6_n_0 ),
        .O(\ALUResult_out_OBUF[20]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    \ALUResult_out_OBUF[20]_inst_i_3 
       (.I0(\Dout_reg[31]_0 [11]),
        .I1(\Dout_reg[31]_0 [14]),
        .I2(\Dout_reg[31]_0 [12]),
        .I3(\Dout_reg[27]_rep_0 ),
        .O(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \ALUResult_out_OBUF[20]_inst_i_4 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [13]),
        .I2(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00F900F9000000F9)) 
    \ALUResult_out_OBUF[20]_inst_i_5 
       (.I0(\Dout_reg[31]_4 [20]),
        .I1(Q[20]),
        .I2(\Dout[29]_i_2_n_0 ),
        .I3(\ALUResult_out_OBUF[20]_inst_i_7_n_0 ),
        .I4(\Dout_reg[31]_4 [23]),
        .I5(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[20]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00A0000000080000)) 
    \ALUResult_out_OBUF[20]_inst_i_6 
       (.I0(\Dout_reg[26] [13]),
        .I1(Q[20]),
        .I2(\Dout_reg[31]_0 [12]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [14]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[20]_inst_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h80808F80)) 
    \ALUResult_out_OBUF[20]_inst_i_7 
       (.I0(\Dout_reg[31]_4 [20]),
        .I1(\Dout_reg[23]_0 ),
        .I2(Q[20]),
        .I3(\Dout_reg[26] [13]),
        .I4(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[20]_inst_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h75F5FDFD55D5DDDD)) 
    \ALUResult_out_OBUF[21]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[21]_inst_i_2_n_0 ),
        .I1(\Dout_reg[31]_4 [21]),
        .I2(Q[21]),
        .I3(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ),
        .I4(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I5(\Dout_reg[23]_1 ),
        .O(D[21]));
  LUT6 #(
    .INIT(64'h00000000DDDDD0DD)) 
    \ALUResult_out_OBUF[21]_inst_i_2 
       (.I0(S_Add_in[21]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I2(\Dout[29]_i_2_n_0 ),
        .I3(\Dout_reg[31]_4 [21]),
        .I4(Q[21]),
        .I5(\ALUResult_out_OBUF[21]_inst_i_3_n_0 ),
        .O(\ALUResult_out_OBUF[21]_inst_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \ALUResult_out_OBUF[21]_inst_i_3 
       (.I0(\Dout_reg[24]_0 ),
        .I1(\Dout_reg[31]_4 [24]),
        .I2(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .I3(Q[21]),
        .O(\ALUResult_out_OBUF[21]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h5D5D55D5FFFF55D5)) 
    \ALUResult_out_OBUF[22]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[22]_inst_i_2_n_0 ),
        .I1(\Dout_reg[23]_1 ),
        .I2(\Dout_reg[31]_4 [22]),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(Q[22]),
        .I5(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .O(D[22]));
  LUT6 #(
    .INIT(64'h7707770700007707)) 
    \ALUResult_out_OBUF[22]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[22]_inst_i_3_n_0 ),
        .I1(\Dout_reg[31]_4 [22]),
        .I2(S_Add_in[22]),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(\Dout_reg[31]_4 [25]),
        .I5(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[22]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000044CF000044CC)) 
    \ALUResult_out_OBUF[22]_inst_i_3 
       (.I0(\Dout_reg[31]_0 [5]),
        .I1(\Dout_reg[31]_0 [13]),
        .I2(\Dout_reg[31]_0 [12]),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I5(Q[22]),
        .O(\ALUResult_out_OBUF[22]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFDFFFDFDF)) 
    \ALUResult_out_OBUF[23]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_3_n_0 ),
        .I2(\ALUResult_out_OBUF[23]_inst_i_4_n_0 ),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(S_Add_in[23]),
        .I5(\ALUResult_out_OBUF[23]_inst_i_6_n_0 ),
        .O(D[23]));
  LUT6 #(
    .INIT(64'h000000000DDDDDDD)) 
    \ALUResult_out_OBUF[23]_inst_i_2 
       (.I0(\Dout_reg[31]_4 [26]),
        .I1(\Dout_reg[24]_0 ),
        .I2(\Dout_reg[23]_0 ),
        .I3(Q[23]),
        .I4(\Dout_reg[31]_4 [23]),
        .I5(\ALUResult_out_OBUF[23]_inst_i_7_n_0 ),
        .O(\ALUResult_out_OBUF[23]_inst_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h880C)) 
    \ALUResult_out_OBUF[23]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_8_n_0 ),
        .I1(\Dout_reg[26] [14]),
        .I2(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I3(Q[23]),
        .O(\ALUResult_out_OBUF[23]_inst_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h83BB)) 
    \ALUResult_out_OBUF[23]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(\Dout_reg[31]_4 [23]),
        .I2(Q[23]),
        .I3(\Dout_reg[24]_1 ),
        .O(\ALUResult_out_OBUF[23]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h55454545)) 
    \ALUResult_out_OBUF[23]_inst_i_5 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [13]),
        .I2(\Dout_reg[31]_0 [12]),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0202020202AA0202)) 
    \ALUResult_out_OBUF[23]_inst_i_6 
       (.I0(Q[23]),
        .I1(\Dout_reg[26] [14]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [13]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[23]_inst_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \ALUResult_out_OBUF[23]_inst_i_7 
       (.I0(\Dout_reg[26] [14]),
        .I1(\Dout_reg[31]_0 [11]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(\Dout_reg[27]_rep_0 ),
        .O(\ALUResult_out_OBUF[23]_inst_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \ALUResult_out_OBUF[23]_inst_i_8 
       (.I0(\Dout_reg[31]_0 [12]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[23]_inst_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h5D5D55D5FFFF55D5)) 
    \ALUResult_out_OBUF[24]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[24]_inst_i_2_n_0 ),
        .I1(\Dout_reg[23]_1 ),
        .I2(\Dout_reg[31]_4 [24]),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(Q[24]),
        .I5(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .O(D[24]));
  LUT6 #(
    .INIT(64'h7707770700007707)) 
    \ALUResult_out_OBUF[24]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[24]_inst_i_5_n_0 ),
        .I1(\Dout_reg[31]_4 [24]),
        .I2(S_Add_in[24]),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(\Dout_reg[31]_4 [27]),
        .I5(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[24]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \ALUResult_out_OBUF[24]_inst_i_3 
       (.I0(\Dout_reg[31]_0 [12]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [13]),
        .O(\Dout_reg[23]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hFFFFDD1D)) 
    \ALUResult_out_OBUF[24]_inst_i_4 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[31]_0 [11]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(\Dout_reg[27]_rep_0 ),
        .O(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000044CF000044CC)) 
    \ALUResult_out_OBUF[24]_inst_i_5 
       (.I0(\Dout_reg[31]_0 [5]),
        .I1(\Dout_reg[31]_0 [13]),
        .I2(\Dout_reg[31]_0 [12]),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I5(Q[24]),
        .O(\ALUResult_out_OBUF[24]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFDCDFFFFFFFFF)) 
    \ALUResult_out_OBUF[25]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[31]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[25]_inst_i_2_n_0 ),
        .I2(Q[25]),
        .I3(\ALUResult_out_OBUF[30]_inst_i_2_n_0 ),
        .I4(\ALUResult_out_OBUF[25]_inst_i_3_n_0 ),
        .I5(\ALUResult_out_OBUF[25]_inst_i_4_n_0 ),
        .O(D[25]));
  LUT6 #(
    .INIT(64'h0003AAAAAAAA0000)) 
    \ALUResult_out_OBUF[25]_inst_i_2 
       (.I0(\Dout_reg[24]_1 ),
        .I1(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(\Dout_reg[31]_4 [25]),
        .I5(Q[25]),
        .O(\ALUResult_out_OBUF[25]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \ALUResult_out_OBUF[25]_inst_i_3 
       (.I0(\Dout_reg[26] [15]),
        .I1(\Dout_reg[31]_0 [11]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(\Dout_reg[27]_rep_0 ),
        .O(\ALUResult_out_OBUF[25]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hDD0DDD0D0000DD0D)) 
    \ALUResult_out_OBUF[25]_inst_i_4 
       (.I0(S_Add_in[25]),
        .I1(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I2(\Dout_reg[31]_4 [25]),
        .I3(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I4(\Dout_reg[31]_4 [28]),
        .I5(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[25]_inst_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \ALUResult_out_OBUF[25]_inst_i_5 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [14]),
        .O(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \ALUResult_out_OBUF[26]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[31]_inst_i_2_n_0 ),
        .I1(Q[26]),
        .I2(\ALUResult_out_OBUF[26]_inst_i_2_n_0 ),
        .O(D[26]));
  LUT6 #(
    .INIT(64'hFFFFFFFF75FF7575)) 
    \ALUResult_out_OBUF[26]_inst_i_2 
       (.I0(\Dout[26]_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I2(\Dout_reg[26] [15]),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(S_Add_in[26]),
        .I5(\Dout_reg[26]_0 ),
        .O(\ALUResult_out_OBUF[26]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \ALUResult_out_OBUF[27]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[31]_inst_i_2_n_0 ),
        .I1(Q[27]),
        .I2(\ALUResult_out_OBUF[27]_inst_i_2_n_0 ),
        .O(D[27]));
  LUT6 #(
    .INIT(64'hFFFFFFFF75FF7575)) 
    \ALUResult_out_OBUF[27]_inst_i_2 
       (.I0(\Dout[27]_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I2(\Dout_reg[26] [15]),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(S_Add_in[27]),
        .I5(\Dout[27]_i_3_n_0 ),
        .O(\ALUResult_out_OBUF[27]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hC505CDCDFFFFFFFF)) 
    \ALUResult_out_OBUF[28]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_2_n_0 ),
        .I1(\Dout_reg[31]_4 [28]),
        .I2(Q[28]),
        .I3(\Dout_reg[23]_0 ),
        .I4(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I5(\ALUResult_out_OBUF[28]_inst_i_2_n_0 ),
        .O(D[28]));
  LUT6 #(
    .INIT(64'h000000000B000B0B)) 
    \ALUResult_out_OBUF[28]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[31]_inst_i_2_n_0 ),
        .I1(Q[28]),
        .I2(\ALUResult_out_OBUF[25]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(S_Add_in[28]),
        .I5(\Dout_reg[28] ),
        .O(\ALUResult_out_OBUF[28]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7500FFFFFFFFFFFF)) 
    \ALUResult_out_OBUF[29]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[31]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ),
        .I2(\Dout_reg[31]_4 [29]),
        .I3(Q[29]),
        .I4(\ALUResult_out_OBUF[29]_inst_i_3_n_0 ),
        .I5(\ALUResult_out_OBUF[29]_inst_i_4_n_0 ),
        .O(D[29]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ALUResult_out_OBUF[29]_inst_i_2 
       (.I0(\Dout_reg[31]_0 [14]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [12]),
        .O(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBBB0B0BB)) 
    \ALUResult_out_OBUF[29]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I1(\Dout_reg[26] [15]),
        .I2(\Dout[29]_i_2_n_0 ),
        .I3(\Dout_reg[31]_4 [29]),
        .I4(Q[29]),
        .O(\ALUResult_out_OBUF[29]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h0000D0DD)) 
    \ALUResult_out_OBUF[29]_inst_i_4 
       (.I0(\Dout_reg[31]_4 [0]),
        .I1(\Dout_reg[24]_0 ),
        .I2(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I3(S_Add_in[29]),
        .I4(\ALUResult_out_OBUF[29]_inst_i_5_n_0 ),
        .O(\ALUResult_out_OBUF[29]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h04FF0404)) 
    \ALUResult_out_OBUF[29]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I1(\Dout_reg[26] [15]),
        .I2(Q[29]),
        .I3(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I4(\Dout_reg[31]_4 [29]),
        .O(\ALUResult_out_OBUF[29]_inst_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFDFF)) 
    \ALUResult_out_OBUF[2]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[2]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[2]_inst_i_3_n_0 ),
        .I2(\Dout_reg[2]_0 ),
        .I3(\ALUResult_out_OBUF[2]_inst_i_5_n_0 ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hA23FA23F0000A23F)) 
    \ALUResult_out_OBUF[2]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(\Dout_reg[24]_1 ),
        .I2(Q[2]),
        .I3(\Dout_reg[31]_4 [2]),
        .I4(S_Add_in[2]),
        .I5(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .O(\ALUResult_out_OBUF[2]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0202020202AA0202)) 
    \ALUResult_out_OBUF[2]_inst_i_3 
       (.I0(Q[2]),
        .I1(\Dout_reg[26] [2]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [13]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[2]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h20FFA8FF)) 
    \ALUResult_out_OBUF[2]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I1(Q[2]),
        .I2(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I3(\Dout_reg[26] [2]),
        .I4(\ALUResult_out_OBUF[23]_inst_i_8_n_0 ),
        .O(\ALUResult_out_OBUF[2]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hC505CDCDFFFFFFFF)) 
    \ALUResult_out_OBUF[30]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_2_n_0 ),
        .I1(\Dout_reg[31]_4 [30]),
        .I2(Q[30]),
        .I3(\Dout_reg[23]_0 ),
        .I4(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I5(\ALUResult_out_OBUF[30]_inst_i_5_n_0 ),
        .O(D[30]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hF1FFFFFF)) 
    \ALUResult_out_OBUF[30]_inst_i_2 
       (.I0(\Dout_reg[31]_0 [11]),
        .I1(\Dout_reg[31]_0 [13]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(\Dout_reg[26] [15]),
        .O(\ALUResult_out_OBUF[30]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \ALUResult_out_OBUF[30]_inst_i_3 
       (.I0(\Dout_reg[31]_0 [12]),
        .I1(\Dout_reg[31]_0 [11]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [14]),
        .O(\Dout_reg[23]_0 ));
  LUT5 #(
    .INIT(32'hFFFDFDFD)) 
    \ALUResult_out_OBUF[30]_inst_i_4 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [5]),
        .I4(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000000000B000B0B)) 
    \ALUResult_out_OBUF[30]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[31]_inst_i_2_n_0 ),
        .I1(Q[30]),
        .I2(\ALUResult_out_OBUF[25]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(S_Add_in[30]),
        .I5(\Dout_reg[30] ),
        .O(\ALUResult_out_OBUF[30]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFF4FF)) 
    \ALUResult_out_OBUF[31]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[31]_inst_i_2_n_0 ),
        .I1(Q[31]),
        .I2(\ALUResult_out_OBUF[31]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[31]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[31]_inst_i_5_n_0 ),
        .O(D[31]));
  LUT6 #(
    .INIT(64'hFFFFFFFFF4F5C5F5)) 
    \ALUResult_out_OBUF[31]_inst_i_2 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[31]_0 [12]),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(\Dout_reg[26] [15]),
        .I5(\Dout_reg[27]_rep_0 ),
        .O(\ALUResult_out_OBUF[31]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h4FF44444)) 
    \ALUResult_out_OBUF[31]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I1(S_Add_in[31]),
        .I2(\Dout_reg[31]_4 [31]),
        .I3(Q[31]),
        .I4(\Dout_reg[24]_1 ),
        .O(\ALUResult_out_OBUF[31]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFB00FBFB)) 
    \ALUResult_out_OBUF[31]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I1(\Dout_reg[26] [15]),
        .I2(Q[31]),
        .I3(\Dout_reg[24]_0 ),
        .I4(\Dout_reg[31]_4 [2]),
        .O(\ALUResult_out_OBUF[31]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h50D050D0FFFF50D0)) 
    \ALUResult_out_OBUF[31]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(Q[31]),
        .I2(\Dout_reg[31]_4 [31]),
        .I3(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ),
        .I4(\Dout_reg[26] [15]),
        .I5(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .O(\ALUResult_out_OBUF[31]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h555DDDDDFFFFFFFF)) 
    \ALUResult_out_OBUF[3]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[3]_inst_i_2_n_0 ),
        .I1(Q[3]),
        .I2(\Dout_reg[26] [3]),
        .I3(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I5(\ALUResult_out_OBUF[3]_inst_i_3_n_0 ),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h000000005DFD5555)) 
    \ALUResult_out_OBUF[3]_inst_i_2 
       (.I0(\Dout_reg[26] [3]),
        .I1(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I2(Q[3]),
        .I3(\ALUResult_out_OBUF[23]_inst_i_8_n_0 ),
        .I4(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I5(\Dout_reg[3]_1 ),
        .O(\ALUResult_out_OBUF[3]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8A8ACF008A00CFCF)) 
    \ALUResult_out_OBUF[3]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I2(S_Add_in[3]),
        .I3(\Dout[29]_i_2_n_0 ),
        .I4(\Dout_reg[31]_4 [3]),
        .I5(Q[3]),
        .O(\ALUResult_out_OBUF[3]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF57FF5555)) 
    \ALUResult_out_OBUF[4]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[4]_inst_i_2_n_0 ),
        .I1(\Dout_reg[26] [4]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I4(Q[4]),
        .I5(\ALUResult_out_OBUF[4]_inst_i_3_n_0 ),
        .O(D[4]));
  LUT5 #(
    .INIT(32'h0000D0DD)) 
    \ALUResult_out_OBUF[4]_inst_i_2 
       (.I0(\Dout_reg[31]_4 [4]),
        .I1(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I2(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I3(S_Add_in[4]),
        .I4(\ALUResult_out_OBUF[4]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[4]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF14FF14FFFFFF14)) 
    \ALUResult_out_OBUF[4]_inst_i_3 
       (.I0(\Dout[29]_i_2_n_0 ),
        .I1(\Dout_reg[31]_4 [4]),
        .I2(Q[4]),
        .I3(\ALUResult_out_OBUF[4]_inst_i_5_n_0 ),
        .I4(\Dout_reg[31]_4 [7]),
        .I5(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[4]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00A0000000080000)) 
    \ALUResult_out_OBUF[4]_inst_i_4 
       (.I0(\Dout_reg[26] [4]),
        .I1(Q[4]),
        .I2(\Dout_reg[31]_0 [12]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [14]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[4]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h80808F80)) 
    \ALUResult_out_OBUF[4]_inst_i_5 
       (.I0(\Dout_reg[31]_4 [4]),
        .I1(\Dout_reg[23]_0 ),
        .I2(Q[4]),
        .I3(\Dout_reg[26] [4]),
        .I4(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[4]_inst_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h5700FFFF)) 
    \ALUResult_out_OBUF[5]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I1(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I2(\Dout_reg[26] [5]),
        .I3(Q[5]),
        .I4(\ALUResult_out_OBUF[5]_inst_i_2_n_0 ),
        .O(D[5]));
  LUT6 #(
    .INIT(64'h0000000051005151)) 
    \ALUResult_out_OBUF[5]_inst_i_2 
       (.I0(\Dout_reg[5] ),
        .I1(\Dout_reg[26] [5]),
        .I2(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(S_Add_in[5]),
        .I5(\ALUResult_out_OBUF[5]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[5]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF4554455FFFF4455)) 
    \ALUResult_out_OBUF[5]_inst_i_4 
       (.I0(\ALUResult_out_OBUF[5]_inst_i_5_n_0 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_8_n_0 ),
        .I2(\Dout_reg[23]_0 ),
        .I3(Q[5]),
        .I4(\Dout_reg[31]_4 [5]),
        .I5(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[5]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h51515155FFFFFFFF)) 
    \ALUResult_out_OBUF[5]_inst_i_5 
       (.I0(Q[5]),
        .I1(\Dout_reg[31]_0 [14]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [13]),
        .I4(\Dout_reg[31]_0 [11]),
        .I5(\Dout_reg[26] [5]),
        .O(\ALUResult_out_OBUF[5]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h555DDDDDFFFFFFFF)) 
    \ALUResult_out_OBUF[6]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[6]_inst_i_2_n_0 ),
        .I1(Q[6]),
        .I2(\Dout_reg[26] [6]),
        .I3(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I5(\ALUResult_out_OBUF[6]_inst_i_3_n_0 ),
        .O(D[6]));
  LUT6 #(
    .INIT(64'h000000005DFD5555)) 
    \ALUResult_out_OBUF[6]_inst_i_2 
       (.I0(\Dout_reg[26] [6]),
        .I1(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I2(Q[6]),
        .I3(\ALUResult_out_OBUF[23]_inst_i_8_n_0 ),
        .I4(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I5(\Dout_reg[6]_1 ),
        .O(\ALUResult_out_OBUF[6]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8A8ACF008A00CFCF)) 
    \ALUResult_out_OBUF[6]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I2(S_Add_in[6]),
        .I3(\Dout[29]_i_2_n_0 ),
        .I4(\Dout_reg[31]_4 [6]),
        .I5(Q[6]),
        .O(\ALUResult_out_OBUF[6]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h77777F77FF77FF77)) 
    \ALUResult_out_OBUF[7]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[7]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[7]_inst_i_3_n_0 ),
        .I2(Q[7]),
        .I3(\Dout_reg[26] [7]),
        .I4(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I5(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .O(D[7]));
  LUT6 #(
    .INIT(64'h000000008A008A8A)) 
    \ALUResult_out_OBUF[7]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[7]_inst_i_5_n_0 ),
        .I1(\Dout_reg[24]_0 ),
        .I2(\Dout_reg[31]_4 [10]),
        .I3(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I4(S_Add_in[7]),
        .I5(\ALUResult_out_OBUF[7]_inst_i_6_n_0 ),
        .O(\ALUResult_out_OBUF[7]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hD7)) 
    \ALUResult_out_OBUF[7]_inst_i_3 
       (.I0(\Dout_reg[24]_1 ),
        .I1(Q[7]),
        .I2(\Dout_reg[31]_4 [7]),
        .O(\ALUResult_out_OBUF[7]_inst_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'hDDDF)) 
    \ALUResult_out_OBUF[7]_inst_i_4 
       (.I0(\Dout_reg[31]_0 [14]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [13]),
        .I3(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h7400FFFF)) 
    \ALUResult_out_OBUF[7]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_8_n_0 ),
        .I1(\Dout_reg[26] [7]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I4(Q[7]),
        .O(\ALUResult_out_OBUF[7]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0002FFFF00000000)) 
    \ALUResult_out_OBUF[7]_inst_i_6 
       (.I0(Q[7]),
        .I1(\Dout_reg[31]_0 [12]),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I4(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I5(\Dout_reg[31]_4 [7]),
        .O(\ALUResult_out_OBUF[7]_inst_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF4F44FFFFFFFF)) 
    \ALUResult_out_OBUF[8]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_2_n_0 ),
        .I1(Q[8]),
        .I2(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I3(\Dout_reg[26] [7]),
        .I4(\ALUResult_out_OBUF[8]_inst_i_4_n_0 ),
        .I5(\ALUResult_out_OBUF[8]_inst_i_5_n_0 ),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hFFFFDDCDFFFFDD1D)) 
    \ALUResult_out_OBUF[8]_inst_i_2 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[31]_0 [11]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(\Dout_reg[27]_rep_0 ),
        .I5(\Dout_reg[26] [7]),
        .O(\ALUResult_out_OBUF[8]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'hBFFF)) 
    \ALUResult_out_OBUF[8]_inst_i_3 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [12]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [11]),
        .O(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h14141F14)) 
    \ALUResult_out_OBUF[8]_inst_i_4 
       (.I0(\Dout[29]_i_2_n_0 ),
        .I1(\Dout_reg[31]_4 [8]),
        .I2(Q[8]),
        .I3(\Dout_reg[26] [7]),
        .I4(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .O(\ALUResult_out_OBUF[8]_inst_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hA2A200A2)) 
    \ALUResult_out_OBUF[8]_inst_i_5 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_6_n_0 ),
        .I1(S_Add_in[8]),
        .I2(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I3(\Dout_reg[31]_4 [11]),
        .I4(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[8]_inst_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFD0000FFFFFFFF)) 
    \ALUResult_out_OBUF[8]_inst_i_6 
       (.I0(Q[8]),
        .I1(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I5(\Dout_reg[31]_4 [8]),
        .O(\ALUResult_out_OBUF[8]_inst_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h4FFF)) 
    \ALUResult_out_OBUF[9]_inst_i_1 
       (.I0(\ALUResult_out_OBUF[10]_inst_i_2_n_0 ),
        .I1(Q[9]),
        .I2(\ALUResult_out_OBUF[9]_inst_i_2_n_0 ),
        .I3(\ALUResult_out_OBUF[9]_inst_i_3_n_0 ),
        .O(D[9]));
  LUT6 #(
    .INIT(64'hAF00AF8F008FAF8F)) 
    \ALUResult_out_OBUF[9]_inst_i_2 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_3_n_0 ),
        .I1(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I2(\Dout_reg[26] [8]),
        .I3(Q[9]),
        .I4(\Dout_reg[24]_1 ),
        .I5(\Dout_reg[31]_4 [9]),
        .O(\ALUResult_out_OBUF[9]_inst_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hA2A200A2)) 
    \ALUResult_out_OBUF[9]_inst_i_3 
       (.I0(\ALUResult_out_OBUF[9]_inst_i_4_n_0 ),
        .I1(S_Add_in[9]),
        .I2(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I3(\Dout_reg[31]_4 [12]),
        .I4(\Dout_reg[24]_0 ),
        .O(\ALUResult_out_OBUF[9]_inst_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFD0000FFFFFFFF)) 
    \ALUResult_out_OBUF[9]_inst_i_4 
       (.I0(Q[9]),
        .I1(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I5(\Dout_reg[31]_4 [9]),
        .O(\ALUResult_out_OBUF[9]_inst_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAACFAAC0FFCFFFC0)) 
    \Dout[0]_i_1 
       (.I0(\Dout[0]_i_2_n_0 ),
        .I1(\Dout_reg[31]_5 [0]),
        .I2(\Dout_reg[21]_0 [2]),
        .I3(\Dout_reg[31]_3 ),
        .I4(\Dout_reg[31]_6 [0]),
        .I5(\ALUResult_out_OBUF[0]_inst_i_2_n_0 ),
        .O(\Dout_reg[31]_1 [0]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Dout[0]_i_1__0 
       (.I0(\Dout_reg[31]_0 [0]),
        .I1(\Dout_reg[27]_rep_0 ),
        .O(\Dout_reg[27]_rep_1 [0]));
  LUT6 #(
    .INIT(64'h0202020202AA0202)) 
    \Dout[0]_i_2 
       (.I0(Q[0]),
        .I1(\Dout_reg[26] [0]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [13]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\Dout[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF4FFF0F0FFFFF0F0)) 
    \Dout[10]_i_1 
       (.I0(\ALUResult_out_OBUF[10]_inst_i_2_n_0 ),
        .I1(Q[10]),
        .I2(\Dout[10]_i_2_n_0 ),
        .I3(\Dout[10]_i_3_n_0 ),
        .I4(\Dout_reg[31]_3 ),
        .I5(\ALUResult_out_OBUF[10]_inst_i_4_n_0 ),
        .O(\Dout_reg[31]_1 [10]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[10]_i_1__0 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [5]),
        .O(\Dout_reg[27]_rep_1 [7]));
  LUT6 #(
    .INIT(64'hAFAEAAAEFFFEFAFE)) 
    \Dout[10]_i_2 
       (.I0(\Dout[10]_i_4_n_0 ),
        .I1(\Dout_reg[31]_6 [8]),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout_reg[21]_0 [2]),
        .I4(\Dout_reg[31]_5 [6]),
        .I5(\Dout[10]_i_5_n_0 ),
        .O(\Dout[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hD7)) 
    \Dout[10]_i_3 
       (.I0(\Dout_reg[24]_1 ),
        .I1(Q[10]),
        .I2(\Dout_reg[31]_4 [10]),
        .O(\Dout[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \Dout[10]_i_4 
       (.I0(\Dout_reg[26] [8]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(\Dout_reg[31]_0 [14]),
        .I5(\Dout_reg[31]_0 [12]),
        .O(\Dout[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF1FFFFFF)) 
    \Dout[10]_i_5 
       (.I0(\Dout_reg[31]_0 [11]),
        .I1(\Dout_reg[31]_0 [13]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(\Dout_reg[26] [8]),
        .I5(Q[10]),
        .O(\Dout[10]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFF7FFF7FFF7F0F0)) 
    \Dout[14]_i_1 
       (.I0(\ALUResult_out_OBUF[14]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[14]_inst_i_3_n_0 ),
        .I2(\Dout_reg[14]_0 ),
        .I3(\Dout[14]_i_3_n_0 ),
        .I4(\Dout_reg[21]_0 [3]),
        .I5(\Dout_reg[21]_0 [4]),
        .O(\Dout_reg[31]_1 [11]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[14]_i_1__0 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [6]),
        .O(\Dout_reg[27]_rep_1 [8]));
  LUT6 #(
    .INIT(64'h0202020202AA0202)) 
    \Dout[14]_i_3 
       (.I0(Q[14]),
        .I1(\Dout_reg[26] [9]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [13]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\Dout[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFF7FFF7FFF7F0F0)) 
    \Dout[15]_i_1 
       (.I0(\ALUResult_out_OBUF[15]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[15]_inst_i_3_n_0 ),
        .I2(\Dout_reg[15]_1 ),
        .I3(\Dout[15]_i_3_n_0 ),
        .I4(\Dout_reg[21]_0 [3]),
        .I5(\Dout_reg[21]_0 [4]),
        .O(\Dout_reg[31]_1 [12]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[15]_i_1__0 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [7]),
        .O(\Dout_reg[27]_rep_1 [9]));
  LUT6 #(
    .INIT(64'h0202020202AA0202)) 
    \Dout[15]_i_3 
       (.I0(Q[15]),
        .I1(\Dout_reg[26] [10]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [13]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\Dout[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFDD00DF00)) 
    \Dout[16]_i_1 
       (.I0(\ALUResult_out_OBUF[16]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[16]_inst_i_3_n_0 ),
        .I2(\Dout_reg[16] ),
        .I3(\Dout_reg[31]_3 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I5(\Dout[16]_i_3_n_0 ),
        .O(\Dout_reg[31]_1 [13]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[16]_i_1__0 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [8]),
        .O(\Dout_reg[27]_rep_1 [10]));
  LUT6 #(
    .INIT(64'h4444F0FF4444F000)) 
    \Dout[16]_i_3 
       (.I0(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I1(Q[16]),
        .I2(\Dout_reg[31]_5 [7]),
        .I3(\Dout_reg[21]_0 [2]),
        .I4(\Dout_reg[31]_3 ),
        .I5(\Dout_reg[31]_6 [9]),
        .O(\Dout[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFF00FFFFFF00)) 
    \Dout[17]_i_1 
       (.I0(\ALUResult_out_OBUF[17]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[17]_inst_i_4_n_0 ),
        .I2(\Dout[17]_i_2_n_0 ),
        .I3(\Dout[17]_i_3_n_0 ),
        .I4(\Dout_reg[31]_3 ),
        .I5(\ALUResult_out_OBUF[17]_inst_i_5_n_0 ),
        .O(\Dout_reg[31]_1 [14]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[17]_i_1__0 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [9]),
        .O(\Dout_reg[27]_rep_1 [11]));
  LUT6 #(
    .INIT(64'h0004000000000000)) 
    \Dout[17]_i_2 
       (.I0(\Dout_reg[31]_0 [11]),
        .I1(\Dout_reg[31]_0 [14]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(Q[17]),
        .I5(\Dout_reg[26] [12]),
        .O(\Dout[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hDDDDF0FFDDDDF000)) 
    \Dout[17]_i_3 
       (.I0(\Dout[17]_i_4_n_0 ),
        .I1(\Dout[17]_i_5_n_0 ),
        .I2(\Dout_reg[31]_5 [8]),
        .I3(\Dout_reg[21]_0 [2]),
        .I4(\Dout_reg[31]_3 ),
        .I5(\Dout_reg[31]_6 [10]),
        .O(\Dout[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFEFFFFFFF)) 
    \Dout[17]_i_4 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [12]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(Q[17]),
        .I5(\Dout_reg[26] [12]),
        .O(\Dout[17]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \Dout[17]_i_5 
       (.I0(Q[17]),
        .I1(\Dout_reg[31]_0 [11]),
        .I2(\Dout_reg[31]_0 [13]),
        .I3(\Dout_reg[27]_rep_0 ),
        .O(\Dout[17]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFE0FFEE)) 
    \Dout[18]_i_1 
       (.I0(\Dout_reg[21]_0 [4]),
        .I1(\Dout_reg[21]_0 [3]),
        .I2(\Dout[18]_i_2_n_0 ),
        .I3(\Dout_reg[18]_0 ),
        .I4(\ALUResult_out_OBUF[18]_inst_i_2_n_0 ),
        .O(\Dout_reg[31]_1 [15]));
  LUT6 #(
    .INIT(64'h00000008AAAAAAAA)) 
    \Dout[18]_i_2 
       (.I0(\Dout_reg[31]_4 [18]),
        .I1(Q[18]),
        .I2(\Dout_reg[31]_0 [12]),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I5(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .O(\Dout[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7F4C7F7F7F4C4C4C)) 
    \Dout[19]_i_1 
       (.I0(\ALUResult_out_OBUF[19]_inst_i_2_n_0 ),
        .I1(\Dout_reg[31]_3 ),
        .I2(\Dout[19]_i_2_n_0 ),
        .I3(\Dout_reg[31]_5 [9]),
        .I4(\Dout_reg[21]_0 [2]),
        .I5(\Dout_reg[31]_6 [11]),
        .O(\Dout_reg[31]_1 [16]));
  LUT5 #(
    .INIT(32'hB833BBBB)) 
    \Dout[19]_i_2 
       (.I0(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .I1(Q[19]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [19]),
        .I4(\Dout_reg[23]_1 ),
        .O(\Dout[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFF7FFF7FFF7F0F0)) 
    \Dout[1]_i_1 
       (.I0(\ALUResult_out_OBUF[1]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[1]_inst_i_3_n_0 ),
        .I2(\Dout_reg[1]_0 ),
        .I3(\Dout[1]_i_3_n_0 ),
        .I4(\Dout_reg[21]_0 [3]),
        .I5(\Dout_reg[21]_0 [4]),
        .O(\Dout_reg[31]_1 [1]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \Dout[1]_i_1__0 
       (.I0(\Dout_reg[31]_0 [1]),
        .I1(\Dout_reg[27]_rep_0 ),
        .O(\Dout_reg[27]_rep_1 [1]));
  LUT6 #(
    .INIT(64'h0202020202AA0202)) 
    \Dout[1]_i_3 
       (.I0(Q[1]),
        .I1(\Dout_reg[26] [1]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [13]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\Dout[1]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFE0FFEE)) 
    \Dout[20]_i_1 
       (.I0(\Dout_reg[21]_0 [4]),
        .I1(\Dout_reg[21]_0 [3]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_2_n_0 ),
        .I3(\Dout[20]_i_2_n_0 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_5_n_0 ),
        .O(\Dout_reg[31]_1 [17]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[20]_i_1__0 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [10]),
        .O(\Dout_reg[27]_rep_1 [12]));
  LUT6 #(
    .INIT(64'hFFFFFFFF50007000)) 
    \Dout[20]_i_2 
       (.I0(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I1(\Dout_reg[26] [13]),
        .I2(Q[20]),
        .I3(\Dout_reg[31]_3 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I5(\Dout_reg[20] ),
        .O(\Dout[20]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFE0FFEE)) 
    \Dout[21]_i_1 
       (.I0(\Dout_reg[21]_0 [4]),
        .I1(\Dout_reg[21]_0 [3]),
        .I2(\Dout[21]_i_2_n_0 ),
        .I3(\Dout_reg[21]_1 ),
        .I4(\ALUResult_out_OBUF[21]_inst_i_2_n_0 ),
        .O(\Dout_reg[31]_1 [18]));
  LUT6 #(
    .INIT(64'h00000008AAAAAAAA)) 
    \Dout[21]_i_2 
       (.I0(\Dout_reg[31]_4 [21]),
        .I1(Q[21]),
        .I2(\Dout_reg[31]_0 [12]),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I5(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .O(\Dout[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7F4C7F7F7F4C4C4C)) 
    \Dout[22]_i_1 
       (.I0(\ALUResult_out_OBUF[22]_inst_i_2_n_0 ),
        .I1(\Dout_reg[31]_3 ),
        .I2(\Dout[22]_i_2_n_0 ),
        .I3(\Dout_reg[31]_5 [10]),
        .I4(\Dout_reg[21]_0 [2]),
        .I5(\Dout_reg[31]_6 [12]),
        .O(\Dout_reg[31]_1 [19]));
  LUT5 #(
    .INIT(32'hB833BBBB)) 
    \Dout[22]_i_2 
       (.I0(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .I1(Q[22]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [22]),
        .I4(\Dout_reg[23]_1 ),
        .O(\Dout[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFDDD0)) 
    \Dout[23]_i_1 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_3_n_0 ),
        .I2(\Dout_reg[21]_0 [4]),
        .I3(\Dout_reg[21]_0 [3]),
        .I4(\Dout[23]_i_2_n_0 ),
        .O(\Dout_reg[31]_1 [20]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[23]_i_1__0 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [11]),
        .O(\Dout_reg[27]_rep_1 [13]));
  LUT6 #(
    .INIT(64'hF8F8FAF8FAFAFAFA)) 
    \Dout[23]_i_2 
       (.I0(\Dout_reg[31]_3 ),
        .I1(\ALUResult_out_OBUF[23]_inst_i_6_n_0 ),
        .I2(\Dout_reg[23]_3 ),
        .I3(S_Add_in[23]),
        .I4(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I5(\ALUResult_out_OBUF[23]_inst_i_4_n_0 ),
        .O(\Dout[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7F4C7F7F7F4C4C4C)) 
    \Dout[24]_i_1 
       (.I0(\ALUResult_out_OBUF[24]_inst_i_2_n_0 ),
        .I1(\Dout_reg[31]_3 ),
        .I2(\Dout[24]_i_2_n_0 ),
        .I3(\Dout_reg[31]_5 [11]),
        .I4(\Dout_reg[21]_0 [2]),
        .I5(\Dout_reg[31]_6 [13]),
        .O(\Dout_reg[31]_1 [21]));
  LUT5 #(
    .INIT(32'hB833BBBB)) 
    \Dout[24]_i_2 
       (.I0(\ALUResult_out_OBUF[24]_inst_i_4_n_0 ),
        .I1(Q[24]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [24]),
        .I4(\Dout_reg[23]_1 ),
        .O(\Dout[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFF570000)) 
    \Dout[25]_i_1 
       (.I0(\ALUResult_out_OBUF[25]_inst_i_4_n_0 ),
        .I1(Q[25]),
        .I2(\ALUResult_out_OBUF[30]_inst_i_2_n_0 ),
        .I3(\Dout[25]_i_2_n_0 ),
        .I4(\Dout_reg[31]_3 ),
        .I5(\Dout[25]_i_3_n_0 ),
        .O(\Dout_reg[31]_1 [22]));
  LUT3 #(
    .INIT(8'h28)) 
    \Dout[25]_i_2 
       (.I0(\Dout_reg[24]_1 ),
        .I1(Q[25]),
        .I2(\Dout_reg[31]_4 [25]),
        .O(\Dout[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFDFDDCCCC)) 
    \Dout[25]_i_3 
       (.I0(\Dout[31]_i_6_n_0 ),
        .I1(\Dout_reg[25]_0 ),
        .I2(\Dout[29]_i_5_n_0 ),
        .I3(\Dout_reg[31]_4 [25]),
        .I4(Q[25]),
        .I5(\Dout[31]_i_7_n_0 ),
        .O(\Dout[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFD0FFD0FFFFFFD0)) 
    \Dout[26]_i_1 
       (.I0(\Dout[26]_i_2_n_0 ),
        .I1(\Dout_reg[26]_0 ),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout[26]_i_4_n_0 ),
        .I4(Q[26]),
        .I5(\Dout[31]_i_6_n_0 ),
        .O(\Dout_reg[31]_1 [23]));
  LUT6 #(
    .INIT(64'hF000B0B0FFFFBBBB)) 
    \Dout[26]_i_2 
       (.I0(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I1(\Dout_reg[26] [15]),
        .I2(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I3(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ),
        .I4(Q[26]),
        .I5(\Dout_reg[31]_4 [26]),
        .O(\Dout[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF5400)) 
    \Dout[26]_i_4 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I1(\Dout_reg[21]_0 [3]),
        .I2(\Dout_reg[21]_0 [4]),
        .I3(S_Add_in[26]),
        .I4(\Dout_reg[26]_1 ),
        .I5(\Dout[31]_i_7_n_0 ),
        .O(\Dout[26]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFD0FFD0FFFFFFD0)) 
    \Dout[27]_i_1 
       (.I0(\Dout[27]_i_2_n_0 ),
        .I1(\Dout[27]_i_3_n_0 ),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout[27]_i_4_n_0 ),
        .I4(Q[27]),
        .I5(\Dout[31]_i_6_n_0 ),
        .O(\Dout_reg[31]_1 [24]));
  LUT6 #(
    .INIT(64'hF000B0B0FFFFBBBB)) 
    \Dout[27]_i_2 
       (.I0(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .I1(\Dout_reg[26] [15]),
        .I2(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I3(\ALUResult_out_OBUF[29]_inst_i_2_n_0 ),
        .I4(Q[27]),
        .I5(\Dout_reg[31]_4 [27]),
        .O(\Dout[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h14FF1414)) 
    \Dout[27]_i_3 
       (.I0(\Dout[29]_i_2_n_0 ),
        .I1(\Dout_reg[31]_4 [27]),
        .I2(Q[27]),
        .I3(\Dout_reg[24]_0 ),
        .I4(\Dout_reg[31]_4 [30]),
        .O(\Dout[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF5400)) 
    \Dout[27]_i_4 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I1(\Dout_reg[21]_0 [3]),
        .I2(\Dout_reg[21]_0 [4]),
        .I3(S_Add_in[27]),
        .I4(\Dout_reg[27]_2 ),
        .I5(\Dout[31]_i_7_n_0 ),
        .O(\Dout[27]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFD0FFD0FFFFFFD0)) 
    \Dout[28]_i_1 
       (.I0(\Dout[28]_i_2_n_0 ),
        .I1(\Dout_reg[28] ),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout[28]_i_4_n_0 ),
        .I4(Q[28]),
        .I5(\Dout[31]_i_6_n_0 ),
        .O(\Dout_reg[31]_1 [25]));
  LUT6 #(
    .INIT(64'h2AFF2AFF20F02AFF)) 
    \Dout[28]_i_2 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(\Dout_reg[23]_0 ),
        .I2(Q[28]),
        .I3(\Dout_reg[31]_4 [28]),
        .I4(\Dout_reg[26] [15]),
        .I5(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .O(\Dout[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF5400)) 
    \Dout[28]_i_4 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I1(\Dout_reg[21]_0 [3]),
        .I2(\Dout_reg[21]_0 [4]),
        .I3(S_Add_in[28]),
        .I4(\Dout_reg[28]_0 ),
        .I5(\Dout[31]_i_7_n_0 ),
        .O(\Dout[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF57750000)) 
    \Dout[29]_i_1 
       (.I0(\ALUResult_out_OBUF[29]_inst_i_4_n_0 ),
        .I1(\Dout[29]_i_2_n_0 ),
        .I2(\Dout_reg[31]_4 [29]),
        .I3(Q[29]),
        .I4(\Dout_reg[31]_3 ),
        .I5(\Dout[29]_i_3_n_0 ),
        .O(\Dout_reg[31]_1 [26]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \Dout[29]_i_2 
       (.I0(\Dout_reg[31]_0 [14]),
        .I1(\Dout_reg[31]_0 [12]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(\Dout_reg[31]_0 [13]),
        .O(\Dout[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFDFDDCCCC)) 
    \Dout[29]_i_3 
       (.I0(\Dout[31]_i_6_n_0 ),
        .I1(\Dout_reg[29] ),
        .I2(\Dout[29]_i_5_n_0 ),
        .I3(\Dout_reg[31]_4 [29]),
        .I4(Q[29]),
        .I5(\Dout[31]_i_7_n_0 ),
        .O(\Dout[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFEFFFEFFFF)) 
    \Dout[29]_i_5 
       (.I0(\Dout_reg[31]_0 [12]),
        .I1(\Dout_reg[31]_0 [11]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(\Dout_reg[21]_0 [3]),
        .I5(\Dout_reg[21]_0 [4]),
        .O(\Dout[29]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFDFFF00FF00)) 
    \Dout[2]_i_1 
       (.I0(\ALUResult_out_OBUF[2]_inst_i_5_n_0 ),
        .I1(\Dout_reg[2]_0 ),
        .I2(\ALUResult_out_OBUF[2]_inst_i_2_n_0 ),
        .I3(\Dout_reg[2]_1 ),
        .I4(\ALUResult_out_OBUF[2]_inst_i_3_n_0 ),
        .I5(\Dout_reg[31]_3 ),
        .O(\Dout_reg[31]_1 [2]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[2]_i_1__0 
       (.I0(\Dout_reg[31]_0 [0]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [2]),
        .O(\Dout_reg[27]_rep_1 [2]));
  LUT6 #(
    .INIT(64'hFFD0FFD0FFFFFFD0)) 
    \Dout[30]_i_1 
       (.I0(\Dout[30]_i_2_n_0 ),
        .I1(\Dout_reg[30] ),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout[30]_i_4_n_0 ),
        .I4(Q[30]),
        .I5(\Dout[31]_i_6_n_0 ),
        .O(\Dout_reg[31]_1 [27]));
  LUT6 #(
    .INIT(64'h2AFF2AFF20F02AFF)) 
    \Dout[30]_i_2 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(\Dout_reg[23]_0 ),
        .I2(Q[30]),
        .I3(\Dout_reg[31]_4 [30]),
        .I4(\Dout_reg[26] [15]),
        .I5(\ALUResult_out_OBUF[7]_inst_i_4_n_0 ),
        .O(\Dout[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF5400)) 
    \Dout[30]_i_4 
       (.I0(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .I1(\Dout_reg[21]_0 [3]),
        .I2(\Dout_reg[21]_0 [4]),
        .I3(S_Add_in[30]),
        .I4(\Dout_reg[30]_0 ),
        .I5(\Dout[31]_i_7_n_0 ),
        .O(\Dout[30]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \Dout[31]_i_1__0 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout_reg[31]_0 [12]),
        .O(\Dout_reg[27]_rep_1 [14]));
  LUT6 #(
    .INIT(64'hFF70FF70FFFFFF70)) 
    \Dout[31]_i_2 
       (.I0(\Dout[31]_i_3_n_0 ),
        .I1(\ALUResult_out_OBUF[31]_inst_i_4_n_0 ),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout[31]_i_5_n_0 ),
        .I4(Q[31]),
        .I5(\Dout[31]_i_6_n_0 ),
        .O(\Dout_reg[31]_1 [28]));
  LUT6 #(
    .INIT(64'hA23FA23F0000A23F)) 
    \Dout[31]_i_3 
       (.I0(\ALUResult_out_OBUF[30]_inst_i_4_n_0 ),
        .I1(\Dout_reg[24]_1 ),
        .I2(Q[31]),
        .I3(\Dout_reg[31]_4 [31]),
        .I4(S_Add_in[31]),
        .I5(\ALUResult_out_OBUF[23]_inst_i_5_n_0 ),
        .O(\Dout[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF3202)) 
    \Dout[31]_i_5 
       (.I0(\Dout_reg[31]_6 [14]),
        .I1(\Dout_reg[31]_3 ),
        .I2(\Dout_reg[21]_0 [2]),
        .I3(\Dout_reg[31]_5 [12]),
        .I4(\Dout[31]_i_7_n_0 ),
        .I5(\Dout[31]_i_8_n_0 ),
        .O(\Dout[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEFFE0E0E0FF)) 
    \Dout[31]_i_6 
       (.I0(\Dout_reg[27]_rep_0 ),
        .I1(\Dout[31]_i_9_n_0 ),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\Dout_reg[21]_0 [3]),
        .I4(\Dout_reg[21]_0 [4]),
        .I5(\Dout_reg[26] [15]),
        .O(\Dout[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \Dout[31]_i_7 
       (.I0(\Dout_reg[26] [15]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(\Dout_reg[31]_0 [14]),
        .I5(\Dout_reg[31]_0 [12]),
        .O(\Dout[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    \Dout[31]_i_8 
       (.I0(Q[31]),
        .I1(\Dout_reg[31]_4 [31]),
        .I2(\Dout_reg[31]_3 ),
        .I3(\ALUResult_out_OBUF[25]_inst_i_5_n_0 ),
        .I4(\Dout_reg[31]_0 [11]),
        .I5(\Dout_reg[31]_0 [12]),
        .O(\Dout[31]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hF4F5F5F5)) 
    \Dout[31]_i_9 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[31]_0 [12]),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(\Dout_reg[26] [15]),
        .O(\Dout[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFF7FFF7FFF7F0F0)) 
    \Dout[3]_i_1 
       (.I0(\ALUResult_out_OBUF[3]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[3]_inst_i_3_n_0 ),
        .I2(\Dout_reg[3]_0 ),
        .I3(\Dout[3]_i_3_n_0 ),
        .I4(\Dout_reg[21]_0 [3]),
        .I5(\Dout_reg[21]_0 [4]),
        .O(\Dout_reg[31]_1 [3]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[3]_i_1__0 
       (.I0(\Dout_reg[31]_0 [1]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [3]),
        .O(\Dout_reg[27]_rep_1 [3]));
  LUT6 #(
    .INIT(64'h0202020202AA0202)) 
    \Dout[3]_i_3 
       (.I0(Q[3]),
        .I1(\Dout_reg[26] [3]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [13]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\Dout[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFDD00DF00)) 
    \Dout[4]_i_1 
       (.I0(\ALUResult_out_OBUF[4]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[4]_inst_i_3_n_0 ),
        .I2(\Dout_reg[4]_0 ),
        .I3(\Dout_reg[31]_3 ),
        .I4(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I5(\Dout[4]_i_3_n_0 ),
        .O(\Dout_reg[31]_1 [4]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[4]_i_1__0 
       (.I0(\Dout_reg[31]_0 [2]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [4]),
        .O(\Dout_reg[27]_rep_1 [4]));
  LUT6 #(
    .INIT(64'h4444F0FF4444F000)) 
    \Dout[4]_i_3 
       (.I0(\ALUResult_out_OBUF[20]_inst_i_4_n_0 ),
        .I1(Q[4]),
        .I2(\Dout_reg[31]_5 [1]),
        .I3(\Dout_reg[21]_0 [2]),
        .I4(\Dout_reg[31]_3 ),
        .I5(\Dout_reg[31]_6 [3]),
        .O(\Dout[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFACA0ACFFFCF0FC)) 
    \Dout[5]_i_1 
       (.I0(\Dout[5]_i_2_n_0 ),
        .I1(\Dout_reg[31]_6 [4]),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout_reg[21]_0 [2]),
        .I4(\Dout_reg[31]_5 [2]),
        .I5(\ALUResult_out_OBUF[5]_inst_i_2_n_0 ),
        .O(\Dout_reg[31]_1 [5]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[5]_i_1__0 
       (.I0(\Dout_reg[31]_0 [3]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [5]),
        .O(\Dout_reg[27]_rep_1 [5]));
  LUT6 #(
    .INIT(64'h0202020202AA0202)) 
    \Dout[5]_i_2 
       (.I0(Q[5]),
        .I1(\Dout_reg[26] [5]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [13]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\Dout[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFF7FFF7FFF7F0F0)) 
    \Dout[6]_i_1 
       (.I0(\ALUResult_out_OBUF[6]_inst_i_2_n_0 ),
        .I1(\ALUResult_out_OBUF[6]_inst_i_3_n_0 ),
        .I2(\Dout_reg[6]_0 ),
        .I3(\Dout[6]_i_3_n_0 ),
        .I4(\Dout_reg[21]_0 [3]),
        .I5(\Dout_reg[21]_0 [4]),
        .O(\Dout_reg[31]_1 [6]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[6]_i_1__0 
       (.I0(\Dout_reg[31]_0 [4]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [5]),
        .O(\Dout_reg[27]_rep_1 [6]));
  LUT6 #(
    .INIT(64'h0202020202AA0202)) 
    \Dout[6]_i_3 
       (.I0(Q[6]),
        .I1(\Dout_reg[26] [6]),
        .I2(\ALUResult_out_OBUF[20]_inst_i_3_n_0 ),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[31]_0 [13]),
        .I5(\Dout_reg[31]_0 [11]),
        .O(\Dout[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF4CC44444)) 
    \Dout[7]_i_1 
       (.I0(\ALUResult_out_OBUF[7]_inst_i_2_n_0 ),
        .I1(\Dout_reg[31]_3 ),
        .I2(\Dout_reg[31]_4 [7]),
        .I3(Q[7]),
        .I4(\Dout_reg[24]_1 ),
        .I5(\Dout[7]_i_3_n_0 ),
        .O(\Dout_reg[31]_1 [7]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'h00000004)) 
    \Dout[7]_i_2 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[31]_0 [11]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [12]),
        .I4(\Dout_reg[31]_0 [14]),
        .O(\Dout_reg[24]_1 ));
  LUT6 #(
    .INIT(64'hAFAEAAAEFFFEFAFE)) 
    \Dout[7]_i_3 
       (.I0(\Dout[7]_i_4_n_0 ),
        .I1(\Dout_reg[31]_6 [5]),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout_reg[21]_0 [2]),
        .I4(\Dout_reg[31]_5 [3]),
        .I5(\Dout[7]_i_5_n_0 ),
        .O(\Dout[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \Dout[7]_i_4 
       (.I0(\Dout_reg[26] [7]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout_reg[31]_0 [11]),
        .I4(\Dout_reg[31]_0 [14]),
        .I5(\Dout_reg[31]_0 [12]),
        .O(\Dout[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF1FFFFFF)) 
    \Dout[7]_i_5 
       (.I0(\Dout_reg[31]_0 [11]),
        .I1(\Dout_reg[31]_0 [13]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(\Dout_reg[26] [7]),
        .I5(Q[7]),
        .O(\Dout[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFF4FFFFF0F0F0F0)) 
    \Dout[8]_i_1 
       (.I0(\ALUResult_out_OBUF[8]_inst_i_2_n_0 ),
        .I1(Q[8]),
        .I2(\Dout[8]_i_2_n_0 ),
        .I3(\ALUResult_out_OBUF[8]_inst_i_4_n_0 ),
        .I4(\ALUResult_out_OBUF[8]_inst_i_5_n_0 ),
        .I5(\Dout_reg[31]_3 ),
        .O(\Dout_reg[31]_1 [8]));
  LUT6 #(
    .INIT(64'h4444F4FF4444F444)) 
    \Dout[8]_i_2 
       (.I0(\Dout[8]_i_3_n_0 ),
        .I1(\Dout_reg[26] [7]),
        .I2(\Dout_reg[31]_5 [4]),
        .I3(\Dout_reg[21]_0 [2]),
        .I4(\Dout_reg[31]_3 ),
        .I5(\Dout_reg[31]_6 [6]),
        .O(\Dout[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF7F7F7FFF)) 
    \Dout[8]_i_3 
       (.I0(\Dout_reg[31]_0 [12]),
        .I1(\Dout_reg[31]_0 [14]),
        .I2(\Dout_reg[31]_0 [11]),
        .I3(\Dout_reg[21]_0 [4]),
        .I4(\Dout_reg[21]_0 [3]),
        .I5(\Dout_reg[27]_rep_0 ),
        .O(\Dout[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF4FFF0F0FFFFF0F0)) 
    \Dout[9]_i_1 
       (.I0(\ALUResult_out_OBUF[10]_inst_i_2_n_0 ),
        .I1(Q[9]),
        .I2(\Dout[9]_i_2_n_0 ),
        .I3(\Dout[9]_i_3_n_0 ),
        .I4(\Dout_reg[31]_3 ),
        .I5(\ALUResult_out_OBUF[9]_inst_i_3_n_0 ),
        .O(\Dout_reg[31]_1 [9]));
  LUT6 #(
    .INIT(64'hFFFFFFFF5F5C505C)) 
    \Dout[9]_i_2 
       (.I0(\Dout[9]_i_4_n_0 ),
        .I1(\Dout_reg[31]_6 [7]),
        .I2(\Dout_reg[31]_3 ),
        .I3(\Dout_reg[21]_0 [2]),
        .I4(\Dout_reg[31]_5 [5]),
        .I5(\Dout[10]_i_4_n_0 ),
        .O(\Dout[9]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hD7)) 
    \Dout[9]_i_3 
       (.I0(\Dout_reg[24]_1 ),
        .I1(Q[9]),
        .I2(\Dout_reg[31]_4 [9]),
        .O(\Dout[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF1FFFFFF)) 
    \Dout[9]_i_4 
       (.I0(\Dout_reg[31]_0 [11]),
        .I1(\Dout_reg[31]_0 [13]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[31]_0 [14]),
        .I4(\Dout_reg[26] [8]),
        .I5(Q[9]),
        .O(\Dout[9]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [0]),
        .Q(\Dout_reg[31]_0 [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [6]),
        .Q(\Dout_reg[31]_0 [6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [7]),
        .Q(\Dout_reg[31]_0 [7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [8]),
        .Q(\Dout_reg[31]_0 [8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [9]),
        .Q(\Dout_reg[31]_0 [9]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [10]),
        .Q(\Dout_reg[31]_0 [10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [1]),
        .Q(\Dout_reg[31]_0 [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [11]),
        .Q(\Dout_reg[31]_0 [11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [12]),
        .Q(\Dout_reg[31]_0 [12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [13]),
        .Q(\Dout_reg[31]_0 [13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [14]),
        .Q(\Dout_reg[31]_0 [14]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[27]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [15]),
        .Q(\Dout_reg[31]_0 [15]),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[27]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27]_rep 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[27]_rep_2 ),
        .Q(\Dout_reg[27]_rep_0 ),
        .R(SR));
  (* ORIG_CELL_NAME = "Dout_reg[27]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27]_rep__0 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[27]_rep__0_0 ),
        .Q(ADDR_R1[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [2]),
        .Q(\Dout_reg[31]_0 [2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [16]),
        .Q(\Dout_reg[31]_0 [16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [3]),
        .Q(\Dout_reg[31]_0 [3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [4]),
        .Q(\Dout_reg[31]_0 [4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(\Dout_reg[21]_0 [0]),
        .D(\Dout_reg[31]_7 [5]),
        .Q(\Dout_reg[31]_0 [5]),
        .R(SR));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_onehot_current_state[0]_i_2 
       (.I0(\Dout_reg[31]_0 [15]),
        .I1(\Dout_reg[31]_0 [12]),
        .O(\Dout_reg[27]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h00400000)) 
    \FSM_onehot_current_state[11]_i_1 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[31]_0 [16]),
        .I2(\Dout_reg[21]_0 [1]),
        .I3(\Dout_reg[21]_0 [0]),
        .I4(\Dout_reg[27]_rep_0 ),
        .O(\Dout_reg[24]_2 [1]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h00800000)) 
    \FSM_onehot_current_state[12]_i_1 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[31]_0 [16]),
        .I2(\Dout_reg[21]_0 [1]),
        .I3(\Dout_reg[21]_0 [0]),
        .I4(\Dout_reg[27]_rep_0 ),
        .O(\Dout_reg[24]_2 [2]));
  LUT6 #(
    .INIT(64'h0000000040400040)) 
    \FSM_onehot_current_state[6]_i_1 
       (.I0(\Dout_reg[21]_0 [0]),
        .I1(\Dout_reg[21]_0 [1]),
        .I2(\Dout_reg[31]_0 [16]),
        .I3(\Dout_reg[31]_0 [13]),
        .I4(\Dout_reg[31]_0 [12]),
        .I5(\Dout_reg[31]_0 [15]),
        .O(\Dout_reg[24]_2 [0]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \FSM_onehot_current_state[8]_i_2 
       (.I0(\Dout_reg[31]_0 [6]),
        .I1(\Dout_reg[31]_0 [9]),
        .I2(\Dout_reg[31]_0 [7]),
        .I3(\Dout_reg[31]_0 [8]),
        .O(\Dout_reg[12]_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_10
       (.I0(ADDR_R1[1]),
        .I1(\Dout_reg[31]_0 [8]),
        .O(ADDRD[2]));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_11
       (.I0(ADDR_R1[1]),
        .I1(\Dout_reg[31]_0 [7]),
        .O(ADDRD[1]));
  LUT2 #(
    .INIT(4'h2)) 
    RF_reg_r1_0_15_0_5_i_12
       (.I0(\Dout_reg[31]_0 [6]),
        .I1(ADDR_R1[1]),
        .O(ADDRD[0]));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_8
       (.I0(ADDR_R1[1]),
        .I1(\Dout_reg[31]_0 [10]),
        .O(ADDR_R1[0]));
  LUT2 #(
    .INIT(4'hE)) 
    RF_reg_r1_0_15_0_5_i_9
       (.I0(ADDR_R1[1]),
        .I1(\Dout_reg[31]_0 [9]),
        .O(ADDRD[3]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__0_i_1
       (.I0(Q[7]),
        .I1(\Dout_reg[31]_4 [7]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [7]),
        .O(\Dout_reg[7] [3]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__0_i_2
       (.I0(Q[6]),
        .I1(\Dout_reg[31]_4 [6]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [6]),
        .O(\Dout_reg[7] [2]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__0_i_3
       (.I0(Q[5]),
        .I1(\Dout_reg[31]_4 [5]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [5]),
        .O(\Dout_reg[7] [1]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__0_i_4
       (.I0(Q[4]),
        .I1(\Dout_reg[31]_4 [4]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [4]),
        .O(\Dout_reg[7] [0]));
  LUT4 #(
    .INIT(16'hA9AA)) 
    S0_carry__1_i_1
       (.I0(Q[11]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [11]),
        .O(\Dout_reg[11] [3]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__1_i_2
       (.I0(Q[10]),
        .I1(\Dout_reg[31]_4 [10]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [8]),
        .O(\Dout_reg[11] [2]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__1_i_3
       (.I0(Q[9]),
        .I1(\Dout_reg[31]_4 [9]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [8]),
        .O(\Dout_reg[11] [1]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__1_i_4
       (.I0(Q[8]),
        .I1(\Dout_reg[31]_4 [8]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [7]),
        .O(\Dout_reg[11] [0]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__2_i_1
       (.I0(Q[15]),
        .I1(\Dout_reg[31]_4 [15]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [10]),
        .O(\Dout_reg[15]_0 [3]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__2_i_2
       (.I0(Q[14]),
        .I1(\Dout_reg[31]_4 [14]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [9]),
        .O(\Dout_reg[15]_0 [2]));
  LUT4 #(
    .INIT(16'hA9AA)) 
    S0_carry__2_i_3
       (.I0(Q[13]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [13]),
        .O(\Dout_reg[15]_0 [1]));
  LUT4 #(
    .INIT(16'hA9AA)) 
    S0_carry__2_i_4
       (.I0(Q[12]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [12]),
        .O(\Dout_reg[15]_0 [0]));
  LUT4 #(
    .INIT(16'hA9AA)) 
    S0_carry__3_i_1
       (.I0(Q[19]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [19]),
        .O(\Dout_reg[19] [3]));
  LUT4 #(
    .INIT(16'hA9AA)) 
    S0_carry__3_i_2
       (.I0(Q[18]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [18]),
        .O(\Dout_reg[19] [2]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__3_i_3
       (.I0(Q[17]),
        .I1(\Dout_reg[31]_4 [17]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [12]),
        .O(\Dout_reg[19] [1]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__3_i_4
       (.I0(Q[16]),
        .I1(\Dout_reg[31]_4 [16]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [11]),
        .O(\Dout_reg[19] [0]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__4_i_1
       (.I0(Q[23]),
        .I1(\Dout_reg[31]_4 [23]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [14]),
        .O(\Dout_reg[23]_2 [3]));
  LUT4 #(
    .INIT(16'hA9AA)) 
    S0_carry__4_i_2
       (.I0(Q[22]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [22]),
        .O(\Dout_reg[23]_2 [2]));
  LUT4 #(
    .INIT(16'hA9AA)) 
    S0_carry__4_i_3
       (.I0(Q[21]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [21]),
        .O(\Dout_reg[23]_2 [1]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__4_i_4
       (.I0(Q[20]),
        .I1(\Dout_reg[31]_4 [20]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [13]),
        .O(\Dout_reg[23]_2 [0]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__5_i_1
       (.I0(Q[27]),
        .I1(\Dout_reg[31]_4 [27]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [15]),
        .O(\Dout_reg[27]_1 [3]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__5_i_2
       (.I0(Q[26]),
        .I1(\Dout_reg[31]_4 [26]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [15]),
        .O(\Dout_reg[27]_1 [2]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__5_i_3
       (.I0(Q[25]),
        .I1(\Dout_reg[31]_4 [25]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [15]),
        .O(\Dout_reg[27]_1 [1]));
  LUT4 #(
    .INIT(16'hA9AA)) 
    S0_carry__5_i_4
       (.I0(Q[24]),
        .I1(\Dout_reg[27]_rep_0 ),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[31]_4 [24]),
        .O(\Dout_reg[27]_1 [0]));
  LUT5 #(
    .INIT(32'h01FDFE02)) 
    S0_carry__6_i_1
       (.I0(\Dout_reg[31]_4 [31]),
        .I1(\Dout_reg[31]_0 [14]),
        .I2(\Dout_reg[27]_rep_0 ),
        .I3(\Dout_reg[26] [15]),
        .I4(Q[31]),
        .O(\Dout_reg[31]_2 [3]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__6_i_2
       (.I0(Q[30]),
        .I1(\Dout_reg[31]_4 [30]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [15]),
        .O(\Dout_reg[31]_2 [2]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__6_i_3
       (.I0(Q[29]),
        .I1(\Dout_reg[31]_4 [29]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [15]),
        .O(\Dout_reg[31]_2 [1]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry__6_i_4
       (.I0(Q[28]),
        .I1(\Dout_reg[31]_4 [28]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [15]),
        .O(\Dout_reg[31]_2 [0]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry_i_1
       (.I0(Q[3]),
        .I1(\Dout_reg[31]_4 [3]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [3]),
        .O(S[3]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry_i_2
       (.I0(Q[2]),
        .I1(\Dout_reg[31]_4 [2]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [2]),
        .O(S[2]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry_i_3
       (.I0(Q[1]),
        .I1(\Dout_reg[31]_4 [1]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [1]),
        .O(S[1]));
  LUT5 #(
    .INIT(32'h5556AAA6)) 
    S0_carry_i_4
       (.I0(Q[0]),
        .I1(\Dout_reg[31]_4 [0]),
        .I2(\Dout_reg[31]_0 [14]),
        .I3(\Dout_reg[27]_rep_0 ),
        .I4(\Dout_reg[26] [0]),
        .O(S[0]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[0]_inst_i_1 
       (.I0(\Dout_reg[31]_6 [0]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[0]),
        .O(\Dout_reg[0]_0 [0]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[10]_inst_i_1 
       (.I0(PCPlus8[7]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[10]),
        .O(\Dout_reg[0]_0 [10]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[11]_inst_i_1 
       (.I0(PCPlus8[8]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[11]),
        .O(\Dout_reg[0]_0 [11]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[12]_inst_i_1 
       (.I0(PCPlus8[9]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[12]),
        .O(\Dout_reg[0]_0 [12]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[13]_inst_i_1 
       (.I0(PCPlus8[10]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[13]),
        .O(\Dout_reg[0]_0 [13]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[14]_inst_i_1 
       (.I0(PCPlus8[11]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[14]),
        .O(\Dout_reg[0]_0 [14]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[15]_inst_i_1 
       (.I0(PCPlus8[12]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[15]),
        .O(\Dout_reg[0]_0 [15]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[16]_inst_i_1 
       (.I0(PCPlus8[13]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[16]),
        .O(\Dout_reg[0]_0 [16]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[17]_inst_i_1 
       (.I0(PCPlus8[14]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[17]),
        .O(\Dout_reg[0]_0 [17]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[18]_inst_i_1 
       (.I0(PCPlus8[15]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[18]),
        .O(\Dout_reg[0]_0 [18]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[19]_inst_i_1 
       (.I0(PCPlus8[16]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[19]),
        .O(\Dout_reg[0]_0 [19]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[1]_inst_i_1 
       (.I0(\Dout_reg[31]_6 [1]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[1]),
        .O(\Dout_reg[0]_0 [1]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[20]_inst_i_1 
       (.I0(PCPlus8[17]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[20]),
        .O(\Dout_reg[0]_0 [20]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[21]_inst_i_1 
       (.I0(PCPlus8[18]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[21]),
        .O(\Dout_reg[0]_0 [21]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[22]_inst_i_1 
       (.I0(PCPlus8[19]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[22]),
        .O(\Dout_reg[0]_0 [22]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[23]_inst_i_1 
       (.I0(PCPlus8[20]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[23]),
        .O(\Dout_reg[0]_0 [23]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[24]_inst_i_1 
       (.I0(PCPlus8[21]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[24]),
        .O(\Dout_reg[0]_0 [24]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[25]_inst_i_1 
       (.I0(PCPlus8[22]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[25]),
        .O(\Dout_reg[0]_0 [25]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[26]_inst_i_1 
       (.I0(PCPlus8[23]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[26]),
        .O(\Dout_reg[0]_0 [26]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[27]_inst_i_1 
       (.I0(PCPlus8[24]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[27]),
        .O(\Dout_reg[0]_0 [27]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[28]_inst_i_1 
       (.I0(PCPlus8[25]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[28]),
        .O(\Dout_reg[0]_0 [28]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[29]_inst_i_1 
       (.I0(PCPlus8[26]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[29]),
        .O(\Dout_reg[0]_0 [29]));
  LUT6 #(
    .INIT(64'h7FFFFFFF40000000)) 
    \WriteData_out_OBUF[2]_inst_i_1 
       (.I0(\Dout_reg[31]_6 [2]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[2]),
        .O(\Dout_reg[0]_0 [2]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[30]_inst_i_1 
       (.I0(PCPlus8[27]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[30]),
        .O(\Dout_reg[0]_0 [30]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[31]_inst_i_1 
       (.I0(PCPlus8[28]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[31]),
        .O(\Dout_reg[0]_0 [31]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[3]_inst_i_1 
       (.I0(PCPlus8[0]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[3]),
        .O(\Dout_reg[0]_0 [3]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[4]_inst_i_1 
       (.I0(PCPlus8[1]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[4]),
        .O(\Dout_reg[0]_0 [4]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[5]_inst_i_1 
       (.I0(PCPlus8[2]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[5]),
        .O(\Dout_reg[0]_0 [5]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[6]_inst_i_1 
       (.I0(PCPlus8[3]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[6]),
        .O(\Dout_reg[0]_0 [6]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[7]_inst_i_1 
       (.I0(PCPlus8[4]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[7]),
        .O(\Dout_reg[0]_0 [7]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[8]_inst_i_1 
       (.I0(PCPlus8[5]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[8]),
        .O(\Dout_reg[0]_0 [8]));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \WriteData_out_OBUF[9]_inst_i_1 
       (.I0(PCPlus8[6]),
        .I1(\Dout_reg[31]_0 [0]),
        .I2(\Dout_reg[31]_0 [3]),
        .I3(\Dout_reg[31]_0 [1]),
        .I4(\Dout_reg[31]_0 [2]),
        .I5(DATA_OUT2[9]),
        .O(\Dout_reg[0]_0 [9]));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_4
   (DATA_IN,
    Q,
    D,
    ADDR_R1,
    RF_reg_r2_0_15_30_31,
    \Dout_reg[2]_0 ,
    DATA_OUT1,
    SR,
    \Dout_reg[31]_0 ,
    CLK);
  output [31:0]DATA_IN;
  output [31:0]Q;
  output [2:0]D;
  input [0:0]ADDR_R1;
  input [31:0]RF_reg_r2_0_15_30_31;
  input \Dout_reg[2]_0 ;
  input [2:0]DATA_OUT1;
  input [0:0]SR;
  input [31:0]\Dout_reg[31]_0 ;
  input CLK;

  wire [0:0]ADDR_R1;
  wire CLK;
  wire [2:0]D;
  wire [31:0]DATA_IN;
  wire [2:0]DATA_OUT1;
  wire \Dout_reg[2]_0 ;
  wire [31:0]\Dout_reg[31]_0 ;
  wire [31:0]Q;
  wire [31:0]RF_reg_r2_0_15_30_31;
  wire [0:0]SR;

  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[0]_i_1__1 
       (.I0(Q[0]),
        .I1(\Dout_reg[2]_0 ),
        .I2(DATA_OUT1[0]),
        .O(D[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \Dout[1]_i_1__1 
       (.I0(Q[1]),
        .I1(\Dout_reg[2]_0 ),
        .I2(DATA_OUT1[1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \Dout[2]_i_1__1 
       (.I0(Q[2]),
        .I1(\Dout_reg[2]_0 ),
        .I2(DATA_OUT1[2]),
        .O(D[2]));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [0]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [10]),
        .Q(Q[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [11]),
        .Q(Q[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [12]),
        .Q(Q[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [13]),
        .Q(Q[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [14]),
        .Q(Q[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [15]),
        .Q(Q[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [16]),
        .Q(Q[16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [17]),
        .Q(Q[17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [18]),
        .Q(Q[18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [19]),
        .Q(Q[19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [1]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [20]),
        .Q(Q[20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [21]),
        .Q(Q[21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [22]),
        .Q(Q[22]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [23]),
        .Q(Q[23]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [24]),
        .Q(Q[24]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [25]),
        .Q(Q[25]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [26]),
        .Q(Q[26]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [27]),
        .Q(Q[27]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [28]),
        .Q(Q[28]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [29]),
        .Q(Q[29]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [2]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [30]),
        .Q(Q[30]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [31]),
        .Q(Q[31]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [3]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [4]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [5]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [6]),
        .Q(Q[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [7]),
        .Q(Q[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [8]),
        .Q(Q[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\Dout_reg[31]_0 [9]),
        .Q(Q[9]),
        .R(SR));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_0_5_i_2
       (.I0(Q[1]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[1]),
        .O(DATA_IN[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_0_5_i_3
       (.I0(Q[0]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[0]),
        .O(DATA_IN[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_0_5_i_4
       (.I0(Q[3]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[3]),
        .O(DATA_IN[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_0_5_i_5
       (.I0(Q[2]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[2]),
        .O(DATA_IN[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_0_5_i_6
       (.I0(Q[5]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[5]),
        .O(DATA_IN[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_0_5_i_7
       (.I0(Q[4]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[4]),
        .O(DATA_IN[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_12_17_i_1
       (.I0(Q[13]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[13]),
        .O(DATA_IN[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_12_17_i_2
       (.I0(Q[12]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[12]),
        .O(DATA_IN[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_12_17_i_3
       (.I0(Q[15]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[15]),
        .O(DATA_IN[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_12_17_i_4
       (.I0(Q[14]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[14]),
        .O(DATA_IN[14]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_12_17_i_5
       (.I0(Q[17]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[17]),
        .O(DATA_IN[17]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_12_17_i_6
       (.I0(Q[16]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[16]),
        .O(DATA_IN[16]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_18_23_i_1
       (.I0(Q[19]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[19]),
        .O(DATA_IN[19]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_18_23_i_2
       (.I0(Q[18]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[18]),
        .O(DATA_IN[18]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_18_23_i_3
       (.I0(Q[21]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[21]),
        .O(DATA_IN[21]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_18_23_i_4
       (.I0(Q[20]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[20]),
        .O(DATA_IN[20]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_18_23_i_5
       (.I0(Q[23]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[23]),
        .O(DATA_IN[23]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_18_23_i_6
       (.I0(Q[22]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[22]),
        .O(DATA_IN[22]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_24_29_i_1
       (.I0(Q[25]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[25]),
        .O(DATA_IN[25]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_24_29_i_2
       (.I0(Q[24]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[24]),
        .O(DATA_IN[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_24_29_i_3
       (.I0(Q[27]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[27]),
        .O(DATA_IN[27]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_24_29_i_4
       (.I0(Q[26]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[26]),
        .O(DATA_IN[26]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_24_29_i_5
       (.I0(Q[29]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[29]),
        .O(DATA_IN[29]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_24_29_i_6
       (.I0(Q[28]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[28]),
        .O(DATA_IN[28]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_30_31_i_1
       (.I0(Q[31]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[31]),
        .O(DATA_IN[31]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_30_31_i_2
       (.I0(Q[30]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[30]),
        .O(DATA_IN[30]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_6_11_i_1
       (.I0(Q[7]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[7]),
        .O(DATA_IN[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_6_11_i_2
       (.I0(Q[6]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[6]),
        .O(DATA_IN[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_6_11_i_3
       (.I0(Q[9]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[9]),
        .O(DATA_IN[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_6_11_i_4
       (.I0(Q[8]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[8]),
        .O(DATA_IN[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_6_11_i_5
       (.I0(Q[11]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[11]),
        .O(DATA_IN[11]));
  LUT3 #(
    .INIT(8'hB8)) 
    RF_reg_r1_0_15_6_11_i_6
       (.I0(Q[10]),
        .I1(ADDR_R1),
        .I2(RF_reg_r2_0_15_30_31[10]),
        .O(DATA_IN[10]));
endmodule

(* ORIG_REF_NAME = "REGrwe_n" *) 
module REGrwe_n_5
   (\Dout_reg[13]_0 ,
    \Dout_reg[31]_0 ,
    \Dout_reg[21]_0 ,
    \Dout_reg[18]_0 ,
    \Dout_reg[13]_1 ,
    Q,
    D,
    \Dout_reg[21]_1 ,
    \Dout_reg[18]_1 ,
    \Dout_reg[21]_2 ,
    \Dout_reg[18]_2 ,
    SR,
    CLK);
  output [2:0]\Dout_reg[13]_0 ;
  output [31:0]\Dout_reg[31]_0 ;
  output \Dout_reg[21]_0 ;
  output \Dout_reg[18]_0 ;
  input [2:0]\Dout_reg[13]_1 ;
  input [4:0]Q;
  input [31:0]D;
  input \Dout_reg[21]_1 ;
  input \Dout_reg[18]_1 ;
  input \Dout_reg[21]_2 ;
  input \Dout_reg[18]_2 ;
  input [0:0]SR;
  input CLK;

  wire CLK;
  wire [31:0]D;
  wire [2:0]\Dout_reg[13]_0 ;
  wire [2:0]\Dout_reg[13]_1 ;
  wire \Dout_reg[18]_0 ;
  wire \Dout_reg[18]_1 ;
  wire \Dout_reg[18]_2 ;
  wire \Dout_reg[21]_0 ;
  wire \Dout_reg[21]_1 ;
  wire \Dout_reg[21]_2 ;
  wire [31:0]\Dout_reg[31]_0 ;
  wire [4:0]Q;
  wire [0:0]SR;

  LUT6 #(
    .INIT(64'hFFFFFFB8000000B8)) 
    \Dout[11]_i_1 
       (.I0(\Dout_reg[31]_0 [11]),
        .I1(\Dout_reg[13]_1 [0]),
        .I2(Q[0]),
        .I3(\Dout_reg[13]_1 [2]),
        .I4(\Dout_reg[13]_1 [1]),
        .I5(D[11]),
        .O(\Dout_reg[13]_0 [0]));
  LUT6 #(
    .INIT(64'hFFFFFFB8000000B8)) 
    \Dout[12]_i_1 
       (.I0(\Dout_reg[31]_0 [12]),
        .I1(\Dout_reg[13]_1 [0]),
        .I2(Q[1]),
        .I3(\Dout_reg[13]_1 [2]),
        .I4(\Dout_reg[13]_1 [1]),
        .I5(D[12]),
        .O(\Dout_reg[13]_0 [1]));
  LUT6 #(
    .INIT(64'hFFFFFFB8000000B8)) 
    \Dout[13]_i_1 
       (.I0(\Dout_reg[31]_0 [13]),
        .I1(\Dout_reg[13]_1 [0]),
        .I2(Q[2]),
        .I3(\Dout_reg[13]_1 [2]),
        .I4(\Dout_reg[13]_1 [1]),
        .I5(D[13]),
        .O(\Dout_reg[13]_0 [2]));
  LUT6 #(
    .INIT(64'hFF00B8B80000B8B8)) 
    \Dout[18]_i_3 
       (.I0(\Dout_reg[31]_0 [18]),
        .I1(\Dout_reg[13]_1 [0]),
        .I2(Q[3]),
        .I3(\Dout_reg[21]_1 ),
        .I4(\Dout_reg[18]_1 ),
        .I5(\Dout_reg[18]_2 ),
        .O(\Dout_reg[18]_0 ));
  LUT6 #(
    .INIT(64'hFF00B8B80000B8B8)) 
    \Dout[21]_i_3 
       (.I0(\Dout_reg[31]_0 [21]),
        .I1(\Dout_reg[13]_1 [0]),
        .I2(Q[4]),
        .I3(\Dout_reg[21]_1 ),
        .I4(\Dout_reg[18]_1 ),
        .I5(\Dout_reg[21]_2 ),
        .O(\Dout_reg[21]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\Dout_reg[31]_0 [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[10]),
        .Q(\Dout_reg[31]_0 [10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[11]),
        .Q(\Dout_reg[31]_0 [11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[12]),
        .Q(\Dout_reg[31]_0 [12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[13]),
        .Q(\Dout_reg[31]_0 [13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[14]),
        .Q(\Dout_reg[31]_0 [14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[15]),
        .Q(\Dout_reg[31]_0 [15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[16] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[16]),
        .Q(\Dout_reg[31]_0 [16]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[17] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[17]),
        .Q(\Dout_reg[31]_0 [17]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[18] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[18]),
        .Q(\Dout_reg[31]_0 [18]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[19] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[19]),
        .Q(\Dout_reg[31]_0 [19]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\Dout_reg[31]_0 [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[20] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[20]),
        .Q(\Dout_reg[31]_0 [20]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[21] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[21]),
        .Q(\Dout_reg[31]_0 [21]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[22] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[22]),
        .Q(\Dout_reg[31]_0 [22]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[23] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[23]),
        .Q(\Dout_reg[31]_0 [23]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[24] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[24]),
        .Q(\Dout_reg[31]_0 [24]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[25] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[25]),
        .Q(\Dout_reg[31]_0 [25]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[26] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[26]),
        .Q(\Dout_reg[31]_0 [26]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[27] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[27]),
        .Q(\Dout_reg[31]_0 [27]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[28] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[28]),
        .Q(\Dout_reg[31]_0 [28]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[29] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[29]),
        .Q(\Dout_reg[31]_0 [29]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\Dout_reg[31]_0 [2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[30] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[30]),
        .Q(\Dout_reg[31]_0 [30]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[31] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[31]),
        .Q(\Dout_reg[31]_0 [31]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\Dout_reg[31]_0 [3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[4]),
        .Q(\Dout_reg[31]_0 [4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[5]),
        .Q(\Dout_reg[31]_0 [5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[6]),
        .Q(\Dout_reg[31]_0 [6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[7]),
        .Q(\Dout_reg[31]_0 [7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[8]),
        .Q(\Dout_reg[31]_0 [8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \Dout_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[9]),
        .Q(\Dout_reg[31]_0 [9]),
        .R(SR));
endmodule

module ROM_ARRAY
   (D,
    \Dout_reg[5] ,
    \Dout_reg[5]_0 ,
    Q);
  output [14:0]D;
  output \Dout_reg[5] ;
  output \Dout_reg[5]_0 ;
  input [5:0]Q;

  wire [14:0]D;
  wire \Dout_reg[5] ;
  wire \Dout_reg[5]_0 ;
  wire [5:0]Q;

  LUT6 #(
    .INIT(64'h0000110100010000)) 
    \Dout[12]_i_1 
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[4]),
        .I5(Q[0]),
        .O(D[5]));
  LUT6 #(
    .INIT(64'h0001000100000100)) 
    \Dout[13]_i_1 
       (.I0(Q[5]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[4]),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(D[6]));
  LUT6 #(
    .INIT(64'h1110100001001114)) 
    \Dout[14]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(D[7]));
  LUT6 #(
    .INIT(64'h0000010000010000)) 
    \Dout[15]_i_1 
       (.I0(Q[5]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[4]),
        .I5(Q[1]),
        .O(D[8]));
  LUT5 #(
    .INIT(32'h00000200)) 
    \Dout[18]_i_1 
       (.I0(Q[2]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(Q[3]),
        .I4(Q[1]),
        .O(D[9]));
  LUT6 #(
    .INIT(64'h0000000100015540)) 
    \Dout[1]_i_1 
       (.I0(Q[5]),
        .I1(Q[0]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[4]),
        .I5(Q[1]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h0000511500010115)) 
    \Dout[21]_i_1 
       (.I0(Q[5]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(Q[4]),
        .I5(Q[1]),
        .O(D[10]));
  LUT6 #(
    .INIT(64'h1111110101110113)) 
    \Dout[23]_i_1 
       (.I0(Q[4]),
        .I1(Q[5]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[3]),
        .O(D[11]));
  LUT6 #(
    .INIT(64'h0000000140451555)) 
    \Dout[24]_i_1 
       (.I0(Q[5]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(Q[4]),
        .O(D[12]));
  LUT6 #(
    .INIT(64'h0000000000000600)) 
    \Dout[27]_i_1 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[4]),
        .I5(Q[5]),
        .O(D[13]));
  LUT6 #(
    .INIT(64'h0000000000000600)) 
    \Dout[27]_rep__0_i_1 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[4]),
        .I5(Q[5]),
        .O(\Dout_reg[5]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000600)) 
    \Dout[27]_rep_i_1 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[4]),
        .I5(Q[5]),
        .O(\Dout_reg[5] ));
  LUT6 #(
    .INIT(64'h1000100000101004)) 
    \Dout[2]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(Q[2]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'h0000000155555555)) 
    \Dout[31]_i_1 
       (.I0(Q[5]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(D[14]));
  LUT6 #(
    .INIT(64'h0000400000010000)) 
    \Dout[3]_i_1 
       (.I0(Q[5]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(Q[4]),
        .I5(Q[3]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \Dout[4]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(Q[0]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \Dout[6]_i_1 
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[0]),
        .O(D[4]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
