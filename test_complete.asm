start:
	MOV		R0, 0xff			;R0 = 0xff
	MVN		R1, R0				;R1 = !R0 = 0xffffff00
	MVN		R1, 0xAA			;R1 = !0xAA = 0xffffff55
	ADD 	R1, R1, 0x55		;R1 = 0xfffff55 + 0x55 = 0xFFFFFFAA
	SUB		R1, 0xAA			;R1 = 0xFFFFFFAA - 0xAA = 0xFFFFFF00
	CMP		R0, R1				;0xFF == 0xFFFFFF00 ? Updates flags
	EOREQ	R2, R1, 0xAA		;R2 = 0xFFFFFF00 ^ 0xAA -- Should not execute
	EORSAL	R2, R1, 0xAA		;R2 = 0xFFFFFF00 ^ 0xAA = 0xFFFFFFAA, update flags
	AND		R2, R1, 0xFF		;R2 = 0xFFFFFF00 & 0xFF = 0
	
	ORRMI	R3, R1, R2			;Run only if previous result was < 0, should not run
	ORRPL	R3, R1, R2			;R3 = R1 | R2 = 0xFFFFFF00 | 0 = 0xFFFFFF00
	;; -- As above, but should run (flags have not changed)

	SUB		R0, R0, R2			;R0 = R0 - R2 = 0xFF - 0 = 0xFF
	BL		sum					;R0 = R0+R1+R2+R3 = 0xFF+0xFFFFFF00+0+0xFFFFFF00
	;; R0 should be 0xfffffeff now, and have triggered some overflows

	;; Test shifts
	MOV		R0, R0				;NOP
	LSL		R0, R0, #2			;R0 = 0xfffffeff >> 1 = 0x3FFFFFBF
	LSR		R0, R0, #1			;R0 = 0x3FFFFFBF << 2 = 0x7FFFFF7E
	MOV		R0, #1				;R0 = 1
	ROR		R0, R0, #1			;R0 = 1 ROR 1 = 0x80000000
	ASR		R0, R0, #1			;R0 = 0x80000000 ASR 1 = 0xC0000000

	;; Test LDR/STR
	MOV		R4, #1				;Load address 1 to R4
	STR		R0, [R4, #0]		;Store R0 to address 1 of RAM
	LDR		R1, [R4, #0]		;Load address 1 from RAM to R4 -- S4a
	CMP 	R0, R1				;Compare them
	STR 	PC, [R4, #0]
	LDR		PC, [R4, #0]		;Test load PC + 1 to PC -- S4b

	;; DP instruction on R15 
	ADDS	PC, PC, #-4			;SL = 1 -- S4f
	ADD		PC, PC, #-4			;SL = 0 -- S4b
	
	B		exit				;exit
	
sum:							;SUM R0+R1+R2+R3
	MOV		R12, R4				;R12 = R4 -- save R4
	ADD		R4, R0, R1			;R4 = R0 + R1
	ADD		R4, R4, R2			;R4 = R4 + R2
	ADD		R4, R4, R3			;R4 = R4 + R3
	MOV		R0, R4				;R0 = R4
	MOV		R4, R12				;R4 = R12
	MOV		PC, LR				;Return
exit:	
