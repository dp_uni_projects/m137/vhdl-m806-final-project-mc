----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/12/2020 06:19:45 PM
-- Design Name: 
-- Module Name: COND_LOGIC_TB - structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use std.env.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity COND_LOGIC_TB is
end COND_LOGIC_TB;

architecture structural of COND_LOGIC_TB is
  component COND_LOGIC is
    port(
      FLAGS      : in  std_logic_vector(3 downto 0);  --N,Z,C,V
      COND_FIELD : in  std_logic_vector(3 downto 0);
      CondEx_in  : out std_logic
      );
  end component;
  signal FLAGS      : std_logic_vector(3 downto 0);
  signal COND_FIELD : std_logic_vector(3 downto 0);
  signal CondEx_in  : std_logic;

  constant CLK_period : time := 10 ns;
begin
  uut : COND_LOGIC
    port map(
      FLAGS      => FLAGS,
      COND_FIELD => COND_FIELD,
      Condex_In  => Condex_In
      );

  stimulus_process : process
  begin
    wait for 100 ns;
    COND_FIELD <= "0000";               --EQ
    FLAGS      <= "0000";
    wait for 1*CLK_period;
    COND_FIELD <= "0001";               --NE
    wait for 1*CLK_period;

    FLAGS      <= "0010";               --Zero only
    COND_FIELD <= "0000";               --NE
    wait for 1*CLK_period;

    FLAGS      <= "1000";
    COND_FIELD <= "0110";
    wait for 1*CLK_period;

    FLAGS <= "0010";
    wait for 1*CLK_period;
    stop(2);
  end process;

end structural;
