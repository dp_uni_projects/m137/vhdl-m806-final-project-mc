----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/06/2020 01:03:49 PM
-- Design Name: 
-- Module Name: ASR_A_B_n_TB - structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use STD.ENV.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ASR_A_B_n_TB is

end ASR_A_B_n_TB;

architecture structural of ASR_A_B_n_TB is
  component ASR_A_B_n is
    generic (WIDTH : positive := 4);
    port(
      A : in  std_logic_vector (WIDTH-1 downto 0);
      B : in  std_logic_vector (4 downto 0);
      Z : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  signal A            : std_logic_vector (3 downto 0);
  signal B            : std_logic_vector (4 downto 0);
  signal Z            : std_logic_vector (3 downto 0);
  constant CLK_period : time := 10 ns;

begin
  uut : ASR_A_B_n
    generic map (WIDTH => 4)
    port map(
      A => A,
      B => B,
      Z => Z
      );
  stimulus_process : process
  begin
    wait for 100 ns;

    -- ASR 1 by 1 place
    A <= "0001";
    B <= "00001";
    wait for 1 * CLK_period;

    -- ASR 2 by 1 place
    A <= "0010";
    B <= "00001";
    wait for 1 * CLK_period;

    -- ASR 3 by 1 place
    A <= "0011";
    B <= "00001";
    wait for 1 * CLK_period;

    -- ASR -1 by 2 places
    A <= "1001";
    B <= "00010";
    wait for 1 * CLK_period;

    stop(2);
  end process;
end structural;
