----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/27/2020 06:32:52 PM
-- Design Name: 
-- Module Name: PROCESSOR_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use std.env.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PROCESSOR_TB is
--  Port ( );
end PROCESSOR_TB;

architecture Behavioral of PROCESSOR_TB is
  component ARM_PROCESSOR_n is
    -- generic (WIDTH : positive := 32);
    port(
      CLK   : in std_logic;
      RESET : in std_logic;

      ALUControl_out : out std_logic_vector(3 downto 0);
      Instr_out      : out std_logic_vector(31 downto 0);
      -- PC_out         : out std_logic_vector(31 downto 0);
      ALUResult_out  : out std_logic_vector(31 downto 0);
      Result_out     : out std_logic_vector(31 downto 0);
      WriteData_out  : out std_logic_vector(31 downto 0)
      );
  end component;

  signal CLK            : std_logic := '0';
  signal RESET          : std_logic := '1';
  signal ALUControl_out : std_logic_vector(3 downto 0);
  signal Instr_out      : std_logic_vector(31 downto 0);
  -- signal PC_out         : std_logic_vector(31 downto 0);
  signal ALUResult_out  : std_logic_vector(31 downto 0);
  signal Result_out     : std_logic_vector(31 downto 0);
  signal WriteData_out  : std_logic_vector(31 downto 0);

  constant MAX_INSTR_CYCLES : positive := 5;
  constant CLK_period       : time     := 10 ns;
begin

  uut : ARM_PROCESSOR_n
    port map(
      CLK            => CLK,
      RESET          => RESET,
      ALUControl_out => ALUControl_out,
      Instr_out      => Instr_out,
      -- PC_out         => PC_out,
      ALUResult_out  => ALUResult_out,
      Result_out     => Result_out,
      WriteData_out  => WriteData_out
      );

  clk_process : process
  begin
    CLK <= '0';
    wait for CLK_period/2;
    CLK <= '1';
    wait for CLK_period/2;
  end process;

  stimulus_process : process
  begin
    RESET <= '1';
    wait for 100 ns;
    wait until (CLK = '0' and CLK'event);
    RESET <= '0';

    wait for 35*MAX_INSTR_CYCLES*CLK_period;
    stop(2);
  end process;
end Behavioral;
