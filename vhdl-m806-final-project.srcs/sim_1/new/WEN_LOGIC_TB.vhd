----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/12/2020 12:34:13 PM
-- Design Name: 
-- Module Name: WEN_LOGIC_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use STD.ENV.all;

entity WEN_LOGIC_TB is

end WEN_LOGIC_TB;

architecture structural of WEN_LOGIC_TB is

  component WEN_LOGIC is
    port(
      OP         : in std_logic_vector(1 downto 0);
      SL         : in std_logic;
      NoWrite_in : in std_logic;        --From INSTR_DEC
      Instr_24   : in std_logic;

      RegWrite_in   : out std_logic;
      FlagsWrite_in : out std_logic;
      MemWrite_in   : out std_logic
      );
  end component;
  signal OP            : std_logic_vector(1 downto 0);
  signal SL            : std_logic;
  signal NoWrite_in    : std_logic;     --From INSTR_DEC
  signal Instr_24      : std_logic;
  signal RegWrite_in   : std_logic;
  signal FlagsWrite_in : std_logic;
  signal MemWrite_in   : std_logic;


  constant CLK_period : time := 10 ns;
begin
  uut : WEN_LOGIC
    port map(
      OP            => OP,
      SL            => SL,
      NoWrite_in    => NoWrite_in,
      Instr_24      => Instr_24,
      RegWrite_in   => RegWrite_in,
      FlagsWrite_in => FlagsWrite_in,
      MemWrite_in   => MemWrite_in
      );

  stimulus_process : process
  begin
    wait for 100 ns;
    -- DP
    Instr_24   <= '0';
    OP         <= "00";
    SL         <= '0';
    NoWrite_in <= '0';
    wait for 1*CLK_period;

    SL <= '1';
    wait for 1*CLK_period;

    -- DP (CMP only)
    NoWrite_in <= '1';
    SL         <= '0';
    wait for 1*CLK_period;

    NoWrite_in <= '0';
    OP         <= "01";
    -- LDR
    SL         <= '1';
    wait for 1*CLK_period;

    -- STR
    SL <= '0';
    wait for 1*CLK_period;

    -- Branch instructions
    -- B
    OP <= "10";
    wait for 1*CLK_period;

    -- BL
    Instr_24 <= '1';
    wait for 1*CLK_period;
    stop(2);
  end process;

end structural;
