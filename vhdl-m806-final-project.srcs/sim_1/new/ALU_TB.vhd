----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/13/2020 12:26:49 PM
-- Design Name: 
-- Module Name: ALU_TB - structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use std.env.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALU_TB is
end ALU_TB;

architecture structural of ALU_TB is
  component ALU_REGWE_32 is
    port(
      SRC_A      : in  std_logic_vector (32-1 downto 0);  -- Source A data
      SRC_B      : in  std_logic_vector (32-1 downto 0);  -- Source B data
      ALUControl : in  std_logic_vector (3 downto 0);  -- Control signal from CU
      SHAMT5     : in  std_logic_vector (4 downto 0);
      Flags      : out std_logic_vector (3 downto 0);  -- 4 status flags
      ALUResult  : out std_logic_vector (32-1 downto 0);  -- Final result
      CLK        : in  std_logic;
      RESET      : in  std_logic;
      WE         : in  std_logic
      );
  end component;

  signal SRC_A        : std_logic_vector (31 downto 0) := (others => 'X');  -- Source A data
  signal SRC_B        : std_logic_vector (31 downto 0) := (others => 'X');  -- Source B data
  signal ALUControl   : std_logic_vector (3 downto 0)  := (others => 'X');  -- Control signal from CU
  signal SHAMT5       : std_logic_vector (4 downto 0)  := (others => 'X');
  signal Flags        : std_logic_vector (3 downto 0)  := (others => 'X');  -- 4 status flags
  signal ALUResult    : std_logic_vector (31 downto 0);  -- Final result
  signal CLK          : std_logic                      := '0';
  signal RESET        : std_logic                      := '1';
  signal WE           : std_logic                      := '0';
  constant CLK_period : time                           := 10 ns;
begin

  uut : ALU_REGWE_32
    port map(
      SRC_A      => SRC_A,
      SRC_B      => SRC_B,
      ALUControl => ALUControl,
      SHAMT5     => SHAMT5,
      Flags      => Flags,
      ALUResult  => ALUResult,
      CLK        => CLK,
      WE         => WE,
      RESET      => RESET
      );

  clk_process : process
  begin
    CLK <= '0';
    wait for CLK_period/2;
    CLK <= '1';
    wait for CLK_period/2;
  end process;

  stimulus_process : process
  begin
    RESET <= '1';
    wait for 100 ns;
    wait until (CLK = '0' and CLK'event);
    RESET <= '0';
    WE    <= '1';

    -- ADD
    ALUControl <= "0000";
    SHAMT5     <= "00000";

    -- 1 + 0
    SRC_A <= std_logic_vector(to_signed(0, 32));
    SRC_B <= std_logic_vector(to_signed(1, 32));
    wait for 1*CLK_period;

    -- 455555 + 1
    SRC_A <= std_logic_vector(to_signed(455555, 32));
    SRC_B <= std_logic_vector(to_signed(1, 32));
    wait for 1*CLK_period;

    -- 1 + -1
    SRC_A <= std_logic_vector(to_signed(1, 32));
    SRC_B <= std_logic_vector(to_signed(-1, 32));
    wait for 1*CLK_period;

    -- 0x7fffffff + 1
    -- Should overflow
    SRC_A <= X"7FFFFFFF";
    SRC_B <= std_logic_vector(to_signed(1, 32));
    wait for 1*CLK_period;

    -- 0x7fffffff + 0x80000001 = 0
    SRC_B <= X"80000001";
    wait for 1*CLK_period;

    ALUControl <= "0001";               -- SUB

    -- 0 - 1
    SRC_A <= std_logic_vector(to_signed(0, 32));
    SRC_B <= std_logic_vector(to_signed(1, 32));
    wait for 1*CLK_period;

    -- 0x7fffffff - 0x7fffffff = 0
    -- SHould be zero
    SRC_A <= X"7FFFFFFF";
    SRC_B <= X"7FFFFFFF";
    wait for 1*CLK_period;

    ALUControl <= "0010";               --AND

    -- 0x7fffffff AND 0x7fffffff = 0x7fffffff
    wait for 1*CLK_period;

    -- 0x7fffffff AND !(0x7fffffff) = 0
    -- SHould be zero
    SRC_A <= not(X"7FFFFFFF");
    wait for 1*CLK_period;

    ALUControl <= "0011";               --OR
    -- 0x7fffffff OR !(0x7fffffff) = 0xffffffff = -1
    wait for 1*CLK_period;

    -- 0 OR !(0x7fffffff) = !(0x7fffffff)
    SRC_A <= X"00000000";
    wait for 1*CLK_period;

    -- 0 OR 0 = 0
    SRC_B <= X"00000000";
    wait for 1*CLK_period;

    ALUControl <= "0100";               --MOV

    -- MOV 0 = 0
    wait for 1*CLK_period;

    ALUControl <= "0101";               --MVN

    -- MVN 0 = 0xffffffff = -1
    wait for 1*CLK_period;

    ALUControl <= "0110";               --XOR
    -- 0x7FFFFFFF XOR 0x7FFFFFFF = 0
    SRC_A      <= X"7FFFFFFF";
    SRC_B      <= X"7FFFFFFF";
    wait for 1*CLK_period;

    -- 0x7FFFFFFF XOR 0 = 0x7FFFFFFF
    SRC_B <= X"00000000";
    wait for 1*CLK_period;

    ALUControl <= "0111";               --XOR
    wait for 1*CLK_period;

    ALUControl <= "1000";               --LSL
    -- 0x80000000 LSL 0 = 0x80000000
    SRC_B      <= X"80000000";
    wait for 1*CLK_period;

    -- 0x80000000 LSL 1 = 0
    SHAMT5 <= std_logic_vector(to_unsigned(1, 5));
    wait for 1*CLK_period;

    -- 1 LSL 1 = 2
    SRC_B <= X"00000001";
    wait for 1*CLK_period;

    -- 1 LSL 31 = 0x80000000
    SHAMT5 <= std_logic_vector(to_unsigned(31, 5));
    wait for 1*CLK_period;

    ALUControl <= "1001";               --LSR
    -- 1 LSR 31 = 0
    wait for 1*CLK_period;

    ALUControl <= "1010";               --ASR
    -- 1 ASR 31 = 0 
    wait for 1*CLK_period;

    -- 1 ROR 31 = 2
    ALUControl <= "1011";               --ROR
    wait for 1*CLK_period;

    -- 1 ROR 1 = 0x80000000
    SHAMT5 <= std_logic_vector(to_unsigned(1, 5));
    wait for 1*CLK_period;

    ALUControl <= "1100";               --LSL
    -- 1 LSL 1 = 2
    wait for 1*CLK_period;

    ALUControl <= "1101";               --LSR
    -- 1 LSR 1 = 0
    wait for 1*CLK_period;

    ALUControl <= "1110";               --ASR
    -- 1 ASR 1 = 0
    wait for 1*CLK_period;

    -- 0x80000000 ASR 31 = 0xffffffff
    SRC_B  <= X"80000000";
    SHAMT5 <= std_logic_vector(to_unsigned(31, 5));
    wait for 1*CLK_period;

    ALUControl <= "1111";               --ROR
    -- 1 ROR 1 = 0x80000000
    SRC_B      <= X"00000001";
    SHAMT5     <= std_logic_vector(to_unsigned(1, 5));
    wait for 3*CLK_period;
    stop(2);
  end process;
end structural;
