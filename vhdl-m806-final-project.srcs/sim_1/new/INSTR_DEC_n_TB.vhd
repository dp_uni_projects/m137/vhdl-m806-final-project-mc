----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/06/2020 08:12:00 PM
-- Design Name: 
-- Module Name: INSTR_DEC_n_TB - structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use STD.ENV.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity INSTR_DEC_n_TB is
--  Port ( );
end INSTR_DEC_n_TB;

architecture structural of INSTR_DEC_n_TB is
  component INSTR_DEC_n is
    -- generic (WIDTH : positive := 32);
    port (
      OP          : in  std_logic_vector(1 downto 0);  -- 2-bit signal
      FUNCT       : in  std_logic_vector(5 downto 0);  -- 6-bit signal
      SH          : in  std_logic_vector(1 downto 0);  -- 2-bit signal
      REG_SRC     : out std_logic_vector(2 downto 0);  -- 3-bit signal
      ALU_SRC     : out std_logic;                     -- 1-bit signal
      MEM_TO_REG  : out std_logic;                     -- 1-bit signal
      ALU_CONTROL : out std_logic_vector(3 downto 0);  -- 4-bit signal
      IMM_SRC     : out std_logic;                     -- 1-bit signal
      NO_WRITE_in : out std_logic                      -- 1-bit signal
      );
  end component;

  constant CLK_period : time := 10 ns;
  signal OP           : std_logic_vector(1 downto 0);  -- 2-bit signal
  signal FUNCT        : std_logic_vector(5 downto 0);  -- 6-bit signal
  signal SH           : std_logic_vector(1 downto 0);  -- 2-bit signal
  signal REG_SRC      : std_logic_vector(2 downto 0);  -- 3-bit signal
  signal ALU_SRC      : std_logic;                     -- 1-bit signal
  signal MEM_TO_REG   : std_logic;                     -- 1-bit signal
  signal ALU_CONTROL  : std_logic_vector(3 downto 0);  -- 4-bit signal
  signal IMM_SRC      : std_logic;                     -- 1-bit signal
  signal NO_WRITE_in  : std_logic;                     -- 1-bit signal  
begin
  uut : INSTR_DEC_n
    -- generic map (WIDTH => 32)
    port map(
      OP          => OP,
      FUNCT       => FUNCT,
      SH          => SH,
      REG_SRC     => REG_SRC,
      ALU_SRC     => ALU_SRC,
      MEM_TO_REG  => MEM_TO_REG,
      ALU_CONTROL => ALU_CONTROL,
      IMM_SRC     => IMM_SRC,
      NO_WRITE_in => NO_WRITE_in
      );

  stimulus_process : process
  begin
    wait for 100 ns;

    -- DATA INSTRUCTIONS
    OP <= "00";
    SH <= "00";

    -- ADD IMM
    FUNCT <= "101000";
    wait for 1 * CLK_period;

    FUNCT <= "101001";
    wait for 1 * CLK_period;

    -- ADD REG
    FUNCT <= "001000";
    wait for 1 * CLK_period;

    FUNCT <= "001001";
    wait for 1 * CLK_period;

    -- SUB IMM
    FUNCT <= "100100";
    wait for 1 * CLK_period;

    FUNCT <= "100101";
    wait for 1 * CLK_period;

    -- SUB REG
    FUNCT <= "000100";
    wait for 1 * CLK_period;

    FUNCT <= "000101";
    wait for 1 * CLK_period;

    -- CMP IMM
    FUNCT <= "110101";
    wait for 1 * CLK_period;

    -- CMP REG
    FUNCT <= "010101";
    wait for 1 * CLK_period;

    -- MOV REG, LSL, LSR, ASL, ROR
    FUNCT <= "011010";

    SH <= "00";
    wait for 1 * CLK_period;

    SH <= "01";
    wait for 1 * CLK_period;

    SH <= "10";
    wait for 1 * CLK_period;

    SH <= "11";
    wait for 1 * CLK_period;
    -- OP <= "10";
    -- wait for 1 * CLK_period;
    stop(2);
  end process;
end structural;
