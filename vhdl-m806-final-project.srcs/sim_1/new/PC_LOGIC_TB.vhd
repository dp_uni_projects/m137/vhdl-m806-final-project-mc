----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/12/2020 02:17:31 PM
-- Design Name: 
-- Module Name: PC_LOGIC_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use std.env.all;

entity PC_LOGIC_TB is
--  Port ( );
end PC_LOGIC_TB;

architecture structural of PC_LOGIC_TB is
  component PC_LOGIC is
    port(
      RegWrite_in : in std_logic;
      RD          : in std_logic_vector (3 downto 0);
      OP_1        : in std_logic;       -- OP bit 1

      PcSrc_in : out std_logic

      );
  end component;

  signal RegWrite_in  : std_logic;
  signal RD           : std_logic_vector (3 downto 0);
  signal OP_1         : std_logic;
  signal PcSrc_in     : std_logic;
  constant CLK_period : time := 10 ns;
begin
  uut : PC_LOGIC
    port map(
      RegWrite_in => RegWrite_in,
      RD          => RD,
      OP_1        => OP_1,
      PcSrc_in    => PcSrc_in
      );

  stimulus_process : process
  begin
    wait for 100 ns;

    -- Any non-branch instruction
    OP_1        <= '0';
    RD          <= "0000";
    RegWrite_in <= '0';
    wait for 1*CLK_period;
    RegWrite_in <= '1';
    RD          <= "1110";
    wait for 1*CLK_period;
    RegWrite_in <= '0';
    RD          <= "1111";
    wait for 1*CLK_period;
    RegWrite_in <= '1';
    wait for 1*CLK_period;

    -- Any branch instruction
    OP_1        <= '1';
    RD          <= "0000";
    RegWrite_in <= '0';
    wait for 1*CLK_period;
    RD          <= "1110";
    wait for 1*CLK_period;
    RD          <= "1111";
    wait for 1*CLK_period;
    RegWrite_in <= '1';
    wait for 1*CLK_period;

    stop(2);
  end process;
end structural;
