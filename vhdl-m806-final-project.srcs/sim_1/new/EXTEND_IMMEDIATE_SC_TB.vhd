----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/21/2020 08:16:16 PM
-- Design Name: 
-- Module Name: EXTEND_IMMEDIATE_SC_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;
use STD.ENV.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EXTEND_IMMEDIATE_SC_TB is

--  Port ( );
end EXTEND_IMMEDIATE_SC_TB;

architecture structural of EXTEND_IMMEDIATE_SC_TB is

  -- UUT
  component EXTEND_IMMEDIATE_SC is
    port(
      SZ_in  : in  std_logic_vector (23 downto 0);
      ImmSrc : in  std_logic;
      SZ_out : out std_logic_vector (31 downto 0)
      );
  end component;

  -- in
  signal SZ_in  : std_logic_vector (23 downto 0) := (others => 'X');
  signal ImmSrc : std_logic;

  -- out
  signal SZ_out : std_logic_vector (31 downto 0);

  constant CLK_period : time := 10 ns;

begin
  uut : EXTEND_IMMEDIATE_SC
    port map(
      SZ_in  => SZ_in,
      ImmSrc => ImmSrc,
      SZ_out => SZ_out
      );

  stimulus_process : process
  begin
    wait for 100 ns;

    -- Input multiplied by 4,
    -- unsigned extension to 32
    ImmSrc <= '1';
    wait for 1 * CLK_period;
    SZ_in  <= X"000001";
    wait for 1 * CLK_period;
    SZ_in  <= X"000002";
    wait for 1 * CLK_period;
    SZ_in  <= X"000F01";
    wait for 1 * CLK_period;
    SZ_in  <= X"F00000";
    wait for 1 * CLK_period;
    SZ_in  <= X"800000";
    wait for 1 * CLK_period;

    -- 12 to 32 bit signed extension
    ImmSrc <= '0';
    wait for 1 * CLK_period;
    SZ_in  <= X"000001";
    wait for 1 * CLK_period;
    SZ_in  <= X"000002";
    wait for 1 * CLK_period;
    SZ_in  <= X"000F01";
    wait for 1 * CLK_period;
    SZ_in  <= X"001000";                -- This is out of the 12-bit range,
    -- should result to zero after extension
    wait for 1 * CLK_period;
    SZ_in  <= X"000800";                -- 0000 0000 0000 1000 0000 0000
    wait for 1 * CLK_period;

    report "TESTS COMPLETED";
    stop(2);
  end process;

end structural;
