----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/01/2020 09:33:57 PM
-- Design Name: 
-- Module Name: XOR_A_B_n_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use std.env.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity XOR_A_B_n_TB is

end XOR_A_B_n_TB;

architecture Behavioral of XOR_A_B_n_TB is
  component XOR_A_B_n is
    generic (WIDTH : positive := 4);
    port(
      A : in  std_logic_vector (WIDTH-1 downto 0);  -- SRC A
      B : in  std_logic_vector (WIDTH-1 downto 0);  -- SRC B
      Z : out std_logic_vector (WIDTH-1 downto 0)   --Result
      );
  end component;
  signal A            : std_logic_vector (4-1 downto 0);
  signal B            : std_logic_vector (4-1 downto 0);
  signal Z            : std_logic_vector (4-1 downto 0);
  constant CLK_period : time := 10 ns;
begin
  uut : XOR_A_B_n
    generic map(WIDTH => 4)
    port map(
      A => A,
      Z => Z,
      B => B
      );

  stimulus_process : process
  begin
    wait for 100 ns;
    -- AND tests
    A <= "0000";
    B <= "1111";
    wait for 1 * CLK_period;

    A <= "0001";
    B <= "1111";
    wait for 1 * CLK_period;

    A <= "0010";
    B <= "1111";
    wait for 1 * CLK_period;

    A <= "0100";
    B <= "1111";
    wait for 1 * CLK_period;

    A <= "1000";
    B <= "1111";
    wait for 1 * CLK_period;

    A <= "1111";
    B <= "1111";
    wait for 1 * CLK_period;

    A <= "1010";
    B <= "0101";
    wait for 1 * CLK_period;
    -- end AND tests

    -- OR tests
    A <= "1010";
    B <= "0000";
    wait for 1 * CLK_period;

    A <= "0000";
    B <= "0000";
    wait for 1 * CLK_period;

    -- end OR tests
    stop(2);

  end process;

end Behavioral;
