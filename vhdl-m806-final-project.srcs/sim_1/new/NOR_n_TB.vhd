----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/13/2020 01:16:24 PM
-- Design Name: 
-- Module Name: NOR_n_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use std.env.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NOR_n_TB is

end NOR_n_TB;

architecture Behavioral of NOR_n_TB is
  component NOR_n is
    generic (WIDTH : positive := 4);
    port(
      A : in  std_logic_vector (WIDTH-1 downto 0);
      Z : out std_logic
      );
  end component;
  constant CLK_period : time := 10 ns;

  signal A : std_logic_vector(3 downto 0);
  signal Z : std_logic;
begin
  uut : NOR_n
    generic map (WIDTH => 4)
    port map(
      A => A,
      Z => Z
      );
  stimulus_process : process
  begin
    wait for 100 ns;
    NOR_0  : A <= "0000";               --Expect Z=1
    wait for 1 * CLK_period;
    NOR_1  : A <= "0001";               --Expect Z=0
    wait for 1 * CLK_period;
    NOR_N1 : A <= "1111";               --Expect Z=0
    wait for 1 * CLK_period;

    stop(2);
  end process;

end Behavioral;
