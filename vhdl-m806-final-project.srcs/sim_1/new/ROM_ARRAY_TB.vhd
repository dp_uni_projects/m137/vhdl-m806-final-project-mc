----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/13/2020 07:35:49 PM
-- Design Name: 
-- Module Name: ROM_ARRAY_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use std.env.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ROM_ARRAY_TB is
--  Port ( );
end ROM_ARRAY_TB;

architecture Behavioral of ROM_ARRAY_TB is
  component ROM_ARRAY is
    generic (
      N : positive := 6;                --address length
      M : positive := 32                --data word length
      );
    port (
      ADDR     : in  std_logic_vector (N-1 downto 0);
      DATA_OUT : out std_logic_vector (M-1 downto 0)
      );
  end component;

  constant CLK_period : time := 10 ns;

  signal ADDR     : std_logic_vector (5 downto 0);
  signal DATA_OUT : std_logic_vector (31 downto 0);
begin
  uut : ROM_ARRAY
    generic map(N => 6, M => 32)
    port map(
      ADDR     => ADDR,
      DATA_OUT => DATA_OUT
      );

  stimulus_process : process
  begin
    wait for 100 ns;
    ADDR <= std_logic_vector(to_unsigned(0, 6));
    wait for 1*CLK_period;
    ADDR <= std_logic_vector(to_unsigned(1, 6));
    wait for 1*CLK_period;
    ADDR <= std_logic_vector(to_unsigned(2, 6));
    wait for 1*CLK_period;
    ADDR <= std_logic_vector(to_unsigned(3, 6));
    wait for 1*CLK_period;
    ADDR <= std_logic_vector(to_unsigned(4, 6));
    wait for 1*CLK_period;
    ADDR <= std_logic_vector(to_unsigned(5, 6));
    wait for 1*CLK_period;
    ADDR <= std_logic_vector(to_unsigned(6, 6));
    wait for 1*CLK_period;
    ADDR <= std_logic_vector(to_unsigned(7, 6));
    wait for 1*CLK_period;
    ADDR <= std_logic_vector(to_unsigned(63, 6));
    wait for 1*CLK_period;
    ADDR <= std_logic_vector(to_unsigned(62, 6));
    wait for 1*CLK_period;



    stop(2);
  end process;

end Behavioral;
