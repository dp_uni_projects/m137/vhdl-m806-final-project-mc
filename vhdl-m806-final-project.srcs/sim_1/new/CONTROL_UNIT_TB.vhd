----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/15/2020 07:39:11 PM
-- Design Name: 
-- Module Name: CONTROL_UNIT_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use std.env.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CONTROL_UNIT_TB is
end CONTROL_UNIT_TB;

architecture Behavioral of CONTROL_UNIT_TB is
  component CONTROL_UNIT is
    port(
      CLK        : in  std_logic;
      RESET      : in  std_logic;
      INSTR      : in  std_logic_vector(31 downto 0);
      FLAGS      : in  std_logic_vector(3 downto 0);
      PcSrc      : out std_logic_vector(1 downto 0);
      MemToReg   : out std_logic;
      MemWrite   : out std_logic;
      ALUControl : out std_logic_vector(3 downto 0);
      ALUSrc     : out std_logic;
      ImmSrc     : out std_logic;
      RegWrite   : out std_logic;
      RegSrc     : out std_logic_vector(2 downto 0);
      FlagsWrite : out std_logic;
      PCWrite    : out std_logic;
      IRWrite    : out std_logic;
      MAWrite    : out std_logic
      );
  end component;

  signal CLK        : std_logic;
  signal RESET      : std_logic;
  signal INSTR      : std_logic_vector(32-1 downto 0) := (others => 'X');
  signal FLAGS      : std_logic_vector(3 downto 0)    := (others => 'X');
  signal PcSrc      : std_logic_vector(1 downto 0);
  signal MemToReg   : std_logic;
  signal MemWrite   : std_logic;
  signal ALUControl : std_logic_vector(3 downto 0);
  signal ALUSrc     : std_logic;
  signal ImmSrc     : std_logic;
  signal RegWrite   : std_logic;
  signal RegSrc     : std_logic_vector(2 downto 0);
  signal FlagsWrite : std_logic;
  signal PCWrite    : std_logic;
  signal IRWrite    : std_logic;
  signal MAWrite    : std_logic;

  constant CLK_period : time := 10 ns;
begin
  uut : CONTROL_UNIT
    port map(
      INSTR      => INSTR,
      FLAGS      => FLAGS,
      PcSrc      => PcSrc,
      Memtoreg   => MemToReg,
      MemWrite   => MemWrite,
      ALUControl => ALUControl,
      ALUSrc     => ALUSrc,
      ImmSrc     => ImmSrc,
      RegWrite   => RegWrite,
      RegSrc     => RegSrc,
      FlagsWrite => FlagsWrite,
      PCWrite    => PCWrite,
      IRWrite    => IRWrite,
      MAWrite    => MAWrite,
      CLK        => CLK,
      RESET      => RESET
      );

  clock_process : process
  begin
    CLK <= '0';
    wait for CLK_period/2;
    CLK <= '1';
    wait for CLK_period/2;
  end process;

  stimulus_process : process
  begin
    RESET <= '1';
    wait for 100 ns;
    wait until (CLK = '0' and CLK'event);
    RESET <= '0';

    INSTR <= X"E3A00000";               --NOP
    FLAGS <= "0000";
    wait for 1 * CLK_period;
    FLAGS <= "0001";                    -- Negative
    wait for 1 * CLK_period;
    FLAGS <= "0011";                    -- Zero 
    wait for 1 * CLK_period;
    FLAGS <= "0111";                    -- Carry
    wait for 1 * CLK_period;
    FLAGS <= "1111";                    -- Overflow
    wait for 1 * CLK_period;            --end NOP

    INSTR <= X"E0812000";               --ADD : R2 = R1 + R0
    FLAGS <= "0000";
    wait for 1 * CLK_period;
    FLAGS <= "0001";                    --With negative result
    wait for 1 * CLK_period;
    FLAGS <= "0010";                    --With zero result
    wait for 1 * CLK_period;            --end ADD

    INSTR <= X"E0912000";               --ADDS : R2 = R1 + R0
    FLAGS <= "0000";
    wait for 1 * CLK_period;
    FLAGS <= "0001";                    --With negative result
    wait for 1 * CLK_period;
    FLAGS <= "0010";                    --With zero result
    wait for 1 * CLK_period;            --end ADDS

    INSTR <= X"00812000";               --ADDEQ : R2 = R1 + R0
    FLAGS <= "0000";
    wait for 1 * CLK_period;
    FLAGS <= "0001";                    --With negative result
    wait for 1 * CLK_period;
    FLAGS <= "0010";                    --With zero result
    wait for 1 * CLK_period;            --end ADDEQ

    INSTR <= X"00912000";               --ADDSEQ : R2 = R1 + R0
    FLAGS <= "0000";
    wait for 1 * CLK_period;
    FLAGS <= "0001";                    --With negative result
    wait for 1 * CLK_period;
    FLAGS <= "0010";                    --With zero result
    wait for 1 * CLK_period;            --end ADDSEQ

    INSTR <= X"E24230FF";               --SUB : R3 = R2 - 255
    FLAGS <= "0000";
    wait for 1 * CLK_period;
    FLAGS <= "0001";                    --With negative result
    wait for 1 * CLK_period;
    FLAGS <= "0010";                    --With zero result
    wait for 1 * CLK_period;            --end SUB

    -- Test the compiled program which will be run from ROM
    FLAGS <= "0000";
    INSTR <= X"e3a00000";               --MOV R0, #0 -> R0 = 0
    wait for 1 * CLK_period;

    INSTR <= X"e3e01000";               --MVN R1, #0 -> R1 = 0xffffffff
    FLAGS <= "0001";
    wait for 1 * CLK_period;

    FLAGS <= "0001";
    INSTR <= X"e0812000";  --ADD R2, R1, R0 -> R2 = 0 + 0xffffffff         
    wait for 1 * CLK_period;

    FLAGS <= "0000";
    INSTR <= X"e24230ff";               --SUB R3, R2, #255 -> R3 = 0xffffffff -
    --0x000000ff = 0xFFFFFF00
    wait for 1 * CLK_period;

    INSTR <= X"e1a00000";               --MOV, R0, R0 -> R0 = R0
    wait for 1 * CLK_period;

    INSTR <= X"eafffff9";               --B MAIN PROGRAM
    wait for 3 * CLK_period;

    stop(2);
  end process;

end Behavioral;
