----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/16/2020 08:21:39 PM
-- Design Name: 
-- Module Name: ASR_ROR_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use std.env.all;

entity ASR_ROR_TB is
--  Port ( );
end ASR_ROR_TB;

architecture Behavioral of ASR_ROR_TB is
  component ASR_ROR_n is
    generic(WIDTH : positive := 4);
    port (
      A    : in  std_logic_vector (WIDTH-1 downto 0);  -- First operand
      B    : in  std_logic_vector (4 downto 0);        -- Shift amount
      RorA : in  std_logic;                            -- ROR or ASR
      R    : out std_logic_vector (WIDTH-1 downto 0)   -- Result
      );
  end component;

  signal A            : std_logic_vector (32-1 downto 0);  -- First operand
  signal B            : std_logic_vector (4 downto 0);     -- Shift amount
  signal RorA         : std_logic;                         -- ROR or ASR
  signal R            : std_logic_vector (32-1 downto 0);  -- Result
  constant CLK_period : time := 10 ns;
begin

  uut : ASR_ROR_n
    generic map(WIDTH => 32)
    port map(
      A    => A,
      B    => B,
      RorA => RorA,
      R    => R
      );

  stimulus_process : process
  begin
    wait for 100 ns;

    RorA <= '0';
    A    <= std_logic_vector(to_signed(1, 32));
    B    <= std_logic_vector(to_signed(1, 5));
    wait for 1*CLK_period;

    A <= X"00000008";
    wait for 1*CLK_period;

    A <= X"80000000";
    wait for 1*CLK_period;

    B <= std_logic_vector(to_signed(2, 5));
    wait for 1*CLK_period;

    B <= std_logic_vector(to_signed(31, 5));
    wait for 1*CLK_period;
    stop(2);
  end process;

end Behavioral;
