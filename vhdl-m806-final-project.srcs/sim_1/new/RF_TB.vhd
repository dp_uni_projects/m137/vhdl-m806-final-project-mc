----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/23/2020 09:38:44 PM
-- Design Name: 
-- Module Name: RF_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use std.env.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RF_TB is
end RF_TB;

architecture Behavioral of RF_TB is
  component RF_REGrwe is
    port (
      CLK       : in  std_logic;
      WE        : in  std_logic;
      REG_WRITE : in  std_logic;
      ADDR_W    : in  std_logic_vector(4-1 downto 0);
      ADDR_R1   : in  std_logic_vector(4-1 downto 0);
      ADDR_R2   : in  std_logic_vector(4-1 downto 0);
      DATA_IN   : in  std_logic_vector(32-1 downto 0);
      R15       : in  std_logic_vector(32-1 downto 0);  -- Extra register,
                                                        -- coming from PC
      RESET     : in  std_logic;
      DATA_OUT1 : out std_logic_vector(32-1 downto 0);
      DATA_OUT2 : out std_logic_vector(32-1 downto 0)
      );
  end component;

  signal CLK          : std_logic                       := '0';
  signal WE           : std_logic                       := '0';  --For other registers
  signal RESET        : std_logic                       := '1';
  signal REG_WRITE    : std_logic                       := '0';  --For RF
  signal ADDR_W       : std_logic_vector(4-1 downto 0)  := (others => 'X');
  signal ADDR_R1      : std_logic_vector(4-1 downto 0)  := (others => 'X');
  signal ADDR_R2      : std_logic_vector(4-1 downto 0)  := (others => 'X');
  signal DATA_IN      : std_logic_vector(32-1 downto 0) := (others => 'X');
  signal R15          : std_logic_vector(32-1 downto 0) := (others => 'X');
  signal DATA_OUT1    : std_logic_vector(32-1 downto 0) := (others => 'X');
  signal DATA_OUT2    : std_logic_vector(32-1 downto 0) := (others => 'X');
  constant CLK_period : time                            := 10 ns;
begin
  uut : RF_REGrwe
    port map(
      CLK       => CLK,
      WE        => WE,
      RESET     => RESET,
      ADDR_W    => ADDR_W,
      ADDR_R1   => ADDR_R1,
      ADDR_R2   => ADDR_R2,
      DATA_IN   => DATA_IN,
      REG_WRITE => REG_WRITE,
      R15       => R15,
      DATA_OUT1 => DATA_OUT1,
      DATA_OUT2 => DATA_OUT2
      );
  clk_process : process
  begin
    CLK <= '0';
    wait for CLK_period/2;
    CLK <= '1';
    wait for CLK_period/2;
  end process;

  stimulus_process : process
  begin
    RESET <= '1';
    wait for 100 ns;
    wait until (CLK = '0' and CLK'event);
    RESET <= '0';
    WE    <= '1';
    R15   <= std_logic_vector(to_unsigned(15*4, 32));  --R15 = 0x3C

    -- Initialize all registers to a value same as their (reg number * 4)
    REG_WRITE <= '1';
    INIT : for I in 0 to 14 loop
      DATA_IN <= std_logic_vector(to_unsigned(I*4, 32));
      ADDR_W  <= std_logic_vector(to_unsigned(I, 4));  --R0    
      wait for 1*CLK_period;
    end loop INIT;

    -- Test reading from all reagisters
    REG_WRITE <= '0';                   --Disable write
    TEST_READ : for I in 0 to 7 loop
      -- Read from R0
      ADDR_R1 <= std_logic_vector(to_unsigned(I*2, 4));
      -- Read from R1
      ADDR_R2 <= std_logic_vector(to_unsigned(I*2+1, 4));
      wait for 1*CLK_period;
    end loop TEST_READ;

    -- Test writing 999 (0x3E7)  to R15, should fail
    REG_WRITE <= '1';
    ADDR_W    <= std_logic_vector(to_unsigned(15, 4));  --R15
    DATA_IN   <= std_logic_vector(to_unsigned(999, 32));
    wait for 1*CLK_period;

    -- Read from R15, should read 0x3c on DATA_OUT1
    REG_WRITE <= '0';
    ADDR_R1   <= std_logic_vector(to_unsigned(15, 4));
    wait for 3*CLK_period;
    stop(2);
  end process;

end Behavioral;
