----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/03/2020 04:45:03 PM
-- Design Name: 
-- Module Name: FSM_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use std.env.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSM_TB is
--  Port ( );
end FSM_TB;

architecture Behavioral of FSM_TB is
  component FSM is
    port(
      NoWrite_in : in std_logic;
      CondEx_in  : in std_logic;
      OP         : in std_logic_vector(1 downto 0);
      RD         : in std_logic_vector(3 downto 0);
      SL         : in std_logic;
      Instr_24   : in std_logic;
      CLK        : in std_logic;
      RESET      : in std_logic;

      PCWrite    : out std_logic;
      IRWrite    : out std_logic;
      RegWrite   : out std_logic;
      FlagsWrite : out std_logic;
      MAWrite    : out std_logic;
      MemWrite   : out std_logic;
      PCSrc      : out std_logic_vector(1 downto 0)
      );
  end component;

  signal NoWrite_in   : std_logic;
  signal CondEx_in    : std_logic;
  signal OP           : std_logic_vector(1 downto 0);
  signal RD           : std_logic_vector(3 downto 0);
  signal SL           : std_logic;
  signal Instr_24     : std_logic;
  signal PCWrite      : std_logic;
  signal IRWrite      : std_logic;
  signal RegWrite     : std_logic;
  signal FlagsWrite   : std_logic;
  signal MAWrite      : std_logic;
  signal MemWrite     : std_logic;
  signal PCSrc        : std_logic_vector(1 downto 0);
  signal CLK          : std_logic;
  signal RESET        : std_logic;
  constant CLK_period : time := 10 ns;
begin
  uut : FSM
    port map(
      NoWrite_in => NoWrite_in,
      CondEx_in  => CondEx_in,
      OP         => OP,
      RD         => RD,
      SL         => SL,
      Instr_24   => Instr_24,
      PCWrite    => PCWrite,
      IRWrite    => IRWrite,
      RegWrite   => RegWrite,
      FlagsWrite => FlagsWrite,
      MAWrite    => MAWrite,
      MemWrite   => MemWrite,
      PCSrc      => PCSrc,
      CLK        => CLK,
      RESET      => RESET
      );

  clock_process : process
  begin
    CLK <= '0';
    wait for CLK_period/2;
    CLK <= '1';
    wait for CLK_period/2;
  end process;

  stimulus_process : process
  begin
    RESET <= '1';
    wait for 100 ns;
    wait until (CLK = '0' and CLK'event);
    RESET <= '0';

    wait for 2*CLK_period;
    stop(2);
  end process;
end Behavioral;
