----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/21/2020 08:48:57 PM
-- Design Name: 
-- Module Name: EXTEND_n_m_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;
use STD.ENV.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EXTEND_n_m_TB is

end EXTEND_n_m_TB;

architecture Behavioral of EXTEND_n_m_TB is
  component EXTEND_n_m is
    generic (
      WIDTH_IN  : positive := 12;
      WIDTH_OUT : positive := 32
      );
    port(
      SZ_in  : in  std_logic_vector (WIDTH_IN-1 downto 0);  --Signed/Unsigned in
      SZ_out : out std_logic_vector (WIDTH_OUT-1 downto 0);
      SorZ   : in  std_logic            --Select Signed or unsigned extension
      );
  end component;

  -- in
  signal SZ_in : std_logic_vector (11 downto 0) := (others => 'X');
  signal SorZ  : std_logic;

  -- out
  signal SZ_out       : std_logic_vector (31 downto 0);
  constant CLK_period : time := 10 ns;

begin

  uut : EXTEND_n_m
    port map (
      SZ_in  => SZ_in,
      SorZ   => SorZ,
      SZ_out => SZ_out
      );
  stimulus_process : process
  begin
    wait for 100 ns;

    -- Unsigned
    SorZ  <= '0';
    SZ_in <= X"001";
    wait for 1 * CLK_period;
    SZ_in <= X"002";
    wait for 1 * CLK_period;
    SZ_in <= X"F01";
    wait for 1 * CLK_period;
    SZ_in <= X"801";
    wait for 1 * CLK_period;
    -- Signed
    SorZ  <= '1';
    SZ_in <= X"001";
    wait for 1 * CLK_period;
    SZ_in <= X"002";
    wait for 1 * CLK_period;
    SZ_in <= X"F01";
    wait for 1 * CLK_period;
    SZ_in <= X"801";
    wait for 1 * CLK_period;

    stop(2);
  end process;
end Behavioral;
