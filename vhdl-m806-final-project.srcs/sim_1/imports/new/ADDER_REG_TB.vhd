----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 07/05/2020 09:35:10 PM
-- Design Name:
-- Module Name: ADDER_REG_TB - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use STD.ENV.all;

entity ADDER_REG_TB is

end ADDER_REG_TB;


architecture Behavioral of ADDER_REG_TB is

-- Unit Under Test (UUT)
  component ADDER_REG_8
    port(
      CLK   : in  std_logic;
      RESET : in  std_logic;
      WE    : in  std_logic;
      A     : in  std_logic_vector (7 downto 0);
      B     : in  std_logic_vector (7 downto 0);
      S     : out std_logic_vector (7 downto 0);
      Cout  : out std_logic;
      OV    : out std_logic);
  end component;

-- Internal signals
  signal CLK   : std_logic                     := '0';
  signal RESET : std_logic                     := '1';
  signal WE    : std_logic                     := '0';
  signal A     : std_logic_vector (7 downto 0) := (others => 'X');
  signal B     : std_logic_vector (7 downto 0) := (others => 'X');

  -- Internal outputs from UUT
  signal S    : std_logic_vector (7 downto 0);
  signal Cout : std_logic;
  signal OV   : std_logic;

  -- Clock period definition
  constant CLK_period : time := 5.270 ns;

begin

  -- Instantiate the UUT
  uut : ADDER_REG_8
    port map(
      CLK   => CLK,
      RESET => RESET,
      WE    => WE,
      A     => A,
      B     => B,
      S     => S,
      Cout  => Cout,
      OV    => OV
      );

  -- Clock process definition
  CLK_process : process
  begin
    CLK <= '0';
    wait for CLK_period/2;
    CLK <= '1';
    wait for CLK_period/2;
  end process;

  Stimulus_process : process
  begin
    -- Sync RESET is deasserted on CLK falling edge
    -- after GSR signal disable (it remains enabled for 100 ns)
    RESET <= '1';
    wait for 100 ns;
    wait until (CLK = '0' and CLK'event);
    RESET <= '0';

    WE <= '0';
    A  <= X"00";
    B  <= X"00";
    wait for 1*CLK_period;
    WE <= '0';
    A  <= X"FF";
    B  <= X"FF";
    wait for 1*CLK_period;
    WE <= '0';
    A  <= X"00";
    B  <= X"00";
    wait for 1*CLK_period;
    WE <= '1';
    A  <= X"FF";
    B  <= X"FF";
    wait for 1*CLK_period;
    WE <= '1';
    A  <= X"00";
    B  <= X"FF";
    wait for 1*CLK_period;
    WE <= '1';
    A  <= X"00";
    B  <= X"00";
    wait for 1*CLK_period;
    WE <= '1';
    A  <= X"FF";
    B  <= X"00";
    wait for 1*CLK_period;
    WE <= '1';
    A  <= X"00";
    B  <= X"00";
    wait for 1*CLK_period;
    WE <= '1';
    A  <= X"7F";
    B  <= X"01";
    wait for 1*CLK_period;
    WE <= '1';
    A  <= X"00";
    B  <= X"00";
    wait for 1*CLK_period;
    WE <= '1';
    A  <= X"FF";
    B  <= X"80";
    wait for 1*CLK_period;
    WE <= '1';
    A  <= X"00";
    B  <= X"00";
    wait for 2*CLK_period;

    -- Termination message
    report "TESTS COMPLETED";
    stop(2);
  end process;

end Behavioral;
