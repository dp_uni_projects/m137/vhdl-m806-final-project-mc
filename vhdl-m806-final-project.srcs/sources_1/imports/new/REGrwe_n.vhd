--------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/02/2020 03:02:15 PM
-- Design Name: 
-- Module Name: REGrwe_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity REGrwe_n is
  generic (WIDTH : positive := 4);      -- Default value of 4
  port (
    CLK   : in  std_logic;
    RESET : in  std_logic;
    WE    : in  std_logic;
    Din   : in  std_logic_vector (WIDTH-1 downto 0);
    Dout  : out std_logic_vector (WIDTH-1 downto 0));
end REGrwe_n;

architecture Behavioral of REGrwe_n is

begin
  process (CLK)
  begin
    if (CLK = '1' and CLK'event) then
      if (RESET = '1') then Dout <= (others => '0');
      elsif (WE = '1') then Dout <= Din;
      end if;
    end if;
  end process;
end Behavioral;
