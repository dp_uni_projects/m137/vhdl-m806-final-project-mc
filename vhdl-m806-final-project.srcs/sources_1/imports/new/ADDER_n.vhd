----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/02/2020 03:51:48 PM
-- Design Name: 
-- Module Name: ADDER_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ADDER_n is
  generic (WIDTH : positive := 4);
  port (
    A    : in  std_logic_vector (WIDTH-1 downto 0);
    B    : in  std_logic_vector (WIDTH-1 downto 0);
    S    : out std_logic_vector (WIDTH-1 downto 0);
    Cout : out std_logic;
    OV   : out std_logic);
end ADDER_n;

architecture BEHAVIORAL of ADDER_n is
begin
  ADDER : process (A, B)
    variable A_s, B_s, S_s : signed (WIDTH+1 downto 0);
  begin
    A_s  := signed('0' & A(WIDTH-1) & A);
    B_s  := signed('0' & B(WIDTH-1) & B);
    S_s  := A_s + B_s;
    S    <= std_logic_vector(S_s(WIDTH-1 downto 0));  -- numeric_std
    OV   <= S_s(WIDTH) xor S_s(WIDTH-1);
    Cout <= S_s(WIDTH+1);
  end process;
end BEHAVIORAL;

