----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/16/2020 03:30:21 PM
-- Design Name: 
-- Module Name: MUX_2_to_1_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUX_2_to_1_n is
  generic (
    WIDTH : positive := 4
    );
  port(
    A0 : in  std_logic_vector (WIDTH-1 downto 0);
    A1 : in  std_logic_vector (WIDTH-1 downto 0);
    S  : in  std_logic;
    Y  : out std_logic_vector (WIDTH-1 downto 0)
    );
end MUX_2_to_1_n;

architecture Behavioral of MUX_2_to_1_n is

begin
  process(A0, A1, S)
  begin
    if (S = '0') then
      Y <= A0;
    else
      Y <= A1;
    end if;
  end process;
end Behavioral;
