----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/29/2020 07:21:43 PM
-- Design Name: 
-- Module Name: NOR_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NOR_n is
  generic (WIDTH : positive := 4);
  port(
    A : in  std_logic_vector (WIDTH-1 downto 0);
    Z : out std_logic
    );
end NOR_n;

architecture Behavioral of NOR_n is
  signal X : std_logic_vector (WIDTH downto 0);
begin
  INITIALIZATION : X(0) <= '0';
  G1             : for I in 0 to WIDTH-1 generate
    X(I+1) <= not(A(I) nor X(I));
  end generate G1;
  OUTPUT : Z <= not(X(WIDTH));

end Behavioral;
