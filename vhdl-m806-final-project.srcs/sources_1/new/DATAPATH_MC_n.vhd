----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/16/2020 12:05:07 PM
-- Design Name: 
-- Module Name: DATAPATH_MC_n - Structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Single cycle VHDL implementation of a minimal 32-bit ARM architecture
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DATAPATH_MC_n is
  generic (WIDTH : positive := 32);
  port (
    CLK           : in  std_logic;
    RESET         : in  std_logic;
    PcSrc         : in  std_logic_vector(1 downto 0);
    MemToReg      : in  std_logic;
    MemWrite      : in  std_logic;
    ALUControl    : in  std_logic_vector(3 downto 0);
    ALUSrc        : in  std_logic;
    ImmSrc        : in  std_logic;
    RegWrite      : in  std_logic;
    RegSrc        : in  std_logic_vector(2 downto 0);
    FlagsWrite    : in  std_logic;
    Flags_out     : out std_logic_vector(3 downto 0);
    Instr_out     : out std_logic_vector(WIDTH-1 downto 0);
    PC_out        : out std_logic_vector(WIDTH-1 downto 0);
    ALUResult_out : out std_logic_vector(WIDTH-1 downto 0);
    Result_out    : out std_logic_vector(WIDTH-1 downto 0);
    WriteData_out : out std_logic_vector(WIDTH-1 downto 0);

    -- For multi-cycle implementation
    PCWrite : in std_logic;
    IRWrite : in std_logic;
    MAWrite : in std_logic
    );
end DATAPATH_MC_n;

architecture Structural of DATAPATH_MC_n is
  component REG_n
    generic (WIDTH : positive := 32);
    port(
      CLK  : in  std_logic;
      Din  : in  std_logic_vector (WIDTH-1 downto 0);
      Dout : out std_logic_vector(WIDTH-1 downto 0)
      );
  end component;

  component REGrwe_n is
    generic (WIDTH : positive := 4);    -- Default value of 4
    port (
      CLK   : in  std_logic;
      RESET : in  std_logic;
      WE    : in  std_logic;
      Din   : in  std_logic_vector (WIDTH-1 downto 0);
      Dout  : out std_logic_vector (WIDTH-1 downto 0));
  end component;

  component ROM_ARRAY
    generic (
      N : positive := 4;                --address length
      M : positive := 32                --data word length
      );
    port (
      ADDR     : in  std_logic_vector (N-1 downto 0);
      DATA_OUT : out std_logic_vector (M-1 downto 0)
      );
  end component;

  component RAM_ARRAY is
    generic (
      N : positive := 10;               --address length
      M : positive := 32);              --data word length
    port(
      CLK      : in  std_logic;
      WE       : in  std_logic;
      ADDR     : in  std_logic_vector (N-1 downto 0);
      DATA_IN  : in  std_logic_vector (M-1 downto 0);
      DATA_OUT : out std_logic_vector (M-1 downto 0)
      );
  end component;

  -- Configurable 2-to-1 MUX
  component MUX_2_to_1_n is
    generic (
      WIDTH : positive := 4
      );
    port(
      A0 : in  std_logic_vector (WIDTH-1 downto 0);
      A1 : in  std_logic_vector (WIDTH-1 downto 0);
      S  : in  std_logic;
      Y  : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  component MUX_3_to_1_n is
    generic(WIDTH : positive := 4);
    port(
      S  : in  std_logic_vector (1 downto 0);
      A0 : in  std_logic_vector (WIDTH-1 downto 0);
      A1 : in  std_logic_vector (WIDTH-1 downto 0);
      A2 : in  std_logic_vector (WIDTH-1 downto 0);
      Y  : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  component ADDER_n is
    generic (WIDTH : positive := 4);
    port (
      A    : in  std_logic_vector (WIDTH-1 downto 0);
      B    : in  std_logic_vector (WIDTH-1 downto 0);
      S    : out std_logic_vector (WIDTH-1 downto 0);
      Cout : out std_logic;
      OV   : out std_logic);
  end component;

  -- Register file (R0-R14 and input for R15)
  component REGISTER_FILE_wR15 is
    generic (
      N : positive := 4;                --Register addressing width
      M : positive := 32                --Data width
      );
    port (
      CLK       : in  std_logic;
      WE        : in  std_logic;
      ADDR_W    : in  std_logic_vector(N-1 downto 0);
      ADDR_R1   : in  std_logic_vector(N-1 downto 0);
      ADDR_R2   : in  std_logic_vector(N-1 downto 0);
      DATA_IN   : in  std_logic_vector(M-1 downto 0);
      R15       : in  std_logic_vector(M-1 downto 0);  -- Extra register,
                                                       -- coming from PC
      DATA_OUT1 : out std_logic_vector(M-1 downto 0);
      DATA_OUT2 : out std_logic_vector(M-1 downto 0)
      );
  end component;

  -- Extend immediate unit
  component EXTEND_IMMEDIATE_SC is
    port(
      SZ_in  : in  std_logic_vector (23 downto 0);
      ImmSrc : in  std_logic;
      SZ_out : out std_logic_vector (31 downto 0)
      );
  end component;

  -- ALU
  component ALU_n is
    -- generic (WIDTH : positive := 32);
    port(
      -- in
      SRC_A      : in  std_logic_vector (WIDTH-1 downto 0);  -- Source A data
      SRC_B      : in  std_logic_vector (WIDTH-1 downto 0);  -- Source B data
      ALUControl : in  std_logic_vector (3 downto 0);  -- Control signal from CU
      SHAMT5     : in  std_logic_vector (4 downto 0);
      Flags      : out std_logic_vector (3 downto 0);  -- 4 status flags
      ALUResult  : out std_logic_vector (WIDTH-1 downto 0)   -- Final result
      );
  end component;

  -- Various internal signals
  -- Program counter
  signal PC_next         : std_logic_vector (WIDTH-1 downto 0);
  signal PC_current      : std_logic_vector (WIDTH-1 downto 0);
  signal Instr_in        : std_logic_vector (WIDTH-1 downto 0);  --Internal instruction signal
  signal Instr_in_regIR  : std_logic_vector (WIDTH-1 downto 0);  --Internal instruction signal
                                        --after REG_IR register
  signal PCPlus4         : std_logic_vector (WIDTH-1 downto 0);
  signal PCPlus4_regPCP4 : std_logic_vector (WIDTH-1 downto 0);  --After REG_PCP4 register
  signal PCPlus8         : std_logic_vector (WIDTH-1 downto 0);

  -- MUX outputs
  signal RA1_in             : std_logic_vector (3 downto 0);
  signal RA2_in             : std_logic_vector (3 downto 0);
  signal Result             : std_logic_vector (WIDTH-1 downto 0);
  signal SrcB               : std_logic_vector (WIDTH-1 downto 0);
  signal ALUResult_in       : std_logic_vector(WIDTH-1 downto 0);  --After ALU
  signal ALUResult_in_regMA : std_logic_vector(WIDTH-1 downto 0);  --After REG_MA
  signal ALUResult_in_regS  : std_logic_vector(WIDTH-1 downto 0);  --After REG_MA  

  -- Register File I/O
  signal SrcA            : std_logic_vector (WIDTH-1 downto 0);  --After RF
  signal SrcA_regA       : std_logic_vector (WIDTH-1 downto 0);  --After REG_A
  signal WriteData       : std_logic_vector (WIDTH-1 downto 0);  --After RF
  signal WriteData_regB  : std_logic_vector (WIDTH-1 downto 0);  --After REG_B
  signal WriteData_regWD : std_logic_vector (WIDTH-1 downto 0);  --After REG_WD
  signal WD3_in          : std_logic_vector (WIDTH-1 downto 0);
  signal RA3_in          : std_logic_vector (3 downto 0);

  -- RAM
  signal ReadData_in       : std_logic_vector (WIDTH-1 downto 0);  --After DM
  signal ReadData_in_regRD : std_logic_vector (WIDTH-1 downto 0);  --After REG_RD

  -- ALU
  signal FlagsALU_in : std_logic_vector (3 downto 0);

  -- Extend
  signal ExtImm      : std_logic_vector (WIDTH-1 downto 0);  --After EXTEND
  signal ExtImm_regI : std_logic_vector (WIDTH-1 downto 0);  --After REG_I

begin  --Subcomponents
  EXTEND : EXTEND_IMMEDIATE_SC
    port map(
      ImmSrc => ImmSrc,
      SZ_in  => Instr_in_regIR(23 downto 0),
      SZ_out => ExtImm
      );

  REG_I : REGrwe_n
    generic map (WIDTH => 32)
    port map (
      RESET => RESET,
      CLK   => CLK,
      WE    => '1',
      Din   => ExtImm,
      Dout  => ExtImm_regI
      );

  MUX_PC_SRC : MUX_3_to_1_n
    generic map(WIDTH => WIDTH)
    port map(
      A2 => ALUResult_in,
      A1 => Result,
      A0 => PCPlus4_regPCP4,
      S  => PcSrc,
      Y  => PC_next
      );

  -- Status register, holding the ALU flags
  STATUS_REG : REGrwe_n
    generic map (WIDTH => 4)
    port map (
      RESET => RESET,
      CLK   => CLK,
      WE    => FlagsWrite,
      Din   => FlagsALU_in,
      Dout  => Flags_out
      );

  -- Progam counter register
  PC_REG : REGrwe_n
    generic map (WIDTH => WIDTH)
    port map (
      CLK   => CLK,
      RESET => RESET,
      WE    => PCWrite,
      Din   => PC_next,
      Dout  => PC_current
      );

  REG_IR : REGrwe_n
    generic map (WIDTH => 32)
    port map (
      RESET => RESET,
      CLK   => CLK,
      WE    => IRWrite,
      Din   => Instr_in,
      Dout  => Instr_in_regIR
      );

  -- Instruction memory ROM
  INSTR_MEMORY : ROM_ARRAY
    generic map (N => 6, M => 32)       --2^6 instructions max, 32-bit words
    port map(
      ADDR     => PC_current(7 downto 2),  --Keep 7:2 bits from instr (word addressing)
      DATA_OUT => Instr_in
      );

  DATA_MEMORY : RAM_ARRAY
    generic map (N => 5, M => 32)
    port map(
      CLK      => CLK,
      WE       => MemWrite,
      DATA_IN  => WriteData_regWD,
      ADDR     => ALUResult_in_regMA(6 downto 2),
      DATA_OUT => ReadData_in
      );

  REG_RD : REGrwe_n
    generic map (WIDTH => 32)
    port map (
      RESET => RESET,
      CLK   => CLK,
      WE    => '1',
      Din   => ReadData_in,
      Dout  => ReadData_in_regRD
      );

  MUX_REG_SRC_0 : MUX_2_to_1_n
    generic map (WIDTH => 4)
    port map(
      A0 => Instr_in_regIR(19 downto 16),
      A1 => X"F",
      S  => RegSrc(0),                  -- fist bit of RegSrc
      Y  => RA1_in                      -- MUX output
      );

  MUX_REG_SRC_1 : MUX_2_to_1_n
    generic map (WIDTH => 4)
    port map(
      A0 => Instr_in_regIR(3 downto 0),
      A1 => Instr_in_regIR(15 downto 12),
      S  => RegSrc(1),                  -- second  bit of RegSrc
      Y  => RA2_in                      -- MUX output
      );

  MUX_REG_SRC_2_0 : MUX_2_to_1_n
    generic map (WIDTH => 4)
    port map(
      A0 => Instr_in_regIR(15 downto 12),
      A1 => X"E",
      S  => RegSrc(2),                  -- third bit of RegSrc
      Y  => RA3_in                      -- MUX output
      );

  MUX_REG_SRC_2_1 : MUX_2_to_1_n
    generic map (WIDTH => WIDTH)
    port map(
      A0 => Result,
      A1 => PCPlus4_regPCP4,
      S  => RegSrc(2),                  -- third bit of RegSrc
      Y  => WD3_in                      -- MUX output
      );

  -- First adder
  INC_4_0 : ADDER_n
    generic map (WIDTH => WIDTH)
    port map(
      A    => PC_current,
      B    => X"00000004",
      S    => PCPlus4,
      Cout => open,
      OV   => open
      );

  REG_PCP4 : REGrwe_n
    generic map (WIDTH => 32)
    port map (
      RESET => RESET,
      CLK   => CLK,
      WE    => '1',
      Din   => PCPlus4,
      Dout  => PCPlus4_regPCP4
      );

  -- Second adder
  INC_4_1 : ADDER_n
    generic map (WIDTH => WIDTH)
    port map(
      A    => PCPlus4_regPCP4,
      B    => X"00000004",
      S    => PCPlus8,
      Cout => open,
      OV   => open
      );

  REG_FILE : REGISTER_FILE_wR15
    generic map (N => 4, M => 32)
    port map(
      CLK       => CLK,
      WE        => RegWrite,
      ADDR_R1   => RA1_in,
      ADDR_R2   => RA2_in,
      ADDR_W    => RA3_in,
      R15       => PCPlus8,
      DATA_IN   => WD3_in,
      DATA_OUT1 => SrcA,
      DATA_OUT2 => WriteData
      );

  REG_A : REGrwe_n
    generic map (WIDTH => 32)
    port map (
      RESET => RESET,
      CLK   => CLK,
      WE    => '1',
      Din   => SrcA,
      Dout  => SrcA_regA
      );

  REG_B : REGrwe_n
    generic map (WIDTH => 32)
    port map (
      RESET => RESET,
      CLK   => CLK,
      WE    => '1',
      Din   => WriteData,
      Dout  => WriteData_regB
      );

  MUX_ALU_SRC : MUX_2_to_1_n
    generic map (WIDTH => WIDTH)
    port map(
      A0 => WriteData_regB,
      A1 => ExtImm_regI,
      S  => ALUSrc,
      Y  => SrcB
      );

  MUX_MEM_REG : MUX_2_to_1_n
    generic map (WIDTH => WIDTH)
    port map(
      A0 => ALUResult_in_regS,
      A1 => ReadData_in_regRD,
      S  => MemtoReg,
      Y  => Result
      );

  ALU : ALU_n
    -- generic map(WIDTH => WIDTH)
    port map(
      SRC_A      => SrcA_regA,
      SRC_B      => SrcB,
      ALUControl => ALUControl,
      ALUResult  => ALUResult_in,
      Flags      => FlagsALU_in,
      SHAMT5     => Instr_in_regIR(11 downto 7)
      );

  REG_MA : REGrwe_n
    generic map (WIDTH => 32)
    port map (
      RESET => RESET,
      CLK   => CLK,
      WE    => MAWrite,
      Din   => ALUResult_in,
      Dout  => ALUResult_in_regMA
      );

  REG_WD : REGrwe_n
    generic map (WIDTH => 32)
    port map (
      RESET => RESET,
      CLK   => CLK,
      WE    => '1',
      Din   => WriteData_regB,
      Dout  => WriteData_regWD
      );

  REG_S : REGrwe_n
    generic map (WIDTH => 32)
    port map (
      RESET => RESET,
      CLK   => CLK,
      WE    => '1',
      Din   => ALUResult_in,
      Dout  => ALUResult_in_regS
      );

  Instr_out     <= Instr_in_regIR;
  WriteData_out <= WriteData;
  PC_out        <= PC_current;
  ALUResult_out <= ALUResult_in;
  Result_out    <= Result;
end Structural;
