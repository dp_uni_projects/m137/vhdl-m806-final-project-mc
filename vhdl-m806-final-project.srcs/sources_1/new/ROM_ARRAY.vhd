----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/15/2020 01:49:38 PM
-- Design Name: 
-- Module Name: ROM_ARRAY - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ROM_ARRAY is
  generic (
    N : positive := 6;                  --address length
    M : positive := 32                  --data word length
    );
  port (
    ADDR     : in  std_logic_vector (N-1 downto 0);
    DATA_OUT : out std_logic_vector (M-1 downto 0)
    );
end ROM_ARRAY;

architecture Behavioral of ROM_ARRAY is
  type ROM_ARRAY is array (0 to 2**N-1)  --Used this instead of downto which
                                         --accessed the instructions in
                                         --reverse order
    of std_logic_vector (M-1 downto 0);
  constant ROM : ROM_ARRAY := (
    -- MAIN_PROGRAM:
    --   MOV R0, #0
    --   MVN R1, #0
    --   ADD R2, R1, R0
    --   SUB R3, R2, #255
    --   MOV R0, R0   
    --   B MAIN_PROGRAM    
    -- X"e3a00000", X"e3e01000", X"e0812000", X"e24230ff",
    -- X"e1a00000", X"eafffff9", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000"

--     start:
--  MOV     R0, #1              ;R0 = 1
--  MOV     R1, #1              ;R1 = 1
--  MOV     R2, #2              ;R2 = 2
--  MOV     R3, #0              ;R3 = 0
--  MOV     R4, #0x12           ;R4 = 0x12
--  BL      sum                 ;SUM R1, R2, R3, R4
--  ;; R0 should be 1+1+2+0 = 4 by now
--  MOV     R0, R0              ;NOP
--  AND     R5, R0, R1          ;R5 = R0 & R1 = 4 & 1 = 0
--  ROR     R4, R0, R1          ;R4 = R0 ROR R1 = 1 ROR 1 = 0x80000000
--  B       exit

-- sum:                         ;SUM R0+R1+R2+R3
--  MOV     R12, R4             ;R12 = R4 -- save R4
--  ADD     R4, R0, R1          ;R4 = R0 + R1
--  ADD     R4, R4, R2          ;R4 = R4 + R2
--  ADD     R4, R4, R3          ;R4 = R4 + R3
--  MOV     R0, R4              ;R0 = R4
--  MOV     R4, R12             ;R4 = R12
--  MOV     PC, LR              ;Return
-- exit:    
    -- X"e3a00001", X"e3a01001", X"e3a02002", X"e3a03000",
    -- X"e3a04012", X"eb000003", X"e1a00000", X"e0005001",
    -- X"e1a041e0", X"ea000006", X"e1a0c004", X"e0804001",
    -- X"e0844002", X"e0844003", X"e1a00004", X"e1a0400c",
    -- X"e1a0f00e", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000",
    -- X"00000000", X"00000000", X"00000000", X"00000000"
    -- );


    -- *** test_complete.asm *** 
    X"e3a000ff",
    X"e1e01000",
    X"e3e010aa",
    X"e2811055",
    X"e24110aa",
    X"e1500001",
    X"022120aa",
    X"e23120aa",
    X"e20120ff",
    X"41813002",
    X"51813002",
    X"e0400002",
    X"eb00000e",
    X"e1a00000",
    X"e1a00100",
    X"e1a000a0",
    X"e3a00001",
    X"e1a000e0",
    X"e1a000c0",
    X"e3a04001",
    X"e5840000",
    X"e5941000",
    X"e1500001",
    X"e584f000",
    X"e594f000",
    X"e25ff004",
    X"e24ff004",
    X"ea000006",
    X"e1a0c004",
    X"e0804001",
    X"e0844002",
    X"e0844003",
    X"e1a00004",
    X"e1a0400c",
    X"e1a0f00e",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000",
    X"00000000"
    );

begin
  DATA_OUT <= ROM(to_integer(unsigned(ADDR)));

end Behavioral;
