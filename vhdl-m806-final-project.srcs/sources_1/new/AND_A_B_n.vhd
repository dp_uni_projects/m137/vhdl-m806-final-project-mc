----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/30/2020 02:11:08 PM
-- Design Name: 
-- Module Name: AND_A_B_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AND_A_B_n is
  generic (WIDTH : positive := 4);
  port(
    A : in  std_logic_vector (WIDTH-1 downto 0);
    B : in  std_logic_vector (WIDTH-1 downto 0);
    Z : out std_logic_vector (WIDTH-1 downto 0)
    );
end AND_A_B_n;

architecture Behavioral of AND_A_B_n is
  signal X : std_logic_vector (WIDTH-1 downto 0);
begin
  -- INITIALIZATION : X(0) <= (others => 'X');
  G1 : for I in 0 to WIDTH-1 generate
    X(I) <= A(I) and B(I);
  end generate G1;
  OUTPUT : Z <= X;


end Behavioral;
