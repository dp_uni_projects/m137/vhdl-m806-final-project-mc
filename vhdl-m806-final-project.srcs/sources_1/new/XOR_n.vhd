----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/01/2020 08:03:36 PM
-- Design Name: 
-- Module Name: XOR_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity XOR_n is
  generic (WIDTH : positive := 4);
  port(
    A    : in  std_logic_vector (WIDTH-1 downto 0);
    B    : in  std_logic_vector (WIDTH-1 downto 0);
    XorX : in  std_logic;               --Unconnected, reserved for future use
    R    : out std_logic_vector (WIDTH-1 downto 0)
    );
end XOR_n;

architecture structural of XOR_n is
  component MUX_2_to_1_n is
    generic (
      WIDTH : positive := 4
      );
    port(
      A0 : in  std_logic_vector (WIDTH-1 downto 0);
      A1 : in  std_logic_vector (WIDTH-1 downto 0);
      S  : in  std_logic;
      Y  : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  component XOR_A_B_n is
    generic (WIDTH : positive := 4);
    port(
      A : in  std_logic_vector (WIDTH-1 downto 0);  -- SRC A
      B : in  std_logic_vector (WIDTH-1 downto 0);  -- SRC B
      Z : out std_logic_vector (WIDTH-1 downto 0)   --Result
      );
  end component;

  signal S_Xor_in : std_logic_vector (WIDTH-1 downto 0);
begin
  MUX : MUX_2_to_1_n
    generic map(WIDTH => WIDTH)
    port map(
      -- Connect the XOR to both A and B of the MUX
      A0 => S_Xor_in,
      A1 => S_Xor_in,
      S  => XorX,
      Y  => R
      );

  XOR_UNIT : XOR_A_B_n
    generic map(WIDTH => WIDTH)
    port map(
      A => A,
      B => B,
      Z => S_Xor_in
      );
end structural;


