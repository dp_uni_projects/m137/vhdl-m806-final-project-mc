----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/15/2020 03:07:39 PM
-- Design Name: 
-- Module Name: RAM_ARRAY - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RAM_ARRAY is
  generic (
    N : positive := 10;                 --address length
    M : positive := 32);                --data word length
  port(
    CLK      : in  std_logic;
    WE       : in  std_logic;
    ADDR     : in  std_logic_vector (N-1 downto 0);
    DATA_IN  : in  std_logic_vector (M-1 downto 0);
    DATA_OUT : out std_logic_vector (M-1 downto 0)
    );
end RAM_ARRAY;

architecture Behavioral of RAM_ARRAY is
  type RAM_ARRAY is array (0 to 2**N-1)
    of std_logic_vector (M-1 downto 0);
  signal RAM : RAM_ARRAY;
begin
  BLOCK_RAM : process (CLK)
  begin
    if(CLK = '1' and CLK'event) then
      if (WE = '1') then RAM(TO_INTEGER(unsigned(ADDR))) <= DATA_IN;
      end if;
    end if;
  end process;
  DATA_OUT <= RAM(TO_INTEGER(unsigned(ADDR)));  -- This line makes the read async
end Behavioral;
