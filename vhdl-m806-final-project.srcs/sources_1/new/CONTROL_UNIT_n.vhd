----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/02/2020 08:38:49 PM
-- Design Name: 
-- Module Name: CONTROL_UNIT_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CONTROL_UNIT is
  port(
    INSTR : in std_logic_vector(31 downto 0);
    FLAGS : in std_logic_vector(3 downto 0);
    CLK   : in std_logic;
    RESET : in std_logic;

    PcSrc      : out std_logic_vector(1 downto 0);
    MemToReg   : out std_logic;
    MemWrite   : out std_logic;
    ALUControl : out std_logic_vector(3 downto 0);
    ALUSrc     : out std_logic;
    ImmSrc     : out std_logic;
    RegWrite   : out std_logic;
    RegSrc     : out std_logic_vector(2 downto 0);
    FlagsWrite : out std_logic;
    PCWrite    : out std_logic;
    IRWrite    : out std_logic;
    MAWrite    : out std_logic
    );
end CONTROL_UNIT;

architecture Structural of CONTROL_UNIT is
  signal COND  : std_logic_vector(3 downto 0);
  signal OP    : std_logic_vector(1 downto 0);
  signal FUNCT : std_logic_vector(5 downto 0);
  signal RD    : std_logic_vector(3 downto 0);
  signal SH    : std_logic_vector(1 downto 0);

  component INSTR_DEC is
    port (
      OP          : in  std_logic_vector(1 downto 0);  -- 2-bit signal
      FUNCT       : in  std_logic_vector(5 downto 0);  -- 6-bit signal
      SH          : in  std_logic_vector(1 downto 0);  -- 2-bit signal
      REG_SRC     : out std_logic_vector(2 downto 0);  -- 3-bit signal
      ALU_SRC     : out std_logic;                     -- 1-bit signal
      MEM_TO_REG  : out std_logic;                     -- 1-bit signal
      ALU_CONTROL : out std_logic_vector(3 downto 0);  -- 4-bit signal
      IMM_SRC     : out std_logic;                     -- 1-bit signal
      NO_WRITE_in : out std_logic                      -- 1-bit signal
      );
  end component;

  component COND_LOGIC is
    port(
      FLAGS      : in  std_logic_vector(3 downto 0);  --N,Z,C,V
      COND_FIELD : in  std_logic_vector(3 downto 0);
      CondEx_in  : out std_logic
      );
  end component;

  component FSM is
    port(
      CLK        : in  std_logic;
      RESET      : in  std_logic;
      NoWrite_in : in  std_logic;
      CondEx_in  : in  std_logic;
      OP         : in  std_logic_vector(1 downto 0);
      RD         : in  std_logic_vector(3 downto 0);
      SL         : in  std_logic;
      Instr_24   : in  std_logic;
      PCWrite    : out std_logic;
      IRWrite    : out std_logic;
      RegWrite   : out std_logic;
      FlagsWrite : out std_logic;
      MAWrite    : out std_logic;
      MemWrite   : out std_logic;
      PCSrc      : out std_logic_vector(1 downto 0)
      );
  end component;

  signal NoWrite_in : std_logic;
  signal CondEx_in  : std_logic;
begin
  INSTR_DEC_UNIT : INSTR_DEC
    port map(
      OP          => OP,
      FUNCT       => FUNCT,
      SH          => SH,
      REG_SRC     => RegSrc,
      ALU_SRC     => ALUSrc,
      MEM_TO_REG  => MemToReg,
      ALU_CONTROL => ALUControl,
      IMM_SRC     => ImmSrc,
      NO_WRITE_in => NoWrite_in
      );

  COND_LOGIC_UNIT : COND_LOGIC
    port map(
      COND_FIELD => COND,
      FLAGS      => FLAGS,
      CondEx_in  => CondEx_in
      );

  FSM_UNIT : FSM
    port map(
      CLK        => CLK,
      RESET      => RESET,
      NoWrite_in => NoWrite_in,
      CondEx_in  => CondEx_in,
      OP         => OP,
      RD         => RD,
      SL         => FUNCT(0),
      Instr_24   => FUNCT(4),
      PCWrite    => PCWrite,
      IRWrite    => IRWrite,
      RegWrite   => RegWrite,
      FlagsWrite => FlagsWrite,
      MAWrite    => MAWrite,
      MemWrite   => MemWrite,
      PCSrc      => PcSrc
      );

  COND  <= INSTR(31 downto 28);
  OP    <= INSTR(27 downto 26);
  FUNCT <= INSTR(25 downto 20);
  RD    <= INSTR(15 downto 12);
  SH    <= INSTR(6 downto 5);
end Structural;
