----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/01/2020 08:03:54 PM
-- Design Name: 
-- Module Name: LSL_LSR_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LSL_LSR_n is
  generic(WIDTH : positive := 4);
  port (
    A    : in  std_logic_vector (WIDTH-1 downto 0);  -- First operand
    B    : in  std_logic_vector (4 downto 0);        -- Shift amount
    RorL : in  std_logic;                            -- LSR or LSL
    R    : out std_logic_vector (WIDTH-1 downto 0)   -- Result
    );
end LSL_LSR_n;

architecture structural of LSL_LSR_n is
  component MUX_2_to_1_n is
    generic (
      WIDTH : positive := 4
      );
    port(
      A0 : in  std_logic_vector (WIDTH-1 downto 0);
      A1 : in  std_logic_vector (WIDTH-1 downto 0);
      S  : in  std_logic;
      Y  : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;
  component LSL_A_B_n is
    generic (WIDTH : positive := 4);
    port(
      A : in  std_logic_vector (WIDTH-1 downto 0);
      B : in  std_logic_vector (4 downto 0);
      Z : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  component LSR_A_B_n is
    generic (WIDTH : positive := 4);
    port(
      A : in  std_logic_vector (WIDTH-1 downto 0);
      B : in  std_logic_vector (4 downto 0);
      Z : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  -- Internal signals
  signal S_Lsr_in : std_logic_vector (WIDTH-1 downto 0);  -- LSR result
  signal S_Lsl_in : std_logic_vector (WIDTH-1 downto 0);  -- LSL result


begin
  MUX : MUX_2_to_1_n
    generic map(WIDTH => WIDTH)
    port map(
      A0 => S_Lsl_in,
      A1 => S_Lsr_in,
      S  => RorL,
      Y  => R
      );
  LSR_UNIT : LSR_A_B_n
    generic map(WIDTH => WIDTH)
    port map(
      A => A,
      B => B,
      Z => S_Lsr_in
      );

  LSL_UNIT : LSL_A_B_n
    generic map(WIDTH => WIDTH)
    port map(
      A => A,
      B => B,
      Z => S_Lsl_in
      );

end structural;
