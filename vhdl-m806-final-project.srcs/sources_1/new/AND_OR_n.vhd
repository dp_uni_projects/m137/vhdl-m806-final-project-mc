----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/30/2020 01:14:25 PM
-- Design Name: 
-- Module Name: AND_OR_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AND_OR_n is
  generic(WIDTH : positive := 4);
  port (
    A    : in  std_logic_vector (WIDTH-1 downto 0);  -- First operand
    B    : in  std_logic_vector (WIDTH-1 downto 0);  -- Second operand
    OorA : in  std_logic;                            -- OR or AND
    R    : out std_logic_vector (WIDTH-1 downto 0)   -- Result
    );
end AND_OR_n;

architecture structural of AND_OR_n is

  component AND_A_B_n is
    generic (WIDTH : positive := 4);
    port(
      A : in  std_logic_vector (WIDTH-1 downto 0);
      B : in  std_logic_vector (WIDTH-1 downto 0);
      Z : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  component MUX_2_to_1_n is
    generic (
      WIDTH : positive := 4
      );
    port(
      A0 : in  std_logic_vector (WIDTH-1 downto 0);
      A1 : in  std_logic_vector (WIDTH-1 downto 0);
      S  : in  std_logic;
      Y  : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  component OR_A_B_n is
    generic (WIDTH : positive := 4);
    port(
      A : in  std_logic_vector (WIDTH-1 downto 0);
      B : in  std_logic_vector (WIDTH-1 downto 0);
      Z : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;


  -- Internal signals
  signal S_Or_in  : std_logic_vector (WIDTH-1 downto 0);  -- OR result
  signal S_And_in : std_logic_vector (WIDTH-1 downto 0);  -- AND result

begin

  MUX : MUX_2_to_1_n
    generic map(WIDTH => WIDTH)
    port map(
      A0 => S_And_in,
      A1 => S_Or_in,
      S  => OorA,
      Y  => R
      );

  AND_UNIT : AND_A_B_n
    generic map(WIDTH => WIDTH)
    port map(
      A => A,
      B => B,
      Z => S_And_in
      );

  OR_UNIT : OR_A_B_n
    generic map(WIDTH => WIDTH)
    port map(
      A => A,
      B => B,
      Z => S_Or_in
      );
end structural;
