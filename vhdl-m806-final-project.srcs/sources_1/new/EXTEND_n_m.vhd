----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/18/2020 07:49:55 PM
-- Design Name: 
-- Module Name: EXTEND_n_m - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Simple extender for UNSIGNED inputs, i.e. always puts zeros to the front
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EXTEND_n_m is
  generic (
    WIDTH_IN  : positive := 4;
    WIDTH_OUT : positive := 8
    );
  port(
    SZ_in  : in  std_logic_vector (WIDTH_IN-1 downto 0);  --Signed/Zero in
    SZ_out : out std_logic_vector (WIDTH_OUT-1 downto 0);
    SorZ   : in  std_logic              --Select Signed or Zero extension
    );
end EXTEND_n_m;

architecture Behavioral of EXTEND_n_m is

begin
  EXTEND : process (SZ_in, SorZ)
    variable SZ_in_u  : unsigned (WIDTH_IN-1 downto 0);
    variable SZ_in_s  : signed (WIDTH_IN-1 downto 0);
    variable SZ_out_u : unsigned (WIDTH_OUT-1 downto 0);
    variable SZ_out_s : signed (WIDTH_OUT-1 downto 0);
  begin
    -- Conversion to a signed and unsigned variable
    SZ_in_u := unsigned (SZ_in);
    SZ_in_s := signed (SZ_in);
    if (SorZ = '1') then
      SZ_out_s := RESIZE(SZ_in_s, WIDTH_OUT);
      SZ_out   <= std_logic_vector(SZ_out_s);
    else
      SZ_out_u := RESIZE(SZ_in_u, WIDTH_OUT);
      SZ_out   <= std_logic_vector(SZ_out_u);
    end if;
  end process;

end Behavioral;
