----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/23/2020 09:19:20 PM
-- Design Name: 
-- Module Name: RF_REGrwe - structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RF_REGrwe is
  port (
    CLK       : in  std_logic;
    WE        : in  std_logic;
    REG_WRITE : in  std_logic;
    ADDR_W    : in  std_logic_vector(4-1 downto 0);
    ADDR_R1   : in  std_logic_vector(4-1 downto 0);
    ADDR_R2   : in  std_logic_vector(4-1 downto 0);
    DATA_IN   : in  std_logic_vector(32-1 downto 0);
    R15       : in  std_logic_vector(32-1 downto 0);  -- Extra register,
                                                      -- coming from PC
    RESET     : in  std_logic;
    DATA_OUT1 : out std_logic_vector(32-1 downto 0);
    DATA_OUT2 : out std_logic_vector(32-1 downto 0)
    );
end RF_REGrwe;

architecture structural of RF_REGrwe is
  component REGrwe_n is
    generic (WIDTH : positive := 32);   -- Default value of 4
    port (
      CLK   : in  std_logic;
      RESET : in  std_logic;
      WE    : in  std_logic;
      Din   : in  std_logic_vector (WIDTH-1 downto 0);
      Dout  : out std_logic_vector (WIDTH-1 downto 0));
  end component;

  component REGISTER_FILE_wR15 is
    generic (
      N : positive := 4;                --Register addressing width
      M : positive := 32                --Data width
      );
    port (
      CLK       : in  std_logic;
      WE        : in  std_logic;
      ADDR_W    : in  std_logic_vector(N-1 downto 0);
      ADDR_R1   : in  std_logic_vector(N-1 downto 0);
      ADDR_R2   : in  std_logic_vector(N-1 downto 0);
      DATA_IN   : in  std_logic_vector(M-1 downto 0);
      R15       : in  std_logic_vector(M-1 downto 0);  -- Extra register,
                                                       -- coming from PC
      DATA_OUT1 : out std_logic_vector(M-1 downto 0);
      DATA_OUT2 : out std_logic_vector(M-1 downto 0)
      );
  end component;

  -- in
  signal ADDR_W_in  : std_logic_vector(4-1 downto 0)  := (others => 'X');
  signal ADDR_R1_in : std_logic_vector(4-1 downto 0)  := (others => 'X');
  signal ADDR_R2_in : std_logic_vector(4-1 downto 0)  := (others => 'X');
  signal DATA_IN_in : std_logic_vector(32-1 downto 0) := (others => 'X');
  signal R15_in     : std_logic_vector(32-1 downto 0) := (others => 'X');

  signal REG_WRITE_in : std_logic := '0';  --to synch RF's WE with the CLK

  -- out
  signal DATA_OUT1_in : std_logic_vector(32-1 downto 0);
  signal DATA_OUT2_in : std_logic_vector(32-1 downto 0);
begin
  rf : REGISTER_FILE_wR15
    generic map(
      N => 4,
      M => 32
      )
    port map(
      CLK       => CLK,
      WE        => REG_WRITE_in,
      ADDR_W    => ADDR_W_in,
      ADDR_R1   => ADDR_R1_in,
      ADDR_R2   => ADDR_R2_in,
      DATA_IN   => DATA_IN_in,
      R15       => R15_in,
      DATA_OUT1 => DATA_OUT1_in,
      DATA_OUT2 => DATA_OUT2_in
      );

  reg_addr_w : REGrwe_n
    generic map(WIDTH => 4)
    port map(
      CLK   => CLK,
      WE    => WE,
      RESET => RESET,
      Din   => ADDR_W,
      Dout  => ADDR_W_in
      );
  reg_addr_r1 : REGrwe_n
    generic map(WIDTH => 4)
    port map(
      CLK   => CLK,
      WE    => WE,
      RESET => RESET,
      Din   => ADDR_R1,
      Dout  => ADDR_R1_in
      );
  reg_addr_r2 : REGrwe_n
    generic map(WIDTH => 4)
    port map(
      CLK   => CLK,
      WE    => WE,
      RESET => RESET,
      Din   => ADDR_R2,
      Dout  => ADDR_R2_in
      );
  reg_data_in : REGrwe_n
    generic map(WIDTH => 32)
    port map(
      CLK   => CLK,
      WE    => WE,
      RESET => RESET,
      Din   => DATA_IN,
      Dout  => DATA_IN_in
      );
  reg_r15 : REGrwe_n
    generic map(WIDTH => 32)
    port map(
      CLK   => CLK,
      WE    => WE,
      RESET => RESET,
      Din   => R15,
      Dout  => R15_in
      );

  reg_data_out1 : REGrwe_n
    generic map(WIDTH => 32)
    port map(
      CLK   => CLK,
      WE    => WE,
      RESET => RESET,
      Din   => DATA_OUT1_in,
      Dout  => DATA_OUT1
      );

  reg_data_out2 : REGrwe_n
    generic map(WIDTH => 32)
    port map(
      CLK   => CLK,
      WE    => WE,
      RESET => RESET,
      Din   => DATA_OUT2_in,
      Dout  => DATA_OUT2
      );

  REG_WE : process(CLK)
  begin
    if (CLK = '1' and CLK'event) then
      if (RESET = '1') then REG_WRITE_in <= '0';
      elsif (WE = '1') then REG_WRITE_in <= REG_WRITE;
      end if;
    end if;
  end process;



end structural;
