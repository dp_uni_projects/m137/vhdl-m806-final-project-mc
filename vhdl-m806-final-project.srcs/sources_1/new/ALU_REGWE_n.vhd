----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/15/2020 09:29:05 PM
-- Design Name: 
-- Module Name: ALU_REGWE_32 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALU_REGWE_32 is
  -- generic (WIDTH : positive := 32);
  port(
    SRC_A      : in  std_logic_vector (32-1 downto 0);  -- Source A data
    SRC_B      : in  std_logic_vector (32-1 downto 0);  -- Source B data
    ALUControl : in  std_logic_vector (3 downto 0);  -- Control signal from CU
    SHAMT5     : in  std_logic_vector (4 downto 0);
    Flags      : out std_logic_vector (3 downto 0);  -- 4 status flags
    ALUResult  : out std_logic_vector (32-1 downto 0);  -- Final result
    CLK        : in  std_logic;
    RESET      : in  std_logic;
    WE         : in  std_logic
    );

end ALU_REGWE_32;

architecture structural of ALU_REGWE_32 is
  component ALU_n is
    generic (WIDTH : positive := 32);
    port(
      SRC_A      : in  std_logic_vector (WIDTH-1 downto 0);  -- Source A data
      SRC_B      : in  std_logic_vector (WIDTH-1 downto 0);  -- Source B data
      ALUControl : in  std_logic_vector (3 downto 0);  -- Control signal from CU
      SHAMT5     : in  std_logic_vector (4 downto 0);
      Flags      : out std_logic_vector (3 downto 0);  -- 4 status flags
      ALUResult  : out std_logic_vector (WIDTH-1 downto 0)   -- Final result
      );
  end component;

  component REGrwe_n is
    generic (WIDTH : positive := 32);   -- Default value of 4
    port (
      CLK   : in  std_logic;
      RESET : in  std_logic;
      WE    : in  std_logic;
      Din   : in  std_logic_vector (WIDTH-1 downto 0);
      Dout  : out std_logic_vector (WIDTH-1 downto 0));
  end component;

  signal SRC_A_in      : std_logic_vector(32-1 downto 0);
  signal SRC_B_in      : std_logic_vector(32-1 downto 0);
  signal ALUResult_in  : std_logic_vector(32-1 downto 0);
  signal SHAMT5_in     : std_logic_vector(4 downto 0);
  signal Flags_in      : std_logic_vector(3 downto 0);
  signal ALUControl_in : std_logic_vector(3 downto 0);
begin
  alu_in : ALU_n
    generic map(WIDTH => 32)
    port map(
      SRC_B      => SRC_B_in,
      SRC_A      => SRC_A_in,
      ALUResult  => ALUResult_in,
      SHAMT5     => SHAMT5_in,
      ALUControl => ALUControl_in,
      Flags      => Flags_in
      );

  reg_alu_control : REGrwe_n
    generic map(WIDTH => 4)
    port map(
      RESET => RESET,
      WE    => WE,
      CLK   => CLK,
      Din   => ALUControl,
      Dout  => ALUControl_in
      );

  reg_a : REGrwe_n
    generic map(WIDTH => 32)
    port map(
      RESET => RESET,
      WE    => WE,
      CLK   => CLK,
      Din   => SRC_A,
      Dout  => SRC_A_in
      );
  reg_b : REGrwe_n
    generic map(WIDTH => 32)
    port map(
      RESET => RESET,
      WE    => WE,
      CLK   => CLK,
      Din   => SRC_B,
      Dout  => SRC_B_in
      );
  reg_result : REGrwe_n
    generic map(WIDTH => 32)
    port map(
      RESET => RESET,
      WE    => '1',
      CLK   => CLK,
      Din   => ALUResult_in,
      Dout  => ALUResult
      );

  reg_shamt : REGrwe_n
    generic map(WIDTH => 5)
    port map(
      RESET => RESET,
      WE    => WE,
      CLK   => CLK,
      Din   => SHAMT5,
      Dout  => SHAMT5_in
      );

  reg_flags : REGrwe_n
    generic map(WIDTH => 4)
    port map(
      RESET => RESET,
      WE    => '1',
      CLK   => CLK,
      Din   => Flags_in,
      Dout  => Flags
      );


end structural;
