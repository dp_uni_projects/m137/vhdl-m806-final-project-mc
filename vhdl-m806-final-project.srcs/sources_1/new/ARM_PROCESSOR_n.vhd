----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/12/2020 08:34:45 PM
-- Design Name: 
-- Module Name: ARM_PROCESSOR_n - structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ARM_PROCESSOR_n is
  port(
    CLK   : in std_logic;
    RESET : in std_logic;

    ALUControl_out : out std_logic_vector(3 downto 0);
    Instr_out      : out std_logic_vector(31 downto 0);
    ALUResult_out  : out std_logic_vector(31 downto 0);
    Result_out     : out std_logic_vector(31 downto 0);
    WriteData_out  : out std_logic_vector(31 downto 0)
    );
end ARM_PROCESSOR_n;

architecture structural of ARM_PROCESSOR_n is
  component CONTROL_UNIT is
    port(
      CLK   : in std_logic;
      RESET : in std_logic;
      INSTR : in std_logic_vector(31 downto 0);
      FLAGS : in std_logic_vector(3 downto 0);

      PcSrc      : out std_logic_vector(1 downto 0);
      MemToReg   : out std_logic;
      MemWrite   : out std_logic;
      ALUControl : out std_logic_vector(3 downto 0);
      ALUSrc     : out std_logic;
      ImmSrc     : out std_logic;
      RegWrite   : out std_logic;
      RegSrc     : out std_logic_vector(2 downto 0);
      FlagsWrite : out std_logic;
      PCWrite    : out std_logic;
      IRWrite    : out std_logic;
      MAWrite    : out std_logic
      );
  end component;

  component DATAPATH_MC_n is
    generic (WIDTH : positive := 32);
    port (
      CLK           : in  std_logic;
      RESET         : in  std_logic;
      PcSrc         : in  std_logic_vector(1 downto 0);
      MemToReg      : in  std_logic;
      MemWrite      : in  std_logic;
      ALUControl    : in  std_logic_vector(3 downto 0);
      ALUSrc        : in  std_logic;
      ImmSrc        : in  std_logic;
      RegWrite      : in  std_logic;
      RegSrc        : in  std_logic_vector(2 downto 0);
      FlagsWrite    : in  std_logic;
      Flags_out     : out std_logic_vector(3 downto 0);
      Instr_out     : out std_logic_vector(WIDTH-1 downto 0);
      PC_out        : out std_logic_vector(WIDTH-1 downto 0);
      ALUResult_out : out std_logic_vector(WIDTH-1 downto 0);
      Result_out    : out std_logic_vector(WIDTH-1 downto 0);
      WriteData_out : out std_logic_vector(WIDTH-1 downto 0);

      -- For multi-cycle implementation
      PCWrite : in std_logic;
      IRWrite : in std_logic;
      MAWrite : in std_logic
      );
  end component;

  signal PC_DP_out        : std_logic_vector(31 downto 0);
  signal ALUResult_DP_out : std_logic_vector(31 downto 0);
  signal Result_DP_out    : std_logic_vector(31 downto 0);
  signal WriteData_DP_out : std_logic_vector(31 downto 0);

  signal ALUControl_in : std_logic_vector (3 downto 0);  --4 bits IIRC
  signal RegSrc_in     : std_logic_vector (2 downto 0);  --2 bits for Single cycle
  signal ImmSrc_in     : std_logic;
  signal ALUSrc_in     : std_logic;
  signal PcSrc_in      : std_logic_vector(1 downto 0);
  signal MemtoReg_in   : std_logic;
  signal RegWrite_in   : std_logic;
  signal FlagsWrite_in : std_logic;
  signal Flags_in      : std_logic_vector(3 downto 0);
  signal MemWrite_in   : std_logic;
  signal Instr_in      : std_logic_vector(31 downto 0);
  signal PCWrite_in    : std_logic;
  signal IRWrite_in    : std_logic;
  signal MAWrite_in    : std_logic;

begin

  CONTROL : CONTROL_UNIT
    -- generic map (WIDTH => WIDTH)
    port map(
      INSTR      => Instr_in,
      FLAGS      => Flags_in,
      PcSrc      => PcSrc_in,
      MemToReg   => MemToReg_in,
      MemWrite   => MemWrite_in,
      ALUControl => ALUControl_in,
      ALUSrc     => ALUSrc_in,
      ImmSrc     => ImmSrc_in,
      RegWrite   => RegWrite_in,
      RegSrc     => RegSrc_in,
      FlagsWrite => FlagsWrite_in,
      PCWrite    => PCWrite_in,
      IRWrite    => IRWrite_in,
      MAWrite    => MAWrite_in,
      CLK        => CLK,
      RESET      => RESET
      );

  DATAPATH : DATAPATH_MC_n
    generic map(WIDTH => 32)
    port map(
      CLK           => CLK,
      RESET         => RESET,
      PcSrc         => PcSrc_in,
      MemToReg      => MemToReg_in,
      MemWrite      => MemWrite_in,
      ALUControl    => ALUControl_in,
      ALUSrc        => ALUSrc_in,
      ImmSrc        => ImmSrc_in,
      RegWrite      => RegWrite_in,
      RegSrc        => RegSrc_in,
      FlagsWrite    => FlagsWrite_in,
      Flags_out     => Flags_in,
      Instr_out     => Instr_in,
      ALUResult_out => ALUResult_DP_out,
      PC_out        => PC_DP_out,
      Result_out    => Result_DP_out,
      WriteData_out => WriteData_DP_out,
      PCWrite       => PCWrite_in,
      IRWrite       => IRWrite_in,
      MAWrite       => MAWrite_in
      );
  Instr_out      <= Instr_in;
  ALUControl_out <= ALUControl_in;
  ALUResult_out  <= ALUResult_DP_out;
  Result_out     <= Result_DP_out;
  WriteData_out  <= WriteData_DP_out;
end structural;
