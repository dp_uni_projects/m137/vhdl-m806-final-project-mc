----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/29/2020 08:26:57 PM
-- Design Name: 
-- Module Name: ADD_SUB_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ADD_SUB_n is
  generic(WIDTH : positive := 4);
  port (
    A    : in  std_logic_vector (WIDTH-1 downto 0);  -- First operand
    B    : in  std_logic_vector (WIDTH-1 downto 0);  -- Second operand
    SorA : in  std_logic;                            -- Subtraction or addition
    R    : out std_logic_vector (WIDTH-1 downto 0);  -- Result
    Cout : out std_logic;                            -- Carry
    OV   : out std_logic                             -- Overflow
    );
end ADD_SUB_n;

-- Signed Adder
architecture structural of ADD_SUB_n is
  component ADDER_n is
    generic (WIDTH : positive := 4);
    port (
      A    : in  std_logic_vector (WIDTH-1 downto 0);
      B    : in  std_logic_vector (WIDTH-1 downto 0);
      S    : out std_logic_vector (WIDTH-1 downto 0);
      Cout : out std_logic;
      OV   : out std_logic);
  end component;

  component SUBTRACTOR_n is
    generic (WIDTH : positive := 4);
    port (
      A    : in  std_logic_vector (WIDTH-1 downto 0);
      B    : in  std_logic_vector (WIDTH-1 downto 0);
      S    : out std_logic_vector (WIDTH-1 downto 0);
      Cout : out std_logic;
      OV   : out std_logic);
  end component;

  component MUX_2_to_1_n is
    generic (
      WIDTH : positive := 4
      );
    port(
      A0 : in  std_logic_vector (WIDTH-1 downto 0);
      A1 : in  std_logic_vector (WIDTH-1 downto 0);
      S  : in  std_logic;
      Y  : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  -- Internal signals
  signal S_Add_in : std_logic_vector (WIDTH-1 downto 0);  -- Adder result
  signal S_Sub_in : std_logic_vector (WIDTH-1 downto 0);  -- Subtractor result
  signal C_Add_in : std_logic;          -- carry from adder
  signal C_Sub_in : std_logic;          -- carry from subtractor
  signal V_Add_in : std_logic;          -- overfow from adder
  signal V_Sub_in : std_logic;          -- overflow from subtractor

begin

  ADD_UNIT : ADDER_n
    generic map (WIDTH => WIDTH)
    port map(
      A    => A,
      B    => B,
      S    => S_Add_in,
      OV   => V_Add_in,
      Cout => C_Add_in
      );

  SUB_UNIT : SUBTRACTOR_n
    generic map (WIDTH => WIDTH)
    port map(
      A    => A,
      B    => B,
      S    => S_Sub_in,
      OV   => V_Sub_in,
      Cout => C_Sub_in
      );

  MUX_S : MUX_2_to_1_n
    generic map(WIDTH => WIDTH)
    port map(
      A0 => S_Add_in,
      A1 => S_Sub_in,
      S  => SorA,
      Y  => R
      );

  MUX_C : MUX_2_to_1_n
    generic map(WIDTH => 1)
    port map(
      A0(0) => C_Add_in,
      A1(0) => C_Sub_in,
      S     => SorA,
      Y(0)  => Cout
      );
  MUX_V : MUX_2_to_1_n
    generic map(WIDTH => 1)
    port map(
      A0(0) => V_Add_in,
      A1(0) => V_Sub_in,
      S     => SorA,
      Y(0)  => OV
      );
end structural;
