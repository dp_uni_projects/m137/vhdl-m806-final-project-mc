----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/01/2020 08:03:34 PM
-- Design Name: 
-- Module Name: MUX_3_to_1_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUX_3_to_1_n is
  generic(WIDTH : positive := 4);
  port(
    S  : in  std_logic_vector(1 downto 0);
    A0 : in  std_logic_vector (WIDTH-1 downto 0);
    A1 : in  std_logic_vector (WIDTH-1 downto 0);
    A2 : in  std_logic_vector (WIDTH-1 downto 0);
    Y  : out std_logic_vector (WIDTH-1 downto 0)
    );
end MUX_3_to_1_n;

architecture Behavioral of MUX_3_to_1_n is

begin
  MUX_PROCESSES : process (S, A0, A1, A2)
  begin
    case S is
      when "00"=>
        Y <= A0;
      when "10"=>
        Y <= A1;
      when "11"=>
        Y <= A2;
      when others =>
        Y <= (others => 'X');
    end case;
  end process;

end Behavioral;
