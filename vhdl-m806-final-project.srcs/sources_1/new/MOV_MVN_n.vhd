----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/01/2020 08:03:01 PM
-- Design Name: 
-- Module Name: MOV_MVN_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MOV_MVN_n is
  generic(WIDTH : positive := 4);
  port (
    -- A    : in  std_logic_vector (WIDTH-1 downto 0);  -- First operand
    B    : in  std_logic_vector (WIDTH-1 downto 0);  -- Second operand
    NorM : in  std_logic;                            -- OR or AND
    R    : out std_logic_vector (WIDTH-1 downto 0)   -- Result
    );
end MOV_MVN_n;

architecture Structural of MOV_MVN_n is
  component MUX_2_to_1_n is
    generic (
      WIDTH : positive := 4
      );
    port(
      A0 : in  std_logic_vector (WIDTH-1 downto 0);
      A1 : in  std_logic_vector (WIDTH-1 downto 0);
      S  : in  std_logic;
      Y  : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;
  component MVN_n is
    generic (WIDTH : positive := 4);
    port(
      A : in std_logic_vector (WIDTH-1 downto 0);

      Z : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  signal S_Mvn_in : std_logic_vector (WIDTH-1 downto 0);  -- MVN result
begin
  MUX : MUX_2_to_1_n
    generic map(WIDTH => WIDTH)
    port map(
      A0 => B,                          --Leave unchanged for MOV
      A1 => S_Mvn_in,
      S  => NorM,
      Y  => R
      );

  NOT_UNIT : MVN_n
    generic map(WIDTH => WIDTH)
    port map(
      A => B,
      Z => S_Mvn_in
      );

end Structural;
