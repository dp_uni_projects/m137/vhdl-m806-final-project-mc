----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/17/2020 10:20:01 PM
-- Design Name: 
-- Module Name: AND_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AND_n is
  generic (WIDTH : positive := 4);
  port(
    A : in  std_logic_vector (WIDTH-1 downto 0);
    Z : out std_logic
    );
end AND_n;

architecture Behavioral of AND_n is
  signal X : std_logic_vector (WIDTH downto 0);
begin
  -- No idea why this works??? copy-paste from notes, NOR example
  INITIALIZATION : X(0) <= '1';
  G1             : for I in 0 to WIDTH-1 generate
    X(I+1) <= A(I) and X(I);
  end generate G1;
  OUTPUT : Z <= X(WIDTH);
end Behavioral;
