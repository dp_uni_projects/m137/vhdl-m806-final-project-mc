----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/01/2020 09:43:22 PM
-- Design Name: 
-- Module Name: LSL_A_B_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LSL_A_B_n is
  generic (WIDTH : positive := 4);
  port(
    A : in  std_logic_vector (WIDTH-1 downto 0);
    B : in  std_logic_vector (4 downto 0);
    Z : out std_logic_vector (WIDTH-1 downto 0)
    );
end LSL_A_B_n;

architecture Behavioral of LSL_A_B_n is
begin
  LLSHIFTER : process (A, B)
    variable A_n : unsigned (WIDTH-1 downto 0);
    variable B_n : natural range 0 to 31;  --Shift amount
  begin
    B_n := to_integer(unsigned(B));        --Convert B to number
    A_n := unsigned(A);
    Z   <= std_logic_vector(SHIFT_LEFT(A_n, B_n));
  end process LLSHIFTER;


end Behavioral;
