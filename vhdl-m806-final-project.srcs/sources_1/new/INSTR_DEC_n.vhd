----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/02/2020 08:39:19 PM
-- Design Name: 
-- Module Name: INSTR_DEC_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity INSTR_DEC is
  generic (WIDTH : positive := 32);
  port (
    OP          : in  std_logic_vector(1 downto 0);  -- 2-bit signal
    FUNCT       : in  std_logic_vector(5 downto 0);  -- 6-bit signal
    SH          : in  std_logic_vector(1 downto 0);  -- 2-bit signal
    REG_SRC     : out std_logic_vector(2 downto 0);  -- 3-bit signal
    ALU_SRC     : out std_logic;                     -- 1-bit signal
    MEM_TO_REG  : out std_logic;                     -- 1-bit signal
    ALU_CONTROL : out std_logic_vector(3 downto 0);  -- 4-bit signal
    IMM_SRC     : out std_logic;                     -- 1-bit signal
    NO_WRITE_in : out std_logic                      -- 1-bit signal
    );
end INSTR_DEC;

architecture Behavioral of INSTR_DEC is

begin
  DECODER : process (OP, FUNCT, SH)
  begin
    case OP is
      -- Instruction type
      when "00" =>                      -- Data processing instruction
        case FUNCT is

          when "101000"|"101001" =>     --ADD IMMEDIATE
            REG_SRC     <= "0X0";       --CHECK
            ALU_SRC     <= '1';
            IMM_SRC     <= '0';
            ALU_CONTROL <= "0000";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "001000"|"001001" =>     --ADD REGISTER
            REG_SRC     <= "000";       --CHECK
            ALU_SRC     <= '0';
            IMM_SRC     <= 'X';
            ALU_CONTROL <= "0000";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "100101"|"100100" =>     --SUB IMMEDIATE
            REG_SRC     <= "0X0";       --CHECK
            ALU_SRC     <= '1';
            IMM_SRC     <= '0';
            ALU_CONTROL <= "0001";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "000101"|"000100" =>     --SUB REGISTER
            REG_SRC     <= "000";       --CHECK
            ALU_SRC     <= '0';
            IMM_SRC     <= 'X';
            ALU_CONTROL <= "0001";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "110101" =>              --CMP IMMEDIATE
            REG_SRC     <= "0X0";       --CHECK
            ALU_SRC     <= '1';
            IMM_SRC     <= '0';
            ALU_CONTROL <= "0001";
            MEM_TO_REG  <= 'X';
            NO_WRITE_IN <= '1';

          when "010101" =>              --CMP REGISTER
            REG_SRC     <= "000";       --CHECK
            ALU_SRC     <= '0';
            IMM_SRC     <= 'X';
            ALU_CONTROL <= "0001";
            MEM_TO_REG  <= 'X';
            NO_WRITE_IN <= '1';

          when "100001"|"100000" =>     --AND IMMEDIATE
            REG_SRC     <= "0X0";       --CHECK
            ALU_SRC     <= '1';
            IMM_SRC     <= '0';
            ALU_CONTROL <= "0010";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "000000"|"000001" =>     --AND REGISTER
            REG_SRC     <= "000";       --CHECK
            ALU_SRC     <= '0';
            IMM_SRC     <= 'X';
            ALU_CONTROL <= "0010";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "111000"|"111001" =>     --ORR IMMEDIATE
            REG_SRC     <= "0X0";       --CHECK
            ALU_SRC     <= '1';
            IMM_SRC     <= '0';
            ALU_CONTROL <= "0011";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "011001"|"011000" =>     --ORR REGISTER
            REG_SRC     <= "000";       --CHECK
            ALU_SRC     <= '0';
            IMM_SRC     <= 'X';
            ALU_CONTROL <= "0011";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "100011"|"100010" =>     --XOR IMMEDIATE
            REG_SRC     <= "0X0";       --
            ALU_SRC     <= '1';
            IMM_SRC     <= '0';
            ALU_CONTROL <= "011X";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "000011"|"000010" =>     --XOR REGISTER
            REG_SRC     <= "000";       --CHECK
            ALU_SRC     <= '0';
            IMM_SRC     <= 'X';
            ALU_CONTROL <= "011X";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "111110" =>              --MVN IMMEDIATE
            REG_SRC     <= "0XX";       --CHECK
            ALU_SRC     <= '1';
            IMM_SRC     <= '0';
            ALU_CONTROL <= "0101";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "111111" =>              --MVN REGISTER
            REG_SRC     <= "00X";       --CHECK
            ALU_SRC     <= '0';
            IMM_SRC     <= 'X';
            ALU_CONTROL <= "0101";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "111010" =>              --MOV IMMEDIATE
            REG_SRC     <= "0XX";       --CHECK
            ALU_SRC     <= '1';
            IMM_SRC     <= '0';
            ALU_CONTROL <= "0100";
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';

          when "011010" =>              --MOV REGISTER, LSL, LSR, ASL, ROR
            -- MOV will be virtually useless, as it implements
            -- as an LSL with shamt5 = 0
            REG_SRC     <= "00X";
            ALU_SRC     <= '0';
            IMM_SRC     <= 'X';
            ALU_CONTROL <= "1X" & SH;   -- See commented case below
            MEM_TO_REG  <= '0';
            NO_WRITE_IN <= '0';
            -- case SH is
            --   when "00" =>              --LSL
            --     ALU_CONTROL <= "1X00";
            --   when "01" =>              --LSR
            --     ALU_CONTROL <= "1X01";
            --   when "10" =>              --ASL
            --     ALU_CONTROL <= "1X10";
            --   when "11" =>              --ROR
            --     ALU_CONTROL <= "1X11";
            -- end case;

          when others =>                --??
            REG_SRC     <= "XXX";       --CHECK
            ALU_SRC     <= 'X';
            IMM_SRC     <= 'X';
            ALU_CONTROL <= "XXXX";
            MEM_TO_REG  <= 'X';
            NO_WRITE_IN <= 'X';
        end case;  -- FUNCT
      when "01" =>                      -- Memory instruction
        NO_WRITE_IN <= '0';
        ALU_SRC     <= '1';
        IMM_SRC     <= '0';
        -- U is 1 or 0, indicating +imm12 or -imm12
        case FUNCT(3) is
          when '1' =>                   -- U = 1, ADD
            ALU_CONTROL <= "0000";
          when '0' =>                   -- U = 0, SUB
            ALU_CONTROL <= "0001";
          when others =>
            ALU_CONTROL <= "XXXX";
        end case;


        -- Instruction-specific signals
        case FUNCT is
          when "011001"|"010001" =>     --LDR
            REG_SRC    <= "0X0";
            MEM_TO_REG <= '1';
          when "011000"|"010000" =>     --STR
            REG_SRC    <= "010";
            MEM_TO_REG <= 'X';
          when others =>
            REG_SRC    <= "XXX";
            MEM_TO_REG <= 'X';
        end case;
      when "10" =>                      -- Branch instruction
        IMM_SRC     <= '1';
        ALU_SRC     <= '1';
        ALU_CONTROL <= "0000";
        MEM_TO_REG  <= '0';
        NO_WRITE_IN <= '0';
        case FUNCT(5 downto 4) is
          when "10" =>                  -- B
            REG_SRC <= "0X1";
          when "11" =>                  -- BL
            REG_SRC <= "1X1";
          when others =>
            REG_SRC <= "XXX";           --CHECK
        end case;
      when others =>
        REG_SRC     <= "XXX";           --CHECK
        ALU_SRC     <= 'X';
        IMM_SRC     <= 'X';
        ALU_CONTROL <= "XXXX";
        MEM_TO_REG  <= 'X';
        NO_WRITE_IN <= 'X';
    end case;
  end process DECODER;

end Behavioral;
