----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/29/2020 06:45:49 PM
-- Design Name: 
-- Module Name: ALU_32_SC - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALU_n is
  generic (WIDTH : positive := 32);
  port(
    -- in
    SRC_A      : in std_logic_vector (WIDTH-1 downto 0);  -- Source A data
    SRC_B      : in std_logic_vector (WIDTH-1 downto 0);  -- Source B data
    ALUControl : in std_logic_vector (3 downto 0);  -- Control signal from CU
    SHAMT5     : in std_logic_vector (4 downto 0);

    -- out
    Flags     : out std_logic_vector (3 downto 0);       -- 4 status flags
    ALUResult : out std_logic_vector (WIDTH-1 downto 0)  -- Final result
    );
end ALU_n;

architecture structural of ALU_n is
  component MUX_2_to_1_n is
    generic (
      WIDTH : positive := 4
      );
    port(
      A0 : in  std_logic_vector (WIDTH-1 downto 0);
      A1 : in  std_logic_vector (WIDTH-1 downto 0);
      S  : in  std_logic;
      Y  : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  component NOR_n is
    generic (WIDTH : positive := 4);
    port(
      A : in  std_logic_vector (WIDTH-1 downto 0);
      Z : out std_logic
      );
  end component;

  component ADD_SUB_n is
    generic(WIDTH : positive := 4);
    port (
      A    : in std_logic_vector (WIDTH -1 downto 0);  -- First operand
      B    : in std_logic_vector (WIDTH -1 downto 0);  -- Second operand
      SorA : in std_logic;              -- Subtraction or addition

      R    : out std_logic_vector (WIDTH -1 downto 0);  -- Result
      Cout : out std_logic;                             -- Carry
      OV   : out std_logic                              -- Overflow
      );
  end component;

  component AND_OR_n is
    generic(WIDTH : positive := 4);
    port (
      A    : in  std_logic_vector (WIDTH-1 downto 0);  -- First operand
      B    : in  std_logic_vector (WIDTH-1 downto 0);  -- Second operand
      OorA : in  std_logic;                            -- OR or AND
      R    : out std_logic_vector (WIDTH-1 downto 0)   -- Result
      );
  end component;

  component LSL_LSR_n is
    generic(WIDTH : positive := 4);
    port (
      A    : in  std_logic_vector (WIDTH-1 downto 0);  -- First operand
      B    : in  std_logic_vector (4 downto 0);        -- Shift amount
      RorL : in  std_logic;                            -- LSR or LSL
      R    : out std_logic_vector (WIDTH-1 downto 0)   -- Result
      );
  end component;

  component ASR_ROR_n is
    generic(WIDTH : positive := 32);
    port (
      A    : in  std_logic_vector (WIDTH-1 downto 0);  -- First operand
      B    : in  std_logic_vector (4 downto 0);        -- SHift amount
      RorA : in  std_logic;                            -- ROR or ASR
      R    : out std_logic_vector (WIDTH-1 downto 0)   -- Result
      );
  end component;

  component XOR_n is
    generic(WIDTH : positive := 4);
    port (
      A    : in  std_logic_vector (WIDTH-1 downto 0);  -- First operand
      B    : in  std_logic_vector (WIDTH-1 downto 0);  -- Second operand
      XorX : in  std_logic;                            -- OR or AND
      R    : out std_logic_vector (WIDTH-1 downto 0)   -- Result
      );
  end component;

  component MOV_MVN_n is
    generic(WIDTH : positive := 4);
    port (
      B    : in  std_logic_vector (WIDTH-1 downto 0);  -- Second operand
      NorM : in  std_logic;                            -- OR or AND
      R    : out std_logic_vector (WIDTH-1 downto 0)   -- Result
      );
  end component;

  -- SIGNALS
  signal AddSub_in    : std_logic_vector (WIDTH-1 downto 0);
  signal AndOr_in     : std_logic_vector (WIDTH-1 downto 0);
  signal MovMvn_in    : std_logic_vector (WIDTH-1 downto 0);
  signal Xor_in       : std_logic_vector (WIDTH-1 downto 0);
  signal LSLLSR_in    : std_logic_vector (WIDTH-1 downto 0);
  signal ASRROR_in    : std_logic_vector (WIDTH-1 downto 0);
  signal MuxAA_in     : std_logic_vector (WIDTH-1 downto 0);
  signal MuxMX_in     : std_logic_vector (WIDTH-1 downto 0);
  signal MuxLA_in     : std_logic_vector (WIDTH-1 downto 0);
  signal MuxAM_in     : std_logic_vector (WIDTH-1 downto 0);
  signal ALUResult_in : std_logic_vector (WIDTH-1 downto 0);
  signal C_in         : std_logic;
  signal V_in         : std_logic;
begin
  -- Add/Sub or And/Or
  MUX_AA : MUX_2_to_1_n
    generic map(WIDTH => 32)
    port map(
      A0 => AddSub_in,
      A1 => AndOr_in,
      S  => ALUControl(1),
      Y  => MuxAA_in
      );

  -- MOV/MVN or XOR
  MUX_MX : MUX_2_to_1_n
    generic map(WIDTH => 32)
    port map(
      A0 => MovMvn_in,
      A1 => Xor_in,
      S  => ALUControl(1),
      Y  => MuxMX_in
      );

  -- Logic or arithmetic shift
  MUX_LA : MUX_2_to_1_n
    generic map(WIDTH => 32)
    port map(
      A0 => LSLLSR_in,
      A1 => ASRROR_in,
      S  => ALUControl(1),
      Y  => MuxLA_in
      );

  -- ADD/SUB/AND/OR or MOV/MVN/XOR
  MUX_AM : MUX_2_to_1_n
    generic map(WIDTH => 32)
    port map(
      A0 => MuxAA_in,
      A1 => MuxMX_in,
      S  => ALUControl(2),
      Y  => MuxAM_in
      );

  MUX_ML : MUX_2_to_1_n
    generic map(WIDTH => 32)
    port map(
      A0 => MuxAM_in,
      A1 => MuxLA_in,
      S  => ALUControl(3),
      Y  => ALUResult_in
      );

  NOR_TREE : NOR_n
    generic map(WIDTH => 32)
    port map(
      A => ALUResult_in,
      Z => Flags(1)
      );

  -- ADD/SUB
  ADD_SUB : ADD_SUB_n
    generic map (WIDTH => 32)
    port map(
      A    => SRC_A,
      B    => SRC_B,
      SorA => ALUControl(0),
      R    => AddSub_in,
      Cout => C_in,
      OV   => V_in
      );

  AND_OR : AND_OR_n
    generic map (WIDTH => 32)
    port map(
      OorA => ALUControl(0),
      R    => AndOr_in,
      A    => SRC_A,
      B    => SRC_B
      );

  LSL_LSR : LSL_LSR_n
    generic map (WIDTH => 32)
    port map(
      RorL => ALUControl(0),
      R    => LSLLSR_in,
      -- A    => SRC_A,
      -- B    => SRC_B
      A    => SRC_B,
      B    => SHAMT5
      );
  ASR_ROR : ASR_ROR_n
    generic map (WIDTH => 32)
    port map(
      RorA => ALUControl(0),
      R    => ASRROR_in,
      -- A    => SRC_A,
      A    => SRC_B,
      B    => SHAMT5
      );

  XOR_UNIT : XOR_n
    generic map (WIDTH => 32)
    port map(
      XorX => ALUControl(0),
      R    => Xor_in,
      A    => SRC_A,
      B    => SRC_B
      );

  MOV_MVN : MOV_MVN_n
    generic map (WIDTH => 32)
    port map(
      NorM => ALUControl(0),
      R    => MovMvn_in,
      B    => SRC_B
      );

  -- Flags
  -- 0: N
  -- 1: Z
  -- 2: C
  -- 3: V
  -- Prevent V and C from being 1 if calculation is not ADD or SUB
  Flags(3) <= ((V_in and not ALUControl(1)) and not ALUControl(2)) and not ALUControl(3);
  Flags(2) <= ((C_in and not ALUControl(1)) and not ALUControl(2)) and not ALUControl(3);
  Flags(0) <= ALUResult_in(WIDTH-1);

  -- Final result
  ALUResult <= ALUResult_in;

end structural;
