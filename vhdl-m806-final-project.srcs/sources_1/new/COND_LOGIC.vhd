----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/12/2020 06:02:03 PM
-- Design Name: 
-- Module Name: COND_LOGIC - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity COND_LOGIC is
  port(
    FLAGS      : in  std_logic_vector(3 downto 0);  --N,Z,C,V
    COND_FIELD : in  std_logic_vector(3 downto 0);
    CondEx_in  : out std_logic
    );
end COND_LOGIC;

architecture Behavioral of COND_LOGIC is
  signal N : std_logic;
  signal Z : std_logic;
  signal C : std_logic;
  signal V : std_logic;
begin
  N <= FLAGS(0);
  Z <= FLAGS(1);
  C <= FLAGS(2);
  V <= FLAGS(3);
  CondEx : process(COND_FIELD, N, Z, C, V)
  begin
    case COND_FIELD is
      when "0000"        => CondEx_in <= Z;                           --EQ
      when "0001"        => CondEx_in <= not Z;                       --NE
      when "0010"        => CondEx_in <= C;                           --CS/HS
      when "0011"        => CondEx_in <= not C;                       --CC/LO
      when "0100"        => CondEx_in <= N;                           --MI
      when "0101"        => CondEx_in <= not N;                       --PL
      when "0110"        => CondEx_in <= V;                           --VS
      when "0111"        => CondEx_in <= not V;                       --VC
      when "1000"        => CondEx_in <= (not Z) and C;               --HI
      when "1001"        => CondEx_in <= Z or not C;                  --LS
      when "1010"        => CondEx_in <= not(N xor V);                --GE
      when "1011"        => CondEx_in <= N xor V;                     --LT
      when "1100"        => CondEx_in <= (not Z) and (not(N xor V));  --GT
      when "1101"        => CondEx_in <= Z or (N xor V);              --LE
      when "1110"|"1111" => CondEx_in <= '1';                         --AL/none
      when others        => CondEx_in <= 'X';
    end case;
  end process;
end Behavioral;
