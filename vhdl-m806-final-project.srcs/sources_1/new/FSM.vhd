----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/01/2020 09:38:59 PM
-- Design Name: 
-- Module Name: FSM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSM is
  port(
    NoWrite_in : in std_logic;
    CondEx_in  : in std_logic;
    OP         : in std_logic_vector(1 downto 0);
    RD         : in std_logic_vector(3 downto 0);
    SL         : in std_logic;
    Instr_24   : in std_logic;
    CLK        : in std_logic;
    RESET      : in std_logic;

    PCWrite    : out std_logic;
    IRWrite    : out std_logic;
    RegWrite   : out std_logic;
    FlagsWrite : out std_logic;
    MAWrite    : out std_logic;
    MemWrite   : out std_logic;
    PCSrc      : out std_logic_vector(1 downto 0)
    );
end FSM;

architecture Behavioral of FSM is
  type FSM_STATES is
    (S0, S1, S2a, S2b, S3, S4a, S4b, S4c, S4d, S4e, S4f, S4g, S4h, S4i);
  signal current_state, next_state : FSM_STATES;

begin
  SYNC : process (CLK)
  begin
    if (CLK = '1' and CLK'event) then
      if (RESET = '1') then current_state <= S0;
      else current_state                  <= next_state;
      end if;
    end if;
  end process;

  ASYNC : process (current_state, NoWrite_in, CondEx_in, OP, RD, SL, Instr_24)
  begin
    next_state <= S0;
    PCWrite    <= '0';
    IRWrite    <= '1';
    RegWrite   <= '0';
    FlagsWrite <= '0';
    MAWrite    <= '0';
    MemWrite   <= '0';
    PCSrc      <= "00";
    case current_state is
      when S0 =>
        next_state <= S1;

      when S1 =>
        PCWrite    <= '0';
        IRWrite    <= '0';
        RegWrite   <= '0';
        FlagsWrite <= '0';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "00";
        if(CondEx_in = '0') then
          next_state <= S4c;
        elsif (CondEx_in = '1') then
          if (OP = "01") then
            next_state <= S2a;
          elsif (OP = "00" and NoWrite_in = '0') then  --Not CMP
            next_state <= S2b;
          elsif (OP = "00" and NoWrite_in = '1') then  --Not CMP
            next_state <= S4g;
          elsif (OP = "10") then
            if (Instr_24 = '0') then
              next_state <= S4h;
            elsif (Instr_24 = '1') then
              next_state <= S4i;
            end if;
          end if;
        end if;

      when S2a =>
        PCWrite    <= '0';
        IRWrite    <= '0';
        RegWrite   <= '0';
        FlagsWrite <= '0';
        MAWrite    <= '1';
        MemWrite   <= '0';
        PCSrc      <= "00";
        if (SL = '1') then
          next_state <= S3;
        elsif (SL = '0') then
          next_state <= S4d;
        end if;

      when S2b =>
        PCWrite    <= '0';
        IRWrite    <= '0';
        RegWrite   <= '0';
        FlagsWrite <= '0';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "00";
        if (SL = '0') then
          if(RD = X"F") then
            next_state <= S4b;
          else
            next_state <= S4a;
          end if;
        elsif (SL = '1') then
          if(RD = X"F") then
            next_state <= S4f;
          else
            next_state <= S4e;
          end if;
        end if;

      when S3 =>
        PCWrite    <= '0';
        IRWrite    <= '0';
        RegWrite   <= '0';
        FlagsWrite <= '0';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "00";
        if(RD = X"F") then
          next_state <= S4b;
        else
          next_state <= S4a;
        end if;

      when S4a =>
        PCWrite    <= '1';
        IRWrite    <= '0';
        RegWrite   <= '1';
        FlagsWrite <= '0';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "00";
        next_state <= S0;

      when S4b =>
        PCWrite    <= '1';
        IRWrite    <= '0';
        RegWrite   <= '0';
        FlagsWrite <= '0';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "10";
        next_state <= S0;

      when S4c =>
        PCWrite    <= '1';
        IRWrite    <= '0';
        RegWrite   <= '0';
        FlagsWrite <= '0';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "00";
        next_state <= S0;

      when S4d =>
        PCWrite    <= '1';
        IRWrite    <= '0';
        RegWrite   <= '0';
        FlagsWrite <= '0';
        MAWrite    <= '0';
        MemWrite   <= '1';
        PCSrc      <= "00";
        next_state <= S0;

      when S4e =>
        PCWrite    <= '1';
        IRWrite    <= '0';
        RegWrite   <= '1';
        FlagsWrite <= '1';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "00";
        next_state <= S0;

      when S4f =>
        PCWrite    <= '1';
        IRWrite    <= '0';
        RegWrite   <= '0';
        FlagsWrite <= '1';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "10";
        next_state <= S0;

      when S4g =>
        PCWrite    <= '1';
        IRWrite    <= '0';
        RegWrite   <= '0';
        FlagsWrite <= '1';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "00";
        next_state <= S0;

      when S4h =>
        PCWrite    <= '1';
        IRWrite    <= '0';
        RegWrite   <= '0';
        FlagsWrite <= '0';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "11";
        next_state <= S0;

      when S4i =>
        PCWrite    <= '1';
        IRWrite    <= '0';
        RegWrite   <= '1';
        FlagsWrite <= '0';
        MAWrite    <= '0';
        MemWrite   <= '0';
        PCSrc      <= "11";
        next_state <= S0;
    end case;
  end process;

end Behavioral;
