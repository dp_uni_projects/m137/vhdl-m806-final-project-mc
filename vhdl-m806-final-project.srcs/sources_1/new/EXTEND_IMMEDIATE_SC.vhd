----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/18/2020 08:09:44 PM
-- Design Name: 
-- Module Name: EXTEND - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EXTEND_IMMEDIATE_SC is
  port(
    SZ_in  : in  std_logic_vector (23 downto 0);
    ImmSrc : in  std_logic;
    SZ_out : out std_logic_vector (31 downto 0)
    );
end EXTEND_IMMEDIATE_SC;

architecture Structural of EXTEND_IMMEDIATE_SC is
  component EXTEND_n_m is
    generic (
      WIDTH_IN  : positive := 4;
      WIDTH_OUT : positive := 8
      );
    port(
      SZ_in  : in  std_logic_vector (WIDTH_IN-1 downto 0);
      SZ_out : out std_logic_vector (WIDTH_OUT-1 downto 0);
      SorZ   : in  std_logic            --Select Signed or Zero extension      
      );
  end component;

  component MUX_2_to_1_n is
    generic (
      WIDTH : positive := 4
      );
    port(
      A0 : in  std_logic_vector (WIDTH-1 downto 0);
      A1 : in  std_logic_vector (WIDTH-1 downto 0);
      S  : in  std_logic;
      Y  : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  -- Interal signals
  signal Ext_12_32_in : std_logic_vector (31 downto 0);
  signal Ext_26_32_in : std_logic_vector (31 downto 0);
  signal SZ_In_mul_4  : std_logic_vector (25 downto 0);

begin
  MUX_IN : MUX_2_to_1_n
    generic map(WIDTH => 32)
    port map(
      A0 => Ext_12_32_in,
      A1 => Ext_26_32_in,
      S  => ImmSrc,
      Y  => SZ_out
      );

  --Get 12 Least significant bits and extend to 32 if ImmSrc = 0
  EXTEND_12_32 : EXTEND_n_m
    generic map(
      WIDTH_IN  => 12,
      WIDTH_OUT => 32)
    port map(
      SZ_in  => SZ_in (11 downto 0),
      SZ_out => Ext_12_32_in,
      SorZ   => '0'                     --Zero extension
      );
  --Get 26 bits and extend to 32 if ImmSrc = 0
  EXTEND_26_32 : EXTEND_n_m
    generic map(
      WIDTH_IN  => 26,
      WIDTH_OUT => 32)
    port map(
      SZ_in  => SZ_in_mul_4,
      SZ_out => Ext_26_32_in,
      SorZ   => '1'                     --Signed extension
      );

  -- multiply SZ_in by 4, by appending two zeros
  SZ_in_mul_4 <= SZ_in & "00";

end Structural;
