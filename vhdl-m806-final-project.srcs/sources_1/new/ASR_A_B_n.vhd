----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/02/2020 07:54:40 PM
-- Design Name: 
-- Module Name: ASR_A_B_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ASR_A_B_n is
  generic (WIDTH : positive := 4);
  port(
    A : in  std_logic_vector (WIDTH-1 downto 0);
    B : in  std_logic_vector (4 downto 0);
    Z : out std_logic_vector (WIDTH-1 downto 0)
    );
end ASR_A_B_n;

architecture Behavioral of ASR_A_B_n is

begin
  ARSHIFTER : process (A, B)
    variable A_n : signed (WIDTH-1 downto 0);
    variable B_n : natural range 0 to 31;  --Shift amount
  begin
    B_n := to_integer(unsigned(B));        --Convert B to number
    A_n := signed(A);
    Z   <= std_logic_vector(SHIFT_RIGHT(A_n, B_n));
  end process ARSHIFTER;

end Behavioral;
