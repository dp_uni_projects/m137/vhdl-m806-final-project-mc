----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/16/2020 04:19:29 PM
-- Design Name: 
-- Module Name: REGISTER_FILE_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
-- use UNISIM.VComponents.all;

entity REGISTER_FILE_n is
  generic (
    N : positive := 4;                  --Register addressing width
    M : positive := 32                  --Data width
    );
  port (
    CLK       : in  std_logic;
    WE        : in  std_logic;
    ADDR_W    : in  std_logic_vector(N-1 downto 0);
    ADDR_R1   : in  std_logic_vector(N-1 downto 0);
    ADDR_R2   : in  std_logic_vector(N-1 downto 0);
    DATA_IN   : in  std_logic_vector(M-1 downto 0);
    DATA_OUT1 : out std_logic_vector(M-1 downto 0);
    DATA_OUT2 : out std_logic_vector(M-1 downto 0)
    );
end REGISTER_FILE_n;

architecture Behavioral of REGISTER_FILE_n is
  -- Declare a 2D array of 2**N-1 * M bits
  type RF_ARRAY is array (2**N-1 downto 0)
    of std_logic_vector (M-1 downto 0);

  signal RF : RF_array;
begin
  -- Process to write data to register, if WE is enabled
  REG_FILE : process(CLK)
  begin
    if (CLK = '1' and CLK'event) then
      if (WE = '1') then
        RF(to_integer(unsigned(ADDR_W))) <= DATA_IN;
      end if;
    end if;
  end process;
  -- Always output two registers, depending on the two addresses
  DATA_OUT1 <= RF(to_integer(unsigned(ADDR_R1)));
  DATA_OUT2 <= RF(to_integer(unsigned(ADDR_R2)));
end Behavioral;

