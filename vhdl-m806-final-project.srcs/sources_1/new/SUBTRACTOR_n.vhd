----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/30/2020 01:05:09 PM
-- Design Name: 
-- Module Name: SUBTRACTOR_n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Signed subtractor
entity SUBTRACTOR_n is
  generic (WIDTH : positive := 4);
  port (
    A    : in  std_logic_vector (WIDTH-1 downto 0);
    B    : in  std_logic_vector (WIDTH-1 downto 0);
    S    : out std_logic_vector (WIDTH-1 downto 0);
    Cout : out std_logic;
    OV   : out std_logic);
end SUBTRACTOR_n;

architecture Behavioral of SUBTRACTOR_n is
begin
  SUBTACTOR : process (A, B)
    variable A_s, B_s, S_s : signed (WIDTH+1 downto 0);
  begin
    A_s  := signed('0' & A(WIDTH-1) & A);
    B_s  := signed('0' & B(WIDTH-1) & B);
    S_s  := A_s - B_s;
    S    <= std_logic_vector(S_s(WIDTH-1 downto 0));  -- numeric_std
    OV   <= S_s(WIDTH) xor S_s(WIDTH-1);
    Cout <= S_s(WIDTH+1);
  end process;
end Behavioral;
