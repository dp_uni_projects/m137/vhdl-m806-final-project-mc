----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/17/2020 09:51:06 PM
-- Design Name: 
-- Module Name: REGISTER_FILE_wR15 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity REGISTER_FILE_wR15 is
  generic (
    N : positive := 4;                  --Register addressing width
    M : positive := 32                  --Data width
    );
  port (
    CLK       : in  std_logic;
    WE        : in  std_logic;
    ADDR_W    : in  std_logic_vector(N-1 downto 0);
    ADDR_R1   : in  std_logic_vector(N-1 downto 0);
    ADDR_R2   : in  std_logic_vector(N-1 downto 0);
    DATA_IN   : in  std_logic_vector(M-1 downto 0);
    R15       : in  std_logic_vector(M-1 downto 0);  -- Extra register,
                                                     -- coming from PC
    DATA_OUT1 : out std_logic_vector(M-1 downto 0);
    DATA_OUT2 : out std_logic_vector(M-1 downto 0)
    );

end REGISTER_FILE_wR15;

architecture Structural of REGISTER_FILE_wR15 is
  component AND_n is
    generic (WIDTH : positive := 4);
    port(
      A : in  std_logic_vector (WIDTH-1 downto 0);
      Z : out std_logic
      );
  end component;

  component REGISTER_FILE_n is
    generic (
      N : positive := 4;
      M : positive := 32
      );
    port (
      CLK       : in  std_logic;
      WE        : in  std_logic;
      ADDR_W    : in  std_logic_vector(N-1 downto 0);
      ADDR_R1   : in  std_logic_vector(N-1 downto 0);
      ADDR_R2   : in  std_logic_vector(N-1 downto 0);
      DATA_IN   : in  std_logic_vector(M-1 downto 0);
      DATA_OUT1 : out std_logic_vector(M-1 downto 0);
      DATA_OUT2 : out std_logic_vector(M-1 downto 0)
      );
  end component;

  -- Use two MUXes for getting R15
  component MUX_2_to_1_n is
    generic (
      WIDTH : positive := 4
      );
    port(
      A0 : in  std_logic_vector (WIDTH-1 downto 0);
      A1 : in  std_logic_vector (WIDTH-1 downto 0);
      S  : in  std_logic;
      Y  : out std_logic_vector (WIDTH-1 downto 0)
      );
  end component;

  -- Two signals coming out of each AND gate
  signal And1_in : std_logic;
  signal And2_in : std_logic;

  -- Two signals coming out of internal Register file
  signal DataOut1_in : std_logic_vector(M-1 downto 0);
  signal DataOut2_in : std_logic_vector(M-1 downto 0);

begin
  -- Internal component of final register file,
  -- will contain the first 15 registers, R0-R14.
  -- Using N = 4, it will contain 16 registers actually,
  -- but we will only use the first 15.
  REG_FILE_IN : REGISTER_FILE_n
    generic map (
      N => N,
      M => M
      )
    port map(
      CLK       => CLK,
      WE        => WE,
      ADDR_W    => ADDR_W,
      ADDR_R1   => ADDR_R1,
      ADDR_R2   => ADDR_R2,
      DATA_IN   => DATA_IN,
      DATA_OUT1 => DataOut1_in,
      DATA_OUT2 => DataOut2_in
      );

  -- First MUX, selects between RD1 from RF or R15
  MUX_DATA_OUT1 : MUX_2_to_1_n
    generic map (WIDTH => M)
    port map (
      A0 => DataOut1_in,
      A1 => R15,
      S  => And1_in,
      Y  => DATA_OUT1
      );

  -- Second MUX, selects between RD2 from RF or R15
  MUX_DATA_OUT2 : MUX_2_to_1_n
    generic map (WIDTH => M)
    port map (
      A0 => DataOut2_in,
      A1 => R15,
      S  => And2_in,
      Y  => DATA_OUT2
      );

  AND1 : AND_n
    generic map (WIDTH => N)
    port map(
      A => ADDR_R1,
      Z => And1_in
      );

  AND2 : AND_n
    generic map (WIDTH => N)
    port map(
      A => ADDR_R2,
      Z => And2_in
      );

end Structural;
